/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader.files', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="files-uploader abstract-uploader">'
            + '<div class="files-list clearfix" ui-sortable="{placeholder:\'item placeholder\', forcePlaceholderSize:true, items:\'>.item\', opacity:0.8, tolerance:\'pointer\', axis:\'y\'}" ng-model="files">'
                + '<files-list-item file="file" ng-repeat="file in files"></files-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Файлы не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить файлы</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
			+ '<div class="btn btn-small btn-info" ng-controller="fileFtpSelectCtrl" ng-click="ftpShowWindow()">'
                + '<i class="icon-white icon-hdd"></i>&nbsp;'
                + 'Выбрать по FTP'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-info btn-small" tooltip="Заголовки файлов, даты и сортировка сохраняется сразу, без отправки формы">'
                + '<i class="icon-question-sign icon-white"></i>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.files', template);
    }])
	/**
     * Контроллер для выбора файла по FTP
     */
    .controller('fileFtpSelectCtrl', ['$scope', '$dialog', '$http', '$rootScope', function($scope, $dialog, $http, $rootScope) {
        /**
         * Диалог выбора файла по FTP
         */
        $scope.ftpShowWindow = function() {
            var d = $dialog.dialog({
                modalFade:true
            });

            var temp = window.tinymce;

            window.tinymce = {
                activeEditor: {
                    windowManager: {
                        getParams: function() {
                            return {
                                setUrl: function(url) {
                                    var self = window.location.protocol + '//' + window.location.host;

                                    if (url.toString().indexOf(self) === 0) {
                                        url = url.toString().substr(self.length);
                                    }

                                    $scope.$apply(function() {
                                        $scope.uploadByFtp(url);
                                    });
                                }
                            }
                        },

                        close: function(){
                            d.close();
                            window.tinymce = temp;
                        }
                    }
                }
            };

            d.open('cms.filesuploader.video.ftp');
        };

        /**
         * Загрузка видео по FTP
         */
        $scope.uploadByFtp = function(url) {
            $scope.$parent.inProgress++;

            $http({
                method:'POST',
                data: {url:url},
                url: $scope.$parent.url + 'byftp/'
            }).success(function(response) {
                $scope.$parent.inProgress--;

                if (response.success&& !!response.entity) {
                    $scope.$parent.files.push(response.entity);
                } else {
                    $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке файла' + response.msg, 10000);
                }
            }).error(function(response) {
                $scope.$parent.inProgress--;
                console.log(response);
                $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке файла' + response.msg, 10000);
            });
        };
    }])
    /**
     * Элемент загрузчика файлов
     */
    .directive('filesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item">'
                    + '<input type="text" ng-model="file.title" class="input-title">'
                    + '<input type="text" ng-model="file.date" class="input-date">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-small" tooltip="Скачать {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-small btn-inverse sort-icon" tooltip="Потяните для сортировки"><i class="icon-move icon-white"></i></div>'
                    + '<div class="btn btn-danger btn-small" tooltip="Удалить" ng-click="remove()"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление файла';
                    var msg = 'Вы уверены, что хотите удалить файл?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Обновление полей
                 */
                var fst = 0;
                $scope.updateFields = function() {
                    if (fst < 2) {
                        fst++;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {title: $scope.file.title, date: $scope.file.date},
                        timeout: this.canceler.promise
                    });
                };
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', $scope.updateFields);

                // Редактирование даты файла
                $scope.$watch('file.date', $scope.updateFields);
            }
        }
    }])
;
