<?php

namespace Krd\CityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\CoreBundle\Entity\Node;
use Q\FormMetadataBundle\Configuration as Form;


/**
 * Парки
 *
 * @ORM\Entity
 * @ORM\Table(name="city_park",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     icon={"files", {
 *         "label"="Иконка",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     redirect={"text", {"label"="Внешняя ссылка", "required"=false}},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Park implements NodeUrlIntf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Название
     * @ORM\Column(type="string")
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="city_park_icon_relation",
     *      joinColumns={@ORM\JoinColumn(name="park_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="icon_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $icon;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="city_park_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="park_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $images;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $redirect;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("icon_main")
     * @JMS\Accessor(getter="getMainIcon")
     */
    private $mainImage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->icon = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Park
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Park
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Park
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return Park
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Park
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Park
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Q\CoreBundle\Entity\Node $parent
     * @return Park
     */
    public function setParent(\Q\CoreBundle\Entity\Node $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Q\CoreBundle\Entity\Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add icon
     *
     * @param \Q\FilesBundle\Entity\Image $icon
     * @return Park
     */
    public function addIcon(\Q\FilesBundle\Entity\Image $icon)
    {
        $this->icon[] = $icon;

        return $this;
    }

    /**
     * Remove icon
     *
     * @param \Q\FilesBundle\Entity\Image $icon
     */
    public function removeIcon(\Q\FilesBundle\Entity\Image $icon)
    {
        $this->icon->removeElement($icon);
    }

    /**
     * Get icon
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIcon()
    {
        return $this->icon;
    }

    public function hasIcon()
    {
        return count($this->getIcon()) > 0;
    }

    public function getMainIcon()
    {
        foreach($this->getIcon() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getIcon();

        return $images[0];
    }

    /**
     * Add images
     *
     * @param \Q\FilesBundle\Entity\Image $images
     * @return Park
     */
    public function addImage(\Q\FilesBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Q\FilesBundle\Entity\Image $images
     */
    public function removeImage(\Q\FilesBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Park
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Park
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set redirect
     *
     * @param string $redirect
     * @return Park
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;

        return $this;
    }

    /**
     * Get redirect
     *
     * @return string
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Has redirect
     *
     * @return boolean
     */
    public function hasRedirect()
    {
        return !empty($this->redirect);
    }

    public function getUrl()
    {
        if ($this->hasRedirect()) {
            return $this->getRedirect();
        }

        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getDetailRoute()
    {
        return 'krd_city_park_detail';
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Park
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
