<?php

namespace Q\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;


/**
 * Точка на яндекс-карте
 */
class MapPointType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'type' => 'text',
            'allow_add' => true,
            'allowAddPolygon' => false,
        ));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['allow_add_polygon'] = $options['allowAddPolygon'];
    }

    public function getParent()
    {
        return 'collection';
    }

    public function getName()
    {
        return 'map_point';
    }
}
