<?php

namespace Krd\CommentsBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Bridge\Monolog\Logger;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;
use Q\CoreBundle\Entity\Config;
use Krd\CommentsBundle\Entity\Comment;


/**
 * События для Comment
 */
class CommentSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var array
     */
    protected $deferred = array();

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Сбор ново-добавленных обращений
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (($entity instanceof Comment)) {
            $this->deferred[] = $entity;
        }
    }

    /**
     * Отправка Email уведомлений о новых комментариях
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $list = $this->deferred;
        $this->deferred = array();

        foreach($list as $entity) {
            $message = $this->getMailer()->newMessage('Новый комментарий на сайте %title%');

            try {
                $this->getMailer()->sendTemplate($message, 'KrdCommentsBundle:Email:new-comment.admin.html.twig', array('comment' => $entity));
            } catch (\Exception $e) {
                $this->logger->crit('Ошибка отправки email уведомления о новом комментарии', array($entity->getId(), $e->getMessage()));
            }
        }
    }
}

