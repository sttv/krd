<?php

namespace Krd\UserBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\SiteBundle\Modules\Reporter as ReporterMod;
use Krd\UserBundle\Entity\Widget;


/**
 * Модуль подписок личного кабинета
 */
class SubscribeModule extends AbstractModule
{
    /**
     * @var ReporterMod
     */
    protected $reporterModule;

    public function setReporterModule(ReporterMod $m)
    {
        $this->reporterModule = $m;
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    /**
     * Получение текущей просматриваемой подписки на основе запроса
     */
    public function getCurrent()
    {
        $id = $this->request->attributes->get('id');
        return $this->getRepository()->find($id);
    }

    /**
     * Список для текущей страницы
     * @param integer[optional] $count
     * @return array
     */
    public function getItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.sent = 1')
            ->orderBy('n.date', 'DESC');

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count);
    }

    /**
     * Список подписок
     */
    public function renderContent()
    {
        return $this->twig->render('KrdUserBundle:Module:subscribe/index.html.twig', array('list' => $this->getItemsList()));
    }

    /**
     * Список публикаций репортера
     */
    public function renderReporterContent()
    {
        $list = $this->reporterModule->getLastPublications(0, 10);
        return $this->twig->render('KrdUserBundle:Module:subscribe/index-reporter.html.twig', array('list' => $list));
    }

    /**
     * Подробная страница
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdUserBundle:Module:subscribe/detail.html.twig', array('letter' => $this->getCurrent()));
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget(Widget $widget)
    {
        return $this->twig->render('KrdUserBundle:Module:subscribe/widget.html.twig', array('list' => $this->getItemsList(4), 'widget' => $widget));
    }
}
