<?php

namespace Krd\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Личный кабинет
 */
class KrdUserBundle extends Bundle
{
}
