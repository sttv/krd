<?php

namespace Krd\CultureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Бандл страницы главы
 */
class KrdCultureBundle extends Bundle
{
}
