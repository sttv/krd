/**
 * Комментарии
 */
angular.module('comments', ['user', 'user.notifications'])
    /**
     * Расшаренные данные блока комментариев
     */
    .service('commentsData', [function() {
        var container = {};

        this.registerList = function(id) {
            container[id] = {};

            return container[id];
        };

        this.get = function(id) {
            return !!container[id] ? container[id] : false;
        };
    }])

    /**
     * Список комментариев
     */
    .directive('commentsList', ['commentsData', function(commentsData) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                commentsData.registerList(attrs.commentsList);
            }
        }
    }])

    /**
     * Элемент комментариев
     */
    .directive('commentItem', ['user', 'commentsData', '$timeout', function(user, commentsData, $timeout) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.children('.links')
                    .after('<div class="well" ng-include="formTemplate" ng-if="isOpen()"></div>')
                    .children('.action-reply').attr('ng-click', 'toggleOpen()')
                    .parent()
                    .children('.action-activate')
                        .attr('ng-if', 'isAdmin && !active')
                        .attr('ng-click', 'activate()')
                    .parent()
                    .children('.action-deactivate')
                        .attr('ng-if', 'isAdmin && active')
                        .attr('ng-click', 'deactivate()')
                    .parent()
                    .children('.action-delete')
                        .attr('ng-if', 'isAdmin')
                        .attr('ng-click', 'remove()')
                ;

                return function($scope, $element, attrs) {
                    $scope.id = parseInt($element.data('id'), 10);
                    $scope.formTemplate = user.isAuthorized() ? 'comments-create-well' : 'comments-auth-well';
                    $scope.shared = {};
                    $scope.isAdmin = user.isGranted('ROLE_COMMENT_ADMIN');
                    $scope.active = (attrs.active == 1);
                    $timeout(function() {
                        $scope.shared = commentsData.get(attrs.commentItem);
                    });

                    $scope.$watch('isOpen()', function(open) {
                        if (open) {
                            $element.addClass('open');
                        } else {
                            $element.removeClass('open');
                        }
                    });
                }
            },

            controller: ['$scope', '$element', '$http', 'router', 'notifications', function($scope, $element, $http, router, notifications) {
                // Проверка открыт ли комментарий
                $scope.isOpen = function() {
                    return $scope.shared.openedId == $scope.id;
                };

                // Переключение состояния комментария
                $scope.toggleOpen = function(id) {
                    if (id === false || $scope.isOpen()) {
                        $scope.shared.openedId = -1;
                    } else {
                        $scope.shared.openedId = $scope.id;
                    }
                };

                // Активация комментария
                $scope.activate = function() {
                    $http.post(router.generate('krd_comments_activate', {':id':$scope.id}))
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий активирован');
                                $scope.active = true;
                            } else {
                                notifications.user.error('Не удалось активировать комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось активировать комментарий');
                        });
                };

                // Деактивация комментария
                $scope.deactivate = function() {
                    $http.post(router.generate('krd_comments_deactivate', {':id':$scope.id}))
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий деактивирован');
                                $scope.active = false;
                            } else {
                                notifications.user.error('Не удалось деактивировать комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось деактивировать комментарий');
                        });
                };

                // Удаление комментария
                $scope.remove = function() {
                    if (!confirm('Удалить этот и все вложенные комментарии?')) {
                        return;
                    }

                    $http({
                            method: 'DELETE',
                            url: router.generate('krd_comments_delete', {':id':$scope.id})
                        })
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий удален');
                                $element.remove();
                            } else {
                                notifications.user.error('Не удалось удалить комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось удалить комментарий');
                        });
                };
            }]
        }
    }])

    /**
     * Контроллер формы добавления комментариев
     */
    .controller('CommentsCreateFormCtrl', ['$scope', 'notifications', function($scope, notifications) {
        $scope.onSuccess = function(response) {
            notifications.user.info(response.message || 'Комментарий успешно отправлен и появится на сайте после проверки модератором.');

            if (!!$scope.$parent.toggleOpen) {
                $scope.$parent.toggleOpen(false);
            }

            $scope.content = '';
        };

        $scope.onFatalError = function() {
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Контроллер формы авторизации
     */
    .controller('CommentsAuthFormCtrl', ['$scope', 'notifications', function($scope) {
        $scope.onResponse = function(response) {
            // Костыли костыли :(
            var $r = $(response);

            if ($r.filter('.container-fluid').find('form > .alert-error').length > 0) {
                $scope.errors['_username'] = ['Неверный логин или пароль'];
            } else {
                window.location.reload();
            }
        };

        $scope.onFatalError = function(){
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Отправка сообщения по Ctrl+Enter
     */
    .directive('ctrlEnter', [function() {
        return function($scope, $element, attrs) {
            if (!attrs.ctrlEnter) {
                return;
            }

            $element.on('keydown', function(e) {
                if (e.ctrlKey && e.keyCode == 13) {
                    $scope.$eval(attrs.ctrlEnter);
                }
            });
        }
    }])
;
