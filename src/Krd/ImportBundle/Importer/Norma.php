<?php

namespace Krd\ImportBundle\Importer;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Entity\Node;
use Krd\DocumentBundle\Entity\Document;
use Krd\ImportBundle\Exception\ImportNormaException;
use Krd\ImportBundle\Old\RemoteLoader;


/**
 * Импорт документов с http://old.krd.ru/www/norma.nsf/BaseSearch/
 */
class Norma extends AbstractImporter
{
    protected $remote;

    public function __construct(EntityManager $em, RemoteLoader $remote)
    {
        $this->em = $em;
        $this->remote = $remote;

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em->getConfiguration()->setResultCacheImpl(new \Krd\ImportBundle\Doctrine\Cache\NoCache());
    }

    /**
     * Импорт всех постановлений в указанный раздел
     * @param  Node   $node
     * @param  boolean[optional] $updateMode
     */
    public function importAll(Node $node, $updateMode = false)
    {
        $this->outputWriteln('Try to load remote page...');

        $html = $this->remote->getNormasListPage();

        if (!$html) {
            $this->outputWriteln('<error>Import failed. No remote content.</error>');
            return;
        }

        $crawler = new Crawler();
        $crawler->addHtmlContent($html, 'utf-8');

        $html = $crawler->filter('body')->html();

        $items = explode('<hr noshade size="1">', $html);

        $i = 0;
        $j = 0;
        foreach($items as $item) {
            if ($this->importItem($item, $node, $updateMode)) {
                $i++;
            } else {
                $j++;
            }
        }

        if ($i > 0) {
            $this->outputWriteln('Imported <info>'.$i.'</info> normas and <info>'.$j.'</info> was skipped');
        } else {
            $this->outputWriteln('<comment>No one normas imported</comment>');
        }
    }

    /**
     * Импорт одного элементв
     * @param string $item
     * @param Node $node
     * @param  boolean[optional] $updateMode
     */
    protected function importItem($item, Node $node, $updateMode = false)
    {
        $tmp = explode('<br', $item);
        $title = trim(strip_tags($tmp[0]));

        if (!preg_match('/^постановление/iu', $title) && !preg_match('/^решение городской думы/iu', $title)) {
            return false;
        }

        $tmp = explode(' ', $title);
        $date = end($tmp);
        $date = \DateTime::createFromFormat('d.m.Y', $date);

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($item, 'utf-8');
            $link = $crawler->filter('a');

            $tmp = explode('/', $link->attr('href'));
            $hash = end($tmp);

            $announce = $link->text();
        } catch(\InvalidArgumentException $e) {
            return false;
        }

        if (empty($title) || empty($announce) || empty($date) || empty($hash)) {
            return false;
        }

        $document = $this->em->getRepository('KrdDocumentBundle:Document')->findOneBy(array('hash' => $hash, 'parent' => $node->getId()));

        if ($document instanceof Document) {
            if (!$updateMode) {
                return false;
            }
        } else {
            $document = new Document();
            $document->setParent($node);
            $this->em->persist($document);
        }

        $document->setTitle($title);
        $document->setAnnounce($announce);
        $document->setDate($date);
        $document->setHash($hash);
        $document->setActive(true);
        $document->setAutoupdate(true);
        $document->setForceUpdate(true);
        $document->generateName();

        $this->em->flush();

        $this->outputWriteln('Add <info>'.$hash.'</info>');

        return true;
    }
}
