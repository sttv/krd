<?php

namespace Q\CoreBundle\Seo;

use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Router\Breadcrumb\Manager as BreadcrumbManager;
use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Entity\Config;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;


/**
 * Управление SEO данными текущей страницы
 */
class SeoManager
{
    /**
     * Название сайта для title`а
     */
    protected $titleRoot = '';

    /**
     * Seo title
     */
    protected $title = array();

    /**
     * Seo description
     */
    protected $description;

    /**
     * Seo key-words
     */
    protected $keywords;

    /**
     * Seo image
     */
    protected $image = 'http://krd.ru/bundles/krdsite/i/logo.png';

    protected $config;
    protected $breadcrumb;
    protected $request;
    protected $curerntNode;

    public function __construct(Config $config, BreadcrumbManager $breadcrumb, Request $request, CurrentNode $currentNode)
    {
        $this->config = $config;
        $this->breadcrumb = $breadcrumb;
        $this->request = $request;
        $this->currentNode = $currentNode;

        $this->titleRoot = $config->getTitle();

        foreach($this->breadcrumb->getItems() as $i => $item) {
            if ($i > 0) {
                $this->addTitle($item->getTitle());
            }
        }

        $this->title = array_reverse($this->title);

        $node = $this->currentNode->getNode();

        if ($node instanceof Node) {
            if ($node->hasSeoTitle()) {
                $this->setTitle($node->getSeoTitle());
            }

            if ($node->hasSeoKeywords()) {
                $this->setKeywords($node->getSeoKeywords());
            }

            if ($node->hasSeoDescription()) {
                $this->setDescription($node->getSeoDescription());
            }

            if ($node->hasSeoImage()) {
                $this->setImage($node->getSeoImage());
            }
        }
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Вывод заголовка в виде одной строки
     * @return string
     */
    public function getTitlePrint()
    {
        if (empty($this->title)) {
            return $this->titleRoot;
        } else {
            return implode(' :: ', $this->title).' :: '.$this->titleRoot;
        }
    }

    public function hasTitle()
    {
        return !empty($this->title);
    }

    public function addTitle($title)
    {
        $this->title[] = $title;
    }

    public function setTitle($title)
    {
        if (is_array($title)) {
            $this->title = $title;
        } else {
            $this->title = array($title);
        }
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function hasDescription()
    {
        return !empty($this->description);
    }

    public function setDescription($descr)
    {
        $this->description = $descr;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function hasKeywords()
    {
        return !empty($this->keywords);
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function hasImage()
    {
        return !empty($this->image);
    }

    public function setImage($image)
    {
        if (!is_string($image) && !($image instanceof Image)) {
            throw new \InvalidArgumentException('Image must be a string or an Image');
        }

        if ($image instanceof Image) {
            $image = $image->getUrl('socialimg');
        }

        if (mb_strpos($image, 'http://') !== 0 && mb_strpos($image, 'https://') !== 0 && mb_strpos($image, '//') !== 0) {
            $image = $this->request->getScheme().'://'.$this->request->getHost().$image;
        }

        if (mb_strpos($image, '//') === 0) {
            $image = $this->request->getScheme().':'.$image;
        }

        $this->image = $image;
    }
}
