<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Парки города
 *
 * @Route("/park")
 */
class ParkController extends Controller
{
    /**
     * Список
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdCityBundle:Park')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->setParameter('parent', $parent)
            ->orderBy('n.sort', 'ASC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Детально
     *
     * @Route("/item/{id}.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function itemAction($id)
    {
        $item = $this->getDoctrine()->getRepository('KrdCityBundle:Park')->find($id);

        if (!$item || !$item->getActive()) {
            throw $this->createNotFoundException();
        }

        $response = new Response();
        $response->headers->set('Content-type', 'application/json');

        $json = $this->get('jms_serializer')->serialize($item, 'json');
        $json = json_decode($json, true);
        unset($json['icon_main']);
        unset($json['name']);
        unset($json['active']);
        unset($json['sort']);
        $json['images'] = $item->getImages();
        $json['content'] = $item->getContent();
        $json = $this->get('jms_serializer')->serialize($json, 'json');

        $response->setContent($json);

        return $response;
    }
}
