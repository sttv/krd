<?php

namespace Krd\SubscribeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Форма подписки на экстренные новости
 */
class ESubscriberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'text', array(
            'required' => false,
            'label' => 'Введите ваш e-mail:',
            'constraints' => array(
                new Assert\Email(array('message' => 'Email имеет не верный формат')),
                new Assert\NotBlank(array('message' => 'Вы забыли указать Email')),
            ),
            'csrf_protection' => true,
            'label_attr' => array('class' => 'label'),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\SubscribeBundle\Entity\ESubscriber',
        ));
    }

    public function getName()
    {
        return 'e_subscriber';
    }
}
