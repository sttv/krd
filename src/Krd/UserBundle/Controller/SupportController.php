<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Техподдержка
 */
class SupportController extends Controller implements ActiveSecuredController
{
    /**
     * Главная
     *
     * @Route("/", name="krd_user_support")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/list.json", name="ajax_krd_user_support_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.user.modules.support')->getMyMessages();
    }

    /**
     * Просмотр ветки сообщений
     *
     * @Route("/{name}.html", name="krd_user_support_detail")
     * @Template()
     */
    public function detailAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($item = $this->get('krd.user.modules.support')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));

            $form = $this->get('krd.user.modules.support')->getReplyFormBuilder()->getForm();
            $this->getDoctrine()->getManager()->getRepository('KrdSupportBundle:Message')->markAsReaded($item, $this->getUser());

            return array('message' => $item, 'form_reply' => $form->createView());
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
