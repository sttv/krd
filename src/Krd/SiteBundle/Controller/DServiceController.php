<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Предоставляемые услуги
 */
class DServiceController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * Страница подробно
     *
     * @Route("/{name}.html", name="krd_site_dservice_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.site.modules.dservice')->getCurrent()) {
            if ($item->hasRedirect()) {
                return $this->redirect($item->getRedirect());
            }

            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
