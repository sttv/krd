<?php

namespace Krd\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Страница галереи
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * Списка галарей по сути нет, мы просто редиректим на первую галерею
     *
     * @Route("/")
     */
    public function indexAction()
    {
        if ($first = $this->get('krd.gallery.modules.gallery')->getRepository()->findOneBy(array('active' => 1, 'parent' => $this->get('qcore.routing.current')->getNode()->getId()))) {
            return $this->redirect($first->getUrl());
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Страница галереи
     *
     * @Route("/{name}.html", name="krd_gallery_gallery_detail")
     * @Template()
     */
    public function detailAction($name)
    {
        if ($item = $this->get('krd.gallery.modules.gallery')->getCurrentGallery()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));

            if ($item->hasImages()) {
                $this->get('qcore.seo.manager')->setImage($item->getMainImage());
                $this->get('qcore.seo.manager')->setDescription($item->getTitle());
            }

        } else {
            throw $this->createNotFoundException();
        }
    }
}
