// mobile Hybrid Intel XDK app
;

var app = {
    isIntel: (typeof intel != "undefined"), // gali buti grynas HTML5
    offline_datadir: 'vilnius', // kitkas index.html nurodoma

    _fHandlers: {}, // intel device file download id:{callback:(), errback()}
    _popups: {},
    connection: "none", // default value for testing;  "wifi", "cell", or "none", possible "unknown"
    timeout: 3000, // miliseconds to load version.txt and check if connection is alive
    interval: 6000000, // miliseconds to check for new version

    ready: 0, // step download stops.txt and routes.txt ready++.
    //localStorage: {},
    localStorage: window.localStorage, // stops_lastSync, routes_lastSync, stops, routes

    i18n: function (id) {
        if (id in i18n) return i18n[id];
        else return {
            'noInternet': 'No internet connection!', // +
            'updateSuccessful': 'New data downloaded. Restart application to finish installing updates!',
            'outOfDate': 'out of date', // +
            'newDataFound': 'New data available: ', // +
            'checkForUpdates': 'Check for updates!',
            'cancel': 'Cancel', // +
            'restart': 'Restart',
            'download': 'Download new data!', // +
            'dataUpToDate': 'Data is up to date!'
        }[id];
    },

    getLastSync: function (key) {
        var key = key + '_lastSync';

        if (this.localStorage && key in this.localStorage) {
            var data = this.localStorage[key];

            if (data.length >= 18) { // 2014/01/01 10:00:00
                var d = new Date(data);
                return d.getTime();
            }
            else {
                return parseInt(data, 10); // int:ms
            }
        }
        return false;
    },

    /* patikrina ar device's turi "connection", jeigu turi bando atsisiusti version.txt - jeigu jo negauna per kazkuri laika:
    error:"slow internet connection", jeigu ok iraso data callback({online:true, version:false}) // slow...
    */
    checkOnlineDate: function (callback) { // negalima kviesti kol nera inicializuotas intel?
        var self = this;

        function test() { // testuojam ar pavyks atsisiusti version txt per app.timeout laika
            self.downloadUrl('version', 'GET', self.getURL('version')
                , function (data) {
                    var t = new Date(data); // parseInt(data, 10);
                    t = t && t.getTime()
                    //t = self.getTime(); // fake...

                    callback({ online: true, version: t });
                }
                , function (err) {
                    console.log("Version download error: ", err.message); // pvz neradome version.txt:404, arba ivyko timeout'as
                    callback({ online: false }); // gali nebuti interneto, o gali nebuti version.txt - zodziu rysio nera...
                }
                , self.timeout);
        }

        if (self.isIntel) {
            intel.xdk.device.updateConnection();

            window.setTimeout(function () {
                if (self.connection != "none") { // connection yra - bet ar yra padorus internetas?
                    test();
                }
                else {
                    callback({ online: false }); // device "connection" isjungtas
                }
            }, 100); // duodam laiko intel.xdk.device.connection.update eventu ivykti
        }
        else {
            test();
        }
    },

    // console.log - isjungiame isvedima...
    debugStop: function () {
        if (typeof this._logtmp != "function") { // jau turime
            this._logtmp = console.log;
        }
        console.log = function () { };

        if (this._logDiv) {
            document.body.removeChild(this._logDiv);
            this._logDiv = null;
        }
    },

    debugStart: function (todiv) {
        if (todiv) {
            this.debugStop();

            if (!this._logDiv) {
                this._logDiv = document.createElement('div');
                this._logDiv.style.cssText = 'color:black;position:absolute;width:100%;height:100%;opacity:1.0;z-index:100;';
                this._logDiv.id = "log";
                document.body.appendChild(this._logDiv);
            }

            var log = function (text, a1, a2, a3, a4, a5) {
                a1 = a1 ? a1.toString() + ", " : "";
                a2 = a2 ? a2.toString() + ", " : "";
                a3 = a3 ? a3.toString() + ", " : "";
                a4 = a4 ? a4.toString() + ", " : "";
                a5 = a5 ? a5.toString() + ", " : "";
                app._logDiv.innerHTML += [text.toString(), a1, a2, a3, a4, a5].join("") + "<br/>";
            }
            console.log = log;
        } else if (typeof this._logtmp == "function") {
            console.log = this._logtmp;
        }
    },

    startAutoUpdate: function () {
        window.setInterval(function () {
            if (!jq("#mask").length) {
                app.checkForUpdates(true);
            }
        }, this.interval);
    },

    // mobile app init()
    init: function (callback) {
        var self = this; //_fHandlers


        if (self.isIntel) {
            this.deviceReady = false;

            var onDeviceReady = function () {
                if (!self.deviceReady) {
                    intel.xdk.device.hideSplashScreen();
                    self.deviceReady = true;
                    callback();
                    self.startAutoUpdate();
                }
            };
            document.addEventListener("intel.xdk.device.ready", onDeviceReady, false);

            /*
            var getRemoteDataEvent = function(event) {
            //console.log("event: ", event);
            console.log(["remote.data: ", event.success, event.id, event.response.length].join(", "));

            var extras = event.extras || {status:200, statusText:"fake 200"};


            //for(k in event.extras.headers) {
            //    console.log(["event.extras.headers", k, " = ", event.extras.headers[k]].join(""));
            //}
            var h = self._fHandlers[event.id];
            //console.log("getRemoteDataEvent timeout value: ", h.timeout);

            if(h && h.timeout != 2) { // null - nera timeout, 1-timeout dar neivyko, 2-timeout ivyko
            if(h.timeout == 1) h.timeout = 2; //apdorosime zemiau...

            //console.log("callback!", event.success && extras.status == 200);
            if(event.success && extras.status == 200) {
            //var response = event.response.replace("i\n", "i").replace("B\n", "B");
            // ipad/android testing dont like such symbols....  lines.split("\n") fails...
            //var response = event.response.replace(/\\n/g, '');
            h.callback(event.response);
            } else if (typeof h.errback == "function") {
            h.errback({message:["getRemoteDataExt: ", extras.status, " ", extras.statusText].join("")});
            }
            }
            //document.removeEventListener("intel.xdk.device.remote.data", getRemoteDataEvent, false);
            };
            document.addEventListener("intel.xdk.device.remote.data", getRemoteDataEvent, false);
            */

            var updateConnection = function () {
                //alert("updateConnection: ");
                //console.log("intel.xdk.device.connection: ", intel.xdk.device.connection);
                self.connection = intel.xdk.device.connection;
            };

            document.addEventListener("intel.xdk.device.connection.update", updateConnection, false);
            intel.xdk.device.updateConnection();

            /*
            //This array holds the options for the command
            var options = {timeout: 10000, maximumAge: 11000, enableHighAccuracy: true };

            //This function is called on every iteration of the watch Position command that fails
            var fail = function(){
            //alert("Geolocation failed. \nPlease enable GPS in Settings.");
            if(typeof pg.GMap == "object" && typeof pg.youAreHere == "object") {
            pg.youAreHere.setMap(null);
            }
            };

            //This function is called on every iteration of the watchPosition command that is a success
            var suc = function(p) {
            if(typeof pg.GMap == "object" && typeof pg.youAreHere == "object") {
            pg.youAreHere.setPosition(new google.maps.LatLng(p.coords.latitude, p.coords.longitude));
            pg.youAreHere.setMap(pg.GMap);
            }
            //alert("Moved To: Latitude:" + p.coords.latitude + "Longitude:" + p.coords.longitude);
            };

            //This command starts watching the geolocation
            var geolocationWatchTimer = intel.xdk.geolocation.watchPosition(suc,fail,options);
            */

            //Call the stopGeolocation function to stop the geolocation watch
            //var stopGeolocation = function(){
            //    intel.xdk.geolocation.clearWatch(geolocationWatchTimer);
            //}
        }
        else {
            callback();
            self.startAutoUpdate();
        }
    },

    downloadUrl: function (key, method, url, callback, errback, timeout) {
        console.log("downloadUrl: key: ", key, url);
        var timeoutStatus = 0;

        var xhr = new XMLHttpRequest();

        if (!('withCredentials' in xhr) && typeof XDomainRequest != 'undefined' && url.indexOf('http') >= 0) {
            xhr = new XDomainRequest(); // to be able debug in Visual Studio with Internet Explorer
            xhr.open(method, url);

            xhr.onload = function () {
                if (timeoutStatus == 1) {
                    timeoutStatus = 2; //for ignoring timeout event
                }
                //_alert('loaded: ' + url); //TMP

                callback(xhr.responseText);
            }
        }
        else {
            xhr.open(method, url, true);
            xhr.setRequestHeader("Content-Type", " text/plain; charset=UTF-8");
            xhr.setRequestHeader("Accept-Charset", "utf-8");
            xhr.setRequestHeader("Accept", "text/plain");
        }

        xhr.onreadystatechange = function () {
            //console.log("onreadystatechange", xhr.readyState, xhr.status);
            if (xhr.readyState == 4) {
                if (timeoutStatus != 2) { // null - nera timeout, 1-timeout dar neivyko, 2-timeout ivyko
                    if (timeoutStatus == 1) {
                        timeoutStatus = 2; //apdorosime zemiau...
                    }

                    if (xhr.status == 200 || (xhr.status == 0 && url.indexOf('http') != 0)) { // status = 0 when downloading from local file
                        callback(xhr.responseText);
                    }
                    else {
                        errback({ message: ["Bad response status: ", xhr.status].join(""), status: xhr.status });
                    }
                }
            }
        };

        try {
            if (timeout) { // jeigu po tam tikro laiko failas vis dar nera atsiustas, ismetame klaida "time out"
                timeoutStatus = 1; // kas pirmas? ar timeout ar atsisius failas

                window.setTimeout(function () {
                    //console.log("Timeout: ", key, self._fHandlers[key].timeout);
                   // _alert('timeout: ' + timeoutStatus);

                    if (timeoutStatus == 1) { // jeigu 2 - reiskia jau ivykdytas (gali buti ir su klaida, bet apdorotas).
                        timeoutStatus = 2; // apdorojome su errback
                        errback({ message: "File download time out: " + timeout, status: 1 });
                    };
                }, timeout);
            }
            xhr.send(null);
        }
        catch (err) {
            errback({ message: "Request send error: " + err.description, status: 3 });
        }
    },


    downloadUrlIntel: function (key, method, url, callback, errback, timeout) {
        console.log("downloadUrl: key: ", key, url, " device: ", cfg.isApp);
        var self = this;
        this._fHandlers[key] = { callback: callback, errback: errback };

        if (cfg.isApp) {
            var parameters = new intel.xdk.Device.RemoteDataParameters();
            parameters.url = url;
            parameters.id = key;
            parameters.method = method;
            parameters.addHeader("Content-Type", " text/plain; charset=UTF-8");
            parameters.addHeader("Accept-Charset", "utf-8");
            parameters.addHeader("Accept", "text/plain");

            //parameters.addHeader("Connection", "Keep-Alive"); // tik serveris..
            //parameters.addHeader("Keep-Alive", "timeout=15, max=100");

            intel.xdk.device.getRemoteDataExt(parameters);

            if (timeout) { // jeigu po tam tikro laiko failas vis dar nera atsiustas, ismetame klaida "time out"
                this._fHandlers[key].timeout = 1; // kas pirmas? ar timeout ar atsisius failas
                window.setTimeout(function () {
                    console.log("Timeout: ", key, self._fHandlers[key].timeout);
                    if (self._fHandlers[key].timeout == 1) { // jeigu 2 - reiskia jau ivykdytas (gali buti ir su klaida, bet apdorotas).
                        self._fHandlers[key].timeout = 2; // apdorojome su errback
                        errback({ message: "File download time out: " + timeout });
                    };
                }, timeout);
            }

        } else {
            // jeigu neranda version.txt - tai tegu naudoja lokalius duomenis...
            errback({ message: "Testing: app.downloadUrl: " + key });
            //this._fDownloadUrl(method, url, callback, true); // neturi errback... todel nepatogu naudotis
        }
    },

    /* works only for local data */
    fetchHeader: function (url, callback) {
        jQuery.ajax({
            cache: false,
            type: "HEAD",
            url: url,
            success: function (data, textStatus, jqXHR) {
                console.log("success", data, textStatus, jqXHR);
                callback(jqXHR.getResponseHeader('Last-Modified'));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error", textStatus, errorThrown);
            }
        });
    },

    updateVersionHTML: function (version) {
        var timeStr = this.getTimeStr(version) || (' ' + this.i18n('outOfDate'));
        jQuery("#version").html(timeStr);
    },

    loadFromLocalStorage: function (key, callback, errback) {
        var offlineDate = this.getLastSync(key);

        if (!offlineDate) {
            console.log("loadFromLocalStorage: check for localStorage data before calling");
            errback({ message: ["localStorage.", key, " not found"].join("") });
            if (key == "routes") this.updateVersionHTML(); // out of date
        } else {
            console.log("loadFromLocalStorage... ", key);
            callback(this.localStorage[key]);
            if (key == "routes") this.updateVersionHTML(offlineDate);
        }
    },

    loadNewVersion: function (key, version, data, callback) {
        //var version = date.toJSON();
        this.localStorage[key + "_lastSync"] = version;
        this.localStorage[key] = data;

        callback(data);
        if (key == "routes") this.updateVersionHTML(version);
    },

    popup: function (id, show, options) {
        if (show) {
            if (id in this._popups) {
                return; // ir taip jau rodome...
            } else {
                var cancelCallbackTemp = options.cancelCallback;
                var cancelCallback = function () {
                    delete app._popups[id];
                    if (typeof cancelCallbackTemp == "function") cancelCallbackTemp();
                };
                options.cancelCallback = cancelCallback;
                this._popups[id] = jq("body").popup(options);
            }
        } else if (id in this._popups) { // hide - remove
            this._popups[id].hide();
            delete this._popups[id];
        }
    },

    /* downloading stops/routes popup steps:1-start, 2-one file is ready, 3-error */
    downloading: function (step) {
        var self = this;

        window.setTimeout(function () {
            console.log(["downloading step: ", step, " & ", self.ready, " & ", typeof self.downloading_popup].join("")); //, typeof this.downloading_popup);

            if (step == 1) {
                if (self.ready == 0) {
                    self.popup("downloading", true, {
                        cancelClass: "hide",
                        doneClass: "hide",
                        message: [i18n.loading, '<img src="mobile/img/loading.gif">'].join("")
                    });
                }
            } else if (step == 2) {
                self.ready++;
                if (self.ready == 2) {
                    self.popup("downloading", false);
                    self.ready = 0;
                } // else !!!!!!!!!!!!!!
            } else if (step == 3) {
                self.popup("downloading", false);
                self.ready = 0;
            }
        }, 10);
    },

    /*is serverio atnaujina stops/routes duomenis localStorage, pakeicia #version texta */
    syncData: function (key, url, callback, errback) {
        var self = this;

        this.checkOnlineDate(function (response) {
            if (response.online) {
                var onlineDate = response.version;

                var offlineDate = self.getLastSync(key);
                console.log("Offline version: ", offlineDate, " online version: ", onlineDate);

                if (!onlineDate) { // neradome version.txt
                    onlineDate = self.getTime(); // vistiek bandysime gauti naujausia duomenu versija ir laikysime, kad ji yra sios dienos
                    //console.log("Fake onlineDate: ", onlineDate);
                }

                if (!offlineDate || offlineDate != onlineDate) { // nera localStorage versijos arba senesni duomenys
                    console.log("Loading online ", key, " data...");
                    self.downloading(1);

                    self.downloadUrl(key, "GET", url, function (data) {
                        self.loadNewVersion(key, onlineDate, data, callback);
                        self.downloading(2); // neivyks jei iphone nulush callback(routes.txt)
                    }, function (e) { // nepavyko gauti online stops/routes versijos - bandysime naudoti senus duomenis
                        self.downloading(3);
                        errback({ message: ["Online file ", key, " download error!"].join("") });
                    });
                }
                else { // localStorage yra pakankamai naujas
                    self.downloading(2); // neuzsidarydavo popup jeigu stops.txt buvo is localStorage
                    errback({ message: ["Data ", key, " is up to date!"].join("") });
                }
            }
            else { // try load from local storage
                self.downloading(3);
                errback({ message: 'No internet connection!' });
            }
        });
    },

    getURL: function (key) {
        if (key == "version") {
            return cfg.city.urlVersion; //[this.offline_datadir, "/version.txt"].join("");//"http://trails.lt/cache/version.txt";
        }
        return cfg.city.datadir + '/' + key + '.txt';
    },

    getLocalURL: function (key) { // lokaliai siunciamiems failams neturi buti ?timeout=blabla
        return [this.offline_datadir, "/", key, ".txt"].join("");
    },

    getTime: function () {
        var d = new Date();
        return d.getTime();
    },

    getTimeStr: function (t) {
        if (!t) return null;

        var d = new Date()
        d.setTime(t)
        return d.toJSON();
    },

    /* Download and install data from the server */
    update: function (callback, errback) {
        var self = this;

        self.syncData("stops", self.getURL("stops"), function (stops_data) {
            self.syncData("routes", self.getURL("routes"), function (routes_data) {
                callback();
            }, errback);
        }, errback);
    },

    checkForUpdates: function (silent) {
        console.log("check for updates, silent: ", silent);
        var self = this;

        this.checkOnlineDate(function (response) {
            console.log("checkOnlineDate: ", response.version);

            if (response.online) {
                var onlineDate = response.version;

                var offlineDate = self.getLastSync("routes");

                if (!onlineDate) { // negavome/neradome version.txt
                    onlineDate = self.getTime(); // laikysime, kad vistiek reikia bandyti atsinaujinti...
                }

                // turime tik failus, arba senesnius uz online
                if (!offlineDate || onlineDate != offlineDate) {

                    var popup = jq('body').popup({
                        title: "New version found!",
                        message: [self.i18n('newDataFound'), self.getTimeStr(onlineDate)].join(""),
                        cancelText: self.i18n('cancel'),
                        cancelCallback: function () { console.log("cancelled"); },
                        doneText: self.i18n('download'),
                        doneCallback: function () {
                            //console.log("doneCallback", this, popup);
                            //jQuery("#downloading").show();
                            //jQuery("#action").hide();
                            // dabar automatiskai issoks downloading...
                            popup.hide();

                            self.update(function () {
                                // success
                                //alert("before reload: " + window.location.hash);
                                window.location.reload();
                                //popup.hide();
                            }, function (err) {
                                // error
                                //popup.hide();
                                alert("Server error: " + err.message);
                            });
                        },
                        cancelOnly: false,
                        doneClass: 'btn btn-primary btn-lg',
                        cancelClass: 'btn btn-primary btn-lg',
                        onShow: function () { console.log('showing popup'); },
                        autoCloseDone: false, //default is true will close the popup when done is clicked.
                        suppressTitle: true //Do not show the title if set to true
                    });
                }
                else if (!silent) {
                    alert(self.i18n('dataUpToDate'));
                }
            }
            else if (silent) {
                console.log('call on timeout or on some event...! to check for new version');
            }
            else {
                alert(self.i18n('noInternet'));
            }
        });

    }

    //alert(fetchHeader(location.href,'Last-Modified'));
};

app.debugStop();
//app.debugStart(true);
pg.mapShowStopsNames = true;

ti._fDownloadUrl = ti.fDownloadUrl; // original

ti.fDownloadUrl = function (method, url, callback, crossdomain) {
    //console.log("app.js: ti.fDownloadUrl:", method, url, crossdomain);

    var key = url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
    var index = key.indexOf("=");
    if(key.indexOf("=") != -1) key = key.substring(index +1); // php=gps.txt

    function errback(e) {
        console.log(['Fallback to ti.fDownloadUrl("', app.getLocalURL(key), '"), key: ', key, e.message].join(""));
        app.downloadUrl(key, method, app.getLocalURL(key), callback, function(e) { console.log("Local file download error: ", e.message); });
    }

    if (key == "stops" || key == "routes") {
        app.syncData(key, app.getURL(key), callback, function(err) { // bandome gauti online duomenis
            // console.log("app.syncData error: ", err.message);
            app.loadFromLocalStorage(key, callback, errback); // jeigu negavome ar ju nereikia naujinti bandome localStorage, errback - paims senus failus
        });
    }
    else if (key == "departures2") {
        app.downloadUrl(key, method, url, callback, function(err) {
            //err.status: 0-server error; 1-timeout
            callback({status:err.status, message: app.i18n('noInternet')});
        }, 5000);
    }
    else if (key == "gps") { // gps, realtimedata
        app.downloadUrl(key, method, url, function(data) {
            app.popup("nointernet", false); // paslepiame popupa jeigu toks issoko
            app._skipInternet = false;
            callback(data);
        }, function(err) {
            //err.status: 0-server error; 1-timeout
            //alert(app._skipInternet);
            if (!app._skipInternet) {
                app.popup("nointernet", true, {
                        cancelText:"OK", // Po 'OK', nereiketu jungti popupo is naujo (pakol neatsiras rysis ir vel dings)
                        cancelCallback: function() { app._skipInternet = true; },
                        doneClass:"hide",
                        message: app.i18n('noInternet')
                    }
                );
            }
        }, 5000);
    }
    else { // maproutes
        app.downloadUrl(key, method, url, callback, errback);
    }
};


pg._fMapShow = pg.fMapShow;

pg.fMapShow = function() {
    jq("body").addClass("MapDisplayed") //tam kad pirmam puslapyje pirma neparodytu marsrutu

    if (typeof ti.stops !== 'object' || typeof ti.routes !== 'object') { // kitaip window.location.reload() ims kviesti intel komandas iki ju atsiradimo!
        setTimeout(pg.fMapShow, 200);
        return;
    }

    app.checkOnlineDate(function(response) {
        if(response.online) {
            pg._fMapShow();
        }
        else {
            //jq("body").removeClass("MapDisplayed")
            alert(app.i18n('noInternet'));
            pg.divMapHide_Click();
        }
    });
};

/* Per ilgai laukti rezultato gaunasi - norisi kelis kartus spausti...
pg._fTabShowMap_Click = pg.fTabShowMap_Click;
pg.fTabShowMap_Click = function (event, mode) {

    app.checkOnlineDate(function(response) {
        if(response.online) {
            pg._fTabShowMap_Click(event, mode);
        } else {
            alert(app.i18n('noInternet'));
        }
    });
}*/


mobile.render_footer = function() {
    var offlineDate = app.getLastSync('routes');
    var dateStr = app.getTimeStr(offlineDate);

    return [
        '<div class="row-fluid"><div class="span12 logos"><!--a href="', cfg.city.logoURL[pg.language] || cfg.city.logoURL['en'] || '', '"></a--></div></div>',
        '<div class="row-fluid"><div class="span12 footer" style="text-align:center;">',

        //<a class="btn btn-fullsite" href="' + cfg.city.standard_website + '#' + pg.language + '" alt="', i18n.openStandardWebsite, '" title="', i18n.openStandardWebsite, '">', i18n.openStandardWebsite, '</a>
        //'v<span id="version">', dateStr || (' ' + app.i18n('outOfDate')), '</span>',
        //'<br/><button class="btn btn-primary btn-lg" onclick="app.checkForUpdates()">', app.i18n('checkForUpdates'), '</button>',

        '</div></div>'
    ].join('');
};

/* timer object for app profiling */
var timer = {
    timers : {},
    logs: {},

    clear: function() {
        this.timers = {};
        this.logs = {};
    },

    start: function(key) {
        var d = new Date();
        this.timers[key] = d.getTime();
        this.logs[key] = [];
    },

    log: function(key, message) {
        var d = new Date();
        var time = d.getTime() - this.timers[key];
        this.logs[key].push({message:message, time:time});
        //return time;
    },

    get: function(key) {
        var d = new Date();
        var time = d.getTime() - this.timers[key];
        return time;
    },

    printAll: function() {
        var all = [];
        for(key in this.logs) {
            var log = this.logs[key];
            var str = []
            for(var i=0; i<log.length; i++) {
                var m = log[i];
                str.push([m.message, ": ", m.time, "ms"].join(""));
            }
            all.push(str.join("\n"));
        }
        alert(all.join("\n\n"));
    },

    print: function(key) {
        if(key in this.logs) {
            var str = []
            for(var i=0; i<this.logs[key].length; i++) {
                var log = this.logs[key][i];
                str.push([log.message, ": ", log.time, "ms"].join(""));
            }
            alert(str.join("\n"));
        } else {
            alert(["timer ", key, " not found"].join(""));
        }
    }
};
