<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль подведомственных организаций
 */
class SubOrganization extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/suborganization.html.twig');
    }

    /**
     * Текущая организация
     * @return SubOrganization
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Список для текущей страницы
     * @return array
     */
    protected function getItemsList()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->addOrderBy('n.sort');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        return $queryBuilder->getQuery()->useResultCache(true, 15)->getResult();
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdSiteBundle:Module:suborganization/list.html.twig', array('list' => $result));
    }

    /**
     * Подробная страница
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdSiteBundle:Module:suborganization/detail.html.twig', array('item' => $this->getCurrent()));
    }

    /**
     * Список на главной департамента
     */
    public function renderDepartamentIndex()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->addOrderBy('n.sort');

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 11)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $node = $temp[0];
        } else {
            return '';
        }

        $queryBuilder->setParameter('parent', $node->getId());

        $result = $queryBuilder->getQuery()
            ->setMaxResults(3)
            ->useResultCache(true, 15)
            ->getResult();

        return trim($this->twig->render('KrdSiteBundle:Module:suborganization/departament-block.html.twig', array('list' => $result, 'node' => $node)));
    }
}
