<?php

namespace Q\CoreBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;


class NodeToIntTransformer extends EntityToIntTransformer
{
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        parent::__construct($om);
        $this->setEntityClass('Q\CoreBundle\Entity\Node');
        $this->setEntityRepository("QCoreBundle:Node");
        $this->setEntityType("node");
    }
}
