<?php

namespace Q\CoreBundle\Tools;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;


/**
 * Очистка кеша роутинга
 */
class RouterCacheCleaner
{
    protected $cacheDir;
    protected $filesystem;

    public function __construct($cacheDir, Filesystem $filesystem)
    {
        $this->cacheDir = $cacheDir;
        $this->filesystem = $filesystem;
    }

    public function clear()
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('app*UrlMatcher*')
            ->depth(0)
            ->in($this->cacheDir);

        foreach($files as $file) {
            $this->filesystem->remove($file->getRealpath());
        }

        $finder = new Finder();
        $files = $finder->files()
            ->name('app*UrlGenerator*')
            ->depth(0)
            ->in($this->cacheDir);

        foreach($files as $file) {
            $this->filesystem->remove($file->getRealpath());
        }
    }
}
