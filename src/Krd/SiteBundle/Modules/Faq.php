<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль FAQ
 */
class Faq extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/faq.html.twig');
    }

    /**
     * Список faq для текущей страницы
     * @return array
     */
    protected function getItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }

    /**
     * Список faq
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdSiteBundle:Module:faq/list.html.twig', array('list' => $result));
    }
	
	/**
     * Список faq
     */
    public function renderGlavaContent()
    {
        $result = $this->getItemsList();
        return $this->twig->render('KrdGlavaBundle:Module:faq/list.html.twig', array('list' => $result));
    }

    /**
     * Небольшой блок на главной департамента
     */
    public function renderDepartamentIndex()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->addOrderBy('n.date', 'DESC')
            ->addOrderBy('n.id', 'DESC');

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 9)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $faqNode = $temp[0];
        } else {
            return '';
        }

        $queryBuilder->setParameter('parent', $faqNode->getId());

        $result = $queryBuilder->getQuery()
            ->setMaxResults(3)
            ->useResultCache(true, 15)
            ->getResult();

        return $this->twig->render('KrdSiteBundle:Module:faq/departament-block.html.twig', array('list' => $result, 'faqNode' => $faqNode));
    }
}
