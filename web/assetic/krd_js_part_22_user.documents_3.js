/**
 * Документы пользователя
 */
angular.module('user.documents', ['user.notifications', 'forms'])
    .run(['udSelection', function(udSelection) {
        $('body').on('mouseup', function() {
            udSelection.detect();
        });
    }])

    /**
     * Просто хранилище для выделенного текста
     */
    .service('udSelection', [function() {
        var selection = '';
        var stack = [];
        var that = this;

        this.set = function(value) {
            selection = value;

            if (!!selection) {
                stack.push(value);
            }
        };

        this.detect = function() {
            var t = '';

            if(window.getSelection){
                t = window.getSelection();
            }else if(document.getSelection){
                t = document.getSelection();
            }else if(document.selection){
                t = document.selection.createRange().text;
            }

            that.set(t.toString());
        };

        this.get = function() {
            return selection;
        };

        this.getLast = function() {
            if (stack.length > 0) {
                return stack[stack.length - 1];
            }
        };
    }])

    /**
     * Управление документами в личном кабинете
     */
    .directive('userDocuments', ['$http', 'notifications', function($http, notifications) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.children('.remove-selected-btn')
                    .attr('ng-click', 'removeSelected()')
                    .attr('ng-show', 'hasSelected()');

                return function($scope, $element, attrs) {
                    $scope.selectedDirectories = {};
                    $scope.selectedDocuments = {};
                    $scope.urlRemove = attrs.urlRemove;

                    $element.find('input.checkbox').on('change', function() {
                        var type = $(this).data('type');
                        var id = parseInt($(this).data('id'), 10);

                        switch(type) {
                            case 'directory':
                                $scope.selectedDirectories[id] = $(this).is(':checked');
                                $scope.$apply();
                                break;

                            case 'document':
                                $scope.selectedDocuments[id] = $(this).is(':checked');
                                $scope.$apply();
                                break;
                        }
                    });
                }
            },

            controller: ['$scope', '$element', function($scope, $element) {
                /**
                 * У нас есть хоть один выбранный?
                 */
                $scope.hasSelected = function() {
                    for (var i in $scope.selectedDirectories) {
                        if ($scope.selectedDirectories.hasOwnProperty(i)) {
                            if (!!$scope.selectedDirectories[i]) {
                                return true;
                            }
                        }
                    }

                    for (var i in $scope.selectedDocuments) {
                        if ($scope.selectedDocuments.hasOwnProperty(i)) {
                            if (!!$scope.selectedDocuments[i]) {
                                return true;
                            }
                        }
                    }

                    return false;
                };

                /**
                 * Удаление выбранных элементов
                 */
                $scope.removeSelected = function() {
                    if (!confirm('Вы уверены?')) {
                        return;
                    }

                    var directories = [];
                    var documents = [];
                    var $elementsToRemove = $();

                    for (var i in $scope.selectedDirectories) {
                        if ($scope.selectedDirectories.hasOwnProperty(i)) {
                            if (!!$scope.selectedDirectories[i]) {
                                directories.push(i);
                                $elementsToRemove = $elementsToRemove.add($element.find('*[data-directory-element="'+i+'"]'));
                            }
                        }
                    }

                    for (var i in $scope.selectedDocuments) {
                        if ($scope.selectedDocuments.hasOwnProperty(i)) {
                            if (!!$scope.selectedDocuments[i]) {
                                documents.push(i);
                                $elementsToRemove = $elementsToRemove.add($element.find('*[data-document-element="'+i+'"]'));
                            }
                        }
                    }

                    if ((directories.length == 0) && (documents.length == 0)) {
                        return;
                    }

                    $elementsToRemove.addClass('prepare-to-remove');

                    $http({
                        method: 'DELETE',
                        url: $scope.urlRemove,
                        data:{directories:directories, documents: documents}
                    }).success(function() {
                        notifications.user.info('Элементы удалены');

                        $scope.selectedDirectories = {};
                        $scope.selectedDocuments = {};
                        $elementsToRemove.remove();
                    }).error(function() {
                        notifications.user.error('Произошла ошибка');
                        $elementsToRemove.removeClass('prepare-to-remove');
                    });
                };
            }]
        }
    }])

    /**
     * Сворачиваемая папка с документами
     */
    .directive('userDocumentsItem', [function() {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element) {
                $scope.open = $element.hasClass('open');

                $scope.$watch('open', function() {
                    if ($scope.open) {
                        $element.addClass('open').removeClass('close');
                    } else {
                        $element.removeClass('open').addClass('close');
                    }
                });

                $element.find('.directory .icon').on('click', function(e) {
                    e.preventDefault();

                    $scope.open = !$scope.open;
                    $scope.$apply();
                });

                $element.find('.directory-name .real-name').on('click', function(e) {
                    e.preventDefault();

                    if ($(e.target).is('i')) {
                        return;
                    }

                    $scope.open = !$scope.open;
                    $scope.$apply();
                });
            }
        }
    }])

    /**
     * Редактирование имен папок и документов
     */
    .directive('userDocumentsEditable', ['$http', 'notifications', function($http, notifications) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                $element.find('.real-name')
                    .attr('ng-show', 'mode=="real"')
                    .children('i')
                        .attr('ng-click', 'setEditMode()');

                $element.find('.edit-name')
                    .attr('ng-show', 'mode=="edit"')
                    .attr('ng-enter', 'saveTitle()')
                    .attr('ng-esc', 'discard()')
                    .children('i')
                        .attr('ng-click', 'saveTitle()');

                return function($scope, $element, attrs) {
                    $scope.mode = 'real';
                    $scope.title = attrs.title;
                    $scope.url = attrs.url;

                    $element.find('.real-name a').off('click');
                }
            },

            controller: ['$scope', '$element', function($scope, $element) {
                /**
                 * Количество документов
                 */
                $scope.count = function() {
                    return $element.next('.documents-list').find('.document').length;
                };

                /**
                 * Включить режим редактирования
                 */
                $scope.setEditMode = function() {
                    $scope.title_edit = $scope.title;
                    $scope.mode = 'edit';

                    setTimeout(function() {
                        $element.find('.edit-name input, .edit-name textarea').focus();
                    });
                };

                /**
                 * Сохранить изменение
                 */
                $scope.saveTitle = function() {
                    $scope.title = $scope.title_edit;
                    $scope.mode = 'real';

                    $http.post($scope.url, {title:$scope.title})
                        .success(function(response) {
                            if (response.title) {
                                $scope.title = response.title;
                            } else {
                                notifications.user.error(response.message || 'Произошла ошибка');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.message || 'Произошла ошибка');
                        });
                };

                /**
                 * Отменить редактирование
                 */
                $scope.discard = function() {
                    $scope.mode = 'real';
                };
            }]
        }
    }])

    /**
     * Инициализация tinyMCE
     */
    .directive('tinymce', ['$timeout', function($timeout) {
        var generatedIds = 0;

        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                attrs.$set('id', 'tiny-mce-' + generatedIds++);

                $timeout(function() {
                    var options = angular.extend(window.tinyMCEConfig, {
                    });

                    initTinyMCE(options, $element);
                });
            }
        };
    }])

    /**
     * Контроллер формы редактирования документа
     */
    .controller('UserDocumentDetailCtrl', ['$scope', 'notifications', function($scope, notifications) {
        $scope.onResponse = function(response) {
            notifications.user.info(response.msg || 'Изменения сохранены');
        };
    }])

    /**
     * Панель добавления документов
     */
    .directive('userDocumentsAdd', ['notifications', 'udSelection', '$http', 'router', function(notifications, udSelection, $http, router) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element) {
                $scope.text = udSelection.getLast();
                $scope.document = {};
                $scope.directory = {};
                $scope.loading = false;

                if (!$scope.text) {
                    $element.html('Вы ничего не выбрали');
                }
            },

            controller: ['$scope', function($scope) {
                /**
                 * Сохраняем
                 */
                $scope.saveAll = function() {
                    if (!$scope.document) {
                        notifications.user.error('Нужно выбрать документ');
                        return;
                    }

                    var url = router.generate('krd_user_documents_document_edit', {':id':$scope.document.id});
                    var content = ($scope.document.content || '') + '<p>' + $scope.text + '</p>';

                    $scope.loading = true;

                    $http.post(url, {content:content})
                        .success(function(response) {
                            $scope.loading = false;
                            $scope.$emit('dialog-hide');
                            notifications.user.info('Текст успешно добавлен в документ');
                        })
                        .error(function() {
                            notifications.user.error('Произошла ошибка при сохранении');
                            $scope.loading = false;
                        });
                };
            }]
        }
    }])

    /**
     * Дерево документов
     */
    .directive('userDocumentsTree', ['router', 'notifications', '$http',function(router, notifications, $http) {
        return {
            restrict: 'A',
            scope: true,
            template: '<div class="wrap">'
                        + '<div class="text-block" ng-if="!directories || directories.length == 0">Нет ни одной папки. <a href="" ng-click="newDirectory()">Создать папку?</a></div>'
                        + '<div class="directory" ng-repeat="directory in directories" ng-class="{open:directory.$open, selected:directory.$selected}">'
                            + '<i class="icon-lk-document-tree-directory" ng-click="openToggle(directory)" ng-class="{open:directory.$open, editmode:directory.$editmode}"></i>'
                            + '<div class="real-name" ng-click="selectDirectory(directory)" ng-if="!directory.$editmode">{{ directory.title }}</div>'
                            + '<div class="edit-name" ng-if="directory.$editmode">'
                                + '<input type="text" ng-model="directory.title" ng-enter="submitEditDirectory(directory)" ng-esc="removeDirectory(directory)" ng-blur="submitEditDirectory(directory)" maxlength="80">'
                            + '</div>'

                            + '<div class="documents" ng-show="directory.$open">'
                                + '<div class="text-block" ng-if="!directory.documents || directory.documents.length == 0">Папка не содержит документов. <a href="javascript:void()" ng-click="newDocument(directory)">Создать?</a></div>'
                                + '<div class="document" ng-repeat="document in directory.documents" ng-class="{selected:document.$selected && !document.$editmode}" ng-click="selectDocument(document)">'
                                    + '<i class="icon-lk-document-tree-document" ng-class="{selected:document.$selected && !document.$editmode, editmode:document.$editmode}"></i>'
                                    + '<div class="real-name" ng-if="!document.$editmode">{{ document.title }}</div>'
                                    + '<div class="edit-name" ng-if="document.$editmode">'
                                        + '<input type="text" ng-model="document.title" ng-enter="submitEditDocument(document, directory)" ng-esc="removeDocument(document, directory)" ng-blur="submitEditDocument(document, directory)" maxlength="80">'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            link: function($scope, $element) {
                $scope.directories = [];
                $scope.update();
            },

            controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
                /**
                 * Устанавливает модель документа
                 */
                $scope.setDocumentModel = function(document) {
                    $scope.$parent[$attrs.modelDocument] = document;
                };

                /**
                 * Устанавливает модель директории
                 */
                $scope.setDirectoryModel = function(directory) {
                    $scope.$parent[$attrs.modelDirectory] = directory;
                };

                /**
                 * Обновление списка
                 */
                $scope.update = function() {
                    $http.get(router.generate('krd_user_documents_list'))
                        .success(function(response) {
                            $scope.directories = response;
                        })
                        .error(function() {
                            notifications.user.error('Не удалось загрузить список документов');
                        });
                };

                /**
                 * Раскрытие папки
                 */
                $scope.openToggle = function(directory) {
                    directory.$open = !directory.$open;
                };

                /**
                 * Выбор документа
                 */
                $scope.selectDocument = function(document) {
                    $scope.deselectAll();

                    document.$selected = true;
                    $scope.setDocumentModel(document);

                    for (var i = 0, l = $scope.directories.length; i < l; i++) {
                        var directory = $scope.directories[i];

                        if (!!directory.documents && directory.documents.indexOf(document) !== -1) {
                            directory.$selected = true;
                            $scope.setDirectoryModel(directory);
                            break;
                        }
                    }
                };

                /**
                 * Выбор директории
                 */
                $scope.selectDirectory = function(directory) {
                    $scope.deselectAll();

                    directory.$selected = true;
                    directory.$open = true;
                    $scope.setDirectoryModel(directory);
                };

                /**
                 * Снимаем все выделения
                 */
                $scope.deselectAll = function() {
                    $scope.setDirectoryModel(null);
                    $scope.setDocumentModel(null);

                    for (var i = 0, l = $scope.directories.length; i < l; i++) {
                        var directory = $scope.directories[i];
                        directory.$selected = false;

                        if (!!directory.documents) {
                            for (var ii = 0, ll = directory.documents.length; ii < ll; ii++) {
                                directory.documents[ii].$selected = false;
                            }
                        }
                    }
                };

                /**
                 * Новая папка
                 */
                $scope.newDirectory = function() {
                    $scope.deselectAll();

                    $scope.directories.push({
                        title: 'Новая папка',
                        documents: [],
                        $editmode: true
                    });

                    setTimeout(function() {
                        $element.find('input:visible').focus();
                    }, 13);
                };

                /**
                 * Удаление директории
                 */
                $scope.removeDirectory = function(directory) {
                    var index = $scope.directories.indexOf(directory);

                    if (index !== -1) {
                        $scope.directories.splice(index, index);
                    }
                };

                /**
                 * Подтверждение редактирования директории
                 */
                $scope.submitEditDirectory = function(directory) {
                    if (directory.$saveInProgress) {
                        return;
                    }

                    directory.$editmode = false;
                    directory.$saveInProgress = true;
                    $scope.selectDirectory(directory);

                    $http.post(router.generate('krd_user_documents_directory_create'), {title:directory.title})
                        .success(function(response) {
                            directory.id = response.id;
                            directory.title = response.title;
                            directory.$saveInProgress = false;
                        })
                        .error(function() {
                            notifications.user.error('Произошла ошибка');
                            $scope.removeDirectory(directory);
                        });
                };

                /**
                 * Новый документ
                 */
                $scope.newDocument = function(directory) {
                    $scope.deselectAll();

                    directory.documents.push({
                        title: 'Новый документ',
                        $editmode: true
                    });

                    setTimeout(function() {
                        $element.find('input:visible').focus();
                    }, 13);
                };

                /**
                 * Удаление документа
                 */
                $scope.removeDocument = function(document, directory) {
                    var index = directory.documents.indexOf(document);

                    if (index !== -1) {
                        directory.documents.splice(index, index);
                    }
                };

                /**
                 * Подтверждение редактирования документа
                 */
                $scope.submitEditDocument = function(document, directory) {
                    if (document.$saveInProgress) {
                        return;
                    }

                    document.$editmode = false;
                    document.$saveInProgress = true;
                    $scope.selectDocument(document);

                    $http.post(router.generate('krd_user_documents_document_create'), {title:document.title, directory:directory.id})
                        .success(function(response) {
                            document.id = response.id;
                            document.title = response.title;
                            document.$saveInProgress = false;
                        })
                        .error(function() {
                            notifications.user.error('Произошла ошибка');
                            $scope.removeDocument(document, directory);
                        });
                };
            }]
        }
    }])

    /**
     * Панель управления деревом документов
     */
    .directive('userDocumentsTreeControl', [function() {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                $scope.$treeScope = $('*[user-documents-tree="'+attrs.userDocumentsTreeControl+'"]').scope();
            },

            controller: ['$scope', function($scope) {
                $scope.newDirectory = function() {
                    $scope.$treeScope.newDirectory();
                };

                $scope.newDocument = function(directory) {
                    $scope.$treeScope.newDocument(directory);
                };
            }]
        }
    }])
;
