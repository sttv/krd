<?php

namespace Q\CoreBundle\Router\Breadcrumb;

use Symfony\Bridge\Monolog\Logger;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Entity\Node;


/**
 * Менеджер хлебных крошек
 */
class Manager
{
    /**
     * Элементы
     */
    protected $items = array();

    /**
     * @var CurrentNode
     */
    protected $currentNode;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Автоматически заполняем список родительской веткой
     * @param CurrentNode $currentNode
     */
    public function __construct(CurrentNode $currentNode, Logger $logger)
    {
        $this->currentNode = $currentNode;
        $this->logger = $logger;

        if ($currentNode->getNode() instanceof Node && $currentNode->getNode()->getId() != 1) {
            $nodes = $currentNode->getNodeParentBranch();

            foreach($nodes as $node) {
                $this->addItemArray(array('title' => $node->getFrontendTitle(), 'url' => $node->getUrl(true)));
            }
        }
    }

    /**
     * Построение навигационной цепочки для нода
     * Толкьо возвращает массив, на глобальную цепочку не влияет
     *
     * @param  Node   $node исходный нод
     * @param boolean[optional] $include Включать сам нод в цепочку?
     * @param boolean[optional] $noIndex Не включать главную страницу в цепочку?
     *
     * @return array
     */
    public function buildForNode(Node $node, $include = false, $noIndex = false)
    {
        $nodeList = $this->currentNode->getNodeParentBranch($node);

        if ($noIndex) {
            array_shift($nodeList);
        }

        if (!$include) {
            array_pop($nodeList);
        }

        $list = array();

        foreach ($nodeList as $node) {
            $item = new Item();

            $item->setTitle($node->getFrontendTitle());
            $item->setUrl($node->getUrl(true));

            $list[] = $item;
        }

        return $list;
    }

    /**
     * Возвращает тайтл последнего элемента
     * @return string
     */
    public function getTitle()
    {
        if (!empty($this->items)) {
            return $this->items[count($this->items) - 1]->getTitle();
        } else {
            return '';
        }
    }

    /**
     * URL предпоследнего пункта (уровень выше текущей страницы)
     * @return string
     */
    public function getBackUrl()
    {
        if (!empty($this->items) && count($this->items) > 1) {
            return $this->items[count($this->items) - 2]->getUrl();
        } else {
            return '';
        }
    }

    /**
     * Проверка крошек на пустоту
     * @return boolean
     */
    public function hasItems()
    {
        return !empty($this->items);
    }

    /**
     * Список элементов
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Достает последний элемент, убирая его из хлебных крошек
     * @return Item
     */
    public function popItem()
    {
        $item = array_pop($this->items);

        if (count($this->items) > 0) {
            $this->items[count($this->items) - 1]->setLast(true);
        }

        return $item;
    }

    /**
     * Очистка хлебных крошек
     */
    public function clear()
    {
        $this->items = array();
    }

    /**
     * Добавление элемента в виде массива
     * @param array $itemArr
     */
    public function addItemArray(array $itemArr)
    {
        $item = new Item();

        $item->setTitle($itemArr['title']);
        $item->setUrl($itemArr['url']);

        $this->addItem($item);
    }

    /**
     * Добавление элемента
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        if (count($this->items) > 0) {
            $this->items[count($this->items) - 1]->setLast(false);
        }

        $item->setLast(true);
        $this->items[] = $item;
    }

    /**
     * Проверяет два последних элемента на идентичность и удаляет предпоследний в случае дубля
     */
    public function removeLastDuplicates()
    {
        if (count($this->items) < 2) {
            return;
        }

        if ($this->items[count($this->items) - 1]->isEqual($this->items[count($this->items) - 2])) {
            unset($this->items[count($this->items) - 2]);
            $this->items = array_values($this->items);
        }
    }
}
