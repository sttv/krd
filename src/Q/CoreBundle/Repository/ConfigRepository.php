<?php

namespace Q\CoreBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\CoreBundle\Entity\Config;


/**
 * Репозиторий для настроек сайта
 */
class ConfigRepository extends EntityRepository
{
    /**
     * Возвращает конфиг сайта
     * Если конфиг не создан, то создает дефолтный
     * @return Config
     */
    public function getConfig()
    {
        $config = $this->find(1);

        if (!$config) {
            $config = new Config();

            $config->setTitle('Simple web project');
            $config->setEmail('example@project.ru');
            $config->addAlertEmail('notify@project.ru');

            $this->_em->persist($config);
            $this->_em->flush($config);
        }

        return $config;
    }
}
