<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Главная страница департамента
 */
class DepartamentController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {

    }
}
