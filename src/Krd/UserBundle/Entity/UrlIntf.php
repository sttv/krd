<?php

namespace Krd\UserBundle\Entity;


/**
 * Интерфейс для авто-генерации ссылок на подробную страницу
 */
interface UrlIntf
{
    /**
     * Имя роута детального просмотра
     */
    public function getDetailRoute();

    /**
     * Уникальное имя элемента
     */
    public function getName();

    /**
     * ID контроллера страницы для генерации ссылки
     */
    public function getControllerId();

    /**
     * Установка URL
     */
    public function setUrl($url);

    /**
     * Получение URL
     */
    public function getUrl();
}
