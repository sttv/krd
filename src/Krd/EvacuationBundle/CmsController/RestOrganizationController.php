<?php

namespace Krd\EvacuationBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер организация для эвакуации автомобилей
 *
 * @Route("/rest/krd/evacuation/organization")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestOrganizationController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdEvacuationBundle:Organization';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_evacuation_organization_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_evacuation_organization_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_evacuation_organization", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_evacuation_organization_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_evacuation_organization_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_evacuation_organization_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_evacuation_organization_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_evacuation_organization_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }
}
