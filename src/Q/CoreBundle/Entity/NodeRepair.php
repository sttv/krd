<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Нод для восстановления структуры
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @ORM\Table(name="node_repair",
 *      indexes={
 *          @ORM\Index(name="root", columns={"root"}),
 *          @ORM\Index(name="leftkey", columns={"`left`"}),
 *          @ORM\Index(name="rightkey", columns={"`right`"}),
 *          @ORM\Index(name="lvl", columns={"level"}),
 *          @ORM\Index(name="l_r", columns={"`left`", "`right`"}),
 *          @ORM\Index(name="oldid", columns={"oldid"}),
 *      }
 * )
 *
 * @JMS\ExclusionPolicy("all")
 */
class NodeRepair
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $oldid;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="`left`", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="`right`", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="NodeRepair", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="NodeRepair", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    private $children;


    public function getId()
    {
        return $this->id;
    }

    public function getOldId()
    {
        return $this->oldid;
    }

    public function setOldId($id)
    {
        $this->oldid = $id;
    }

    public function getLeft()
    {
        return $this->left;
    }

    public function getRight()
    {
        return $this->right;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setParent(NodeRepair $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function addChild(NodeRepair $node)
    {
        $node->setParent($this);
    }

    public function getChildren()
    {
        return $this->children ?: $this->children = new ArrayCollection();
    }
}
