/**
 * Version: 1.0 Alpha-1
 * Build Date: 13-Nov-2007
 * Copyright (c) 2006-2007, Coolite Inc. (http://www.coolite.com/). All rights reserved.
 * License: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/.
 * Website: http://www.datejs.com/ or http://www.coolite.com/datejs/
 */
Date.CultureInfo = {
        /* Culture Name */
    name: "ru-RU",
    englishName: "Russian (Russia)",
    nativeName: "русский (Россия)",

    /* Day Name Strings */
    dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
    abbreviatedDayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    shortestDayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    firstLetterDayNames: ["В", "П", "В", "С", "Ч", "П", "С"],

    /* Month Name Strings */
    monthNames: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноябрь", "декабря"],
    abbreviatedMonthNames: ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"],

        /* AM/PM Designators */
    amDesignator: "",
    pmDesignator: "",

    firstDayOfWeek: 1,
    twoDigitYearMax: 2029,

    /**
     * The dateElementOrder is based on the order of the
     * format specifiers in the formatPatterns.DatePattern.
     *
     * Example:
     <pre>
     shortDatePattern    dateElementOrder
     ------------------  ----------------
     "M/d/yyyy"          "mdy"
     "dd/MM/yyyy"        "dmy"
     "yyyy-MM-dd"        "ymd"
     </pre>
     * The correct dateElementOrder is required by the parser to
     * determine the expected order of the date elements in the
     * string being parsed.
     *
     * NOTE: It is VERY important this value be correct for each Culture.
     */
    dateElementOrder: "dmy",

    /* Standard date and time format patterns */
    formatPatterns: {
        shortDate: "dd.MM.yyyy",
        longDate: "d MMMM yyyy г.",
        shortTime: "H:mm",
        longTime: "H:mm:ss",

                // new
        shortDateTime: "dd.MM.yyyy, H:mm",
                // /new

        fullDateTime: "d MMMM yyyy г., H:mm:ss",
        sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
        universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
        rfc1123: "ddd, dd MMM yyyy, HH:mm:ss GMT",
        monthDay: "d/M",
        yearMonth: "MMMM yyyy 'г.'"
    },

    /**
     * NOTE: If a string format is not parsing correctly, but
     * you would expect it parse, the problem likely lies below.
     *
     * The following regex patterns control most of the string matching
     * within the parser.
     *
     * The Month name and Day name patterns were automatically generated
     * and in general should be (mostly) correct.
     *
     * Beyond the month and day name patterns are natural language strings.
     * Example: "next", "today", "months"
     *
     * These natural language string may NOT be correct for this culture.
     * If they are not correct, please translate and edit this file
     * providing the correct regular expression pattern.
     *
     * If you modify this file, please post your revised CultureInfo file
     * to the Datejs Discussions located at
     *     http://groups.google.com/group/date-js
     *
     * Please mark the subject with [CultureInfo]. Example:
     *    Subject: [CultureInfo] Translated "da-DK" Danish(Denmark)
     *
     * We will add the modified patterns to the master source files.
     *
     * As well, please review the list of "Future Strings" section below.
     */
    regexPatterns: {
        jan: /^янв(арь)?/i,
        feb: /^фев(раль)?/i,
        mar: /^мар(т)?/i,
        apr: /^апр(ель)?/i,
        may: /^май/i,
        jun: /^июн(ь)?/i,
        jul: /^июл(ь)?/i,
        aug: /^авг(уст)?/i,
        sep: /^сен(тябрь)?/i,
        oct: /^окт(ябрь)?/i,
        nov: /^ноя(брь)?/i,
        dec: /^дек(абрь)?/i,

        sun: /^воскресенье/i,
        mon: /^понедельник/i,
        tue: /^вторник/i,
        wed: /^среда/i,
        thu: /^четверг/i,
        fri: /^пятница/i,
        sat: /^суббота/i,

        future: /^след|завтра/i,
        past: /^пред|вчера/i,
        add: /^(\+|через|после|вперед|и|следую?щ(ая|ий|ее)?)/i,
        subtract: /^(\-|за|до|поза|пе?ред((ыдущ|шев?ствующ)(ая|ий|ее))|назад)/i,

        yesterday: /^вчера/i,
        today: /^сегодня/i,
        tomorrow: /^завтра/i,
        now: /^сейчас|сечас|щас/i,

        millisecond: /^мс|мили(секунд)?s?/i,
        second: /^с(ек(унд)?)?/i,
        minute: /^м(ин(ут)?)?/i,
        hour: /^ч((ас)?ов)?/i,
        week: /^н(ед(ель)?)?/i,
        month: /^мес(яцев)?/i,
        day: /^д(ень|ней|ня)?/i,
        year: /^г(ода?)?|л(ет)?/i,

        shortMeridian: /^(a|p)/i,
        longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i,
        timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i,
        ordinalSuffix: /^\s*(st|nd|rd|th)/i,
        timeContext: /^\s*(\:|д|а)/i
    },

    abbreviatedTimeZoneStandard: { GMT: "-000", EST: "-0400", CST: "-0500", MST: "-0600", PST: "-0700" },
    abbreviatedTimeZoneDST: { GMT: "-000", EDT: "-0500", CDT: "-0600", MDT: "-0700", PDT: "-0800" }

};
