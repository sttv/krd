<?php

namespace Krd\EvacuationBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Krd\SiteBundle\Entity\EvacuatedAvto;


/**
 * Репозиторий Avto
 */
class AvtoRepository extends EntityRepository
{
    /**
     * Список авто в виджете на главной
     * @return array
     */
    public function getWidgetList()
    {
        return $this->createQuery("SELECT e FROM __CLASS__ e WHERE e.active = 1 AND e.toindex = 1 ORDER BY e.date DESC")
            ->setMaxResults(4)
            ->useResultCache(true, 15)
            ->getResult();
    }

    /**
     * Время последнего обновления списка
     * @return DateTime
     */
    public function getLastDate()
    {
        $dates = $this->createQuery("SELECT e.date FROM __CLASS__ e WHERE e.active = 1 ORDER BY e.date DESC")
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($dates[0])) {
            return $dates[0]['date'];
        } else {
            return new \DateTime();
        }
    }
}
