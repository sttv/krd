<?php

namespace Krd\UserBundle\Modules;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormBuilder;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Admin\AbstractModule;
use Q\UserBundle\Entity\User;
use Krd\SupportBundle\Entity\Message;


/**
 * Модуль техподдержки личного кабинета
 */
class SupportModule extends AbstractModule
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Router
     */
    protected $router;

    public function setSecurityContext(SecurityContext $sc)
    {
        $this->securityContext = $sc;
    }

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->securityContext) {
            if (($user = $this->securityContext->getToken()->getUser()) && ($user instanceof User)) {
                return $user;
            }
        }
    }

    /**
     * Список комментариев пользователя
     * @return Paginator
     */
    public function getMyMessages($count = 10)
    {
        if ($user = $this->getUser()) {
            if (!$this->securityContext->isGranted('ROLE_CMS')) {
                $query = $this->getRepository()->createQueryBuilder('e')
                    ->andWhere('e.createdBy = :user')
                    ->andWhere('e.level = 0')
                    ->addOrderBy('e.created', 'DESC')
                    ->setParameter('user', $user->getId())
                    ->getQuery();
            } else {
                $query = $this->getRepository()->createQueryBuilder('e')
                    ->andWhere('e.level = 0')
                    ->addOrderBy('e.created', 'DESC')
                    ->getQuery();
            }

            return Paginator::createFromRequest($this->request, $query, true, $count);
        } else {
            throw new \Exception('Not authorized');
        }
    }

    /**
     * Текущая ветка
     */
    public function getCurrent()
    {
        if ($user = $this->getUser()) {
            $filter = array();
            if (!$this->securityContext->isGranted('ROLE_CMS')) {
                $filter['createdBy'] = $this->getUser()->getId();
            }
            $filter['id'] = (int)$this->request->attributes->get('name');
            $filter['level'] = 0;

            return $this->getRepository()->findOneBy($filter);
        }
    }

    /**
     * Форма создания сообщения
     * @param  Message $message
     * @return FormBuilder
     */
    public function getCreateFormBuilder(Message $message = null, $options = array())
    {
        $fb = $this->formFactory->createNamedBuilder('message', 'support_message_create', $message, $options);
        $fb->setAction($this->router->generate('krd_support_io_create'));

        return $fb;
    }

    /**
     * Форма ответа на сообщение
     * @param  Message $message
     * @return FormBuilder
     */
    public function getReplyFormBuilder(Message $message = null, $options = array())
    {
        $fb = $this->formFactory->createNamedBuilder('message', 'support_message_reply', $message, $options);
        $fb->setAction($this->router->generate('krd_support_io_reply'));

        return $fb;
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    public function renderContent()
    {
        $form = $this->getCreateFormBuilder()->getForm();

        return $this->twig->render('KrdUserBundle:Module:support/index.html.twig', array('form_create' => $form->createView()));
    }
}
