<?php

namespace Krd\ImportBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * Банд, отвкчающий за импорт со сторонних источников,
 * за исключением городского репортера
 */
class KrdImportBundle extends Bundle
{
}
