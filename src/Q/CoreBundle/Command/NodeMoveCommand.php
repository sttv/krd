<?php

namespace Q\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Перенос разделов
 */
class NodeMoveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('q:core:node:move')
            ->setDescription('Move node and set it the last child of another node')
            ->addArgument('id', InputArgument::REQUIRED, 'What node we will move?')
            ->addArgument('parent_id', InputArgument::REQUIRED, 'To this node we will move');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('QCoreBundle:Node');

        $node = $repository->find($input->getArgument('id'));
        $node2 = $repository->find($input->getArgument('parent_id'));

        if (!$node) {
            $output->writeln('<error>Node not found</error>');
            return;
        }

        if (!$node2) {
            $output->writeln('<error>Another node not found</error>');
            return;
        }

        $em->getConnection()->beginTransaction();

        $repository->moveNode($node, $node2, 'inner');
        $output->writeln('Node moved. Need to update url...');

        $repository->updateUrl($node);
        $output->writeln('<info>Success!</info>');

        $em->getConnection()->commit();
    }
}
