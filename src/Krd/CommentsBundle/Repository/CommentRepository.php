<?php

namespace Krd\CommentsBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Symfony\Component\Security\Core\SecurityContext;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

use Q\UserBundle\Entity\User;


/**
* Репозиторий Comment
*/
class CommentRepository extends NestedTreeRepository
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var User
     */
    protected $user;

    /**
     * @JMSDI\InjectParams({
     *     "securityContext" = @JMSDI\Inject("security.context")
     * })
     */
    public function setUserBySecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    /**
     * Поиск одного элемента по ID
     * @param  integer $id
     * @return mixed
     */
    public function findOne($id)
    {
        return $this->findOneById($id);
    }

    /**
     * Список полей со связью ManyToMany
     * @return array
     */
    public function getManyToManyFields()
    {
        $result = array();

        foreach($this->_class->getAssociationNames() as $field) {
            $mapping  = $this->_class->getAssociationMapping($field);
            if ($mapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                $result[] = $field;
            }
        }

        return $result;
    }

    /**
     * Список комментариев на странице
     * @param  string $page Страница комментариев
     * @return array<integer, array<Comment>>
     */
    public function getPageList($page)
    {
        $pageHash = md5($page);

        if ($this->securityContext && $this->securityContext->isGranted('ROLE_COMMENT_ADMIN')) {
            $activeWhere = '';
        } else {
            $activeWhere = 'AND c.active = 1';

            if ($this->user instanceof User) {
                $activeWhere .= ' OR c.createdBy = '.$this->user->getId();
            }
        }

        $comments = $this->_em
            ->createQuery("SELECT c
                          FROM KrdCommentsBundle:Comment c
                          WHERE c.pageHash = :pageHash {$activeWhere}
                          ORDER BY c.created")
            ->setParameter('pageHash', $pageHash)
            ->getResult();

        $count = count($comments);

        $comments = array_filter($comments, function($item) {
            return $item->getLevel() == 0;
        });

        return array('count' => $count, 'comments' => $comments);
    }
	
	public function getCount($page)
	{
		$pageHash = md5($page);
		 $count = $this->_em
            ->createQuery("SELECT COUNT(c)
                          FROM KrdCommentsBundle:Comment c
                          WHERE c.pageHash = :pageHash AND c.active = 1
                          ORDER BY c.created")
            ->setParameter('pageHash', $pageHash)
			->useResultCache(true, 15)
            ->getSingleScalarResult();

        return $count;
	}
}
