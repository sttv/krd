<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Интерактивная 3D-модель микрорайона Московский в Краснодаре</title>
        <meta name="description" content='Интерактивная 3D-модель микрорайона "Московский" в Краснодаре показывает существующую застройку жилого района и планируемые новостройки'>
        <meta name="keywords" content="новостройки Краснодара, ЖК Московский, 3D модель жилого комплекса, застройка микрорайона, план застройки района">
		<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
        <script type="text/javascript" charset="utf-8" src="./style/js/jquery_1.8.3.min.js"></script>
        <script type="text/javascript" src="./style/js/jquery.fancybox.pack.js?v=2.0.5"></script>
        <link rel="stylesheet" type="text/css" href="./style/css/jquery.fancybox.css?v=2.0.5" media="screen" />
        <link rel="stylesheet" type="text/css" href="./style/css/main.css" media="screen" />
		<script type="text/javascript">
		<!--
		var unityObjectUrl = "http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js";
		if (document.location.protocol == 'https:')
			unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
		document.write('<script type="text\/javascript" src="' + unityObjectUrl + '"><\/script>');
		-->
		</script><script type="text/javascript" src="http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js"></script>
		<script type="text/javascript">
		<!--
			var get_objects_path = "http://krd.ru/3d/admin/get_objects.php";

			function ShowHelp()
			{
				//window.open("http://spider.ru", '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
                $('#unity-help').show();
                $('#unity-help').scrollView();
                //$.fancybox( {'maxWidth': 1000, 'content': $('#contentHelp').html()} );
			}

			function OnPlayerLoad()
			{
			    u.getUnity().SendMessage("SceneManager", "StartHttpRequest", get_objects_path);
			}

			var config = {
				width: 1000, 
				height: 625,
				params: { enableDebugging:"0" }
				
			};

			var params = {
	            backgroundcolor: "FFFFFF",
	            bordercolor: "FFFFFF",
	            textcolor: "FFFFFF",
	            logoimage: "bg-load.png",
	            progressbarimage: "brogressBar.png",
	            disableContextMenu: true
	        };

        	var u = new UnityObject2({ params: params });

			jQuery(function() {
            
                $.fn.scrollView = function () {
                    return this.each(function () {
                        $('html, body').animate({
                            scrollTop: $(this).offset().top
                        }, 500);
                    });
                }
                $('#closeHelp').click(function(){
                    $('#unity-help').hide();
                });
				var $missingScreen = jQuery("#unityPlayer").find(".missing");
				var $brokenScreen = jQuery("#unityPlayer").find(".broken");
				$missingScreen.hide();
				$brokenScreen.hide();
				
				u.observeProgress(function (progress) {
					switch(progress.pluginStatus) {
						case "broken":
							$brokenScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$brokenScreen.show();
						break;
						case "missing":
							$missingScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$missingScreen.show();
						break;
						case "installed":
							$missingScreen.remove();
						break;
						case "first":
						break;
					}
				});
				u.initPlugin(jQuery("#unityPlayer")[0], "krd.unity3d");
			});
		-->
		</script>
	</head>
	<body>
		<div class="content">
			<div style="border: 1px solid; z-index: -1;" id="unityPlayer"><embed src="krd.unity3d" type="application/vnd.unity" width="100%" height="100%" firstframecallback="UnityObject2.instances[0].firstFrameCallback();" backgroundcolor="FFFFFF" bordercolor="FFFFFF" textcolor="FFFFFF" logoimage="bg-load.png" progressbarimage="brogressBar.png" disablecontextmenu="true" style="display: block; width: 100%; height: 100%; z-index: -1;"></div>
            <div class="unity_help">
                <h5>Интерактивный сервис визуализации данных о микрорайоне «Московский» муниципального образования город Краснодар</h5>
                <p>Сервис включает информацию о существующей и перспективной застройке жилого района и предназначен для получения информации о его развитии.</p>
                <p>Чтобы получить доступ к модели, требуется установить бесплатное дополнение к веб-браузеру — Unity3D player.</p>
                <span id="unity_download_button"><a href="https://unity3d.com/ru/webplayer">Скачать Unity3D player</a></span>
            </div>
            <div class="unity_help" id="unity-help" style="display: none;">
                <?php include_once('./admin/help.php');?>
            </div>
            <div class="unity_help">
				<h5>Экспликация объектов, представленных на интерактивной 3D-модели жилого микрорайона «Московский»</h5>
				<div class="houses_list">
                <?php include_once('./admin/list_objects.php');?>
                </div>
            </div>
		</div>
    </body>
</html>
