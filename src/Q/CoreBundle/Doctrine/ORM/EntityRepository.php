<?php

namespace Q\CoreBundle\Doctrine\ORM;

use Doctrine\ORM\EntityRepository as BaseRepository;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/**
 * Стандартный репозиторий сущностей
 */
class EntityRepository extends BaseRepository
{
    /**
     * Поиск одного элемента по ID
     * @param  integer $id
     * @return mixed
     */
    public function findOne($id)
    {
        return $this->findOneById($id);
    }

    /**
     * Поиск указанной активной записи по имени и родителю
     * @param  string $name
     * @param  integer $parentId
     * @return array
     */
    public function findOneActiveByNameAndParent($name, $parentId)
    {
        $items = $this->createQuery("SELECT g FROM __CLASS__ g WHERE g.active = 1 AND g.name = :name AND g.parent = :parent")
            ->setMaxResults(1)
            ->setParameter('name', $name)
            ->setParameter('parent', $parentId)
            ->useResultCache(true, 15)
            ->getResult();

        if (!empty($items) && isset($items[0])) {
            return $items[0];
        }
    }

    /**
     * Создание запроса. В качестве текущей сущности следует указывать __CLASS__
     * @param  string $sql Запрос
     * @return Query
     */
    public function createQuery($sql)
    {
        return $this->_em->createQuery(str_replace('__CLASS__', $this->_entityName, $sql));
    }

    /**
     * Создание новой, пустой сущности
     * @return mixed
     */
    public function createNew()
    {
        return new $this->_class->rootEntityName();
    }

    /**
     * Список полей со связью ManyToMany
     * @return array
     */
    public function getManyToManyFields()
    {
        $result = array();

        foreach($this->_class->getAssociationNames() as $field) {
            $mapping  = $this->_class->getAssociationMapping($field);
            if ($mapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                $result[] = $field;
            }
        }

        return $result;
    }

    /**
     * Возвращает следующий индекс сортировки
     * @param  integer[optional] $parent
     * @return integer
     */
    public function getNextSort($parent = null)
    {
        if ($parent) {
            $res = $this->createQuery("SELECT MAX(e.sort) as max_sort FROM __CLASS__ e WHERE e.parent = :parent")->setParameter('parent', (int)$parent)->getResult();
        } else {
            $res = $this->createQuery("SELECT MAX(e.sort) as max_sort FROM __CLASS__ e")->getResult();
        }

        if (!$res) {
            return 10;
        }

        foreach($res as $item) {
            if (isset($item['max_sort'])) {
                return (int)$item['max_sort'] + 10;
            }
        }

        return 10;
    }

    /**
     * Сортировка элементов
     * @param  mixed             $entity
     * @param  mixed             $destEntity
     * @param  string[optional] $moveType
     */
    public function sortEntity($entity, $destEntity, $moveType = 'next')
    {
        // Сортируемые объекты должны быть объектами
        if (!is_object($entity) || !is_object($destEntity) || !method_exists($entity, 'getSort') || !method_exists($destEntity, 'getSort')) {
            return;
        }

        $isParented = false;
        $filter = array();

        // Если объекты имеют родителя - он должен быть один и тот же
        if (method_exists($entity, 'getParent') && method_exists($destEntity, 'getParent')) {
            if ($entity->getParent() !== $destEntity->getParent()) {
                return;
            } else {
                $filter['parent'] = $entity->getParent();
                if (is_object($filter['parent'])) {
                    $filter['parent'] = $filter['parent']->getId();
                }
            }
        }

        $this->_em->persist($entity);
        $this->_em->persist($destEntity);

        switch ($moveType) {
            case 'next':
                $entity->setSort($destEntity->getSort() + 1);
            break;

            case 'prev':
                $entity->setSort($destEntity->getSort() - 1);
            break;
        }

        $this->_em->flush();

        // Выравниваем сортировку
        $i = 10;
        foreach($this->findBy($filter, array('sort' => 'ASC')) as $item) {
            $item->setSort($i);
            $i += 10;
        }

        $this->_em->flush();
    }

    /**
     * Перемещает сущность в начао списка
     */
    public function setFirstSortEntity($entity)
    {
        // Сортируемые объекты должны быть объектами
        if (!is_object($entity) || !method_exists($entity, 'getSort')) {
            return;
        }

        $entity->setSort(-1000);
        $this->_em->flush();

        $filter = array();

        // Если объекты имеют родителя - он должен быть один и тот же
        if (method_exists($entity, 'getParent')) {
            $filter['parent'] = $entity->getParent();

            if (is_object($filter['parent'])) {
                $filter['parent'] = $filter['parent']->getId();
            }
        }

        // Выравниваем сортировку
        $i = 10;
        foreach($this->findBy($filter, array('sort' => 'ASC')) as $item) {
            $item->setSort($i);
            $i += 10;
        }

        $this->_em->flush();
    }

    /**
     * Получение следующего по сорировке элемента
     * @param  mixed   $entity
     * @param  integer[optional] $parent
     * @return mixed|NULL
     */
    public function getNextItem($entity, $parent = null)
    {
        if (!is_object($entity) || !method_exists($entity, 'getSort')) {
            return;
        }

        if ($parent) {
            $result = $this->createQuery('SELECT n FROM __CLASS__ n WHERE n.parent = :parent AND n.sort > :sort AND n.id != :id ORDER BY n.sort')
                ->setMaxResults(2)
                ->setParameters(array(
                    'id' => $entity->getId(),
                    'parent' => (int)$parent,
                    'sort' => $entity->getSort(),
                ))
                ->getResult();
        } else {
            $result = $this->createQuery('SELECT n FROM __CLASS__ n WHERE n.sort > :sort AND n.id != :id ORDER BY n.sort')
                ->setMaxResults(2)
                ->setParameters(array(
                    'id' => $entity->getId(),
                    'sort' => $entity->getSort(),
                ))
                ->getResult();
        }

        if (isset($result[0])) {
            return $result[0];
        }
    }

    /**
     * Получение предыдущего по сорировке элемента
     * @param  mixed   $entity
     * @param  integer[optional] $parent
     * @return mixed|NULL
     */
    public function getPrevItem($entity, $parent = null)
    {
        if (!is_object($entity) || !method_exists($entity, 'getSort')) {
            return;
        }

        if ($parent) {
            $result = $this->createQuery('SELECT n FROM __CLASS__ n WHERE n.parent = :parent AND n.sort < :sort AND n.id != :id ORDER BY n.sort DESC')
                ->setMaxResults(2)
                ->setParameters(array(
                    'id' => $entity->getId(),
                    'parent' => (int)$parent,
                    'sort' => $entity->getSort(),
                ))
                ->getResult();
        } else {
            $result = $this->createQuery('SELECT n FROM __CLASS__ n WHERE n.sort < :sort AND n.id != :id ORDER BY n.sort DESC')
                ->setMaxResults(2)
                ->setParameters(array(
                    'id' => $entity->getId(),
                    'sort' => $entity->getSort(),
                ))
                ->getResult();
        }

        if (isset($result[0])) {
            return $result[0];
        }
    }

    /**
     * Общее количество сущностей
     */
    public function getEntityCount()
    {
        return $this->_em->createQueryBuilder()
            ->select('COUNT(e.id)')
            ->from($this->_entityName, 'e')
            ->getQuery()
                ->getSingleScalarResult();
    }
}
