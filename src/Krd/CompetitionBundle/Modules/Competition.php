<?php

namespace Krd\CompetitionBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль открытые конкурсы
 */
class Competition extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdCompetitionBundle:Admin:Module/competition.html.twig');
    }

    /**
     * Построение списка
     * @param  integer $root
     * @return array
     */
    public function getItems($root)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array();
        }

        $itemsQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            ->orderBy('d.sort')
            ->setParameter('parent', $rootNode->getId())
            ->getQuery();

        return Paginator::createFromRequest($this->request, $itemsQuery, true);
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItems($this->node->getNode()->getId());

        switch($this->node->getNode()->getController()->getId()) {
            case 25:
                $title = 'Заказчик';
                break;

            case 26:
            default:
                $title = 'Организатор конкурса';
                break;
        }

        return $this->twig->render('KrdCompetitionBundle:Module:competition/list.html.twig', array('list' => $result, 'title' => $title));
    }
}
