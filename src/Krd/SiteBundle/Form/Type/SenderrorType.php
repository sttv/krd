<?php

namespace Krd\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Форма сообщения об ошибке
 */
class SenderrorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $name = $builder->create('name', 'text', array(
            'label' => 'Контактные данные',
            'required' => false,
            'constraints' => array(
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());
        $email = $builder->create('email', 'text', array(
            'label' => 'Электронная почта',
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Email(),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $url = $builder->create('url', 'text', array(
            'label' => 'Адрес страницы с ошибкой',
            'required' => true,
            'attr' => array('readonly' => 'readonly'),
            'constraints' => array(
                new Assert\NotBlank(),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $error = $builder->create('error', 'choice', array(
            'label' => 'Характер ошибки',
            'required' => true,
            'choices' => array(
                'Неверная информация' => 'Неверная информация',
                'Орфографическая ошибка' => 'Орфографическая ошибка',
                'Функциональная ошибка' => 'Функциональная ошибка',
                'Просто совет' => 'Просто совет',
                'Сообщение для администрации проекта' => 'Сообщение для администрации проекта',
            ),
            'empty_value' => '--- Выберите из списка ---',
            'constraints' => array(
                new Assert\NotBlank(),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $message = $builder->create('message', 'textarea', array(
            'label' => 'Описание',
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($name);
        $builder->add($email);
        $builder->add($url);
        $builder->add($error);
        $builder->add($message);

        $builder->add('submit', 'submit', array(
            'label' => 'Отправить',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\SiteBundle\Entity\Senderror',
            'csrf_protection' => true,
        ));
    }

    public function getName()
    {
        return 'site_senderror';
    }
}
