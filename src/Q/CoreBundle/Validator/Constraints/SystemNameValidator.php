<?php

namespace Q\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Валидатор системного имени элемента привязанного к ноду
 */
class SystemNameValidator extends ConstraintValidator
{
    protected $em;

    /**
     * @param entityManager $entityManager Doctrine entitymanager
     */
    public function __construct($entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $parentNode = $this->context->getRoot()->get('parent')->getData();
        $entity = $this->context->getRoot()->getData();

        if (is_null($parentNode) || (!empty($value) && !is_object($entity))) {
            return;
        }

        if (empty($value) || !preg_match('/^[a-z0-9\-_]+$/', $value)) {
            $this->context->addViolation($constraint->message);
            return;
        }

        $qb = $this->em->createQueryBuilder()
            ->select('n')
            ->from(get_class($entity), 'n')
            ->andWhere('n.parent = :parent')
            ->andWhere('n.name = :name')
            ->setParameter('parent', $parentNode->getId())
            ->setParameter('name', $value);

        if ($entityId = $entity->getId()) {
            $qb->andWhere('n.id != :id')
                ->setParameter('id', $entityId);
        }

        // Список соседних элементов с таким же системным именем
        $siblings = $qb->getQuery()->getResult();

        if (!empty($siblings)) {
            $this->context->addViolation($constraint->messageUnique);
            return;
        }
    }
}
