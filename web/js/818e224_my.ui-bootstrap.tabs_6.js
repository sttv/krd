/**
 * Переопределение табов для запоминания в адресной строке
 * Так же взаимодействует с директивой entityList инициируя обновление списка
 */
angular.module('ui.bootstrap.tabs', [])
    /**
     * Контроллер переключения табов
     */
    .controller('TabsController', ['$scope', '$element', '$location', '$timeout', function($scope, $element, $location, $timeout) {
        var panes = this.panes = $scope.panes = [];

        /**
         * Делает вкладку активной
         * pane - scope вкладки
         * changeLocation - изменять ли хеш текущей страницы
         * update - инициировать ли обновление entityList
         */
        this.select = $scope.select = function selectPane(pane, changeLocation, update) {
            angular.forEach(panes, function(pane) {
                pane.selected = false;
            });

            pane.selected = true;

            if (changeLocation !== false) {
                $location.path(pane.url);
            }

            if (update === true && $.isFunction(pane.updateEntityList)) {
                pane.updateEntityList();
            }
        };

        this.addPane = function addPane(pane, index) {
            if ($.inArray(pane, panes) === -1) {
                panes.splice(index, 0, pane);
            }

            if (panes.length == 0 || $location.path() == pane.url) {
                $scope.select(pane, $location.path() == pane.url);
            }
        };

        this.hidePane = $scope.hidePane = function hidePane(pane) {
            var index = panes.indexOf(pane);
            pane.hidden = true;
            if (panes.length > 0) {
                var newPane = pane.getParent();
                $scope.select(newPane ? newPane : panes[index < panes.length ? index : index-1], true, true);
            }
        }

        this.removePane = function removePane(pane) {
            var index = panes.indexOf(pane);
            panes.splice(index, 1);
            if (pane.selected && panes.length > 0) {
                $scope.select(panes[index < panes.length ? index : index-1]);
            }
        };
    }])

    /**
     * Контейнер с табами
     */
    .directive('tabs', function() {
        return {
            restrict: 'EA',
            transclude: true,
            scope: {
            },
            controller: 'TabsController',
            template:  "<div class=\"tabbable\">\n" +
                "  <ul class=\"nav nav-tabs\">\n" +
                "    <li ng-repeat=\"pane in panes\" ng-class=\"{active:pane.selected}\" ng-show=\"!pane.hidden\">\n" +
                "      <a ng-click=\"select(pane)\">{{pane.heading}}</a>" +
                "    </li>\n" +
                "  </ul>\n" +
                "  <div class=\"tab-content\" ng-transclude></div>\n" +
                "</div>",
            replace: true
        };
    })

    /**
     * Обычная вкладка.
     * Может содержать entityList
     */
    .directive('pane', ['$parse', '$location', function($parse, $location) {
        return {
            require: '^tabs',
            restrict: 'EA',
            transclude: true,
            scope:{
                heading:'@',
            },
            link: function(scope, element, attrs, tabsCtrl) {
                var getSelected, setSelected;
                scope.selected = false;

                if (!!attrs.url) {
                    scope.url = attrs.url;

                    if (scope.url[scope.url.length - 1] != '/') {
                        scope.url = scope.url + '/';
                    }

                    if (scope.url[0] != '/') {
                        scope.url = '/' + scope.url;
                    }
                }

                if (attrs.active) {
                    getSelected = $parse(attrs.active);
                    setSelected = getSelected.assign;
                    scope.$watch(
                        function watchSelected() {return getSelected(scope.$parent);},
                        function updateSelected(value) {scope.selected = value;}
                    );
                    scope.selected = getSelected ? getSelected(scope.$parent) : false;
                }

                scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select(scope, false);
                    }

                    if(setSelected) {
                        setSelected(scope.$parent, selected);
                    }
                });

                tabsCtrl.addPane(scope, element.index());

                scope.location = $location;
                scope.$watch('location.path()', function(path) {
                    if (path == scope.url) {
                        scope.selected = true;
                    }
                });

                scope.$on('$destroy', function() {
                    tabsCtrl.removePane(scope);
                });

                scope.updateEntityList = function() {
                    element.find('*[entity-list]').each(function() {
                        $(this).scope().$broadcast('needUpdate');
                    });
                };
            },
            templateUrl: 'template/tabs/pane.html',
            replace: true
        };
    }])

    /**
     * Лениво-загружаемая вкладка
     * Обычно содержит формы добавления/редактирования
     *
     * parent - индекс родительской вкладки. На нее будет переключние в случае закрытия формы
     *
     * Пример:
     *     <ghostpane heading="Новая группа" url="/usergroup/create/" parent="3" template="{{ path('cms_rest_usergroup_form_add') }}"></ghostpane>
     */
    .directive('ghostpane', ['$parse', '$location', '$templateCache', '$timeout', function($parse, $location, $templateCache, $timeout) {
        return {
            require: '^tabs',
            restrict: 'EA',
            transclude: true,

            scope:{
                heading: '@',
                template: '@'
            },

            link: function($scope, element, attrs, tabsCtrl) {
                $scope.selected = false;
                $scope.hidden = true;
                $scope.location = $location;
                $scope.parent = (attrs.parent === undefined ? null : parseInt(attrs.parent, 10));

                $scope.url = attrs.url;

                if ($scope.url[$scope.url.length - 1] != '/' && $scope.url.indexOf('{id}') == -1) {
                    $scope.url = $scope.url + '/';
                }

                if ($scope.url[0] != '/') {
                    $scope.url = '/' + $scope.url;
                }

                $scope.urlRegexp = $scope.url.replace('{id}', '(\\d+)');
                $scope._url = $scope.url;

                $scope._heading = $scope.heading;

                $scope._template = $scope.template;
                $scope.template = '';
                $scope.getTemplateUrl = function(id) {
                    return $scope._template.replace('{id}', id);
                };
                $templateCache.put($scope._template, 'default empty');

                $scope.refresh = function(id) {
                    var tpl = $scope.template;

                    $scope.template = '';
                    $scope.url = $scope._url.replace('{id}', id);
                    $scope.heading = $scope._heading.replace('{id}', id);
                    $timeout(function() {
                        $templateCache.remove($scope.getTemplateUrl(id));
                        $scope.template = $scope.getTemplateUrl(id);
                    });
                };

                $scope.getParent = function() {
                    return tabsCtrl.panes[$scope.parent];
                };

                $scope.$watch('location.path()', function(path) {
                    var matcher = new RegExp($scope.urlRegexp);

                    if (matcher.test(path)) {
                        $scope.selected = true;
                        var id = path.match(matcher)[1];

                        if ($scope.hidden || $scope.lastId != id) {
                            $scope.refresh(id);
                            $scope.lastId = id;
                        }

                        $scope.hidden = false;
                    }
                });

                $scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select($scope, false);
                    }
                });

                $scope.$on('$destroy', function() {
                    tabsCtrl.removePane($scope);
                });

                $scope.$on('entityFormClosed', function(event) {
                    tabsCtrl.hidePane($scope);
                });

                tabsCtrl.addPane($scope, element.index());
            },

            template: "<div class=\"tab-pane\" ng-class=\"{active: selected}\" ng-show=\"selected && !hidden\" ng-include=\"template\"></div>",
            replace: true
        };
    }])
;
