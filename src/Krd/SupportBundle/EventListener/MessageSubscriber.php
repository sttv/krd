<?php

namespace Krd\SupportBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Bridge\Monolog\Logger;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;
use Q\CoreBundle\Entity\Config;
use Q\CoreBundle\Router\CurrentNode;
use Q\UserBundle\Entity\User;
use Krd\SupportBundle\Entity\Message;
use Krd\SupportBundle\Entity\UrlIntf;


/**
 * События для Message
 */
class MessageSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var CurrentNode
     */
    protected $qrouter;

    /**
     * Запланированные сообщения для отправки уведомлеий администратору
     * @var array
     */
    protected $deferredAdmin = array();

    /**
     * Запланированные сообщения для отправки уведомлеий пользователю
     * @var array
     */
    protected $deferredOwner = array();

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush',
            'postLoad',
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer|null
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Ленивая закгрузка сервиса qcore.config
     * @return Config|null
     */
    protected function getConfig() {
        if ($this->config === null) {
            $this->config = $this->container->get('qcore.config');
        }

        return $this->config;
    }

    /**
     * Ленивая загрузка пользователя
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->user === null) {
            $this->user = $this->container->get('security.context')->getToken()->getUser();
        }

        return $this->user;
    }

    /**
     * Ленивая загрузка роутинга Q
     * @return CurrentNode
     */
    protected function getQRouter()
    {
        if ($this->qrouter === null) {
            $this->qrouter = $this->container->get('qcore.routing.current');
        }

        return $this->qrouter;
    }

    /**
     * Сбор ново-добавленных сообщений
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Message) {
            if ($entity->getCreatedBy()->hasRole('ROLE_CMS')) {
                $this->deferredOwner[] = $entity;
            } else {
                $this->deferredAdmin[] = $entity;
            }
        }
    }

    /**
     * Отправка Email уведомлений о новых сообщениях
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        foreach($this->deferredAdmin as $entity) {
            if (($entity instanceof UrlIntf) && ($entity->getLevel() == 0)) {
                $this->updateMessageUrl($entity);
            }

            $message = $this->getMailer()->newMessage('Запрос в техподдержку на сайте %title%');

            foreach ($this->getConfig()->getAlertEmails() as $email) {
                $message->addTo($email);
            }

            try {
                $this->getMailer()->sendTemplate($message, 'KrdSupportBundle:Email:new-message.admin.html.twig', array('message' => $entity));
            } catch (\Exception $e) {
                $this->logger->crit('Ошибка отправки email уведомления о новом сообщении в сапорт админу', array($entity->getId(), $e->getMessage()));
            }
        }

        foreach($this->deferredOwner as $entity) {
            if (($entity instanceof UrlIntf) && ($entity->getLevel() == 0)) {
                $this->updateMessageUrl($entity);
                return;
            }

            $message = $this->getMailer()->newMessage('Сотрудник техподдержки ответил на ваш вопрос на сайте %title%');
            $message->addTo($entity->getRootMessage()->getCreatedBy()->getEmailCanonical());

            try {
                $this->getMailer()->sendTemplate($message, 'KrdSupportBundle:Email:new-message.user.html.twig', array('message' => $entity, 'who' => $entity->getRootMessage()->getCreatedBy()));
            } catch (\Exception $e) {
                $this->logger->crit('Ошибка отправки email уведомления о новом сообщении в сапорт пользователю', array($entity->getId(), $e->getMessage()));
            }
        }

        foreach($this->deferredOwner as $entity) {
            if ($entity->getLevel() == 0) {
                $message = $this->getMailer()->newMessage('Ваше обращение в службу технической поддержки сайта krd.ru получено.');
                $message->addTo($entity->getRootMessage()->getCreatedBy()->getEmailCanonical());

                try {
                    $this->getMailer()->sendTemplate($message, 'KrdSupportBundle:Email:created-message.user.html.twig', array('message' => $entity, 'who' => $entity->getRootMessage()->getCreatedBy()));
                } catch (\Exception $e) {
                    $this->logger->crit('Ошибка отправки email уведомления о новом сообщении в сапорт пользователю', array($entity->getId(), $e->getMessage()));
                }
            }
        }

        $this->deferredOwner = array();
        $this->deferredAdmin = array();
    }

    /**
     * Обновление урла сообщения
     * @param  Message $message
     */
    protected function updateMessageUrl(Message $message)
    {
        $url = $this->getQRouter()->generate($message->getControllerId(), $message->getDetailRoute(), array('name' => $message->getName()));

        $message->setUrl($url);
    }

    /**
     * Индикация прочитанности веток сообщений
     * @param  LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if (($entity instanceof Message) && ($entity->getLevel() == 0)) {
            if (($user = $this->getUser()) && ($user instanceof User)) {
                $em->getRepository('KrdSupportBundle:Message')->updateReaded($entity, $user);
            }
        }

        if (($entity instanceof UrlIntf) && ($entity->getLevel() == 0)) {
            $this->updateMessageUrl($entity);
        }
    }
}

