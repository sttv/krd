var searchMapParams = Backbone.Model.extend({
	defaults:{
		center: [45.034942, 38.976032],
		zoom: 13,
		fullscreen: false
	}
});

/**
 * Представление для метки
 */
var searchMapPlacemarkView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;
		self.mapView = options.mapView;
		self.ymap = options.mapView.ymap;

		self.createPlacemark();

		self.on('change', function(){
			self.placemark.properties.set({place: self.model.toJSON()});
		});

		self.model.on('change:yIcon', function(){
			self.placemark.properties.set({iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>'});
		});

		self.model.on('change:yColor change:yState', function(){
			self.placemark.options.set({iconImageHref: self.getIconSrc()});
		});

		// Скрытие неактивной метки
		self.model.on('change:yState', function(model, yState){
			// if (window.socialMapSearch.hasOne){
			// 	return;
			// }

			if (yState == 'active'){
				self.mapView.addPlaceMarkToBuffer(self.placemark);
			}else{
				self.mapView.removePlacemark(self.placemark);
			}
		});
	},

	/**
	 * Возвращает класс дял иконки
	 */
	getIconClass: function(){
		return 'i-'+this.model.get('yIcon');
	},

	/**
	 * Возвращает ссылку на фон плейсмарка
	 */
	getIconSrc: function(){
		var self = this;

		if (self.model.get('yState') == 'active'){
			return '/bundles/krdopendata/i/placemark-bg-'+self.model.get('yColor')+'.png';
		}else{
			return '/bundles/krdopendata/i/placemark-bg.png';
		}
	},

	// Первоначальное создание метки и нанесение ее на карту
	createPlacemark: function(){
		var self = this;

		self.placemark = new ymaps.Placemark(self.model.get('yCoords'),
			{
				iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>',
				place: self.model.toJSON()
			},
			{
				iconLayout:'default#imageWithContent',
				iconImageHref: self.getIconSrc(),
				iconImageSize: [37, 36],
				iconImageOffset: [0, -36],

				hideIconOnBalloonOpen: true,
				balloonPane: 'movableOuters',
				balloonShadowPane: 'movableOuters',
				balloonShadow: false,
				balloonOffset: [100, 13],
				balloonMaxWidth: 196,
				balloonMinWidth: 196,
				balloonContentLayout: self.mapView.yPlacemarkTLF,

				// overlayFactory: 'default#interactiveGraphics'
			}
		);

		self.mapView.addPlaceMarkToBuffer(self.placemark);
	}
});

/**
 * Представление карты
 */
var searchMapView = Backbone.View.extend({
	// Полигоны районов города
	regions: {},

	initialize: function(){
		var self = this;

		// Параметры отображения карты
		self.params = new searchMapParams();

		// При добавлении учреждения в общую коллекцию - создаем плейсмарк для карты
		self.on('placeAdd', function(model){
			var view = new searchMapPlacemarkView({model:model, mapView:self});
		});

		// Мигание карты при определенных событиях
		self.on('highlight', function(){
			self.$el.closest('.sm-map-wrap').addClass('highlighted');
			!!this.__timer && clearTimeout(this.__timer);
			this.__timer = setTimeout(function(){
				self.$el.closest('.sm-map-wrap').removeClass('highlighted');
			}, 300);
		});

        // Отрисовка карты
        self.on('renderMap', function() {
            self.ymap.geoObjects.add(self.clusterer);
        });

		window.___ymapsLoadCallback = function(){self.initYMap()};
		$.getScript('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU&onload=___ymapsLoadCallback');
	},

	// Добавление кнопки разворачивания на весь экран
	initFullScreen: function(){
		var self = this;

		var $button = $('<a href="javascript:$.noop();" class="full-screen-btn btn btn-deep-blue"></a>');
		$button.appendTo('.sm-map-wrap');

		var __onFSChange = function(){
			if (self.params.get('fullscreen')){
				$button.text('Свернуть');
			}else{
				$button.text('Развернуть');
			}
		};

		// Клик по кнопке
		$button.on('click', function(e){
			self.params.set({fullscreen:!self.params.get('fullscreen')});
		});

		__onFSChange();

		// Изменение полноэкранного режима
		self.params.on('change:fullscreen', function(model, fullscreen){
			__onFSChange();

			if (fullscreen){
				$('body').addClass('fullscreen');
			}else{
				$('body').removeClass('fullscreen');
			}

			self.ymap.container.fitToViewport();
		});
	},

	/**
	 * Обработка событий
	 */
	initEvents: function(){
		var self = this;

		// Изменение центра карты
		self.params.on('change:center', function(params, center){
			self.ymap.setCenter(center);
		});

		// Изменение зума карты
		self.params.on('change:zoom', function(params, zoom){
			self.ymap.setZoom(zoom);
		});

		// Запонинание позиции и зума карты
		self.ymap.events.add('boundschange', function(e){
			if (e.get('oldCenter') != e.get('newCenter')){
				self.params.set({center:e.get('newCenter')}, {silent:true});
			}

			if (e.get('oldZoom') != e.get('newZoom')){
				self.params.set({zoom:e.get('newZoom')}, {silent:true});
			}
		});

		// При выборе района города - показываем этот район

		window.socialMapSearch.searchParams.on('change:regions', function(model, regions){
			var region = regions[0];

			if (self.regions[region] === undefined){
				return;
			}

			var pol = new ymaps.Polygon([self.regions[region]]);
			self.ymap.geoObjects.add(pol);
			self.ymap.setBounds(pol.geometry.getBounds());
			self.ymap.geoObjects.remove(pol);
		});
	},

	// Закрытие любого балуна при клике на пустом месте карты
	initCloseOnEmptyClick: function(){
		var self = this;

		self.ymap.events.add('click', function(){
			if (!!self.ymap.balloon) self.ymap.balloon.close();
		});
	},

	/**
	 * Создает шаблон плейсмарки
	 */
	precachePlacemark: function(){
		var self = this;

		self.yPlacemarkTLF = ymaps.templateLayoutFactory.createClass(
			'<span class="ttl">$[properties.place.title]</span>' +
			'<div class="descr">' +
				'<div class="descr-row"><span>Категория:</span> $[properties.place.subcategories_txt]</div>' +
				'<div class="descr-row"><span>Адрес:</span> $[properties.place.address]</div>' +
				'<div class="descr-row"><span>Телефон:</span> $[properties.place.phone]</div>' +
				'<a href="$[properties.place.href]" class="button">Подробнее<i></i></a>' +
			'</div>',
			{
				build: function() {
						var layout = this;
						layout.constructor.superclass.build.call(this);
						var $this = $(this.getParentElement());
						var $body = $('<div class="sm-balloon-wrap-outer"><div class="sm-balloon-wrap-inner"></div></div>');
						var $oldBalloonBody = $this.closest('.ymaps-b-balloon');


						$oldBalloonBody.after($body);
						$this.appendTo($body.find('.sm-balloon-wrap-inner'));
						$oldBalloonBody.remove();
						setTimeout(function(){
							$body.width(236);
						}, 100);
					}
			}
		);
	},

	/**
	 * Инициализация кластера меток
	 */
	initClusterer: function(){
		var self = this;

		self.clusterer = new ymaps.Clusterer({
			gridSize: 48,
			maxZoom: 16,
			clusterOpenBalloonOnClick: false,
	        clusterNumbers: [1000]
		});
	},

	/**
	 * Добавляет метку в буффер, но на карту они добавляются с задежкой, для улучшения быстродействия
	 * * Нифига. Пока не реализовано
	 */
	addPlaceMarkToBuffer: function(placemark){
		var self = this;

		self.clusterer.add(placemark);
	},

	/**
	 * Убирает метку с карты
	 */
	removePlacemark: function(placemark){
		var self = this;

		self.clusterer.remove(placemark);
	},

	/**
	 * Первичная инициализация карты
	 */
	initYMap: function(){
		var self = this;

		// Создание карты
		self.ymap = new ymaps.Map(self.$el.attr('id'), {
			center: self.params.get('center'),
			zoom: self.params.get('zoom'),
			behaviors: ['default', 'scrollZoom'],
			overlayFactory: ymaps.geoObject.overlayFactory.interactiveGraphics
		});

		// Добавление контролов
		self.ymap.controls
			.add('typeSelector', {top:40, right:7})
			.add('scaleLine')
			.add('mapTools')
			.add('zoomControl')
			// .add('trafficControl')
			.add(new ymaps.control.RouteEditor())
			.add(new ymaps.control.MiniMap({type: 'yandex#publicMap'}));

		self.initFullScreen();
		self.initEvents();
		self.precachePlacemark();
		self.initCloseOnEmptyClick();
		self.initClusterer();

		self.trigger('initialized');
	},

	/**
	 * Добавялет полигон района города в список
	 */
	appendRegion: function(region){
		var self = this;

		if (!$.isArray(region.polygon)){
			return;
		}

		$.each(region.polygon, function(key, val){
			$.each(val, function(k2, v2){
				val[k2] = parseFloat(v2, 10);
			});

			region.polygon[key] = val;
		});


		self.regions[region.id] = region.polygon;
	}
});
