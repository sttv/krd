<?php

namespace Krd\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Форма профиля пользователя
 */
class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastname', 'text', array(
            'label' => 'Фамилия',
            'required' => false,
            'attr' => array('placeholder' => 'Иванов'),
        ));

        $builder->add('name', 'text', array(
            'label' => 'Имя',
            'required' => false,
            'attr' => array('placeholder' => 'Иван'),
        ));

        $builder->add('surname', 'text', array(
            'label' => 'Отчество',
            'required' => false,
            'attr' => array('placeholder' => 'Иванович'),
        ));

        $builder->add('phone', 'text', array(
            'label' => 'Телефон',
            'required' => false,
            'attr' => array('placeholder' => '+7-900-111-22-33'),
        ));

        $builder->add('address_index', 'text', array(
            'label' => 'Индекс',
            'required' => false,
            'attr' => array('placeholder' => '350002'),
        ));

        $builder->add('address_city', 'text', array(
            'label' => 'Город',
            'required' => false,
            'attr' => array('placeholder' => 'Краснодар'),
        ));

        $builder->add('address_region', 'text', array(
            'label' => 'Район',
            'required' => false,
            'attr' => array('placeholder' => 'Западный'),
        ));

        $builder->add('address_street', 'text', array(
            'label' => 'Улица',
            'required' => false,
            'attr' => array('placeholder' => 'Красная'),
        ));

        $builder->add('address_home', 'text', array(
            'label' => 'Дом/корпус',
            'required' => false,
            'attr' => array('placeholder' => '12/4'),
        ));

        $builder->add('address_room', 'text', array(
            'label' => 'Квартира',
            'required' => false,
            'attr' => array('placeholder' => '12'),
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Пароли не совпадают',
            'first_options' => array('label' => 'Новый пароль'),
            'second_options' => array('label' => 'Повтор пароля'),
            'required' => false,
        ));

        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Q\UserBundle\Entity\User',
            'validation_groups' => array('profile'),
        ));
    }

    public function getName()
    {
        return 'user_profile';
    }
}
