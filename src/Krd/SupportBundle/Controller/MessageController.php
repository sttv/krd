<?php

namespace Krd\SupportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\SupportBundle\Entity\Message;


/**
 * Сообщения в техподдержке
 * @PreAuthorize("hasRole('ROLE_USER')")
 */
class MessageController extends Controller
{
    /**
     * Создание новой ветки сообщений
     *
     * @Route("/create/", defaults={"_format"="json"}, name="krd_support_io_create")
     * @Method({"POST"})
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->get('krd.user.modules.support')->getCreateFormBuilder()->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }

        $message = $form->getData();

        if ($message instanceof Message) {
            $this->getDoctrine()->getManager()->persist($message);
            $this->getDoctrine()->getManager()->flush();

            $this->getDoctrine()->getManager()->getRepository('KrdSupportBundle:Message')->markAsReaded($message, $message->getCreatedBy());

            return array('success' => true);
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }

    /**
     * Ответ на сообщение
     *
     * @Route("/reply/", defaults={"_format"="json"}, name="krd_support_io_reply")
     * @Method({"POST"})
     * @Template()
     */
    public function replyAction(Request $request)
    {
        $form = $this->get('krd.user.modules.support')->getReplyFormBuilder()->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }

        $message = $form->getData();

        if ($message instanceof Message) {
            $this->getDoctrine()->getManager()->persist($message);
            $this->getDoctrine()->getManager()->flush();

            $this->getDoctrine()->getManager()->getRepository('KrdSupportBundle:Message')->markAsReaded($message, $message->getCreatedBy());

            return array('success' => true);
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }
}
