<?
include_once 'conf.php';
$page = 5;

$auth = new auth();
if (!$auth->check()){
	header('Location: index.php');
    exit;
}

$db = mysql::getInstance();

$item = new items();
$field = new fields();
switch($_REQUEST['action']) {
	case 'fielddel':
		$nid = intval($_REQUEST['nid']);
		$fid = intval($_REQUEST['fid']);
        if ($nid > 0 && $fid > 0){
            $db->query("DELETE FROM item_fields WHERE node_id={$nid} AND field_id={$fid} LIMIT 1");
        }
    case 'edit':
		$nid = intval($_REQUEST['nid']);
		$list_fields = $field->list_fields();
		if (isset($_REQUEST['send']) && $nid > 0) {
			$new_field = array();
			$old_field = array();
			$del_field = array();
			foreach($_REQUEST['new_field_id_'.$nid] as $key => $val){
				if (intval($val) > 0) {
					$new_field[] = array(
						'field_id' => intval($val),
						'body' => $_REQUEST['new_field_body_'.$nid][$key],
						'order' => $_REQUEST['new_field_order_'.$nid][$key]
					);
				}
			}
			foreach($_REQUEST['field_id_'.$nid] as $key => $val){
				if (intval($val) > 0 && !isset($_REQUEST['field_del_'.$nid][$key])) {
					$old_field[$key] = array(
						'field_id' => intval($val),
						'body' => $_REQUEST['field_body_'.$nid][$key],
						'order' => $_REQUEST['field_order_'.$nid][$key]
					);
				}
			}
			foreach($_REQUEST['field_del_'.$nid] as $key => $val){
				if (intval($val) == 1) {
					$del_field[] = $key;
				}
			}
			$fieldsPost = array(
				'old' => $old_field,
				'new' => $new_field,
				'del' => $del_field
			);

			if ($item->update($nid, $_REQUEST['title'], $fieldsPost, array('settings' => $settingPhoto, 'files' => $_FILES['photo']), $_REQUEST['photodel'], $_REQUEST['status_obj'])) {
				header('Location: items.php');
				exit;
			} else {
				$dataObj = $item->get($nid);
				if ($dataObj === false) {
					header('Location: items.php');
					exit;
				}
				$_REQUEST['title'] = $dataObj['node']['title'];
			}
		} else {
			$dataObj = $item->get($nid);
			
			if ($dataObj === false) {
				header('Location: items.php');
				exit;
			}
			$_REQUEST['title'] = $dataObj['node']['title'];
			$_REQUEST['status_obj'] = $dataObj['node']['status_obj'];
		}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
		<!-- Add fancyBox main JS and CSS files -->
		<script type="text/javascript" src="./media/js/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="./media/css/jquery.fancybox.css?v=2.1.5" media="screen" />
		
        <script src="./media/js/bootstrap.min.js"></script>
		<script src="./media/js/admin.js"></script>
		
        <style type="text/css">
            .empty-form {display: none;}
			.table th.text-center, td.text-center {
				text-align: center;   
			}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Редактирование объекта: <?=stripcslashes($dataObj['node']['address']);?></h1>
					<hr>
					<form method="post" enctype="multipart/form-data">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<div class="control-group">
							<label for="title">Название:</label>
							<input id="title" type="text" name="title" value="<?=@$_REQUEST['title']?>" class="span6">
						</div>
						<div class="control-group">
							<label for="title">Cтатус объекта:</label>
							<?=$item->select_status($arrStatus, 'status_obj', 'status_obj', $_REQUEST['status_obj']);?>
						</div>
						<div class="control-group">
							<input id="photo" type="file" name="photo" style="visibility:hidden;width:1px;height:1px" class="file">
							<label for="photo">Фото объекта:</label>
							<input id="photo-upload" type="text" placeholder="Загрузить файл" style="cursor:pointer" readonly="" class="file-upload span6">
							<?if(!empty($dataObj['node']['img_preview']) && !empty($dataObj['node']['img_detail'])):?>
							<label class="checkbox"><input type="checkbox" name="photodel" value="1" />Удалить фото</label>
							<p><a class="fancybox" rel="group" href="<?=$dataObj['node']['img_detail']?>"><img src="<?=$dataObj['node']['img_preview']?>" class="img-upload img-polaroid" /></a></p>
							<?endif;?>
						</div>
						<div class="control-group">
							<label>Свойства объекта</label>
						</div>
						<table class="table table-striped table-bordered">
							<tbody id="rowFields">
								<tr>
									<th>Заголовок</th><th>Значение</th><th>Сортировка</th><th class="text-center">Удалить?</th>
								</tr>
								<?
								if (count($dataObj['fields']) > 0) {
									foreach($dataObj['fields'] as $fielddata) {
										echo '<tr>';
										echo '<td>'.$field->select_fields($list_fields, 'field_id_'.$nid.'['.$fielddata['id'].']', 'field_id_'.$nid.'_'.$fielddata['id'], $fielddata['field_id']).'</td>';
										echo '<td><textarea name="field_body_'.$nid.'['.$fielddata['id'].']" class="span5">'.$fielddata['body'].'</textarea></td>';
										echo '<td><input type="text" size="5" name="field_order_'.$nid.'['.$fielddata['id'].']" value="'.$fielddata['order'].'" class="span1" /></td>';
										echo '<td class="text-center"><input type="checkbox" name="field_del_'.$nid.'['.$fielddata['id'].']" value="1" class="span1" /></td>';
										echo '</tr>';
									}
								} else {?>
								<tr>
									<td><?=$field->select_fields($list_fields, 'new_field_id_'.$nid.'[]')?></td>
									<td><textarea name="new_field_body_<?=$nid;?>[]" class="span5"></textarea></td>
									<td><input type="text" size="5" name="new_field_order_<?=$nid;?>[]" value="10" class="span1" /></td>
									<td></td>
								</tr>
								<?}?>
								<tr class="empty-form" id="entry_set-empty">
									<td>
										<?=$field->select_fields($list_fields, 'new_field_id___prefix__[]');?>
									</td>
									<td>
										<textarea name="new_field_body___prefix__[]" class="span5"></textarea>
									</td>
									<td>
										<input type="text" size="5" name="new_field_order___prefix__[]" value="10" class="span1" />
									</td>
									<td id="delete" class="text-center"><a href="#" id="delRow" rowid="__prefix1__"><i class="icon-trash"></i></a></td>
								</tr>
							</tbody>
						</table>
						<div class="control-group">
							<input type="button" value="Добавить свойство" id="addRow" nodeid="<?=$nid?>" class="btn" />
						</div>
						<div class="control-group">
							<input type="hidden" name="action" value="edit" />
							<input type="hidden" name="nid" value="<?=$nid?>" />
							<input type="hidden" name="send" value="1" />
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary input-large">Сохранить</button>&nbsp;&nbsp;<a href="items.php" class="btn">Отмена</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				var count = 0;
				$('#addRow').click(function(){
					count = count+1;
					var nodeid = $( this ).attr('nodeid');
					var template = $("#entry_set-empty").html();
					template = template.replace(/__prefix__/g, nodeid);
					template = template.replace(/__prefix1__/g, count);
					
					$('#rowFields').append('<tr id="row_'+count+'">'+template+'</tr>');
				});
				
				$('body').on('click', 'a#delRow', function() {
					var rowid = $( this ).attr('rowid');					
					var tr = $('#row_'+rowid);			
					tr.fadeOut(400, function(){
						tr.remove();
					});
				  return false;
				});
			});
		</script>
	</body>
</html>
        <?
        break;
    default:
		$cnode = new nodes();
		$nodes = $cnode->list_nodes();
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
			.table th.text-center, td.text-center {
				text-align: center;   
			}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Объекты</h1>
					<hr>
					<form action="" method="post">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<table class="table table-striped table-bordered table-hover">
							<tbody>
								<tr>
									<th>Адрес</th>
								</tr>
								<?
								if (count($nodes) > 0) {
									foreach($nodes as $nid => $node){
										echo '<tr>';
										echo '<td><a href="items.php?action=edit&nid='.$nid.'"><i class="icon-edit"></i>&nbsp;'.$node['address'].'</a></td>';
										echo '</tr>';
									}
								} else {
									echo '<tr><td>Нет ни одного объекта</td></tr>';
								}
								?>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
    </body>
</html>
        <?
        break;
}