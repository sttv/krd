<?php

namespace Q\CoreBundle\Repository;

use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\CoreBundle\Entity\Redirect;


/**
 * Репозиторий для редиректов
 */
class RedirectRepository extends EntityRepository
{
    public function getByRequest(Request $request)
    {
        $url1 = $request->getPathInfo();
        $url2 = mb_strtolower($url1);
        $url3 = rtrim($url2, ' /');
        $url4 = $url3.'/';

        $hashes = array(md5($url1), md5($url2), md5($url3), md5($url4));

        $redirect = $this->createQueryBuilder('e')
            ->andWhere('e.urlhash IN (:hashes)')
            ->setParameters(array('hashes' => $hashes))
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if ($redirect && isset($redirect[0]) && $redirect[0] instanceof Redirect) {
            return $redirect[0];
        } else {
            return false;
        }
    }
}
