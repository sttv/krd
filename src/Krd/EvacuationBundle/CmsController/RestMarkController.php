<?php

namespace Krd\EvacuationBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер марок эвакуированных автомобилей
 *
 * @Route("/rest/krd/evacuation/mark")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestMarkController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdEvacuationBundle:Mark';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_evacuation_mark_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_evacuation_mark_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_evacuation_mark", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        $qb->orderBy('e.sort');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_evacuation_mark_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $res = parent::createAction($request);

        if (isset($res['entity'])) {
            $this->getRepository()->setFirstSortEntity($res['entity']);
        }

        $this->getDoctrine()->getManager()->flush();

        return $res;
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_evacuation_mark_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_evacuation_mark_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_evacuation_mark_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_evacuation_mark_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Сортировка элемента
     *
     * @Route("/sort/{order}/", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function actionSortOfParent(Request $request, $order)
    {
        return parent::actionSortOfParent($request, $order);
    }
}
