<?php

namespace Krd\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\Video;


/**
 * Новость
 *
 * @ORM\Entity(repositoryClass="Krd\NewsBundle\Repository\NewsRepository")
 * @ORM\Table(name="news",
 *      indexes={
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="refid", columns={"refid"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="rssdisabled", columns={"rssdisabled"}),
 *          @ORM\Index(name="emersent", columns={"emersent"}),
 *      })
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     redirect={"text", {
 *         "label"="Редирект<br /><em>абсолютный URL</em>",
 *         "required"=false
 *     }},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     themes={"news_theme", {
 *         "label"="Темы",
 *         "required"=false
 *     }},
 *     announce={"textarea", {
 *         "label"="Краткое содержание",
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     seoTitle={"text", {
 *         "label"="SEO Title",
 *         "required"=false
 *     }},
 *     seoKeywords={"textarea", {
 *         "label"="SEO Keywords",
 *         "required"=false
 *     }},
 *     seoDescription={"textarea", {
 *         "label"="SEO Description",
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     onhome={"checkbox", {
 *         "label"="На главную",
 *         "required"=false
 *     }},
 *     rssdisabled={"checkbox", {
 *         "label"="Не выводить в RSS ленту",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     video={"files", {
 *         "label"="Видео",
 *         "class"="QFilesBundle:Video",
 *         "widget"="video"
 *     }},
 *     files={"files", {
 *         "label"="Файлы"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class News implements NodeUrlIntf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Название
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;


    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;


    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;

    /**
     * Редирект
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $redirect = '';


    /**
     * Краткое содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $announce = '';


    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';

    /**
     * SEO Title
     * @ORM\Column(type="text", name="seo_title", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $seoTitle = '';

    /**
     * SEO keywords
     * @ORM\Column(type="text", name="seo_keywords", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $seoKeywords = '';

    /**
     * SEO Description
     * @ORM\Column(type="text", name="seo_description", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $seoDescription = '';

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Не выводить в RSS
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $rssdisabled = false;

    /**
     * @ORM\ManyToMany(targetEntity="NewsTheme", inversedBy="news", cascade={"persist"})
     * @ORM\JoinTable(name="news_newstheme_relation")
     *
     * @ORM\OrderBy({"title" = "ASC"})
     *
     * @JMS\Expose
     */
    private $themes;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="news_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $files;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="news_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Video", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="news_video_relation",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $video;
	
	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $refid;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("content_type")
     * @JMS\Accessor(getter="getContentType")
     */
    private $contentType;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getMainImage")
     */
    private $mainImage;
	
	/**
	 * @JMS\Expose
	 * @JMS\SerializedName("comments")
     * @JMS\Accessor(getter="getCountComment")
	 */
	private $comments;
	
	/**
     * Выводить новость на главную
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
	private $onhome;

	/**
     * Экстренная новость была отправлена
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
	private $emersent = false;


    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRedirect()
    {
        return $this->redirect;
    }

    public function hasRedirect()
    {
        return !empty($this->redirect);
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }
	
	public function setRefId($id) {
		$this->refid = $id;
	}
	
	public function getRefId()
	{
		return $this->refid;
	}

    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    public function hasSeoTitle()
    {
        return !empty($this->seoTitle);
    }

    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    public function hasSeoKeywords()
    {
        return !empty($this->seoKeywords);
    }

    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    }

    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    public function hasSeoDescription()
    {
        return !empty($this->seoDescription);
    }

    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }
	
	public function getOnHome()
    {
        return $this->onhome;
    }

    public function setOnHome($onhome)
    {
        $this->onhome = (boolean)$onhome;
    }

    public function getRssdisabled()
    {
        return $this->rssdisabled;
    }

    public function setRssdisabled($rssdisabled)
    {
        $this->rssdisabled = (boolean)$rssdisabled;
    }

    public function getThemes()
    {
        return $this->themes ?: $this->themes = new ArrayCollection();
    }

    public function addThemes(NewsTheme $theme)
    {
        if (!$this->getThemes()->contains($theme)) {
            $this->getThemes()->add($theme);
        }
    }

    public function removeThemes(NewsTheme $theme)
    {
        if ($this->getThemes()->contains($theme)) {
            $this->getThemes()->removeElement($theme);
        }
    }

    public function getFiles()
    {
        return $this->files ?: $this->files = new ArrayCollection();
    }

    public function hasFiles()
    {
        return count($this->getFiles()) > 0;
    }

    public function addFiles(File $file)
    {
        if (!$this->getFiles()->contains($file)) {
            $this->getFiles()->add($file);
        }
    }

    public function removeFiles(File $file)
    {
        if ($this->getFiles()->contains($file)) {
            $this->getFiles()->removeElement($file);
        }
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function getVideo()
    {
        return $this->video ?: $this->video = new ArrayCollection();
    }

    public function getVideoConverted()
    {
        $list = new ArrayCollection();

        foreach($this->getVideo() as $video) {
            if ($video->getConverted()) {
                $list[] = $video;
            }
        }

        return $list;
    }

    public function hasVideo()
    {
        return count($this->getVideo()) > 0;
    }

    public function hasVideoConverted()
    {
        return count($this->getVideoConverted()) > 0;
    }

    public function addVideo(Video $image)
    {
        if (!$this->getVideo()->contains($image)) {
            $this->getVideo()->add($image);
        }
    }

    public function removeVideo(Video $image)
    {
        if ($this->getVideo()->contains($image)) {
            $this->getVideo()->removeElement($image);
        }
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
	
	public function setCountComment( $comments )
	{
		$this->comments = $comments;
	}
	
	public function getCountComment()
	{
		return $this->comments;
	}
    /**
     * Возвращает строку обозначающую содержание новости
     * Изображение или видео
     * @return string
     */
    public function getContentType()
    {
        if ($this->hasVideoConverted()) {
            return 'video';
        }

        if ($this->hasImages()) {
            return 'photo';
        }

        return '';
    }

    public function getDetailRoute()
    {
        return 'krd_news_news_detail';
    }
}


