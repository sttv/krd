
angular.module('news.themes', [])
	.directive('newsThemes', function($http, $timeout) {
		return {
			restrict : 'A',
			scope : true,
			
			link : function(scope, $element, attrs) {
				scope.items = [];
				scope.source = [];
				scope.selected = [];
				scope.tags = {};
				var timer = null;
				
				$element.find('.flow-content').addClass('loading');

				scope.getItems = function() {
					var query = $('#news-tags_tag').val().toString().toLowerCase();

					if (query && query.length > 2) {
						var items = [];

						for(var i = 0; i < scope.items.length; i++) {
							if (scope.items[i].selected || scope.items[i].title.toLowerCase().indexOf(query) !== -1) {
								items.push(scope.items[i]);
							}
						}

						return items;
					} else {
						return scope.items;
					}
				};
				
				$element.on('keyup', '#news-tags_tag', function() {
					scope.$apply();
					/*
					var $inp = $(this), v = $inp.val();

					scope.$apply(function() {
						scope.items = [];
						
						if (v.length > 2) {
							for (var i in scope.source) {
								var item = scope.source[i];
								if (item.title.indexOf(v) >= 0 || item.selected) {
									scope.items.push(item);
								}
							}
						} else {
							for (var i in scope.source) {
								scope.items.push(scope.source[i]);
							}
						}
					});*/
				});
				
				scope.$watch('items', function() {
					clearTimeout(timer);
					timer = setTimeout(function() {
						for (var i in scope.selected) {
							var $inp = $element.find('input[value="'+scope.selected[i]+'"]');
							$inp.prop('checked', true);
							scope.tags[$inp.val()] = $inp.data('content');
							
							for(var j in scope.source) {
								if( j == scope.selected[i] ) {
									scope.source[j].selected = true;
								}
							}
						}
						$element.trigger('tagchange');
					}, 30);
				}, true);
				
				$element.on('click', '.tagsinput .tag', function() {
					var id = $(this).attr('id');
				
					scope.$apply(function() {
						// сброс выделеного флажка
						$element.find('li input[value="' + id +'"]').prop('checked',false);
						// сброс выделеного элемента
						scope.source[parseInt(id)].selected = false;
						// удаляем тег
						delete scope.tags[id];
						
						// удаляем ранее активный checkbox по которому проводился уже поиск
						if( scope.selected[id] ) {
							delete scope.selected[id];
						}
					});
					return false;
				});
				
				$element.on('change', 'input[type="checkbox"]', function() {
					var t = $(this), id = t.val();
					
					// сбрасываем строку поиска
					$('#news-tags_tag').val('').keyup();
					
					scope.$apply(function() {
						if( t.is(':checked') ) {
							// выделяем элемент
							scope.source[id].selected = true;
							// добавляем в пул тегов
							scope.tags[id] = t.data('content');
							
						} else {
							
							// удаляем тег
							delete scope.tags[id];
							// сброс выделения у элемента
							scope.source[id].selected = false;
							
							// удаляем ранее активный checkbox по которому проводился уже поиск
							if( scope.selected[id] ) {
								delete scope.selected[id];
							}
						}
					})
				});
				
				scope.$watch('tags', function() {
					var $tagsinput = $element.find('.tagsinput');
					
					// удаляем ранее созданые теги
					$tagsinput.find('span.tag').remove();
					
					// добавляем теги
					for(var n in scope.tags) {
						var tag = scope.tags[n];
						if (tag != null) {
							$tagsinput.prepend('<span class="tag" id="' + n + '"><span>' + tag + '&nbsp;&nbsp;</span><a href="#"></a></span>')
						}
					}
				}, true);
				
				$http({
					url : $element.data('themes-url'),
					method : 'GET'
				}).success(function(data) {
					$element.find('.flow-content').removeClass('loading');
					$inp = $element.find('.news-filter-inp');
					
					for(var i = 0; i < data.items.length; i++) {
						var source = data.items[i];
						source.title = source.title.replace(/[^а-яa-z0-9_\s]+/gi, '');
						scope.items.push(source);
						scope.source[source.id] = source;
					}
					
					scope.selected = jQuery.parseJSON(attrs.themesSelected);
					
					$timeout(function(){
						var scrollPane = $element.find(".scroll-pane"), scrollContent = $element.find(".scroll-content");
						
						var scrollbar = $element.find('#scroll-bar');
						scrollbar.slider({
							value : 100,
							orientation: 'vertical',
							slide: function(event, ui){
								if (scrollContent.height() > scrollPane.height()) {
									scrollContent.css("margin-top", -Math.round((ui.value-100) / 100 * (scrollPane.height() - scrollContent.height())) +
									"px");
								}
								else {
									scrollContent.css("margin-top", 0);
								}
							}
						})
					}, 20);
				}).error(function() {
					
				});
			}
		}
	}).directive('cancelFilter', function($location) {
		return {
			restrict : 'A',
			scope : true,
			
			link : function(scope, $element, attrs) {
				$element.on('click', function() {
					var id = attrs.cancelFilter;
					var params = $.deparam.querystring(window.location.href);
					var qr = { themes : [] };
					
					if( params.hasOwnProperty('themes') ) {
						for(var i = 0; i < params.themes.length; i++) {
							if( parseInt(params.themes[i], 10) !== parseInt(id, 10) ) {
								qr.themes.push(params.themes[i]);
							}
						}
					}
					var uri = window.location.pathname;
					if( qr.themes.length > 0 )
						uri = uri + '?' + $.param(qr);
					window.location = uri;
					return false;
				});
			}
		};
	});
