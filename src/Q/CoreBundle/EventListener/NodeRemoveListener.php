<?php

namespace Q\CoreBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Entity\Node;


/**
 * Очищение дочерних элементов при удалении ноды
 */
class NodeRemoveListener
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function get($service)
    {
        return $this->container->get($service);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Node) {
            foreach($args->getEntityManager()->getRepository('QCoreBundle:Node')->getChildren($entity, false, null, 'ASC', true) as $node) {
                foreach($node->getController()->getModules() as $module) {
                    $module = $this->get($module);
                    if (method_exists($module, 'onNodeRemove')) {
                        $module->onNodeRemove($node);
                    }
                }
            }
        }
    }
}
