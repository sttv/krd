<?php

namespace Krd\UserBundle\Modules;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Admin\AbstractModule;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\Entity\Widget;


/**
 * Модуль комментариев личного кабинета
 */
class CommentsModule extends AbstractModule
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    public function setSecurityContext(SecurityContext $sc)
    {
        $this->securityContext = $sc;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->securityContext) {
            if (($user = $this->securityContext->getToken()->getUser()) && ($user instanceof User)) {
                return $user;
            }
        }
    }

    /**
     * Список URL нодов с разрешенными комментариями
     * @return array<String>
     */
    protected function getAvailableNodes()
    {
        $result = array();

        $nodes = $this->em->createQuery('SELECT e.url FROM QCoreBundle:Node e WHERE e.active = 1 AND e.commentable = 1')->getResult();

        foreach ($nodes as $node) {
            $result[] = $node['url'];
        }

        return $result;
    }

    /**
     * Список последних комментариев
     * @return Paginator
     */
    public function getLastComments($count = 5)
    {
        $urls = $this->getAvailableNodes();

        if (!empty($urls)) {
            $query = $this->getRepository()->createQueryBuilder('e')
                ->andWhere('e.active = 1')
                ->andWhere('e.page IN (:pages)')
                ->addOrderBy('e.created', 'DESC')
                ->setParameter('pages', $urls)
                ->getQuery();
        } else {
            $query = $this->getRepository()->createQueryBuilder('e')
                ->andWhere('1 != 1')
                ->getQuery();
        }

        return Paginator::createFromRequest($this->request, $query, true, $count);
    }

    /**
     * Список последних комментариев страницы
     * @return Paginator
     */
    public function getPageLastComments($page, $count = 5, $like = false)
    {
        $qb = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->addOrderBy('e.created', 'DESC');

        if ($like) {
            $qb->andWhere('e.page LIKE :page')
                ->setParameter('page', $page.'%');
        } else {
            $hash = md5($page);

            $qb->andWhere('e.pageHash = :hash')
                ->setParameter('hash', $hash);
        }

        return Paginator::createFromRequest($this->request, $qb->getQuery(), true, $count);
    }

    /**
     * Список комментариев пользователя
     * @return Paginator
     */
    public function getMyComments($count = 5)
    {
        $urls = $this->getAvailableNodes();

        if ($user = $this->getUser()) {

            if (!empty($urls)) {
                $query = $this->getRepository()->createQueryBuilder('e')
                    ->andWhere('e.active = 1')
                    ->andWhere('e.page IN (:pages)')
                    ->andWhere('e.createdBy = :user')
                    ->addOrderBy('e.created', 'DESC')
                    ->setParameter('user', $user->getId())
                    ->setParameter('pages', $urls)
                    ->getQuery();
            } else {
                $query = $this->getRepository()->createQueryBuilder('e')
                    ->andWhere('1 != 1')
                    ->getQuery();
            }

            return Paginator::createFromRequest($this->request, $query, true, $count);
        } else {
            throw new \Exception('Not authorized');
        }
    }

    /**
     * Считает количество комментариев на странице
     * @return integer
     */
    public function getCountByPage($page, $like = false)
    {
        $qb = $this->getRepository()->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->andWhere('e.active = 1');

        if ($like) {
            $qb->andWhere('e.page LIKE :page')
                ->setParameter('page', $page.'%');
        } else {
            $hash = md5($page);

            $qb->andWhere('e.pageHash = :hash')
                ->setParameter('hash', $hash);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Считает количество комментариев текущего пользователя на странице
     * @return integer
     */
    public function getMyCountByPage($page, $like = false)
    {
        if ($user = $this->getUser()) {
            $qb = $this->getRepository()->createQueryBuilder('e')
                ->select('COUNT(e.id)')
                ->andWhere('e.createdBy = :user')
                ->andWhere('e.active = 1')
                ->setParameter('user', $user->getId());

            if ($like) {
                $qb->andWhere('e.page LIKE :page')
                    ->setParameter('page', $page.'%');
            } else {
                $hash = md5($page);

                $qb->andWhere('e.pageHash = :hash')
                    ->setParameter('hash', $hash);
            }

            return $qb->getQuery()->getSingleScalarResult();
        } else {
            throw new \Exception('Not authorized');
        }
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    public function renderContent()
    {
        return $this->twig->render('KrdUserBundle:Module:comments/index.html.twig');
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget(Widget $widget)
    {
        $list = $this->getMyComments(2);
        $isMyComments = true;

        if (count($list) == 0) {
            $list = $this->getLastComments(2);
            $isMyComments = false;
        }

        return $this->twig->render('KrdUserBundle:Module:comments/widget.html.twig', array('list' => $list, 'isMyComments' => $isMyComments, 'widget' => $widget));
    }

    /**
     * Блок для "Где написать комментарии"
     */
    public function renderWhereToWrite()
    {
        $urlsRaw = $this->em->createQuery('SELECT DISTINCT c.page FROM KrdCommentsBundle:Comment c WHERE c.active = 1')->getResult();
        $urlsRaw2 = $this->em->createQuery('SELECT DISTINCT n.url FROM QCoreBundle:Node n WHERE n.active = 1 AND n.commentable = 1')->getResult();

        $urlsRaw = array_merge($urlsRaw, $urlsRaw2);

        $urls = array();

        foreach ($urlsRaw as $item) {
            $url = isset($item['page']) ? $item['page'] : $item['url'];

            preg_match_all('/^(\/[^\/]+\/[^\/]+)/', $url, $matches);

            if (isset($matches[0]) && isset($matches[0][0])) {
                $urls[] = $matches[0][0].'/';
            }
        }

        $urls = array_unique($urls);
        $urlhashes = array_map('md5', $urls);

        $list = array();

        $urls = $this->getAvailableNodes();

        if (!empty($urlhashes) && !empty($urls)) {
            $nodesRaw = $this->em
                ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.active = 1 AND n.urlhash IN (:urlhashes) AND n.url IN (:urls) ORDER BY n.left')
                ->setParameter('urlhashes', $urlhashes)
                ->setParameter('urls', $urls)
                ->getResult();

            foreach ($nodesRaw as $node) {
                $parent = $node->getParent();

                $list[$parent->getId()]['node'] = $parent;
                $list[$parent->getId()]['childs'][] = $node;
            }
        }

        return $this->twig->render('KrdUserBundle:Module:comments/where-write.html.twig', array('list' => $list, 'isMy' => false));
    }

    /**
     * Блок для "Где написать комментарии" только "мои"
     */
    public function renderWhereMy()
    {
        $urlsRaw = $this->em->createQuery('SELECT DISTINCT c.page FROM KrdCommentsBundle:Comment c WHERE c.active = 1')->getResult();

        $urls = array();

        foreach ($urlsRaw as $item) {
            $url = isset($item['page']) ? $item['page'] : $item['url'];

            preg_match_all('/^(\/[^\/]+\/[^\/]+)/', $url, $matches);

            if (isset($matches[0]) && isset($matches[0][0])) {
                $urls[] = $matches[0][0].'/';
            }
        }

        $urls = array_unique($urls);
        $urlhashes = array_map('md5', $urls);

        $list = array();

        $urls = $this->getAvailableNodes();

        if (!empty($urlhashes) && !empty($urls)) {
            $nodesRaw = $this->em
                ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.active = 1 AND n.urlhash IN (:urlhashes) AND n.url IN (:urls) ORDER BY n.left')
                ->setParameter('urlhashes', $urlhashes)
                ->setParameter('urls', $urls)
                ->getResult();

            foreach ($nodesRaw as $node) {
                $parent = $node->getParent();

                $list[$parent->getId()]['node'] = $parent;
                $list[$parent->getId()]['childs'][] = $node;
            }
        }

        return $this->twig->render('KrdUserBundle:Module:comments/where-write.html.twig', array('list' => $list, 'isMy' => true));
    }
}
