<?php

namespace Q\FilesBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\FilesBundle\Entity\Video;
use Q\FilesBundle\Entity\VideoFormat;

/**
 * События для Video
 */
class VideoSubscriber implements EventSubscriber
{
    protected $uploadDir;

    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postRemove',
        );
    }

    /**
     * Удаление видео файлов
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Video) {
            $file = $entity->getFile();
            @unlink($this->uploadDir.$file['path']);
        } elseif ($entity instanceof VideoFormat) {
            $file = $entity->getFile();
            @unlink($this->uploadDir.$file['path']);
        }
    }
}

