<?php

namespace Krd\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Виджет в личном кабинете
 *
 * @ORM\Entity
 * @ORM\Table(name="user_widget",
 *      indexes={
 *          @ORM\Index(name="sort", columns={"sort"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="created", columns={"created"}),
 *          @ORM\Index(name="updated", columns={"updated"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     submit={"submit", {"label"="Отправить"}}
 * )
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 */
class Widget
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     * @Form\Field("text", label="Заголовок")
     */
    private $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     * @Form\Field("text", label="Модуль")
     */
    private $module;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Form\Field("textarea", label="Текст-заглушка", required=false, attr={"class"="tinymce", "data-theme"="advanced"})
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     * @Form\Field("checkbox", label="Активность", required=false)
     */
    private $active = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Widget
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Widget
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Widget
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return Widget
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Widget
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Widget
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Widget
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Widget
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Widget
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
}