<?php

namespace Krd\AdministrationBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;

/**
 * Модуль списка людей структуры администрации
 */
class Human extends AbstractModule
{
    protected $contentModule;

    public function setContentModule(AbstractModule $contentModule)
    {
        $this->contentModule = $contentModule;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdAdministrationBundle:Admin:Module/human.html.twig');
    }

    /**
     * Текущий человек
     * @return Human
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Построение древовидного списка
     * @param  integer $root
     * @return array
     */
    public function getItems($root)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array('list' => array(), 'items' => array(), 'rootNode' => $rootNode);
        }

        $struct = $this->em->createQueryBuilder()
            ->select('n')
            ->from('QCoreBundle:Node', 'n')
            ->andWhere('n.parent = :parent')
            ->andWhere('n.active = 1')
            ->innerJoin('n.controller', 'c', 'WITH', 'c.id = :samectrl')
            ->orderBy('n.left', 'ASC')
            ->setParameters(array(
                'parent' => $rootNode->getId(),
                'samectrl' => $rootNode->getController()->getId(),
            ))
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        foreach($struct as $i => $node) {
            $items = $this->getRepository()->createQueryBuilder('d')
                ->andWhere('d.active = 1')
                ->andWhere('d.parent = :parent')
                // ->andWhere('d.parentHuman IS NULL')
                ->orderBy('d.sort')
                ->setParameter('parent', $node->getId())
                ->getQuery()
                    ->useResultCache(true, 15)
                    ->getResult();

            if (!empty($items)) {
                $node->setCustomData('items', $items);
            } else {
                unset($struct[$i]);
            }
        }

        $itemsQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            // ->andWhere('d.parentHuman IS NULL')
            ->orderBy('d.sort')
            ->setParameter('parent', $rootNode->getId())
            ->getQuery();

        $items = $itemsQuery->useResultCache(true, 15)->getResult();

        return array('list' => $struct, 'items' => $items, 'rootNode' => $rootNode);
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItems($this->node->getNode()->getId());
        $result['content'] = $this->contentModule->getRepository()->findOneByParent($this->node->getNode()->getId());

        return $this->twig->render('KrdAdministrationBundle:Module:human/list.html.twig', $result);
    }

    /**
     * Подробная страница вакансии
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdAdministrationBundle:Module:human/detail.html.twig', array('item' => $this->getCurrent()));
    }
}
