<?php

namespace Krd\AppealBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Krd\AppealBundle\Entity\Appeal;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Doctrine\Common\Collections\Criteria;

use Q\FilesBundle\Entity\File;

/**
 * Обращения интернет приемной
 */
class AppealController extends Controller implements ActiveSecuredController
{
    /**
     * Форма обращения
     *
     * @Route("/")
     * @Template()
     */
    public function formAction(Request $request)
    {

        // TODO: тестирование формы
        if(isset($_GET['dddd'])) {

            setcookie('dddd', 1, (time() + 1800), '/', '.krd.ru');
            header('Location: /internet-priemnaya/priyom-ofitsialnykh-obrascheniy-grazhdan/obraschenie/');
            die;
        }

        if ($this->get('session')->getFlashBag()->has('response')) {
            return;
        }

        $parentNode = $this->get('qcore.routing.current')->getNode()->getParent();
    }

    private function fileUpload(Request $request) {

        $files = $_FILES['appealFiles'];

        // http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
        $allowMime = array(
            'image/jpeg',
            'text/plain',
            'application/msword',
            'application/rtf',
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-powerpoint',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'application/pdf',
            'image/bmp',
            'image/png',
            'image/tiff',
            'image/x-pcx',
        );
        $maxFileSize = 1024*1024*5;
//        $maxFileSize = 5;

        Header('Content-Type: application/json; charset=UTF8');

        $files = isset($files[0]) ? $files : array($files);

        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/uploads/files/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';

        $result = array();

        foreach($files as $file) {

            $_i = pathinfo($file['name']);

            $fileName = uniqid($_i['basename'].'-').'.'.$_i['extension'];

            $upFile = new UploadedFile($file['tmp_name'], $file['name']);

            try {
            if($upFile->getSize() > $maxFileSize) {

                echo json_encode(array(
                    'error' => 'Превышен размер файла'
                ));
                die;
            }
            if(!in_array($upFile->getMimeType(), $allowMime)) {

                echo json_encode(array(
                    'error' => 'Данный тип файлов не поддерживается'
                ));
                die;
            }
            } catch (\Exception $e) {

                var_dump($e->getMessage());die;
            }
            $upFile->move($filePath, $fileName);

            $_file = new File();

            $_file->setTitle($_i['basename']);

            $_file->setDate(new \DateTime);

            $_file->setFile($upFile);

            // сохраняем данные о файле в базу
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($_file);
            $doctrine->flush();

            $f = $_file->getFile();

            rename($filePath.$fileName, $filePath.basename($f['path']));

            $result = array(
                'id' => $_file->getId(),
                'files' => array($file)
            );
        }

        echo json_encode($result);

        die;
    }

    /**
     * Сабмит формы обращения
     *
     * @Route("/ajax/krd/appeal/appeal/submit/", name="krd_appeal_appeal_submit_form", schemes={"https"})
     * @Method({"POST"})
     */
    public function submitAction(Request $request)
    {
        if(isset($_GET['file'])) {

            $this->fileUpload($request);
            die;
        }

        $data = $request->get('appeal');
        $options = array(
            'validation_groups' => array('Default'),
        );

        if ($data['replyType'] == 2) {
            $options['validation_groups'][] = 'address';
        }

        if ($request->get('draft')) {
            $options['validation_groups'] = array('non-exists');
        }

        $formBuilder = $this->get('krd.appeal.modules.appeal')->getAppealFormBuilder(new Appeal(), $options);
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            $this->get('session')->getFlashBag()->set('appeal.form.data', $request->request->all());
            $errors = $this->get('qcore.form.helper.errors')->getErrors($form);
            return $this->submitResponse(array('success' => false, 'errors' => $errors, 'reason' => 'validation'));
        }

        $appeal = $form->getData();

        if (($appeal instanceof Appeal) && $appeal->getMessage()) {

            $appeal->setDate(new \DateTime());

            if ($request->get('draft')) {

                $appeal->setStatus(Appeal::STATUS_DRAFT);
            } else {

                $appeal->setStatus(Appeal::STATUS_NEW);
            }

            $requestArr = (array)$request->server->all();

            if (isset($requestArr['HTTP_COOKIE'])) {
                unset($requestArr['HTTP_COOKIE']);
            }

            $appeal->setRequest($requestArr);
            $appeal->setIp($request->getClientIp());

            switch((int)$appeal->getReplyType()) {
                case 1:
                    $appeal->setReplyType('На электронную почту');
                    break;

                case 2:
                    $appeal->setReplyType('На почту');
                    break;
            }

            if ($request->get('mobile')) {
                $appeal->setGotFrom('Мобильное приложение');
            }

            if($appeal->hasAppealFiles()) {

                try {

                    $filesIds = $appeal->getAppealFiles();

                    $appeal->clearAppealFiles();

                    foreach ($filesIds as $f) {

                        $tryFile = $this->getDoctrine()->getManager()->getRepository('QFilesBundle:File')->find($f);

                        if(!empty($tryFile) && $tryFile instanceof File) {

                            $appeal->addAppealFiles($tryFile);
                        }
                    }
                } catch (\Exception $e) {

                    var_dump($e->getMessage(), __LINE__);die;
                } catch (\RuntimeException $e) {

                    var_dump($e->getMessage(), __LINE__);die;
                }
            }

            try {

                $this->getDoctrine()->getManager()->persist($appeal);
//                if(!isset($_COOKIE['dddd']))
                $this->getDoctrine()->getManager()->flush();
            } catch (\Exception $e) {

                var_dump($e->getMessage(), $e->getFile(), $e->getLine());die;
            } catch (\RuntimeException $e) {

                var_dump($e->getMessage(), $e->getFile(), $e->getLine());die;
            }

            $this->get('session')->getFlashBag()->set('appeal.form.data', '');

            if ($request->get('draft')) {
                $date = new \DateTime();
                $date->modify('-5 minutes');
                $removeQuery = $this->getDoctrine()->getRepository('KrdAppealBundle:Appeal')->createQueryBuilder('e')
                    ->andWhere('e.id != :created_id')
                    ->andWhere('e.createdBy = :user')
                    ->andWhere('e.date > :date')
                    ->andWhere('e.status = :status')
                    ->andWhere('e.parent = :parent')
                    ->setParameter('created_id', $appeal->getId())
                    ->setParameter('date', $date)
                    ->setParameter('user', $this->get('security.context')->getToken()->getUser()->getId())
                    ->setParameter('status', Appeal::STATUS_DRAFT)
                    ->setParameter('parent', $appeal->getParent()->getId())
                    ->getQuery();

                foreach ($removeQuery->iterate() as $rAppeal) {
                    $this->getDoctrine()->getManager()->remove($rAppeal[0]);
                }

                $this->getDoctrine()->getManager()->flush();

                return $this->submitResponse(array('success' => true, 'successText' => 'Черновик сохранен'));
            } else {

                return $this->submitResponse(array('success' => true, 'successText' => $appeal->getDestination()->getSuccess()));
            }
        } else {
            if ($request->get('draft')) {
                return $this->submitResponse(array('success' => false, 'error' => 'Нечего сохранять, нужно заполнить текст обращения'));
            } else {
                return $this->submitResponse(array('success' => false, 'error' => 'Нужно заполнить текст обращения', 'reason' => 124));
            }
        }
    }

    /**
     * Формирование ответа
     *
     * @param  array $data
     *
     * @return mixed
     */
    protected function submitResponse($data)
    {
        $request = $this->get('request');
        $referer = $request->server->get('HTTP_REFERER');

        if ($request->request->get('noajax') == 1 && !empty($referer)) {
            $this->get('session')->getFlashBag()->set('response', $data);

            return $this->redirect($referer . '#flash-response');
        }

        return new JsonResponse($data);
    }
}
