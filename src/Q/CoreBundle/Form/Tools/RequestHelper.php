<?php

namespace Q\CoreBundle\Form\Tools;

use Symfony\Component\HttpFoundation\Request;


/**
 * Дополнительная обработка запросов
 */
class RequestHelper
{
    /**
     * Парсинг входящих данных из <select multiple name="name[]" />
     * На выходе получается многомерный массив, так что приходится делать так
     *
     * @param  Request $request
     * @param  array $fields
     * @param  string $formname
     */
    public function parseMultipleSelect(Request $request, array $fields, $formname = null)
    {
        foreach($fields as $field) {
            if ($formname) {
                $formdata = $request->request->get($formname);
                $value = isset($formdata[$field]) ? $formdata[$field] : false;
            } else {
                $value = $request->request->get($field);
            }

            if (is_array($value)) {
                $result = array();
                $isFalse = false;

                foreach($value as $i => $val) {
                    if (is_array($val) && isset($val[$i])) {
                        $result[] = array_pop($val);
                    } else {
                        $isFalse = true;
                    }
                }

                if (!$isFalse) {
                    if ($formname) {
                        $formdata[$field] = $result;
                        $request->request->set($formname, $formdata);
                    } else {
                        $request->request->set($field, $result);
                    }
                }
            }
        }
    }
}
