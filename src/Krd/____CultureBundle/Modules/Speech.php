<?php

namespace Krd\GlavaBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\VideoFormat;
use Q\FilesBundle\Manager\ImageCacheUrl;
use Krd\GlavaBundle\Entity\Video as VideoEntity;


/**
 * Модуль глава-видео
 */
class Speech extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdGlavaBundle:Admin:Module/speech.html.twig');
    }
	
	public function renderContent()
	{
		$result = $this->getItemsList();

        return $this->twig->render('KrdGlavaBundle:Module:speech/list.html.twig', array('list' => $result));
	}
	
	public function renderDetailContent()
	{
		return $this->twig->render('KrdGlavaBundle:Module:speech/detail.html.twig', array('item' => $this->getCurrentSpeech()));
	}
	
	private function getCurrentSpeech()
	{
		return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
	}
	
	/**
     * Список дат за которые существуют новости
     * @param  integer[optional] $parentId
     * @return array
     */
    public function getDatesList($parentId = null) {
        if (is_null($parentId)) {
            $dates = $this->getRepository()->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM __CLASS__ n WHERE n.active = 1 GROUP BY formatted")
            ->useResultCache(true, 15)
            ->getResult();
        } else {
            $dates = $this->getRepository()->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM __CLASS__ n WHERE n.active = 1 AND n.parent = :parent GROUP BY formatted")
            ->setParameter('parent', $parentId)
            ->useResultCache(true, 15)
            ->getResult();
        }

        foreach($dates as &$date) {
            $date = $date['date'];
        }
        unset($date);

        return $dates;
    }

	
	 /**
     * Календарь новостей в сайдбаре
     */
    public function renderAsideCalendar()
    {
        $dates = $this->getDatesList($this->node->getNode()->getId());

        if ($date = $this->request->get('date')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
        }

        return $this->twig->render('KrdGlavaBundle:Module:speech/aside.calendar.html.twig', array(
                'availDates' => $dates,
                'nowDate' => $date,
            ));
    }
	
	/**
     * Список речей для текущей страницы
     * @return array
     */
    protected function getItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());
			
		if ($date = $this->request->get('date')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') = :date');
            $queryBuilder->setParameter('date', $date->format('Y-m-d'));
        }
		
		if ($date = $this->request->get('date_begin')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') >= :date1');
            $queryBuilder->setParameter('date1', $date->format('Y-m-d'));
        }
		
		if ($date = $this->request->get('date_end')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') <= :date2');
            $queryBuilder->setParameter('date2', $date->format('Y-m-d'));
        }
		
        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }
}
