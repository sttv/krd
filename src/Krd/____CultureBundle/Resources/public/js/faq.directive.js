/**
 * @author User
 */
angular.module('glava.faq', [])
	.directive('faqItem', function($timeout) {
		return {
			restrict : 'A',
			scope : true,
			
			link : function(scope, $element, attrs) {
				scope.$zone = $element.parent();
				
				var $faq = scope.$zone.find('.faq-title');
				$timeout(function() {
					var $container = $($faq.data('id'));
					$element.find('#ya' + attrs.faqId+' a').each(function() {
						var url = 'http://share.yandex.ru/go.xml?service='+$(this).data('service')+'&url='+window.location.href+'detail-'+attrs.faqId+'.html';
						url += '&title='+document.title;
						$(this).attr('href', url);
					});
				}, 40);
				
				
				$faq.on('click', function(e) {
					e.preventDefault();
					var id = $(this).data('id');
					var $container = $(id);
					
					$(this).addClass('active');
					$(this).prev().addClass('active');
					$container.slideDown(500);
					return false;
				});
				
				scope.$zone.find('.close').on('click', function() {
					var $parent = $(this).parent();
					var $title = $('[data-id="#' + $parent.attr('id')+'"]');
					$title.removeClass('active');
					$title.prev().removeClass('active');
					$parent.slideUp(400);
					return false;
				})
			}
		}
	});
