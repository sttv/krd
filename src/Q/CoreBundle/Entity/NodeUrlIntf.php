<?php

namespace Q\CoreBundle\Entity;


/**
 * Интерфейс для авто-генерации ссылок на подробную страницу
 */
interface NodeUrlIntf
{
    /**
     * Получение родителя. Должен возвращаться Node
     */
    public function getParent();

    /**
     * Имя роута детального просмотра
     */
    public function getDetailRoute();

    /**
     * Уникальное имя сущности
     */
    public function getName();

    /**
     * Установка URL
     */
    public function setUrl($url);
}
