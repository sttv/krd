<?php

namespace Krd\ApiBundle\GCM;

use A\NotificationsBundle\Entity\GCMToken;
use A\NotificationsBundle\Entity\GCMUser;
use DateTime;
use Doctrine\ORM\EntityManager;

/**
 * Управление GCM
 */
class GCMManager
{
    private $URL_SEND = 'https://gcm-http.googleapis.com/gcm/send';
    private $apiKey;
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct($apiKey, EntityManager $em)
    {
        $this->apiKey = $apiKey;
        $this->em = $em;
    }

    /**
     * Отпарвка сообщеняи всем зарегистрированным устройствам
     */
    public function sendToAll(array $data)
    {
        $tokens = array();

        $lastUpdated = new DateTime();
        $lastUpdated->modify('-7 days');

        $res = $this->em->createQuery('SELECT e.token FROM KrdApiBundle:GCMToken e WHERE e.updated >= :last_updated')
            ->setParameter('last_updated', $lastUpdated)
            ->getResult();

        foreach($res as $item){
            $tokens[] = $item['token'];

            if (count($tokens) > 300) {
                $this->sendGCMMessage($tokens, $data);
                $tokens = array();
            }
        }

        if (!empty($tokens)) {
            $this->sendGCMMessage($tokens, $data);
        }
    }

    /**
     * Отправка сообщений
     *
     * @param array $regIds
     * @param array $data
     * @return array|string
     */
    public function sendGCMMessage(array $regIds, array $data)
    {
        $post = array(
            'registration_ids' => $regIds,
            'data' => $data,
        );

        $headers = array(
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URL_SEND);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'GCM error: ' . curl_error($ch);
        }
        curl_close($ch);

        if ($res = json_decode($result, true)) {
            return $res;
        }

        return $result;
    }
}