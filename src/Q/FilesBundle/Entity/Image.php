<?php

namespace Q\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Загружаемое изображение
 *
 * @ORM\Entity(repositoryClass="Q\FilesBundle\Repository\ImageRepository")
 * @ORM\Table(name="image",
 *     indexes={
 *          @ORM\Index(name="sort", columns={"sort"}),
 *          @ORM\Index(name="main", columns={"main"})
 *     }
 * )
 *
 * @FileStore\Uploadable
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Название"}},
 *     main={"checkbox", {"label"="Название"}},
 *     file={"file", {"label"="Файл"}}
 * )
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     */
    private $title = '';

    /**
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     */
    private $main = false;

    /**
     * Загруженный файл
     * @ORM\Column(type="array")
     * @FileStore\UploadableField(mapping="images")
     *
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/gif", "image/bmp"})
     *
     * @JMS\Expose
     */
    private $file;

    /**
     * @ORM\OneToMany(targetEntity="ImageCache", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $cached;


    /**
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Список возможных размеров
     * @JMS\Expose
     */
    private $size = array();

    public function setId($id)
    {
        $this->id = (int)$id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = (string)$title;
    }

    public function getMain()
    {
        return $this->main;
    }

    public function setMain($main)
    {
        $this->main = (boolean)$main;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (empty($this->title)) {
            $this->title = $file->getClientOriginalName();
        }
    }

    public function setFileArray(array $file)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function hasFile()
    {
        return !empty($this->file);
    }

    public function setSize(array $size)
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }

    /**
     * Возвращает ссылку на нужный размер
     * @param  string $size
     * @return string
     */
    public function getUrl($size)
    {
        return $this->size[$size];
    }

    public function getCached()
    {
        return $this->cached ?: $this->cached = new ArrayCollection();
    }

    /**
     * Ассоциативный массив кешированных изображений
     */
    public function getCachedByName()
    {
        $result = array();

        foreach($this->getCached() as $img) {
            $result[$img->getName()] = $img;
        }

        return $result;
    }

    public function clearCached()
    {
        foreach($this->getCached() as $imageCache) {
            $this->removeCached($imageCache);
        }
    }

    public function addCached(ImageCache $imageCache)
    {
        if (!$this->getCached()->contains($imageCache)) {
            $this->getCached()->add($imageCache);
            $imageCache->setParent($this);
        }
    }

    public function removeCached(ImageCache $imageCache)
    {
        if ($this->getCached()->contains($imageCache)) {
            $imageCache->clearParent();
            $this->getCached()->removeElement($imageCache);
        }
    }

    public function setSort($sort)
    {
        $this->sort = (int)$sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}
