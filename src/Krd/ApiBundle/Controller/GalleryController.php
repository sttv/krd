<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Галереи
 *
 * @Route("/gallery")
 */
class GalleryController extends Controller
{
    /**
     * Список галерейных разделов сайта
     *
     * @Route("/nodes.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function nodesAction()
    {
        $nodes = $this->getDoctrine()->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1 AND e.controller = 5')
            ->addOrderBy('e.left')
            ->getQuery()
                ->getResult();

        $result = array();

        foreach ($nodes as $node) {
            $result[] = array('id' => $node->getId(), 'title' => $node->getTitle(), 'url' => $node->getUrl(true));
        }

        return $result;
    }

    /**
     * Список галерей
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdGalleryBundle:Gallery')
            ->createQueryBuilder('n')
//            ->leftJoin('n.images', 'i')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->setParameter('parent', $parent)
            ->orderBy('n.date', 'DESC');

        $result = Paginator::createFromRequest($request, $queryBuilder->getQuery());

        foreach ($result->getItems() as $item) {
            $item->updateImagesCount();
            $mainImage = $item->getMainImage();

            foreach ($item->getImages() as $image) {
                if ($image->getId() != $mainImage->getId()) {
                    $item->removeImages($image);
                }
            }
        }

        return $result;
    }

    /**
     * Элемент галереи
     *
     * Обязательные параметры:
     *  id - ID элемента
     *
     * @Route("/item/{id}.{_format}", requirements={"id"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function itemAction(Request $request, $id)
    {
        $items = $this->getDoctrine()->getRepository('KrdGalleryBundle:Gallery')
            ->createQueryBuilder('n')
//            ->leftJoin('n.images', 'i')
            ->andWhere('n.active = 1')
            ->andWhere('n.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        if (isset($items[0])) {
            return $items[0];
        }
    }
}
