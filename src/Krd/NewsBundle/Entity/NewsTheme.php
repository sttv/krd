<?php

namespace Krd\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Тема новости
 *
 * @ORM\Entity
 * @ORM\Table(name="news_theme",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="disabled", columns={"disabled"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Заголовок"}},
 *     disabled={"checkbox", {
 *         "label"="Не использовать",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class NewsTheme
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Тема
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     *
     * @Gedmo\Versioned
     */
    private $title;

    /**
     * Список новостей
     *
     * @ORM\ManyToMany(targetEntity="News", mappedBy="themes")
     */
    private $news;
	
	/**
	 * Количество новостей
	 * @JMS\Expose
	 * @JMS\Type("integer")
	 */
	private $newscount;

    /**
     * Тег отключен для выбора в админке
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $disabled = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getNews()
    {
        return !empty($this->news) ? $this->news : new ArrayCollection();
    }
	
	public function getNewsCount()
	{
		return $this->newscount;
	}
	
	public function setNewsCount($count)
	{
		$this->newscount = (int)$count;
	}

    public function getDisabled()
    {
        return $this->disabled;
    }

    public function setDisabled($disabled)
    {
        $this->disabled = (boolean)$disabled;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}


