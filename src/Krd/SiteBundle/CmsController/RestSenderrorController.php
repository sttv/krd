<?php

namespace Krd\SiteBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер Senderror
 *
 * @Route("/rest/krd/site/senderror")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestSenderrorController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdSiteBundle:Senderror';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_site_senderror_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_site_senderror_edit';
    }

    /**
     * Список
     * @Route("/", name="cms_rest_krd_site_senderror", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        $qb->addOrderBy('e.created', 'DESC');
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_site_senderror_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_site_senderror_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_site_senderror_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_site_senderror_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }
}
