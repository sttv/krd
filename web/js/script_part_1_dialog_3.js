/**
 * Всплывающие окна
 */
angular.module('dialog', [])
    /**
     * Кнопки показа всплывающиего окна
     */
    .directive('dialogShow', ['$compile', '$timeout', function($compile, $timeout) {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                var template = attrs.dialogShow;

                $element.on('click', function() {
                    var $dialog = $('<div dialog-window="'+template+'"></div>');
                    $dialog.appendTo('body');

                    $timeout(function() {
                        $compile($dialog)($scope);
                    });
                });
            },

            controller: function($scope, $element) {
            }
        }
    }])

    /**
     * Окно диалога
     */
    .directive('dialogWindow', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: true,
            template: '<div class="dialog">'
                        + '<div class="icon-close close-btn" ng-click="hide()"></div>'
                        + '<div class="w"></div>'
                    + '</div>',
            replace: true,

            compile: function($element, attrs) {
                $element.find('.w').html($('#'+attrs.dialogWindow).html());

                return function($scope, $element) {
                    $(document).ready($scope.relocate);
                    $(window).load($scope.relocate);
                    $(window).resize($scope.relocate);
                    $timeout($scope.relocate, 100);

                    $scope.$overlay = $('<div class="dialog-overlay"></div>');
                    $scope.$overlay.appendTo('body');
                    $scope.$overlay.on('click', function() {
                        $scope.hide();
                    });

                    $element.hide();

                    $scope.show();
                }
            },

            controller: function($scope, $element) {
                /**
                 * Обновляем позиционирование на экране
                 */
                $scope.relocate = function() {
                    $element.css({
                        marginLeft: -$element.width() / 2,
                        marginTop: -$element.height() / 1.6
                    })
                };

                $scope.show = function() {
                    $element.stop(true, true).css({opacity:1}).fadeIn(200);
                    $scope.$overlay.stop(true, true).css({display:'block', opacity:0}).animate({opacity:0.5}, function() {
                        $timeout($scope.relocate);
                    });
                };

                $scope.hide = function() {
                    $element.stop(true, true).fadeOut(200);
                    $scope.$overlay.stop(true, true).fadeOut(200, function() {
                        $scope.$overlay.remove();
                        $element.remove();
                    });
                };
            }
        }
    }])
;
