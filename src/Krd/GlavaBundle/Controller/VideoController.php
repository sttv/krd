<?php

namespace Krd\GlavaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Видео
 */
class VideoController extends Controller implements ActiveSecuredController
{
    /**
     * Список видео
     *
     * @Route("/")
     * @Template()
     */
	// @Secure(roles="ROLE_CMS")
    public function listAction()
    {
    }
	
	/**
     * Список альбомов для постраничного вывода
     * @Route("/ajax/glava/video/list.json", name="ajax_glava_video_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
    	$queryBuilder = $this->get('krd.glava.modules.video')->getRepository()
            ->createQueryBuilder('v')
            ->andWhere('v.active = 1')
            ->andWhere('v.parent = :parent')
            ->orderBy('v.date', 'DESC')
            ->setParameter('parent', $request->get('parent'));
		return Paginator::create($queryBuilder->getQuery(), $request->get('page', 1) + 2, 3, true);	
        //return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 3);
    }
	
	/**
	 * Загрузка видео
	 * @Route("/source-{id}.{_format}", requirements={"id"="^\d+$", "_format"="json"})
	 * @Method({"GET"})
	 */
	public function sourceAction($id)
	{
		$items = $this->get('krd.glava.modules.video')->getRepository()
            ->createQueryBuilder('v')
            ->andWhere('v.active = 1')
            ->andWhere('v.id = :id')
            ->orderBy('v.date', 'DESC')
            ->setParameter('id', $id)
			->getQuery()
            ->getResult();
		
		return $items[0];
	}
	
	/**
	 * Загрузка видео
	 * @Route("/detail-{id}.{_format}", requirements={"id"="^\d+$", "_format"="html"})
	 * @Method({"GET"})
	 */
	public function detailAction(Request $request)
	{
		$item = $this->getDoctrine()->getManager()->find('KrdGlavaBundle:Video', $request->get('id'));
		
		return $this->render('KrdGlavaBundle:Module:video/detail.html.twig', array('item' => $item));
	}
}