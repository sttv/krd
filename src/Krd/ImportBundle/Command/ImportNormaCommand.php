<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Импорт документов с http://old.krd.ru/www/norma.nsf/BaseSearch
 */
class ImportNormaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:norma')
            ->setDescription('Import normas from http://old.krd.ru/www/norma.nsf/BaseSearch')
            ->addArgument('destination', InputArgument::REQUIRED, 'Destination structure ID')
            ->addOption('update', null, InputOption::VALUE_NONE, 'If set, the existing normas will be updated')
            ->setHelp(<<<EOT
                      The <info>krd:import:structure</info> imports simple page from old krd.ru site to new cms.
EOT
                    );
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('destination')) {
            $destination = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please enter a destination structure ID: ',
                function($destination) {
                    if (empty($destination)) {
                        throw new \Exception('Can not be empty');
                    }

                    return $destination;
                }
            );

            $input->setArgument('destination', $destination);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $destinationID = $input->getArgument('destination');

        if (!is_numeric($destinationID)) {
            throw new \Exception('ID must be numeric');
        }

        $destinationNode = $this->getManager()->find('QCoreBundle:Node', $destinationID);

        if (!$destinationNode) {
            throw new Exception('Distination node not found');
        }

        $output->writeln('<info>------------------------------ Import start at '.date('H:i:s').' ------------------------------</info>');

        $output->writeln('Import to [<comment>'.$destinationNode->getUrl(true).'</comment>]');

        $this->getContainer()->get('krd_import.listener.normacache')->disable();
        $this->getContainer()->get('krd_import.importer.norma')->setOutput($output);
        $this->getContainer()->get('krd_import.importer.norma')->importAll($destinationNode, $input->getOption('update'));

        $output->writeln('<info>----------------------------- Import complete at '.date('H:i:s').' ----------------------------</info>');
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
