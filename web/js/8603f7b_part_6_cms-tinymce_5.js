/**
 * TinyMCE для админки
 */
angular.module('cms.tinemce', [])
    /**
     * Инициализация tinyMCE
     */
    .directive('tinymce', ['$timeout', function($timeout) {
        var generatedIds = 0;

        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                attrs.$set('id', 'tiny-mce-' + generatedIds++);

                $timeout(function() {
                    var options = angular.extend(window.tinyMCEConfig, {
                    });

                    initTinyMCE(options, $element);
                });
            }
        };
    }])
;
