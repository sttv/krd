<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Подписки в личном кабинете
 */
class SubscribeController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/", name="krd_user_subscribes")
     * @Template()
     */
    public function listAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Элемент подписки
     *
     * @Route("/{id}.html", name="krd_user_subscribes_item", requirements={"id"="^\d+$"})
     * @Template()
     */
    public function detailAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($item = $this->get('krd.user.modules.subscribe')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Информационная подписка за '.$item->getDate()->format('d.m.Y'), 'url' => '#'));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/list.json", name="krd_user_subscribes_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.user.modules.subscribe')->getItemsList();
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
