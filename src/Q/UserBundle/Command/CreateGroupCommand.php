<?php

namespace Q\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Q\UserBundle\Entity\UserGroup;

/**
 * Создание группы пользователей
 */
class CreateGroupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('q:user:group')
            ->setDescription('Create or update existing user group')
            ->setDefinition(array(
                new InputArgument('name', InputArgument::REQUIRED, 'Group name'),
                new InputArgument('roles', InputArgument::IS_ARRAY, 'Roles'),
            ))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groupName = $input->getArgument('name');
        $roles = $input->getArgument('roles');

        $groupRepository = $this->getDoctrine()->getEntityManager()->getRepository('QUserBundle:UserGroup');

        if (!($group = $groupRepository->findOneByName($groupName))) {
            $group = new UserGroup();
            $group->setName($groupName);
            $this->getDoctrine()->getEntityManager()->persist($group);
        }

        $group->setRoles($roles);

        $output->writeln($groupName);
        $output->writeln($roles);
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('name')) {
            $name = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a group name:',
                function($name) {
                    if (empty($name)) {
                        throw new \Exception('Group name can not be empty');
                    }

                    return $name;
                }
            );
            $input->setArgument('name', $name);
        }

        if (!$input->getArgument('roles')) {
            $roles = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Roles list:',
                function($roles) {
                    if (empty($roles)) {
                        throw new \Exception('Email can not be empty');
                    }

                    return $roles;
                }
            );
            $input->setArgument('roles', $roles);
        }
    }
}
