<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль карты сайта
 */
class Sitemap extends AbstractModule
{
    public function renderAdminContent()
    {
    }

    public function renderContent($itemTemplate = 'KrdSiteBundle:Module:sitemap/sitemap.item.html.twig')
    {
        $menu = $this->menuManager->getMenu(array('top'), 1);

        return $this->twig->render('KrdSiteBundle:Module:sitemap/sitemap.html.twig',
            array(
                'menu_0' => array_shift($menu),
                'menu_1' => array_shift($menu),
                'menu_2' => array_shift($menu),
                'menu_3' => array_shift($menu),
                'menu_4' => array_shift($menu),

                'itemTemplate' => $itemTemplate,
            ));
    }
}
