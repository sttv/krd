<?php

namespace Q\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;

/**
 * Модель пользователя
 *
 * @ORM\Entity
 * @ORM\Table(name="user",
 *      indexes={
 *          @ORM\Index(name="confirmation_token", columns={"confirmation_token"}),
 *          @ORM\Index(name="enabled", columns={"enabled"}),
 *          @ORM\Index(name="creply", columns={"creply"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     username={"text", {"label"="Логин"}},
 *
 *     name={"text", {"label"="Имя", "required"=false}},
 *     lastname={"text", {"label"="Фамилия", "required"=false}},
 *     surname={"text", {"label"="Отчество", "required"=false}},
 *
 *     phone={"text", {"label"="Телефон", "required"=false}},
 *     email={"email", {"label"="E-mail"}},
 *
 *     address_index={"text", {"label"="Адрес (индекс)", "required"=false}},
 *     address_city={"text", {"label"="Адрес (Город)", "required"=false}},
 *     address_region={"text", {"label"="Адрес (Район)", "required"=false}},
 *     address_street={"text", {"label"="Адрес (Улица)", "required"=false}},
 *     address_home={"text", {"label"="Адрес (Дом/Корпус)", "required"=false}},
 *     address_room={"text", {"label"="Адрес (Квартира)", "required"=false}},
 *
 *     plainPassword={"repeated", {
 *         "type"="password",
 *         "invalid_message"="Пароли должны совпадать",
 *         "required"=false,
 *         "first_options"={"label"="Пароль"},
 *         "second_options"={"label"="Повтор пароля"}}
 *     },
 *     groups={"entity", {
 *         "label"="Группы",
 *         "class"="QUserBundle:UserGroup",
 *         "property"="name",
 *         "multiple"=true,
 *         "required"=false
 *     }},
 *     cmsindex={"text", {"label"="Первая страница CMS", "required"=false}},
 *     enabled={"checkbox", {"label"="Активность", "required"=false}},
 *     locked={"checkbox", {"label"="Заблокирован", "required"=false}},
 *     newbie={"checkbox", {"label"="Новый пользователь", "required"=false}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class User extends BaseUser
{
    /**
     * ID группы фронтэнд пользователей
     */
    const FRONTEND_GROUP = 29;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     */
    protected $id;

    /**
     * Имя
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали имя", groups={"registration", "profile"})
     */
    protected $name = '';

    /**
     * Фамилия
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали фамилию", groups={"registration", "profile"})
     */
    protected $lastname = '';

    /**
     * Отчество
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали отчество", groups={"registration", "profile"})
     */
    protected $surname = '';

    /**
     * Телефон
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали телефон", groups={"registration", "profile"})
     * @Assert\Regex(pattern="/^[0-9\-+]{5,255}$/", match=true, message="Телефон может состоять только из цифр и должен быть больше 5 символов длиной", groups={"registration", "profile"})
     */
    protected $phone = '';


    /**
     * Адрес - индекс
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали индекс адреса", groups={"registration", "profile"})
     * @Assert\Regex(pattern="/^\d{5,10}$/", match=true, message="Индекс может состоять только из цифр и должен быть от 5 до 10 цифр длиной", groups={"registration", "profile"})
     */
    protected $address_index = '';

    /**
     * Адрес - город
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали город", groups={"registration", "profile"})
     * @Assert\Regex(pattern="/^[A-ZА-Я0-9]/u", match=true, message="Город должен начинаться с заглавной буквы или цифры", groups={"registration", "profile"})
     */
    protected $address_city = '';

    /**
     * Адрес - округ
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали округ", groups={"registration", "profile"})
     * @Assert\Regex(pattern="/^[A-ZА-Я0-9]/u", match=true, message="Район должен начинаться с заглавной буквы или цифры", groups={"registration", "profile"})
     */
    protected $address_region = '';

    /**
     * Адрес - улица
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали улицу", groups={"registration", "profile"})
     */
    protected $address_street = '';

    /**
     * Адрес - дом/корпус
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank(message="Вы не указали номер дома", groups={"registration", "profile"})
     * @Assert\Regex(pattern="/^[0-9]/u", match=true, message="Номер дома должен начинаться с цифры", groups={"registration", "profile"})
     */
    protected $address_home = '';

    /**
     * Адрес - квартира
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\Regex(pattern="/^[0-9]/u", match=true, message="Номер квартиры должен начинаться с цифры", groups={"registration", "profile"})
     */
    protected $address_room = '';

    /**
     * @ORM\ManyToMany(targetEntity="UserGroup")
     * @ORM\JoinTable(name="user_group_relation",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     *
     * @JMS\Expose
     */
    protected $groups;

    /**
     * Страница CMS на которую попадет пользователь, если у него нет прав на главную
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    protected $cmsindex = '';

    /**
     * Контрольный вопрос
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    protected $cquestion = '';

    /**
     * Ответ на контрольный вопрос
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    protected $creply = '';

    /**
     * Список активных виджетов в личном кабинете
     * @ORM\Column(type="array")
     * @JMS\Expose
     */
    private $widgets = array();

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Assert\Type("boolean")
     */
    private $newbie = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();

        parent::__construct();
    }

    /**
     * Строка имени пользователя для селектов в админке
     * @return string
     */
    public function getCmsTitle()
    {
        return $this->getUsername().' ('.$this->getFullName().')';
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getFullName()
    {
        return trim($this->getLastname().' '.$this->getName());
    }

    public function getCmsIndex()
    {
        return $this->cmsindex;
    }

    public function setCmsIndex($cmsindex)
    {
        $this->cmsindex = $cmsindex;
    }

    public function hasCmsIndex()
    {
        return !empty($this->cmsindex);
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address_index
     *
     * @param string $addressIndex
     * @return User
     */
    public function setAddressIndex($addressIndex)
    {
        $this->address_index = $addressIndex;

        return $this;
    }

    /**
     * Get address_index
     *
     * @return string
     */
    public function getAddressIndex()
    {
        return $this->address_index;
    }

    /**
     * Set address_city
     *
     * @param string $addressCity
     * @return User
     */
    public function setAddressCity($addressCity)
    {
        $this->address_city = $addressCity;

        return $this;
    }

    /**
     * Get address_city
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->address_city;
    }

    /**
     * Set address_region
     *
     * @param string $addressRegion
     * @return User
     */
    public function setAddressRegion($addressRegion)
    {
        $this->address_region = $addressRegion;

        return $this;
    }

    /**
     * Get address_region
     *
     * @return string
     */
    public function getAddressRegion()
    {
        return $this->address_region;
    }

    /**
     * Set address_street
     *
     * @param string $addressStreet
     * @return User
     */
    public function setAddressStreet($addressStreet)
    {
        $this->address_street = $addressStreet;

        return $this;
    }

    /**
     * Get address_street
     *
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->address_street;
    }

    /**
     * Set address_home
     *
     * @param string $addressHome
     * @return User
     */
    public function setAddressHome($addressHome)
    {
        $this->address_home = $addressHome;

        return $this;
    }

    /**
     * Get address_home
     *
     * @return string
     */
    public function getAddressHome()
    {
        return $this->address_home;
    }

    /**
     * Set address_room
     *
     * @param string $addressRoom
     * @return User
     */
    public function setAddressRoom($addressRoom)
    {
        $this->address_room = $addressRoom;

        return $this;
    }

    /**
     * Get address_room
     *
     * @return string
     */
    public function getAddressRoom()
    {
        return $this->address_room;
    }

    public function setCquestion($cquestion)
    {
        $this->cquestion = $cquestion;

        return $this;
    }

    public function getCquestion()
    {
        return $this->cquestion;
    }

    public function setCreply($creply)
    {
        $this->creply = $creply;

        return $this;
    }

    public function getCreply()
    {
        return $this->creply;
    }

    public function getWidgets()
    {
        return $this->widgets;
    }

    public function setWidgets(array $widgets)
    {
        $this->widgets = array();

        foreach($widgets as $widget) {
            $this->addWidget($widget);
        }
    }

    public function addWidget($widget)
    {
        $widget = mb_strtolower($widget);

        if (!empty($widget) && !$this->hasWidget($widget)) {
            $this->widgets[] = $widget;
        }
    }

    public function removeWidget($widget)
    {
        $widget = mb_strtolower($widget);

        $res = array();

        foreach ($this->getWidgets() as $w) {
            if ($w != $widget) {
                $res[] = $w;
            }
        }

        $this->widgets = $res;
    }

    public function hasWidget($widget)
    {
        return in_array(mb_strtolower($widget), $this->widgets, true);
    }

    public function setNewbie($newbie)
    {
        $this->newbie = (boolean)$newbie;
    }

    public function getNewbie()
    {
        return $this->newbie;
    }
}
