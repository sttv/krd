angular.module('entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap'])
    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', function($http, $resource, ngTableParams, $parse, $dialog) {

        return {
            restrict: 'A',
            scope: true,
            transclude: true,

            template: function(tElement, tAttrs) {
                var columnsGetter = $parse(tAttrs.columns);
                var template = '';
                columns = columnsGetter({});

                template += '<div loading-container="loading">';

                template += '<div class="btn-group">';
                    if (!!tAttrs.btnAdd) {
                        template += '<div class="btn btn-success" ng-click="showAddForm()"><i class="icon-plus icon-white"></i> '+tAttrs.btnAdd+'</div>';
                    }
                template += '</div>';

                template += '<div ng-show="itemList"><table ng-table="tableParams" class="table table-hover table-striped">'+
                    '<tr ng-repeat="item in itemList">';

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\'';
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'" sortable="item.'+i+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label';
                    }

                    template += '</td>';
                }

                template += '<td data-title="Действия">';
                    template += '<button class="btn btn-warning btn-small" tooltip="Редактировать"><i class="icon-white icon-edit"></i></button>&nbsp;&nbsp;';
                    template += '<button class="btn btn-danger btn-small" tooltip="Удалить"><i class="icon-white icon-trash"></i></button>';
                template += '</td>';

                template += '</tr></table></div>';
                template += '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    total: 0,
                    count: parseInt($attrs.count, 10)
                });

                $scope.itemList = [];

                // Наблюдаем за параметрами таблицы
                $scope.$watch('tableParams', function(params) {
                    $scope.loading = true;

                    $scope.entityResource.query(params.url(), function(data) {
                        $scope.loading = false;
                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                    });
                }, true);

                // Наблюдаем за изменением ресурсов
                $scope.$watch('itemList', function(itemList, oldList) {
                    for(var i = 0; i < oldList.length; i++) {
                        if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                            continue;
                        }

                        if (!angular.equals(itemList[i], oldList[i])) {
                            $http.post(($attrs.entityList + '/').replace('//', '/') + 'set/' + itemList[i].id, itemList[i], function(response) {
                                $scope.itemList[i] = response;
                            });
                        }
                    }
                }, true);

                /**
                 * Показываем форму добавления элемента
                 */
                $scope.showAddForm = function() {
                    var d = $dialog.dialog({
                        backdrop: true,
                        keyboard: true,
                        backdropClick: true,
                        templateUrl: ($attrs.entityList + '/').replace('//', '/') + 'form/'
                    });

                    d.open();
                }
            },

            compile: function($element, $attrs) {
                // Список колонок таблицы
                var columnsGetter = $parse($attrs.columns);
                var columnSetter = columnsGetter.assign;

                $element.addClass('entity-list');

                return function($scope, $element, attrs) {
                    $scope.columns = columnsGetter($scope);
                    $scope.entityResource = $resource(($attrs.entityList + '/').replace('//', '/') + ':id', {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'}
                        });
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', function($http) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $http[$scope.method]($scope.actionUrl, $element.serializeObject()).success(function(response) {
                        if (response.success) {
                            console.log('Success', response);
                        } else {
                            console.log('Errors', response);
                            if (!!response.errors) {
                                $scope.errors = response.errors;
                            }
                        }
                        console.log(response);
                    });
                }
            },

            compile: function($element) {
                $element.find('input[type="text"]').each(function() {
                    $(this).attr('popover', '{{errors[\'' + $(this).attr('name') + '\'] }}').attr('popover-placement', 'right').attr('popover-trigger', 'focus');
                });

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                }
            }
        }
    }])

    /**
     * Тип полей - список текстовых полей
     */
    .directive('formCollectionText', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                var $controls = $element.find('.controls');
                var prototype = $attrs.prototype + removeButtonTpl;

                $scope.nextIndex = $controls.find('input').length + 1;

                $scope.addElement = function() {
                    var input = prototype.replace(/__name__/g, $scope.nextIndex);
                    $controls.children().last().before($compile(input)($scope));

                    $scope.nextIndex++;
                }

                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.srcElement);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.add($removeButton.prev('input')).remove();
                }
            },

            compile: function($element, $attrs) {
                if (!$attrs.prototype) {
                    $element.find('.controls').html('<span class="label label-important">Empty prototype</span>');
                }

                $element.find('.controls input').not(':first').after(removeButtonTpl);

                $element.find('.btn-add').attr('ng-click', 'addElement()');

                return function($scope, $element, attrs) {

                }
            }
        }
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'">'+value+'</a>';
        }
    }])
;
