<?php

namespace Krd\SiteBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер PollAnswer
 *
 * @Route("/rest/krd/site/pollanswer")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestPollAnswerController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdSiteBundle:PollAnswer';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_conference_pollanswer_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_conference_pollanswer_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_conference_pollanswer", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->orderBy('e.sort', 'ASC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_conference_pollanswer_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_conference_pollanswer_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_conference_pollanswer_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_conference_pollanswer_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        // $request->query->set('parent', $parent);
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        if (($parent = $request->get('parent')) && ($parent = $this->getDoctrine()->getManager()->getRepository('KrdSiteBundle:Poll')->find($parent))) {
            $entity->setParent($parent);
        }
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_conference_pollanswer_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Шаблон списка элементов
     * @Route("/list_tpl/{id}", name="cms_rest_krd_conference_pollanswer_list", defaults={"_format"="html", "id"=""}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("KrdSiteBundle:Admin:Module/pollanswer-list.html.twig")
     */
    public function listByParentAction($id)
    {
        return array('parentId' => $id);
    }

    /**
     * Сортировка элемента
     *
     * @Route("/sort/{order}", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function actionSortOfParent(Request $request, $order)
    {
        return parent::actionSortOfParent($request, $order);
    }
}
