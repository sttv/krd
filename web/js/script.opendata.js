//     Underscore.js 1.4.2
//     http://underscorejs.org
//     (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
//     Underscore may be freely distributed under the MIT license.
(function(){var e=this,t=e._,n={},r=Array.prototype,i=Object.prototype,s=Function.prototype,o=r.push,u=r.slice,a=r.concat,f=r.unshift,l=i.toString,c=i.hasOwnProperty,h=r.forEach,p=r.map,d=r.reduce,v=r.reduceRight,m=r.filter,g=r.every,y=r.some,b=r.indexOf,w=r.lastIndexOf,E=Array.isArray,S=Object.keys,x=s.bind,T=function(e){if(e instanceof T)return e;if(!(this instanceof T))return new T(e);this._wrapped=e};typeof exports!="undefined"?(typeof module!="undefined"&&module.exports&&(exports=module.exports=T),exports._=T):e._=T,T.VERSION="1.4.2";var N=T.each=T.forEach=function(e,t,r){if(e==null)return;if(h&&e.forEach===h)e.forEach(t,r);else if(e.length===+e.length){for(var i=0,s=e.length;i<s;i++)if(t.call(r,e[i],i,e)===n)return}else for(var o in e)if(T.has(e,o)&&t.call(r,e[o],o,e)===n)return};T.map=T.collect=function(e,t,n){var r=[];return e==null?r:p&&e.map===p?e.map(t,n):(N(e,function(e,i,s){r[r.length]=t.call(n,e,i,s)}),r)},T.reduce=T.foldl=T.inject=function(e,t,n,r){var i=arguments.length>2;e==null&&(e=[]);if(d&&e.reduce===d)return r&&(t=T.bind(t,r)),i?e.reduce(t,n):e.reduce(t);N(e,function(e,s,o){i?n=t.call(r,n,e,s,o):(n=e,i=!0)});if(!i)throw new TypeError("Reduce of empty array with no initial value");return n},T.reduceRight=T.foldr=function(e,t,n,r){var i=arguments.length>2;e==null&&(e=[]);if(v&&e.reduceRight===v)return r&&(t=T.bind(t,r)),arguments.length>2?e.reduceRight(t,n):e.reduceRight(t);var s=e.length;if(s!==+s){var o=T.keys(e);s=o.length}N(e,function(u,a,f){a=o?o[--s]:--s,i?n=t.call(r,n,e[a],a,f):(n=e[a],i=!0)});if(!i)throw new TypeError("Reduce of empty array with no initial value");return n},T.find=T.detect=function(e,t,n){var r;return C(e,function(e,i,s){if(t.call(n,e,i,s))return r=e,!0}),r},T.filter=T.select=function(e,t,n){var r=[];return e==null?r:m&&e.filter===m?e.filter(t,n):(N(e,function(e,i,s){t.call(n,e,i,s)&&(r[r.length]=e)}),r)},T.reject=function(e,t,n){var r=[];return e==null?r:(N(e,function(e,i,s){t.call(n,e,i,s)||(r[r.length]=e)}),r)},T.every=T.all=function(e,t,r){t||(t=T.identity);var i=!0;return e==null?i:g&&e.every===g?e.every(t,r):(N(e,function(e,s,o){if(!(i=i&&t.call(r,e,s,o)))return n}),!!i)};var C=T.some=T.any=function(e,t,r){t||(t=T.identity);var i=!1;return e==null?i:y&&e.some===y?e.some(t,r):(N(e,function(e,s,o){if(i||(i=t.call(r,e,s,o)))return n}),!!i)};T.contains=T.include=function(e,t){var n=!1;return e==null?n:b&&e.indexOf===b?e.indexOf(t)!=-1:(n=C(e,function(e){return e===t}),n)},T.invoke=function(e,t){var n=u.call(arguments,2);return T.map(e,function(e){return(T.isFunction(t)?t:e[t]).apply(e,n)})},T.pluck=function(e,t){return T.map(e,function(e){return e[t]})},T.where=function(e,t){return T.isEmpty(t)?[]:T.filter(e,function(e){for(var n in t)if(t[n]!==e[n])return!1;return!0})},T.max=function(e,t,n){if(!t&&T.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.max.apply(Math,e);if(!t&&T.isEmpty(e))return-Infinity;var r={computed:-Infinity};return N(e,function(e,i,s){var o=t?t.call(n,e,i,s):e;o>=r.computed&&(r={value:e,computed:o})}),r.value},T.min=function(e,t,n){if(!t&&T.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.min.apply(Math,e);if(!t&&T.isEmpty(e))return Infinity;var r={computed:Infinity};return N(e,function(e,i,s){var o=t?t.call(n,e,i,s):e;o<r.computed&&(r={value:e,computed:o})}),r.value},T.shuffle=function(e){var t,n=0,r=[];return N(e,function(e){t=T.random(n++),r[n-1]=r[t],r[t]=e}),r};var k=function(e){return T.isFunction(e)?e:function(t){return t[e]}};T.sortBy=function(e,t,n){var r=k(t);return T.pluck(T.map(e,function(e,t,i){return{value:e,index:t,criteria:r.call(n,e,t,i)}}).sort(function(e,t){var n=e.criteria,r=t.criteria;if(n!==r){if(n>r||n===void 0)return 1;if(n<r||r===void 0)return-1}return e.index<t.index?-1:1}),"value")};var L=function(e,t,n,r){var i={},s=k(t);return N(e,function(t,o){var u=s.call(n,t,o,e);r(i,u,t)}),i};T.groupBy=function(e,t,n){return L(e,t,n,function(e,t,n){(T.has(e,t)?e[t]:e[t]=[]).push(n)})},T.countBy=function(e,t,n){return L(e,t,n,function(e,t,n){T.has(e,t)||(e[t]=0),e[t]++})},T.sortedIndex=function(e,t,n,r){n=n==null?T.identity:k(n);var i=n.call(r,t),s=0,o=e.length;while(s<o){var u=s+o>>>1;n.call(r,e[u])<i?s=u+1:o=u}return s},T.toArray=function(e){return e?e.length===+e.length?u.call(e):T.values(e):[]},T.size=function(e){return e.length===+e.length?e.length:T.keys(e).length},T.first=T.head=T.take=function(e,t,n){return t!=null&&!n?u.call(e,0,t):e[0]},T.initial=function(e,t,n){return u.call(e,0,e.length-(t==null||n?1:t))},T.last=function(e,t,n){return t!=null&&!n?u.call(e,Math.max(e.length-t,0)):e[e.length-1]},T.rest=T.tail=T.drop=function(e,t,n){return u.call(e,t==null||n?1:t)},T.compact=function(e){return T.filter(e,function(e){return!!e})};var A=function(e,t,n){return N(e,function(e){T.isArray(e)?t?o.apply(n,e):A(e,t,n):n.push(e)}),n};T.flatten=function(e,t){return A(e,t,[])},T.without=function(e){return T.difference(e,u.call(arguments,1))},T.uniq=T.unique=function(e,t,n,r){var i=n?T.map(e,n,r):e,s=[],o=[];return N(i,function(n,r){if(t?!r||o[o.length-1]!==n:!T.contains(o,n))o.push(n),s.push(e[r])}),s},T.union=function(){return T.uniq(a.apply(r,arguments))},T.intersection=function(e){var t=u.call(arguments,1);return T.filter(T.uniq(e),function(e){return T.every(t,function(t){return T.indexOf(t,e)>=0})})},T.difference=function(e){var t=a.apply(r,u.call(arguments,1));return T.filter(e,function(e){return!T.contains(t,e)})},T.zip=function(){var e=u.call(arguments),t=T.max(T.pluck(e,"length")),n=new Array(t);for(var r=0;r<t;r++)n[r]=T.pluck(e,""+r);return n},T.object=function(e,t){var n={};for(var r=0,i=e.length;r<i;r++)t?n[e[r]]=t[r]:n[e[r][0]]=e[r][1];return n},T.indexOf=function(e,t,n){if(e==null)return-1;var r=0,i=e.length;if(n){if(typeof n!="number")return r=T.sortedIndex(e,t),e[r]===t?r:-1;r=n<0?Math.max(0,i+n):n}if(b&&e.indexOf===b)return e.indexOf(t,n);for(;r<i;r++)if(e[r]===t)return r;return-1},T.lastIndexOf=function(e,t,n){if(e==null)return-1;var r=n!=null;if(w&&e.lastIndexOf===w)return r?e.lastIndexOf(t,n):e.lastIndexOf(t);var i=r?n:e.length;while(i--)if(e[i]===t)return i;return-1},T.range=function(e,t,n){arguments.length<=1&&(t=e||0,e=0),n=arguments[2]||1;var r=Math.max(Math.ceil((t-e)/n),0),i=0,s=new Array(r);while(i<r)s[i++]=e,e+=n;return s};var O=function(){};T.bind=function(t,n){var r,i;if(t.bind===x&&x)return x.apply(t,u.call(arguments,1));if(!T.isFunction(t))throw new TypeError;return i=u.call(arguments,2),r=function(){if(this instanceof r){O.prototype=t.prototype;var e=new O,s=t.apply(e,i.concat(u.call(arguments)));return Object(s)===s?s:e}return t.apply(n,i.concat(u.call(arguments)))}},T.bindAll=function(e){var t=u.call(arguments,1);return t.length==0&&(t=T.functions(e)),N(t,function(t){e[t]=T.bind(e[t],e)}),e},T.memoize=function(e,t){var n={};return t||(t=T.identity),function(){var r=t.apply(this,arguments);return T.has(n,r)?n[r]:n[r]=e.apply(this,arguments)}},T.delay=function(e,t){var n=u.call(arguments,2);return setTimeout(function(){return e.apply(null,n)},t)},T.defer=function(e){return T.delay.apply(T,[e,1].concat(u.call(arguments,1)))},T.throttle=function(e,t){var n,r,i,s,o,u,a=T.debounce(function(){o=s=!1},t);return function(){n=this,r=arguments;var f=function(){i=null,o&&(u=e.apply(n,r)),a()};return i||(i=setTimeout(f,t)),s?o=!0:(s=!0,u=e.apply(n,r)),a(),u}},T.debounce=function(e,t,n){var r,i;return function(){var s=this,o=arguments,u=function(){r=null,n||(i=e.apply(s,o))},a=n&&!r;return clearTimeout(r),r=setTimeout(u,t),a&&(i=e.apply(s,o)),i}},T.once=function(e){var t=!1,n;return function(){return t?n:(t=!0,n=e.apply(this,arguments),e=null,n)}},T.wrap=function(e,t){return function(){var n=[e];return o.apply(n,arguments),t.apply(this,n)}},T.compose=function(){var e=arguments;return function(){var t=arguments;for(var n=e.length-1;n>=0;n--)t=[e[n].apply(this,t)];return t[0]}},T.after=function(e,t){return e<=0?t():function(){if(--e<1)return t.apply(this,arguments)}},T.keys=S||function(e){if(e!==Object(e))throw new TypeError("Invalid object");var t=[];for(var n in e)T.has(e,n)&&(t[t.length]=n);return t},T.values=function(e){var t=[];for(var n in e)T.has(e,n)&&t.push(e[n]);return t},T.pairs=function(e){var t=[];for(var n in e)T.has(e,n)&&t.push([n,e[n]]);return t},T.invert=function(e){var t={};for(var n in e)T.has(e,n)&&(t[e[n]]=n);return t},T.functions=T.methods=function(e){var t=[];for(var n in e)T.isFunction(e[n])&&t.push(n);return t.sort()},T.extend=function(e){return N(u.call(arguments,1),function(t){for(var n in t)e[n]=t[n]}),e},T.pick=function(e){var t={},n=a.apply(r,u.call(arguments,1));return N(n,function(n){n in e&&(t[n]=e[n])}),t},T.omit=function(e){var t={},n=a.apply(r,u.call(arguments,1));for(var i in e)T.contains(n,i)||(t[i]=e[i]);return t},T.defaults=function(e){return N(u.call(arguments,1),function(t){for(var n in t)e[n]==null&&(e[n]=t[n])}),e},T.clone=function(e){return T.isObject(e)?T.isArray(e)?e.slice():T.extend({},e):e},T.tap=function(e,t){return t(e),e};var M=function(e,t,n,r){if(e===t)return e!==0||1/e==1/t;if(e==null||t==null)return e===t;e instanceof T&&(e=e._wrapped),t instanceof T&&(t=t._wrapped);var i=l.call(e);if(i!=l.call(t))return!1;switch(i){case"[object String]":return e==String(t);case"[object Number]":return e!=+e?t!=+t:e==0?1/e==1/t:e==+t;case"[object Date]":case"[object Boolean]":return+e==+t;case"[object RegExp]":return e.source==t.source&&e.global==t.global&&e.multiline==t.multiline&&e.ignoreCase==t.ignoreCase}if(typeof e!="object"||typeof t!="object")return!1;var s=n.length;while(s--)if(n[s]==e)return r[s]==t;n.push(e),r.push(t);var o=0,u=!0;if(i=="[object Array]"){o=e.length,u=o==t.length;if(u)while(o--)if(!(u=M(e[o],t[o],n,r)))break}else{var a=e.constructor,f=t.constructor;if(a!==f&&!(T.isFunction(a)&&a instanceof a&&T.isFunction(f)&&f instanceof f))return!1;for(var c in e)if(T.has(e,c)){o++;if(!(u=T.has(t,c)&&M(e[c],t[c],n,r)))break}if(u){for(c in t)if(T.has(t,c)&&!(o--))break;u=!o}}return n.pop(),r.pop(),u};T.isEqual=function(e,t){return M(e,t,[],[])},T.isEmpty=function(e){if(e==null)return!0;if(T.isArray(e)||T.isString(e))return e.length===0;for(var t in e)if(T.has(e,t))return!1;return!0},T.isElement=function(e){return!!e&&e.nodeType===1},T.isArray=E||function(e){return l.call(e)=="[object Array]"},T.isObject=function(e){return e===Object(e)},N(["Arguments","Function","String","Number","Date","RegExp"],function(e){T["is"+e]=function(t){return l.call(t)=="[object "+e+"]"}}),T.isArguments(arguments)||(T.isArguments=function(e){return!!e&&!!T.has(e,"callee")}),typeof /./!="function"&&(T.isFunction=function(e){return typeof e=="function"}),T.isFinite=function(e){return T.isNumber(e)&&isFinite(e)},T.isNaN=function(e){return T.isNumber(e)&&e!=+e},T.isBoolean=function(e){return e===!0||e===!1||l.call(e)=="[object Boolean]"},T.isNull=function(e){return e===null},T.isUndefined=function(e){return e===void 0},T.has=function(e,t){return c.call(e,t)},T.noConflict=function(){return e._=t,this},T.identity=function(e){return e},T.times=function(e,t,n){for(var r=0;r<e;r++)t.call(n,r)},T.random=function(e,t){return t==null&&(t=e,e=0),e+(0|Math.random()*(t-e+1))};var _={escape:{"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","/":"&#x2F;"}};_.unescape=T.invert(_.escape);var D={escape:new RegExp("["+T.keys(_.escape).join("")+"]","g"),unescape:new RegExp("("+T.keys(_.unescape).join("|")+")","g")};T.each(["escape","unescape"],function(e){T[e]=function(t){return t==null?"":(""+t).replace(D[e],function(t){return _[e][t]})}}),T.result=function(e,t){if(e==null)return null;var n=e[t];return T.isFunction(n)?n.call(e):n},T.mixin=function(e){N(T.functions(e),function(t){var n=T[t]=e[t];T.prototype[t]=function(){var e=[this._wrapped];return o.apply(e,arguments),F.call(this,n.apply(T,e))}})};var P=0;T.uniqueId=function(e){var t=P++;return e?e+t:t},T.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var H=/(.)^/,B={"'":"'","\\":"\\","\r":"r","\n":"n","	":"t","\u2028":"u2028","\u2029":"u2029"},j=/\\|'|\r|\n|\t|\u2028|\u2029/g;T.template=function(e,t,n){n=T.defaults({},n,T.templateSettings);var r=new RegExp([(n.escape||H).source,(n.interpolate||H).source,(n.evaluate||H).source].join("|")+"|$","g"),i=0,s="__p+='";e.replace(r,function(t,n,r,o,u){s+=e.slice(i,u).replace(j,function(e){return"\\"+B[e]}),s+=n?"'+\n((__t=("+n+"))==null?'':_.escape(__t))+\n'":r?"'+\n((__t=("+r+"))==null?'':__t)+\n'":o?"';\n"+o+"\n__p+='":"",i=u+t.length}),s+="';\n",n.variable||(s="with(obj||{}){\n"+s+"}\n"),s="var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n"+s+"return __p;\n";try{var o=new Function(n.variable||"obj","_",s)}catch(u){throw u.source=s,u}if(t)return o(t,T);var a=function(e){return o.call(this,e,T)};return a.source="function("+(n.variable||"obj")+"){\n"+s+"}",a},T.chain=function(e){return T(e).chain()};var F=function(e){return this._chain?T(e).chain():e};T.mixin(T),N(["pop","push","reverse","shift","sort","splice","unshift"],function(e){var t=r[e];T.prototype[e]=function(){var n=this._wrapped;return t.apply(n,arguments),(e=="shift"||e=="splice")&&n.length===0&&delete n[0],F.call(this,n)}}),N(["concat","join","slice"],function(e){var t=r[e];T.prototype[e]=function(){return F.call(this,t.apply(this._wrapped,arguments))}}),T.extend(T.prototype,{chain:function(){return this._chain=!0,this},value:function(){return this._wrapped}})}).call(this);

// Backbone.js 0.9.2

// (c) 2010-2012 Jeremy Ashkenas, DocumentCloud Inc.
// Backbone may be freely distributed under the MIT license.
// For all details and documentation:
// http://backbonejs.org
(function(){var l=this,y=l.Backbone,z=Array.prototype.slice,A=Array.prototype.splice,g;g="undefined"!==typeof exports?exports:l.Backbone={};g.VERSION="0.9.2";var f=l._;!f&&"undefined"!==typeof require&&(f=require("underscore"));var i=l.jQuery||l.Zepto||l.ender;g.setDomLibrary=function(a){i=a};g.noConflict=function(){l.Backbone=y;return this};g.emulateHTTP=!1;g.emulateJSON=!1;var p=/\s+/,k=g.Events={on:function(a,b,c){var d,e,f,g,j;if(!b)return this;a=a.split(p);for(d=this._callbacks||(this._callbacks=
{});e=a.shift();)f=(j=d[e])?j.tail:{},f.next=g={},f.context=c,f.callback=b,d[e]={tail:g,next:j?j.next:f};return this},off:function(a,b,c){var d,e,h,g,j,q;if(e=this._callbacks){if(!a&&!b&&!c)return delete this._callbacks,this;for(a=a?a.split(p):f.keys(e);d=a.shift();)if(h=e[d],delete e[d],h&&(b||c))for(g=h.tail;(h=h.next)!==g;)if(j=h.callback,q=h.context,b&&j!==b||c&&q!==c)this.on(d,j,q);return this}},trigger:function(a){var b,c,d,e,f,g;if(!(d=this._callbacks))return this;f=d.all;a=a.split(p);for(g=
z.call(arguments,1);b=a.shift();){if(c=d[b])for(e=c.tail;(c=c.next)!==e;)c.callback.apply(c.context||this,g);if(c=f){e=c.tail;for(b=[b].concat(g);(c=c.next)!==e;)c.callback.apply(c.context||this,b)}}return this}};k.bind=k.on;k.unbind=k.off;var o=g.Model=function(a,b){var c;a||(a={});b&&b.parse&&(a=this.parse(a));if(c=n(this,"defaults"))a=f.extend({},c,a);b&&b.collection&&(this.collection=b.collection);this.attributes={};this._escapedAttributes={};this.cid=f.uniqueId("c");this.changed={};this._silent=
{};this._pending={};this.set(a,{silent:!0});this.changed={};this._silent={};this._pending={};this._previousAttributes=f.clone(this.attributes);this.initialize.apply(this,arguments)};f.extend(o.prototype,k,{changed:null,_silent:null,_pending:null,idAttribute:"id",initialize:function(){},toJSON:function(){return f.clone(this.attributes)},get:function(a){return this.attributes[a]},escape:function(a){var b;if(b=this._escapedAttributes[a])return b;b=this.get(a);return this._escapedAttributes[a]=f.escape(null==
b?"":""+b)},has:function(a){return null!=this.get(a)},set:function(a,b,c){var d,e;f.isObject(a)||null==a?(d=a,c=b):(d={},d[a]=b);c||(c={});if(!d)return this;d instanceof o&&(d=d.attributes);if(c.unset)for(e in d)d[e]=void 0;if(!this._validate(d,c))return!1;this.idAttribute in d&&(this.id=d[this.idAttribute]);var b=c.changes={},h=this.attributes,g=this._escapedAttributes,j=this._previousAttributes||{};for(e in d){a=d[e];if(!f.isEqual(h[e],a)||c.unset&&f.has(h,e))delete g[e],(c.silent?this._silent:
b)[e]=!0;c.unset?delete h[e]:h[e]=a;!f.isEqual(j[e],a)||f.has(h,e)!=f.has(j,e)?(this.changed[e]=a,c.silent||(this._pending[e]=!0)):(delete this.changed[e],delete this._pending[e])}c.silent||this.change(c);return this},unset:function(a,b){(b||(b={})).unset=!0;return this.set(a,null,b)},clear:function(a){(a||(a={})).unset=!0;return this.set(f.clone(this.attributes),a)},fetch:function(a){var a=a?f.clone(a):{},b=this,c=a.success;a.success=function(d,e,f){if(!b.set(b.parse(d,f),a))return!1;c&&c(b,d)};
a.error=g.wrapError(a.error,b,a);return(this.sync||g.sync).call(this,"read",this,a)},save:function(a,b,c){var d,e;f.isObject(a)||null==a?(d=a,c=b):(d={},d[a]=b);c=c?f.clone(c):{};if(c.wait){if(!this._validate(d,c))return!1;e=f.clone(this.attributes)}a=f.extend({},c,{silent:!0});if(d&&!this.set(d,c.wait?a:c))return!1;var h=this,i=c.success;c.success=function(a,b,e){b=h.parse(a,e);if(c.wait){delete c.wait;b=f.extend(d||{},b)}if(!h.set(b,c))return false;i?i(h,a):h.trigger("sync",h,a,c)};c.error=g.wrapError(c.error,
h,c);b=this.isNew()?"create":"update";b=(this.sync||g.sync).call(this,b,this,c);c.wait&&this.set(e,a);return b},destroy:function(a){var a=a?f.clone(a):{},b=this,c=a.success,d=function(){b.trigger("destroy",b,b.collection,a)};if(this.isNew())return d(),!1;a.success=function(e){a.wait&&d();c?c(b,e):b.trigger("sync",b,e,a)};a.error=g.wrapError(a.error,b,a);var e=(this.sync||g.sync).call(this,"delete",this,a);a.wait||d();return e},url:function(){var a=n(this,"urlRoot")||n(this.collection,"url")||t();
return this.isNew()?a:a+("/"==a.charAt(a.length-1)?"":"/")+encodeURIComponent(this.id)},parse:function(a){return a},clone:function(){return new this.constructor(this.attributes)},isNew:function(){return null==this.id},change:function(a){a||(a={});var b=this._changing;this._changing=!0;for(var c in this._silent)this._pending[c]=!0;var d=f.extend({},a.changes,this._silent);this._silent={};for(c in d)this.trigger("change:"+c,this,this.get(c),a);if(b)return this;for(;!f.isEmpty(this._pending);){this._pending=
{};this.trigger("change",this,a);for(c in this.changed)!this._pending[c]&&!this._silent[c]&&delete this.changed[c];this._previousAttributes=f.clone(this.attributes)}this._changing=!1;return this},hasChanged:function(a){return!arguments.length?!f.isEmpty(this.changed):f.has(this.changed,a)},changedAttributes:function(a){if(!a)return this.hasChanged()?f.clone(this.changed):!1;var b,c=!1,d=this._previousAttributes,e;for(e in a)if(!f.isEqual(d[e],b=a[e]))(c||(c={}))[e]=b;return c},previous:function(a){return!arguments.length||
!this._previousAttributes?null:this._previousAttributes[a]},previousAttributes:function(){return f.clone(this._previousAttributes)},isValid:function(){return!this.validate(this.attributes)},_validate:function(a,b){if(b.silent||!this.validate)return!0;var a=f.extend({},this.attributes,a),c=this.validate(a,b);if(!c)return!0;b&&b.error?b.error(this,c,b):this.trigger("error",this,c,b);return!1}});var r=g.Collection=function(a,b){b||(b={});b.model&&(this.model=b.model);b.comparator&&(this.comparator=b.comparator);
this._reset();this.initialize.apply(this,arguments);a&&this.reset(a,{silent:!0,parse:b.parse})};f.extend(r.prototype,k,{model:o,initialize:function(){},toJSON:function(a){return this.map(function(b){return b.toJSON(a)})},add:function(a,b){var c,d,e,g,i,j={},k={},l=[];b||(b={});a=f.isArray(a)?a.slice():[a];c=0;for(d=a.length;c<d;c++){if(!(e=a[c]=this._prepareModel(a[c],b)))throw Error("Can't add an invalid model to a collection");g=e.cid;i=e.id;j[g]||this._byCid[g]||null!=i&&(k[i]||this._byId[i])?
l.push(c):j[g]=k[i]=e}for(c=l.length;c--;)a.splice(l[c],1);c=0;for(d=a.length;c<d;c++)(e=a[c]).on("all",this._onModelEvent,this),this._byCid[e.cid]=e,null!=e.id&&(this._byId[e.id]=e);this.length+=d;A.apply(this.models,[null!=b.at?b.at:this.models.length,0].concat(a));this.comparator&&this.sort({silent:!0});if(b.silent)return this;c=0;for(d=this.models.length;c<d;c++)if(j[(e=this.models[c]).cid])b.index=c,e.trigger("add",e,this,b);return this},remove:function(a,b){var c,d,e,g;b||(b={});a=f.isArray(a)?
a.slice():[a];c=0;for(d=a.length;c<d;c++)if(g=this.getByCid(a[c])||this.get(a[c]))delete this._byId[g.id],delete this._byCid[g.cid],e=this.indexOf(g),this.models.splice(e,1),this.length--,b.silent||(b.index=e,g.trigger("remove",g,this,b)),this._removeReference(g);return this},push:function(a,b){a=this._prepareModel(a,b);this.add(a,b);return a},pop:function(a){var b=this.at(this.length-1);this.remove(b,a);return b},unshift:function(a,b){a=this._prepareModel(a,b);this.add(a,f.extend({at:0},b));return a},
shift:function(a){var b=this.at(0);this.remove(b,a);return b},get:function(a){return null==a?void 0:this._byId[null!=a.id?a.id:a]},getByCid:function(a){return a&&this._byCid[a.cid||a]},at:function(a){return this.models[a]},where:function(a){return f.isEmpty(a)?[]:this.filter(function(b){for(var c in a)if(a[c]!==b.get(c))return!1;return!0})},sort:function(a){a||(a={});if(!this.comparator)throw Error("Cannot sort a set without a comparator");var b=f.bind(this.comparator,this);1==this.comparator.length?
this.models=this.sortBy(b):this.models.sort(b);a.silent||this.trigger("reset",this,a);return this},pluck:function(a){return f.map(this.models,function(b){return b.get(a)})},reset:function(a,b){a||(a=[]);b||(b={});for(var c=0,d=this.models.length;c<d;c++)this._removeReference(this.models[c]);this._reset();this.add(a,f.extend({silent:!0},b));b.silent||this.trigger("reset",this,b);return this},fetch:function(a){a=a?f.clone(a):{};void 0===a.parse&&(a.parse=!0);var b=this,c=a.success;a.success=function(d,
e,f){b[a.add?"add":"reset"](b.parse(d,f),a);c&&c(b,d)};a.error=g.wrapError(a.error,b,a);return(this.sync||g.sync).call(this,"read",this,a)},create:function(a,b){var c=this,b=b?f.clone(b):{},a=this._prepareModel(a,b);if(!a)return!1;b.wait||c.add(a,b);var d=b.success;b.success=function(e,f){b.wait&&c.add(e,b);d?d(e,f):e.trigger("sync",a,f,b)};a.save(null,b);return a},parse:function(a){return a},chain:function(){return f(this.models).chain()},_reset:function(){this.length=0;this.models=[];this._byId=
{};this._byCid={}},_prepareModel:function(a,b){b||(b={});a instanceof o?a.collection||(a.collection=this):(b.collection=this,a=new this.model(a,b),a._validate(a.attributes,b)||(a=!1));return a},_removeReference:function(a){this==a.collection&&delete a.collection;a.off("all",this._onModelEvent,this)},_onModelEvent:function(a,b,c,d){("add"==a||"remove"==a)&&c!=this||("destroy"==a&&this.remove(b,d),b&&a==="change:"+b.idAttribute&&(delete this._byId[b.previous(b.idAttribute)],this._byId[b.id]=b),this.trigger.apply(this,
arguments))}});f.each("forEach,each,map,reduce,reduceRight,find,detect,filter,select,reject,every,all,some,any,include,contains,invoke,max,min,sortBy,sortedIndex,toArray,size,first,initial,rest,last,without,indexOf,shuffle,lastIndexOf,isEmpty,groupBy".split(","),function(a){r.prototype[a]=function(){return f[a].apply(f,[this.models].concat(f.toArray(arguments)))}});var u=g.Router=function(a){a||(a={});a.routes&&(this.routes=a.routes);this._bindRoutes();this.initialize.apply(this,arguments)},B=/:\w+/g,
C=/\*\w+/g,D=/[-[\]{}()+?.,\\^$|#\s]/g;f.extend(u.prototype,k,{initialize:function(){},route:function(a,b,c){g.history||(g.history=new m);f.isRegExp(a)||(a=this._routeToRegExp(a));c||(c=this[b]);g.history.route(a,f.bind(function(d){d=this._extractParameters(a,d);c&&c.apply(this,d);this.trigger.apply(this,["route:"+b].concat(d));g.history.trigger("route",this,b,d)},this));return this},navigate:function(a,b){g.history.navigate(a,b)},_bindRoutes:function(){if(this.routes){var a=[],b;for(b in this.routes)a.unshift([b,
this.routes[b]]);b=0;for(var c=a.length;b<c;b++)this.route(a[b][0],a[b][1],this[a[b][1]])}},_routeToRegExp:function(a){a=a.replace(D,"\\$&").replace(B,"([^/]+)").replace(C,"(.*?)");return RegExp("^"+a+"$")},_extractParameters:function(a,b){return a.exec(b).slice(1)}});var m=g.History=function(){this.handlers=[];f.bindAll(this,"checkUrl")},s=/^[#\/]/,E=/msie [\w.]+/;m.started=!1;f.extend(m.prototype,k,{interval:50,getHash:function(a){return(a=(a?a.location:window.location).href.match(/#(.*)$/))?a[1]:
""},getFragment:function(a,b){if(null==a)if(this._hasPushState||b){var a=window.location.pathname,c=window.location.search;c&&(a+=c)}else a=this.getHash();a.indexOf(this.options.root)||(a=a.substr(this.options.root.length));return a.replace(s,"")},start:function(a){if(m.started)throw Error("Backbone.history has already been started");m.started=!0;this.options=f.extend({},{root:"/"},this.options,a);this._wantsHashChange=!1!==this.options.hashChange;this._wantsPushState=!!this.options.pushState;this._hasPushState=
!(!this.options.pushState||!window.history||!window.history.pushState);var a=this.getFragment(),b=document.documentMode;if(b=E.exec(navigator.userAgent.toLowerCase())&&(!b||7>=b))this.iframe=i('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo("body")[0].contentWindow,this.navigate(a);this._hasPushState?i(window).bind("popstate",this.checkUrl):this._wantsHashChange&&"onhashchange"in window&&!b?i(window).bind("hashchange",this.checkUrl):this._wantsHashChange&&(this._checkUrlInterval=setInterval(this.checkUrl,
this.interval));this.fragment=a;a=window.location;b=a.pathname==this.options.root;if(this._wantsHashChange&&this._wantsPushState&&!this._hasPushState&&!b)return this.fragment=this.getFragment(null,!0),window.location.replace(this.options.root+"#"+this.fragment),!0;this._wantsPushState&&this._hasPushState&&b&&a.hash&&(this.fragment=this.getHash().replace(s,""),window.history.replaceState({},document.title,a.protocol+"//"+a.host+this.options.root+this.fragment));if(!this.options.silent)return this.loadUrl()},
stop:function(){i(window).unbind("popstate",this.checkUrl).unbind("hashchange",this.checkUrl);clearInterval(this._checkUrlInterval);m.started=!1},route:function(a,b){this.handlers.unshift({route:a,callback:b})},checkUrl:function(){var a=this.getFragment();a==this.fragment&&this.iframe&&(a=this.getFragment(this.getHash(this.iframe)));if(a==this.fragment)return!1;this.iframe&&this.navigate(a);this.loadUrl()||this.loadUrl(this.getHash())},loadUrl:function(a){var b=this.fragment=this.getFragment(a);return f.any(this.handlers,
function(a){if(a.route.test(b))return a.callback(b),!0})},navigate:function(a,b){if(!m.started)return!1;if(!b||!0===b)b={trigger:b};var c=(a||"").replace(s,"");this.fragment!=c&&(this._hasPushState?(0!=c.indexOf(this.options.root)&&(c=this.options.root+c),this.fragment=c,window.history[b.replace?"replaceState":"pushState"]({},document.title,c)):this._wantsHashChange?(this.fragment=c,this._updateHash(window.location,c,b.replace),this.iframe&&c!=this.getFragment(this.getHash(this.iframe))&&(b.replace||
this.iframe.document.open().close(),this._updateHash(this.iframe.location,c,b.replace))):window.location.assign(this.options.root+a),b.trigger&&this.loadUrl(a))},_updateHash:function(a,b,c){c?a.replace(a.toString().replace(/(javascript:|#).*$/,"")+"#"+b):a.hash=b}});var v=g.View=function(a){this.cid=f.uniqueId("view");this._configure(a||{});this._ensureElement();this.initialize.apply(this,arguments);this.delegateEvents()},F=/^(\S+)\s*(.*)$/,w="model,collection,el,id,attributes,className,tagName".split(",");
f.extend(v.prototype,k,{tagName:"div",$:function(a){return this.$el.find(a)},initialize:function(){},render:function(){return this},remove:function(){this.$el.remove();return this},make:function(a,b,c){a=document.createElement(a);b&&i(a).attr(b);c&&i(a).html(c);return a},setElement:function(a,b){this.$el&&this.undelegateEvents();this.$el=a instanceof i?a:i(a);this.el=this.$el[0];!1!==b&&this.delegateEvents();return this},delegateEvents:function(a){if(a||(a=n(this,"events"))){this.undelegateEvents();
for(var b in a){var c=a[b];f.isFunction(c)||(c=this[a[b]]);if(!c)throw Error('Method "'+a[b]+'" does not exist');var d=b.match(F),e=d[1],d=d[2],c=f.bind(c,this),e=e+(".delegateEvents"+this.cid);""===d?this.$el.bind(e,c):this.$el.delegate(d,e,c)}}},undelegateEvents:function(){this.$el.unbind(".delegateEvents"+this.cid)},_configure:function(a){this.options&&(a=f.extend({},this.options,a));for(var b=0,c=w.length;b<c;b++){var d=w[b];a[d]&&(this[d]=a[d])}this.options=a},_ensureElement:function(){if(this.el)this.setElement(this.el,
!1);else{var a=n(this,"attributes")||{};this.id&&(a.id=this.id);this.className&&(a["class"]=this.className);this.setElement(this.make(this.tagName,a),!1)}}});o.extend=r.extend=u.extend=v.extend=function(a,b){var c=G(this,a,b);c.extend=this.extend;return c};var H={create:"POST",update:"PUT","delete":"DELETE",read:"GET"};g.sync=function(a,b,c){var d=H[a];c||(c={});var e={type:d,dataType:"json"};c.url||(e.url=n(b,"url")||t());if(!c.data&&b&&("create"==a||"update"==a))e.contentType="application/json",
e.data=JSON.stringify(b.toJSON());g.emulateJSON&&(e.contentType="application/x-www-form-urlencoded",e.data=e.data?{model:e.data}:{});if(g.emulateHTTP&&("PUT"===d||"DELETE"===d))g.emulateJSON&&(e.data._method=d),e.type="POST",e.beforeSend=function(a){a.setRequestHeader("X-HTTP-Method-Override",d)};"GET"!==e.type&&!g.emulateJSON&&(e.processData=!1);return i.ajax(f.extend(e,c))};g.wrapError=function(a,b,c){return function(d,e){e=d===b?e:d;a?a(b,e,c):b.trigger("error",b,e,c)}};var x=function(){},G=function(a,
b,c){var d;d=b&&b.hasOwnProperty("constructor")?b.constructor:function(){a.apply(this,arguments)};f.extend(d,a);x.prototype=a.prototype;d.prototype=new x;b&&f.extend(d.prototype,b);c&&f.extend(d,c);d.prototype.constructor=d;d.__super__=a.prototype;return d},n=function(a,b){return!a||!a[b]?null:f.isFunction(a[b])?a[b]():a[b]},t=function(){throw Error('A "url" property or function must be specified');}}).call(this);

/*
 * jQuery Templates Plugin 1.0.0pre
 * http://github.com/jquery/jquery-tmpl
 * Requires jQuery 1.4.2
 *
 * Copyright 2011, Software Freedom Conservancy, Inc.
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 */
(function(a){var r=a.fn.domManip,d="_tmplitem",q=/^[^<]*(<[\w\W]+>)[^>]*$|\{\{\! /,b={},f={},e,p={key:0,data:{}},i=0,c=0,l=[];function g(g,d,h,e){var c={data:e||(e===0||e===false)?e:d?d.data:{},_wrap:d?d._wrap:null,tmpl:null,parent:d||null,nodes:[],calls:u,nest:w,wrap:x,html:v,update:t};g&&a.extend(c,g,{nodes:[],parent:d});if(h){c.tmpl=h;c._ctnt=c._ctnt||c.tmpl(a,c);c.key=++i;(l.length?f:b)[i]=c}return c}a.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(f,d){a.fn[f]=function(n){var g=[],i=a(n),k,h,m,l,j=this.length===1&&this[0].parentNode;e=b||{};if(j&&j.nodeType===11&&j.childNodes.length===1&&i.length===1){i[d](this[0]);g=this}else{for(h=0,m=i.length;h<m;h++){c=h;k=(h>0?this.clone(true):this).get();a(i[h])[d](k);g=g.concat(k)}c=0;g=this.pushStack(g,f,i.selector)}l=e;e=null;a.tmpl.complete(l);return g}});a.fn.extend({tmpl:function(d,c,b){return a.tmpl(this[0],d,c,b)},tmplItem:function(){return a.tmplItem(this[0])},template:function(b){return a.template(b,this[0])},domManip:function(d,m,k){if(d[0]&&a.isArray(d[0])){var g=a.makeArray(arguments),h=d[0],j=h.length,i=0,f;while(i<j&&!(f=a.data(h[i++],"tmplItem")));if(f&&c)g[2]=function(b){a.tmpl.afterManip(this,b,k)};r.apply(this,g)}else r.apply(this,arguments);c=0;!e&&a.tmpl.complete(b);return this}});a.extend({tmpl:function(d,h,e,c){var i,k=!c;if(k){c=p;d=a.template[d]||a.template(null,d);f={}}else if(!d){d=c.tmpl;b[c.key]=c;c.nodes=[];c.wrapped&&n(c,c.wrapped);return a(j(c,null,c.tmpl(a,c)))}if(!d)return[];if(typeof h==="function")h=h.call(c||{});e&&e.wrapped&&n(e,e.wrapped);i=a.isArray(h)?a.map(h,function(a){return a?g(e,c,d,a):null}):[g(e,c,d,h)];return k?a(j(c,null,i)):i},tmplItem:function(b){var c;if(b instanceof a)b=b[0];while(b&&b.nodeType===1&&!(c=a.data(b,"tmplItem"))&&(b=b.parentNode));return c||p},template:function(c,b){if(b){if(typeof b==="string")b=o(b);else if(b instanceof a)b=b[0]||{};if(b.nodeType)b=a.data(b,"tmpl")||a.data(b,"tmpl",o(b.innerHTML));return typeof c==="string"?(a.template[c]=b):b}return c?typeof c!=="string"?a.template(null,c):a.template[c]||a.template(null,q.test(c)?c:a(c)):null},encode:function(a){return(""+a).split("<").join("&lt;").split(">").join("&gt;").split('"').join("&#34;").split("'").join("&#39;")}});a.extend(a.tmpl,{tag:{tmpl:{_default:{$2:"null"},open:"if($notnull_1){__=__.concat($item.nest($1,$2));}"},wrap:{_default:{$2:"null"},open:"$item.calls(__,$1,$2);__=[];",close:"call=$item.calls();__=call._.concat($item.wrap(call,__));"},each:{_default:{$2:"$index, $value"},open:"if($notnull_1){$.each($1a,function($2){with(this){",close:"}});}"},"if":{open:"if(($notnull_1) && $1a){",close:"}"},"else":{_default:{$1:"true"},open:"}else if(($notnull_1) && $1a){"},html:{open:"if($notnull_1){__.push($1a);}"},"=":{_default:{$1:"$data"},open:"if($notnull_1){__.push($.encode($1a));}"},"!":{open:""}},complete:function(){b={}},afterManip:function(f,b,d){var e=b.nodeType===11?a.makeArray(b.childNodes):b.nodeType===1?[b]:[];d.call(f,b);m(e);c++}});function j(e,g,f){var b,c=f?a.map(f,function(a){return typeof a==="string"?e.key?a.replace(/(<\w+)(?=[\s>])(?![^>]*_tmplitem)([^>]*)/g,"$1 "+d+'="'+e.key+'" $2'):a:j(a,e,a._ctnt)}):e;if(g)return c;c=c.join("");c.replace(/^\s*([^<\s][^<]*)?(<[\w\W]+>)([^>]*[^>\s])?\s*$/,function(f,c,e,d){b=a(e).get();m(b);if(c)b=k(c).concat(b);if(d)b=b.concat(k(d))});return b?b:k(c)}function k(c){var b=document.createElement("div");b.innerHTML=c;return a.makeArray(b.childNodes)}function o(b){return new Function("jQuery","$item","var $=jQuery,call,__=[],$data=$item.data;with($data){__.push('"+a.trim(b).replace(/([\\'])/g,"\\$1").replace(/[\r\t\n]/g," ").replace(/\$\{([^\}]*)\}/g,"{{= $1}}").replace(/\{\{(\/?)(\w+|.)(?:\(((?:[^\}]|\}(?!\}))*?)?\))?(?:\s+(.*?)?)?(\(((?:[^\}]|\}(?!\}))*?)\))?\s*\}\}/g,function(m,l,k,g,b,c,d){var j=a.tmpl.tag[k],i,e,f;if(!j)throw"Unknown template tag: "+k;i=j._default||[];if(c&&!/\w$/.test(b)){b+=c;c=""}if(b){b=h(b);d=d?","+h(d)+")":c?")":"";e=c?b.indexOf(".")>-1?b+h(c):"("+b+").call($item"+d:b;f=c?e:"(typeof("+b+")==='function'?("+b+").call($item):("+b+"))"}else f=e=i.$1||"null";g=h(g);return"');"+j[l?"close":"open"].split("$notnull_1").join(b?"typeof("+b+")!=='undefined' && ("+b+")!=null":"true").split("$1a").join(f).split("$1").join(e).split("$2").join(g||i.$2||"")+"__.push('"})+"');}return __;")}function n(c,b){c._wrap=j(c,true,a.isArray(b)?b:[q.test(b)?b:a(b).html()]).join("")}function h(a){return a?a.replace(/\\'/g,"'").replace(/\\\\/g,"\\"):null}function s(b){var a=document.createElement("div");a.appendChild(b.cloneNode(true));return a.innerHTML}function m(o){var n="_"+c,k,j,l={},e,p,h;for(e=0,p=o.length;e<p;e++){if((k=o[e]).nodeType!==1)continue;j=k.getElementsByTagName("*");for(h=j.length-1;h>=0;h--)m(j[h]);m(k)}function m(j){var p,h=j,k,e,m;if(m=j.getAttribute(d)){while(h.parentNode&&(h=h.parentNode).nodeType===1&&!(p=h.getAttribute(d)));if(p!==m){h=h.parentNode?h.nodeType===11?0:h.getAttribute(d)||0:0;if(!(e=b[m])){e=f[m];e=g(e,b[h]||f[h]);e.key=++i;b[i]=e}c&&o(m)}j.removeAttribute(d)}else if(c&&(e=a.data(j,"tmplItem"))){o(e.key);b[e.key]=e;h=a.data(j.parentNode,"tmplItem");h=h?h.key:0}if(e){k=e;while(k&&k.key!=h){k.nodes.push(j);k=k.parent}delete e._ctnt;delete e._wrap;a.data(j,"tmplItem",e)}function o(a){a=a+n;e=l[a]=l[a]||g(e,b[e.parent.key+n]||e.parent)}}}function u(a,d,c,b){if(!a)return l.pop();l.push({_:a,tmpl:d,item:this,data:c,options:b})}function w(d,c,b){return a.tmpl(a.template(d),c,b,this)}function x(b,d){var c=b.options||{};c.wrapped=d;return a.tmpl(a.template(b.tmpl),b.data,c,b.item)}function v(d,c){var b=this._wrap;return a.map(a(a.isArray(b)?b.join(""):b).filter(d||"*"),function(a){return c?a.innerText||a.textContent:a.outerHTML||s(a)})}function t(){var b=this.nodes;a.tmpl(null,null,null,this).insertBefore(b[0]);a(b).remove()}})(jQuery);

/*!
 * jQuery TextChange Plugin
 * http://www.zurb.com/playground/jquery-text-change-custom-event
 *
 * Copyright 2010, ZURB
 * Released under the MIT License
 */
 (function(a){a.event.special.textchange={setup:function(){a(this).data("lastValue",this.contentEditable==="true"?a(this).html():a(this).val());a(this).bind("keyup.textchange",a.event.special.textchange.handler);a(this).bind("cut.textchange paste.textchange input.textchange",a.event.special.textchange.delayedHandler)},teardown:function(){a(this).unbind(".textchange")},handler:function(){a.event.special.textchange.triggerIfChanged(a(this))},delayedHandler:function(){var c=a(this);setTimeout(function(){a.event.special.textchange.triggerIfChanged(c)},
 25)},triggerIfChanged:function(a){var b=a[0].contentEditable==="true"?a.html():a.val();b!==a.data("lastValue")&&(a.trigger("textchange",[a.data("lastValue")]),a.data("lastValue",b))}};a.event.special.hastext={setup:function(){a(this).bind("textchange",a.event.special.hastext.handler)},teardown:function(){a(this).unbind("textchange",a.event.special.hastext.handler)},handler:function(c,b){b===""&&b!==a(this).val()&&a(this).trigger("hastext")}};a.event.special.notext={setup:function(){a(this).bind("textchange",
 a.event.special.notext.handler)},teardown:function(){a(this).unbind("textchange",a.event.special.notext.handler)},handler:function(c,b){a(this).val()===""&&a(this).val()!==b&&a(this).trigger("notext")}}})(jQuery);

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");

var searchMapParams = Backbone.Model.extend({
	defaults:{
		center: [45.034942, 38.976032],
		zoom: 13,
		fullscreen: false
	}
});

/**
 * Представление для метки
 */
var searchMapPlacemarkView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;
		self.mapView = options.mapView;
		self.ymap = options.mapView.ymap;

		self.createPlacemark();

		self.on('change', function(){
			self.placemark.properties.set({place: self.model.toJSON()});
		});

		self.model.on('change:yIcon', function(){
			self.placemark.properties.set({iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>'});
		});

		self.model.on('change:yColor change:yState', function(){
			self.placemark.options.set({iconImageHref: self.getIconSrc()});
		});

		// Скрытие неактивной метки
		self.model.on('change:yState', function(model, yState){
			// if (window.socialMapSearch.hasOne){
			// 	return;
			// }

			if (yState == 'active'){
				self.mapView.addPlaceMarkToBuffer(self.placemark);
			}else{
				self.mapView.removePlacemark(self.placemark);
			}
		});
	},

	/**
	 * Возвращает класс дял иконки
	 */
	getIconClass: function(){
		return 'i-'+this.model.get('yIcon');
	},

	/**
	 * Возвращает ссылку на фон плейсмарка
	 */
	getIconSrc: function(){
		var self = this;

		if (self.model.get('yState') == 'active'){
			return '/bundles/krdopendata/i/placemark-bg-'+self.model.get('yColor')+'.png';
		}else{
			return '/bundles/krdopendata/i/placemark-bg.png';
		}
	},

	// Первоначальное создание метки и нанесение ее на карту
	createPlacemark: function(){
		var self = this;

		self.placemark = new ymaps.Placemark(self.model.get('yCoords'),
			{
				iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>',
				place: self.model.toJSON()
			},
			{
				iconLayout:'default#imageWithContent',
				iconImageHref: self.getIconSrc(),
				iconImageSize: [37, 36],
				iconImageOffset: [0, -36],

				hideIconOnBalloonOpen: true,
				balloonPane: 'movableOuters',
				balloonShadowPane: 'movableOuters',
				balloonShadow: false,
				balloonOffset: [100, 13],
				balloonMaxWidth: 196,
				balloonMinWidth: 196,
				balloonContentLayout: self.mapView.yPlacemarkTLF,

				// overlayFactory: 'default#interactiveGraphics'
			}
		);

		self.mapView.addPlaceMarkToBuffer(self.placemark);
	}
});

/**
 * Представление карты
 */
var searchMapView = Backbone.View.extend({
	// Полигоны районов города
	regions: {},

	initialize: function(){
		var self = this;

		// Параметры отображения карты
		self.params = new searchMapParams();

		// При добавлении учреждения в общую коллекцию - создаем плейсмарк для карты
		self.on('placeAdd', function(model){
			var view = new searchMapPlacemarkView({model:model, mapView:self});
		});

		// Мигание карты при определенных событиях
		self.on('highlight', function(){
			self.$el.closest('.sm-map-wrap').addClass('highlighted');
			!!this.__timer && clearTimeout(this.__timer);
			this.__timer = setTimeout(function(){
				self.$el.closest('.sm-map-wrap').removeClass('highlighted');
			}, 300);
		});

        // Отрисовка карты
        self.on('renderMap', function() {
            self.ymap.geoObjects.add(self.clusterer);
        });

		window.___ymapsLoadCallback = function(){self.initYMap()};
		$.getScript('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU&onload=___ymapsLoadCallback');
	},

	// Добавление кнопки разворачивания на весь экран
	initFullScreen: function(){
		var self = this;

		var $button = $('<a href="javascript:$.noop();" class="full-screen-btn btn btn-deep-blue"></a>');
		$button.appendTo('.sm-map-wrap');

		var __onFSChange = function(){
			if (self.params.get('fullscreen')){
				$button.text('Свернуть');
			}else{
				$button.text('Развернуть');
			}
		};

		// Клик по кнопке
		$button.on('click', function(e){
			self.params.set({fullscreen:!self.params.get('fullscreen')});
		});

		__onFSChange();

		// Изменение полноэкранного режима
		self.params.on('change:fullscreen', function(model, fullscreen){
			__onFSChange();

			if (fullscreen){
				$('body').addClass('fullscreen');
			}else{
				$('body').removeClass('fullscreen');
			}

			self.ymap.container.fitToViewport();
		});
	},

	/**
	 * Обработка событий
	 */
	initEvents: function(){
		var self = this;

		// Изменение центра карты
		self.params.on('change:center', function(params, center){
			self.ymap.setCenter(center);
		});

		// Изменение зума карты
		self.params.on('change:zoom', function(params, zoom){
			self.ymap.setZoom(zoom);
		});

		// Запонинание позиции и зума карты
		self.ymap.events.add('boundschange', function(e){
			if (e.get('oldCenter') != e.get('newCenter')){
				self.params.set({center:e.get('newCenter')}, {silent:true});
			}

			if (e.get('oldZoom') != e.get('newZoom')){
				self.params.set({zoom:e.get('newZoom')}, {silent:true});
			}
		});

		// При выборе района города - показываем этот район

		window.socialMapSearch.searchParams.on('change:regions', function(model, regions){
			var region = regions[0];

			if (self.regions[region] === undefined){
				return;
			}

			var pol = new ymaps.Polygon([self.regions[region]]);
			self.ymap.geoObjects.add(pol);
			self.ymap.setBounds(pol.geometry.getBounds());
			self.ymap.geoObjects.remove(pol);
		});
	},

	// Закрытие любого балуна при клике на пустом месте карты
	initCloseOnEmptyClick: function(){
		var self = this;

		self.ymap.events.add('click', function(){
			if (!!self.ymap.balloon) self.ymap.balloon.close();
		});
	},

	/**
	 * Создает шаблон плейсмарки
	 */
	precachePlacemark: function(){
		var self = this;

		self.yPlacemarkTLF = ymaps.templateLayoutFactory.createClass(
			'<span class="ttl">$[properties.place.title]</span>' +
			'<div class="descr">' +
				'<div class="descr-row"><span>Категория:</span> $[properties.place.subcategories_txt]</div>' +
				'<div class="descr-row"><span>Адрес:</span> $[properties.place.address]</div>' +
				'<div class="descr-row"><span>Телефон:</span> $[properties.place.phone]</div>' +
				'<a href="$[properties.place.href]" class="button">Подробнее<i></i></a>' +
			'</div>',
			{
				build: function() {
						var layout = this;
						layout.constructor.superclass.build.call(this);
						var $this = $(this.getParentElement());
						var $body = $('<div class="sm-balloon-wrap-outer"><div class="sm-balloon-wrap-inner"></div></div>');
						var $oldBalloonBody = $this.closest('.ymaps-b-balloon');


						$oldBalloonBody.after($body);
						$this.appendTo($body.find('.sm-balloon-wrap-inner'));
						$oldBalloonBody.remove();
						setTimeout(function(){
							$body.width(236);
						}, 100);
					}
			}
		);
	},

	/**
	 * Инициализация кластера меток
	 */
	initClusterer: function(){
		var self = this;

		self.clusterer = new ymaps.Clusterer({
			gridSize: 48,
			maxZoom: 16,
			clusterOpenBalloonOnClick: false,
	        clusterNumbers: [1000]
		});
	},

	/**
	 * Добавляет метку в буффер, но на карту они добавляются с задежкой, для улучшения быстродействия
	 * * Нифига. Пока не реализовано
	 */
	addPlaceMarkToBuffer: function(placemark){
		var self = this;

		self.clusterer.add(placemark);
	},

	/**
	 * Убирает метку с карты
	 */
	removePlacemark: function(placemark){
		var self = this;

		self.clusterer.remove(placemark);
	},

	/**
	 * Первичная инициализация карты
	 */
	initYMap: function(){
		var self = this;

		// Создание карты
		self.ymap = new ymaps.Map(self.$el.attr('id'), {
			center: self.params.get('center'),
			zoom: self.params.get('zoom'),
			behaviors: ['default', 'scrollZoom'],
			overlayFactory: ymaps.geoObject.overlayFactory.interactiveGraphics
		});

		// Добавление контролов
		self.ymap.controls
			.add('typeSelector', {top:40, right:7})
			.add('scaleLine')
			.add('mapTools')
			.add('zoomControl')
			// .add('trafficControl')
			.add(new ymaps.control.RouteEditor())
			.add(new ymaps.control.MiniMap({type: 'yandex#publicMap'}));

		self.initFullScreen();
		self.initEvents();
		self.precachePlacemark();
		self.initCloseOnEmptyClick();
		self.initClusterer();

		self.trigger('initialized');
	},

	/**
	 * Добавялет полигон района города в список
	 */
	appendRegion: function(region){
		var self = this;

		if (!$.isArray(region.polygon)){
			return;
		}

		$.each(region.polygon, function(key, val){
			$.each(val, function(k2, v2){
				val[k2] = parseFloat(v2, 10);
			});

			region.polygon[key] = val;
		});


		self.regions[region.id] = region.polygon;
	}
});

/**
 * Представление результата поиска
 * @type {[type]}
 */
var placeResultView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;

		self.setElement($('#sm-r-item-tpl').tmpl(self.model.toJSON()));
	}
});


/**
 * Пагинатор результатов поиска
 */
var pagesModel = Backbone.Model.extend({
	defaults: {
		onPage: 3,
		totalCount: 0,
		page: 1,

		pagesCount: 1,
		slice: [0, 0],

		extended: true,
		slideLeft: 0
	},

	initialize: function(){
		var self = this;

		self.on('change:onPage change:totalCount change:page', self.reCalc, self);
	},

	/**
	 * Пересчет параметров пагинации
	 */
	reCalc: function(){
		var self = this;

		var page = self.get('page');
		var onPage = self.get('onPage');
		var totalCount = self.get('totalCount');

		var pagesCount = Math.ceil(totalCount/onPage);

		var slice = [page*onPage - onPage, page*onPage];

		self.set({
			pagesCount: pagesCount,
			slice: slice
		});
	}
});

/**
 * Представление пагинатора
 */
var resultPaginator = Backbone.View.extend({
	maxWidth: 10*37,

	initialize: function(){
		var self = this;

		self.$list = self.$('.pages-list .float-wrap .w');
		self.$arLeft = self.$('.page-link.l');
		self.$arRight = self.$('.page-link.r');

		self.model = new pagesModel();

		// Изменение параметров пагинации - перерисовка всего пагинатора
		self.model.on('change:totalCount change:onPage', self.render, self);

		// Изменение текущей страницы - выделяем ее
		self.model.on('change:page', self.markActivePage, self);

		// Изменение текущей страницы - генерация внещнего события
		self.model.on('change:page', function(model, page){
			self.trigger('pageChanged', page, self.model.get('slice'));
		});

		// Изменение представления пагинатора
		self.model.on('change:extended', function(model, extended){
			if (extended){
				self.showExtended();
			}else{
				self.hideExtended();
			}
		});

		// Щелчки по "стрелочкам"
		self.$arLeft.on('click', function(e){
			e.preventDefault();

			self.slideLeft();
		});

		self.$arRight.on('click', function(e){
			e.preventDefault();

			self.slideRight();
		});

		// Анимация прокрутки пагинатора
		self.model.on('change:slideLeft', function(model, slideLeft){
			self.$list.stop().animate({marginLeft:slideLeft}, 100);
		});
	},

	/**
	 * Установка параметров пагинации
	 */
	setParams: function(totalCount, onPage){
		var self = this;

		self.model.set({
			totalCount: totalCount,
			onPage: onPage,
			page: 1
		});
	},

	/**
	 * Обычное представление пагинатора
	 */
	hideExtended: function(){
		var self = this;

		self.$arLeft.hide();
		self.$arRight.hide();
	},

	/**
	 * Расширенное представление пагинатора - "стрелочки побокам"
	 */
	showExtended: function(){
		var self = this;

		self.$arLeft.hide();
		self.$arRight.hide();
	},

	/**
	 * Прокрутка пагинатора влево
	 */
	slideLeft: function(){
		var self = this;

		var sl = self.model.get('slideLeft');

		if (sl < 0)
			self.model.set({slideLeft:sl + 37});
	},

	/**
	 * Прокрутка пагинатора вправо
	 */
	slideRight: function(){
		var self = this;

		var sl = self.model.get('slideLeft');
		if (sl/37 + (self.model.get('pagesCount') - Math.floor(self.maxWidth/37)) > 0)
			self.model.set({slideLeft:sl - 37});
	},

	/**
	 * Отрисовка пагинатора в соответствии с количеством элементов
	 */
	render: function(){
		var self = this;

        self.$el.show();
		self.$list.empty();

		if (self.model.get('pagesCount') <= 1){
            self.$('.pages-title, .pages-list').hide();
			return;
		}else{
			self.$('.pages-title, .pages-list').show();
		}

		for(var i = 0, count = self.model.get('pagesCount'); i < count; i++){
			var $a = $('<a class="page-link btn btn-blue" href="#">'+(i+1)+'</a>');

			(function(page){
				$a.on('click', function(e){
					e.preventDefault();

					self.model.set({page: page});
				});
			}(i+1))

			$a.appendTo(self.$list);
		}

		var width = self.model.get('pagesCount')*37;

		if (width > self.maxWidth){
			width = self.maxWidth;
			self.model.set({extended: true});
		}else{
			self.model.set({extended: false});
		}

		self.$list.parent().width(width);

		self.markActivePage();
		self.trigger('pageChanged', self.model.get('page'), self.model.get('slice'));
	},

	/**
	 * Отмечает активную страницу
	 */
	markActivePage: function(){
		var self = this;

		self.$list.find('.page-link').removeClass('page-active');
		self.$list.find('.page-link').eq(self.model.get('page')-1).addClass('page-active');
	}
});


/**
 * Список результатов поиска
 */
var resultsView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.$title = self.$el.children('.ttl');
		self.$title.data('success', self.$title.html());

		self.$list = self.$('.sm-r-list');
		self.$paginator = new resultPaginator({el:self.$('.sm-r-pages')});

		self.on('placeAdd', function(model){
			self.onPlaceAdd(model);
			self.resortResults();
		});

		// Список элементов-результатов
		self.$resultsList = $('');
		self.on('resortResults', self.resortResults, self);
		self.$paginator.on('pageChanged', function(page, slice){
			self.$resultsList.hide();
			self.$resultsList.slice(slice[0], slice[1]).show();
		});
	},

	// Добавление учреждения
	onPlaceAdd: function(model){
		var self = this;

		var view = new placeResultView({model:model});
		model._resultView = view;
		view.$el.appendTo(self.$list);
	},

	/**
	 * Скрытие неактивных элементов и назначение классов активным
	 */
	resortResults: function(){
		var self = this;

		if (window.socialMapSearch.placesLoaded !== true)
			return;

		_.each(window.socialMapSearch.places.where({yState:'inactive'}), function(place){
			place._resultView.$el.hide();
		});

		var i = 0, c = 0;;
		var $lr1 = self.$list.children('.sm-r-item:first');
		var height = 0;
		var $prev1 = null;
		var $prev2 = null;
		var $prev3 = null;
		var showOnPage = 9;

		self.$resultsList = $('');

		var activePlaces = window.socialMapSearch.places.where({yState:'active'});

		self.$list.children('.sm-r-item').removeClass('r-1 r-2 r-3 r-lr');

		_.each(activePlaces, function(place){
			self.$resultsList = self.$resultsList.add(place._resultView.$el);
			place._resultView.$el.show().css({height:'auto'});
			height = Math.max(height, place._resultView.$el.height());

			i++;
			c++;
			place._resultView.$el.addClass('r-'+i);

			if (i == 1){
				$lr1 = place._resultView.$el;
			}

			if (i == 3){
				place._resultView.$el.height(height);
				if (!!$prev1) $prev1.height(height);
				if (!!$prev2) $prev2.height(height);

				i = 0;
				$prev1 = null;
				$prev2 = null;
				$prev3 = null;
				height = 0;
			}else{
				$prev3 = $prev2;
				$prev2 = $prev1;
				$prev1 = place._resultView.$el;
			}

			if (c % (showOnPage - 2) == 0){
				// place._resultView.$el.addClass('r-lr');
				// place._resultView.$el.nextAll().slice(0,2).addClass('r-lr');
			}
		});

		if (!!$prev1) $prev1.height(height);
		if (!!$prev2) $prev2.height(height);
		if (!!$prev3) $prev3.height(height);

		var i = 1;
		self.$resultsList.each(function(){
			if (i%7 == 0 || i%8 == 0 || i%9 == 0){
				$(this).addClass('r-lr');
			}

			if (i%9 == 0){
				i = 1;
			}else{
				i++;
			}
		});

		$lr1.addClass('r-lr');
		$lr1.nextAll().addClass('r-lr');

		// Смена заголовка
		self.$title.html( self.$title.data(activePlaces.length > 0 ? 'success' : 'fail') );

		self.$paginator.setParams(activePlaces.length, showOnPage);
	},

	/**
	 * Скрывает весь блок
	 */
	instantHide: function(){
		var self = this;

		self.$el.hide();
	}
});

var defaultModel = Backbone.Model.extend({
	// Приводит массив к целым числам
	arrToInt: function(name){
		var self = this;

		var arr = self.get(name);
		_.each(arr, function(value, i){
			arr[i] = parseInt(arr[i], 10);
		});

		return arr;
	},

	// Приводит массив к float`ам
	arrToFloat: function(name){
		var self = this;

		var arr = self.get(name);
		_.each(arr, function(value, i){
			arr[i] = parseFloat(arr[i], 10);
		});

		return arr;
	}
});

/**
 * Хранилище параметров поиска
 * @type {[type]}
 */
var searchParamsModel = defaultModel.extend({
	defaults:{
		text: '',
		categories: [],
		subcategories: [],
		regions: []
	}
});

/** ****************************************************************************** **/

/**
 * Представление для селектора категории поиска
 */
var searchCategoriesView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.$itemTpl = $('#sm-fl-item-tpl');
		self.$list = self.$('.lst');
		self.collection = new searchCategoriesCollection();

		self.collection.on('add', self.onAddItem, self);
	},

	/**
	 * Добавляет категорию в список
	 * @param  {object} category Обьект категории или массив с категориями
	 */
	appendNew: function(category){
		var self = this;

		self.collection.add(category);
	},

	/**
	 * Каллбек на добавление категории
	 */
	onAddItem: function(model){
		var self = this;

		var $tpl = self.$itemTpl.tmpl(model.toJSON());
		var view = new searchCategoriesItemView({el:$tpl, model:model});

		self.$list.append($tpl);

		self.$list.find('.sm-fl-item').removeClass('last');
		self.$list.find('.sm-fl-item:nth-child(4n)').addClass('last');
	}
});

/**
 * Представление элемента в селекторе категорий
 */
var searchCategoriesItemView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;

		// Клик по кнопке
		self.$('.sm-icon, .sm-title').on('click', function(e){
			e.preventDefault();

			if (!window.socialMapSearch.searchEnabled){
				window.location.href = '/opendata/list/#!/category-'+self.model.id;
			}else{
				// self.model.set({selected:!self.model.get('selected')});
				self.model.set({selected:true});
			}

			window.socialMapSearch.resetAll();
		});

		self.model.on('change:selected', self.render, self);
	},

	/**
	 * Отрисовка состояния
	 */
	render: function(){
		var self = this;

		if (self.model.get('selected')){
			self.$el.addClass('current');
		}else{
			self.$el.removeClass('current');
		}
	}
});

/**
 * Модель для категории поиска
 */
var searchCategoriesModel = Backbone.Model.extend({
	defaults:{
		title: 'Заголовок',
		color: 'blue',
		icon: 'health',
		selected: false
	},

	initialize: function(){
		var self = this;

		self.set({
			id: parseInt(self.get('id'), 10)
		});
	}
});

/**
 * Коллекция моделей категорий поиска
 * @type {[type]}
 */
var searchCategoriesCollection = Backbone.Collection.extend({
	model: searchCategoriesModel,

	initialize: function(){
		var self = this;

		self.noOneMore();
	},

	/**
	 * Включает состояние при кором может быть выбрана только одна категория
	 */
	noOneMore: function(){
		var self = this;

		self.on('change:selected', function(model){
			if (!model.get('selected'))
				return;

			self.each(function(item){
				if (item.cid == model.cid)
					return;

				item.set({selected:false});
			});
		});
	}
});

/** ****************************************************************************** **/

/**
 * Модель одного учреждения
 */
var placeModel = defaultModel.extend({
	defaults:{
		title: '',
		categories: [],
		subcategories: [],
		region: -1,
		subcategories_txt: '',
		address: '',
		phone: '',
		href: '',
		content: '',

		yState: 'active',
		yCoords: [45.034942, 38.976032],
		yIcon: 'health',
		yColor: 'blue',
	},

	initialize: function(){
		var self = this;

		self.set({
			region: parseInt(self.get('region'), 10),
			categories: self.arrToInt('categories'),
			subcategories: self.arrToInt('subcategories'),
			yCoords: self.arrToFloat('yCoords'),
		});

		self.on('change:subcategories', self.subCatsTxtUpdate, self);
		self.subCatsTxtUpdate();
	},

	// Обновление текстового представления категорий
	subCatsTxtUpdate: function(){
		var self = this;

		var txt = [];

		_.each(self.get('subcategories'), function(subcat){
			txt.push(window.socialMapSearch.subcategoriesList[subcat]);
		});

		self.set({subcategories_txt:txt.join(', ')});
	}
});

/**
 * Коллекция учреждений
 */
var placeCollection = Backbone.Collection.extend({
	model: placeModel
});

/** ****************************************************************************** **/

/**
 * Приложение для поиска по карте
 */
var SMSearchApp = Backbone.Router.extend({
	searchEnabled: true,
	hasOne: false,

	routes:{
		'!/category-:id': 'categorySelect'
	},

	initialize: function(){
		var self = this;

		self.searchParams = new searchParamsModel();
		self.initCategories();
		self.initFilters();
		self.initTextSearch();

		// Коллекция учреждений
		self.places = new placeCollection();
		self.searchParams.on('change', self.search, self);

		// Карта
		if ($('#socialmap').length > 0){
			self.searchMap = new searchMapView({el:$('#socialmap')});
				// Совместная инициализация
				self.searchMap.on('initialized', function(){

					self.search();
					self.trigger('initialized');
				});

				// Событие по добавлению учреждения
				self.places.on('add', function(model){
					self.searchMap.trigger('placeAdd', model);
				});

                // Отрисовка меток на карте по инициализации
                self.on('full-init', function() {
                    self.searchMap.trigger('renderMap');
                });
		}

		// Список результатов поиска
		self.results = new resultsView({el:$('.sm-results')});
			// Событие по добавлению учреждения
			self.places.on('add', function(model){
				self.results.trigger('placeAdd', model);
			});

			// Событие для инициации поиска
			self.searchParams.on('change', function(){
				self.results.trigger('resortResults');

                // Сохранение параметров поиска в куках
                if (!self.searchParams.get('text') || self.searchParams.get('text') == '') {
                    $.cookie('sm_params_text', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_text');
                } else {
                    $.cookie('sm_params_text', self.searchParams.get('text'), {expires:1, path:'/'});
                }

                if (!self.searchParams.get('subcategories') || self.searchParams.get('subcategories').length == 0) {
                    $.cookie('sm_params_subcategories', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_subcategories');
                } else {
                    $.cookie('sm_params_subcategories', self.searchParams.get('subcategories').join('|'), {expires:1, path:'/'});
                }
                if (!self.searchParams.get('regions') || self.searchParams.get('regions').length == 0) {
                    $.cookie('sm_params_regions', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_regions');
                } else {
                    $.cookie('sm_params_regions', self.searchParams.get('regions').join('|'), {expires:1, path:'/'});
                }
			});
	},

	/**
	 * Указывает на карте одно учреждение
	 */
	onePlace: function(placeId){
		var self = this;

		placeId = parseInt(placeId, 10);

		if (!placeId){
			return false;
		}

		self.searchEnabled = false;
		self.hasOne = true;

		self.places.each(function(place){  place.set({yState:'inactive'}); });
		self.places.get(placeId).set({yState:'active'});

		self.searchMap.params.set({
			center: self.places.get(placeId).get('yCoords'),
			zoom: 17
		});

		self.searchCategories.collection.get( self.places.get(placeId).get('categories')[0] ).set({selected:true});

		self.results.instantHide();
	},

	/**
	 * Инициализация строки поиска
	 */
	initTextSearch: function(){
		var self = this;

		// Текстовый инпут
		self.$textSearchInput = $('input.sm-text-search').on('textchange', function(){
			var text = $(this).val();

			!!this.__timeout && clearInterval(this.__timeout);
			this.__timeout = setTimeout(function(){
				self.searchParams.set({
					text: text
				});
			}, (text == '' ? 0 : 250));
		});

		// Кнопка очищения
		self.$textSearchClearCross = $('.sm-t-clear').on('click', function(e){
			e.preventDefault();

			self.$textSearchInput.val('').trigger('textchange');
		});

		// Скрытие этой кнопки при пустом инпуте
		self.$textSearchInput.on('textchange', function(){
			if ($(this).val() == ''){
				self.$textSearchClearCross.addClass('hidden');
			}else{
				self.$textSearchClearCross.removeClass('hidden');
			}
		});

		// Всплывающие подсказки
		self.$textSearchHintList = $('.sm-t-hint-list').on('clickoutside', function(){
			self.$textSearchHintList.hideList();
		});

		// Скрытие всплывающих подсказок
		self.$textSearchHintList.hideList = function(){
			self.$textSearchHintList.addClass('hidden');
			self.$textSearchHintList.children('a.hover').removeClass('hover');
		};

		// Ховер всплывающих подсказок
		$('.sm-t-hint-list').on('mouseover', 'a', function(){
			$('.sm-t-hint-list').children('a.hover').removeClass('hover');
			$(this).addClass('hover');
		});

		$('.sm-t-hint-list').on('mouseleave', 'a', function(){
			$('.sm-t-hint-list').children('a.hover').removeClass('hover');
		});

		// Навигация стрелками
		self.$textSearchInput.on('keyup', function(e){
			if (e.keyCode == 38 || e.keyCode == 40){
				if (!self.$textSearchHintList.is(':empty')){
					self.$textSearchHintList.removeClass('hidden');
				}

				var $curr = self.$textSearchHintList.children('.hover');

			}

			switch(e.keyCode){
				// UP
				case 38:
					var $currNew = $curr.removeClass('hover').prev('a');

					if ($curr.length == 0 || $currNew.length == 0){
						self.$textSearchHintList.children('a:last').addClass('hover');
					}else{
						$currNew.addClass('hover');
					}
				break;

				// DOWN
				case 40:
					var $currNew = $curr.removeClass('hover').next('a');

					if ($curr.length == 0 || $currNew.length == 0){
						self.$textSearchHintList.children('a:first').addClass('hover');
					}else{
						$currNew.addClass('hover');
					}
				break;

				// ENTER
				case 13:
					if (!self.$textSearchHintList.is(':empty') && !self.$textSearchHintList.hasClass('hidden')){
						self.$textSearchHintList.children('a.hover').click();
					}
				break;
			}
		});

		self.$textSearchInput.on('textchange', function(){
			var text = $(this).val();

			if (text == ''){
				self.$textSearchHintList.hideList();
			}else{
				self.$textSearchHintList.removeClass('hidden').empty();
				var counter = 0;
				self.places.each(function(place){
					var title = place.get('title');
					if (counter <= 10 && title.toLowerCase().indexOf(text) != -1){
						var $a = $('<a href="#">'+title.replace(new RegExp(text, 'ig'), '<b>$&</b>')+'</a>');

						(function($a, title){
							$a.on('click', function(e){
								e.preventDefault();

								self.$textSearchInput.val(title).trigger('textchange');
								$(this).removeClass('hover');
							});
						}($a, title));


						$a.appendTo(self.$textSearchHintList);
						counter++;
					}
				});

				if (counter == 0){
					self.$textSearchHintList.hideList();
				}
			}
		}).on('dblclick', function(){
			if (!self.$textSearchHintList.is(':empty')){
				self.$textSearchHintList.removeClass('hidden');
			}
		});

        var _cached = $.cookie('sm_params_text');
        if (!!_cached && _cached != '') {
            self.on('full-init', function() {
                self.$textSearchInput.val(_cached).trigger('textchange');
                self.$textSearchHintList.hideList();
            });
        }
	},

	/**
	 * Инициализация селектора категорий
	 */
	initCategories: function(){
		var self = this;

		self.searchCategories = new searchCategoriesView({el:$('.sm-filter-list')});

		self.searchCategories.collection.on('change:selected', function(){
			var ids = [];
			var selectedList = self.searchCategories.collection.where({selected:true});
			_.each(selectedList, function(item){
				ids.push(item.get('id'));
			});

			self.searchParams.set({categories:ids});
            $('.sm-r-pages .export.button').attr('href', '#!/export-' + ids[0]);
		});

		// Список наименований категорий и подкатегорий
		self.categoriesList = {};
		self.subcategoriesList = {};
		self.searchCategories.collection.on('add', function(model){
			self.categoriesList[model.get('id')] = model.get('title');

			_.each(model.get('subcategories'), function(subcat){
				self.subcategoriesList[subcat.id] = subcat.title;
			});
		});

		// Роут смены выбранной категории
		self.on('route:categorySelect', function(id){
			self.searchCategories.collection.mustBe = id;
			self.searchCategories.collection.get(id).set({selected:true});
		});

		// Выбор первой категории поумолчанию
		self.on('full-init', function(){
			if (self.searchEnabled && self.searchCategories.collection.where({selected:true}).length == 0){
				self.navigate('!/category-'+self.searchCategories.collection.at(0).get('id'), {trigger:true});
			}
		});
	},

	/**
	 * Инициализация под-фильтров
	 */
	initFilters: function(){
		var self = this;

		// Фильтр подкатегорий
		self.$selectSubcategory = $('.sm-filter-select select[name="subcategory"]').on('change', function(){
			if ($(this).val() != -1){
				self.searchParams.set({
					subcategories: [parseInt($(this).val(), 10)]
				});
			}else{
				self.searchParams.set({subcategories: []});
			}
		});

		self.searchParams.on('change:categories', self._fillSubcategories, self);
		self._fillSubcategories();

        var _cached = $.cookie('sm_params_subcategories');
        if (!!_cached && _cached != '') {
            _cached = parseInt(_cached.split('|').pop(), 10);
            self.on('full-init', function() {
                self.$selectSubcategory.val(_cached).trigger('change');
            });
        }

		// Фильтр районов
		self.$selectRegion = $('.sm-filter-select select[name="region"]').on('change', function(){
			var value = $(this).val();
			if (value != -1){
				self.searchParams.set({
					regions: [parseInt(value, 10)]
				});
			}else{
				self.searchParams.set({regions: []});
			}
		});

		// Убираем регионы, в которых нет ни одного учреждения
		self.on('full-init', function(){
			var allowedRegions = [];
			self.places.each(function(place){
				allowedRegions.push(place.get('region'));
			});

			self.$selectRegion.find('option').each(function(){
				var $this = $(this),
					id = parseInt($this.attr('value'), 10);
				if (id != -1 && $.inArray(id, allowedRegions) === -1){
					$this.remove();
				}
			});
		});

        var _cached = $.cookie('sm_params_regions');
        if (!!_cached && _cached != '') {
            _cached = parseInt(_cached.split('|').pop(), 10);
            self.on('full-init', function() {
                self.$selectRegion.val(_cached).trigger('change');
            });
        }

		// Кнопка сброса фильтра
		self.$filterResetBtn = $('.reset-filter').on('click', function(e){
			e.preventDefault();

			self.$selectSubcategory.val(-1).change();
			self.$selectRegion.val(-1).change();
		}).hide();

		// При смене глобальной категории - сбрасываем фильтр подкатегорий
		self.searchParams.on('change:categories', function(){
			self.searchParams.set({subcategories:[]});
		});

		// При смене фильтрации показываем/скрываем кнопку
		self.searchParams.on('change:subcategories change:regions', function(){
			if (self.searchParams.get('subcategories').length == 0 && self.searchParams.get('regions').length == 0){
				self.$filterResetBtn.stop().css({opacity:1}).fadeOut(200);
			}else{
				self.$filterResetBtn.stop().css({opacity:1}).fadeIn(200);
			}
		});
	},

	/**
	 * Заполняет список подкатегорий
	 */
	_fillSubcategories: function(){
		var self = this;

		self.$selectSubcategory.empty();
		self.$selectSubcategory.append('<option value="-1">-- Выберите подраздел --</option>');

		_.each(self.searchCategories.collection.where({selected:true}), function(item){
			_.each(item.get('subcategories'), function(subcat){
				self.$selectSubcategory.append('<option value="'+subcat.id+'">'+subcat.title+'</option>');
			});
		});
	},

	/**
	 * Сброс всех параметров поиска
	 */
	resetAll: function(){
		var self = this;

		self.$selectSubcategory.val(-1).change();
		self.$selectRegion.val(-1).change();
		self.$textSearchInput.val('').trigger('textchange');
	},

	/**
	 * Добавляет новое учреждение
	 */
	appendPlace: function(place){
		var self = this;

		if (!place) return;

		if ($.isArray(place)){
			_.each(place, self.appendPlace, self);
		}else{
			self.places.add(place);
		}
	},

	/**
	 * Добавляет категорию поиска
	 */
	appendCategory: function(category){
		var self = this;

		self.searchCategories.appendNew(category);
	},

	/**
	 * Добавляет регион города в список фильтров
	 */
	appendRegion: function(region){
		var self = this;

		if (!region) return;

		if ($.isArray(region)){
			_.each(region, self.appendRegion, self);
		}else{
			self.searchMap.appendRegion(region);
			self.$selectRegion.append('<option value="'+region.id+'">'+region.title+'</option>');
		}
	},

	/**
	 * Производит поиск простым перебором коллекции
	 */
	search: function(){
		var self = this;

		if (!self.searchEnabled){
			return false;
		}

		var selectedCategories = self.searchParams.get('categories');
		var selectedSubCategories = self.searchParams.get('subcategories');
		var selectedRegions = self.searchParams.get('regions');
		var selectedText = $.trim(self.searchParams.get('text').toLowerCase());

		// Перебираем все учреждения
		self.places.each(function(place){
			// Сравнение категории
			var categoriesActivity = true;
			if (selectedCategories.length > 0){
				categoriesActivity = false;
				var placeCategories = place.get('categories');

				for(var i = 0; i < placeCategories.length; i++){
					if ($.inArray(placeCategories[i], selectedCategories) != -1){
						categoriesActivity = true;
						break;
					}
				}
			}

			// Сравнение подкатегории
			var subcatActivity = true;
			if (selectedSubCategories.length > 0){
				subcatActivity = false;

				var placeSubCategories = place.get('subcategories');

				for(var i = 0; i < placeSubCategories.length; i++){
					if ($.inArray(placeSubCategories[i], selectedSubCategories) != -1){
						subcatActivity = true;
						break;
					}
				}
			}

			// Сравнение местоположения
			var regionActivity = true;
			if (selectedRegions.length > 0){
				regionActivity = false;

				if ($.inArray(place.get('region'), selectedRegions) != -1){
					regionActivity = true;
				}
			}

			// Поиск по текстовому запросу
			var textActivity = true;
			var searchIn = ['title', 'address', 'phone', 'content'];
			if (selectedText != ''){
				textActivity = false;

				_.each(searchIn, function(parameter){
					if (place.get(parameter).toLowerCase().indexOf(selectedText) != -1){
						textActivity = true;
						return false;
					}
				});
			}

			place.set({
				yState: (categoriesActivity && subcatActivity && regionActivity && textActivity) ? 'active' : 'inactive'
			});
		});

		self.searchMap.trigger('highlight');
	}
});

/**
 * Голосовалка
 */
var starsView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.id = self.$el.data('id');
		self.rate = self.$el.data('rate');

		self.appendStars();

		if (self.$el.hasClass('disabled')){
			self.disable();
		}

		self.$stars.hover(function(){
			if (!self.disabled){
				$(this).prevAll().andSelf().addClass('hovered').removeClass('nothovered');
				$(this).nextAll().removeClass('hovered').addClass('nothovered');
			}
		});

	},

	/**
	 * Отключает возможность голосования
	 */
	disable: function(){
		var self = this;

		self.disabled = true;
		self.$stars.removeClass('hovered nothovered').removeAttr('title');
	},

	/**
	 * Устаналивает рейтинг
	 */
	setRate: function(rate){
		var self = this;

		self.rate = rate;
		self.$stars.addClass('inactive');
		self.$stars.slice(0, self.rate).removeClass('inactive');
	},

	/**
	 * Добавляет 10 звезд
	 */
	appendStars: function(){
		var self = this;

		var $star = $('<span class="star inactive"></span>');

		for(var i = 0; i < 10; i++){
			var $s = $star.clone();

			if (!self.disabled){
				$s.attr('title', 'Оценка: '+(i+1));

				(function(i){
					$s.on('click', function(e){
						e.preventDefault();

						$.post('/opendata/list/?action=vote', {id:self.id, rate:i+1}, function(r){
							if (r.success){
								self.disable();
								self.setRate(r.rate);
							}else{
								alert('Произошла ошибка');
							}
						});
					});
				}(i))

			}

			self.$el.append($s);
		}

		self.$stars = self.$('.star');

		self.setRate(self.rate);
	}
});

$(function() {
    $('body').on('click', '.error-report', function(e) {
        e.preventDefault();

        var $overlay = $('<div class="export-overlay"></div>').appendTo('body');
        var $window = $('#error-report-window').appendTo('body');

        $window.find('input:hidden[name="page"]').val(window.location.href);
        $window.find('a.page-address').html(window.location.href);
        $window.find('.row').show();
        $window.find('.success').remove();

        $overlay.css({opacity:0, display:'block'}).animate({opacity:0.5});
        $window.css({opacity:1}).fadeIn(200);

        var __close = function() {
            $overlay.fadeOut(200);
            $window.fadeOut(200, function() {
                $overlay.remove();
            });

            return false;
        };

        $overlay.on('click', __close);
        $window.find('.close').on('click', __close);
    });

    var submitFrom = function(){
        var $window = $('#error-report-window');

        var data = {
            name: $window.find('input[name="name"]').val(),
            page: window.location.href,
            type: $window.find('select[name="type"]').val(),
            description: $window.find('textarea[name="description"]').val()
        }

        var error = false;

        if (data.type == '') {
            $window.find('select[name="type"]').closest('.row').addClass('error');
            error = true;
        } else {
            $window.find('select[name="type"]').closest('.row').removeClass('error');
        }

        if (data.description == '') {
            $window.find('textarea[name="description"]').closest('.row').addClass('error');
            error = true;
        } else {
            $window.find('textarea[name="description"]').closest('.row').removeClass('error');
        }

        if (!error) {
            $.post('/opendata/list/?errorreport=1', data, function(r) {
                if (r.success) {
                    $window.find('.row').slideUp(200);
                    setTimeout(function(){
                        $window.find('.w').append('<div class="success">Спасибо, Ваше сообщение отправлено!</div>');
                    }, 200);
                } else {
                    alert('Произошла ошибка, попробуйте еще раз');
                }
            });
        }
    }

    $('#error-report-window').find('input').on('keyup', function(e) {
        if (e.keyCode == 13) {
            submitFrom();
        }
    });

    $('#error-report-window').find('button').on('click', function() {
        submitFrom();
    });
});

$(document).ready(function(){
	// Инициализация приложения поиска
	window.socialMapSearch = new SMSearchApp();

	// Добавляем категории поиска
	try{
		window.socialMapSearch.appendCategory(__categoriesList);
	}catch(e){};

	// Добавляем регионы поиска
	try{
		window.socialMapSearch.appendRegion(__regionsList);
	}catch(e){};

	// Добавление учреждений
	window.socialMapSearch.on('initialized', function(){
		try{
			var __loading = setInterval(function() {
                if (__placesList.length == 0) {
                    clearInterval(__loading);
                    window.socialMapSearch.placesLoaded = true;

                    var $details = $('.sm-place-details[data-id]');

                    if ($details.length > 0) {
                        window.socialMapSearch.onePlace($details.data('id'));
                    }

                    Backbone.history.start();

                    window.socialMapSearch.trigger('full-init');
                } else {
                    var places = [];
                    for(var i = 0; i < 500; i++) {
                        if (__placesList.length > 0) {
                            places.push(__placesList.pop());
                        }
                    }

                    window.socialMapSearch.appendPlace(places);
                }
            }, 1);
		}catch(e){};
	});

	// Запуск галереи
	var $gallery = $('.sm-detail-gallery');
	if ($gallery.length > 0){
		fototape($gallery);
	}

	// Голосовалка
	var $rating = $('.sm-place-details .place-rating');
	if ($rating.length > 0){
		new starsView({el:$rating});
	}
});

/**
 * Парсинг координат района города
 */
function __parseYP(str){
	var matches = str.match(/GeoPoint\(([0-9\.]+), ([0-9\.]+)\)/g);

	var arr = [];

	_.each(matches, function(item){
		var sm = item.match(/([0-9\.]+), ([0-9\.]+)/g);

		sm = sm[0].split(',');
		var tmp = sm[0];
		sm[0] = sm[1];
		sm[1] = tmp;

		sm[0] = $.trim(sm[0]);
		sm[1] = $.trim(sm[1]);

		arr.push(sm);
	});


	_.each(arr, function(v, k){
		arr[k] = v[0]+'|'+v[1];
	});


	return arr.join('+');
}

$(function() {
    $('body').on('click', 'a[href^="#!/export-"]', function(e) {
        e.preventDefault();

        if ($(this).hasClass('disabled')) {
            return;
        }

        var category = $(this).attr('href').split('-')[1];

        var $this = $(this).addClass('disabled');

        $.get('/opendata/', {exportwindow:category}, function(data) {
            $this.removeClass('disabled');

            var $overlay = $('<div class="export-overlay"></div>').appendTo('body');
            var $window = $('#export-window-tpl').tmpl(data).appendTo('body');

            $overlay.css({opacity:0, display:'block'}).animate({opacity:0.5});
            $window.css({opacity:1}).fadeIn(200);

            var __close = function() {
                $overlay.fadeOut(200);
                $window.fadeOut(200, function() {
                    $window.remove();
                    $overlay.remove();
                });

                return false;
            };

            setTimeout(function() {
                $overlay.on('click', __close);
                $window.find('.close').on('click', __close);
            }, 400);
        });
    });
});
