<?php

namespace Krd\ConferenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\FilesBundle\Entity\Image;


/**
 * Партнер - баннер
 *
 * @ORM\Entity
 * @ORM\Table(name="conference_partner",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="blank", columns={"blank"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Заголовок"}},
 *     url={"text", {"label"="URL"}},
 *     blank={"checkbox", {
 *         "label"="В новом окне",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображение",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Partner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * URL
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $url = '#';


    /**
     * Открыть в новом окне
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $blank = false;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="conference_partner_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="cp_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getMainImage")
     */
    private $mainImage;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getBlank()
    {
        return $this->blank;
    }

    public function setBlank($blank)
    {
        $this->blank = (boolean)$blank;
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    /**
     * Первое изображение
     */
    public function getImage()
    {
        foreach($this->getImages() as $image) {
            return $image;
        }
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}


