/**
 * @author User
 */
var videoCache = {};
angular.module('glava.video', [])
	.directive('videoPreview', function($timeout, $http, $compile) {
		var tpl = '<div class="preview-container" style="display:none"> \
		<div class="format-switcher" ng-show="enableSD || enableHD"><div><a href="#" ng-click="switchSD($event)" ng-show="enableSD">SD</a><a href="#" ng-click="switchHD($event)" ng-show="enableHD">HD</a></div></div> \
		<div class="pos-right" style="position:relative;margin-top: 20px;margin-right: 20px;z-index: 1000;"> \
			<div class="item dd-list"> \
	            <i class="icon-share-panel-share" title="Рассказать друзьям"></i> \
	            <div class="dd-wrap"> \
	                <i class="icon-share-dd-share"></i> \
					<div class="yashare-auto-init" id="ya_share{{ $id }}" data-yasharel10n="ru" data-yasharetype="none" data-yasharequickservices="twitter,facebook,vkontakte"> \
					</div> \
	            </div> \
	        </div> \
		</div> \
	<video width="960" height="540" poster="{{ poster.0.size.glava960x540 }}" controls="controls" preload="none" id="glava_video_el{{ $id }}"> \
		<source type="video/mp4" src="" /> \
		<source type="video/webm" src="" /> \
	    <object width="960" height="540" type="application/x-shockwave-flash" data="/bundles/krdsite/vendor/mediaelement/flashmediaelement.swf"> \
	        <param name="movie" value="/bundles/krdsite/vendor/mediaelement/flashmediaelement.swf" /> \
	        <param name="flashvars" value="controls=true&file={{ mp4 }}" /> \
	        <img src="{{ poster.0.size.glava960x540 }}" width="960" height="540" title="No video playback capabilities" /> \
	    </object> \
	</video> \
</div>';
		
		return {
			restrict : 'A',
			scope : true,
			template : tpl,
			
			link : function(scope, $element) {
				scope.row = $element.parent();
				scope.container = scope.row.parent();
				scope.sd = { mp4 : '', webm : ''};
				scope.hd = { mp4 : '', webm : ''};
				scope.player = null;
				scope.title = '';
				
				scope.enableSD = false;
				scope.enableHD = false;
				
				$timeout(function() {
					scope.player = new MediaElementPlayer('#glava_video_el' + scope.$id, {
	                    pluginPath: '/bundles/krdsite/vendor/mediaelement/',
	                    alwaysShowControls: true,
	                    iPadUseNativeControls: false,
	                    iPhoneUseNativeControls: false,
	                    AndroidUseNativeControls: false,
	                    pauseOtherPlayers: true,
	                    enableAutosize: false
	                });
				}, 10);
				
				function videoProcess() {
					if (scope.player && scope.video) {
						if (scope.video.format.length) {
							scope.player.setSrc([{
								src: scope.video.format[0].file.path,
								type: 'video/mp4'
							}, {
								src: scope.video.format[1].file.path,
								type: 'video/webm'
							}, {
								src: scope.video.file.path,
								type: 'video/flv'
							}]);
						}
						else {
							scope.player.setSrc([{
								src: scope.video.file.path,
								type: 'video/mp4'
							}, {
								src: scope.video.file.path,
								type: 'video/webm'
							}, {
								src: scope.video.file.path,
								type: 'video/flv'
							}]);
						}
						scope.player.load();
					}
				};
				
				scope.$watch('item', function(n, o) {
					if (scope.item) {
						
						scope.container.find('.item').removeClass('active');
						scope.item.addClass('active');

						scope.container.find('.list-row').filter(':not(#'+scope.row.attr('id')+')').each(function() {
							$(this).addClass('looser');
						 	$(this).find('.preview-container').slideUp(300);
						})
						
						$timeout(function() {
							scope.share = new Ya.share({
								element: 'ya_share' + scope.$id,
								elementStyle: {
									'type': 'none',
									'border': false,
									'quickServices': ['twitter', 'facebook', 'vkontakte']
								}
							});
							scope.share.updateShareLink(window.location.href+'detail-'+scope.id+'.html', scope.title);
						}, 20);
						
						$element.children('.preview-container').slideDown(200, function() {
							$('html,body').stop().animate({
								scrollTop : $element.offset().top
							}, 500);
						});
						
						videoProcess();
					}
				});
				
				scope.row.on('click', '.list-view .item', function(e) {
					e.preventDefault();
					
					var self = $(this);
					var id = self.attr('id');
					scope.id = id;
					
					scope.enableSD = false;
					scope.enableHD = false;
										
					if (videoCache[id]) {
						scope.$apply(function() {
							scope.item = self;
							scope.title = videoCache[id].title;
							scope.poster = videoCache[id].poster;
							if (videoCache[id].hasOwnProperty('video') && videoCache[id].video.length > 0) {
								scope.video = videoCache[id].video[0];
							} else {
								var data = videoCache[id];
								if( data.mp4sd.length > 0 ) {
									scope.sd.mp4 = data.mp4sd[0].file.path;
								}
								if( data.webmsd.length > 0 ) {
									scope.sd.webm = data.webmsd[0].file.path;
								}
								if( data.mp4hd.length > 0 ) {
									scope.hd.mp4 = data.mp4hd[0].file.path;
								}
								if( data.webmhd.length > 0 ) {
									scope.hd.webm = data.webmhd[0].file.path;
								}
								scope.enableSD = data.mp4sd.length || data.webmsd.length;
								scope.enableHD = data.mp4hd.length || data.webmhd.length;
							}
						});
					} else {
						self.addClass('clicked');
						$http({
							method: 'GET',
							url: scope.container.data('source') + '-' + id + '.json'
						}).success(function(data){
							self.removeClass('clicked');
							scope.item = self;
							scope.title = data.title;
							scope.poster = data.poster;
							if (data.hasOwnProperty('video') && data.video.length > 0) {
								scope.video = data.video[0];
							} else {
								if( data.mp4sd.length > 0 ) {
									scope.sd.mp4 = data.mp4sd[0].file.path;
								}
								if( data.webmsd.length > 0 ) {
									scope.sd.webm = data.webmsd[0].file.path;
								}
								if( data.mp4hd.length > 0 ) {
									scope.hd.mp4 = data.mp4hd[0].file.path;
								}
								if( data.webmhd.length > 0 ) {
									scope.hd.webm = data.webmhd[0].file.path;
								}
								scope.enableSD = data.mp4sd.length || data.webmsd.length;
								scope.enableHD = data.mp4hd.length || data.webmhd.length;
							}
							videoCache[id] = data;
						}).error(function(data){
						
						});
					}
				});
				
				$element.on('mouseenter mouseleave', '.item.dd-list', function(e) {
					if (e.type === 'mouseenter') {
						$(this).children('.dd-wrap').show();
					} else {
						$(this).children('.dd-wrap').hide();
					}
				});
			},
			
			controller : function($scope) {
				
				$scope.switchSD = function($event) {
					$event.preventDefault();
					$scope.enableSD = false;
					$scope.enableHD = false;
					
					$scope.player.setSrc([
					   { src:$scope.sd.mp4, type:'video/mp4' },
					   { src:$scope.sd.webm, type:'video/webm' }
					]);
					$scope.player.load();
				}
				
				$scope.switchHD = function($event) {
					$event.preventDefault();
					$scope.enableSD = false;
					$scope.enableHD = false;
					$scope.player.setSrc([
					   { src:$scope.hd.mp4, type:'video/mp4' },
					   { src:$scope.hd.webm, type:'video/webm' }
					]);
					$scope.player.load();
				}
			}
		}
	}).directive('detailVideo', function($timeout) {
		return {
			restrict : "C",
			link : function($scope, $element, attrs) {
				$scope.formats = formats;
				
				$scope.enableSD = $scope.formats.hasOwnProperty('sd');
				$scope.enableHD = $scope.formats.hasOwnProperty('hd');
				
				$scope.player = new MediaElementPlayer('#glava_video_detail', {
                    pluginPath: '/bundles/krdsite/vendor/mediaelement/',
                    alwaysShowControls: true,
                    iPadUseNativeControls: false,
                    iPhoneUseNativeControls: false,
                    AndroidUseNativeControls: false,
                    pauseOtherPlayers: true,
                    enableAutosize: false
                });
			},
			
			controller : function($scope, $element) {
				$scope.sd = function($event) {
					$event.preventDefault();
					$($event.target).parents('.format-switcher').remove();
					
					$scope.player.setSrc([
					   { src:$scope.formats.sd.mp4, type:'video/mp4' },
					   { src:$scope.formats.sd.webm, type:'video/webm' }
					]);
					$scope.player.load();
				}
				
				$scope.hd = function($event) {
					$event.preventDefault();
					$($event.target).parents('.format-switcher').remove();
					
					$scope.player.setSrc([
					   { src:$scope.formats.hd.mp4, type:'video/mp4' },
					   { src:$scope.formats.hd.webm, type:'video/webm' }
					]);
					$scope.player.load();
				}
			}
		};
	});
