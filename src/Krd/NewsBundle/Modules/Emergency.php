<?php

namespace Krd\NewsBundle\Modules;

use DateTime;
use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль срочных сообщений
 */
class Emergency extends AbstractModule
{
    private $inactiveChecked = false;

    public function renderAdminContent()
    {
        return $this->twig->render('KrdNewsBundle:Admin:Module/emergency.html.twig');
    }

    /**
     * Проверка на старые сообщения
     */
    protected function checkInactive()
    {
        if ($this->inactiveChecked) {
            return;
        }

        $this->inactiveChecked = true;

        $date = new DateTime();
        $date->modify('-1 days');

        $this->em->createQuery('UPDATE KrdNewsBundle:Emergency e SET e.active = 0 WHERE e.date < :date')
            ->setParameter('date', $date)
            ->getResult();
    }

    /**
     * Список для текущей страницы
     * @return array
     */
    public function getItemsList($count = 10)
    {
        $this->checkInactive();

        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC')
            ->setParameter('parent', $this->node->getNode()->getId());

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }

    /**
     * Список новостей
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdNewsBundle:Module:emergency/list.html.twig', array(
            'list' => $result,
        ));
    }

    /**
     * Виджет
     */
    public function renderWidget()
    {
        $this->checkInactive();

        $list = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->orderBy('e.date', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->useResultCache(true, 15)
            ->getResult();

        if (!$list) {
            return '';
        }

        return $this->twig->render('KrdNewsBundle:Module:emergency/widget.html.twig', array('items' => $list));
    }
}
