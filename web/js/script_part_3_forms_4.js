/**
 * Модуль работы с формами
 */
angular.module('forms', [])
    .run([function() {
        // расширение jQuery
        $.fn.serializeObject = function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });

            return o;
        };
    }])

    /**
     * Всплывающие сообщения
     */
    .service('$bubble', ['$parse', function($parse) {
        if (!$.fn.qtip) {
            console.error('No bubble popup plugin installed!');
            return {};
        }

        var defaultOptions = {
            overwrite: true,
            suppress: false
        };

        var bubble = {
            /**
             * Создание всплывающей подсказки у элемента
             */
            show: function($element, text, options) {
                options = angular.extend(defaultOptions, options || {});
                options.content = angular.extend({text:text}, options.content || {});
                // options.id = ('qtip-' + Math.random() + Math.random() + Math.random()).replace(/[^a-z0-9]/g, '');

                $element.qtip(options);
                $element.qtip('api').set(options);
                $element.qtip('show');
            },

            /**
             * Удаление подсказки на элементе
             */
            remove: function($element) {
                $element.qtip('destroy', true);
            }
        };

        return bubble;
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Аяксовая форма
     * Можно добавить свой контроллер с методами onSuccess, onError, onFatalError
     */
    .directive('ajaxForm', ['$bubble', '$http', function($bubble, $http) {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.find('input, textarea').each(function() {
                    $(this).attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.action;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input, textarea').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });

                    $element.on('submit', function(e) {
                        e.preventDefault();

                        return false;
                    });

                    $scope.$watch('errors', $scope.updateBubbles);
                }
            },

            controller: function($scope, $element) {
                $scope.submit = function() {
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                if (!!$scope.onSuccess) {
                                    $scope.onSuccess();
                                }
                            } else {
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }

                                if (!!$scope.onError) {
                                    $scope.onError();
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            if (!!$scope.onFatalError) {
                                $scope.onFatalError();
                            }

                            console.error(response);
                        });
                };

                /**
                 * Обновление всплывающих ошибок
                 */
                $scope.updateBubbles = function() {
                    $bubble.remove($element.find('input, select, textarea'));

                    for(var name in $scope.errors) {
                        if (!$scope.errors.hasOwnProperty(name)) {
                            continue;
                        }

                        var errors = $scope.errors[name];
                        var $field = $element.find('*[name="'+name+'"]');

                        // $bubble.show($field, errors.join(', '), {style:{classes:'qtip-red'}});
                    }
                };

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                };
            }
        }
    }])
;
