<?php

namespace Krd\SubscribeBundle\Twig;

use phpQuery;

use Krd\SubscribeBundle\Entity\Subscriber;


/**
 * Расширение Twig для модификации текста расслки
 */
class LetterExtension extends \Twig_Extension
{
    protected $url = '';

    public function setBaseUrl($url)
    {
        $this->url = $url;
    }

    public function getFilters()
    {
        return array(
            'letter_content' => new \Twig_Filter_Function(array($this, 'letter_content')),
        );
    }

    /**
     * Модификация контента перед отправкой рассылки
     * @param  string $content
     * @return string
     */
    public function letter_content($content)
    {
        $content = phpQuery::newDocument($content);

        foreach ($content->find('a') as $link) {
            pq($link)->attr('target', '_blank');
            pq($link)->attr('style', pq($link)->attr('style').';color:#09689f;');

            $url = pq($link)->attr('href');

            if (mb_strpos($url, '//') !== 0 && mb_strpos($url, 'http') !== 0) {
                pq($link)->attr('href', $this->url.'/'.ltrim($url, '/ '));
            }
        }

        return $content->html();
    }

    public function getName()
    {
        return 'krd_subscribe_letter';
    }
}
