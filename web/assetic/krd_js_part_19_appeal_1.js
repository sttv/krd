/**
 * Интернет приемная
 */
angular.module('appeal', ['user.notifications'])
    /**
     * Контроллер отправки формы
     */
    .controller('AppealFormCtrl', ['$scope', '$element', 'notifications', '$http', function($scope, $element, notifications, $http) {
        $scope.onSuccess = function(response) {
            $('#appeal-submit-form-success').appendTo($element.parent()).slideUp(0).slideDown();
            $element.add($element.prev('.text-block')).slideUp(250);

            setTimeout(function() {
                if (!!response.successText) {
                    $('#appeal-submit-form-success').html(response.successText);

                    if (!!$.scrollTo) {
                        $.scrollTo($('#appeal-submit-form-success'), 200, {offset:{top:-150}});
                    }
                }
            }, 300);
        };

        /**
         * Сохранение черновика
         */
        $scope.saveDraft = function() {
            $http.post($element.attr('action'), $element.serialize()+'&draft=1')
                .success(function(response) {
                    if (response.success) {
                        notifications.user.info(response.successText || 'Успешно');
                    } else {
                        notifications.user.error(response.error || 'Произошла ошибка');
                    }
                })
                .error(function(response) {
                    notifications.user.error(response.error || 'Произошла ошибка');
                });
        };
    }])

    /**
     * Поле выбора адресата обращения
     */
    .directive('appealDestinationField', ['$timeout', function($timeout) {
        return {
            restrict: 'C',
            scope: true,
            link: function($scope, $element, attrs) {
                var $item = $element.children('.item');
                var $handler = $item.children('.wrap-icon');
                var $title = $item.children('.wrap-icon').children('.title');

                $handler.on('click', function(e) {
                    e.preventDefault();
                    $item.toggleClass('close open');
                });

                $scope.original = $title.html();
                $scope.value = '';

                var updateBySelected = function() {
                    var $selected = $element.find('input:radio:checked');
                    if ($selected.length > 0) {
                        $scope.value = $selected.parent().find('.name').text();
                    } else {
                        $scope.value = '';
                    }
                };

                setTimeout(updateBySelected);

                $element.find('input:radio').on('change', function() {
                    var $this = $(this);

                    if ($this.is(':checked')) {
                        $scope.value = $this.parent().find('.name').text();
                        $scope.$apply();
                    } else {
                        $scope.$apply(updateBySelected);
                    }
                });

                $scope.$watch('value', function(value) {
                    if (value == '') {
                        $title.html($scope.original);
                    } else {
                        $title.html(attrs.selectedTitle.replace('%value%', '<b>' + value + '</b>'));
                    }
                });

                $timeout(function() {
                    if ($element.find('input:radio').length == 1) {
                        $element.find('input:radio').first().click();
                        $element.hide();
                    }
                });
            }
        }
    }])
;
