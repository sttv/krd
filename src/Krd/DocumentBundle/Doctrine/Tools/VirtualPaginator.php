<?php

namespace Krd\DocumentBundle\Doctrine\Tools;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Наебываение пагинации в документах для поиска по old.krd.ru
 */
class VirtualPaginator extends Paginator
{
    protected $virtualItems = array();

    /**
     * Создание пагинатора из запроса
     * @param  Request $request
     * @param  Query $query
     * @param  boolean[optional] $fetchJoinCollection
     * @param  integer[optional] $count
     * @param  string[optional] $urlTemplate
     * @return Paginator
     */
    public static function createFromRequest(Request $request, Query $query, $fetchJoinCollection = true, $count = null, $urlTemplate = null)
    {
        $page = $request->get('page', 1);

        if ($page < 1) {
            $page = 1;
        }

        if (is_null($count)) {
            $count = $request->get('count', 10);
        }

        if ($count > 100) {
            $count = 100;
        }

        if ($count < 0) {
            $count = 1;
        }

        $query->setFirstResult($page*$count - $count)
            ->setMaxResults($count)
            ->useResultCache(true, 15);

        $paginator = new self($query, $fetchJoinCollection);

        if (!is_null($urlTemplate)) {
            $paginator->setUrlTemplate($urlTemplate);
        }

        return $paginator;
    }

    /**
     * Добавление вирутального элемента в пагинатор
     */
    public function addVirtualItem($item)
    {
        $this->virtualItems[] = $item;
    }

    public function getItems()
    {
        return $this->virtualItems;
    }

    /**
     * Сортировка элементов в соответствии с набором хешей
     * @param  array $hashes
     */
    public function reorderVirtual($hashes)
    {
        foreach (parent::getItems() as $item) {
            $this->virtualItems[] = $item;
        }

        $items = array();

        while ($hash = array_shift($hashes)) {
            foreach ($this->virtualItems as $i => $item) {
                if ($item->getHash() == $hash) {
                    $items[] = $item;
                    unset($this->virtualItems[$i]);
                }
            }
        }

        foreach ($this->virtualItems as $item) {
            $items[] = $item;
        }

        $this->virtualItems = $items;
    }
}
