(function () {
    angular.extend(angular, {
        toParam: toParam
    });

    /**
     * Преобразует объект, массив или массив объектов в строку,
     * которая соответствует формату передачи данных через url
     * Почти эквивалент [url]http://api.jquery.com/jQuery.param/[/url]
     * Источник [url]http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object/1714899#1714899[/url]
     *
     * @param object
     * @param [prefix]
     * @returns {string}
     */
    function toParam(object, prefix) {
        var stack = [];
        var value;
        var key;

        for (key in object) {
            value = object[key];
            key = prefix ? prefix + '[' + key + ']' : key;

            if (value === null) {
                value = encodeURIComponent(key) + '=';
            } else if (typeof( value ) !== 'object') {
                value = encodeURIComponent(key) + '=' + encodeURIComponent(value);
            } else {
                value = toParam(value, key);
            }

            stack.push(value);
        }

        return stack.join('&');
    }
}());


/** Russian datepicker **/
$.datepicker.regional['ru'] = {
    closeText: 'Закрыть',
    prevText: '&#x3c;Пред',
    nextText: 'След&#x3e;',
    currentText: 'Сегодня',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
        'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
    dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    weekHeader: 'Не',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['ru']);

var modulesAll = ['ui.date', 'dateRangePicker', 'interactive', 'header', 'widget.reporter', 'horizontal.scroll', 'gallery.slider', 'icons.slider', 'news.slider', 'calendar', 'content', 'administration', 'dialog', 'forms', 'mayor', 'video', 'city-object', 'documents', 'search', 'evacuation', 'appeal', 'citynum', 'dictionary', 'polls', 'subscribe', 'socialgroups', 'indexmap', 'lazy', 'banners', 'filters', 'user', 'router', 'comments', 'gorod-update', 'support', 'news.themes', 'glava.common', 'glava.faq', 'glava.gallery', 'glava.lightbox', 'glava.video'];
var modules = [];

for (var i = 0; i < modulesAll.length; i++) {
    try {
        angular.module(modulesAll[i]);
        modules.push(modulesAll[i]);
    } catch (e) {
        // Nothing
    }
}

angular.module('krd', modules)
    .config(['$httpProvider', '$anchorScrollProvider', function ($httpProvider, $anchorScrollProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        if (!$httpProvider.defaults.headers['delete']) {
            $httpProvider.defaults.headers['delete'] = {};
        }

        $httpProvider.defaults.headers['delete']['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.transformRequest = function (data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? angular.toParam(data) : data;
        };

        $anchorScrollProvider.disableAutoScrolling();
    }])

    .directive('ngBlur', function () {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                element.bind('blur', function () {
                    scope.$apply(attrs.ngBlur);
                });
            }
        };
    })

    .run([function () {
        if ($('#aside-menu').length > 0) {
            // Скрол бокового меню
            $("#aside-menu .w").mCustomScrollbar({
                theme: 'rounded',
                scrollButtons: {
                    enable: true
                }
            });

            // Сразу устанавливаем куку, чтоб меню по 100 раз не открывалось
            $.cookie('aside-menu-hint', '1', {path: '/', expires: 60 * 60 * 24 * 30});

            // Открываем меню
            var __asideMenuOpen = function () {
                $('#aside-menu').addClass('open');

                var spaceWidth = ($('#page').width() - $('.page-wrap').width()) / 2 - 10;

                if (spaceWidth < 310) {
                    $('body').css('overflow', 'hidden');

                    $('.header-panel, #page, .footer').css('left', 310 - spaceWidth);
                }
            };

            // Закрываем меню
            var __asideMenuClose = function () {
                $('#aside-menu').removeClass('open');
                $('.header-panel, #page, .footer').css('left', 0);

                setTimeout(function () {
                    if (!$('#aside-menu').hasClass('open')) {
                        $('body').css('overflow', 'auto');
                    }
                }, 330);
            };

            // Для уже открытого меню задаем параметны для боди
            if ($('#aside-menu').hasClass('open')) {
                __asideMenuOpen();
            }

            $('#aside-menu .control').on('click', function (e) {
                e.preventDefault();

                $('#aside-menu .control .hint').fadeOut(50, function () {
                    $('#aside-menu .control .hint').remove();
                });

                if ($('#aside-menu').hasClass('open')) {
                    __asideMenuClose();
                } else {
                    __asideMenuOpen();
                }
            }).on('mouseenter', function () {
                $('#aside-menu .control .hint').fadeOut(50, function () {
                    $('#aside-menu .control .hint').remove();
                });
            });

            $('body').on('click', function (e) {
                if ($('#aside-menu').hasClass('open')) {
                    if (!$(e.target).is('#aside-menu') && $(e.target).closest('#aside-menu').length == 0) {
                        e.preventDefault();

                        __asideMenuClose();
                    }
                }
            });
        }

        $.smartbanner({
            appendToSelector: '.header-panel',
            button: 'Установить',
            title: 'Краснодар',
            price: 'Бесплатно',
            inGooglePlay: 'в Google Play'
        });

        var $imgLinks = $('.text-block a[href$=".bmp"]')
                .add('.text-block a[href$=".gif"]')
                .add('.text-block a[href$=".jpg"]')
                .add('.text-block a[href$=".jpeg"]')
                .not('.disable-fancybox')
                .not('.fancybox')
                .not('.lightbox')
                .addClass('fancybox-auto-init')
            ;


        $('.fancybox-auto-init').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $('.fancybox, .lightbox').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            },
            live: true
        });

        if ($.browser.msie && $.browser.version <= 7) {
            return;
        }

        $(window).on('load', function () {
            if (window.MaSha !== undefined) {
                $('.text-block').each(function () {
                    var attr = $(this).attr('no-mashajs');
                    if ((typeof attr == 'undefined' || attr == false) && attr != '') {
                        var id = $(this).attr('id');

                        if (!id) {
                            id = 'mashajs-' + (Math.random() * 100500).toString().replace(/[^0-9]/g, '');
                            $(this).attr('id', id);
                        }

                        new MaSha({
                            selectable: id,
                            select_message: 'upmsg-selectable',
                            marker: 'txtselect_marker'
                        });
                    }
                });
            }

            if (window.location.hash) {
                setTimeout(function () {
                    var $scrollItem = $(window.location.hash.replace('#/', '#'));

                    if ($scrollItem.length > 0) {
                        $.scrollTo($scrollItem, 200, {offset: {top: -75}});
                    }
                }, 13);
            }
        });

        // Плавный якорный скролл
        $('a[href^="#"]:not(a[href="#"])').on('click', function (e) {
            e.preventDefault();

            var $link = $(this);
            var $scrollItem = $($link.attr('href'));

            if ($scrollItem.length == 0) {
                $scrollItem = $('*[name="' + $link.attr('href').toString().replace('#', '') + '"]');

                if ($scrollItem.length == 0) {
                    return;
                }
            }

            $.scrollTo($scrollItem, 200, {offset: {top: -75}});
            window.location.hash = $scrollItem.attr('id');
        });

        // обратный отсчет до 9 мая
        $('.flip-clock').FlipClock(Math.ceil(((new Date(2015, 4, 9)) - (new Date())) / 1000), {
            clockFace: 'DailyCounter',
            language: 'russian',
            countdown: true
        });


        // Таблица MSP
        (function() {
            var $mspTable = $('#msp-list-table');
            var $tableBody = $mspTable.find('tbody');
            var $nextButton = $mspTable.next('.next-page');
            var currentPage = 1;
            var requestStatus = $.Deferred();
            requestStatus.resolve();

            $nextButton.on('click', function (e) {
                e.preventDefault();

                if (requestStatus.state() == 'pending') {
                    return;
                }

                $nextButton.text('Подождите...');

                requestStatus = $.get($nextButton.attr('url'), {page: currentPage + 1})
                    .then(function (result) {
                        $nextButton.text('Показать еще');
                        currentPage++;

                        if (result.page_count == currentPage) {
                            $nextButton.remove();
                        }
                        
                        for(var i = 0; i < result.items.length; i++) {
                            var item = result.items[i];
                            var lastCategory = parseInt($nextButton.attr('last-category'), 10);

                            if (item.category != lastCategory) {
                                switch(item.category) {
                                    case 1:
                                        $tableBody.append('<tr class="category"><td colspan="9">I. Субъекты малого предпринимательства (за исключением микропредприятий)</td></tr>');
                                        break;

                                    case 2:
                                        $tableBody.append('<tr class="category"><td colspan="9">II. Субъект среднего предпринимательства</td></tr>');
                                        break;

                                    case 3:
                                        $tableBody.append('<tr class="category"><td colspan="9">III. Микропредприятие</td></tr>');
                                        break;
                                }
                            }

                            $nextButton.attr('last-category', item.category);

                            var $tr = $('<tr></tr>');
                            $tr.append($('<td></td>').html(item.rec_num + '<br/>' + item.date_inc));
                            $tr.append($('<td></td>').text(item.org_title + ' от ' + item.date_approve));
                            $tr.append($('<td></td>').text(item.llc_title));
                            $tr.append($('<td></td>').text(item.inn));
                            $tr.append($('<td style="word-break:break-all"></td>').text(item.form));
                            $tr.append($('<td style="word-break:break-all"></td>').text(item.view));
                            $tr.append($('<td></td>').text(item.amount));
                            $tr.append($('<td></td>').text(item.year_of));
                            $tr.append($('<td></td>').text(item.information));
                            $tableBody.append($tr);
                        }
                    })
                    .fail(function () {
                        $nextButton.text('Показать еще');
                        alert('Произошла ошибка, попробуйте позже');
                    });
            });
        })();
    }])

/**
 * Контроллер формы сообщения об ошибке
 */
    .controller('SenderrorFormCtrl', ['$scope', '$element', function ($scope, $element) {
        $element.find('#site_senderror_url').val(window.location.href);
        $element.find('#site_senderror_url').hide();
        $element.find('#site_senderror_url').after('<a href="#">' + window.location.href + '</a>');

        $scope.onSuccess = function (response) {
            $element.slideUp(function () {
                $element.html('<div class="text-block"><p><b>Спасибо, ваше сообщение успешно отправлено.</b></p></div>').slideDown();
            });
        };
    }])

/**
 * Опрос о ремонте улиц
 */
    .directive('pollRoad', [function () {
        return {
            restrict: 'A',
            scope: true,
            link: function ($scope, $element, $attrs) {
                var pollId = $attrs.pollRoad;

                if ($.cookie('poll-road-' + pollId) == 1) {
                    return;
                }

                var idList = [];
                $element.find('.list > .item[data-id]').each(function () {
                    idList.push($(this).data('id'));
                });

                $scope.voted = false;
                $scope.pollRoad = {};

                $scope.canVote = function () {
                    var noErrors = true;

                    for (var i = 0; i < idList.length; i++) {
                        noErrors = noErrors && !!$scope.pollRoad['id' + idList[i]];
                    }

                    return noErrors;
                };

                $scope.vote = function () {
                    if (!$scope.canVote() || $scope.voted) {
                        return;
                    }

                    $scope.voted = true;

                    $.post('/pollroad/submit', {poll: $scope.pollRoad});

                    $.cookie('poll-road-' + pollId, 1, {path: '/', expires: 60 * 60 * 24 * 30});

                    $element.find('input:radio:not(:checked)').attr('disabled', 'disabled');
                    $element.find('.submit-button').slideUp(function() {
                        $element.find('.submit-button').html('<div class="text-block"><p>Спасибо, ваш голос будет учтен.</p></div>');
                        $element.find('.submit-button').slideDown();
                    });
                };
            }
        }
    }])
;
