/**
 * Модуль загрузчика видео
 */
angular.module('cms.filesuploader.video', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="video-uploader abstract-uploader">'
            + '<div class="files-list clearfix">'
                + '<video-list-item file="file" ng-repeat="file in files"></video-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Видео не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить видео</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.video', template);
    }])

    /**
     * Элемент загрузчика видео
     */
    .directive('videoListItem', ['$dialog', '$http', function($dialog, $http) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="img-polaroid">'
                        + '<img ng-src="{{file.size.cms}}">'
                    + '</a>'
                    + '<input type="text" placeholder="Заголовок" ng-model="file.title">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="{{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i> Скачать</a>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()"><i class="icon-trash icon-white"></i> Удалить</div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление видео';
                    var msg = 'Вы уверены, что хотите удалить видео?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                }
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', function(title) {
                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {title:title}
                    });
                });
            }
        }
    }])
;
