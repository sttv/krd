<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

use Q\CoreBundle\Entity\Node;


/**
 * @ORM\Entity(repositoryClass="Gedmo\Loggable\Entity\Repository\LogEntryRepository")
 * @ORM\Table(
 *     name="ext_log_entries",
 *  indexes={
 *      @ORM\Index(name="log_class_lookup_idx", columns={"object_class"}),
 *      @ORM\Index(name="log_date_lookup_idx", columns={"logged_at"}),
 *      @ORM\Index(name="log_user_lookup_idx", columns={"username"}),
 *      @ORM\Index(name="log_version_lookup_idx", columns={"object_id", "object_class", "version"})
 *  }
 * )
 */
class ShowLogEntry extends \Gedmo\Loggable\Entity\MappedSuperclass\AbstractLogEntry
{
    /**
     * @JMS\Exclude
     */
    private $entity;

    /**
     * Читабельное имя сущности
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("object_class_name")
     * @JMS\Accessor(getter="getObjectClassName")
     */
    private $objectClassName = '';

    /**
     * Дата логирования с временем
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("logged_at_time")
     * @JMS\Accessor(getter="getloggedAtTime")
     */
    private $loggedAtTime = '';

    /**
     * Читабельное действие
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("action_txt")
     * @JMS\Accessor(getter="getActionTxt")
     */
    private $actionTxt = '';

    /**
     * Наименование объекта
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("entity_title")
     * @JMS\Accessor(getter="getEntityTitle")
     */
    private $entityTitle = '';

    /**
     * URL на редактирование объект
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("entity_url")
     * @JMS\Accessor(getter="getEntityUrl")
     */
    private $entityUrl = '';

    /**
     * URL на раздел объекта
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("node_url")
     * @JMS\Accessor(getter="getNodeUrl")
     */
    private $nodeUrl = '';

    /**
     * Имя родительского раздела объекта
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("node_title")
     * @JMS\Accessor(getter="getNodeTitle")
     */
    private $nodeTitle = '';

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    protected function getEntityParent()
    {
        $entity = $this->getEntity();

        if ($entity && !($entity instanceOf Node) && method_exists($entity, 'getParent') ) {
            $parent = $entity->getParent();

            if ($parent && !($parent instanceOf Node) && method_exists($parent, 'getParent')) {
                $parent = $parent->getParent();
            }

            if ($parent instanceOf Node) {
                return $parent;
            }
        }
    }

    public function getEntityTitle()
    {
        $entity = $this->getEntity();

        if ($entity && method_exists($entity, 'getTitle')) {
            $title = $entity->getTitle();

            if ($title && ($title != $this->getObjectClassName())) {
                return $this->getObjectClassName().': '.$title;
            } else {
                return $this->getObjectClassName();
            }
        }

        return $this->getObjectClassName();
    }

    public function getEntityUrl()
    {
        if ($this->action == 'remove') {
            return '';
        }

        $entity = $this->getEntity();
        $parentUrl = $this->getNodeUrl();

        if ($entity && $parentUrl) {
            $className = get_class($entity);

            switch($className) {
                case 'Krd\EvacuationBundle\Entity\Avto':
                    $className = 'evacuation_avto';
                    break;

                case 'Krd\EvacuationBundle\Entity\Mark':
                    $className = 'evacuation_mark';
                    break;

                case 'Krd\EvacuationBundle\Entity\Organization':
                    $className = 'evacuation_organization';
                    break;

                case 'Krd\EvacuationBundle\Entity\Parking':
                    $className = 'evacuation_parking';
                    break;

                case 'Krd\EvacuationBundle\Entity\Evacuator':
                    $className = 'evacuation_evacuator';
                    break;

                default:
                    $rc = new \ReflectionClass($className);
                    $className = $rc->getShortName();

                    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $className, $matches);
                    $ret = $matches[0];

                    foreach ($ret as &$match) {
                        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
                    }

                    $className = implode('_', $ret);
                    break;
            }

            return $parentUrl.'#!/'.$className.'/edit/'.$entity->getId();
        } elseif ($entity instanceOf Node) {
            return $entity->getUrl(false);
        }
    }

    public function getNodeTitle()
    {
        if ($parent = $this->getEntityParent()) {
            return $parent->getTitle();
        }
    }

    public function getNodeUrl()
    {
        if ($parent = $this->getEntityParent()) {
            return $parent->getUrl(false);
        }
    }

    public function getActionTxt()
    {
        switch($this->action) {
            case 'create':
                return 'Создание';

            case 'update':
                return 'Редактирование';

            case 'remove':
                return 'Удаление';

            default:
                return '['.$this->action.']';
        }
    }

    public function getloggedAtTime()
    {
        if ($this->loggedAt instanceof \DateTime) {
            $now = new \DateTime();
            if ($this->loggedAt->format('dmY') == $now->format('dmY')) {
                return $this->loggedAt->format('сегодня в H:i:s');
            } else {
                return $this->loggedAt->format('d/m/Y H:i:s');
            }
        } else {
            return '---';
        }
    }

    public function getObjectClassName()
    {
        $names = array(
            'Krd\CityBundle\Entity\CityObject' => 'Объект города',
            'Krd\SiteBundle\Entity\Poll' => 'Опрос',
            'Krd\SiteBundle\Entity\PollAnswer' => 'Ответ на опрос',
            'Krd\EvacuationBundle\Entity\Avto' => 'Эвакуированное авто',
            'Krd\EvacuationBundle\Entity\Mark' => 'Эвакуированное авто - марка',
            'Krd\EvacuationBundle\Entity\Organization' => 'Эвакуированное авто - организация',
            'Krd\EvacuationBundle\Entity\Parking' => 'Эвакуированное авто - парковка',
            'Krd\EvacuationBundle\Entity\Evacuator' => 'Эвакуатор',
            'Krd\AppealBundle\Entity\Appeal' => 'Интернет-применая - обращение',
            'Krd\AppealBundle\Entity\Destination' => 'Интернет-применая - получатель',
            'Krd\DocumentBundle\Entity\Document' => 'Документ',
            'Q\CoreBundle\Entity\Content' => 'Контент',
            'Q\CoreBundle\Entity\Node' => 'Раздел',
            'Q\UserBundle\Entity\User' => 'Пользователь',
            'Krd\NewsBundle\Entity\News' => 'Новости',
            'Krd\AdministrationBundle\Entity\Human' => 'Структура администрации - человек',
            'Krd\NewsBundle\Entity\NewsTheme' => 'Новости - тег',
            'Krd\AdministrationBundle\Entity\Mayor' => 'Почетный гражданин',
            'Krd\BannersBundle\Entity\Banner' => 'Баннер',
            'Krd\ConferenceBundle\Entity\Online' => 'Онлайн-конференция',
            'Krd\ConferenceBundle\Entity\Partner' => 'Онлайн-конференция - партнер',
            'Krd\ConferenceBundle\Entity\Question' => 'Онлайн-конференция - вопрос',
            'Krd\GalleryBundle\Entity\Gallery' => 'Галерея',
            'Krd\NewsBundle\Entity\Info' => 'Новая информация',
            'Krd\RssBundle\Entity\Feed' => 'RSS лента',
            'Krd\SiteBundle\Entity\DService' => 'Департамент - услуга',
            'Krd\SiteBundle\Entity\SubOrganization' => 'Департамент - подведомственная организация',
            'Krd\SiteBundle\Entity\Vacancy' => 'Департамент - вакансия',
            'Krd\SiteBundle\Entity\Faq' => 'FAQ',
            'Krd\SiteBundle\Entity\Link' => 'Ссылки в футере',
            'Krd\SubscribeBundle\Entity\Letter' => 'Рассылка',
            'Krd\SubscribeBundle\Entity\Subscriber' => 'Подписчик на рассылку',
            'Q\CoreBundle\Entity\Redirect' => 'Редирект',
        );

        if (isset($names[$this->objectClass])) {
            return '['.$this->objectId.'] '.$names[$this->objectClass];
        } else {
            return $this->objectClass;
        }
    }
}
