<?php

namespace Krd\EvacuationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\EvacuationBundle\Entity\Avto;


/**
 * Побырому написанный импорт для импорта эвакуированных авто
 */
class ImportEvacuatedAvtoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:evacuated:avto')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $oldEm = $this->getContainer()->get('doctrine.orm.oldsite_entity_manager');

        $stAvto = $oldEm->getConnection()->prepare("SELECT * FROM `krd__evacuators` ORDER BY `sort`");
        $stAvto->execute();

        $node = $em->find('QCoreBundle:Node', 45);

        while ($item = $stAvto->fetch()) {
            $avto = new Avto();
            $avto->setParent($node);

            $date = $item['date'].' '.$item['time'];
            $date = new \DateTime($date);

            $avto->setDate($date);

            $avto->setFromPlace($item['place_before']);

            $marks = $em->createQuery("SELECT m FROM KrdEvacuationBundle:Mark m WHERE LOWER(m.title) = :title")->setMaxResults(1)->setParameter('title', mb_strtolower($item['car']))->getResult();

            $avto->setMark($marks[0]);

            $avto->setNumber($item['car_number']);

            $org = $em->createQuery("SELECT m FROM KrdEvacuationBundle:Organization m WHERE LOWER(m.title) = :title")->setMaxResults(1)->setParameter('title', mb_strtolower($item['organization']))->getResult();

            $avto->setOrganization($org[0]);

            $park = $em->createQuery("SELECT m FROM KrdEvacuationBundle:Parking m WHERE LOWER(m.title) = :title")->setMaxResults(1)->setParameter('title', mb_strtolower($item['place_after']))->getResult();

            $avto->setParking($park[0]);

            $avto->setActive(true);
            $avto->setToIndex(true);

            $em->persist($avto);
        }

        $em->flush();
    }
}
