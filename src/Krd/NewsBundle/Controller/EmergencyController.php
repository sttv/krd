<?php

namespace Krd\NewsBundle\Controller;

use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Срочные сообщения
 */
class EmergencyController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template()
     */
    public function listAction()
    {
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/news/emergency/list.json", name="ajax_news_emergency_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        $queryBuilder = $this->get('krd.news.modules.emergency')->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->orderBy('n.date', 'DESC');

        if ($parent = $request->get('parent')) {
            $queryBuilder->andWhere('n.parent = :parent');
            $queryBuilder->setParameter('parent', $parent);
        }

        return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 10);
    }
}
