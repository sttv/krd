<?php

namespace Q\CoreBundle\Navigation;

/**
 * Пункт меню управления редиректами
 */
class Redirect extends Item
{
    public function getTitle()
    {
        return 'Редиректы';
    }

    public function getLink()
    {
        return $this->router->generate('cms_redirect_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS_REDIRECT');
    }
}
