<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\Node;
use Krd\CompetitionBundle\Entity\Competition;


/**
 * Импорт открытых конкурсов
 */
class ImportCompetitionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:competition')
            ->setDescription('Import competitions pages from old site krd.ru')
            ->addArgument('source', InputArgument::REQUIRED, 'Old site absolute url')
            ->addArgument('destination', InputArgument::REQUIRED, 'New site absolute url')
            ->setHelp(<<<EOT
                      The <info>krd:import:competition</info> imports open competitions.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->oldStructure = $this->getContainer()->get('krd_import.old.structure');
        $this->uploadDir = $this->getContainer()->getParameter('kernel.root_dir').'/../web/oldkrd';

        $sourceUrl = $input->getArgument('source');
        $destinationUrl = $input->getArgument('destination');

        $validator = Validation::createValidator();
        $assert = new Assert\Url(array('protocols' => array('http')));

        // Проверка валидности URL
        $violations = $validator->validateValue($sourceUrl, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Source url errors '.$violations);
        }

        $violations = $validator->validateValue($destinationUrl, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Destination url errors '.$violations);
        }

        $output->writeln('<info>------------------------------ Import start at '.date('H:i:s').' ------------------------------</info>');

        $output->writeln('Update mode always<info>ON</info>');

        $sourceUrl = rtrim($sourceUrl, '/ ').'/';
        $destinationUrl = rtrim($destinationUrl, '/ ').'/';

        $output->writeln('<comment>Importing</comment> ['.$sourceUrl.'] to ['.$destinationUrl.']...');

        $sourceUrl = parse_url($sourceUrl);
        $destinationUrl = parse_url($destinationUrl);

        if ($sourceUrl['path'] == '/') {
            $output->writeln('<error>Source url too short</error>');
            return;
        }

        if ($destinationUrl['path'] == '/') {
            $output->writeln('<error>Destination url too short</error>');
            return;
        }

        $destinationNode = $this->em->getRepository('QCoreBundle:Node')->getNodeByPath($destinationUrl['path']);

        if (!$destinationNode) {
            $output->writeln('<error>Destination url not found on this site</error>');
            return;
        }

        // Фиксим обращение к странице по алиасу
        if ($destinationNode->getUrlAlias() == $destinationUrl['path']) {
            $destinationUrl['path'] = $destinationNode->getUrl(false);
            $destinationUrlRes = $destinationUrl['scheme'].'://'.$destinationUrl['host'].$destinationUrl['path'];
        }

        $tmp = explode('/', trim($destinationUrl['path'], '/ '));
        $expectedLevel = count($tmp);

        if ($expectedLevel != $destinationNode->getLevel()) {
            $output->writeln('<error>Please create '.$destinationUrl['path'].'</error>');
            return;
        }

        $sourceNode = $this->oldStructure->getByPath($sourceUrl['path']);

        if (!$sourceNode) {
            $output->writeln('<error>Source url not found on krd.ru</error>');
            return;
        }

        $competitions = $this->oldStructure->getCompetitions($sourceNode['id']);
        $this->getContainer()->get('krd_import.importer.structure')->setOutput($output);

        foreach ($competitions as $item) {
            $output->write('[Competition <info>'.$item['id'].'</info>]');
            $competition = $this->em->getRepository('KrdCompetitionBundle:Competition')->findOneBy(array('parent' => $destinationNode->getId(), 'number' => $item['number']));

            if (!$competition) {
                $competition = new Competition();
            }

            $tmp = explode('/', rtrim($item['url'], ' /'));

            $newUrl = $destinationNode->getUrl(false).array_pop($tmp).'/';

            $competition->setTitle($item['title']);
            $competition->setUrl($newUrl);
            $competition->setDateStart($item['date_start']);
            $competition->setDateEnd($item['date_end']);
            $competition->setCustomer($item['customer']);
            $competition->setNumber((int)$item['number']);
            $competition->setActive(true);
            $competition->setParent($destinationNode);

            $this->em->persist($competition);
        }

        $this->em->flush();

        $output->writeln('<comment>Importing content pages...</comment>');

        foreach ($competitions as $item) {
            $newUrl = $destinationNode->getUrl(false).array_pop($tmp).'/';

            $this->getContainer()->get('krd_import.importer.structure')->import($item['url'], 'http://newsite.krd.ru'.$newUrl, false, true, false);
        }

        $output->writeln('<info>complete</info>');

        $output->writeln('<info>----------------------------- Import complete at '.date('H:i:s').' ----------------------------</info>');
    }
}
