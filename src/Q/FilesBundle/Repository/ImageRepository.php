<?php

namespace Q\FilesBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\ImageCache;


/**
 * Репозиторий изображений
 * Так же выступает в виде менеджера ресайза изображений
 */
class ImageRepository extends EntityRepository
{
    /**
     * Web корень приложения
     */
    protected $webRootDir;

    /**
     * Полный путь к папке загрузки изображений
     */
    protected $imagesUploadDir;

    /**
     * Путь к загрузчику от корня загрузчика
     * q_files.image_cache.upload_path
     */
    protected $cacheUploadPath;

    /**
     * Методы изменения размера изображений
     * q_files.image_cache.sizes
     */
    protected $cacheSizes;

    /**
     * Monolog
     */
    protected $logger;

    /**
     * @JMSDI\InjectParams({
     *     "container" = @JMSDI\Inject("service_container")
     * })
     */
    public function setParameters($container)
    {
        $this->imagesUploadDir = $container->getParameter('q_files.images.upload_dir');
        $this->cacheUploadPath = $container->getParameter('q_files.image_cache.upload_path');
        $this->webRootDir = $container->getParameter('kernel.root_dir').'/../web';
        $this->cacheSizes = $container->getParameter('q_files.image_cache.sizes');
        $this->logger = $container->get('logger');
    }

    /**
     * Создание кешированного изображения
     * не требует указания пути к файлу
     * @param  string $paramskey Имя способа ресайза
     * @param  integer $id        ID сущности изображения
     * @return ImageCache|null
     */
    public function createImageCacheEx($paramskey, $id)
    {
        $image = $this->find($id);

        if (!$image) {
            $this->logger->crit('Попытка createImageCacheEx из несуществующей сущности', array($id));
            return;
        }

        $requestPath = $this->getImageCacheUrl($image, $paramskey);

        return $this->createImageCache($requestPath, $paramskey, $id);
    }

    /**
     * Создание кешированного изображения с предварительной обработкой
     * @param  string $requestPath
     * @param  string $paramskey Имя способа ресайза
     * @param  integer $id        ID сущности изображения
     * @return ImageCache|null
     */
    public function createImageCache($requestPath, $paramskey, $id)
    {
        if (!isset($this->cacheSizes[$paramskey])) {
            throw new \Exception('Resize mode "'.$paramskey.'" not found');
            return;
        }

        $image = $this->find($id);

        if (!$image) {
            throw new \Exception('Image #'.$id.' not found');
            return;
        }


        $savePath = $this->webRootDir.$requestPath;

        $fs = new Filesystem();
        if (!$fs->exists(dirname($savePath))) {
            $this->logger->debug('Папка '.dirname($savePath).' не существует, пробуем создать.');
            try {
                $fs->mkdir(dirname($savePath), 0777);
            } catch (IOException $e) {
                sleep(1);

                if (!$fs->exists(dirname($savePath))) {
                    $fs->mkdir(dirname($savePath), 0777);
                }
            }

            $this->logger->debug('Папка '.dirname($savePath).' успешно создана');
        }

        $imagine = new Imagine();
        $imageFileInfo = $image->getFile();
        $imagineImage = $imagine->open($this->webRootDir.$imageFileInfo['path']);

        foreach($this->cacheSizes[$paramskey] as $params) {
            $methodName = 'method_'.$params[0];
            $params[0] = $imagineImage;
            $imagineImage = call_user_func_array(array($this, $methodName), $params);
        }

        $imagineImage->save($savePath);

        $imageCacheFile = $image->getFile();
        $imageCacheFile['path'] = $requestPath;
        $imageCacheFile['fileName'] = '/'.$fs->makePathRelative($imageCacheFile['path'], $this->cacheUploadPath);
        $imageCacheFile['mimeType'] = $this->getMimeType($savePath);
        $imageCacheFile['size'] = filesize($savePath);
        $imageCacheFile['width'] = $imagineImage->getSize()->getWidth();
        $imageCacheFile['height'] = $imagineImage->getSize()->getHeight();

        $imageCache = new ImageCache();
        $imageCache->setParent($image);
        $imageCache->setName($paramskey);
        $imageCache->setFile($imageCacheFile);

        $image->addCached($imageCache);

        $this->getEntityManager()->persist($imageCache);
        $this->getEntityManager()->flush();

        return $imageCache;
    }

    /**
     * Создание Image из файла
     * При этом указанный файл удалится
     * @param  string $filePath
     * @return Image
     */
    public function createImageFromFile($filePath)
    {
        $fs = new Filesystem();
        if (!$fs->exists($filePath)) {
            throw new \Exception('File does not exists');
        }

        // Кинет исключение если это не картинка
        $imagine = new Imagine();
        $imagineImage = $imagine->open($filePath);

        $image = new Image();
        $image->setTitle('');
        $image->setMain(false);
        $image->setFile(new UploadedFile($filePath, basename($filePath), null, null, null, true));

        return $image;
    }

    /**
     * Определение Mime-типа
     * @param  string $filePath
     * @return string
     */
    protected function getMimeType($filePath)
    {
        $file = new \Symfony\Component\HttpFoundation\File\File($filePath);
        return $file->getMimeType();
    }

    /**
     * Кроп картинки с сохранением пропорций
     * @param  \Imagine\Image\ImageInterface $image
     * @param  integer                       $width
     * @param  integer                       $height
     * @return \Imagine\Image\ImageInterface
     */
    protected function method_crop(\Imagine\Image\ImageInterface $image, $width, $height)
    {
        return $image->thumbnail(new Box($width, $height), $image::THUMBNAIL_OUTBOUND);
    }

    /**
     * Ресайз изображения. Только уменьшение
     * @param  \Imagine\Image\ImageInterface $image
     * @param  integer                       $width
     * @param  integer                       $height
     * @return \Imagine\Image\ImageInterface
     */
    protected function method_cropInset(\Imagine\Image\ImageInterface $image, $width, $height)
    {
        return $image->thumbnail(new Box($width, $height), $image::THUMBNAIL_INSET);
    }
}
