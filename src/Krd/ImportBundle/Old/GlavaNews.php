<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;
use Krd\NewsBundle\Entity\News;
use Krd\NewsBundle\Entity\NewsTheme;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Krd\CommentsBundle\Entity\Comment;

/**
 * Импорт раздела новости с сайта glava.krd.ru на страницу главы на сайт krd.ru
 */
class GlavaNews
{
    protected $em;
	protected $gem;
	
	protected $uploadDir;

    public function __construct(EntityManager $em, EntityManager $gem)
    {
        $this->em = $em;
		$this->gem = $gem;
    }
	
	public function setUploadDir($dir)
	{
		$this->uploadDir = $dir;
	}
	
	public function getUploadDir()
	{
		return $this->uploadDir;
	}
	
	public function import($output)
	{
		$newsList = $this->loadNews();
		$glavaGoroda = $this->em->find('QCoreBundle:Node', 15188);
		
		if( $newsList == 0 ) {
			return;
		}
		
		$newsProcessed = 0;
		$this->em->getConnection()->beginTransaction();
		foreach( $newsList as $oldNews ) {
			$name = 'news_'.date('dmY', strtotime($oldNews['date'])).'_'.$oldNews['id'];
			$exists = false;
			$news = $this->em->getRepository('KrdNewsBundle:News')->findBy(array('name' => $name));
			
			if( !empty($news) ) {
				$news = array_pop($news);
				//$output->writeln("Exists {$news->getId()}:{$name}");
				$news->setRefId((int)$oldNews['id']);
				
				foreach($news->getImages() as $images) {
					foreach( $images->getCached() as $cached ) {
						$images->removeCached( $cached );
						$this->em->remove($cached);
					}
	                $news->removeImages($images);
	                $this->em->remove($images);
	            }
				
				$images = $this->getPhotos($oldNews['icon']);
				
				if (!empty($images)) {
	                foreach($images as $i => $imageArr) {
	                	if( !empty($imageArr['sourceFile']) && is_file($imageArr['sourceFile']) ) {
		                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
		                    $filesystem = new Filesystem();
		                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
		
		                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
		                    $image->setTitle('');
		                    $image->setSort($i);
		                    $news->addImages($image);
						}
	                }
	            }
				
				$exists = true;
			} else {
				$news = new News();
				$news->setTitle(htmlspecialchars_decode( $oldNews['name']) );
				
				// /administratsiya/struktura-administratsii/glava_goroda/ вот этот раздел у новостей родитель
				$news->setParent($glavaGoroda);
				$news->setName($name);
				$news->setDate(new \DateTime($oldNews['date']));
				$news->setAnnounce($oldNews['anons']);
				$news->setContent($oldNews['desc']);
				$news->setActive(true);
				$news->setRefId((int)$oldNews['id']);
				
				$this->em->persist($news);
			}
			
			 if (!empty($oldNews['tags']) && !$exists) {
			 	$tags = $this->getTags($oldNews['id']);
                foreach($tags as $tag) {
                    $theme = $this->em->getRepository('KrdNewsBundle:NewsTheme')->findByTitle($tag);
                    if ($theme) {
                        $theme = array_pop($theme);
                    } else {
                        $theme = new NewsTheme();
                        $theme->setTitle($tag);
                        $this->em->persist($theme);
						$this->em->flush($theme);
                    }
                    $news->addThemes($theme);
                }
            }
			
				
			if( !$exists ) {
				$images = $this->getPhotos($oldNews['icon']);
				
				if (!empty($images)) {
	                foreach($images as $i => $imageArr) {
	                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
	                    $filesystem = new Filesystem();
	                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
	
	                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
	                    $image->setTitle('');
	                    $image->setSort($i);
	                    $news->addImages($image);
	                }
	            }
				
				if ( !empty($oldNews['video']) ) {
					$videoUrl = preg_replace('/http:\/\/(www.)?krd.ru\//i', '/', $oldNews['video']);
					$videoUrl = '/home/krdru/data/www/krd.ru/web' . $videoUrl;
					
					if( is_file($videoUrl) ) {
		                $tempPath = $this->getUploadDir().'/'.uniqid('video-', true).'.'.pathinfo(basename($oldNews['video']), PATHINFO_EXTENSION);
		                $filesystem = new Filesystem();
		                $filesystem->copy($videoUrl, $tempPath);
		
		                $video = $this->em->getRepository('QFilesBundle:Video')->createVideoFromFile($tempPath);
		                $video->setTitle('');
		                $news->addVideo($video);
		                $this->em->flush();
		
		                if (empty($oldNews['video_icon'])) {
		                    $this->em->getRepository('QFilesBundle:Video')->cretePreview($video);
		                } else {
		                	foreach( $this->getPhotos($oldNews['video_icon'], 'size2') as $imageArr ) {
								if( !empty($imageArr['sourceFile']) && is_file($imageArr['sourceFile']) ) {
				                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
				                    $filesystem = new Filesystem();
				                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
				
				                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
				                    $video->setImage($image);
			                    	$this->em->flush();
								}
							}
		                }
					}
	            }
			}
			
			$this->em->flush();
			$newsProcessed++;
			if ($newsProcessed % 30 == 0) {
				$this->em->getConnection()->commit();
                $this->em->clear('KrdNewsBundle:News');
                $output->writeln("Processed {$newsProcessed}");
                $this->em->getConnection()->beginTransaction();
			}
		}
		$this->em->getConnection()->commit();
		$output->writeln('imported '.$newsProcessed.' news...');
	}
	
	function getTags($newsId) {
		$list = $this->gem->getConnection()->prepare("SELECT `tag` FROM `evlanov__tags` WHERE `object_type`='news' AND id_tag=:news_id ORDER BY `tag`");
		$list->execute(array('news_id' => $newsId));
		
		$tags = array();
		while( $row = $list->fetch() ) {
			$tags[] = $row['tag'];
		}
		
		return $tags;
	}
	
	function getPhotos($group_id,$size='size3'){
		$list = $this->gem->getConnection()->prepare("SELECT * FROM `evlanov__photos2` WHERE `group_id`=:group_id ORDER BY `main` DESC, `ord`");
		$list->execute(array('group_id' => $group_id));
		
		$arr = array();
		while($r = $list->fetch()) {
			$photo = unserialize($r['photo']);
			if( !empty($photo[$size]) )
				$r['sourceFile'] = '/home/krdru/data/www/glava.krd.ru/files/image/news/' . $photo[$size];
			$arr[] = $r;
		}

		return $arr;
	}
	
	private function loadNews()
	{ 
		$list = $this->gem->getConnection()->prepare('SELECT * FROM evlanov__news WHERE date >= "2015-05-21" ORDER BY date DESC LIMIT 0, 1800');
		$list->execute();
		
		$news = array();
		while( $row = $list->fetch() ) {
			$news[] = $row;
		}
		echo count($news), "\n";
		return $news;
	}
}