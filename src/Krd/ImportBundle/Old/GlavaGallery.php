<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;
use Krd\GalleryBundle\Entity\Gallery;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Импорт раздела фото с сайта glava.krd.ru на страницу главы на сайт krd.ru
 */
class GlavaGallery
{
    protected $em;
	protected $gem;
	
	protected $uploadDir;

    public function __construct(EntityManager $em, EntityManager $gem)
    {
        $this->em = $em;
		$this->gem = $gem;
    }
	
	public function setUploadDir($dir)
	{
		$this->uploadDir = $dir;
	}
	
	public function getUploadDir()
	{
		return $this->uploadDir;
	}
	
	public function import($output) 
	{
		$galleryList = $this->loadGallery();
		$galleryNode = $this->em->find('QCoreBundle:Node', 15268);
		
		$galleryProcessed = 0;
		$this->em->getConnection()->beginTransaction();
		foreach( $galleryList as $oldGallery ) {
			$name = 'gallery-'.date('dmY', strtotime($oldGallery['date'])).'-'.$oldGallery['id'];
			$exists = false;
			$gallery = $this->em->getRepository('KrdGalleryBundle:Gallery')->findBy(array('name' => $name));
			
			if( !empty($gallery) ) {
				$gallery = array_pop($gallery);
				$gallery->setRefId((int)$oldGallery['id']);
				$gallery->setTitle(htmlspecialchars_decode($gallery->getTitle()));
				//$output->writeln("Exists {$gallery->getId()}:{$name}");
				
				$exists = true;
			} else {
				$gallery = new Gallery();
				$gallery->setTitle($oldGallery['name']);
				
				$gallery->setParent($galleryNode);
				$gallery->setName($name);
				$gallery->setRefId((int)$oldGallery['id']);
				$gallery->setDate(new \DateTime($oldGallery['date']));
				$gallery->setActive(true);
				$this->em->persist($gallery);
			}
			
			if( !$exists ) {
				$images = $this->getPhotos($oldGallery['icon']);
				
				if (!empty($images)) {
	                foreach($images as $i => $imageArr) {
	                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
	                    $filesystem = new Filesystem();
	                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
	
	                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
	                    $image->setTitle('');
	                    $image->setSort($i);
	                    $gallery->addImages($image);
	                }
	            }
			}
			
			$this->em->flush();
			$galleryProcessed++;
			if ($galleryProcessed % 30 == 0) {
				$this->em->getConnection()->commit();
                $this->em->clear('KrdGalleryBundle:Gallery');
                $output->writeln("Processed {$galleryProcessed}");
                $this->em->getConnection()->beginTransaction();
			}
		}
		$this->em->getConnection()->commit();
		$output->writeln('imported '.$galleryProcessed.' gallries...');
	}
	
	function getPhotos($group_id,$main=0){
		$list = $this->gem->getConnection()->prepare("SELECT * FROM `evlanov__photos2` WHERE `group_id`=:group_id ORDER BY `main` DESC, `ord`");
		$list->execute(array('group_id' => $group_id));
		
		$arr = array();
		while($r = $list->fetch()) {
			$photo = unserialize($r['photo']);
			$r['sourceFile'] = '/home/krdru/data/www/glava.krd.ru/files/image/gallery/' . $photo['size3'];
			$arr[] = $r;
		}

		return $arr;
	}
	
	private function loadGallery()
	{
		$list = $this->gem->getConnection()->prepare('SELECT * FROM evlanov__gallery');
		$list->execute();
		
		$gallery = array();
		while( $row = $list->fetch() ) {
			$gallery[] = $row;
		}
		
		return $gallery;
	}
}