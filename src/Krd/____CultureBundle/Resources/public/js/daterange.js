!function(){"use strict";var e=angular.module("pasvaz.bindonce",[]);e.directive("bindonce",function(){var e=function(e){if(e&&0!==e.length){var t=angular.lowercase(""+e);e=!("f"===t||"0"===t||"false"===t||"no"===t||"n"===t||"[]"===t)}else e=!1;return e},t=parseInt((/msie (\d+)/.exec(angular.lowercase(navigator.userAgent))||[])[1],10);isNaN(t)&&(t=parseInt((/trident\/.*; rv:(\d+)/.exec(angular.lowercase(navigator.userAgent))||[])[1],10));var r={restrict:"AM",controller:["$scope","$element","$attrs","$interpolate",function(r,a,i,n){var c=function(t,r,a){var i="show"===r?"":"none",n="hide"===r?"":"none";t.css("display",e(a)?i:n)},o=function(e,t){if(angular.isObject(t)&&!angular.isArray(t)){var r=[];angular.forEach(t,function(e,t){e&&r.push(t)}),t=r}t&&e.addClass(angular.isArray(t)?t.join(" "):t)},s=function(e,t){e.transclude(t,function(t){var r=e.element.parent(),a=e.element&&e.element[e.element.length-1],i=r&&r[0]||a&&a.parentNode,n=a&&a.nextSibling||null;angular.forEach(t,function(e){i.insertBefore(e,n)})})},l={watcherRemover:void 0,binders:[],group:i.boName,element:a,ran:!1,addBinder:function(e){this.binders.push(e),this.ran&&this.runBinders()},setupWatcher:function(e){var t=this;this.watcherRemover=r.$watch(e,function(e){void 0!==e&&(t.removeWatcher(),t.checkBindonce(e))},!0)},checkBindonce:function(e){var t=this,r=e.$promise?e.$promise.then:e.then;"function"==typeof r?r(function(){t.runBinders()}):t.runBinders()},removeWatcher:function(){void 0!==this.watcherRemover&&(this.watcherRemover(),this.watcherRemover=void 0)},runBinders:function(){for(;this.binders.length>0;){var r=this.binders.shift();if(!this.group||this.group==r.group){var a=r.scope.$eval(r.interpolate?n(r.value):r.value);switch(r.attr){case"boIf":e(a)&&s(r,r.scope.$new());break;case"boSwitch":var i,l=r.controller[0];(i=l.cases["!"+a]||l.cases["?"])&&(r.scope.$eval(r.attrs.change),angular.forEach(i,function(e){s(e,r.scope.$new())}));break;case"boSwitchWhen":var u=r.controller[0];u.cases["!"+r.attrs.boSwitchWhen]=u.cases["!"+r.attrs.boSwitchWhen]||[],u.cases["!"+r.attrs.boSwitchWhen].push({transclude:r.transclude,element:r.element});break;case"boSwitchDefault":var u=r.controller[0];u.cases["?"]=u.cases["?"]||[],u.cases["?"].push({transclude:r.transclude,element:r.element});break;case"hide":case"show":c(r.element,r.attr,a);break;case"class":o(r.element,a);break;case"text":r.element.text(a);break;case"html":r.element.html(a);break;case"style":r.element.css(a);break;case"src":r.element.attr(r.attr,a),t&&r.element.prop("src",a);break;case"attr":angular.forEach(r.attrs,function(e,t){var a,i;t.match(/^boAttr./)&&r.attrs[t]&&(a=t.replace(/^boAttr/,"").replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase(),i=r.scope.$eval(r.attrs[t]),r.element.attr(a,i))});break;case"href":case"alt":case"title":case"id":case"value":r.element.attr(r.attr,a)}}}this.ran=!0}};return l}],link:function(e,t,r,a){var i=r.bindonce&&e.$eval(r.bindonce);void 0!==i?a.checkBindonce(i):(a.setupWatcher(r.bindonce),t.bind("$destroy",a.removeWatcher))}};return r}),angular.forEach([{directiveName:"boShow",attribute:"show"},{directiveName:"boHide",attribute:"hide"},{directiveName:"boClass",attribute:"class"},{directiveName:"boText",attribute:"text"},{directiveName:"boBind",attribute:"text"},{directiveName:"boHtml",attribute:"html"},{directiveName:"boSrcI",attribute:"src",interpolate:!0},{directiveName:"boSrc",attribute:"src"},{directiveName:"boHrefI",attribute:"href",interpolate:!0},{directiveName:"boHref",attribute:"href"},{directiveName:"boAlt",attribute:"alt"},{directiveName:"boTitle",attribute:"title"},{directiveName:"boId",attribute:"id"},{directiveName:"boStyle",attribute:"style"},{directiveName:"boValue",attribute:"value"},{directiveName:"boAttr",attribute:"attr"},{directiveName:"boIf",transclude:"element",terminal:!0,priority:1e3},{directiveName:"boSwitch",require:"boSwitch",controller:function(){this.cases={}}},{directiveName:"boSwitchWhen",transclude:"element",priority:800,require:"^boSwitch"},{directiveName:"boSwitchDefault",transclude:"element",priority:800,require:"^boSwitch"}],function(t){var r=200;return e.directive(t.directiveName,function(){var e={priority:t.priority||r,transclude:t.transclude||!1,terminal:t.terminal||!1,require:["^bindonce"].concat(t.require||[]),controller:t.controller,compile:function(e,r,a){return function(e,r,i,n){var c=n[0],o=i.boParent;if(o&&c.group!==o){var s=c.element.parent();c=void 0;for(var l;9!==s[0].nodeType&&s.length;){if((l=s.data("$bindonceController"))&&l.group===o){c=l;break}s=s.parent()}if(!c)throw new Error("No bindonce controller: "+o)}c.addBinder({element:r,attr:t.attribute||t.directiveName,attrs:i,value:i[t.directiveName],interpolate:t.interpolate,group:o,transclude:a,controller:n.slice(1),scope:e})}}};return e})})}();

(function(){
    angular.module("dateRangePicker", ['pasvaz.bindonce']).directive("dateRangePicker", ["$compile", "$parse", "$timeout", function($compile, $parse, $timeout){
		var pickerTemplate = '<div><div class="page-title nm d-line">{select_period} \
		<div class="daterange-single"> \
		    <div class="selectors"> \
		    	<div year-select="year" from="years" calculate="calculateRange"></div> \
		    </div> \
		</div> \
	</div> \
	<div class="date-range-block"> \
		<p>{select_date_title}</p> \
	<div class="daterange-picker" ng-click="handlePickerClick($event)" ng-class="{\'daterange-ranged\': showRanged }"> \
					<div class="daterange-timesheet clearfix"> \
						<a ng-click="move(-1, $event)" class="prev"><i></i></a> \
						<div bindonce ng-repeat="month in months" class="daterange-month"> \
							<div class="daterange-month-name" bo-text="month.name"></div> \
							<table class="daterange-calendar"> \
								<tr bindonce ng-repeat="week in month.weeks"> \
									<td bo-class=\'{ "daterange-available": day.available, "daterange-day": day, "daterange-selected": day.selected, "daterange-disabled": day.disabled, "daterange-start": day.start}\' ng-repeat="day in week track by $index" ng-click="select(day, $event)"> \
										<div class="daterange-day-wrapper" bo-text="day.date.date()"></div> \
									</td> \
								</tr> \
							</table> \
						</div> \
						<a ng-click="move(+1, $event)" class="next"><i></i></a> \
					</div> \
					<div class="actionbar clearfix" ng-show="!!model"> \
						<div class="action-bar-status" ng-show="selection && !withoutRange">{your_select_at} {{ model.start.format(\'DD-MM-YYYY\') }} {of} {{ model.end.format(\'DD-MM-YYYY\') }}</div> \
						<div class="action-bar-status" ng-show="selection && withoutRange">{your_select} {{ model.start.format(\'DD-MM-YYYY\') }}</div> \
						<a ng-click="ok($event)" class="btn btn-blue">{done}</a> \
						<a ng-click="cancel($event)" ng-show="!preventCancel" class="btn btn-blue" style="margin-right: 20px;">{cancel}</a> \
					</div> \
				</div></div></div>';
        return {
            restrict: "AE",
            replace: true,
			template: function(){
				if( window.location.hostname.indexOf('en.krd.ru') >= 0 ) {
					moment.locale('en');
					pickerTemplate = pickerTemplate.replace('{select_period}', 'Select date range');
					pickerTemplate = pickerTemplate.replace('{select_date_title}', 'Select a day or period of the start and end date');
					pickerTemplate = pickerTemplate.replace('{cancel}', 'Cancel');
					pickerTemplate = pickerTemplate.replace('{your_select}', 'You chose');
					pickerTemplate = pickerTemplate.replace('{of}', 'and');
					pickerTemplate = pickerTemplate.replace('{your_select_at}', 'You chose between');
					pickerTemplate = pickerTemplate.replace('{done}', 'Done');
				} else {
					pickerTemplate = pickerTemplate.replace('{select_period}', 'Выбор периода новостей');
					pickerTemplate = pickerTemplate.replace('{select_date_title}', 'Выберите день или период отметив его начальную и конечную дату');
					pickerTemplate = pickerTemplate.replace('{cancel}', 'Отмена');
					pickerTemplate = pickerTemplate.replace('{your_select}', 'Вы выбрали');
					pickerTemplate = pickerTemplate.replace('{your_select_at}', 'Вы выбрали период с');
					pickerTemplate = pickerTemplate.replace('{of}', 'по');
					pickerTemplate = pickerTemplate.replace('{done}', 'Показать');
				}
				return pickerTemplate;
			},
            scope: {
                model: "=ngModel",
                customSelectOptions: "=",
                ranged: "=",
                pastDates: "@",
                callback: "&"
            },
            link: function($scope, element, attrs){
                var documentClickFn, domEl, _calculateRange, _checkQuickList, _makeQuickList, _prepare;
				CUSTOM = "CUSTOM";
                $scope.quickListDefinitions = $scope.customSelectOptions;
                if ($scope.quickListDefinitions == null) {
                    $scope.quickListDefinitions = [{
                        label: "This week",
                        range: moment().range(moment().startOf("week").startOf("day"), moment().endOf("week").startOf("day"))
                    }, {
                        label: "Next week",
                        range: moment().range(moment().startOf("week").add(1, "week").startOf("day"), moment().add(1, "week").endOf("week").startOf("day"))
                    }, {
                        label: "This fortnight",
                        range: moment().range(moment().startOf("week").startOf("day"), moment().add(1, "week").endOf("week").startOf("day"))
                    }, {
                        label: "This month",
                        range: moment().range(moment().startOf("month").startOf("day"), moment().endOf("month").startOf("day"))
                    }, {
                        label: "Next month",
                        range: moment().range(moment().startOf("month").add(1, "month").startOf("day"), moment().add(1, "month").endOf("month").startOf("day"))
                    }];
                }
                $scope.quick = null;
                $scope.range = null;
                $scope.selecting = false;
                $scope.visible = false;
                $scope.start = null;
				$scope.withoutRange = false;
                $scope.showRanged = true;
				$scope.preventCancel = true;
				
				$scope.startMonth = 0;
				$scope.endMonth = 11;
				$scope.currentMonth = 0;
				
				$scope.links = $parse(attrs.links)($scope);
				var years = [];
				$scope.years = [];
				for(var n in $scope.links) {
					years.push($scope.links[n].split('/')[2]);
				}
				$scope.years = $.grep(years, function(v, k){
					    return $.inArray(v ,years) === k;
					}).reverse();
				$scope.year = parseInt($scope.years[0]);
				
                _makeQuickList = function(includeCustom){
                    var e, _i, _len, _ref, _results;
                    if (includeCustom == null) {
                        includeCustom = false;
                    }
                    $scope.quickList = [];
                    if (includeCustom) {
                        $scope.quickList.push({
                            label: "Custom",
                            range: CUSTOM
                        });
                    }
                    _ref = $scope.quickListDefinitions;
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        e = _ref[_i];
                        _results.push($scope.quickList.push(e));
                    }
                    return _results;
                };
                $scope.calculateRange = function(){
                    var end, start;
                    r = $scope.range = $scope.selection ? 
						(start = $scope.selection.start.clone().startOf("month").startOf("day"), 
						end = start.clone().add(2, "months").endOf("month").startOf("day"),
						 moment().range(start, end)) 
						
						: moment().range(moment().startOf("month").subtract(1, "month").startOf("day"), moment().endOf("month").add(1, "month").startOf("day"));
					$scope.currentMonth = r.start.month();
					return r;
                };
                _checkQuickList = function(){
                    var e, _i, _len, _ref;
                    if (!$scope.selection) {
                        return;
                    }
                    _ref = $scope.quickList;
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        e = _ref[_i];
                        if (e.range !== CUSTOM && $scope.selection.start.startOf("day").unix() === e.range.start.startOf("day").unix() 
							&& $scope.selection.end.startOf("day").unix() === e.range.end.startOf("day").unix()) {
                            $scope.quick = e.range;
                            _makeQuickList();
                            return;
                        }
                    }
                    $scope.quick = CUSTOM;
                    return _makeQuickList(true);
                };
                $scope.prepare = function(){
                    var m, startDay, startIndex, _i, _len, _ref;
                    $scope.months = [];
                    startIndex = $scope.range.start.year() * 12 + $scope.range.start.month();
                    startDay = moment().startOf("week").day();
                    $scope.range.by("days", function(date){
                        var d, dis, m, sel, w, _base, _base1;
                        d = date.day() - startDay;
                        if (d < 0) {
                            d = 7 + d;
                        }
                        m = date.year() * 12 + date.month() - startIndex;
                        w = parseInt((7 + date.date() - d) / 7);
                        sel = false;
                        dis = false;
                        if ($scope.start) {
                            sel = date === $scope.start;
                            dis = date < $scope.start;
                        }
                        else {
                            sel = $scope.selection && $scope.selection.contains(date);
							/*if ($scope.selection) {
								if (date < $scope.selection.start || date > $scope.selection.end) {
									dis = true;
								}
							}*/
                        }
                        (_base = $scope.months)[m] ||
                        (_base[m] = {
                            name: date.format("MMMM"),
                            weeks: []
                        });
                        (_base1 = $scope.months[m].weeks)[w] || (_base1[w] = []);
                        return $scope.months[m].weeks[w][d] = {
                            date: date,
                            selected: sel,
							available : $scope.links.indexOf(date.format('DD/MM/YYYY')) != -1,
                            disabled: dis,
                            start: $scope.start && $scope.start.unix() === date.unix()
                        };
                    });
                    _ref = $scope.months;
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        m = _ref[_i];
                        if (!m.weeks[0]) {
                            m.weeks.splice(0, 1);
                        }
                    }
                    return _checkQuickList();
                };
                $scope.show = function(){
                    $scope.selection = $scope.model;
                    $scope.calculateRange();
                    $scope.prepare();
                    return $scope.visible = true;
                };
                $scope.hide = function($event){
                    if ($event != null) {
                        if (typeof $event.stopPropagation === "function") {
                            $event.stopPropagation();
                        }
                    }
                    $scope.visible = false;
                    return $scope.start = null;
                };
                $scope.prevent_select = function($event){
                    return $event != null ? typeof $event.stopPropagation === "function" ? $event.stopPropagation() : void 0 : void 0;
                };
                $scope.ok = function($event){
                    if ($event != null) {
                        if (typeof $event.stopPropagation === "function") {
                            $event.stopPropagation();
                        }
                    }
                    if( !$scope.withoutRange ) {
						window.location = '?date_begin='+$scope.model.start.format('DD-MM-YYYY')+'&date_end='+$scope.model.end.format('DD-MM-YYYY');
					} else {
						window.location = '?date='+$scope.model.start.format('DD-MM-YYYY');
					}
                    $timeout(function(){
                        if ($scope.callback) {
                            return $scope.callback();
                        }
                    });
                    return $scope.hide();
                };
                $scope.select = function(day, $event){
                    if ($event != null) {
                        if (typeof $event.stopPropagation === "function") {
                            $event.stopPropagation();
                        }
                    }
                    if (day.disabled) {
                        return;
                    }
					$scope.preventCancel = false;
                    $scope.selecting = !$scope.selecting;
					var start, end;
					
                    if ($scope.selecting) {
                        $scope.start = day.date;
						$scope.selection = moment().range($scope.start, day.date);
						$scope.model = $scope.selection;
                    } else {
						if( day.date < $scope.start ) {
							start = day.date;
							end = $scope.start;
							$scope.start = start;
						} else {
							start = $scope.start;
							end = day.date;
						}
						
                        $scope.selection = moment().range(start, end);
						$scope.model = $scope.selection;
                        $scope.start = null;
                    }
					$scope.withoutRange = $scope.model.start.startOf('day').unix() === $scope.model.end.startOf('day').unix();
                    return $scope.prepare();
                };
				$scope.cancel = function($event) {
					$event.preventDefault();
					$scope.selection = null;
					$scope.model = null;
					$scope.start = null;
					$scope.preventCancel = true;
					$scope.selecting = false;
					return $scope.prepare();
				};
                $scope.move = function(n, $event){
                    if ($event != null) {
                        if (typeof $event.stopPropagation === "function") {
                            $event.stopPropagation();
                        }
                    }
					
					console.log($scope.currentMonth, n);
					if( n < 0 ) {
						if( $scope.currentMonth < 1 ) {
							return;
						}
					} else if( n > 0 ) {
						if( $scope.currentMonth > 11 ) {
							return;
						}
					}
					
					var start = $scope.currentMonth === 1 ? $scope.range.start.startOf('year') : $scope.range.start.add(n, 'months').startOf("month").startOf("day");
					console.log($scope.currentMonth, n, start.month(), start);
                    $scope.range = moment().range(start, $scope.range.start.clone().add(2, "months").endOf("month").startOf("day"));
					$scope.currentMonth += n;
                    return $scope.prepare();
                };
                $scope.handlePickerClick = function($event){
                    return $event != null ? typeof $event.stopPropagation === "function" ? $event.stopPropagation() : void 0 : void 0;
                };
                $scope.$watch("quick", function(q, o){
                    if (!q || q === CUSTOM) {
                        return;
                    }
                    $scope.selection = $scope.quick;
                    $scope.selecting = false;
                    $scope.start = null;
                    $scope.calculateRange();
                    return $scope.prepare();
                });
                $scope.$watch("customSelectOptions", function(value){
                    if (typeof customSelectOptions === "undefined" || customSelectOptions === null) {
                        return;
                    }
                    return $scope.quickListDefinitions = value;
                });
                $scope.show();
				
                _makeQuickList();
                $scope.calculateRange();
                return $scope.prepare();
            }
        };
    }
]).directive('yearSelect', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            template: '<div class="select" ng-class="className()">'
                        + '<div class="value">{{ selected }}</div>'
                        + '<div class="list">'
                            + '<div class="item" ng-repeat="item in list track by $index" ng-click="clkValue(item)">'
                                + '{{ item }}'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            replace: true,
            scope:{
                value: '=yearSelect',
                list: '=from',
                disabled: '='
            },

            link: function($scope, $element, attrs) {
                $scope.name = attrs.yearSelect;
                $scope.selected = '---';
                $scope.$list = $element.find('.list');
				
                $scope.$watch('list', function(list) {
                    for(var i = 0; i < list.length; i++) {
                        if (list[i] == $scope.value) {
                            $scope.selected = list[i];
                        }
                    }
                });

                $scope.$watch('selected', $scope.updateListWidth);
                $element.on('mouseenter', $scope.updateListWidth);
            },

            controller: function($scope, $element) {
                $scope.updateListWidth = function() {
                    $scope.$list.css({width:'auto'});
                    if ($scope.$list.outerWidth(true) < $element.outerWidth(true)) {
                        $scope.$list.width($element.width() - 1);
                    }
                };

                $scope.clkValue = function(item) {
					var now = new Date();
                    $scope.selected = item;
					now.setYear( item );
					console.log(now);
					
					$scope.$parent.range = moment().range(moment(now).startOf("month").subtract(1, "month").startOf("day"), moment(now).endOf("month").add(1, "month").startOf("day"))
					$scope.$parent.prepare();
					
                    $scope.value = item;
                    $scope.$list.hide();
                    $timeout(function() {
                        $scope.$list.removeAttr('style');
                    }, 100);
                };

                $scope.className = function() {
                    var className = $scope.name;

                    if ($scope.disabled) {
                        className += ' disabled';
                    }

                    if ($scope.hidden) {
                        className += ' hidden';
                    }

                    return className;
                };
            }
        }
    }])
    
}).call(this);