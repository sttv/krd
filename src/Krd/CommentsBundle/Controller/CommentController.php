<?php

namespace Krd\CommentsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\UserBundle\Entity\User;
use Krd\CommentsBundle\Entity\Comment;


/**
 * Комментарии
 * @PreAuthorize("hasRole('ROLE_USER')")
 */
class CommentController extends Controller
{
    /**
     * Создание комментария
     *
     * @Route("/create/{type}", defaults={"_format"="json"}, name="krd_comments_create")
     * @Method({"POST"})
     * @Template()
     */
    public function createAction(Request $request, $type)
    {
        switch($type) {
            case 'root':
                $formBuilder = $this->get('form.factory')->createNamedBuilder('comment_root_form', 'comments_comment_create');
                break;

            default:
                $formBuilder = $this->get('form.factory')->createNamedBuilder('comment_form', 'comments_comment_create');
                break;
        }

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }

        $comment = $form->getData();

        if ($comment instanceof Comment) {
            if ($parent = $comment->getParent()) {
                if (!$parent->getActive()) {
                    return array('success' => false, 'message' => 'Нельзя отвечать на неопубликованный комментарий');
                }
            }

            $comment->setActive($this->get('security.context')->isGranted('ROLE_COMMENT_ADMIN'));
            $comment->setIp($request->getClientIp());

            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            if ($parent = $comment->getParent()) {
                $parentId = $parent->getId();
            } else {
                $parentId = 0;
            }

            return array(
                'success' => true,
                'where' => $comment->getPageHash(),
                'parentId' => $parentId,
                'newElement' => $this->get('twig')->render('KrdCommentsBundle:blocks:comments-item.html.twig', array('item' => $comment)),
            );
        } else {
            return array('success' => false, 'message' => 'Ошибка сервера');
        }
    }

    /**
     * Редактирование комментария
     *
     * @Route("/edit/{id}", defaults={"_format"="json"}, name="krd_comments_edit")
     * @Method({"POST"})
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $comment = $this->getDoctrine()->getManager()->find('KrdCommentsBundle:Comment', $id);

        if ($comment instanceof Comment) {

            if (!$this->get('security.context')->isGranted('ROLE_COMMENT_ADMIN')) {
                $user = $this->get('security.context')->getToken()->getUser();

                if ($user instanceof User) {
                    if ($comment->getCreatedBy()->getId() != $user->getId()) {
                        return array('success' => false, 'message' => 'Нельзя редактировать чужие комментарии');
                    }
                } else {
                    return array('success' => false, 'message' => 'Необходима авторизация');
                }
            }

            $data = $request->get('comment_form');

            if (isset($data['content'])) {
                $data['content'] = strip_tags($data['content']);
                $data['content'] = htmlspecialchars($data['content']);
            }

            if (empty($data['content'])) {
                return array('success' => false, 'message' => 'Нужно указать текст комментария');
            }

            if ($data['content'] == $comment->getContent()) {
                return array('success' => false, 'message' => 'Вы не изменили текст комментария');
            }

            $comment->setContent($data['content']);

            if (!$this->get('security.context')->isGranted('ROLE_COMMENT_ADMIN')) {
                $comment->setActive(false);

                $message = $this->get('qcore.mailer')->newMessage('Редактирование комментария на сайте %title%');

                try {
                    $this->get('qcore.mailer')->sendTemplate($message, 'KrdCommentsBundle:Email:edit-comment.admin.html.twig', array('comment' => $comment));
                } catch (\Exception $e) {
                    $this->get('logger')->crit('Ошибка отправки email уведомления о редактировании комментарии', array($comment->getId(), $e->getMessage()));
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return array('success' => true);
        } else {
            return array('success' => false, 'message' => 'Ошибка сервера');
        }
    }

    /**
     * Активация комментария
     *
     * @Route("/activate/{id}", defaults={"_format"="json"}, requirements={"id"="^\d+$"}, name="krd_comments_activate")
     * @Method({"POST"})
     * @Secure(roles="ROLE_COMMENT_ADMIN")
     * @Template()
     */
    public function activateAction($id)
    {
        $comment = $this->getDoctrine()->getManager()->find('KrdCommentsBundle:Comment', $id);

        if ($comment instanceof Comment) {
            $comment->setActive(true);
            $this->getDoctrine()->getManager()->flush();

            return array('success' => true);
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }

    /**
     * Деактивация комментария
     *
     * @Route("/deactivate/{id}", defaults={"_format"="json"}, requirements={"id"="^\d+$"}, name="krd_comments_deactivate")
     * @Method({"POST"})
     * @Secure(roles="ROLE_COMMENT_ADMIN")
     * @Template()
     */
    public function deactivateAction($id)
    {
        $comment = $this->getDoctrine()->getManager()->find('KrdCommentsBundle:Comment', $id);

        if ($comment instanceof Comment) {
            $comment->setActive(false);
            $this->getDoctrine()->getManager()->flush();

            return array('success' => true);
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }

    /**
     * Удаление комментария и всех дочерних комментариев
     *
     * @Route("/delete/{id}", defaults={"_format"="json"}, requirements={"id"="^\d+$"}, name="krd_comments_delete")
     * @Method({"DELETE"})
     * @Secure(roles="ROLE_COMMENT_ADMIN")
     * @Template()
     */
    public function deleteAction($id)
    {
        $comment = $this->getDoctrine()->getManager()->find('KrdCommentsBundle:Comment', $id);

        if ($comment instanceof Comment) {
            $this->getDoctrine()->getManager()->remove($comment);
            $this->getDoctrine()->getManager()->flush();

            return array('success' => true);
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }
}
