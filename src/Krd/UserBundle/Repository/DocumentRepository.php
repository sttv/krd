<?php

namespace Krd\UserBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\Entity\Document;


/**
 * Репозиторий пользовательских документов
 */
class DocumentRepository extends EntityRepository
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @JMSDI\InjectParams({
     *     "securityContext" = @JMSDI\Inject("security.context")
     * })
     */
    public function setUserBySecurityContext(SecurityContext $securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * Создание нового документа
     * @param  array $data
     * @return Document|false
     */
    public function createMyDocument($data)
    {
        if ($this->user instanceof User) {
            if (isset($data['directory']) && !empty($data['directory'])) {
                $directory = $this->_em->getRepository('KrdUserBundle:DocumentDirectory')->getMyDirectory($data['directory']);

                if (!$directory) {
                    return false;
                }
            }

            $document = new Document();

            if (isset($data['title']) && !empty($data['title'])) {
                $title = strip_tags($data['title']);
                $document->setTitle($title);
                $document->setParent($directory);
                $directory->addDocument($document);

                $this->_em->persist($document);
                $this->_em->flush($document);

                return $document;
            }
        } else {
            return false;
        }
    }

    /**
     * Получение документа пользователя
     * @param  integer $id
     * @return Document|Null
     */
    public function getMyDocument($id)
    {
        if ($this->user instanceof User) {
            return $this->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId()));
        }
    }

    /**
     * Получение документов пользователя
     * @param  integer[optional] $count
     * @return ArrayCollection|Null
     */
    public function getMyDocuments($count = null)
    {
        if ($this->user instanceof User) {
            $qb = $this->createQueryBuilder('e')
                ->andWhere('e.createdBy = :user')
                ->addOrderBy('e.created', 'DESC')
                ->setParameter('user', $this->user->getId());

            if (is_numeric($count) && $count > 0) {
                $qb->setMaxResults($count);
            }

            return $qb->getQuery()->getResult();
        }

        return new ArrayCollection();
    }

    /**
     * Редактирование параметров документа текущего пользователя
     * @param  integer $id
     * @param  array $data
     * @return Document|false
     */
    public function editMyDocument($id, $data)
    {
        if ($this->user instanceof User) {
            $document = $this->getMyDocument($id);

            if ($document) {
                if (isset($data['title']) && !empty($data['title'])) {
                    $title = strip_tags($data['title']);
                    $document->setTitle($title);
                }

                if (isset($data['content']) && !empty($data['content'])) {
                    $document->setContent($data['content']);
                }


                $this->_em->flush($document);
                return $document;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Удаление документа текущего пользователя по ID
     * @param  integer $id
     * @return boolean
     */
    public function removeMyDocument($id)
    {
        if ($this->user instanceof User) {
            $document = $this->getMyDocument($id);

            if ($document) {
                $this->_em->remove($document);
                $this->_em->flush($document);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
