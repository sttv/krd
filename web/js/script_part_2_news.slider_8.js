angular.module('news.slider', [])
    /**
     * Слайдер новостей
     */
    .directive('newsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowTop = $('<div class="arrow top" ng-click="scrollUp()" ng-class="{hidden: position <= 0 || $items.length == 1}"><i></i></div>');

                var $arrowBottom = $('<div class="arrow bottom" ng-click="scrollDown()" ng-class="{hidden: position >= ($items.length - onPage + visibleDiff) || $items.length == 1}"><i></i></div>');

                var $iconsSliders = $element.find('.icon > .icons-slider');

                $element.find('.items')
                    .append($arrowTop)
                    .append($arrowBottom)
                    .find('.item').each(function(i) {
                        $(this).attr({
                            'ng-class': '{active: active == '+i+'}',
                            'ng-click': 'setActive('+i+')'
                        });
                    });

                $element.find('.items > .btn-block').attr({
                    'ng-class': '{hidden: position < $items.length - onPage + 1}'
                });

                return function($scope, $element, attrs) {
                    $scope.$itemsWrap = $element.find('.items > .wrap');
                    $scope.$itemsIcons = $element.find('.icon > .icons-slider');
                    $scope.$items = $element.find('.items > .wrap > .item');
                    $scope.$btnLink = $element.find('.items > .btn-block');
                    $scope.onPage = !!attrs.onPage ? parseInt(attrs.onPage, 10) : 3;
                    $scope.visibleDiff = ($element.find('.items > .btn-block').length > 0) ? 1 : 0;
                    $scope.position = 0;

                    $scope.$watch('position', function(position) {
                        var $item = $scope.$items.eq(position);

                        if ($item.length != 1) {
                            return;
                        }

                        var mt = $item.position().top;

                        $scope.$itemsWrap.stop().animate({marginTop:-mt}, 300);
                    });

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$itemsIcons.eq(active);
                        $scope.$itemsIcons.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;
                $scope.position = 0;

                $scope.setActive = function(active) {
                    $scope.active = active;
                    $scope.setPosition(active - 1);
                };

                $scope.setPosition = function(position) {
                    if (position < 0) {
                        position = 0;
                    } else if (position >= ($scope.$items.length - $scope.visibleDiff)) {
                        position = $scope.$items.length - $scope.onPage;
                    }

                    $scope.position = position;
                };

                $scope.scrollUp = function() {
                    if ($scope.position > 0) {
                        $scope.position--;
                    }
                };

                $scope.scrollDown = function() {
                    if ($scope.position < ($scope.$items.length - $scope.onPage + $scope.visibleDiff)) {
                        $scope.position++;
                    }
                };
            }
        }
    }])
;
