<?php

namespace Krd\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * FAQ
 *
 * @ORM\Entity
 * @ORM\Table(name="faq",
 *      indexes={
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     name={"text", {"label"="Автор вопроса","required"=false}},
 *     title={"text", {"label"="Вопрос"}},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     reply={"textarea", {
 *         "label"="Ответ",
 *         "required"=false,
 *         "attr"={"class"="tinymce", "data-theme"="advanced"}
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Faq
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Название
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;
	
	/**
	 * ФИО задающего вопрос
	 * @ORM\Column(type="text", nullable=true)
	 * 
	 * @JMS\Expose
	 */
	private $name;

    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;


    /**
     * Краткое содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $reply = '';


    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;
	
	/**
	 * Dangozero
	 * 
	 * Возвращает дату для страницу Вопросы у главы.
	 * 
	 * @JMS\VirtualProperty
     * @JMS\SerializedName("date2")
     * @JMS\Type("DateTime<'d/m/Y (H:i)'>")
	 */
	public function getGlavaDate()
	{
		return $this->date;
	}


    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
	
	public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getReply()
    {
        return $this->reply;
    }

    public function setReply($reply)
    {
        $this->reply = $reply;
    }

    public function hasReply()
    {
        return !empty($this->reply);
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}


