/**
 * Закладки пользователя
 */
angular.module('user.bookmarks', ['user.notifications', 'forms'])
    /**
     * Управление закладками в личном кабинете
     */
    .directive('userBookmarks', ['$http', 'notifications', function($http, notifications) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.children('.remove-selected-btn')
                    .attr('ng-click', 'removeSelected()')
                    .attr('ng-show', 'hasSelected()');

                return function($scope, $element, attrs) {
                    $scope.selectedDirectories = {};
                    $scope.selectedBookmarks = {};
                    $scope.urlRemove = attrs.urlRemove;

                    $element.find('input.checkbox').on('change', function() {
                        var type = $(this).data('type');
                        var id = parseInt($(this).data('id'), 10);

                        switch(type) {
                            case 'directory':
                                $scope.selectedDirectories[id] = $(this).is(':checked');
                                $scope.$apply();
                                break;

                            case 'bookmark':
                                $scope.selectedBookmarks[id] = $(this).is(':checked');
                                $scope.$apply();
                                break;
                        }
                    });
                }
            },

            controller: ['$scope', '$element', function($scope, $element) {
                /**
                 * У нас есть хоть один выбранный?
                 */
                $scope.hasSelected = function() {
                    for (var i in $scope.selectedDirectories) {
                        if ($scope.selectedDirectories.hasOwnProperty(i)) {
                            if (!!$scope.selectedDirectories[i]) {
                                return true;
                            }
                        }
                    }

                    for (var i in $scope.selectedBookmarks) {
                        if ($scope.selectedBookmarks.hasOwnProperty(i)) {
                            if (!!$scope.selectedBookmarks[i]) {
                                return true;
                            }
                        }
                    }

                    return false;
                };

                /**
                 * Удаление выбранных элементов
                 */
                $scope.removeSelected = function() {
                    if (!confirm('Вы уверены?')) {
                        return;
                    }

                    var directories = [];
                    var bookmarks = [];
                    var $elementsToRemove = $();

                    for (var i in $scope.selectedDirectories) {
                        if ($scope.selectedDirectories.hasOwnProperty(i)) {
                            if (!!$scope.selectedDirectories[i]) {
                                directories.push(i);
                                $elementsToRemove = $elementsToRemove.add($element.find('*[data-directory-element="'+i+'"]'));
                            }
                        }
                    }

                    for (var i in $scope.selectedBookmarks) {
                        if ($scope.selectedBookmarks.hasOwnProperty(i)) {
                            if (!!$scope.selectedBookmarks[i]) {
                                bookmarks.push(i);
                                $elementsToRemove = $elementsToRemove.add($element.find('*[data-document-element="'+i+'"]'));
                            }
                        }
                    }

                    if ((directories.length == 0) && (bookmarks.length == 0)) {
                        return;
                    }

                    $elementsToRemove.addClass('prepare-to-remove');

                    $http({
                        method: 'DELETE',
                        url: $scope.urlRemove,
                        data:{directories:directories, bookmarks: bookmarks}
                    }).success(function() {
                        notifications.user.info('Элементы удалены');

                        $scope.selectedDirectories = {};
                        $scope.selectedBookmarks = {};
                        $elementsToRemove.remove();
                    }).error(function() {
                        notifications.user.error('Произошла ошибка');
                        $elementsToRemove.removeClass('prepare-to-remove');
                    });
                };
            }]
        }
    }])

    /**
     * Сворачиваемая папка с закладками
     */
    .directive('userBookmarksItem', [function() {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element) {
                $scope.open = $element.hasClass('open');

                $scope.$watch('open', function() {
                    if ($scope.open) {
                        $element.addClass('open').removeClass('close');
                    } else {
                        $element.removeClass('open').addClass('close');
                    }
                });

                $element.find('.directory .icon').on('click', function(e) {
                    e.preventDefault();

                    $scope.open = !$scope.open;
                    $scope.$apply();
                });

                $element.find('.directory-name .real-name').on('click', function(e) {
                    e.preventDefault();

                    if ($(e.target).is('i')) {
                        return;
                    }

                    $scope.open = !$scope.open;
                    $scope.$apply();
                });
            }
        }
    }])

    /**
     * Редактирование имен папок и закладок
     */
    .directive('userBookmarksEditable', ['$http', 'notifications', function($http, notifications) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                $element.find('.real-name')
                    .attr('ng-show', 'mode=="real"')
                    .children('i')
                        .attr('ng-click', 'setEditMode()');

                $element.find('.edit-name')
                    .attr('ng-show', 'mode=="edit"')
                    .attr('ng-enter', 'saveTitle()')
                    .attr('ng-esc', 'discard()')
                    .children('i')
                        .attr('ng-click', 'saveTitle()');

                return function($scope, $element, attrs) {
                    $scope.mode = 'real';
                    $scope.title = attrs.title;
                    $scope.url = attrs.url;

                    $element.find('.real-name a').off('click');
                }
            },

            controller: ['$scope', '$element', function($scope, $element) {
                /**
                 * Количество документов
                 */
                $scope.count = function() {
                    return $element.next('.bookmarks-list').find('.bookmark').length;
                };

                /**
                 * Включить режим редактирования
                 */
                $scope.setEditMode = function() {
                    $scope.title_edit = $scope.title;
                    $scope.mode = 'edit';

                    setTimeout(function() {
                        $element.find('.edit-name input, .edit-name textarea').focus();
                    });
                };

                /**
                 * Сохранить изменение
                 */
                $scope.saveTitle = function() {
                    $scope.title = $scope.title_edit;
                    $scope.mode = 'real';

                    $http.post($scope.url, {title:$scope.title})
                        .success(function(response) {
                            if (response.title) {
                                $scope.title = response.title;
                            } else {
                                notifications.user.error(response.message || 'Произошла ошибка');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.message || 'Произошла ошибка');
                        });
                };

                /**
                 * Отменить редактирование
                 */
                $scope.discard = function() {
                    $scope.mode = 'real';
                };
            }]
        }
    }])

    /**
     * Панель добавления закладок
     */
    .directive('userBookmarksAdd', ['notifications', 'udSelection', '$http', 'router', function(notifications, udSelection, $http, router) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element) {
                $scope.bookmark = {
                    title: document.title || '',
                    url: (window.location.pathname || window.location.href)
                };
                $scope.directory = {};
                $scope.loading = false;

                $scope.bookmark.title = $scope.bookmark.title.split(' :: ')[0];
            },

            controller: ['$scope', function($scope) {
                /**
                 * Сохраняем
                 */
                $scope.saveAll = function() {
                    if (!$scope.bookmark.title) {
                        notifications.user.error('Нужно указать имя закладки');
                        return;
                    }

                    var url = router.generate('krd_user_bookmarks_bookmark_create');

                    $scope.loading = true;

                    $http.post(url, {title:$scope.bookmark.title, url:$scope.bookmark.url, directory:$scope.directory.id})
                        .success(function(response) {
                            $scope.loading = false;
                            $scope.$emit('dialog-hide');
                            notifications.user.info('Закладка успешно создана');
                        })
                        .error(function() {
                            notifications.user.error('Произошла ошибка при сохранении');
                            $scope.loading = false;
                        });
                };
            }]
        }
    }])

    /**
     * Дерево закладок
     */
    .directive('userBookmarksTree', ['router', 'notifications', '$http',function(router, notifications, $http) {
        return {
            restrict: 'A',
            scope: true,
            template: '<div class="wrap">'
                        + '<div class="text-block" ng-if="!directories || directories.length == 0">Нет ни одной папки. <a href="" ng-click="newDirectory()">Создать папку?</a></div>'
                        + '<div class="directory" ng-repeat="directory in directories" ng-class="{selected:directory.$selected}">'
                            + '<i class="icon-lk-document-tree-directory" ng-class="{editmode:directory.$editmode}"></i>'
                            + '<div class="real-name" ng-click="selectDirectory(directory)" ng-if="!directory.$editmode">{{ directory.title }}</div>'
                            + '<div class="edit-name" ng-if="directory.$editmode">'
                                + '<input type="text" ng-model="directory.title" ng-enter="submitEditDirectory(directory)" ng-esc="removeDirectory(directory)" ng-blur="submitEditDirectory(directory)" maxlength="80">'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            link: function($scope, $element) {
                $scope.directories = [];
                $scope.update();
            },

            controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
                /**
                 * Устанавливает модель директории
                 */
                $scope.setDirectoryModel = function(directory) {
                    $scope.$parent[$attrs.modelDirectory] = directory;
                };

                /**
                 * Обновление списка
                 */
                $scope.update = function() {
                    $http.get(router.generate('krd_user_bookmarks_list'))
                        .success(function(response) {
                            $scope.directories = response;

                            if (!!$scope.directories[0]) {
                                $scope.selectDirectory($scope.directories[0]);
                            }
                        })
                        .error(function() {
                            notifications.user.error('Не удалось загрузить список закладок');
                        });
                };

                /**
                 * Выбор директории
                 */
                $scope.selectDirectory = function(directory) {
                    $scope.deselectAll();

                    directory.$selected = true;
                    $scope.setDirectoryModel(directory);
                };

                /**
                 * Снимаем все выделения
                 */
                $scope.deselectAll = function() {
                    $scope.setDirectoryModel(null);

                    for (var i = 0, l = $scope.directories.length; i < l; i++) {
                        var directory = $scope.directories[i];
                        directory.$selected = false;
                    }
                };

                /**
                 * Новая папка
                 */
                $scope.newDirectory = function() {
                    $scope.deselectAll();

                    $scope.directories.push({
                        title: 'Новая папка',
                        documents: [],
                        $editmode: true
                    });

                    setTimeout(function() {
                        $element.find('input:visible').focus();
                    }, 13);
                };

                /**
                 * Удаление директории
                 */
                $scope.removeDirectory = function(directory) {
                    var index = $scope.directories.indexOf(directory);

                    if (index !== -1) {
                        $scope.directories.splice(index, index);
                    }
                };

                /**
                 * Подтверждение редактирования директории
                 */
                $scope.submitEditDirectory = function(directory) {
                    if (directory.$saveInProgress) {
                        return;
                    }

                    directory.$editmode = false;
                    directory.$saveInProgress = true;
                    $scope.selectDirectory(directory);

                    $http.post(router.generate('krd_user_bookmarks_directory_create'), {title:directory.title})
                        .success(function(response) {
                            directory.id = response.id;
                            directory.title = response.title;
                            directory.$saveInProgress = false;
                        })
                        .error(function() {
                            notifications.user.error('Произошла ошибка');
                            $scope.removeDirectory(directory);
                        });
                };
            }]
        }
    }])

    /**
     * Панель управления деревом закладок
     */
    .directive('userBookmarksTreeControl', [function() {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                $scope.$treeScope = $('*[user-bookmarks-tree="'+attrs.userBookmarksTreeControl+'"]').scope();
            },

            controller: ['$scope', function($scope) {
                $scope.newDirectory = function() {
                    $scope.$treeScope.newDirectory();
                };
            }]
        }
    }])

    /**
     * Сворачиваемая помощь в закладках
     */
    .directive('bookmarkHelpBottomSlide', [function() {
        return {
            restrict: 'C',
            scope: true,
            controller: ['$scope', '$element', function($scope, $element) {
                var $slide = $element.children('.slide');
                var $btnGroup = $element.children('.btn-group');

                $scope.open = function() {
                    $slide.slideDown();
                    $btnGroup.slideUp();
                };

                $scope.close = function() {
                    $slide.slideUp();
                    $btnGroup.slideDown();
                };
            }]
        }
    }])
;
