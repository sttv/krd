$(document).ready(function(){
	// ������������� ���������� ������
	window.socialMapSearch = new SMSearchApp();

	// ��������� ��������� ������
	try{
		window.socialMapSearch.appendCategory(__categoriesList);
	}catch(e){};

	// ��������� ������� ������
	try{
		window.socialMapSearch.appendRegion(__regionsList);
	}catch(e){};

	// ���������� ����������
	window.socialMapSearch.on('initialized', function(){
		try{
			var __loading = setInterval(function() {
                if (__placesList.length == 0) {
                    clearInterval(__loading);
                    window.socialMapSearch.placesLoaded = true;

                    var $details = $('.sm-place-details[data-id]');

                    if ($details.length > 0) {
                        window.socialMapSearch.onePlace($details.data('id'));
                    }

                    Backbone.history.start();

                    window.socialMapSearch.trigger('full-init');
                } else {
                    var places = [];
                    for(var i = 0; i < 500; i++) {
                        if (__placesList.length > 0) {
                            places.push(__placesList.pop());
                        }
                    }

                    window.socialMapSearch.appendPlace(places);
                }
            }, 1);
		}catch(e){};
	});

	// ������ �������
	var $gallery = $('.sm-detail-gallery');
	if ($gallery.length > 0){
		fototape($gallery);
	}

	// �����������
	var $rating = $('.sm-place-details .place-rating');
	if ($rating.length > 0){
		new starsView({el:$rating});
	}
});

/**
 * ������� ��������� ������ ������
 */
function __parseYP(str){
	var matches = str.match(/GeoPoint\(([0-9\.]+), ([0-9\.]+)\)/g);

	var arr = [];

	_.each(matches, function(item){
		var sm = item.match(/([0-9\.]+), ([0-9\.]+)/g);

		sm = sm[0].split(',');
		var tmp = sm[0];
		sm[0] = sm[1];
		sm[1] = tmp;

		sm[0] = $.trim(sm[0]);
		sm[1] = $.trim(sm[1]);

		arr.push(sm);
	});


	_.each(arr, function(v, k){
		arr[k] = v[0]+'|'+v[1];
	});


	return arr.join('+');
}
