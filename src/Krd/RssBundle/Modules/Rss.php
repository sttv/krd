<?php

namespace Krd\RssBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль RSS
 */
class Rss extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdRssBundle:Admin:Module/rss.html.twig');
    }

    /**
     * Список лент
     */
    public function renderContent()
    {
        $items = $this->getRepository()
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.parent = :parent')
            ->orderBy('e.sort', 'ASC')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        return $this->twig->render('KrdRssBundle:Module:rss/list.html.twig', array('list' => $items));
    }
}
