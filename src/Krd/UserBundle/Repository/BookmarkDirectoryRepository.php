<?php

namespace Krd\UserBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Symfony\Component\Security\Core\SecurityContext;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\Entity\BookmarkDirectory;


/**
 * Репозиторий для папок пользовательских закладок
 */
class BookmarkDirectoryRepository extends EntityRepository
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @JMSDI\InjectParams({
     *     "securityContext" = @JMSDI\Inject("security.context")
     * })
     */
    public function setUserBySecurityContext(SecurityContext $securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * Создание новой директории
     * @param  array $data
     * @return BookmarkDirectory|false
     */
    public function createMyDirectory($data)
    {
        if ($this->user instanceof User) {
            $directory = new BookmarkDirectory();

            if (isset($data['title']) && !empty($data['title'])) {
                $title = strip_tags($data['title']);
                $directory->setTitle($title);

                $this->_em->persist($directory);
                $this->_em->flush($directory);

                return $directory;
            }
        } else {
            return false;
        }
    }

    /**
     * Получение директории пользователя
     * @param  integer $id
     * @return BookmarkDirectory|Null
     */
    public function getMyDirectory($id)
    {
        if ($this->user instanceof User) {
            return $this->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId()));
        }
    }

    /**
     * Список директорий текущего пользователя
     * @return array
     */
    public function getMyDirectories()
    {
        if ($this->user instanceof User) {
            return $this->createQueryBuilder('e')
                ->andWhere('e.createdBy = :user')
                ->addOrderBy('e.title', 'ASC')
                ->addOrderBy('e.created', 'DESC')
                ->setParameter('user', $this->user->getId())
                ->getQuery()
                    ->useResultCache(true, 5)
                    ->getResult();
        } else {
            return array();
        }
    }

    /**
     * Редактирование параметров директории текущего пользователя
     * @param  integer $id
     * @param  array $data
     * @return BookmarkDirectory|false
     */
    public function editMyDirectory($id, $data)
    {
        if ($this->user instanceof User) {
            $directory = $this->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId()));

            if ($directory) {
                if (isset($data['title']) && !empty($data['title'])) {
                    $title = strip_tags($data['title']);
                    $directory->setTitle($title);
                }

                $this->_em->flush($directory);
                return $directory;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Удаление директории текущего пользователя по ID
     * @param  integer $id
     * @return boolean
     */
    public function removeMyDirectory($id)
    {
        if ($this->user instanceof User) {
            $directory = $this->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId()));

            if ($directory) {
                $this->_em->remove($directory);
                $this->_em->flush($directory);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
