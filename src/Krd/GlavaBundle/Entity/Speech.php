<?php

namespace Krd\GlavaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\Video as VideoEx;


/**
 * Видео
 * 
 * @ORM\Entity
 * @ORM\Table(name="glava_speech",
 *      indexes={
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 * 
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 * 
 * @Form\Fields(
 * 	   parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Speech implements NodeUrlIntf {
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;
	
	 /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;
	
	/**
     * Название
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;


    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;
	
	/**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;
	
	/**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';
	
	/**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;
	
	 /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';
	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $refid;
	
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setParent($parent)
	{
		$this->parent = $parent;
	}
	
	public function getParent()
	{
		return $this->parent;
	}
	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setDate(\DateTime $date)
	{
		$this->date = $date;
	}
	
	public function getDate()
	{
		return $this->date;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setRefId($id) {
		$this->refid = $id;
	}
	
	public function getRefId()
	{
		return $this->refid;
	}
	
	public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    } 
	
    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
	
	public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }
	
	public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
	
	public function getDetailRoute()
    {
        return 'krd_glava_speech_detail';
    }
}