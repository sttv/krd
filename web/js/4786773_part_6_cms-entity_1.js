angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'cms.table.filters', 'cms.tinymce'])
    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', '$rootScope', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache, $rootScope) {

        function quoteattr(s, preserveCR) {
            preserveCR = preserveCR ? '&#13;' : '\n';

            return ('' + s)
                .replace(/&/g, '&amp;')
                .replace(/'/g, '&apos;')
                .replace(/"/g, '&quot;')
                // .replace(/</g, '&lt;')
                // .replace(/>/g, '&gt;')
                // .replace(/\r\n/g, preserveCR)
                // .replace(/[\r\n]/g, preserveCR)
                ;
        }

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var template = '';
                var columns = $parse(tAttrs.columns)();

                template += '<div class="header-buttons" ng-show="headerButtons.length > 0 || hasSelectedItems()">';
                template += '<a class="btn btn-{{ i.type }}" href="#!{{ i.url }}" ng-repeat="i in headerButtons">';
                template += '<i class="icon-{{ i.icon }} icon-white"></i>';
                template += '{{ i.title }}</a>';
                template += '<button class="btn btn-danger" ng-show="hasSelectedItems()" ng-click="removeSelectedEntities()"><i class="icon-trash icon-white"></i> Удалить выбранные ({{ selectedCount() }} шт.)</button>'
                template += '</div>';

                template += '<div ng-show="itemList.length > 0">';
                template += '<table ng-table="tableParams" class="table table-hover table-striped">';
                template += '<tr ng-repeat="item in itemList" ng-class="{selected:selectedItems[item.id] === true}">';

                // Выделение нескольких элементов
                template += '<td data-title=" " width="20">';
                template += '<label class="checkbox"><input type="checkbox" ng-model="selectedItems[item.id]" /></label>';
                template += '</td>';

                // Сортировка
                if (tAttrs.sortable !== undefined) {
                    template += '<td data-title=" " width="40" class="sortable-td">';
                    template += '<i class="icon-chevron-up" tooltip="Поднять" ng-click="sortUp(item)" ng-class="{hidden:isFirstItem(item)}"></i>';
                    template += '&nbsp;';
                    template += '<i class="icon-chevron-down" tooltip="Опустить" ng-click="sortDown(item)" ng-class="{hidden:isLastItem(item)}"></i>';
                    template += '</td>';
                }

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\'';
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html-unsafe="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label>';
                    }

                    template += '</td>';
                }

                // Кнопки действий
                template += '<td data-title="Действия" width="200" class="actions-td">';
                template += '<a class="btn btn-{{ i.type }} btn-small" tooltip="{{ i.tooltip }}" href="#!{{ i.url|replace:\'{id}\':$parent.item.id }}" ng-repeat="i in actionButtons">';
                template += '<i class="icon-white icon-{{ i.icon }}"></i>';
                template += '</a>';
                template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                template += '</td>';

                template += '</tr></table></div>';

                template += '<div class="well well-small" ng-show="itemList.length == 0 && !loading">Список пуст</div>';

                template = '<div loading-container="loading">' + template + '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                /**
                 * Сортировка элемента вверх
                 */
                $scope.sortUp = function(item) {
                    $scope.sortItem(item, 'up');
                };

                /**
                 * Сортировка элемента вниз
                 */
                $scope.sortDown = function(item) {
                    $scope.sortItem(item, 'down');
                };

                /**
                 * Сортировка
                 */
                $scope.sortItem = function(item, order) {
                    var url = $attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    url = url.replace(':id', 'sort/' + order);

                    $scope.loading = true;
                    $http({
                        method: 'POST',
                        url: url,
                        data: {id: item.id}
                    }).success(function(response) {
                        if (response.success) {
                            $scope.update();
                            if (!response.moved) {
                                $rootScope.$broadcast('ntf-warning', 'При сортировке ничего не изменилось. ', 5000);
                            }
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке. ', 5000);
                            $scope.loading = false;
                        }
                    }).error(function() {
                        $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке. ', 5000);
                        $scope.loading = false;
                    });
                };

                /**
                 * Является ли элемент первым в списке
                 * @return {Boolean}
                 */
                $scope.isFirstItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == 1 && angular.equals(item, $scope.itemList[0]);
                };

                /**
                 * Является ли элемент последним в списке
                 * @return {Boolean}
                 */
                $scope.isLastItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == $scope.pagesCount && angular.equals(item, $scope.itemList[$scope.itemList.length - 1]);
                };

                /**
                 * Выполнить каллбек дял каждого выбранного элемента
                 * @param  {Function} callback Каллбек. Первый параметр ID элемента
                 */
                $scope.onEachSelected = function(callback) {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            callback(i);
                        }
                    }
                };

                // Проверяем, если ли выделенные элементы
                $scope.hasSelectedItems = function() {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            return true;
                        }
                    }

                    return false;
                };

                /**
                 * Количество выбранных элементов
                 */
                $scope.selectedCount = function() {
                    var count = 0;

                    $scope.onEachSelected(function() {
                        count++;
                    });

                    return count;
                };

                /**
                 * Сброс выбранных элементов
                 */
                $scope.resetSelected = function() {
                    $scope.selectedItems = {};
                };

                /**
                 * Обновление списка
                 */
                $scope.update = function() {
                    $scope.loading = true;

                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        $scope.loading = false;
                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                        $scope.pagesCount = data.page_count;
                        $scope.totalItems = data.items_count;
                        $scope.resetSelected();

                        if (!!$attrs.onUpdate) {
                            $rootScope.$broadcast($attrs.onUpdate);
                        }
                    }, function(response, errorData){
                        if (response.status == 0) {
                            // Загрузка была отменена
                            return;
                        }

                        console.log(response);
                        $scope.loading = false;
                        $scope.itemList = [];
                        $scope.tableParams.total = 0;

                        var msg = '';

                        try {
                            msg = response.data[0].message;
                        } catch(e) {};

                        $rootScope.$broadcast('ntf-error', response.status + ' - Произошла ошибка при загрузке списка. ' + msg, 5000);
                    });
                };

                /**
                 * Есть ли в списке столбцы с логическими значениями?
                 * @return {Boolean}
                 */
                $scope.hasBooleanColumns = function() {
                    for(var i in $scope.columns) {
                        if (!$scope.columns.hasOwnProperty(i)) {
                            continue;
                        }

                        if ($scope.columns[i].isBoolean === true) {
                            return true;
                            break;
                        }
                    }

                    return false;
                };

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.loading = true;

                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function() {
                                alert('Произошла ошибка при удалении');
                                $scope.$broadcast('needUpdate');
                            });
                        }
                    });
                };

                /**
                 * Удаление выбранных сущностей
                 */
                $scope.removeSelectedEntities = function() {
                    if ($scope.selectedCount() == 0) {
                        return;
                    }

                    var title = 'Удаление элементов';
                    var msg = 'Вы уверены, что хотите удалить выбранные элементы ('+$scope.selectedCount()+' шт.)?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    var removed = 0;

                    var afterDelete = function() {
                        if (++removed == $scope.selectedCount()) {
                            $scope.$broadcast('needUpdate');
                            $scope.resetSelected();
                        }
                    };

                    $dialog.messageBox(title, msg, btns)
                        .open()
                        .then(function(result) {
                            if (result == 'ok') {

                                $scope.loading = true;
                                $scope.onEachSelected(function(id) {
                                    $scope.entityResource.remove({id:id}, afterDelete, function() {
                                        alert('Произошла ошибка');
                                        afterDelete();
                                    });
                                });
                            }
                    });
                };
            },

            compile: function($element, $attrs) {
                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    $scope.pagesCount = 0;
                    $scope.totalItems = 0;

                    // Кнопки в заголовке таблицы
                    $scope.headerButtons = [];

                    if (!!attrs.btnAdd) {
                        $scope.headerButtons.push({
                            type: 'success',
                            url: (attrs.url + '/').replace('//', '/') + 'create/',
                            icon: 'plus',
                            title: attrs.btnAdd
                        });
                    }

                    // Кнопки действий в конце строки каждой таблицы
                    $scope.actionButtons = [];

                    if (!!attrs.actions) {
                        var actions = $parse(attrs.actions)($scope);
                        for(var i = 0; i < actions.length; i++) {
                            $scope.actionButtons.push(actions[i]);
                        }
                    }

                    $scope.actionButtons.push({
                        type: 'warning',
                        url: (attrs.url + '/').replace('//', '/') + 'edit/{id}',
                        icon: 'edit',
                        tooltip: 'Редактировать'
                    });

                    // Список колонок
                    $scope.columns = $parse(attrs.columns)($scope);

                    // параметры аякс-таблицы
                    $scope.tableParams = new ngTableParams({
                        page: 1,
                        total: 0,
                        count: parseInt(attrs.count, 10)
                    });

                    // Список элементов
                    $scope.itemList = [];

                    // Список выделенных элементов
                    $scope.selectedItems = {};

                    // Создаем ресурс
                    var url = attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    $scope.entityResource = $resource(url, {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'}
                        });

                    // Наблюдаем за параметрами таблицы
                    $scope.$watch('tableParams', $scope.update, true);
                    $scope.$on('needUpdate', $scope.update, true);

                    // Событие успешного добавления/редактировния элемента
                    $scope.$on('entityFormClosed', function() {
                        $scope.$broadcast('needUpdate');
                    });

                    // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                    if ($scope.hasBooleanColumns()) {
                        $scope.$watch('itemList', function(itemList, oldList) {
                            for(var i = 0; i < oldList.length; i++) {
                                if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                                    continue;
                                }

                                if (!angular.equals(itemList[i], oldList[i])) {
                                    var url = attrs.entityList.replace(/:id/, 'set/' + itemList[i].id);

                                    $http.post(url, itemList[i])
                                        .success(function(response) {
                                            $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                        })
                                        .error(function(response) {
                                            console.log(response);
                                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                                        });
                                }
                            }
                        }, true);
                    }
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                $scope.$emit('entityFormClosed');
                            } else {
                                $rootScope.$broadcast('ntf-error', 'Форма содержит ошибки заполнения', 5000);
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            console.log(response);
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка');
                        });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"], input[type="email"], input[type="password"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"], input[type="email"], input[type="password"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - список текстовых полей
     */
    .directive('formCollectionText', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            controller: function($scope, $element, $attrs) {
                var $controls = $element.find('.controls');
                var prototype = $attrs.prototype + removeButtonTpl;

                $scope.nextIndex = $controls.find('input').length + 1;

                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var input = prototype.replace(/__name__/g, $scope.nextIndex);
                    $controls.children().last().before($compile(input)($scope));

                    $scope.nextIndex = $controls.children('input').length;
                    $scope.updateNames();
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.target);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.add($removeButton.prev('input')).remove();
                    $scope.updateNames();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var $fInput = $controls.children('input').first();
                    if ($fInput.length == 0) {
                        return;
                    }

                    var name = $fInput.attr('name').replace(/[0-9\[\]]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', name+'_'+i);

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });

                        $compile($this)($scope);

                        i++;
                    });
                };
            },

            compile: function($element, $attrs) {
                if (!$attrs.prototype) {
                    $element.find('.controls').html('<span class="label label-important">Empty prototype</span>');
                }

                $element.find('.controls input').after(removeButtonTpl);
                $element.find('.btn-add').attr('ng-click', 'addElement()');

                return function($scope) {
                    $scope.updateNames();
                }
            }
        }
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })
;
