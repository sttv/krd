<?php

namespace Krd\CityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Список парков
 */
class ParkController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * Детальная страница
     *
     * @Route("/{name}.html", name="krd_city_park_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.city.modules.park')->getCurrent()) {
            if ($item->hasRedirect()) {
                return $this->redirect($item->getRedirect());
            }

            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
