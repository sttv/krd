<?php

namespace Krd\ApiBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityManager;
use Krd\ApiBundle\Entity\GCMToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


/**
 * Google Cloud Messages
 *
 * @Route("/gcm")
 */
class GcmController extends Controller
{
    /**
     * Регистрация нового токена
     *
     * @Route("/register_token")
     * @Method({"POST"})
     */
    public function registerTokenAction(Request $request)
    {
        $token = $request->get('token');

        if (!$token) {
            exit;
        }

        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var GCMToken $gcmToken */
        $gcmToken = $em->getRepository('KrdApiBundle:GCMToken')->findOneBy(array('token' => $token));

        if (!$gcmToken) {
            $gcmToken = new GCMToken();
            $gcmToken->setToken($token);
            $em->persist($gcmToken);
        }

        $gcmToken->setUpdated(new DateTime());
        $em->flush($gcmToken);

        exit;
    }

    /**
     * Тестовая отправка срочного сообщения
     *
     * @Route("/test_emergency_news")
     */
    public function testEmerNewsAction(Request $request)
    {
        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        $emer = $em->getRepository('KrdNewsBundle:Emergency')->find(231);

        $this->get('notifications.gcm.gcmmanager')->sendToAll(array(
            'action' => 'emergency',
            'id' => $emer->getId(),
            'title' => $emer->getTitle(),
            'message' => $emer->getMessage(),
            'time' => $emer->getDate()->format('H:i'),
        ));

        die('finish');
    }
}
