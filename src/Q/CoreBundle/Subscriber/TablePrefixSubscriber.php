<?php

namespace Q\CoreBundle\Subscriber;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

/**
 * Реализация префиксов таблиц
 */
class TablePrefixSubscriber implements \Doctrine\Common\EventSubscriber
{
    protected $prefix = '';
    protected $commonPrefix = 'krdru13__';

    public function __construct($prefix)
    {
        $this->prefix = (string) $prefix;
    }

    public function getSubscribedEvents()
    {
        return array('loadClassMetadata');
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $classMetadata = $args->getClassMetadata();

        switch ($classMetadata->getTableName()) {
            case 'user':
            case 'group':
            case 'user_group':
                $classMetadata->setTableName($this->commonPrefix.$classMetadata->getTableName());
                break;

            default:
                $classMetadata->setTableName($this->prefix.$classMetadata->getTableName());
                break;
        }

        foreach ($classMetadata->getAssociationMappings() as $fieldName => $mapping) {
            if ($mapping['type'] == \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_MANY && isset($classMetadata->associationMappings[$fieldName]['joinTable']['name'])) {
                $mappedTableName = $classMetadata->associationMappings[$fieldName]['joinTable']['name'];

                switch ($mappedTableName) {
                    case 'user_group_relation':
                        $classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->commonPrefix.$mappedTableName;
                        break;

                    default:
                        $classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->prefix.$mappedTableName;
                        break;
                }
            }
        }
    }

}
