<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\File;
use Q\FormMetadataBundle\Configuration as Form;


/**
 * Контент страницы
 *
 * @ORM\Entity(repositoryClass="Q\CoreBundle\Repository\ContentRepository")
 * @ORM\Table(name="content",
 *      indexes={
 *          @ORM\Index(name="name", columns={"name"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     created={"datetime", {
 *         "label"="Дата размещения",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     contentMobile={"textarea", {
 *         "label"="Содержание для мобильного приложения",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     files={"files", {
 *         "label"="Файлы"
 *     }},
 *     hidedate={"checkbox", {
 *         "label"="Не показывать дату размещения",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Content
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Node", inversedBy="content")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     *
     * @Gedmo\Versioned
     */
    private $name;

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';

    /**
     * Содержание для мобильного приложения
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $contentMobile = '';

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="content_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="content_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $files;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="content_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="content_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * Не показывать дату размещения
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $hidedate = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y (H:i)'>")
     *
     * @Gedmo\Versioned
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y (H:i)'>")
     *
     * @Gedmo\Versioned
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getContentMobile()
    {
        return $this->contentMobile;
    }

    public function setContentMobile($contentMobile)
    {
        $this->contentMobile = $contentMobile;
    }

    public function getFiles()
    {
        return $this->files ?: $this->files = new ArrayCollection();
    }

    public function hasFiles()
    {
        return count($this->getFiles()) > 0;
    }

    public function addFiles(File $file)
    {
        if (!$this->getFiles()->contains($file)) {
            $this->getFiles()->add($file);
        }
    }

    public function removeFiles(File $file)
    {
        if ($this->getFiles()->contains($file)) {
            $this->getFiles()->removeElement($file);
        }
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function getHidedate()
    {
        return $this->hidedate;
    }

    public function setHidedate($hidedate)
    {
        $this->hidedate = (boolean)$hidedate;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created = null)
    {
        $this->created = $created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated = null)
    {
        $this->updated = $updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }


    public function __toString()
    {
        return $this->getContent();
    }
}


