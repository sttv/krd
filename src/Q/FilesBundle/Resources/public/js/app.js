/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader', ['cms.filesuploader.files', 'cms.filesuploader.images', 'cms.filesuploader.video']);
