<?php

namespace Krd\SubscribeBundle\Twig;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;

use Krd\SubscribeBundle\Entity\Subscriber;
use Krd\SubscribeBundle\Entity\RSubscriber;
use Q\UserBundle\Entity\User;


/**
 * Расширение Twig для вывода контентной формы подписки
 */
class SubscribeFormExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    public function setTwig(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function getFunctions()
    {
        return array(
            'krd_subscribe_form' => new \Twig_Function_Method($this, 'krd_subscribe_form', array('is_safe' => array('html'))),
            'krd_r_subscribe_form' => new \Twig_Function_Method($this, 'krd_r_subscribe_form', array('is_safe' => array('html'))),
        );
    }

    /**
     * Рендер формы подписки
     * @return string
     */
    public function krd_subscribe_form()
    {
        $subscriber = new Subscriber();
        $unsubscribe = false;

        if (($user = $this->securityContext->getToken()->getUser()) && ($user instanceof User)) {
            $subscriber->setEmail($user->getEmail());

            if ($this->em->getRepository('KrdSubscribeBundle:Subscriber')->findOneBy(array('active' => 1, 'email' => $user->getEmail()))) {
                $unsubscribe = true;
            }
        }

        $builder = $this->formFactory->createNamedBuilder('subscriber', 'subscriber', $subscriber);
        $builder->setAction($this->router->generate('krd_subscribe_form_submit'));

        return $this->twig->render('KrdSubscribeBundle:blocks:content-form.html.twig', array(
                'form' => $builder->getForm()->createView(),
                'form_id' => uniqid('subscribe-'),
                'unsubscribe' => $unsubscribe,
            ));
    }

    /**
     * Рендер формы подписки на репортера
     * @return string
     */
    public function krd_r_subscribe_form()
    {
        $subscriber = new RSubscriber();
        $unsubscribe = false;

        if (($user = $this->securityContext->getToken()->getUser()) && ($user instanceof User)) {
            $subscriber->setEmail($user->getEmail());

            if ($this->em->getRepository('KrdSubscribeBundle:RSubscriber')->findOneBy(array('active' => 1, 'email' => $user->getEmail()))) {
                $unsubscribe = true;
            }
        }

        $builder = $this->formFactory->createNamedBuilder('r_subscriber', 'r_subscriber', $subscriber);
        $builder->setAction($this->router->generate('krd_r_subscribe_form_submit'));

        return $this->twig->render('KrdSubscribeBundle:blocks:reporter-form.html.twig', array(
                'form' => $builder->getForm()->createView(),
                'form_id' => uniqid('rsubscribe-'),
                'unsubscribe' => $unsubscribe,
            ));
    }

    public function getName()
    {
        return 'krd_subscribe_form';
    }
}
