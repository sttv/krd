<?php

namespace Krd\DocumentBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Filesystem\Filesystem;

use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Krd\DocumentBundle\Entity\Document;


/**
 * Удаление всех документов в разделе
 */
class DocumentClearCommand extends ContainerAwareCommand
{
    protected $em;

    protected function configure()
    {
        $this
            ->setName('krd:document:clear')
            ->setDescription('Remove all documents in a node')
            ->addArgument('node_id', InputArgument::REQUIRED, 'Node id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $nodeId = $input->getArgument('node_id');

        $query = $this->em->createQuery("SELECT n FROM KrdDocumentBundle:Document n WHERE n.parent = :parent");
        $query->setParameter('parent', $nodeId);

        $this->em->getConnection()->beginTransaction();

        $i = 0;
        foreach ($query->iterate() as $newsItem) {
            $id = $newsItem[0]->getId();
            $this->em->remove($newsItem[0]);
            $this->em->flush();
            $i++;
            $output->writeln('Removed KrdDocumentBundle:Document with id '.$id);

            if ($i % 100 == 0) {
                $this->em->getConnection()->commit();
                $output->writeln('Commit!');
                $this->em->getConnection()->beginTransaction();
            }
        }

        $this->em->getConnection()->commit();
    }
}
