<?php

namespace Q\CoreBundle\Twig;

use DateTime;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Q\CoreBundle\Navigation\Builder;
use Q\CoreBundle\Entity\Config;


/**
 * Расширение Twig
 */
class QCoreExtension extends \Twig_Extension
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Builder
     */
    protected $navigation;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Router
     */
    protected $router;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->navigation = $container->get('qcore.cms.navigation');
        $this->config = $container->get('qcore.config');
        $this->router = $container->get('router');
    }

    protected function getEntityManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    public function getGlobals()
    {
        return array(
            'navigation' => $this->navigation,
            'appConfig' => $this->config,
            'appContainer' => $this->container,
            'currentNode' => $this->container->get('qcore.routing.current')->getNode(),
        );
    }

    public function getFilters()
    {
        return array(
            'get_class' => new \Twig_Filter_Function(array($this, 'twig_get_class_filter')),
            'filesize' => new \Twig_Filter_Function(array($this, 'twig_filesize')),
            'declension' => new \Twig_Filter_Function(array($this, 'twig_declension')),
            'krd_date_format' => new \Twig_Filter_Function(array($this, 'krd_date_format')),
        );
    }

    public function getFunctions()
    {
        return array(
            'q_elfinder_tinymce_init4' => new \Twig_Function_Method($this, 'elfinder_tinymce_init4', array('is_safe' => array('html'))),
            'node_path' => new \Twig_Function_Method($this, 'node_path'),
            'controller_path' => new \Twig_Function_Method($this, 'controller_path'),
            'is_path_exists' => new \Twig_Function_Method($this, 'is_path_exists'),
        );
    }

    public function getName()
    {
        return 'qcore_export_globals';
    }

    /**
     * Выводит дату в русском формате
     * @param DateTime $dt
     * @return string
     */
    public static function krd_date_format(DateTime $dt, $includeTime = false)
    {
        if (is_null($dt)) {
            return '';
        }

        if ($includeTime) {
            $format = 'd F Y, H:i';
        } else {
            $format = 'd F Y';
        }

        if (isset($_SERVER['HTTP_HOST']) && ($_SERVER['HTTP_HOST'] == 'en.krd.ru')) {
            $months = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            );
        } else {
            $months = array(
                'января',
                'февраля',
                'марта',
                'апреля',
                'мая',
                'июня',
                'июля',
                'августа',
                'сентября',
                'октября',
                'ноября',
                'декабря'
            );
        }

        $format = str_replace('F', '__^^^__', $format);
        $format = $dt->format($format);

        if (mb_strpos($format, '__^^^__') !== false) {
            $month = (int)$dt->format('m') - 1;

            if (isset($months[$month])) {
                $format = str_replace('__^^^__', $months[$month], $format);
            }
        }

        return $format;
    }

    /**
     * Проверка существования роута
     * @param  string $name
     * @return boolean
     */
    public function is_path_exists($name)
    {
        return (null === $this->router->getRouteCollection()->get($name)) ? false : true;
    }

    /**
     * Генерация ссылки на раздел сайта по его ID
     * @param  integer $id ID Node
     * @return string|#
     */
    public function node_path($id)
    {
        $node = $this->getEntityManager()->getRepository('QCoreBundle:Node')->find($id);

        if ($node) {
            return $node->getUrl(true);
        } else {
            return '#';
        }
    }

    /**
     * Склонение слов
     * @param  integer $number
     * @param  array $forms
     * @param  boolean [optional] $implode
     * @return string
     */
    public function twig_declension($number, $forms, $implode = false)
    {
        $cases = array(2, 0, 1, 1, 1, 2);

        return ($implode ? $number . ' ' : '') . $forms[(($number % 100 > 4) && ($number % 100 < 20)) ? 2 : $cases[($number % 10 < 5) ? ($number % 10) : 5]];
    }

    /**
     * Генерация ссылки на раздел сайта по ID контроллера. Так же дописывает экшн
     * @param  integer $controllerId ID контроллера
     * @param  string $action Имя экшена
     * @param  array $parameters Параметры для экшена (если задан)
     * @return string|#
     */
    public function controller_path($controllerId, $action = null, $parameters = array())
    {
        return $this->container->get('qcore.routing.current')->generate($controllerId, $action, $parameters);
    }

    public function elfinder_tinymce_init4()
    {
        return $this->container->get('templating')->render('QCoreBundle:Elfinder:_tinymce4.html.twig');
    }

    public function twig_get_class_filter($object)
    {
        return get_class($object);
    }

    /**
     * Удобочитаемый размер файла
     * @param  integer $bytes
     * @return string
     */
    public function twig_filesize($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' Gb';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' Mb';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' Kb';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' bytes';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
