<?php

namespace Krd\NewsBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль новой информации
 */
class Info extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdNewsBundle:Admin:Module/info.html.twig');
    }

    /**
     * Получение текущего элемента
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Отображение виджета на главной странице
     */
    public function renderWidget()
    {
        $node = $this->em->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.controller = 23')
            ->setMaxResults(1)
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        if (!$node) {
            return '';
        }

        $node = $node[0];

        $list = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.parent = :parent')
            ->setParameter('parent', $node->getId())
            ->orderBy('e.date', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        if (!$list) {
            return '';
        }

        return $this->twig->render('KrdNewsBundle:Module:info/widget.html.twig', array('items' => $list, 'node' => $node));
    }

    /**
     * Детальная страница
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdNewsBundle:Module:info/detail.html.twig', array('item' => $this->getCurrent()));
    }

    /**
     * Список для текущей страницы
     * @return array
     */
    public function getItemsList($parent = null)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        if ($parent === null) {
            $queryBuilder->setParameter('parent', $this->node->getNode()->getId());
        } else {
            $queryBuilder->setParameter('parent', $parent);
        }

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true);
    }

    /**
     * Отображение списка
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdNewsBundle:Module:info/list.html.twig', array('list' => $result));
    }
}
