<?php

namespace Q\CoreBundle\Admin;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Navigation\FrontMenu;
use Q\CoreBundle\Entity\Node;


/**
 * Базовый класс для модуля
 */
abstract class AbstractModule
{
    protected $twig;
    protected $node;
    protected $em;
    protected $entityName;
    protected $menuManager;
    protected $request;

    public function __construct(\Twig_Environment $twig, CurrentNode $node, EntityManager $em, FrontMenu $menuManager, Request $request)
    {
        $this->twig = $twig;
        $this->node = $node;
        $this->em = $em;
        $this->menuManager = $menuManager;
        $this->request = $request;
    }

    /**
     * Устанавливает имя сущности модуля
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * Репозиторий сущности модуля
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityName);
    }

    /**
     * Событие удаление раздела связанного с модулем
     * По умолчанию удаляем все дочерние элементы
     * @param  Node   $node
     */
    public function onNodeRemove(Node $node)
    {
        foreach($this->getRepository()->findBy(array('parent' => $node->getId())) as $item) {
            $this->em->remove($item);
        }

        $this->em->flush();
    }

    /**
     * Получение EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager()
    {
        return $this->em;
    }

    /**
     * Генерация контента для административной части
     * @return string
     */
    abstract public function renderAdminContent();
}
