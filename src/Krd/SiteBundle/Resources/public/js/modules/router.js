/**
 * Роутер
 */
angular.module('router', [])
    .factory('router', [function() {
        var router = function() {
            var routes = window.__route || false;

            /**
             * Генерация ссылки
             */
            this.generate = function(route, params) {
                if (!!routes[route]) {
                    var url = routes[route];
                    params = params || {};

                    for (var from in params) {
                        if (params.hasOwnProperty(from)) {
                            var to = params[from];

                            while (url.indexOf(from) !== -1) {
                                url = url.replace(from, to);
                            }
                        }
                    }

                    return url;
                } else {
                    throw new Error('Route '+route+' not found');
                }
            };
        };

        return new router();
    }])
;
