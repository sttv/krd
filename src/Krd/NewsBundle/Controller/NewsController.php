<?php

namespace Krd\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Новости
 */
class NewsController extends Controller implements ActiveSecuredController
{
    /**
     * Список новостей
     *
     * @Route("/")
     * @Template()
     */
    public function listAction()
    {
		$result = array();
		$request = $this->get('request');
	
    	if( count($request->get('themes', array())) > 0 ) {
	    	$em = $this->getDoctrine()->getManager();
	    	$queryBuilder = $em->createQueryBuilder()
				->select('t')
				->from('KrdNewsBundle:NewsTheme', 't')
	            ->andWhere('t.id IN(:themes)')
	            ->setParameter('themes', $request->get('themes'));
				
			$result = $queryBuilder->getQuery()->getResult();
		}

		$ids = $request->get('themes',array());
		return array(
			'themes' => $result,
			'has_themes' => count($result) > 0,
			'themes_id' => array_combine($ids, $ids), 
		);
    }

    /**
     * Страница новости
     *
     * @Route("/{name}.html", name="krd_news_news_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.news.modules.news')->getCurrentNews()) {
            if ($item->hasRedirect()) {
                return $this->redirect($item->getRedirect());
            }

            if ($item->hasSeoTitle()) {
                $this->get('qcore.seo.manager')->setTitle($item->getSeoTitle());
            }

            if ($item->hasSeoKeywords()) {
                $this->get('qcore.seo.manager')->setKeywords($item->getSeoKeywords());
            }

            if ($item->hasSeoDescription()) {
                $this->get('qcore.seo.manager')->setDescription($item->getSeoDescription());
            }

            if ($item->hasImages()) {
                $this->get('qcore.seo.manager')->setImage($item->getMainImage());
            }


            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список новостей для постраничного вывода
     * @Route("/ajax/news/news/list.json", name="ajax_news_news_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        $queryBuilder = $this->get('krd.news.modules.news')->getRepository()
            ->createQueryBuilder('n')
//            ->leftJoin('n.images', 'i')
//            ->leftJoin('n.video', 'v')
            ->leftJoin('n.themes', 't')
            ->andWhere('n.active = 1')
            ->orderBy('n.date', 'DESC');

        if ($parent = $request->get('parent')) {
        	if( $parent == 7 ) {
        		$queryBuilder->andWhere('n.parent IN(:parent, 15188)');
        	} else
            	$queryBuilder->andWhere('n.parent = :parent');
            $queryBuilder->setParameter('parent', $parent);
        }

        if ($date = $request->get('date')) {
            $date = new \DateTime($date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') = :date');
            $queryBuilder->setParameter('date', $date->format('Y-m-d'));
        }
		
		if( count($request->get('themes', array())) > 0 ) {
			$queryBuilder->andWhere('t.id IN(:themes)')
						->setParameter('themes', $request->get('themes'));
		}
		
		if ($date = $request->get('date_begin')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') >= :date1');
            $queryBuilder->setParameter('date1', $date->format('Y-m-d'));
        }
		
		if ($date = $request->get('date_end')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') <= :date2');
            $queryBuilder->setParameter('date2', $date->format('Y-m-d'));
        }

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }
	
	/**
	 * Список тем по новостям
	 * @Route("/ajax/news/news/range.html", name="ajax_news_news_range", defaults={"_format"="html"})
	 * @Method({"GET"})
	 */
	public function rangeAction(Request $request) 
	{
		$dates = $this->get('krd.news.modules.news')->getRepository()->getDatesList($request->get('parent'));

        $date = \DateTime::createFromFormat('d-m-Y', date('Y-m-d'));
		
		
        return $this->render('KrdNewsBundle:Module:news/range.html.twig', array(
                'availDates' => $dates,
				'date_begin' => $request->get('date_begin'),
				'date_end' => $request->get('date_end'),
                'nowDate' => $date,
            ));
	}
	
	/**
	 * Список тем по новостям
	 * @Route("/ajax/news/news/themes.json", name="ajax_news_news_themes", defaults={"_format"="json"})
	 * @Method({"GET"})
	 */
	public function themesAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		$queryBuilder = $em->createQueryBuilder()
					->select('t')
					->from('KrdNewsBundle:NewsTheme', 't')
					->leftJoin('t.news', 'n')
					->andWhere('n.parent = :parentId')
					->setParameter('parentId', $request->get('parent'))
					->orderBy('t.title', 'ASC');

        $list = $queryBuilder->getQuery()->getResult();
        usort($list, function($a, $b) {
            return $a->getnewsCount() > $b->getNewsCount() ? -1 : 1;
        });

        return array(
            'page_count' => 1,
            'pages' => array(),
            'items_count' => 67,
            'items' => $list,
        );
		
		return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 1000000);
	}
}
