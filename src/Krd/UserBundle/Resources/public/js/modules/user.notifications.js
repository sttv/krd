/**
 * Всплывающие уведомления
 *
 * Для использования создать:
 *     <div class="notifications-container" data-channel="test"></div>
 *
 * Затем используя сервис notifications:
 *     notifications.test.info('Message');
 */
angular.module('user.notifications', [])
    /**
     * Контейнер для всплывающих сообщений
     */
    .directive('notificationsContainer', ['notifications', function(notifications) {
        return {
            restrict: 'C',
            scope: true,
            template: '<div ng-repeat="i in items" ng-class="i.type" class="item">{{ i.message }}</div>',
            link: function($scope, $element, attr) {
                $scope.items = [];

                if (!attr.channel) {
                    console.error('Не зада канал. Укажите атрибут channel');
                    $element.hide();
                    return;
                }

                notifications.createChannel(attr.channel, $scope.items);
            }
        }
    }])

    /**
     * Сервис для управления сообщениями
     */
    .factory('notifications', ['$timeout', function($timeout) {
        var that = {};

        that.createChannel = function(name, refArray) {
            if (that[name] !== undefined) {
                return that[name];
            }

            that[name] = {
                info: function(message, options) {return that.info(name, message, options);},
                warn: function(message, options) {return that.warn(name, message, options);},
                error: function(message, options) {return that.error(name, message, options);},
                __items: refArray
            };

            return that[name];
        };

        that.push = function(channel, type, message, options) {
            that[channel] = that[channel] || [];

            options = options || {};
            options.type = options.type || 'info';
            options.timeout = options.timeout || 4000;

            $timeout(function() {
                var msg = {
                    type: type,
                    message: message
                };

                that[channel].__items.push(msg);

                if (options.timeout > 0) {
                    $timeout(function() {
                        var index = that[channel].__items.indexOf(msg);
                        if (index != -1) {
                            that[channel].__items.splice(index, 1);
                        }
                    }, options.timeout);
                }
            });
        };

        that.info = function(channel, message, options) {
            return that.push(channel, 'info', message, options);
        };

        that.warn = function(channel, message, options) {
            return that.push(channel, 'warn', message, options);
        };

        that.error = function(channel, message, options) {
            return that.push(channel, 'error', message, options);
        };

        that.clear = function(channel) {
            if (!!that[channel]) {
                that[channel] = [];
            }
        }

        return that;
    }])
;
