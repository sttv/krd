<?
include_once 'conf.php';
$page = 6;

$auth = new auth();
if (!$auth->check() || $_SESSION['admin_user'] != 1){
	header('Location: index.php');
    exit;
}

switch($_REQUEST['action']) {
	case 'save':
        if(!empty($_REQUEST['htxt'])){
            //$f = fopen('./help.php', 'w');
            //fwrite($f, $_REQUEST['htxt']);
            //fclose($f);
			file_put_contents('./help.php', $_REQUEST['htxt'], LOCK_EX); 
            $_SESSION['error'] = '<label class="text-success">Файл помощи перезаписан.</label>';
        } else {
            $_SESSION['error'] = '<label class="text-error">Нельзя сохранить пустой файл.</label>';
			$err = 1;
        }
    default:
        if (!isset($_REQUEST['htxt'])) {
            if (file_exists('./help.php')) {
                $_REQUEST['htxt'] = file_get_contents('./help.php');
            } else {
                $_REQUEST['htxt'] = '';
                $_SESSION['error'] = '<label class="text-error">Файл помощи не существует.</label>';
				$err = 1;
            }
        }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
		<script src="./media/js/bootstrap.min.js"></script>
		<script src="./media/js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			$(function () {
				$('textarea[data-editor]').each(function () {
					var textarea = $(this);
					var mode = textarea.data('editor');
					var editDiv = $('<div>', {
						position: 'absolute',
						width: '100%',
						height: '600px',
					}).insertBefore(textarea);
					editDiv[0].style.fontSize='14px';
					textarea.css('visibility', 'hidden');
					
					var editor = ace.edit(editDiv[0]);
					editor.renderer.setShowGutter(true);
					editor.getSession().setValue(textarea.val());
					editor.getSession().setMode("ace/mode/" + mode);
					editor.setTheme("ace/theme/chrome");
					editor.focus();
					
					// copy back to textarea on form submit...
					textarea.closest('form').submit(function () {
						textarea.val(editor.getSession().getValue());
					})
				});
			});
		</script>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Редактирование файла помощи</h1>
					<ul class="nav nav-pills">
						<li><a href="./help.php" target="_blank">Открыть файл помощи в новой вкладке</a></li>
					</ul>
					<hr>
					<form method="post">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<div class="control-group<?=(isset($err)?' error':'')?>">
							<label for="title">HTML код:</label>
                            <textarea name="htxt" data-editor="html" style="display: none;"><?=@$_REQUEST['htxt']?></textarea>
						</div>
                        <div class="control-group">
							<input type="hidden" name="action" value="save" />
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary input-large">Сохранить</button>&nbsp;&nbsp;<a href="index.php" class="btn">Отмена</a>
						</div>
					</form>
				</div>
			</div>
		</div>
    </body>
</html>
