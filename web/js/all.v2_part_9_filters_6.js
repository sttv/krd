/**
 * Различные фильтры
 */
angular.module('filters', [])
    /**
     * Удаление времени из даты
     */
    .filter('dateRemoveTime', [function() {
        return function(value) {
            return value.toString().replace(/ \d{1,2}:\d{1,2}/, '');
        }
    }])
;
