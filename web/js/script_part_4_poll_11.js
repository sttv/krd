/**
 * Модуль опросов
 */
angular.module('polls', [])
    /**
     * Виджет отпросов
     */
    .directive('widgetPolls', [function() {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {

            }
        }
    }])
;
