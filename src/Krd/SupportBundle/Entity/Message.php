<?php

namespace Krd\SupportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\UserBundle\Entity\User;


/**
 * Сообщения
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Krd\SupportBundle\Repository\MessageRepository")
 * @ORM\Table(name="support_message",
 *      indexes={
 *          @ORM\Index(name="root", columns={"root"}),
 *          @ORM\Index(name="leftkey", columns={"`left`"}),
 *          @ORM\Index(name="rightkey", columns={"`right`"}),
 *          @ORM\Index(name="lvl", columns={"level"}),
 *          @ORM\Index(name="l_r", columns={"`left`", "`right`"}),
 *      }
 * )
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 */
class Message implements UrlIntf
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @JMS\Expose
     */
    private $id;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="`left`", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     * @JMS\Expose
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="`right`", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank(message="Нужно указать родителя", groups={"reply"})
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="parent")
     * @ORM\OrderBy({"created" = "ASC"})
     */
    private $children;

    /**
     * Тема сообщения
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     * @JMS\Expose
     * @Assert\NotBlank(message="Нужно выбрать тему сообщения", groups={"create"})
     */
    private $theme;

    /**
     * Заголовок сообщения
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     * @JMS\Expose
     * @Assert\NotBlank(message="Нужно указать заголовок", groups={"create"})
     */
    private $title;

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Пожалуйста, опишите вашу проблему", groups={"create", "reply"})
     * @JMS\Expose
     */
    private $content = '';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y (H:i)'>")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @JMS\Expose
     * @JMS\Accessor(getter="getUrl", setter="setUrl")
     */
    protected $url = '#';

    /**
     * @JMS\Expose
     * @JMS\Accessor(getter="getContentShort")
     */
    protected $contentShort = '';

    /**
     * Сообщение прочитано?
     * @var boolean
     *
     * @JMS\Expose
     */
    protected $readed = false;

    public function setReaded($readed)
    {
        $this->readed = (boolean)$readed;
    }

    public function getReaded()
    {
        return $this->readed;
    }

    public function getContentShort()
    {
        if (mb_strlen($this->getContent()) > 300) {
            return mb_substr($this->getContent(), 0 , 300).'...';
        } else {
            return $this->getContent();
        }
    }

    /**
     * Имя роута детального просмотра
     */
    public function getDetailRoute()
    {
        return 'krd_user_support_detail';
    }

    /**
     * Уникальное имя элемента
     */
    public function getName()
    {
        return $this->getId();
    }

    /**
     * ID контроллера страницы для генерации ссылки
     */
    public function getControllerId()
    {
        return 51;
    }

    /**
     * Установка URL
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Получение URL
     */
    public function getUrl()
    {
        if ($this->getLevel() == 0) {
            return $this->url;
        } else {
            $root = $this->getRootMessage();

            if ($root instanceof Message) {
                return $root->getUrl().'#message-'.$this->getId();
            }
        }
    }

    /**
     * Корневое сообщение
     * @return Message
     */
    public function getRootMessage()
    {
        if ($this->getLevel() == 0) {
            return $this;
        }

        $root = $this;
        $tries = 0;
        while (($root instanceof Message) && $root->getLevel() > 0 && $tries < 100) {
            $root = $root->getParent();
            $tries++;
        }

        return $root;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set left
     *
     * @param integer $left
     * @return Message
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return integer
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Message
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set right
     *
     * @param integer $right
     * @return Message
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return integer
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return Message
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return Message
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Message
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Message
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Krd\SupportBundle\Entity\Message $parent
     * @return Message
     */
    public function setParent(\Krd\SupportBundle\Entity\Message $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Krd\SupportBundle\Entity\Message
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \Krd\SupportBundle\Entity\Message $children
     * @return Message
     */
    public function addChildren(\Krd\SupportBundle\Entity\Message $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Krd\SupportBundle\Entity\Message $children
     */
    public function removeChildren(\Krd\SupportBundle\Entity\Message $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Message
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Message
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
