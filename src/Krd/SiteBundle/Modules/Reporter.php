<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Doctrine\ORM\EntityManager;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль городского репортера
 */
class Reporter extends AbstractModule
{
    /**
     *Рендер виджета для главной
     */
    public function renderWidget()
    {
        $publications = $this->getLastPublications();

        return $this->twig->render('KrdSiteBundle:Module:reporter/widget.html.twig', array('item' => $publications[0]));
    }

    public function renderAdminContent()
    {
        return '';
    }

    /**
     * Получение последних публикаций с сайта репортера
     * @param  integer $offset смещение выборки
     * @param  integer $count  Лимит выборки
     * @return array
     */
    public function getLastPublications($offset = 0, $count = 1)
    {
        $offset = (int)$offset;
        $count = (int)$count;

        $statement = $this->em->getConnection()->prepare("SELECT PB.`title`, PB.`date`,
                CONCAT(S.`url_ru`, REPLACE(REPLACE(LOWER(PB.`title`), ' ', '_'), '-', '_'), '/') as 'url',
                (SELECT I.`file`
                FROM `reporter__images` as I
                LEFT JOIN `reporter__images_fields` as F
                ON F.`img_id` = I.`id`
                WHERE I.`cat` = PB.`images` AND F.`field` = 'cb'
                ORDER BY F.`value` DESC
                LIMIT 1) as 'image',
                (SELECT COUNT(C.`id`) FROM `reporter__comments` as C WHERE C.`publish` = 1 AND C.`page` = CONCAT('publications-', PB.`id`)) as 'comments_count'

            FROM `reporter__publications` as PB

            INNER JOIN `reporter__structure` as S
            ON S.`id` = PB.`parent_id`

            WHERE PB.`publish` = 1 AND PB.`deleted` = 0 AND PB.`subicon` != 1 AND PB.`parent_id` != 164

            ORDER BY PB.`date` DESC

            LIMIT {$offset}, {$count}");

        $statement->execute();
        $publications = $statement->fetchAll();

        foreach($publications as &$pub){
            $pub['date'] = \DateTime::createFromFormat('Y-m-d H:i:s', $pub['date']);
            $pub['url'] = 'http://reporter.krd.ru'.preg_replace('/[^a-zA-Z0-9а-яА-ЯЁё\/_]/u', '', $pub['url']);

            $ext = pathinfo($pub['image'], PATHINFO_EXTENSION);
            $pub['image'] = 'http://reporter.krd.ru/_site/'.mb_substr($pub['image'], 0, -(strlen($ext) + 1)).'_newkrdpreview.'.$ext;
        }

        return $publications;
    }

    public function getManager()
    {
        return $this->em;
    }

    public function onNodeRemove(Node $node)
    {
    }
}
