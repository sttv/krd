<?php

namespace Krd\ConferenceBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Krd\ConferenceBundle\Entity\Online;


/**
 * Добавление SecurityContext к конференциям
 */
class OnlineSCListener
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Online) {
            $entity->setSecurityContext($this->container->get('security.context'));
        }
    }
}
