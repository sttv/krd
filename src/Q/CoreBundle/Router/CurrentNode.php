<?php

namespace Q\CoreBundle\Router;

use Symfony\Component\DependencyInjection\Container;

use Q\CoreBundle\Entity\Node;


/**
 * Определение текущей страницы
 */
class CurrentNode
{
    protected $currentPath;
    protected $container;
    protected $node;
    protected $em;
    protected $logger;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->logger = $this->container->get('logger');
    }

    /**
     * Генерация ссылки на раздел сайта по ID контроллера. Так же дописывает экшн
     * @param  integer $controllerId ID контроллера
     * @param  string $action Имя экшена
     * @param  array $parameters Параметры для экшена (если задан)
     * @return string|#
     */
    public function generate($controllerId, $action = null, $parameters = array())
    {
        $node = $this->em->getRepository('QCoreBundle:Node')->findBy(array('controller' => $controllerId), null, 1);

        if (isset($node[0])) {
            $node = $node[0];
            $url = $node->getUrl(true);

            if (!is_null($action)) {
                $url = rtrim($url, '/').$this->container->get('router')->generate($action, $parameters, false);
            }

            return $url;
        } else {
            return '#';
        }
    }

    /**
     * Устанавливает текущий URI и автоматически находит текущий нод
     * @param string $path
     */
    public function setCurrentPath($path)
    {
        $this->currentPath = rtrim(trim($path), '/');
        if (empty($this->currentPath)) {
            $this->currentPath = '/';
        }

        $this->logger->debug('Try to get node by path.', array($this->currentPath));
        $this->node = $this->em->getRepository('QCoreBundle:Node')->getNodeByPath($this->currentPath);
        $this->container->get('request')->attributes->add(array('node' => $this->node));

        if ($this->node) {
            $this->logger->debug('Matched node ['.$this->node->getId().']['.$this->node->getUrl().']['.$this->node->getTitle().']');
        } else {
            $this->logger->warn('No node matched');
        }
    }

    public function getCurrentPath()
    {
        return $this->currentPath;
    }

    public function hasCurrentPath()
    {
        return !empty($this->currentPath);
    }

    /**
     * Получение родительской цепочки нода
     *
     * @param Node[optional] $node
     * @return array
     */
    public function getNodeParentBranch(Node $node = null)
    {
        return $this->em->getRepository('QCoreBundle:Node')->getParentBranch((is_null($node) ? $this->node : $node), true);
    }

    /**
     * Получение текущего нода
     * @return Node
     */
    public function getNode()
    {
        return $this->node;
    }

    public function hasNode()
    {
        return $this->node instanceof Node;
    }


}
