angular.module('cms.tree', ['cms.utils'])
    /**
     * Дерево структуры
     */
    .directive('nodeTree', ['$http', '$parse', '$timeout', '$rootScope', function($http, $parse, $timeout, $rootScope) {
        // Предзагруженные ноды
        var __preload = {};

        return {
            restrict: 'A',
            scope: {
                url: '@',
                root: '@nodeTree',
                prefix: '@'
            },

            template: '<li ng-repeat="item in items" node-tree-item="item" prefix="prefix" url="url" class="node-tree-item" ng-class="{\'current\':item.current, \'disabled\':!item.active, \'denied\':!item.isGranted}"></li>',

            controller: function($scope) {
                /**
                 * Обновление списка пунктов
                 */
                $scope.updateItems = function(items) {
                    $scope.items = items;

                    $timeout(function() {
                        if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                            $scope.$broadcast('autoExpand', $scope.autoExpand);
                        }

                        $scope.$emit('loaded');
                    });
                };

                /**
                 * Загрузка списко элементов
                 */
                $scope.loadItems = function(callback) {
                    $http({
                            method: 'GET',
                            url: $scope.url,
                            params: {parent:$scope.root, count:1000, page:1}
                        })
                        .success(function(response) {
                            $scope.updateItems(response.items);
                            if ($.isFunction(callback)) {
                                callback(response.items);
                            }
                        })
                        .error(function(response) {
                            console.log(response);
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                        });
                };
            },

            compile: function($element) {
                var $link = $element.children('a.link');

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.autoExpand = $parse(attrs.autoExpand)($scope);
                    $scope.loadItems();
                };
            }
        }
    }])

    /**
     * Элемент дерева
     */
    .directive('nodeTreeItem', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                item: '=nodeTreeItem',
                prefix: '=',
                url: '='
            },
            template: '<i class="expand-btn" ng-click="toggle()" ng-class="icon()"></i>'
                    + '<i class="icon-share-alt" title="Редирект на {{ item.redirect }}" ng-show="!!item.redirect"></i>'
                    + '<a href="{{ prefix }}{{ item.url }}" class="link" target="_self" title="{{ title() }}">{{ item.title|characters:gettl(item) }}</a>',
            link: function($scope, $element, attrs) {
                $scope.expanded = false;
                $scope.loading = false;

                if (!$scope.item.title) {
                    $scope.item.title = '<Без названия>';
                }

                $scope.$on('autoExpand', function(event, autoExpand) {
                    if (event.targetScope === $scope) {
                        return;
                    }

                    $scope.autoExpand = autoExpand;
                    var itemPosition = $.inArray($scope.item.id, $scope.autoExpand);

                    if (itemPosition !== -1) {
                        $scope.toggle();
                        $scope.item.current = true;
                        $scope.autoExpand.splice(itemPosition, 1);

                        if ($scope.autoExpand.length == 0) {
                            $scope.item.currentLast = true;
                        }
                    }
                });

                $scope.$on('updateTree', function() {
                    if (!$scope.item.currentLast || $scope.autoExpand.length > 0) {
                        return;
                    }

                    $scope.$tree.scope().loadItems(function(items) {
                        if (!items || items.length == 0) {
                            $scope.item.isParent = false;
                        } else {
                            $scope.item.isParent = true;
                        }
                    });
                });
            },

            controller: function($scope, $element) {
                /**
                 * Получение иконки папки
                 */
                $scope.icon = function() {
                    if ($scope.loading) {
                        return 'icon-refresh infiinite-rotate';
                    }

                    if ($scope.item.isRoot) {
                        return 'icon-home';
                    }

                    if (!$scope.item.isParent) {
                        return 'icon-file';
                    }

                    if ($scope.expanded) {
                        return 'icon-folder-open';
                    } else {
                        return 'icon-folder-close';
                    }
                };

                /**
                 * Генерация дерева
                 */
                $scope.createTree = function() {
                    $scope.expanded = true;
                    $scope.loading = true;
                    $scope.$tree = angular.element('<ul class="node-tree unstyled" node-tree="{{ item.id }}" prefix="{{ prefix }}" url="{{ url }}" ng-show="items.length > 0"></ul>');

                    $element.append($scope.$tree);
                    $compile($scope.$tree)($scope);

                    $scope.$on('loaded', function(event) {
                        event.stopPropagation();
                        $scope.loading = false;
                        if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                            $scope.$broadcast('autoExpand', $scope.autoExpand);
                        }
                    });
                };

                /**
                 * Переключение состояния дерева
                 */
                $scope.toggle = function() {
                    if (!$scope.$tree) {
                        $scope.createTree();
                        return;
                    }

                    if ($scope.expanded) {
                        $scope.$tree.slideUp(300);
                    } else {
                        $scope.$tree.slideDown(300);
                    }

                    $scope.expanded = !$scope.expanded;
                };

                /**
                 * Возвращает длину строки для обрезки названия раздела
                 */
                $scope.gettl = function(item) {
                    var len = 165;

                    len = len - item.level * 15;

                    if (len < 50) {
                        len = 50;
                    }

                    return len;
                };

                /**
                 * Title-подсказка на категориях
                 */
                $scope.title = function() {
                    if (!$scope.item.isGranted) {
                        return 'Доступ запрещен';
                    } else {
                        return '[' + $scope.item.id + '] [' + $scope.item.controller.name + '] ' + $scope.item.title;
                    }
                }
            }
        }
    }])
;
