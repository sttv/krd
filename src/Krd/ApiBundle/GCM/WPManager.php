<?php

namespace Krd\ApiBundle\GCM;

use A\NotificationsBundle\Entity\WpfChannel;
use DateTime;
use Doctrine\ORM\EntityManager;

/**
 */
class WPManager
{
    /**
     * @var EntityManager
     */
    private $em;
	private $debug = true;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     */
    public function sendToAll(array $data)
    {
        $res = $this->em->createQuery('SELECT e.channel FROM KrdApiBundle:WpfChannel e')
            ->getResult();

        foreach($res as $item){
            $this->sendMessage($item['channel'], $data);
        }
    }
	
	private function sendMessage($channel, $data)
	{
		$format = <<<HTML
<?xml version="1.0" encoding="utf-8"?>
<wp:Notification xmlns:wp="WPNotification">
   <wp:Toast>
        <wp:Text1>{title}</wp:Text1>
        <wp:Text2>{date}</wp:Text2>
        <wp:Param>/Pages/EmergencyPage.xaml?Title=Экстренные сообщения</wp:Param>
   </wp:Toast>
</wp:Notification>
HTML;

		$format = strtr($format, array(
			'{title}' => $data['title'],
			'{date}' => $data['time']
		));
		
		$curl = curl_init();
		
		if( $this->debug )
			echo "Curl: channel ", $channel, "\n";
		
		curl_setopt($curl, CURLOPT_URL, $channel);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"Content-Length: ".strlen($format),
			"Content-Type: text/xml",
			"X-WindowsPhone-Target: toast",
			"X-NotificationClass: 2"
		));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $format);
		$out = curl_exec($curl);
		if( $this->debug )
	    	echo $out;
	    curl_close($curl);
	}
}