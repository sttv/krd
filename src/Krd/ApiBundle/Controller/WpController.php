<?php

namespace Krd\ApiBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityManager;
use Krd\ApiBundle\Entity\WpfChannel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;



/**
 * Windows Phone Test Notifications
 *
 * @Route("/wpf")
 */
class WpController extends Controller
{
	/**
     * Список
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * Сортировка всегда происходит по дате по убыванию
     *
     * @Route("/list.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdNewsBundle:Emergency')
            ->createQueryBuilder('n')
			->andWhere('n.id IN(232, 233, 235, 236, 237, 238)')
            ->orderBy('n.date', 'DESC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

	/**
     * @Route("/register_channel")
     * @Method({"POST"})
     */
    public function registerChannelAction(Request $request)
    {
        $channel = $request->get('channel');

        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var WpfChannel $wpfChannel */
        $wpfChannel = $em->getRepository('KrdApiBundle:WpfChannel')->findOneBy(array('channel' => $channel));

        if (!$wpfChannel) {
            $wpfChannel = new WpfChannel();
            $wpfChannel->setChannel($channel);
            $em->persist($wpfChannel);
        }

        $wpfChannel->setUpdated(new DateTime());
        $em->flush($wpfChannel);
		
		echo $channel, " saved ";
        exit;
    }
    /**
     * @Route("/test_emergency_news")
     */
    public function testEmerNewsAction(Request $request)
    {
		error_reporting(-1);
        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        $emer = $em->getRepository('KrdNewsBundle:Emergency')->find($request->get("id", 238));


        $this->get('notifications.wpf.wpmanager')->sendToAll(array(
            'title' => $emer->getTitle(),
            'time' => $emer->getDate()->format('H:i'), 
        ));

        die('finish');
    }
}
