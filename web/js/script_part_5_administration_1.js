/**
 * Структура администрации
 */
angular.module('administration', ['dialog'])
    /**
     * Показываем курируемые подразделения
     */
    .directive('admShowSubordinates', [function() {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                var prevText = $element.text();
                $element.html('{{ title() }}');

                return function($scope, $element, attrs) {
                    $scope.$body = $('.human-children[data-parent='+attrs.admShowSubordinates+']');

                    $scope.state = false;
                    $scope.text = {
                        visible: attrs.inactiveText,
                        hidden: prevText
                    };

                    $element.add($scope.$body.find('.btn.close')).on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.toggle();
                        });
                    });

                    $scope.$watch('state', function(state) {
                        if (state) {
                            $scope.show();
                        } else {
                            $scope.hide();
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                // Переключение
                $scope.toggle = function() {
                    $scope.state = !$scope.state;
                };

                // Текст кнопки
                $scope.title = function() {
                    return $scope.state ? $scope.text.visible : $scope.text.hidden;
                };

                // Показываем подразделения
                $scope.show = function() {
                    $scope.$body.stop(true, true).animate({opacity:1}, {queue:false}, 170).slideDown(200);
                };

                // Скрываем подразделения
                $scope.hide = function() {
                    $scope.$body.stop(true, true).animate({opacity:0}, {queue:false}, 170).slideUp(200);
                };
            }
        }
    }])
;
