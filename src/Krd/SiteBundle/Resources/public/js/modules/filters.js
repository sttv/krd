/**
 * Различные фильтры
 */
angular.module('filters', [])
    /**
     * Удаление времени из даты
     */
    .filter('dateRemoveTime', [function() {
        return function(value) {
            return value.toString().replace(/ \d{1,2}:\d{1,2}/, '');
        }
    }])
	.filter('decliner', [function() {
		return function(words, cnt) {
			cnt = Math.abs(cnt) % 100;
			$n1 = cnt % 10;
			if (cnt > 10 && cnt < 20)
				return  cnt + ' ' + words[2];
			else if ($n1 > 1 && $n1 < 5)
				return cnt + ' ' + words[1];
			else if ($n1 == 1)
				return cnt + ' ' + words[0];
			return cnt + ' ' + words[2];
		}
	}])
;
