<?php

namespace Krd\UserBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер Widget
 *
 * @Route("/rest/krd/user/widget")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestWidgetController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdUserBundle:Widget';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_user_widget_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_user_widget_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_user_widget", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        $qb->addOrderBy('e.sort');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_user_widget_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $res = parent::createAction($request);

        if (isset($res['entity'])) {
            $res['entity']->setSort($this->getRepository()->getNextSort());
        }

        $this->getDoctrine()->getManager()->flush();

        return $res;
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_user_widget_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_user_widget_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_user_widget_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        $entity->setTitle('Новый виджет');
        $entity->setModule('krd.user.modules.');
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_user_widget_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Сортировка элемента
     *
     * @Route("/sort/{order}/", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function actionSortOfParent(Request $request, $order)
    {
        return parent::actionSortOfParent($request, $order);
    }
}
