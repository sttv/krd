<?php

namespace Q\FilesBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\FilesBundle\Entity\File;


/**
 * Контроллер файлов
 *
 * @Route("/rest/qfiles/file")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestFileController extends Controller
{
    protected $entityName = 'QFilesBundle:File';
	
	/**
     * Загрузка видео по указанному локальному URL
     * Файл выбирается в elFinder
     *
     * @Route("/byftp/", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddByFtpAction(Request $request)
    {
    	ini_set('display_errors', 1);
		error_reporting(-1);
        $filePath = realpath($this->get('kernel')->getRootDir().'/../web').$request->get('url');

        $fs = new Filesystem();

        if (!$fs->exists($filePath) || !$fs->isAbsolutePath($filePath)) {
            return array('success' => false, 'msg' => 'File path error');
        }

        $newFilePath = $this->container->getParameter('q_files.video.cache_dir').'/'.uniqid(basename($filePath, '.'.pathinfo($filePath, PATHINFO_EXTENSION)).'-').'.'.pathinfo($filePath, PATHINFO_EXTENSION);

        try {
            $fs->copy($filePath, $newFilePath);
        } catch (IOException $e) {
            return array('success' => false, 'msg' => $e->getMessage());
        }

        $request->files->set('file', new UploadedFile($newFilePath, basename($filePath), null, null, null, true));
        $request->request->remove('url');

        return $this->restPostAddAction($request);
    }
	
    /**
     * Создание
     *
     * @Route("/", name="cms_rest_qfiles_file_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddAction(Request $request)
    {
        $entity = new File();
        $entity->setDate(new \DateTime());

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'entity' => $entity);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование полей файла
     *
     * @Route("/{id}", name="cms_rest_qfiles_file_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()->getManager()->find($this->entityName, $id);

        if ($title = $request->get('title')) {
            $entity->setTitle($title);
        } elseif ($title === '') {
            $entity->setTitle($title);
        }


        if ($date = $request->get('date')) {
            $date = \DateTime::createFromFormat('d/m/Y H:i', $date);
            $entity->setDate($date);
        }

        $this->getDoctrine()->getManager()->flush();

        return array('success' => true, 'entity' => $entity);
    }

    /**
     * Сортировка
     *
     * @Route("/sort/", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostSortAction(Request $request)
    {
        $sort = $request->get('sort', false);

        if ($sort) {
            foreach($sort as $id => $idx) {
                $entity = $this->getDoctrine()->getManager()->find($this->entityName, $id);
                if ($entity) {
                    $entity->setSort($idx);
                }
            }

            $this->getDoctrine()->getManager()->flush();
        }

        return array('success' => true);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_qfiles_file_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function restPostDeleteAction(Request $request, $id)
    {
        try {
            $entity = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM {$this->entityName} g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Not found');
        }

        $this->getDoctrine()->getManager()->remove($entity);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }
}
