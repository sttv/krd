/**
 * Модуль баннеров
 */
angular.module('banners', [])
    /**
     * Слайдер баннеров
     */
    .directive('bannersSlider', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                var $controll = $('<div class="controll" ng-show="iCount > 1"></div>');

                $element.find('.item').each(function(i) {
                    var $cItem = $('<div class="item" ng-class="{active:'+i+'==active}" ng-click="setActive('+i+')" title="'+$(this).attr('title')+'"></div>');
                    $controll.append($cItem);
                });

                $element.append($controll);

                return function($scope, $element) {
                    $scope.$items = $element.children('.item');
                    $scope.iCount = $scope.$items.length;
                    $scope.setActive(0);

                    $scope.$watch('active', $scope.update);

                    setInterval(function() {
                        $scope.$apply(function() {
                            $scope.autoNext();
                        });
                    }, 7000);
                }
            },

            controller: function($scope, $element) {
                $scope.autoNext = function() {
                    if ($element.find(':hover').length == 0) {
                        var active = $scope.active + 1;

                        if (active >= $scope.$items.length) {
                            active = 0;
                        }

                        $scope.active = active;
                    }
                };

                $scope.setActive = function(i) {
                    $scope.active = i;
                };

                $scope.update = function() {
                    var $active = $scope.$items.eq($scope.active);

                    $scope.$items.stop(true, true).css({opacity:1}).not($active).fadeOut(200);
                    $active.fadeIn(200);
                };
            }
        }
    }])
;
