<?php

namespace Krd\BannersBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;


/**
 * Контроллер CMS - баннеры
 *
 * @PreAuthorize("hasRole('ROLE_CMS_BANNER')")
 */
class BannersController extends Controller
{
    /**
     * Страница со списком баннеров
     *
     * @Route("/banners/", name="cms_banners_index")
     * @Template("KrdBannersBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {
    }
}
