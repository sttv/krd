var MFCMap = {
    rsr: null,
    init: function(width, height) {
        MFCMap.rsr = Raphael('mfc-map-features', width, height);
        MFCMap.build(MFCMap.rsr);
    },
    build: function(rsr) {

        var Layer_18 = rsr.set();
        var path_ap = rsr.path("M 1221.66,821.512 1258.662,820.871    1258.427,802.854 1221.404,803.493   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ap');
        var path_aq = rsr.path("M 1219.749,710.066 1182.53,710.711    1182.782,729.073 1220.01,728.43   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aq');
        var path_ar = rsr.path("M 1219.22,672.708 1182.02,673.352    1182.271,691.697 1219.479,691.054   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ar');
        var path_as = rsr.path("M 1219.484,691.388 1182.275,692.031    1182.526,710.377 1219.744,709.733   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_as');
        var path_at = rsr.path("M 1221.4,803.159 1258.423,802.52    1258.182,784.174 1221.14,784.814   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_at');
        var path_au = rsr.path("M 1220.343,728.425 1257.442,727.783    1257.201,709.42 1220.082,710.062   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_au');
        var path_av = rsr.path("M 1221.135,784.48 1258.178,783.84    1257.938,765.488 1220.876,766.129   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_av');
        var path_aw = rsr.path("M 1220.607,747.11 1257.687,746.469    1257.447,728.117 1220.348,728.758   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aw');
        var path_ax = rsr.path("M 1220.077,709.729 1257.197,709.087    1256.957,690.74 1219.818,691.382   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ax');
        var path_ay = rsr.path("M 1219.813,691.048 1256.952,690.406    1256.712,672.061 1219.555,672.703   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ay');
        var path_az = rsr.path("M 1221.07,803.498 1183.811,804.143    1184.057,822.162 1221.325,821.518   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_az');
        var path_ba = rsr.path("M 1220.807,784.819 1183.554,785.463    1183.806,803.81 1221.066,803.165   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ba');
        var path_bb = rsr.path("M 1220.541,766.135 1183.299,766.779    1183.55,785.13 1220.802,784.486   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bb');
        var path_bc = rsr.path("M 1220.871,765.795 1257.933,765.154    1257.691,746.802 1220.611,747.443   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bc');
        var path_bd = rsr.path("M 1220.278,747.449 1183.042,748.093    1183.295,766.445 1220.537,765.801   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bd');
        var path_be = rsr.path("M 1220.014,728.764 1182.787,729.408    1183.038,747.76 1220.273,747.116   z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_be');
        Layer_18.attr({'id': 'Layer_18','name': 'Layer_18'});
        var group_a = rsr.set();
        var path_bf = rsr.path("M 1304.742,96.45 1304.217,59.303     1286.131,59.626 1286.4,96.767    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bf');
        var path_bg = rsr.path("M 1285.798,59.632 1267.051,59.966     1267.559,97.092 1286.067,96.772    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bg');
        var path_bh = rsr.path("M 1323.602,96.124 1323.113,58.965     1304.551,59.297 1305.076,96.443    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bh');
        var path_bi = rsr.path("M 1307.984,301.964 1344.968,301.325     1344.724,282.639 1307.721,283.278    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bi');
        var path_bj = rsr.path("M 1307.716,282.944 1344.719,282.306     1344.482,264.287 1307.46,264.927    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bj');
        var path_bk = rsr.path("M 1305.805,171.501 1268.587,172.145     1268.838,190.508 1306.065,189.864    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bk');
        var path_bl = rsr.path("M 1304.747,96.783 1267.563,97.426     1267.814,115.772 1305.008,115.129    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bl');
        var path_bm = rsr.path("M 1305.277,134.143 1268.075,134.786     1268.326,153.132 1305.536,152.488    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bm');
        var path_bn = rsr.path("M 1305.012,115.463 1267.819,116.105     1268.07,134.451 1305.271,133.809    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bn');
        var path_bo = rsr.path("M 1305.541,152.821 1268.331,153.465     1268.582,171.811 1305.8,171.168    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bo');
        var path_bp = rsr.path("M 1307.455,264.594 1344.479,263.953     1344.238,245.607 1307.196,246.248    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bp');
        var path_bq = rsr.path("M 1342.273,95.8 1341.786,58.631     1323.447,58.959 1323.934,96.117    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bq');
        var path_br = rsr.path("M 1306.398,189.858 1343.498,189.218     1343.258,170.854 1306.139,171.494    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_br');
        var path_bs = rsr.path("M 1307.191,245.914 1344.233,245.273     1343.993,226.922 1306.932,227.563    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bs');
        var path_bt = rsr.path("M 1306.663,208.544 1343.743,207.902     1343.503,189.551 1306.403,190.191    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bt');
        var path_bu = rsr.path("M 1306.135,171.162 1343.254,170.52     1343.013,152.173 1305.875,152.816    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bu');
        var path_bv = rsr.path("M 1305.605,133.804 1342.764,133.16     1342.522,114.814 1305.346,115.456    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bv');
        var path_bw = rsr.path("M 1305.341,115.123 1342.52,114.48     1342.277,96.134 1305.08,96.777    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bw');
        var path_bx = rsr.path("M 1305.869,152.481 1343.008,151.841     1342.768,133.493 1305.609,134.136    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bx');
        var path_by = rsr.path("M 1289.129,302.624 1289.619,339.683     1308.179,339.309 1307.656,302.303    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_by');
        var path_bz = rsr.path("M 1307.127,264.933 1269.866,265.577     1270.114,283.595 1307.383,282.951    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_bz');
        var path_ca = rsr.path("M 1306.861,246.253 1269.611,246.897     1269.861,265.243 1307.123,264.599    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ca');
        var path_cb = rsr.path("M 1307.387,283.285 1270.118,283.93     1270.374,302.614 1307.651,301.97    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cb');
        var path_cc = rsr.path("M 1306.599,227.568 1269.355,228.212     1269.606,246.564 1306.857,245.92    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cc');
        var path_cd = rsr.path("M 1307.99,302.298 1308.514,339.303     1326.535,338.938 1326.052,301.984    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cd');
        var path_ce = rsr.path("M 1270.379,302.949 1270.887,340.06     1289.285,339.689 1288.796,302.629    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ce');
        var path_cf = rsr.path("M 1306.927,227.229 1343.987,226.588     1343.748,208.236 1306.668,208.877    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cf');
        var path_cg = rsr.path("M 1344.972,301.657 1326.385,301.979     1326.869,338.932 1345.455,338.559    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cg');
        var path_ch = rsr.path("M 1306.334,208.884 1269.099,209.527     1269.35,227.879 1306.594,227.234    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ch');
        var path_ci = rsr.path("M 1306.069,190.197 1268.843,190.841     1269.094,209.193 1306.33,208.549    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ci');
        group_a.attr({'parent': 'Layer_18','name': 'group_a'});
        var group_b = rsr.set();
        var path_cj = rsr.path("M 1211.441,98.323 1210.916,61.176     1192.83,61.499 1193.1,98.641    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cj');
        var path_ck = rsr.path("M 1192.497,61.505 1173.75,61.84     1174.258,98.967 1192.767,98.646    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ck');
        var path_cl = rsr.path("M 1230.301,97.997 1229.813,60.839     1211.25,61.17 1211.775,98.317    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cl');
        var path_cm = rsr.path("M 1214.684,303.837 1251.667,303.198     1251.423,284.512 1214.42,285.152    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cm');
        var path_cn = rsr.path("M 1214.415,284.818 1251.418,284.179     1251.182,266.161 1214.159,266.8    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cn');
        var path_co = rsr.path("M 1212.504,173.375 1175.286,174.019     1175.537,192.382 1212.765,191.737    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_co');
        var path_cp = rsr.path("M 1211.446,98.656 1174.263,99.3     1174.514,117.646 1211.707,117.002    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cp');
        var path_cq = rsr.path("M 1211.977,136.016 1174.774,136.658     1175.025,155.006 1212.235,154.362    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cq');
        var path_cr = rsr.path("M 1211.711,117.336 1174.519,117.979     1174.77,136.325 1211.971,135.682    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cr');
        var path_cs = rsr.path("M 1212.24,154.694 1175.03,155.338     1175.281,173.684 1212.499,173.041    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cs');
        var path_ct = rsr.path("M 1214.154,266.467 1251.178,265.827     1250.938,247.48 1213.896,248.122    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ct');
        var path_cu = rsr.path("M 1248.973,97.674 1248.485,60.504     1230.146,60.832 1230.633,97.991    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cu');
        var path_cv = rsr.path("M 1213.098,191.731 1250.197,191.091     1249.957,172.728 1212.838,173.369    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cv');
        var path_cw = rsr.path("M 1213.891,247.787 1250.933,247.146     1250.692,228.795 1213.631,229.436    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cw');
        var path_cx = rsr.path("M 1213.362,210.418 1250.442,209.776     1250.202,191.424 1213.103,192.065    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cx');
        var path_cy = rsr.path("M 1212.834,173.035 1249.953,172.393     1249.712,154.047 1212.574,154.689    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cy');
        var path_cz = rsr.path("M 1212.305,135.677 1249.463,135.034     1249.222,116.688 1212.045,117.33    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_cz');
        var path_da = rsr.path("M 1212.04,116.997 1249.219,116.354     1248.977,98.008 1211.779,98.651    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_da');
        var path_db = rsr.path("M 1212.568,154.355 1249.707,153.713     1249.467,135.367 1212.309,136.01    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_db');
        var path_dc = rsr.path("M 1195.828,304.498 1196.318,341.556     1214.878,341.182 1214.355,304.178    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dc');
        var path_dd = rsr.path("M 1213.826,266.806 1176.565,267.451     1176.813,285.469 1214.082,284.824    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dd');
        var path_de = rsr.path("M 1213.561,248.127 1176.311,248.771     1176.561,267.116 1213.822,266.473    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_de');
        var path_df = rsr.path("M 1214.086,285.158 1176.817,285.803     1177.073,304.488 1214.351,303.844    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_df');
        var path_dg = rsr.path("M 1213.298,229.442 1176.055,230.086     1176.306,248.438 1213.557,247.794    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dg');
        var path_dh = rsr.path("M 1214.689,304.171 1215.213,341.176     1233.234,340.813 1232.751,303.858    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dh');
        var path_di = rsr.path("M 1177.078,304.822 1177.586,341.934     1195.984,341.563 1195.495,304.503    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_di');
        var path_dj = rsr.path("M 1213.626,229.103 1250.687,228.462     1250.447,210.11 1213.367,210.75    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dj');
        var path_dk = rsr.path("M 1251.671,303.531 1233.084,303.854     1233.568,340.807 1252.154,340.432    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dk');
        var path_dl = rsr.path("M 1213.033,210.757 1175.798,211.4     1176.049,229.752 1213.293,229.107    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dl');
        var path_dm = rsr.path("M 1212.769,192.07 1175.542,192.715     1175.793,211.066 1213.029,210.423    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dm');
        group_b.attr({'parent': 'Layer_18','name': 'group_b'});
        var group_c = rsr.set();
        var path_dn = rsr.path("M 1015.484,101.706 1014.959,64.559     996.873,64.881 997.143,102.023    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dn');
        var path_do = rsr.path("M 996.54,64.888 977.791,65.222     978.301,102.349 996.809,102.029    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_do');
        var path_dp = rsr.path("M 1034.343,101.379 1033.855,64.221     1015.293,64.553 1015.818,101.7    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dp');
        var path_dq = rsr.path("M 1018.727,307.221 1055.71,306.58     1055.465,287.896 1018.463,288.535    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dq');
        var path_dr = rsr.path("M 1018.458,288.201 1055.461,287.561     1055.225,269.543 1018.202,270.184    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dr');
        var path_ds = rsr.path("M 1016.546,176.758 979.329,177.4     979.58,195.764 1016.808,195.12    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ds');
        var path_dt = rsr.path("M 1015.489,102.04 978.305,102.683     978.557,121.028 1015.75,120.386    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dt');
        var path_du = rsr.path("M 1016.019,139.398 978.817,140.041     979.068,158.388 1016.277,157.743    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_du');
        var path_dv = rsr.path("M 1015.754,120.719 978.562,121.362     978.813,139.707 1016.013,139.065    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dv');
        var path_dw = rsr.path("M 1016.283,158.078 979.073,158.722     979.323,177.067 1016.542,176.424    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dw');
        var path_dx = rsr.path("M 1018.198,269.85 1055.22,269.21     1054.98,250.864 1017.938,251.504    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dx');
        var path_dy = rsr.path("M 1053.016,101.057 1052.528,63.888     1034.189,64.215 1034.676,101.374    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dy');
        var path_dz = rsr.path("M 1017.14,195.114 1054.239,194.474     1054,176.109 1016.881,176.752    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_dz');
        var path_ea = rsr.path("M 1017.934,251.17 1054.976,250.53     1054.734,232.178 1017.674,232.819    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ea');
        var path_eb = rsr.path("M 1017.405,213.8 1054.484,213.158     1054.245,194.807 1017.145,195.448    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eb');
        var path_ec = rsr.path("M 1016.875,176.417 1053.996,175.776     1053.754,157.431 1016.616,158.071    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ec');
        var path_ed = rsr.path("M 1016.348,139.059 1053.506,138.416     1053.265,120.07 1016.088,120.713    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ed');
        var path_ee = rsr.path("M 1016.083,120.379 1053.261,119.736     1053.02,101.39 1015.823,102.033    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ee');
        var path_ef = rsr.path("M 1016.611,157.738 1053.75,157.096     1053.51,138.75 1016.352,139.393    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ef');
        var path_eg = rsr.path("M 999.871,307.88 1000.36,344.938     1018.921,344.565 1018.397,307.56    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eg');
        var path_eh = rsr.path("M 1017.869,270.188 980.608,270.833     980.855,288.852 1018.125,288.207    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eh');
        var path_ei = rsr.path("M 1017.604,251.51 980.354,252.154     980.604,270.5 1017.864,269.855    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ei');
        var path_ej = rsr.path("M 1018.129,288.541 980.861,289.185     981.115,307.87 1018.393,307.226    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ej');
        var path_ek = rsr.path("M 1017.341,232.824 980.097,233.469     980.35,251.82 1017.6,251.176    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ek');
        var path_el = rsr.path("M 1018.731,307.554 1019.255,344.559     1037.276,344.196 1036.794,307.241    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_el');
        var path_em = rsr.path("M 981.121,308.204 981.628,345.316     1000.027,344.945 999.538,307.886    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_em');
        var path_en = rsr.path("M 1017.669,232.484 1054.729,231.845     1054.49,213.492 1017.41,214.134    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_en');
        var path_eo = rsr.path("M 1055.715,306.915 1037.127,307.235     1037.61,344.188 1056.198,343.813    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eo');
        var path_ep = rsr.path("M 1017.076,214.139 979.841,214.783     980.092,233.135 1017.336,232.491    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ep');
        var path_eq = rsr.path("M 1016.812,195.454 979.585,196.098     979.836,214.449 1017.07,213.807    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eq');
        group_c.attr({'parent': 'Layer_18','name': 'group_c'});
        var group_d = rsr.set();
        var path_er = rsr.path("M 1118.123,99.697 1117.598,62.55     1099.51,62.873 1099.78,100.015    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_er');
        var path_es = rsr.path("M 1099.177,62.879 1080.431,63.214     1080.938,100.341 1099.446,100.02    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_es');
        var path_et = rsr.path("M 1136.98,99.371 1136.493,62.212     1117.931,62.545 1118.456,99.691    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_et');
        var path_eu = rsr.path("M 1121.365,305.211 1158.348,304.572     1158.103,285.887 1121.1,286.526    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_eu');
        var path_ev = rsr.path("M 1121.095,286.193 1158.099,285.554     1157.861,267.535 1120.841,268.175    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ev');
        var path_ew = rsr.path("M 1118.127,100.031 1080.943,100.674     1081.195,119.02 1118.387,118.377    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ew');
        var path_ex = rsr.path("M 1118.656,137.391 1081.455,138.033     1081.707,156.38 1118.915,155.735    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ex');
        var path_ey = rsr.path("M 1118.392,118.71 1081.199,119.354     1081.451,137.699 1118.652,137.056    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ey');
        var path_ez = rsr.path("M 1118.921,156.069 1081.711,156.714     1081.963,175.06 1119.181,174.416    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ez');
        var path_fa = rsr.path("M 1120.836,267.842 1157.857,267.201     1157.617,248.854 1120.576,249.496    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fa');
        var path_fb = rsr.path("M 1155.653,99.049 1155.166,61.88     1136.827,62.207 1137.314,99.366    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fb');
        var path_fc = rsr.path("M 1119.777,193.106 1156.879,192.465     1156.637,174.102 1119.519,174.743    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fc');
        var path_fd = rsr.path("M 1120.571,249.161 1157.612,248.521     1157.373,230.17 1120.313,230.811    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fd');
        var path_fe = rsr.path("M 1120.042,211.792 1157.123,211.15     1156.883,192.799 1119.783,193.439    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fe');
        var path_ff = rsr.path("M 1119.514,174.409 1156.633,173.768     1156.393,155.421 1119.254,156.063    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ff');
        var path_fg = rsr.path("M 1118.984,137.051 1156.143,136.408     1155.902,118.062 1118.725,118.705    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fg');
        var path_fh = rsr.path("M 1118.721,118.371 1155.898,117.729     1155.657,99.382 1118.462,100.025    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fh');
        var path_fi = rsr.path("M 1119.25,155.73 1156.387,155.088     1156.147,136.742 1118.99,137.384    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fi');
        var path_fj = rsr.path("M 1102.51,305.871 1102.998,342.93     1121.561,342.557 1121.035,305.552    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fj');
        var path_fk = rsr.path("M 1120.506,268.181 1083.247,268.824     1083.493,286.843 1120.762,286.198    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fk');
        var path_fl = rsr.path("M 1120.243,249.502 1082.99,250.145     1083.242,268.492 1120.502,267.848    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fl');
        var path_fm = rsr.path("M 1120.767,286.532 1083.498,287.177     1083.754,305.862 1121.031,305.218    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fm');
        var path_fn = rsr.path("M 1119.979,230.816 1082.735,231.46     1082.986,249.813 1120.237,249.168    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fn');
        var path_fo = rsr.path("M 1121.369,305.545 1121.893,342.55     1139.914,342.188 1139.432,305.233    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fo');
        var path_fp = rsr.path("M 1083.759,306.196 1084.268,343.308     1102.665,342.938 1102.176,305.878    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fp');
        var path_fq = rsr.path("M 1120.307,230.477 1157.367,229.836     1157.127,211.484 1120.048,212.125    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fq');
        var path_fr = rsr.path("M 1158.352,304.906 1139.766,305.227     1140.249,342.181 1158.837,341.806    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fr');
        group_d.attr({'parent': 'Layer_18','name': 'group_d'});
        var group_e = rsr.set();
        var path_fs = rsr.path("M 922.189,103.508 921.664,66.361     903.577,66.684 903.847,103.824    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fs');
        var path_ft = rsr.path("M 903.244,66.69 884.497,67.024     885.006,104.15 903.514,103.831    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ft');
        var path_fu = rsr.path("M 941.047,103.182 940.559,66.023     921.997,66.355 922.523,103.503    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fu');
        var path_fv = rsr.path("M 925.432,309.022 962.414,308.383     962.17,289.697 925.167,290.338    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fv');
        var path_fw = rsr.path("M 925.162,290.004 962.165,289.363     961.929,271.345 924.908,271.985    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fw');
        var path_fx = rsr.path("M 923.252,178.56 886.033,179.202     886.285,197.566 923.511,196.922    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fx');
        var path_fy = rsr.path("M 922.193,103.842 885.01,104.485     885.262,122.831 922.454,122.188    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fy');
        var path_fz = rsr.path("M 922.723,141.2 885.521,141.844     885.773,160.189 922.983,159.546    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_fz');
        var path_ga = rsr.path("M 922.459,122.521 885.266,123.164     885.518,141.51 922.719,140.867    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ga');
        var path_gb = rsr.path("M 922.987,159.88 885.777,160.523     886.029,178.869 923.246,178.226    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gb');
        var path_gc = rsr.path("M 924.902,271.651 961.924,271.012     961.684,252.666 924.644,253.306    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gc');
        var path_gd = rsr.path("M 959.72,102.859 959.233,65.689     940.893,66.018 941.381,103.177    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gd');
        var path_ge = rsr.path("M 923.845,196.917 960.945,196.274     960.703,177.911 923.584,178.554    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ge');
        var path_gf = rsr.path("M 924.638,252.973 961.68,252.332     961.439,233.979 924.379,234.621    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gf');
        var path_gg = rsr.path("M 924.109,215.603 961.189,214.96     960.949,196.609 923.85,197.25    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gg');
        var path_gh = rsr.path("M 923.58,178.221 960.699,177.578     960.459,159.232 923.321,159.874    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gh');
        var path_gi = rsr.path("M 923.051,140.861 960.21,140.218     959.969,121.872 922.792,122.516    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gi');
        var path_gj = rsr.path("M 922.787,122.181 959.965,121.539     959.726,103.193 922.527,103.835    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gj');
        var path_gk = rsr.path("M 923.316,159.54 960.454,158.898     960.215,140.553 923.057,141.195    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gk');
        var path_gl = rsr.path("M 906.576,309.682 907.065,346.74     925.626,346.367 925.103,309.361    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gl');
        var path_gm = rsr.path("M 924.573,271.99 887.313,272.635     887.56,290.654 924.828,290.01    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gm');
        var path_gn = rsr.path("M 924.31,253.313 887.057,253.956     887.31,272.302 924.568,271.658    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gn');
        var path_go = rsr.path("M 924.834,290.343 887.564,290.987     887.821,309.672 925.098,309.027    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_go');
        var path_gp = rsr.path("M 924.045,234.627 886.802,235.271     887.053,253.622 924.305,252.978    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gp');
        var path_gq = rsr.path("M 925.436,309.355 925.959,346.36     943.981,345.997 943.499,309.044    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gq');
        var path_gr = rsr.path("M 887.825,310.006 888.333,347.117     906.731,346.748 906.242,309.688    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gr');
        var path_gs = rsr.path("M 924.373,234.287 961.435,233.646     961.194,215.294 924.114,215.936    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gs');
        var path_gt = rsr.path("M 962.419,308.716 943.832,309.037     944.316,345.991 962.902,345.617    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gt');
        var path_gu = rsr.path("M 923.781,215.941 886.545,216.585     886.797,234.938 924.04,234.293    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gu');
        var path_gv = rsr.path("M 923.516,197.256 886.289,197.9     886.541,216.252 923.775,215.608    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gv');
        group_e.attr({'parent': 'Layer_18','name': 'group_e'});
        var group_f = rsr.set();
        var path_gw = rsr.path("M 828.757,105.527 828.231,68.381     810.145,68.705 810.414,105.846    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gw');
        var path_gx = rsr.path("M 809.811,68.71 791.064,69.045     791.572,106.171 810.081,105.851    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gx');
        var path_gy = rsr.path("M 847.614,105.202 847.128,68.044     828.564,68.375 829.091,105.522    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gy');
        var path_gz = rsr.path("M 831.999,311.043 868.982,310.403     868.737,291.717 831.734,292.357    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_gz');
        var path_ha = rsr.path("M 831.729,292.023 868.732,291.385     868.496,273.366 831.476,274.006    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ha');
        var path_hb = rsr.path("M 828.762,105.862 791.577,106.505     791.829,124.851 829.021,124.208    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hb');
        var path_hc = rsr.path("M 829.29,143.222 792.089,143.863     792.341,162.211 829.55,161.567    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hc');
        var path_hd = rsr.path("M 829.025,124.541 791.833,125.185     792.085,143.531 829.286,142.887    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hd');
        var path_he = rsr.path("M 829.555,161.9 792.345,162.544     792.597,180.89 829.815,180.246    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_he');
        var path_hf = rsr.path("M 831.47,273.673 868.492,273.032     868.251,254.686 831.211,255.326    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hf');
        var path_hg = rsr.path("M 866.287,104.879 865.801,67.71     847.461,68.037 847.948,105.196    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hg');
        var path_hh = rsr.path("M 830.412,198.937 867.513,198.295     867.271,179.933 830.153,180.573    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hh');
        var path_hi = rsr.path("M 831.205,254.993 868.247,254.352     868.007,236.001 830.946,236.641    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hi');
        var path_hj = rsr.path("M 830.677,217.623 867.757,216.981     867.518,198.629 830.417,199.271    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hj');
        var path_hk = rsr.path("M 830.148,180.24 867.267,179.599     867.027,161.252 829.889,161.895    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hk');
        var path_hl = rsr.path("M 829.618,142.881 866.776,142.239     866.537,123.893 829.359,124.535    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hl');
        var path_hm = rsr.path("M 829.355,124.202 866.532,123.56     866.293,105.213 829.096,105.856    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hm');
        var path_hn = rsr.path("M 829.884,161.561 867.021,160.918     866.782,142.572 829.624,143.215    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hn');
        var path_ho = rsr.path("M 813.144,311.701 813.633,348.76     832.194,348.388 831.67,311.382    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ho');
        var path_hp = rsr.path("M 831.141,274.012 793.881,274.656     794.127,292.674 831.396,292.03    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hp');
        var path_hq = rsr.path("M 830.877,255.332 793.624,255.977     793.877,274.322 831.137,273.678    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hq');
        var path_hr = rsr.path("M 831.4,292.363 794.132,293.008     794.389,311.693 831.665,311.049    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hr');
        var path_hs = rsr.path("M 830.613,236.647 793.369,237.291     793.62,255.643 830.872,254.998    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hs');
        var path_ht = rsr.path("M 832.003,311.376 832.528,348.381     850.549,348.018 850.066,311.063    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ht');
        var path_hu = rsr.path("M 794.393,312.026 794.901,349.139     813.299,348.768 812.811,311.708    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hu');
        var path_hv = rsr.path("M 830.941,236.308 868.002,235.666     867.761,217.314 830.682,217.956    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hv');
        var path_hw = rsr.path("M 868.986,310.736 850.399,311.059     850.884,348.011 869.471,347.637    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hw');
        group_f.attr({'parent': 'Layer_18','name': 'group_f'});
        var group_g = rsr.set();
        var path_hx = rsr.path("M 833.656,452.738 833.13,415.591     815.044,415.914 815.314,453.055    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hx');
        var path_hy = rsr.path("M 814.711,415.921 795.964,416.255     796.473,453.381 814.98,453.062    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hy');
        var path_hz = rsr.path("M 852.515,452.412 852.026,415.254     833.464,415.585 833.989,452.732    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_hz');
        var path_ia = rsr.path("M 834.719,527.79 797.5,528.434     797.752,546.797 834.979,546.153    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ia');
        var path_ib = rsr.path("M 833.661,453.072 796.477,453.715     796.729,472.062 833.921,471.418    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ib');
        var path_ic = rsr.path("M 834.189,490.432 796.988,491.073     797.24,509.42 834.449,508.777    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ic');
        var path_id = rsr.path("M 833.926,471.752 796.733,472.395     796.984,490.74 834.186,490.098    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_id');
        var path_ie = rsr.path("M 834.454,509.109 797.245,509.754     797.496,528.1 834.714,527.456    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ie');
        var path_if = rsr.path("M 836.37,620.883 873.392,620.242     873.151,601.896 836.109,602.537    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_if');
        var path_ig = rsr.path("M 871.188,452.089 870.699,414.92     852.36,415.248 852.848,452.406    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ig');
        var path_ih = rsr.path("M 835.313,546.147 872.412,545.506     872.171,527.143 835.052,527.783    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ih');
        var path_ii = rsr.path("M 836.104,602.203 873.146,601.562     872.906,583.211 835.846,583.851    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ii');
        var path_ij = rsr.path("M 835.576,564.832 872.656,564.191     872.416,545.84 835.316,546.481    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ij');
        var path_ik = rsr.path("M 835.048,527.45 872.167,526.809     871.926,508.462 834.787,509.104    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ik');
        var path_il = rsr.path("M 834.519,490.091 871.678,489.449     871.436,471.104 834.259,471.746    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_il');
        var path_im = rsr.path("M 834.254,471.412 871.433,470.77     871.191,452.423 833.995,453.066    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_im');
        var path_in = rsr.path("M 834.783,508.771 871.922,508.129     871.681,489.782 834.524,490.425    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_in');
        var path_io = rsr.path("M 835.776,602.542 798.524,603.186     798.775,621.532 836.036,620.888    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_io');
        var path_ip = rsr.path("M 835.512,583.857 798.269,584.501     798.52,602.854 835.771,602.209    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ip');
        var path_iq = rsr.path("M 835.841,583.518 872.902,582.877     872.661,564.524 835.582,565.166    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iq');
        var path_ir = rsr.path("M 835.247,565.173 798.013,565.816     798.265,584.168 835.506,583.522    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ir');
        var path_is = rsr.path("M 834.982,546.486 797.757,547.131     798.009,565.482 835.243,564.838    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_is');
        group_g.attr({'parent': 'Layer_18','name': 'group_g'});
        var group_h = rsr.set();
        var path_it = rsr.path("M 926.902,451.791 926.376,414.643     908.289,414.967 908.56,452.107    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_it');
        var path_iu = rsr.path("M 907.957,414.973 889.209,415.308     889.718,452.434 908.227,452.113    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iu');
        var path_iv = rsr.path("M 945.76,451.464 945.272,414.306     926.71,414.638 927.235,451.785    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iv');
        var path_iw = rsr.path("M 927.964,526.842 890.746,527.485     890.997,545.85 928.225,545.205    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iw');
        var path_ix = rsr.path("M 926.906,452.125 889.723,452.767     889.974,471.113 927.166,470.471    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ix');
        var path_iy = rsr.path("M 927.437,489.483 890.234,490.127     890.485,508.473 927.695,507.829    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iy');
        var path_iz = rsr.path("M 927.171,470.803 889.979,471.446     890.229,489.792 927.431,489.148    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_iz');
        var path_ja = rsr.path("M 927.699,508.163 890.49,508.807     890.74,527.152 927.959,526.509    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ja');
        var path_jb = rsr.path("M 929.615,619.935 966.638,619.295     966.396,600.949 929.355,601.589    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jb');
        var path_jc = rsr.path("M 964.432,451.143 963.945,413.973     945.605,414.3 946.093,451.46    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jc');
        var path_jd = rsr.path("M 928.558,545.199 965.657,544.558     965.417,526.194 928.298,526.836    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jd');
        var path_je = rsr.path("M 929.35,601.255 966.393,600.614     966.152,582.263 929.091,582.902    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_je');
        var path_jf = rsr.path("M 928.822,563.885 965.902,563.243     965.662,544.891 928.563,545.533    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jf');
        var path_jg = rsr.path("M 928.293,526.502 965.412,525.861     965.172,507.514 928.033,508.156    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jg');
        var path_jh = rsr.path("M 927.765,489.144 964.922,488.501     964.682,470.155 927.504,470.798    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jh');
        var path_ji = rsr.path("M 927.5,470.464 964.678,469.821     964.437,451.476 927.24,452.118    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ji');
        var path_jj = rsr.path("M 928.027,507.822 965.168,507.181     964.927,488.834 927.769,489.477    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jj');
        var path_jk = rsr.path("M 929.021,601.594 891.771,602.238     892.021,620.584 929.282,619.939    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jk');
        var path_jl = rsr.path("M 928.758,582.909 891.515,583.553     891.765,601.905 929.017,601.261    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jl');
        var path_jm = rsr.path("M 929.086,582.57 966.146,581.93     965.906,563.577 928.826,564.219    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jm');
        var path_jn = rsr.path("M 928.493,564.225 891.258,564.868     891.509,583.219 928.752,582.576    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jn');
        var path_jo = rsr.path("M 928.229,545.539 891.002,546.184     891.253,564.534 928.488,563.89    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jo');
        group_h.attr({'parent': 'Layer_18','name': 'group_h'});
        var group_i = rsr.set();
        var path_jp = rsr.path("M 1020.321,450.704 1019.796,413.557     1001.709,413.88 1001.979,451.021    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jp');
        var path_jq = rsr.path("M 1001.377,413.886 982.629,414.221     983.138,451.348 1001.646,451.027    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jq');
        var path_jr = rsr.path("M 1039.179,450.378 1038.691,413.219     1020.131,413.551 1020.655,450.697    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jr');
        var path_js = rsr.path("M 1021.384,525.756 984.165,526.399     984.417,544.763 1021.644,544.118    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_js');
        var path_jt = rsr.path("M 1020.326,451.037 983.142,451.681     983.394,470.027 1020.586,469.383    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jt');
        var path_ju = rsr.path("M 1020.855,488.396 983.653,489.04     983.905,507.386 1021.115,506.742    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ju');
        var path_jv = rsr.path("M 1020.59,469.717 983.397,470.36     983.649,488.706 1020.851,488.063    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jv');
        var path_jw = rsr.path("M 1021.119,507.076 983.909,507.719     984.161,526.066 1021.379,525.422    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jw');
        var path_jx = rsr.path("M 1023.034,618.848 1060.058,618.209     1059.816,599.861 1022.775,600.503    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jx');
        var path_jy = rsr.path("M 1057.852,450.055 1057.365,412.887     1039.025,413.213 1039.513,450.373    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jy');
        var path_jz = rsr.path("M 1021.977,544.113 1059.077,543.472     1058.836,525.108 1021.717,525.75    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_jz');
        var path_ka = rsr.path("M 1022.771,600.168 1059.813,599.527     1059.571,581.176 1022.511,581.816    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ka');
        var path_kb = rsr.path("M 1022.242,562.798 1059.322,562.156     1059.081,543.805 1021.981,544.446    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kb');
        var path_kc = rsr.path("M 1021.713,525.416 1058.832,524.773     1058.592,506.428 1021.453,507.07    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kc');
        var path_kd = rsr.path("M 1021.184,488.058 1058.342,487.415     1058.103,469.069 1020.924,469.711    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kd');
        var path_ke = rsr.path("M 1020.92,469.378 1058.098,468.734     1057.857,450.389 1020.66,451.032    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ke');
        var path_kf = rsr.path("M 1021.448,506.736 1058.586,506.094     1058.346,487.748 1021.188,488.391    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kf');
        var path_kg = rsr.path("M 1022.441,600.508 985.189,601.151     985.441,619.497 1022.701,618.854    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kg');
        var path_kh = rsr.path("M 1022.177,581.823 984.934,582.467     985.186,600.818 1022.437,600.174    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kh');
        var path_ki = rsr.path("M 1022.506,581.483 1059.566,580.842     1059.326,562.491 1022.246,563.132    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ki');
        var path_kj = rsr.path("M 1021.914,563.138 984.678,563.781     984.929,582.132 1022.173,581.488    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kj');
        var path_kk = rsr.path("M 1021.648,544.451 984.421,545.096     984.674,563.447 1021.907,562.804    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kk');
        group_i.attr({'parent': 'Layer_18','name': 'group_i'});
        var group_j = rsr.set();
        var path_kl = rsr.path("M 837.089,714.615 799.871,715.259     800.122,733.621 837.348,732.979    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kl');
        var path_km = rsr.path("M 836.031,639.897 798.848,640.54     799.098,658.887 836.29,658.243    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_km');
        var path_kn = rsr.path("M 836.561,677.256 799.359,677.898     799.609,696.246 836.819,695.602    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kn');
        var path_ko = rsr.path("M 836.296,658.577 799.102,659.221     799.354,677.566 836.555,676.923    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ko');
        var path_kp = rsr.path("M 836.824,695.936 799.613,696.579     799.866,714.925 837.085,714.281    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kp');
        var path_kq = rsr.path("M 838.739,807.707 875.762,807.067     875.521,788.722 838.48,789.361    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kq');
        var path_kr = rsr.path("M 837.682,732.973 874.781,732.332     874.541,713.967 837.422,714.609    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kr');
        var path_ks = rsr.path("M 838.475,789.028 875.517,788.388     875.275,770.035 838.215,770.677    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ks');
        var path_kt = rsr.path("M 837.946,751.657 875.026,751.017     874.785,732.665 837.688,733.305    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kt');
        var path_ku = rsr.path("M 837.417,714.275 874.536,713.634     874.297,695.288 837.158,695.93    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ku');
        var path_kv = rsr.path("M 836.889,676.917 874.046,676.275     873.807,657.928 836.629,658.57    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kv');
        var path_kw = rsr.path("M 836.625,658.236 873.803,657.595     873.562,639.248 836.365,639.891    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kw');
        var path_kx = rsr.path("M 837.152,695.597 874.291,694.954     874.051,676.607 836.894,677.251    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kx');
        var path_ky = rsr.path("M 838.146,789.368 800.895,790.012     801.146,808.357 838.406,807.713    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ky');
        var path_kz = rsr.path("M 837.881,770.683 800.639,771.327     800.89,789.678 838.142,789.033    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_kz');
        var path_la = rsr.path("M 838.211,770.342 875.271,769.702     875.031,751.351 837.95,751.991    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_la');
        var path_lb = rsr.path("M 837.617,751.998 800.383,752.641     800.634,770.992 837.877,770.349    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lb');
        var path_lc = rsr.path("M 837.353,733.312 800.127,733.955     800.378,752.308 837.612,751.664    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lc');
        group_j.attr({'parent': 'Layer_18','name': 'group_j'});
        var group_k = rsr.set();
        var path_ld = rsr.path("M 894.396,806.687 931.418,806.046     931.178,787.7 894.136,788.341    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ld');
        var path_le = rsr.path("M 893.338,731.951 930.438,731.311     930.197,712.947 893.077,713.588    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_le');
        var path_lf = rsr.path("M 894.131,788.007 931.172,787.367     930.932,769.015 893.871,769.656    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lf');
        var path_lg = rsr.path("M 893.602,750.637 930.683,749.996     930.441,731.644 893.343,732.285    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lg');
        var path_lh = rsr.path("M 893.073,713.255 930.192,712.612     929.951,694.267 892.813,694.909    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lh');
        var path_li = rsr.path("M 892.544,675.896 929.703,675.254     929.463,656.907 892.285,657.551    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_li');
        var path_lj = rsr.path("M 892.281,657.217 929.458,656.573     929.218,638.228 892.021,638.871    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lj');
        var path_lk = rsr.path("M 892.809,694.575 929.947,693.934     929.707,675.588 892.55,676.229    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lk');
        var path_ll = rsr.path("M 893.867,769.322 930.928,768.681     930.687,750.33 893.607,750.97    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ll');
        group_k.attr({'parent': 'Layer_18','name': 'group_k'});
        var group_l = rsr.set();
        var path_lm = rsr.path("M 1128.3,821.986 1165.303,821.348     1165.066,803.329 1128.046,803.969    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lm');
        var path_ln = rsr.path("M 1126.389,710.543 1089.172,711.187     1089.423,729.551 1126.648,728.905    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ln');
        var path_lo = rsr.path("M 1125.332,635.825 1088.148,636.468     1088.398,654.813 1125.591,654.17    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lo');
        var path_lp = rsr.path("M 1125.861,673.185 1088.66,673.828     1088.91,692.174 1126.12,691.53    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lp');
        var path_lq = rsr.path("M 1125.597,654.504 1088.402,655.148     1088.655,673.494 1125.855,672.851    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lq');
        var path_lr = rsr.path("M 1126.125,691.863 1088.914,692.507     1089.167,710.853 1126.385,710.209    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lr');
        var path_ls = rsr.path("M 1128.041,803.636 1165.063,802.995     1164.822,784.649 1127.781,785.29    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ls');
        var path_lt = rsr.path("M 1126.982,728.899 1164.082,728.26     1163.842,709.896 1126.723,710.537    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lt');
        var path_lu = rsr.path("M 1127.776,784.955 1164.817,784.316     1164.576,765.964 1127.516,766.605    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lu');
        var path_lv = rsr.path("M 1127.247,747.586 1164.327,746.944     1164.087,728.593 1126.988,729.233    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lv');
        var path_lw = rsr.path("M 1126.718,710.204 1163.837,709.562     1163.598,691.216 1126.459,691.857    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lw');
        var path_lx = rsr.path("M 1126.189,672.844 1163.348,672.202     1163.107,653.856 1125.93,654.498    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lx');
        var path_ly = rsr.path("M 1125.926,654.165 1163.104,653.522     1162.862,635.177 1125.666,635.819    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ly');
        var path_lz = rsr.path("M 1126.453,691.524 1163.592,690.881     1163.352,672.535 1126.194,673.178    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_lz');
        var path_ma = rsr.path("M 1127.711,803.975 1090.451,804.619     1090.698,822.638 1127.966,821.992    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ma');
        var path_mb = rsr.path("M 1127.448,785.295 1090.195,785.939     1090.446,804.285 1127.707,803.641    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mb');
        var path_mc = rsr.path("M 1127.183,766.61 1089.939,767.254     1090.19,785.605 1127.442,784.961    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mc');
        var path_md = rsr.path("M 1127.512,766.271 1164.572,765.63     1164.332,747.277 1127.252,747.919    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_md');
        var path_me = rsr.path("M 1126.918,747.925 1089.684,748.568     1089.935,766.92 1127.178,766.276    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_me');
        var path_mf = rsr.path("M 1126.653,729.24 1089.428,729.883     1089.679,748.235 1126.914,747.591    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mf');
        group_l.attr({'parent': 'Layer_18','name': 'group_l'});
        var group_m = rsr.set();
        var path_mg = rsr.path("M 1128.3,821.986 1165.303,821.348     1165.066,803.329 1128.046,803.969    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mg');
        var path_mh = rsr.path("M 1126.389,710.543 1089.172,711.187     1089.423,729.551 1126.648,728.905    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mh');
        var path_mi = rsr.path("M 1125.332,635.825 1088.148,636.468     1088.398,654.813 1125.591,654.17    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mi');
        var path_mj = rsr.path("M 1125.861,673.185 1088.66,673.828     1088.91,692.174 1126.12,691.53    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mj');
        var path_mk = rsr.path("M 1125.597,654.504 1088.402,655.148     1088.655,673.494 1125.855,672.851    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mk');
        var path_ml = rsr.path("M 1126.125,691.863 1088.914,692.507     1089.167,710.853 1126.385,710.209    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ml');
        var path_mm = rsr.path("M 1128.041,803.636 1165.063,802.995     1164.822,784.649 1127.781,785.29    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mm');
        var path_mn = rsr.path("M 1126.982,728.899 1164.082,728.26     1163.842,709.896 1126.723,710.537    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mn');
        var path_mo = rsr.path("M 1127.776,784.955 1164.817,784.316     1164.576,765.964 1127.516,766.605    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mo');
        var path_mp = rsr.path("M 1127.247,747.586 1164.327,746.944     1164.087,728.593 1126.988,729.233    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mp');
        var path_mq = rsr.path("M 1126.718,710.204 1163.837,709.562     1163.598,691.216 1126.459,691.857    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mq');
        var path_mr = rsr.path("M 1126.189,672.844 1163.348,672.202     1163.107,653.856 1125.93,654.498    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mr');
        var path_ms = rsr.path("M 1125.926,654.165 1163.104,653.522     1162.862,635.177 1125.666,635.819    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ms');
        var path_mt = rsr.path("M 1126.453,691.524 1163.592,690.881     1163.352,672.535 1126.194,673.178    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mt');
        var path_mu = rsr.path("M 1127.711,803.975 1090.451,804.619     1090.698,822.638 1127.966,821.992    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mu');
        var path_mv = rsr.path("M 1127.448,785.295 1090.195,785.939     1090.446,804.285 1127.707,803.641    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mv');
        var path_mw = rsr.path("M 1127.183,766.61 1089.939,767.254     1090.19,785.605 1127.442,784.961    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mw');
        var path_mx = rsr.path("M 1127.512,766.271 1164.572,765.63     1164.332,747.277 1127.252,747.919    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mx');
        var path_my = rsr.path("M 1126.918,747.925 1089.684,748.568     1089.935,766.92 1127.178,766.276    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_my');
        var path_mz = rsr.path("M 1126.653,729.24 1089.428,729.883     1089.679,748.235 1126.914,747.591    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_mz');
        group_m.attr({'parent': 'Layer_18','name': 'group_m'});
        var group_n = rsr.set();
        var path_na = rsr.path("M 1314.795,819.496 1351.798,818.857     1351.563,800.838 1314.541,801.479    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_na');
        var path_nb = rsr.path("M 1312.885,708.053 1275.666,708.695     1275.918,727.059 1313.145,726.416    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nb');
        var path_nc = rsr.path("M 1311.828,633.335 1274.643,633.979     1274.894,652.324 1312.087,651.681    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nc');
        var path_nd = rsr.path("M 1312.355,670.693 1275.154,671.337     1275.406,689.683 1312.615,689.039    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nd');
        var path_ne = rsr.path("M 1312.092,652.015 1274.898,652.658     1275.15,671.002 1312.351,670.36    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ne');
        var path_nf = rsr.path("M 1312.62,689.373 1275.41,690.017     1275.662,708.362 1312.881,707.719    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nf');
        var path_ng = rsr.path("M 1314.535,801.145 1351.557,800.505     1351.317,782.159 1314.276,782.801    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ng');
        var path_nh = rsr.path("M 1313.479,726.41 1350.577,725.77     1350.338,707.404 1313.219,708.047    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nh');
        var path_ni = rsr.path("M 1314.271,782.466 1351.313,781.825     1351.072,763.473 1314.011,764.114    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ni');
        var path_nj = rsr.path("M 1313.743,745.095 1350.823,744.455     1350.582,726.103 1313.482,726.744    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nj');
        var path_nk = rsr.path("M 1313.214,707.714 1350.333,707.071     1350.092,688.726 1312.954,689.367    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nk');
        var path_nl = rsr.path("M 1312.686,670.354 1349.843,669.711     1349.603,651.365 1312.425,652.009    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nl');
        var path_nm = rsr.path("M 1312.42,651.674 1349.598,651.032     1349.357,632.686 1312.16,633.328    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nm');
        var path_nn = rsr.path("M 1312.948,689.033 1350.088,688.392     1349.848,670.046 1312.689,670.688    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nn');
        var path_no = rsr.path("M 1314.207,801.484 1276.945,802.129     1277.192,820.147 1314.461,819.503    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_no');
        var path_np = rsr.path("M 1313.942,782.805 1276.69,783.449     1276.941,801.795 1314.201,801.15    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_np');
        var path_nq = rsr.path("M 1313.677,764.12 1276.435,764.764     1276.686,783.115 1313.938,782.473    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nq');
        var path_nr = rsr.path("M 1314.006,763.781 1351.068,763.14     1350.827,744.787 1313.747,745.429    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nr');
        var path_ns = rsr.path("M 1313.414,745.436 1276.179,746.078     1276.431,764.431 1313.673,763.786    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ns');
        var path_nt = rsr.path("M 1313.149,726.749 1275.922,727.394     1276.174,745.745 1313.408,745.102    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nt');
        group_n.attr({'parent': 'Layer_18','name': 'group_n'});
        var group_o = rsr.set();
        var path_nu = rsr.path("M 1122.917,448.91 1122.392,411.764     1104.305,412.086 1104.574,449.227    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nu');
        var path_nv = rsr.path("M 1103.973,412.093 1085.225,412.428     1085.732,449.553 1104.241,449.233    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nv');
        var path_nw = rsr.path("M 1141.774,448.584 1141.287,411.426     1122.727,411.757 1123.251,448.904    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nw');
        var path_nx = rsr.path("M 1123.979,523.962 1086.761,524.605     1087.013,542.969 1124.239,542.325    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nx');
        var path_ny = rsr.path("M 1122.922,449.244 1085.737,449.887     1085.989,468.233 1123.182,467.59    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ny');
        var path_nz = rsr.path("M 1123.45,486.604 1086.249,487.246     1086.501,505.592 1123.711,504.949    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_nz');
        var path_oa = rsr.path("M 1123.186,467.923 1085.993,468.566     1086.245,486.912 1123.446,486.27    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oa');
        var path_ob = rsr.path("M 1123.715,505.282 1086.505,505.926     1086.757,524.271 1123.974,523.629    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ob');
        var path_oc = rsr.path("M 1125.63,617.053 1162.652,616.414     1162.412,598.068 1125.371,598.709    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oc');
        var path_od = rsr.path("M 1160.447,448.261 1159.961,411.092     1141.621,411.419 1142.108,448.578    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_od');
        var path_oe = rsr.path("M 1124.572,542.318 1161.673,541.678     1161.432,523.314 1124.313,523.957    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oe');
        var path_of = rsr.path("M 1125.366,598.375 1162.406,597.734     1162.167,579.383 1125.106,580.023    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_of');
        var path_og = rsr.path("M 1124.836,561.004 1161.918,560.363     1161.676,542.012 1124.577,542.652    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_og');
        var path_oh = rsr.path("M 1124.309,523.623 1161.428,522.98     1161.188,504.634 1124.049,505.277    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oh');
        var path_oi = rsr.path("M 1123.779,486.264 1160.938,485.621     1160.698,467.275 1123.52,467.917    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oi');
        var path_oj = rsr.path("M 1123.516,467.584 1160.693,466.941     1160.451,448.595 1123.256,449.238    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oj');
        var path_ok = rsr.path("M 1124.043,504.942 1161.182,504.301     1160.941,485.954 1123.784,486.597    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ok');
        var path_ol = rsr.path("M 1125.037,598.714 1087.785,599.358     1088.037,617.704 1125.297,617.06    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ol');
        var path_om = rsr.path("M 1124.772,580.029 1087.529,580.673     1087.78,599.025 1125.032,598.381    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_om');
        var path_on = rsr.path("M 1125.102,579.69 1162.162,579.049     1161.922,560.697 1124.842,561.338    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_on');
        var path_oo = rsr.path("M 1124.508,561.344 1087.273,561.988     1087.524,580.339 1124.769,579.695    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oo');
        var path_op = rsr.path("M 1124.244,542.658 1087.017,543.303     1087.269,561.654 1124.503,561.01    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_op');
        group_o.attr({'parent': 'Layer_18','name': 'group_o'});
        var group_p = rsr.set();
        var path_oq = rsr.path("M 1216.297,447.658 1215.771,410.512     1197.685,410.834 1197.954,447.975    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oq');
        var path_or = rsr.path("M 1197.351,410.841 1178.604,411.174     1179.111,448.301 1197.621,447.981    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_or');
        var path_os = rsr.path("M 1235.154,447.332 1234.666,410.174     1216.104,410.506 1216.629,447.652    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_os');
        var path_ot = rsr.path("M 1217.357,522.71 1180.141,523.354     1180.391,541.716 1217.618,541.072    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ot');
        var path_ou = rsr.path("M 1216.301,447.992 1179.117,448.635     1179.367,466.981 1216.561,466.338    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ou');
        var path_ov = rsr.path("M 1216.83,485.351 1179.629,485.994     1179.879,504.34 1217.089,503.697    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ov');
        var path_ow = rsr.path("M 1216.565,466.672 1179.372,467.314     1179.623,485.66 1216.824,485.018    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ow');
        var path_ox = rsr.path("M 1217.094,504.03 1179.884,504.674     1180.137,523.02 1217.354,522.376    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ox');
        var path_oy = rsr.path("M 1253.827,447.009 1253.339,409.84     1235,410.167 1235.486,447.326    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oy');
        var path_oz = rsr.path("M 1217.951,541.067 1255.051,540.426     1254.811,522.063 1217.691,522.703    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_oz');
        var path_pa = rsr.path("M 1217.688,522.371 1254.807,521.729     1254.566,503.381 1217.427,504.025    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pa');
        var path_pb = rsr.path("M 1217.158,485.012 1254.316,484.368     1254.076,466.022 1216.898,466.666    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pb');
        var path_pc = rsr.path("M 1216.895,466.332 1254.072,465.688     1253.831,447.342 1216.635,447.985    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pc');
        var path_pd = rsr.path("M 1217.423,503.69 1254.561,503.049     1254.32,484.702 1217.164,485.345    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pd');
        group_p.attr({'parent': 'Layer_18','name': 'group_p'});
        var group_q = rsr.set();
        var path_pe = rsr.path("M 1309.591,446.425 1309.066,409.277     1290.979,409.602 1291.25,446.742    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pe');
        var path_pf = rsr.path("M 1290.646,409.606 1271.898,409.941     1272.407,447.068 1290.915,446.747    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pf');
        var path_pg = rsr.path("M 1328.448,446.099 1327.962,408.94     1309.4,409.272 1309.926,446.419    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pg');
        var path_ph = rsr.path("M 1310.653,521.477 1273.435,522.12     1273.688,540.483 1310.913,539.84    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ph');
        var path_pi = rsr.path("M 1309.597,446.758 1272.411,447.401     1272.663,465.747 1309.855,465.104    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pi');
        var path_pj = rsr.path("M 1310.124,484.118 1272.923,484.761     1273.176,503.107 1310.385,502.463    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pj');
        var path_pk = rsr.path("M 1309.861,465.438 1272.668,466.081     1272.919,484.427 1310.12,483.783    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pk');
        var path_pl = rsr.path("M 1310.39,502.797 1273.18,503.441     1273.431,521.787 1310.648,521.144    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pl');
        var path_pm = rsr.path("M 1312.305,614.569 1349.327,613.93     1349.086,595.582 1312.045,596.224    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pm');
        var path_pn = rsr.path("M 1347.123,445.775 1346.635,408.607     1328.295,408.935 1328.783,446.094    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pn');
        var path_po = rsr.path("M 1311.247,539.834 1348.347,539.192     1348.105,520.829 1310.986,521.471    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_po');
        var path_pp = rsr.path("M 1312.041,595.89 1349.082,595.248     1348.841,576.897 1311.78,577.537    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pp');
        var path_pq = rsr.path("M 1311.512,558.52 1348.592,557.878     1348.352,539.525 1311.251,540.168    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pq');
        var path_pr = rsr.path("M 1310.982,521.137 1348.102,520.494     1347.861,502.148 1310.723,502.791    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pr');
        var path_ps = rsr.path("M 1310.453,483.778 1347.612,483.136     1347.372,464.79 1310.193,465.432    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ps');
        var path_pt = rsr.path("M 1310.189,465.099 1347.367,464.456     1347.127,446.109 1309.93,446.753    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pt');
        var path_pu = rsr.path("M 1310.718,502.457 1347.856,501.814     1347.617,483.469 1310.459,484.111    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pu');
        var path_pv = rsr.path("M 1311.711,596.229 1274.459,596.872     1274.711,615.219 1311.972,614.574    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pv');
        var path_pw = rsr.path("M 1311.447,577.544 1274.203,578.188     1274.455,596.54 1311.707,595.896    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pw');
        var path_px = rsr.path("M 1311.775,577.204 1348.837,576.564     1348.598,558.212 1311.517,558.853    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_px');
        var path_py = rsr.path("M 1311.184,558.858 1273.947,559.503     1274.199,577.854 1311.442,577.209    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_py');
        var path_pz = rsr.path("M 1310.918,540.174 1273.691,540.816     1273.943,559.168 1311.178,558.524    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_pz');
        group_q.attr({'parent': 'Layer_18','name': 'group_q'});
        var group_r = rsr.set();
        var path_qa = rsr.path("M 718.198,715.688 680.98,716.33     681.23,734.694 718.458,734.05    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qa');
        var path_qb = rsr.path("M 717.141,640.97 679.957,641.613     680.207,659.959 717.399,659.315    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qb');
        var path_qc = rsr.path("M 717.67,678.328 680.469,678.972     680.719,697.317 717.929,696.674    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qc');
        var path_qd = rsr.path("M 717.405,659.648 680.212,660.292     680.464,678.639 717.664,677.994    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qd');
        var path_qe = rsr.path("M 717.934,697.009 680.725,697.652     680.976,715.998 718.194,715.354    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qe');
        var path_qf = rsr.path("M 719.85,808.78 756.871,808.141     756.631,789.793 719.589,790.435    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qf');
        var path_qg = rsr.path("M 718.792,734.045 755.891,733.403     755.651,715.041 718.531,715.682    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qg');
        var path_qh = rsr.path("M 719.585,790.1 756.626,789.46     756.387,771.107 719.324,771.75    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qh');
        var path_qi = rsr.path("M 719.056,752.73 756.136,752.088     755.896,733.737 718.797,734.379    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qi');
        var path_qj = rsr.path("M 718.527,715.348 755.646,714.706     755.406,696.359 718.268,697.002    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qj');
        var path_qk = rsr.path("M 717.998,677.989 755.157,677.346     754.916,659 717.738,659.644    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qk');
        var path_ql = rsr.path("M 717.734,659.311 754.912,658.666     754.671,640.32 717.475,640.964    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ql');
        var path_qm = rsr.path("M 718.263,696.669 755.402,696.026     755.161,677.681 718.003,678.323    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qm');
        var path_qn = rsr.path("M 719.257,790.44 682.004,791.085     682.255,809.431 719.516,808.786    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qn');
        var path_qo = rsr.path("M 718.991,771.756 681.749,772.398     681.999,790.75 719.251,790.106    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qo');
        var path_qp = rsr.path("M 719.32,771.415 756.381,770.774     756.141,752.422 719.061,753.064    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qp');
        var path_qq = rsr.path("M 718.728,753.069 681.492,753.713     681.744,772.065 718.986,771.421    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qq');
        var path_qr = rsr.path("M 718.463,734.385 681.236,735.028     681.487,753.381 718.723,752.736    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qr');
        group_r.attr({'parent': 'Layer_18','name': 'group_r'});
        var group_s = rsr.set();
        var path_qs = rsr.path("M 624.733,716.965 587.516,717.608     587.767,735.972 624.993,735.328    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qs');
        var path_qt = rsr.path("M 623.676,642.246 586.492,642.891     586.742,661.236 623.935,660.594    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qt');
        var path_qu = rsr.path("M 624.205,679.606 587.004,680.25     587.255,698.596 624.464,697.952    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qu');
        var path_qv = rsr.path("M 623.939,660.926 586.747,661.569     586.998,679.915 624.199,679.271    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qv');
        var path_qw = rsr.path("M 624.468,698.286 587.259,698.93     587.51,717.275 624.729,716.632    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qw');
        var path_qx = rsr.path("M 626.385,810.057 663.406,809.418     663.166,791.072 626.124,791.712    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qx');
        var path_qy = rsr.path("M 625.326,735.322 662.427,734.681     662.186,716.317 625.066,716.959    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qy');
        var path_qz = rsr.path("M 626.12,791.377 663.161,790.737     662.922,772.385 625.859,773.027    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_qz');
        var path_ra = rsr.path("M 625.591,754.008 662.671,753.365     662.431,735.014 625.332,735.655    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ra');
        var path_rb = rsr.path("M 625.062,716.626 662.181,715.982     661.941,697.637 624.803,698.28    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rb');
        var path_rc = rsr.path("M 624.533,679.267 661.691,678.624     661.451,660.278 624.273,660.921    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rc');
        var path_rd = rsr.path("M 624.269,660.587 661.447,659.943     661.206,641.598 624.01,642.241    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rd');
        var path_re = rsr.path("M 624.798,697.945 661.936,697.304     661.696,678.958 624.537,679.6    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_re');
        var path_rf = rsr.path("M 625.791,791.717 588.539,792.361     588.79,810.707 626.051,810.063    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rf');
        var path_rg = rsr.path("M 625.526,773.032 588.284,773.676     588.534,792.028 625.786,791.383    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rg');
        var path_rh = rsr.path("M 625.855,772.693 662.916,772.053     662.676,753.7 625.596,754.342    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rh');
        var path_ri = rsr.path("M 625.263,754.347 588.027,754.991     588.278,773.342 625.521,772.697    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ri');
        var path_rj = rsr.path("M 624.997,735.662 587.771,736.305     588.022,754.656 625.258,754.014    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rj');
        group_s.attr({'parent': 'Layer_18','name': 'group_s'});
        var group_t = rsr.set();
        var path_rk = rsr.path("M 531.326,718.277 494.107,718.92     494.359,737.283 531.586,736.64    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rk');
        var path_rl = rsr.path("M 530.798,680.917 493.597,681.561     493.848,699.906 531.058,699.263    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rl');
        var path_rm = rsr.path("M 531.062,699.597 493.852,700.24     494.104,718.587 531.322,717.943    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rm');
        var path_rn = rsr.path("M 532.978,811.368 570,810.729     569.759,792.383 532.718,793.023    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rn');
        var path_ro = rsr.path("M 531.92,736.634 569.02,735.992     568.778,717.629 531.66,718.271    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ro');
        var path_rp = rsr.path("M 532.713,792.689 569.755,792.049     569.514,773.696 532.453,774.338    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rp');
        var path_rq = rsr.path("M 532.185,755.318 569.265,754.678     569.023,736.326 531.924,736.967    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rq');
        var path_rr = rsr.path("M 531.655,717.937 568.774,717.296     568.533,698.949 531.396,699.591    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rr');
        var path_rs = rsr.path("M 531.391,699.258 568.529,698.615     568.289,680.27 531.132,680.912    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rs');
        var path_rt = rsr.path("M 532.384,793.029 495.132,793.672     495.383,812.018 532.645,811.375    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rt');
        var path_ru = rsr.path("M 532.119,774.343 494.876,774.987     495.128,793.339 532.379,792.695    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ru');
        var path_rv = rsr.path("M 532.449,774.004 569.51,773.363     569.269,755.012 532.188,755.652    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rv');
        var path_rw = rsr.path("M 531.855,755.658 494.62,756.302     494.872,774.654 532.115,774.01    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rw');
        var path_rx = rsr.path("M 531.591,736.974 494.364,737.617     494.616,755.969 531.851,755.324    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rx');
        group_t.attr({'parent': 'Layer_18','name': 'group_t'});
        var group_u = rsr.set();
        var path_ry = rsr.path("M 406.085,458.498 405.559,421.352     387.473,421.674 387.743,458.815    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ry');
        var path_rz = rsr.path("M 387.14,421.681 368.393,422.016     368.9,459.142 387.408,458.822    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_rz');
        var path_sa = rsr.path("M 424.942,458.172 424.455,421.015     405.893,421.346 406.418,458.493    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sa');
        var path_sb = rsr.path("M 407.146,533.551 369.929,534.193     370.18,552.557 407.407,551.914    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sb');
        var path_sc = rsr.path("M 406.09,458.832 368.905,459.475     369.156,477.821 406.35,477.179    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sc');
        var path_sd = rsr.path("M 406.618,496.19 369.417,496.834     369.668,515.18 406.878,514.536    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sd');
        var path_se = rsr.path("M 406.354,477.512 369.161,478.154     369.412,496.5 406.613,495.857    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_se');
        var path_sf = rsr.path("M 406.883,514.87 369.673,515.514     369.924,533.859 407.142,533.216    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sf');
        var path_sg = rsr.path("M 408.799,626.642 445.82,626.003     445.58,607.657 408.539,608.297    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sg');
        var path_sh = rsr.path("M 443.616,457.85 443.128,420.68     424.789,421.008 425.275,458.167    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sh');
        var path_si = rsr.path("M 407.74,551.907 444.841,551.266     444.6,532.902 407.48,533.544    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_si');
        var path_sj = rsr.path("M 408.533,607.963 445.574,607.322     445.335,588.97 408.274,589.611    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sj');
        var path_sk = rsr.path("M 408.004,570.592 445.084,569.95     444.845,551.6 407.745,552.24    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sk');
        var path_sl = rsr.path("M 407.476,533.211 444.596,532.568     444.354,514.223 407.216,514.864    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sl');
        var path_sm = rsr.path("M 406.947,495.852 444.105,495.209     443.864,476.862 406.688,477.506    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sm');
        var path_sn = rsr.path("M 406.683,477.172 443.861,476.529     443.62,458.184 406.424,458.826    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sn');
        var path_so = rsr.path("M 407.212,514.531 444.351,513.889     444.109,495.542 406.951,496.186    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_so');
        var path_sp = rsr.path("M 408.205,608.302 370.953,608.945     371.205,627.292 408.465,626.648    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sp');
        var path_sq = rsr.path("M 407.94,589.617 370.697,590.262     370.947,608.612 408.201,607.968    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sq');
        var path_sr = rsr.path("M 408.27,589.277 445.33,588.638     445.09,570.284 408.01,570.927    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sr');
        var path_ss = rsr.path("M 407.676,570.932 370.441,571.575     370.691,589.928 407.936,589.283    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ss');
        var path_st = rsr.path("M 407.411,552.246 370.185,552.891     370.436,571.242 407.672,570.599    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_st');
        group_u.attr({'parent': 'Layer_18','name': 'group_u'});
        var group_v = rsr.set();
        var path_su = rsr.path("M 409.517,720.375 372.299,721.02     372.551,739.382 409.776,738.737    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_su');
        var path_sv = rsr.path("M 408.46,645.657 371.275,646.301     371.526,664.646 408.719,664.003    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sv');
        var path_sw = rsr.path("M 408.988,683.016 371.787,683.659     372.038,702.005 409.248,701.361    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sw');
        var path_sx = rsr.path("M 408.725,664.336 371.53,664.979     371.783,683.325 408.983,682.682    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sx');
        var path_sy = rsr.path("M 409.252,701.696 372.042,702.34     372.294,720.686 409.513,720.042    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sy');
        var path_sz = rsr.path("M 411.168,813.467 448.19,812.828     447.949,794.48 410.908,795.122    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_sz');
        var path_ta = rsr.path("M 410.11,738.732 447.21,738.091     446.969,719.728 409.851,720.369    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ta');
        var path_tb = rsr.path("M 410.904,794.787 447.945,794.147     447.704,775.796 410.644,776.437    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tb');
        var path_tc = rsr.path("M 410.375,757.417 447.455,756.776     447.215,738.425 410.115,739.065    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tc');
        var path_td = rsr.path("M 409.846,720.035 446.965,719.395     446.725,701.047 409.587,701.689    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_td');
        var path_te = rsr.path("M 409.316,682.677 446.475,682.033     446.235,663.688 409.058,664.331    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_te');
        var path_tf = rsr.path("M 409.054,663.997 446.23,663.354     445.989,645.008 408.793,645.651    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tf');
        var path_tg = rsr.path("M 409.581,701.356 446.72,700.714     446.479,682.368 409.322,683.01    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tg');
        var path_th = rsr.path("M 410.576,795.127 373.322,795.771     373.574,814.118 410.835,813.473    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_th');
        var path_ti = rsr.path("M 410.311,776.443 373.066,777.086     373.318,795.438 410.57,794.794    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ti');
        var path_tj = rsr.path("M 410.64,776.103 447.7,775.463     447.459,757.109 410.379,757.75    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tj');
        var path_tk = rsr.path("M 410.046,757.757 372.811,758.4     373.063,776.753 410.306,776.109    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tk');
        var path_tl = rsr.path("M 409.781,739.071 372.555,739.716     372.807,758.068 410.041,757.424    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tl');
        group_v.attr({'parent': 'Layer_18','name': 'group_v'});
        var group_w = rsr.set();
        var path_tm = rsr.path("M 312.828,459.593 312.303,422.445     294.216,422.769 294.485,459.91    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tm');
        var path_tn = rsr.path("M 293.883,422.775 275.136,423.109     275.645,460.236 294.153,459.916    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tn');
        var path_to = rsr.path("M 331.687,459.267 331.199,422.109     312.637,422.44 313.162,459.588    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_to');
        var path_tp = rsr.path("M 313.891,534.645 276.672,535.289     276.924,553.651 314.15,553.008    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tp');
        var path_tq = rsr.path("M 312.834,459.927 275.648,460.57     275.9,478.916 313.093,478.272    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tq');
        var path_tr = rsr.path("M 313.361,497.285 276.16,497.929     276.412,516.274 313.622,515.631    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tr');
        var path_ts = rsr.path("M 313.097,478.605 275.904,479.249     276.156,497.596 313.357,496.951    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ts');
        var path_tt = rsr.path("M 313.626,515.965 276.416,516.608     276.668,534.955 313.885,534.311    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tt');
        var path_tu = rsr.path("M 315.541,627.737 352.563,627.096     352.323,608.75 315.282,609.391    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tu');
        var path_tv = rsr.path("M 350.359,458.944 349.872,421.775     331.532,422.103 332.02,459.262    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tv');
        var path_tw = rsr.path("M 314.484,553.002 351.584,552.36     351.343,533.997 314.224,534.639    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tw');
        var path_tx = rsr.path("M 315.277,609.059 352.319,608.417     352.078,590.064 315.018,590.706    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tx');
        var path_ty = rsr.path("M 314.749,571.688 351.829,571.047     351.588,552.694 314.488,553.336    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ty');
        var path_tz = rsr.path("M 314.22,534.305 351.339,533.662     351.098,515.316 313.96,515.959    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_tz');
        var path_ua = rsr.path("M 313.69,496.946 350.849,496.304     350.609,477.957 313.431,478.601    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ua');
        var path_ub = rsr.path("M 313.426,478.267 350.604,477.623     350.364,459.277 313.167,459.921    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ub');
        var path_uc = rsr.path("M 313.955,515.626 351.094,514.983     350.854,496.638 313.695,497.279    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uc');
        var path_ud = rsr.path("M 314.948,609.397 277.696,610.041     277.948,628.387 315.209,627.743    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ud');
        var path_ue = rsr.path("M 314.684,590.711 277.44,591.355     277.692,609.708 314.943,609.063    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ue');
        var path_uf = rsr.path("M 315.012,590.372 352.073,589.73     351.833,571.38 314.753,572.021    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uf');
        var path_ug = rsr.path("M 314.42,572.026 277.185,572.671     277.436,591.022 314.68,590.378    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ug');
        var path_uh = rsr.path("M 314.155,553.342 276.928,553.985     277.181,572.336 314.414,571.693    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uh');
        group_w.attr({'parent': 'Layer_18','name': 'group_w'});
        var group_x = rsr.set();
        var path_ui = rsr.path("M 316.26,721.469 279.042,722.112     279.294,740.477 316.521,739.833    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ui');
        var path_uj = rsr.path("M 315.203,646.752 278.019,647.395     278.27,665.74 315.463,665.098    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uj');
        var path_uk = rsr.path("M 315.732,684.111 278.531,684.754     278.781,703.101 315.991,702.456    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uk');
        var path_ul = rsr.path("M 315.468,665.432 278.275,666.075     278.526,684.42 315.727,683.777    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ul');
        var path_um = rsr.path("M 315.997,702.791 278.787,703.435     279.037,721.78 316.256,721.136    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_um');
        var path_un = rsr.path("M 317.912,814.563 354.934,813.922     354.693,795.576 317.652,796.217    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_un');
        var path_uo = rsr.path("M 316.854,739.826 353.953,739.186     353.713,720.822 316.594,721.464    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uo');
        var path_up = rsr.path("M 317.646,795.882 354.688,795.242     354.448,776.891 317.387,777.531    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_up');
        var path_uq = rsr.path("M 317.118,758.513 354.198,757.871     353.958,739.52 316.858,740.16    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uq');
        var path_ur = rsr.path("M 316.59,721.13 353.708,720.488     353.468,702.143 316.33,702.784    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ur');
        var path_us = rsr.path("M 316.062,683.771 353.22,683.129     352.978,664.782 315.801,665.426    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_us');
        var path_ut = rsr.path("M 315.796,665.092 352.975,664.449     352.733,646.103 315.537,646.746    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ut');
        var path_uu = rsr.path("M 316.325,702.451 353.463,701.809     353.223,683.463 316.065,684.105    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uu');
        var path_uv = rsr.path("M 317.318,796.223 280.066,796.867     280.317,815.213 317.578,814.568    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uv');
        var path_uw = rsr.path("M 317.054,777.536 279.811,778.181     280.062,796.533 317.313,795.889    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uw');
        var path_ux = rsr.path("M 317.383,777.197 354.443,776.557     354.203,758.205 317.124,758.846    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ux');
        var path_uy = rsr.path("M 316.789,758.852 279.555,759.496     279.806,777.848 317.049,777.203    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uy');
        var path_uz = rsr.path("M 316.524,740.167 279.299,740.811     279.55,759.162 316.785,758.518    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_uz');
        group_x.attr({'parent': 'Layer_18','name': 'group_x'});
        var group_y = rsr.set();
        var path_va = rsr.path("M 106.811,723.787 143.794,723.147     143.55,704.462 106.547,705.102    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_va');
        var path_vb = rsr.path("M 106.542,704.768 143.545,704.129     143.31,686.109 106.287,686.75    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vb');
        var path_vc = rsr.path("M 104.631,593.324 67.413,593.969     67.664,612.331 104.892,611.688    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vc');
        var path_vd = rsr.path("M 103.573,518.606 66.39,519.248     66.641,537.596 103.834,536.952    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vd');
        var path_ve = rsr.path("M 104.104,555.965 66.901,556.608     67.152,574.954 104.362,574.311    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ve');
        var path_vf = rsr.path("M 103.838,537.285 66.646,537.93     66.896,556.275 104.098,555.631    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vf');
        var path_vg = rsr.path("M 104.367,574.645 67.157,575.288     67.408,593.634 104.626,592.99    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vg');
        var path_vh = rsr.path("M 106.282,686.416 143.305,685.776     143.064,667.43 106.022,668.07    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vh');
        var path_vi = rsr.path("M 105.225,611.682 142.324,611.04     142.084,592.678 104.965,593.318    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vi');
        var path_vj = rsr.path("M 106.018,667.736 143.06,667.097     142.819,648.744 105.759,649.386    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vj');
        var path_vk = rsr.path("M 105.489,630.366 142.57,629.725     142.329,611.374 105.229,612.016    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vk');
        var path_vl = rsr.path("M 104.961,592.984 142.08,592.343     141.839,573.996 104.7,574.639    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vl');
        var path_vm = rsr.path("M 104.432,555.625 141.59,554.983     141.349,536.637 104.172,537.279    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vm');
        var path_vn = rsr.path("M 104.167,536.947 141.345,536.303     141.104,517.957 103.907,518.601    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vn');
        var path_vo = rsr.path("M 104.696,574.306 141.835,573.663     141.594,555.316 104.436,555.959    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vo');
        var path_vp = rsr.path("M 105.954,686.755 68.693,687.399     68.94,705.418 106.209,704.773    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vp');
        var path_vq = rsr.path("M 105.689,668.077 68.438,668.721     68.689,687.066 105.948,686.423    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vq');
        var path_vr = rsr.path("M 106.213,705.107 68.945,705.752     69.201,724.438 106.478,723.792    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vr');
        var path_vs = rsr.path("M 105.425,649.391 68.182,650.035     68.434,668.387 105.684,667.743    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vs');
        var path_vt = rsr.path("M 105.753,649.052 142.814,648.411     142.574,630.06 105.494,630.7    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vt');
        var path_vu = rsr.path("M 105.16,630.705 67.925,631.351     68.176,649.702 105.421,649.058    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vu');
        var path_vv = rsr.path("M 104.896,612.021 67.669,612.665     67.92,631.016 105.156,630.373    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vv');
        group_y.attr({'parent': 'Layer_18','name': 'group_y'});
        var group_z = rsr.set();
        var path_vw = rsr.path("M 162.401,723.175 199.384,722.535     199.141,703.85 162.137,704.49    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vw');
        var path_vx = rsr.path("M 162.133,704.156 199.135,703.517     198.899,685.498 161.877,686.138    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vx');
        var path_vy = rsr.path("M 161.872,685.805 198.895,685.164     198.654,666.818 161.613,667.458    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vy');
        var path_vz = rsr.path("M 160.815,611.069 197.915,610.427     197.674,592.064 160.555,592.705    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_vz');
        var path_wa = rsr.path("M 161.608,667.125 198.65,666.483     198.409,648.133 161.349,648.772    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wa');
        var path_wb = rsr.path("M 161.08,629.755 198.16,629.113     197.919,610.762 160.819,611.402    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wb');
        var path_wc = rsr.path("M 160.551,592.371 197.67,591.73     197.429,573.385 160.291,574.027    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wc');
        var path_wd = rsr.path("M 160.021,555.014 197.18,554.371     196.94,536.024 159.762,536.668    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wd');
        var path_we = rsr.path("M 159.758,536.334 196.935,535.691     196.694,517.346 159.497,517.988    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_we');
        var path_wf = rsr.path("M 160.286,573.692 197.424,573.051     197.184,554.704 160.026,555.348    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wf');
        var path_wg = rsr.path("M 161.343,648.439 198.404,647.799     198.164,629.446 161.084,630.088    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wg');
        group_z.attr({'parent': 'Layer_18','name': 'group_z'});
        var group_aa = rsr.set();
        var path_wh = rsr.path("M 69.93,780.648 70.516,817.652     88.534,817.443 87.947,780.42    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wh');
        var path_wi = rsr.path("M 181.376,778.896 180.786,741.678     162.422,741.902 163.012,779.13    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wi');
        var path_wj = rsr.path("M 256.095,777.945 255.506,740.761     237.16,740.985 237.749,778.18    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wj');
        var path_wk = rsr.path("M 218.736,778.422 218.145,741.22     199.799,741.445 200.389,778.654    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wk');
        var path_wl = rsr.path("M 237.414,778.184 236.825,740.989     218.479,741.214 219.068,778.417    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wl');
        var path_wm = rsr.path("M 200.056,778.659 199.465,741.449     181.119,741.674 181.71,778.893    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wm');
        var path_wn = rsr.path("M 88.281,780.416 88.869,817.438     107.215,817.225 106.627,780.183    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wn');
        var path_wo = rsr.path("M 163.018,779.465 163.605,816.564     181.97,816.351 181.382,779.23    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wo');
        var path_wp = rsr.path("M 106.96,780.177 107.549,817.221     125.9,817.006 125.313,779.945    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wp');
        var path_wq = rsr.path("M 144.332,779.702 144.92,816.783     163.272,816.569 162.684,779.469    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wq');
        var path_wr = rsr.path("M 181.714,779.226 182.304,816.346     200.65,816.133 200.061,778.992    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wr');
        var path_ws = rsr.path("M 219.075,778.75 219.663,815.91     238.01,815.695 237.421,778.517    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ws');
        var path_wt = rsr.path("M 237.754,778.514 238.345,815.691     256.69,815.476 256.101,778.279    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wt');
        var path_wu = rsr.path("M 200.395,778.988 200.984,816.128     219.33,815.914 218.74,778.755    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wu');
        var path_wv = rsr.path("M 87.941,780.085 87.352,742.824     69.332,743.046 69.924,780.315    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wv');
        var path_ww = rsr.path("M 106.622,779.849 106.03,742.596     87.685,742.82 88.276,780.082    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ww');
        var path_wx = rsr.path("M 125.308,779.61 124.717,742.367     106.365,742.592 106.955,779.845    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wx');
        var path_wy = rsr.path("M 125.646,779.939 126.234,817.001     144.586,816.787 143.999,779.706    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wy');
        var path_wz = rsr.path("M 143.992,779.373 143.402,742.137     125.051,742.361 125.641,779.607    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_wz');
        var path_xa = rsr.path("M 162.679,779.134 162.088,741.907     143.735,742.133 144.326,779.368    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xa');
        group_aa.attr({'parent': 'Layer_18','name': 'group_aa'});
        var group_ab = rsr.set();
        var path_xb = rsr.path("M 199.491,685.491 200.077,722.494     218.097,722.285 217.51,685.262    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xb');
        var path_xc = rsr.path("M 217.844,685.258 218.431,722.281     236.777,722.067 236.189,685.024    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xc');
        var path_xd = rsr.path("M 236.522,685.02 237.111,722.063     255.463,721.848 254.875,684.787    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xd');
        var path_xe = rsr.path("M 217.504,684.928 216.913,647.667     198.895,647.889 199.485,685.157    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xe');
        var path_xf = rsr.path("M 236.184,684.691 235.592,647.438     217.246,647.663 217.838,684.924    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xf');
        var path_xg = rsr.path("M 254.869,684.453 254.279,647.209     235.927,647.434 236.518,684.687    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xg');
        group_ab.attr({'parent': 'Layer_18','name': 'group_ab'});
        var group_ac = rsr.set();
        var path_xh = rsr.path("M 198.09,554.996 198.676,592.057     216.682,591.847 216.096,554.767    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xh');
        var path_xi = rsr.path("M 216.429,554.764 217.016,591.843     235.35,591.628 234.762,554.53    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xi');
        var path_xj = rsr.path("M 235.096,554.525 235.684,591.623     254.023,591.408 253.435,554.292    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xj');
        var path_xk = rsr.path("M 216.09,554.434 215.5,517.115     197.493,517.337 198.084,554.662    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xk');
        var path_xl = rsr.path("M 234.757,554.195 234.166,516.886     215.832,517.111 216.424,554.429    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xl');
        var path_xm = rsr.path("M 253.431,553.957 252.839,516.656     234.5,516.881 235.09,554.191    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xm');
        group_ac.attr({'parent': 'Layer_18','name': 'group_ac'});
        var group_ad = rsr.set();
        var path_xn = rsr.path("M 65.753,463.496 66.34,500.499     84.358,500.289 83.771,463.267    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xn');
        var path_xo = rsr.path("M 177.199,461.743 176.609,424.525     158.246,424.75 158.836,461.977    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xo');
        var path_xp = rsr.path("M 251.919,460.792 251.328,423.607     232.982,423.832 233.572,461.025    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xp');
        var path_xq = rsr.path("M 214.56,461.268 213.969,424.066     195.623,424.292 196.214,461.5    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xq');
        var path_xr = rsr.path("M 233.238,461.029 232.649,423.836     214.303,424.062 214.893,461.264    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xr');
        var path_xs = rsr.path("M 195.88,461.505 195.288,424.295     176.943,424.521 177.534,461.739    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xs');
        var path_xt = rsr.path("M 84.104,463.262 84.693,500.284     103.039,500.07 102.451,463.028    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xt');
        var path_xu = rsr.path("M 158.842,462.312 159.43,499.411     177.793,499.197 177.205,462.077    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xu');
        var path_xv = rsr.path("M 102.784,463.023 103.373,500.066     121.725,499.853 121.137,462.791    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xv');
        var path_xw = rsr.path("M 140.156,462.549 140.744,499.629     159.097,499.415 158.508,462.314    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xw');
        var path_xx = rsr.path("M 177.538,462.072 178.128,499.193     196.475,498.979 195.885,461.838    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xx');
        var path_xy = rsr.path("M 214.898,461.597 215.487,498.757     233.835,498.542 233.245,461.363    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xy');
        var path_xz = rsr.path("M 233.578,461.359 234.169,498.539     252.514,498.322 251.925,461.126    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_xz');
        var path_ya = rsr.path("M 196.219,461.835 196.809,498.974     215.154,498.76 214.564,461.602    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ya');
        var path_yb = rsr.path("M 83.766,462.932 83.174,425.672     65.156,425.893 65.748,463.162    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yb');
        var path_yc = rsr.path("M 102.446,462.695 101.854,425.441     83.508,425.667 84.1,462.929    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yc');
        var path_yd = rsr.path("M 121.131,462.457 120.541,425.214     102.188,425.438 102.78,462.691    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yd');
        var path_ye = rsr.path("M 121.47,462.786 122.059,499.848     140.411,499.633 139.822,462.553    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ye');
        var path_yf = rsr.path("M 139.817,462.219 139.227,424.982     120.874,425.208 121.465,462.453    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yf');
        var path_yg = rsr.path("M 158.503,461.98 157.912,424.754     139.561,424.979 140.15,462.215    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yg');
        group_ad.attr({'parent': 'Layer_18','name': 'group_ad'});
        var group_ae = rsr.set();
        var path_yh = rsr.path("M 64.024,324.44 64.765,361.441     82.783,361.156 82.042,324.136    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yh');
        var path_yi = rsr.path("M 175.463,322.225 174.718,285.009     156.355,285.311 157.101,322.535    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yi');
        var path_yj = rsr.path("M 250.177,320.963 249.433,283.78     231.088,284.082 231.832,321.273    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yj');
        var path_yk = rsr.path("M 212.82,321.595 212.075,284.395     193.729,284.695 194.475,321.903    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yk');
        var path_yl = rsr.path("M 231.498,321.278 230.754,284.088     212.409,284.389 213.152,321.589    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yl');
        var path_ym = rsr.path("M 194.141,321.909 193.396,284.701     175.051,285.004 175.796,322.219    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ym');
        var path_yn = rsr.path("M 82.374,324.131 83.117,361.15     101.462,360.859 100.721,323.82    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yn');
        var path_yo = rsr.path("M 157.105,322.868 157.85,359.966     176.213,359.675 175.469,322.558    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yo');
        var path_yp = rsr.path("M 101.054,323.814 101.795,360.854     120.146,360.564 119.404,323.506    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yp');
        var path_yq = rsr.path("M 138.422,323.184 139.165,360.262     157.515,359.973 156.774,322.873    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yq');
        var path_yr = rsr.path("M 175.803,322.553 176.547,359.669     194.893,359.38 194.148,322.242    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yr');
        var path_ys = rsr.path("M 213.161,321.922 213.904,359.078     232.25,358.787 231.505,321.612    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ys');
        var path_yt = rsr.path("M 231.84,321.605 232.583,358.783     250.93,358.491 250.185,321.297    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yt');
        var path_yu = rsr.path("M 194.482,322.238 195.226,359.374     213.57,359.083 212.826,321.928    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yu');
        var path_yv = rsr.path("M 82.034,323.801 81.288,286.544     63.271,286.84 64.018,324.105    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yv');
        var path_yw = rsr.path("M 100.714,323.487 99.968,286.236     81.622,286.537 82.369,323.797    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yw');
        var path_yx = rsr.path("M 119.398,323.171 118.651,285.93     100.301,286.23 101.047,323.481    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yx');
        var path_yy = rsr.path("M 119.737,323.499 120.481,360.558     138.831,360.268 138.09,323.189    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yy');
        var path_yz = rsr.path("M 138.082,322.855 137.336,285.623     118.986,285.923 119.732,323.165    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_yz');
        var path_za = rsr.path("M 156.766,322.539 156.021,285.315     137.671,285.617 138.416,322.85    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_za');
        group_ae.attr({'parent': 'Layer_18','name': 'group_ae'});
        var group_af = rsr.set();
        var path_zb = rsr.path("M 273.732,320.634 274.473,357.635     292.492,357.351 291.75,320.329    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zb');
        var path_zc = rsr.path("M 385.172,318.419 384.427,281.202     366.063,281.505 366.809,318.729    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zc');
        var path_zd = rsr.path("M 422.528,317.788 421.783,280.589     403.438,280.891 404.183,318.098    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zd');
        var path_ze = rsr.path("M 441.208,317.472 440.463,280.281     422.117,280.583 422.862,317.783    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ze');
        var path_zf = rsr.path("M 403.849,318.104 403.105,280.896     384.761,281.197 385.506,318.412    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zf');
        var path_zg = rsr.path("M 292.084,320.324 292.825,357.344     311.17,357.055 310.429,320.016    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zg');
        var path_zh = rsr.path("M 366.815,319.063 367.558,356.159     385.921,355.869 385.178,318.752    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zh');
        var path_zi = rsr.path("M 310.762,320.008 311.505,357.048     329.855,356.758 329.113,319.699    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zi');
        var path_zj = rsr.path("M 348.131,319.377 348.873,356.456     367.225,356.166 366.482,319.067    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zj');
        var path_zk = rsr.path("M 385.511,318.747 386.255,355.863     404.602,355.573 403.856,318.437    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zk');
        var path_zl = rsr.path("M 422.869,318.115 423.612,355.272     441.958,354.981 441.214,317.806    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zl');
        var path_zm = rsr.path("M 404.19,318.432 404.934,355.568     423.279,355.277 422.535,318.122    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zm');
        var path_zn = rsr.path("M 291.743,319.996 290.998,282.738     272.98,283.033 273.726,320.299    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zn');
        var path_zo = rsr.path("M 310.422,319.682 309.677,282.431     291.331,282.73 292.078,319.99    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zo');
        var path_zp = rsr.path("M 329.106,319.365 328.36,282.124     310.011,282.426 310.757,319.675    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zp');
        var path_zq = rsr.path("M 329.447,319.692 330.19,356.752     348.539,356.462 347.798,319.384    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zq');
        var path_zr = rsr.path("M 347.79,319.05 347.046,281.816     328.694,282.118 329.44,319.36    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zr');
        var path_zs = rsr.path("M 366.475,318.733 365.73,281.509     347.379,281.812 348.124,319.044    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zs');
        group_af.attr({'parent': 'Layer_18','name': 'group_af'});
        var group_ag = rsr.set();
        var path_zt = rsr.path("M 470.883,317.14 471.623,354.142     489.642,353.855 488.9,316.836    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zt');
        var path_zu = rsr.path("M 582.32,314.924 581.576,277.709     563.214,278.01 563.959,315.235    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zu');
        var path_zv = rsr.path("M 657.036,313.663 656.291,276.48     637.946,276.781 638.69,313.972    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zv');
        var path_zw = rsr.path("M 619.678,314.294 618.934,277.095     600.589,277.396 601.334,314.604    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zw');
        var path_zx = rsr.path("M 638.357,313.979 637.612,276.787     619.267,277.089 620.012,314.288    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zx');
        var path_zy = rsr.path("M 600.999,314.609 600.255,277.401     581.91,277.703 582.655,314.919    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zy');
        var path_zz = rsr.path("M 489.233,316.829 489.975,353.85     508.32,353.56 507.579,316.521    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_zz');
        var path_aaa = rsr.path("M 563.965,315.567 564.709,352.666     583.07,352.375 582.328,315.258    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaa');
        var path_aab = rsr.path("M 507.912,316.515 508.654,353.554     527.006,353.264 526.262,316.205    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aab');
        var path_aac = rsr.path("M 545.281,315.884 546.024,352.962     564.374,352.673 563.633,315.573    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aac');
        var path_aad = rsr.path("M 582.66,315.254 583.404,352.369     601.751,352.08 601.007,314.942    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aad');
        var path_aae = rsr.path("M 620.02,314.622 620.762,351.778     639.107,351.487 638.364,314.313    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aae');
        var path_aaf = rsr.path("M 638.697,314.306 639.441,351.482     657.787,351.191 657.042,313.997    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaf');
        var path_aag = rsr.path("M 601.34,314.938 602.084,352.073     620.43,351.783 619.686,314.627    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aag');
        var path_aah = rsr.path("M 488.894,316.501 488.147,279.244     470.13,279.54 470.877,316.806    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aah');
        var path_aai = rsr.path("M 507.573,316.188 506.826,278.937     488.48,279.237 489.227,316.497    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aai');
        var path_aaj = rsr.path("M 526.258,315.871 525.511,278.631     507.16,278.931 507.906,316.182    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaj');
        var path_aak = rsr.path("M 526.597,316.199 527.34,353.258     545.689,352.969 544.947,315.89    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aak');
        var path_aal = rsr.path("M 544.94,315.556 544.195,278.322     525.846,278.623 526.59,315.865    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aal');
        var path_aam = rsr.path("M 563.625,315.24 562.88,278.016     544.529,278.316 545.273,315.551    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aam');
        var path_aan = rsr.path("M 694.369,313.09 693.624,275.874     675.263,276.175 676.007,313.399    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aan');
        var path_aao = rsr.path("M 769.084,311.828 768.339,274.646     749.994,274.946 750.738,312.137    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aao');
        var path_aap = rsr.path("M 731.728,312.459 730.981,275.26     712.637,275.561 713.382,312.769    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aap');
        var path_aaq = rsr.path("M 750.406,312.144 749.661,274.953     731.315,275.254 732.061,312.453    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaq');
        var path_aar = rsr.path("M 713.048,312.773 712.303,275.566     693.959,275.868 694.703,313.084    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aar');
        var path_aas = rsr.path("M 676.015,313.733 676.757,350.831     695.119,350.539 694.376,313.423    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aas');
        var path_aat = rsr.path("M 657.33,314.049 658.072,351.127     676.422,350.838 675.681,313.737    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aat');
        var path_aau = rsr.path("M 694.709,313.418 695.453,350.534     713.8,350.244 713.055,313.107    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aau');
        var path_aav = rsr.path("M 732.067,312.787 732.811,349.943     751.156,349.652 750.412,312.477    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aav');
        var path_aaw = rsr.path("M 750.747,312.471 751.489,349.647     769.836,349.356 769.091,312.161    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaw');
        var path_aax = rsr.path("M 713.389,313.103 714.132,350.239     732.478,349.947 731.734,312.793    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aax');
        var path_aay = rsr.path("M 675.673,313.405 674.929,276.181     656.577,276.482 657.323,313.715    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aay');
        group_ag.attr({'parent': 'Layer_18','name': 'group_ag'});
        var group_ah = rsr.set();
        var path_aaz = rsr.path("M 62.861,231.064 63.603,268.066     81.621,267.78 80.879,230.76    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aaz');
        var path_aba = rsr.path("M 174.3,228.849 173.555,191.633     155.193,191.935 155.938,229.16    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aba');
        var path_abb = rsr.path("M 249.016,227.588 248.271,190.405     229.926,190.706 230.67,227.896    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abb');
        var path_abc = rsr.path("M 211.658,228.219 210.912,191.02     192.568,191.319 193.313,228.528    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abc');
        var path_abd = rsr.path("M 230.337,227.902 229.592,190.712     211.246,191.013 211.991,228.213    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abd');
        var path_abe = rsr.path("M 192.979,228.532 192.233,191.326     173.89,191.628 174.635,228.844    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abe');
        var path_abf = rsr.path("M 81.213,230.754 81.954,267.773     100.299,267.483 99.559,230.445    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abf');
        var path_abg = rsr.path("M 155.944,229.492 156.688,266.59     175.05,266.299 174.308,229.183    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abg');
        var path_abh = rsr.path("M 99.891,230.438 100.633,267.479     118.985,267.188 118.241,230.13    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abh');
        var path_abi = rsr.path("M 137.261,229.808 138.003,266.887     156.354,266.596 155.611,229.498    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abi');
        var path_abj = rsr.path("M 174.64,229.177 175.384,266.294     193.73,266.005 192.985,228.867    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abj');
        var path_abk = rsr.path("M 211.999,228.547 212.741,265.702     231.087,265.412 230.344,228.236    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abk');
        var path_abl = rsr.path("M 230.677,228.23 231.421,265.406     249.767,265.115 249.021,227.922    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abl');
        var path_abm = rsr.path("M 193.319,228.861 194.063,265.998     212.409,265.708 211.665,228.552    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abm');
        var path_abn = rsr.path("M 80.873,230.426 80.127,193.169     62.109,193.463 62.856,230.729    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abn');
        var path_abo = rsr.path("M 99.551,230.111 98.805,192.861     80.459,193.162 81.206,230.421    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abo');
        var path_abp = rsr.path("M 118.236,229.795 117.49,192.555     99.14,192.855 99.886,230.105    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abp');
        var path_abq = rsr.path("M 118.576,230.123 119.318,267.183     137.669,266.892 136.927,229.813    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abq');
        var path_abr = rsr.path("M 136.92,229.48 136.175,192.247     117.823,192.548 118.569,229.79    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abr');
        var path_abs = rsr.path("M 155.604,229.164 154.859,191.94     136.508,192.241 137.253,229.475    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abs');
        group_ah.attr({'parent': 'Layer_18','name': 'group_ah'});
        var group_ai = rsr.set();
        var path_abt = rsr.path("M 272.57,227.258 273.312,264.26     291.33,263.975 290.589,226.954    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abt');
        var path_abu = rsr.path("M 384.009,225.043 383.265,187.827     364.902,188.128 365.647,225.354    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abu');
        var path_abv = rsr.path("M 421.367,224.412 420.622,187.213     402.276,187.514 403.021,224.722    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abv');
        var path_abw = rsr.path("M 440.045,224.097 439.3,186.906     420.954,187.207 421.699,224.407    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abw');
        var path_abx = rsr.path("M 402.688,224.727 401.942,187.521     383.598,187.821 384.343,225.037    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abx');
        var path_aby = rsr.path("M 290.921,226.949 291.664,263.969     310.008,263.678 309.267,226.639    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aby');
        var path_abz = rsr.path("M 365.652,225.687 366.396,262.784     384.758,262.493 384.016,225.376    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_abz');
        var path_aca = rsr.path("M 309.601,226.633 310.342,263.672     328.693,263.383 327.951,226.323    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aca');
        var path_acb = rsr.path("M 346.969,226.002 347.712,263.081     366.062,262.791 365.321,225.691    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acb');
        var path_acc = rsr.path("M 384.35,225.371 385.093,262.487     403.438,262.198 402.695,225.062    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acc');
        var path_acd = rsr.path("M 421.708,224.74 422.451,261.896     440.797,261.605 440.052,224.43    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acd');
        var path_ace = rsr.path("M 403.028,225.055 403.772,262.192     422.118,261.901 421.373,224.746    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ace');
        var path_acf = rsr.path("M 290.581,226.619 289.835,189.362     271.817,189.658 272.564,226.924    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acf');
        var path_acg = rsr.path("M 309.26,226.306 308.515,189.055     290.169,189.355 290.914,226.615    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acg');
        var path_ach = rsr.path("M 327.945,225.989 327.198,188.749     308.848,189.049 309.594,226.3    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ach');
        var path_aci = rsr.path("M 328.284,226.317 329.027,263.377     347.378,263.087 346.635,226.008    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aci');
        var path_acj = rsr.path("M 346.629,225.674 345.883,188.441     327.533,188.742 328.277,225.983    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acj');
        var path_ack = rsr.path("M 365.313,225.357 364.567,188.134     346.218,188.436 346.963,225.668    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ack');
        group_ai.attr({'parent': 'Layer_18','name': 'group_ai'});
        var group_aj = rsr.set();
        var path_acl = rsr.path("M 469.72,223.764 470.461,260.766     488.48,260.48 487.738,223.46    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acl');
        var path_acm = rsr.path("M 581.159,221.549 580.414,184.332     562.051,184.634 562.797,221.859    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acm');
        var path_acn = rsr.path("M 655.874,220.287 655.129,183.104     636.783,183.406 637.528,220.597    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acn');
        var path_aco = rsr.path("M 618.517,220.918 617.771,183.72     599.427,184.021 600.171,221.229    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aco');
        var path_acp = rsr.path("M 637.195,220.602 636.45,183.412     618.105,183.713 618.85,220.913    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acp');
        var path_acq = rsr.path("M 599.837,221.232 599.093,184.026     580.748,184.328 581.492,221.543    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acq');
        var path_acr = rsr.path("M 488.07,223.454 488.813,260.474     507.158,260.184 506.417,223.146    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acr');
        var path_acs = rsr.path("M 562.803,222.192 563.546,259.29     581.908,258.999 581.165,221.881    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acs');
        var path_act = rsr.path("M 506.75,223.138 507.492,260.178     525.843,259.888 525.101,222.829    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_act');
        var path_acu = rsr.path("M 544.119,222.508 544.86,259.587     563.212,259.296 562.47,222.197    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acu');
        var path_acv = rsr.path("M 581.498,221.877 582.243,258.994     600.589,258.703 599.845,221.567    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acv');
        var path_acw = rsr.path("M 618.856,221.245 619.601,258.402     637.946,258.111 637.202,220.936    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acw');
        var path_acx = rsr.path("M 637.536,220.93 638.28,258.106     656.626,257.815 655.881,220.621    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acx');
        var path_acy = rsr.path("M 600.179,221.562 600.921,258.698     619.267,258.406 618.522,221.252    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acy');
        var path_acz = rsr.path("M 487.73,223.126 486.986,185.869     468.967,186.163 469.714,223.431    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_acz');
        var path_ada = rsr.path("M 506.41,222.812 505.664,185.561     487.318,185.861 488.065,223.121    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ada');
        var path_adb = rsr.path("M 525.095,222.496 524.349,185.255     505.998,185.556 506.744,222.806    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adb');
        var path_adc = rsr.path("M 525.434,222.824 526.178,259.883     544.527,259.592 543.785,222.513    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adc');
        var path_add = rsr.path("M 543.778,222.181 543.033,184.947     524.683,185.248 525.429,222.49    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_add');
        var path_ade = rsr.path("M 562.463,221.864 561.718,184.639     543.367,184.941 544.112,222.174    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ade');
        var path_adf = rsr.path("M 693.207,219.714 692.462,182.497     674.1,182.8 674.846,220.024    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adf');
        var path_adg = rsr.path("M 767.923,218.453 767.178,181.269     748.833,181.571 749.576,218.762    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adg');
        var path_adh = rsr.path("M 730.564,219.083 729.82,181.884     711.476,182.186 712.219,219.393    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adh');
        var path_adi = rsr.path("M 749.243,218.767 748.498,181.576     730.153,181.878 730.898,219.078    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adi');
        var path_adj = rsr.path("M 711.886,219.397 711.141,182.19     692.796,182.493 693.541,219.707    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adj');
        var path_adk = rsr.path("M 674.851,220.357 675.595,257.455     693.957,257.164 693.214,220.047    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adk');
        var path_adl = rsr.path("M 656.167,220.672 656.909,257.751     675.261,257.461 674.52,220.362    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adl');
        var path_adm = rsr.path("M 693.547,220.042 694.291,257.158     712.637,256.868 711.893,219.732    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adm');
        var path_adn = rsr.path("M 730.905,219.41 731.648,256.567     749.994,256.276 749.251,219.101    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adn');
        var path_ado = rsr.path("M 749.584,219.095 750.328,256.271     768.674,255.98 767.929,218.786    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ado');
        var path_adp = rsr.path("M 712.227,219.727 712.97,256.863     731.315,256.572 730.571,219.417    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adp');
        var path_adq = rsr.path("M 674.512,220.029 673.766,182.804     655.416,183.106 656.16,220.339    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adq');
        group_aj.attr({'parent': 'Layer_18','name': 'group_aj'});
        var group_ak = rsr.set();
        var path_adr = rsr.path("M 468.798,130.708 469.538,167.71     487.556,167.424 486.815,130.402    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adr');
        var path_ads = rsr.path("M 580.236,128.492 579.491,91.277     561.129,91.578 561.874,128.803    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ads');
        var path_adt = rsr.path("M 654.95,127.231 654.205,90.049     635.86,90.35 636.604,127.54    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adt');
        var path_adu = rsr.path("M 617.594,127.862 616.849,90.662     598.503,90.964 599.248,128.172    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adu');
        var path_adv = rsr.path("M 636.271,127.546 635.527,90.354     617.183,90.656 617.926,127.856    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adv');
        var path_adw = rsr.path("M 598.914,128.177 598.169,90.969     579.824,91.271 580.569,128.486    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adw');
        var path_adx = rsr.path("M 487.147,130.397 487.891,167.417     506.235,167.128 505.494,130.089    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adx');
        var path_ady = rsr.path("M 561.881,129.137 562.623,166.234     580.985,165.942 580.242,128.825    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ady');
        var path_adz = rsr.path("M 505.827,130.082 506.568,167.122     524.921,166.832 524.178,129.773    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_adz');
        var path_aea = rsr.path("M 543.195,129.45 543.938,166.529     562.288,166.24 561.548,129.142    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aea');
        var path_aeb = rsr.path("M 580.576,128.82 581.32,165.938     599.666,165.647 598.922,128.511    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeb');
        var path_aec = rsr.path("M 617.934,128.189 618.678,165.346     637.023,165.055 636.278,127.88    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aec');
        var path_aed = rsr.path("M 636.613,127.873 637.357,165.05     655.701,164.76 654.958,127.564    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aed');
        var path_aee = rsr.path("M 599.256,128.505 599.998,165.642     618.344,165.351 617.6,128.195    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aee');
        var path_aef = rsr.path("M 486.809,130.069 486.062,92.813     468.044,93.106 468.791,130.373    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aef');
        var path_aeg = rsr.path("M 505.487,129.755 504.741,92.504     486.396,92.806 487.143,130.064    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeg');
        var path_aeh = rsr.path("M 524.172,129.438 523.425,92.197     505.074,92.499 505.82,129.748    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeh');
        var path_aei = rsr.path("M 524.511,129.766 525.255,166.825     543.604,166.535 542.862,129.457    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aei');
        var path_aej = rsr.path("M 542.855,129.123 542.109,91.89     523.76,92.191 524.504,129.434    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aej');
        var path_aek = rsr.path("M 561.539,128.808 560.795,91.583     542.444,91.885 543.189,129.118    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aek');
        var path_ael = rsr.path("M 692.284,126.658 691.539,89.441     673.178,89.743 673.922,126.968    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_ael');
        var path_aem = rsr.path("M 766.998,125.396 766.255,88.213     747.91,88.515 748.652,125.705    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aem');
        var path_aen = rsr.path("M 729.642,126.026 728.896,88.827     710.551,89.129 711.296,126.336    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aen');
        var path_aeo = rsr.path("M 748.32,125.711 747.575,88.52     729.23,88.822 729.975,126.021    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeo');
        var path_aep = rsr.path("M 710.963,126.342 710.218,89.134     691.873,89.437 692.619,126.651    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aep');
        var path_aeq = rsr.path("M 673.929,127.301 674.672,164.398     693.034,164.107 692.291,126.99    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeq');
        var path_aer = rsr.path("M 655.244,127.615 655.986,164.695     674.337,164.405 673.596,127.306    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aer');
        var path_aes = rsr.path("M 692.624,126.985 693.368,164.102     711.714,163.813 710.97,126.676    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aes');
        var path_aet = rsr.path("M 729.982,126.354 730.726,163.51     749.071,163.22 748.326,126.045    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aet');
        var path_aeu = rsr.path("M 748.661,126.039 749.405,163.216     767.751,162.923 767.006,125.729    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aeu');
        var path_aev = rsr.path("M 711.304,126.67 712.046,163.807     730.393,163.516 729.648,126.361    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aev');
        var path_aew = rsr.path("M 673.588,126.972 672.843,89.748     654.492,90.049 655.237,127.283    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aew');
        group_ak.attr({'parent': 'Layer_18','name': 'group_ak'});
        var group_al = rsr.set();
        var path_aex = rsr.path("M 1373.543,338.301 1410.643,337.66     1410.403,319.296 1373.284,319.938    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aex');
        var path_aey = rsr.path("M 1373.808,356.986 1410.888,356.346     1410.647,337.994 1373.549,338.634    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aey');
        var path_aez = rsr.path("M 1373.279,319.604 1410.398,318.963     1410.158,300.617 1373.019,301.26    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aez');
        var path_afa = rsr.path("M 1372.75,282.245 1409.909,281.603     1409.668,263.257 1372.49,263.899    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afa');
        var path_afb = rsr.path("M 1372.485,263.565 1409.662,262.924     1409.423,244.578 1372.227,245.22    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afb');
        var path_afc = rsr.path("M 1373.015,300.925 1410.152,300.282     1409.913,281.937 1372.755,282.579    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afc');
        var path_afd = rsr.path("M 1374.072,375.672 1411.133,375.031     1410.894,356.679 1373.813,357.32    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afd');
        group_al.attr({'parent': 'Layer_18','name': 'group_al'});
        var group_am = rsr.set();
        var path_afe = rsr.path("M 1372.449,244.944 1409.432,244.305      1409.188,225.62 1372.184,226.259     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afe');
        var path_aff = rsr.path("M 1372.18,225.926 1409.183,225.286      1408.947,207.268 1371.925,207.908     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aff');
        var path_afg = rsr.path("M 1371.92,207.574 1408.942,206.934      1408.702,188.587 1371.66,189.229     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afg');
        var path_afh = rsr.path("M 1370.863,132.838 1407.963,132.197      1407.722,113.834 1370.603,114.476     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afh');
        var path_afi = rsr.path("M 1371.656,188.895 1408.697,188.255      1408.457,169.902 1371.396,170.543     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afi');
        var path_afj = rsr.path("M 1371.127,151.524 1408.207,150.883      1407.967,132.53 1370.867,133.173     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afj');
        var path_afk = rsr.path("M 1370.599,114.142 1407.717,113.5      1407.477,95.153 1370.339,95.796     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afk');
        var path_afl = rsr.path("M 1370.069,76.783 1407.228,76.141      1406.987,57.795 1369.809,58.438     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afl');
        var path_afm = rsr.path("M 1369.805,58.104 1406.983,57.461      1406.742,39.114 1369.545,39.758     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afm');
        var path_afn = rsr.path("M 1370.334,95.462 1407.473,94.82      1407.231,76.475 1370.074,77.116     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afn');
        var path_afo = rsr.path("M 1371.392,170.209 1408.451,169.567      1408.212,151.217 1371.132,151.857     z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afo');
        group_am.attr({'parent': 'Layer_18','name': 'group_am'});
        var group_an = rsr.set();
        var path_afp = rsr.path("M 1377.396,613.964 1414.381,613.324     1414.135,594.64 1377.132,595.279    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afp');
        var path_afq = rsr.path("M 1377.127,594.945 1414.13,594.307     1413.895,576.287 1376.873,576.927    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afq');
        var path_afr = rsr.path("M 1376.868,576.594 1413.891,575.954     1413.649,557.608 1376.608,558.248    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afr');
        var path_afs = rsr.path("M 1375.811,501.859 1412.91,501.217     1412.67,482.854 1375.55,483.496    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afs');
        var path_aft = rsr.path("M 1376.603,557.915 1413.646,557.274     1413.404,538.922 1376.344,539.563    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aft');
        var path_afu = rsr.path("M 1376.074,520.544 1413.155,519.902     1412.914,501.551 1375.814,502.191    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afu');
        var path_afv = rsr.path("M 1375.546,483.162 1412.665,482.521     1412.424,464.175 1375.285,464.815    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afv');
        var path_afw = rsr.path("M 1375.017,445.803 1412.176,445.16     1411.934,426.814 1374.757,427.457    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afw');
        var path_afx = rsr.path("M 1374.752,427.123 1411.931,426.481     1411.69,408.135 1374.493,408.777    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afx');
        var path_afy = rsr.path("M 1375.281,464.482 1412.42,463.84     1412.18,445.494 1375.022,446.137    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afy');
        var path_afz = rsr.path("M 1376.34,539.229 1413.4,538.589     1413.159,520.236 1376.08,520.878    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_afz');
        group_an.attr({'parent': 'Layer_18','name': 'group_an'});
        var group_ao = rsr.set();
        var path_aga = rsr.path("M 1378.942,725.689 1416.042,725.049     1415.801,706.685 1378.682,707.326    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_aga');
        var path_agb = rsr.path("M 1379.207,744.375 1416.287,743.733     1416.046,725.381 1378.946,726.022    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_agb');
        var path_agc = rsr.path("M 1378.678,706.992 1415.797,706.352     1415.556,688.006 1378.418,688.647    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_agc');
        var path_agd = rsr.path("M 1378.148,669.634 1415.307,668.991     1415.066,650.646 1377.89,651.288    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_agd');
        var path_age = rsr.path("M 1377.884,650.954 1415.063,650.313     1414.822,631.966 1377.625,632.608    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_age');
        var path_agf = rsr.path("M 1378.413,688.314 1415.552,687.671     1415.311,669.325 1378.154,669.967    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_agf');
        var path_agg = rsr.path("M 1379.47,763.061 1416.531,762.42     1416.291,744.067 1379.211,744.709    z").attr({fill: '#FFCC00',stroke: '#6D6E70',"stroke-width": '2',"stroke-miterlimit": '10',parent: 'Layer_18','stroke-opacity': '1'}).data('id', 'path_agg');
        group_ao.attr({'parent': 'Layer_18','name': 'group_ao'});

        var rsrGroups = [Layer_18,group_a,group_b,group_c,group_d,group_e,group_f,group_g,group_h,group_i,group_j,group_k,group_l,group_m,group_n,group_o,group_p,group_q,group_r,group_s,group_t,group_u,group_v,group_w,group_x,group_y,group_z,group_aa,group_ab,group_ac,group_ad,group_ae,group_af,group_ag,group_ah,group_ai,group_aj,group_ak,group_al,group_am,group_an,group_ao];
        Layer_18.push(
            path_ap ,
            path_aq ,
            path_ar ,
            path_as ,
            path_at ,
            path_au ,
            path_av ,
            path_aw ,
            path_ax ,
            path_ay ,
            path_az ,
            path_ba ,
            path_bb ,
            path_bc ,
            path_bd ,
            path_be
        );
        group_a.push(
            path_bf ,
            path_bg ,
            path_bh ,
            path_bi ,
            path_bj ,
            path_bk ,
            path_bl ,
            path_bm ,
            path_bn ,
            path_bo ,
            path_bp ,
            path_bq ,
            path_br ,
            path_bs ,
            path_bt ,
            path_bu ,
            path_bv ,
            path_bw ,
            path_bx ,
            path_by ,
            path_bz ,
            path_ca ,
            path_cb ,
            path_cc ,
            path_cd ,
            path_ce ,
            path_cf ,
            path_cg ,
            path_ch ,
            path_ci
        );
        group_b.push(
            path_cj ,
            path_ck ,
            path_cl ,
            path_cm ,
            path_cn ,
            path_co ,
            path_cp ,
            path_cq ,
            path_cr ,
            path_cs ,
            path_ct ,
            path_cu ,
            path_cv ,
            path_cw ,
            path_cx ,
            path_cy ,
            path_cz ,
            path_da ,
            path_db ,
            path_dc ,
            path_dd ,
            path_de ,
            path_df ,
            path_dg ,
            path_dh ,
            path_di ,
            path_dj ,
            path_dk ,
            path_dl ,
            path_dm
        );
        group_c.push(
            path_dn ,
            path_do ,
            path_dp ,
            path_dq ,
            path_dr ,
            path_ds ,
            path_dt ,
            path_du ,
            path_dv ,
            path_dw ,
            path_dx ,
            path_dy ,
            path_dz ,
            path_ea ,
            path_eb ,
            path_ec ,
            path_ed ,
            path_ee ,
            path_ef ,
            path_eg ,
            path_eh ,
            path_ei ,
            path_ej ,
            path_ek ,
            path_el ,
            path_em ,
            path_en ,
            path_eo ,
            path_ep ,
            path_eq
        );
        group_d.push(
            path_er ,
            path_es ,
            path_et ,
            path_eu ,
            path_ev ,
            path_ew ,
            path_ex ,
            path_ey ,
            path_ez ,
            path_fa ,
            path_fb ,
            path_fc ,
            path_fd ,
            path_fe ,
            path_ff ,
            path_fg ,
            path_fh ,
            path_fi ,
            path_fj ,
            path_fk ,
            path_fl ,
            path_fm ,
            path_fn ,
            path_fo ,
            path_fp ,
            path_fq ,
            path_fr
        );
        group_e.push(
            path_fs ,
            path_ft ,
            path_fu ,
            path_fv ,
            path_fw ,
            path_fx ,
            path_fy ,
            path_fz ,
            path_ga ,
            path_gb ,
            path_gc ,
            path_gd ,
            path_ge ,
            path_gf ,
            path_gg ,
            path_gh ,
            path_gi ,
            path_gj ,
            path_gk ,
            path_gl ,
            path_gm ,
            path_gn ,
            path_go ,
            path_gp ,
            path_gq ,
            path_gr ,
            path_gs ,
            path_gt ,
            path_gu ,
            path_gv
        );
        group_f.push(
            path_gw ,
            path_gx ,
            path_gy ,
            path_gz ,
            path_ha ,
            path_hb ,
            path_hc ,
            path_hd ,
            path_he ,
            path_hf ,
            path_hg ,
            path_hh ,
            path_hi ,
            path_hj ,
            path_hk ,
            path_hl ,
            path_hm ,
            path_hn ,
            path_ho ,
            path_hp ,
            path_hq ,
            path_hr ,
            path_hs ,
            path_ht ,
            path_hu ,
            path_hv ,
            path_hw
        );
        group_g.push(
            path_hx ,
            path_hy ,
            path_hz ,
            path_ia ,
            path_ib ,
            path_ic ,
            path_id ,
            path_ie ,
            path_if ,
            path_ig ,
            path_ih ,
            path_ii ,
            path_ij ,
            path_ik ,
            path_il ,
            path_im ,
            path_in ,
            path_io ,
            path_ip ,
            path_iq ,
            path_ir ,
            path_is
        );
        group_h.push(
            path_it ,
            path_iu ,
            path_iv ,
            path_iw ,
            path_ix ,
            path_iy ,
            path_iz ,
            path_ja ,
            path_jb ,
            path_jc ,
            path_jd ,
            path_je ,
            path_jf ,
            path_jg ,
            path_jh ,
            path_ji ,
            path_jj ,
            path_jk ,
            path_jl ,
            path_jm ,
            path_jn ,
            path_jo
        );
        group_i.push(
            path_jp ,
            path_jq ,
            path_jr ,
            path_js ,
            path_jt ,
            path_ju ,
            path_jv ,
            path_jw ,
            path_jx ,
            path_jy ,
            path_jz ,
            path_ka ,
            path_kb ,
            path_kc ,
            path_kd ,
            path_ke ,
            path_kf ,
            path_kg ,
            path_kh ,
            path_ki ,
            path_kj ,
            path_kk
        );
        group_j.push(
            path_kl ,
            path_km ,
            path_kn ,
            path_ko ,
            path_kp ,
            path_kq ,
            path_kr ,
            path_ks ,
            path_kt ,
            path_ku ,
            path_kv ,
            path_kw ,
            path_kx ,
            path_ky ,
            path_kz ,
            path_la ,
            path_lb ,
            path_lc
        );
        group_k.push(
            path_ld ,
            path_le ,
            path_lf ,
            path_lg ,
            path_lh ,
            path_li ,
            path_lj ,
            path_lk ,
            path_ll
        );
        group_l.push(
            path_lm ,
            path_ln ,
            path_lo ,
            path_lp ,
            path_lq ,
            path_lr ,
            path_ls ,
            path_lt ,
            path_lu ,
            path_lv ,
            path_lw ,
            path_lx ,
            path_ly ,
            path_lz ,
            path_ma ,
            path_mb ,
            path_mc ,
            path_md ,
            path_me ,
            path_mf
        );
        group_m.push(
            path_mg ,
            path_mh ,
            path_mi ,
            path_mj ,
            path_mk ,
            path_ml ,
            path_mm ,
            path_mn ,
            path_mo ,
            path_mp ,
            path_mq ,
            path_mr ,
            path_ms ,
            path_mt ,
            path_mu ,
            path_mv ,
            path_mw ,
            path_mx ,
            path_my ,
            path_mz
        );
        group_n.push(
            path_na ,
            path_nb ,
            path_nc ,
            path_nd ,
            path_ne ,
            path_nf ,
            path_ng ,
            path_nh ,
            path_ni ,
            path_nj ,
            path_nk ,
            path_nl ,
            path_nm ,
            path_nn ,
            path_no ,
            path_np ,
            path_nq ,
            path_nr ,
            path_ns ,
            path_nt
        );
        group_o.push(
            path_nu ,
            path_nv ,
            path_nw ,
            path_nx ,
            path_ny ,
            path_nz ,
            path_oa ,
            path_ob ,
            path_oc ,
            path_od ,
            path_oe ,
            path_of ,
            path_og ,
            path_oh ,
            path_oi ,
            path_oj ,
            path_ok ,
            path_ol ,
            path_om ,
            path_on ,
            path_oo ,
            path_op
        );
        group_p.push(
            path_oq ,
            path_or ,
            path_os ,
            path_ot ,
            path_ou ,
            path_ov ,
            path_ow ,
            path_ox ,
            path_oy ,
            path_oz ,
            path_pa ,
            path_pb ,
            path_pc ,
            path_pd
        );
        group_q.push(
            path_pe ,
            path_pf ,
            path_pg ,
            path_ph ,
            path_pi ,
            path_pj ,
            path_pk ,
            path_pl ,
            path_pm ,
            path_pn ,
            path_po ,
            path_pp ,
            path_pq ,
            path_pr ,
            path_ps ,
            path_pt ,
            path_pu ,
            path_pv ,
            path_pw ,
            path_px ,
            path_py ,
            path_pz
        );
        group_r.push(
            path_qa ,
            path_qb ,
            path_qc ,
            path_qd ,
            path_qe ,
            path_qf ,
            path_qg ,
            path_qh ,
            path_qi ,
            path_qj ,
            path_qk ,
            path_ql ,
            path_qm ,
            path_qn ,
            path_qo ,
            path_qp ,
            path_qq ,
            path_qr
        );
        group_s.push(
            path_qs ,
            path_qt ,
            path_qu ,
            path_qv ,
            path_qw ,
            path_qx ,
            path_qy ,
            path_qz ,
            path_ra ,
            path_rb ,
            path_rc ,
            path_rd ,
            path_re ,
            path_rf ,
            path_rg ,
            path_rh ,
            path_ri ,
            path_rj
        );
        group_t.push(
            path_rk ,
            path_rl ,
            path_rm ,
            path_rn ,
            path_ro ,
            path_rp ,
            path_rq ,
            path_rr ,
            path_rs ,
            path_rt ,
            path_ru ,
            path_rv ,
            path_rw ,
            path_rx
        );
        group_u.push(
            path_ry ,
            path_rz ,
            path_sa ,
            path_sb ,
            path_sc ,
            path_sd ,
            path_se ,
            path_sf ,
            path_sg ,
            path_sh ,
            path_si ,
            path_sj ,
            path_sk ,
            path_sl ,
            path_sm ,
            path_sn ,
            path_so ,
            path_sp ,
            path_sq ,
            path_sr ,
            path_ss ,
            path_st
        );
        group_v.push(
            path_su ,
            path_sv ,
            path_sw ,
            path_sx ,
            path_sy ,
            path_sz ,
            path_ta ,
            path_tb ,
            path_tc ,
            path_td ,
            path_te ,
            path_tf ,
            path_tg ,
            path_th ,
            path_ti ,
            path_tj ,
            path_tk ,
            path_tl
        );
        group_w.push(
            path_tm ,
            path_tn ,
            path_to ,
            path_tp ,
            path_tq ,
            path_tr ,
            path_ts ,
            path_tt ,
            path_tu ,
            path_tv ,
            path_tw ,
            path_tx ,
            path_ty ,
            path_tz ,
            path_ua ,
            path_ub ,
            path_uc ,
            path_ud ,
            path_ue ,
            path_uf ,
            path_ug ,
            path_uh
        );
        group_x.push(
            path_ui ,
            path_uj ,
            path_uk ,
            path_ul ,
            path_um ,
            path_un ,
            path_uo ,
            path_up ,
            path_uq ,
            path_ur ,
            path_us ,
            path_ut ,
            path_uu ,
            path_uv ,
            path_uw ,
            path_ux ,
            path_uy ,
            path_uz
        );
        group_y.push(
            path_va ,
            path_vb ,
            path_vc ,
            path_vd ,
            path_ve ,
            path_vf ,
            path_vg ,
            path_vh ,
            path_vi ,
            path_vj ,
            path_vk ,
            path_vl ,
            path_vm ,
            path_vn ,
            path_vo ,
            path_vp ,
            path_vq ,
            path_vr ,
            path_vs ,
            path_vt ,
            path_vu ,
            path_vv
        );
        group_z.push(
            path_vw ,
            path_vx ,
            path_vy ,
            path_vz ,
            path_wa ,
            path_wb ,
            path_wc ,
            path_wd ,
            path_we ,
            path_wf ,
            path_wg
        );
        group_aa.push(
            path_wh ,
            path_wi ,
            path_wj ,
            path_wk ,
            path_wl ,
            path_wm ,
            path_wn ,
            path_wo ,
            path_wp ,
            path_wq ,
            path_wr ,
            path_ws ,
            path_wt ,
            path_wu ,
            path_wv ,
            path_ww ,
            path_wx ,
            path_wy ,
            path_wz ,
            path_xa
        );
        group_ab.push(
            path_xb ,
            path_xc ,
            path_xd ,
            path_xe ,
            path_xf ,
            path_xg
        );
        group_ac.push(
            path_xh ,
            path_xi ,
            path_xj ,
            path_xk ,
            path_xl ,
            path_xm
        );
        group_ad.push(
            path_xn ,
            path_xo ,
            path_xp ,
            path_xq ,
            path_xr ,
            path_xs ,
            path_xt ,
            path_xu ,
            path_xv ,
            path_xw ,
            path_xx ,
            path_xy ,
            path_xz ,
            path_ya ,
            path_yb ,
            path_yc ,
            path_yd ,
            path_ye ,
            path_yf ,
            path_yg
        );
        group_ae.push(
            path_yh ,
            path_yi ,
            path_yj ,
            path_yk ,
            path_yl ,
            path_ym ,
            path_yn ,
            path_yo ,
            path_yp ,
            path_yq ,
            path_yr ,
            path_ys ,
            path_yt ,
            path_yu ,
            path_yv ,
            path_yw ,
            path_yx ,
            path_yy ,
            path_yz ,
            path_za
        );
        group_af.push(
            path_zb ,
            path_zc ,
            path_zd ,
            path_ze ,
            path_zf ,
            path_zg ,
            path_zh ,
            path_zi ,
            path_zj ,
            path_zk ,
            path_zl ,
            path_zm ,
            path_zn ,
            path_zo ,
            path_zp ,
            path_zq ,
            path_zr ,
            path_zs
        );
        group_ag.push(
            path_zt ,
            path_zu ,
            path_zv ,
            path_zw ,
            path_zx ,
            path_zy ,
            path_zz ,
            path_aaa ,
            path_aab ,
            path_aac ,
            path_aad ,
            path_aae ,
            path_aaf ,
            path_aag ,
            path_aah ,
            path_aai ,
            path_aaj ,
            path_aak ,
            path_aal ,
            path_aam ,
            path_aan ,
            path_aao ,
            path_aap ,
            path_aaq ,
            path_aar ,
            path_aas ,
            path_aat ,
            path_aau ,
            path_aav ,
            path_aaw ,
            path_aax ,
            path_aay
        );
        group_ah.push(
            path_aaz ,
            path_aba ,
            path_abb ,
            path_abc ,
            path_abd ,
            path_abe ,
            path_abf ,
            path_abg ,
            path_abh ,
            path_abi ,
            path_abj ,
            path_abk ,
            path_abl ,
            path_abm ,
            path_abn ,
            path_abo ,
            path_abp ,
            path_abq ,
            path_abr ,
            path_abs
        );
        group_ai.push(
            path_abt ,
            path_abu ,
            path_abv ,
            path_abw ,
            path_abx ,
            path_aby ,
            path_abz ,
            path_aca ,
            path_acb ,
            path_acc ,
            path_acd ,
            path_ace ,
            path_acf ,
            path_acg ,
            path_ach ,
            path_aci ,
            path_acj ,
            path_ack
        );
        group_aj.push(
            path_acl ,
            path_acm ,
            path_acn ,
            path_aco ,
            path_acp ,
            path_acq ,
            path_acr ,
            path_acs ,
            path_act ,
            path_acu ,
            path_acv ,
            path_acw ,
            path_acx ,
            path_acy ,
            path_acz ,
            path_ada ,
            path_adb ,
            path_adc ,
            path_add ,
            path_ade ,
            path_adf ,
            path_adg ,
            path_adh ,
            path_adi ,
            path_adj ,
            path_adk ,
            path_adl ,
            path_adm ,
            path_adn ,
            path_ado ,
            path_adp ,
            path_adq
        );
        group_ak.push(
            path_adr ,
            path_ads ,
            path_adt ,
            path_adu ,
            path_adv ,
            path_adw ,
            path_adx ,
            path_ady ,
            path_adz ,
            path_aea ,
            path_aeb ,
            path_aec ,
            path_aed ,
            path_aee ,
            path_aef ,
            path_aeg ,
            path_aeh ,
            path_aei ,
            path_aej ,
            path_aek ,
            path_ael ,
            path_aem ,
            path_aen ,
            path_aeo ,
            path_aep ,
            path_aeq ,
            path_aer ,
            path_aes ,
            path_aet ,
            path_aeu ,
            path_aev ,
            path_aew
        );
        group_al.push(
            path_aex ,
            path_aey ,
            path_aez ,
            path_afa ,
            path_afb ,
            path_afc ,
            path_afd
        );
        group_am.push(
            path_afe ,
            path_aff ,
            path_afg ,
            path_afh ,
            path_afi ,
            path_afj ,
            path_afk ,
            path_afl ,
            path_afm ,
            path_afn ,
            path_afo
        );
        group_an.push(
            path_afp ,
            path_afq ,
            path_afr ,
            path_afs ,
            path_aft ,
            path_afu ,
            path_afv ,
            path_afw ,
            path_afx ,
            path_afy ,
            path_afz
        );
        group_ao.push(
            path_aga ,
            path_agb ,
            path_agc ,
            path_agd ,
            path_age ,
            path_agf ,
            path_agg
        );

    }
}
