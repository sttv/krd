<?
include_once 'conf.php';
$page = 2;

$reg = new auth();
if (!$reg->check() || $_SESSION['admin_user'] != 1){
	header("Location: index.php");
    exit;
}

$db = mysql::getInstance();

switch($_REQUEST['action']) {
    case 'add':
		if (isset($_REQUEST['send'])) {
			if ($reg->reg($_REQUEST['login'], $_REQUEST['pass1'], $_REQUEST['pass2'], $_REQUEST['mail'], intval($_REQUEST['isadm']))) {
				header('Location: users.php');
				exit;
			}
		}
		$addact = 1;
		$titleForm = "Новый пользователь";
    case 'edit':
        if (!isset($addact)) {
			$titleForm = "Изменить пользователя";
            $uid = intval($_REQUEST['uid']);
            if (isset($_REQUEST['send']) && $uid > 0) {
                if ($reg->update($uid, $_REQUEST['login'], $_REQUEST['pass1'], $_REQUEST['pass2'], $_REQUEST['mail'], intval($_REQUEST['isadm']))) {
                    header('Location: users.php');
                    exit;
                }
            } else {
                $udata = $db->query("SELECT login_user, mail_user, is_admin FROM `users` WHERE `id_user` = '".$uid."';", 'assoc', '');
                $_REQUEST['login'] = $udata['login_user'];
				$_REQUEST['mail'] = $udata['mail_user'];
				$_REQUEST['isadm'] = intval($udata['is_admin']);
            }
        }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1><?=$titleForm?></h1>
					<hr>
					<form method="post">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<div class="control-group">
							<label for="login">Логин:</label>
							<input id="login" type="text" name="login" value="<?=@$_REQUEST['login']?>" class="span6">
						</div>
						<div class="control-group">
							<label for="pass1">Пароль:</label>
							<input id="pass1" type="password" name="pass1" class="span6">
						</div>
						<div class="control-group">
							<label for="pass2">Повторите пароль:</label>
							<input id="pass2" type="password" name="pass2" class="span6">
						</div>
						<div class="control-group">
							<label for="email">E-mail:</label>
							<input id="email" type="email" name="mail" value="<?=@$_REQUEST['mail']?>" class="span6">
						</div>
						<div class="control-group">
							<h3>Права доступа:</h3>
							<ul class="unstyled">
								<li>
									<label class="checkbox">Администратор <input type="checkbox" name="isadm" value="1" <?=($_REQUEST['isadm']==1 ? 'checked="checked"':'')?>></label>
								</li>
							</ul>
						</div>
						<div class="control-group">
							<input type="hidden" name="action" value="<?=$_REQUEST['action']?>" />
							<input type="hidden" name="uid" value="<?=$uid?>" />
							<input type="hidden" name="new" value="<?=$addact?>" />
							<input type="hidden" name="send" value="1" />
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary input-large">Сохранить</button>&nbsp;&nbsp;<a href="users.php" class="btn">Отмена</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
        <?
        break;
    case 'delete':
        if (!empty($_REQUEST['userid']) && count($_REQUEST['userid'])>0) {
            $userids = implode(',', $_REQUEST['userid']);
            $db->query("DELETE FROM users WHERE id_user IN ({$userids})");
            header('Location: users.php');
            exit;
        } else {
            header('Location: users.php');
            exit;
        }
        break;
    case 'udel':
        if (isset($_REQUEST['uid'])){
            $uid = intval($_REQUEST['uid']);
            $db->query("DELETE FROM users WHERE id_user={$uid} LIMIT 1");
            header('Location: users.php');
            exit;
        }
        break;
    default:
        $query = $db->query("SELECT * FROM users ORDER BY id_user ASC");
        while($row = mysql_fetch_assoc($query)){
            $users[$row['id_user']] = $row;
        }
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
			.table th.text-center, td.text-center {
				text-align: center;   
			}
        </style>
		<script type="text/javascript"  language="javascript">
            function toggle(source, name) {
                checkboxes = document.getElementsByName(name);
                for(var i=0, n=checkboxes.length;i<n;i++) {
                  checkboxes[i].checked = source.checked;
                }
            }
        </script>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Пользователи</h1>
					<ul class="nav nav-pills">
						<li><a href="users.php?action=add">Новый пользователь</a></li>
					</ul>
					<hr>
						<form action="" method="post">
							<input type="hidden" name="action" value="delete" />
							<table class="table table-striped table-bordered table-hover">
								<tbody>
									<tr>
										<th class="text-center">
												<label for="chdel">Выбрать всё</label>
												<input id="chdel" type="checkbox" onClick="toggle(this, 'userid[]')" />
										</th>
										<th>Логин</th>
										<th class="text-center">Администратор</th>
										<th>Действие</th>
									</tr>
									<?
									foreach($users as $uid => $user){
										echo '<tr>';
										echo '<td class="text-center"><input type="checkbox" name="userid[]" value="'.$uid.'" /></td>';
										echo '<td>'.$user['login_user'].'</td>';
										echo '<td class="text-center">'.($user['is_admin']?'<i class="icon-ok"></i>':'').'</td>';
										echo '<td><a href="users.php?action=edit&uid='.$uid.'"><i class="icon-pencil"></i>&nbsp;Edit</a>&nbsp;&nbsp;&nbsp;<a href="users.php?action=udel&uid='.$uid.'"><i class="icon-trash"></i>&nbsp;Delete</a></td>';
										echo '</tr>';
									}
									?>
									<tr>
										<td colspan="4"><button type="submit" name="send" class="btn btn-primary">Удалить выбранных пользователей</button></td>
									</tr>
								</tbody>
							</table>
						</form>
				</div>
			</div>
		</div>
    </body>
</html>
        <?
        break;
}