<?php

namespace Krd\EvacuationBundle\Modules;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Form;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Entity\Node;
use Krd\EvacuationBundle\Entity\Mark;


/**
 * Модуль - Эвакуация автотранспорта
 */
class Avto extends AbstractModule
{
    protected $formFactory;

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdEvacuationBundle:Admin:Module/avto.html.twig');
    }

    /**
     * Список элементов учитывая параметры запроса
     */
    public function getItems()
    {
        $searchFormBuilder = $this->createSearchFormBuilder();
        $searchForm = $searchFormBuilder->getForm();

        $node = null;

        if ($this->node->getNode() instanceof Node) {
            $node = $this->node->getNode();
        } elseif ($parent = $this->request->get('parent')) {
            $node = $this->em->getRepository('QCoreBundle:Node')->find($parent);
        }

        if (!$node) {
            return array();
        }

        $queryBuilder = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.parent = :parent')
            ->setParameter('parent', $node->getId())
            ->orderBy('e.date', 'DESC');

        $data = array_intersect_key($this->request->query->all(), $searchForm->all());
        $searchForm->bind($data);

        if ($searchForm->isValid()) {
            $formData = $searchForm->getData();
            if (isset($formData['mark']) && $formData['mark'] instanceof Mark) {
                $queryBuilder->andWhere('e.mark = :mark');
                $queryBuilder->setParameter('mark', $formData['mark']->getId());
            }

            if (isset($formData['number']) && !empty($formData['number'])) {
                $formData['number'] = mb_strtolower($formData['number']);
                $formData['number'] = trim($formData['number']);

                $queryBuilder->andWhere('LOWER(e.number) = :number OR LOWER(e.number) LIKE :number_like OR LOWER(e.number) LIKE :number_like_r');

                $queryBuilder->setParameter('number', $formData['number']);
                $queryBuilder->setParameter('number_like', '%'.$formData['number'].'%');

                $numberR = str_replace(' ', '', $formData['number']);
                $numberR = mb_substr($numberR, 0, 1).' '.mb_substr($numberR, 1, 3).' '.mb_substr($numberR, 4, 2).' '.mb_substr($numberR, 6, 3);
                $numberR = trim($numberR);
                $queryBuilder->setParameter('number_like_r', '%'.$numberR.'%');
            }

            if (isset($formData['date']) && !empty($formData['date'])) {
                $queryBuilder->andWhere('DATE_FORMAT(e.date, \'%d.%m.%Y\') = :date');
                $queryBuilder->setParameter('date', $formData['date']);
            }
        }

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true);
    }

    /**
     * Список эвакуированного авто
     */
    public function renderContent()
    {
        $searchFormBuilder = $this->createSearchFormBuilder();
        $searchFormBuilder->setAction($this->node->getNode()->getUrl(true));
        $searchForm = $searchFormBuilder->getForm();
        $searchForm->bind($this->request);

        return $this->twig->render('KrdEvacuationBundle:Module:avto/list.html.twig', array('list' => $this->getItems(), 'searchform' => $searchForm->createView()));
    }

    /**
     * Виджет на главной странице
     */
    public function renderWidget()
    {
        return $this->twig->render('KrdEvacuationBundle:Module:avto/widget.html.twig', array('items' => $this->getRepository()->getWidgetList()));
    }

    /**
     * Время последнего обновления списка
     * @return DateTime
     */
    public function getLastDate()
    {
        return $this->getRepository()->getLastDate();
    }

    /**
     * Создание билдера формы поиска
     * @return FormBuilder
     */
    public function createSearchFormBuilder()
    {
        return $this->formFactory
            ->createNamedBuilder(null, 'form', null, array('csrf_protection' => false))
            ->add('mark', 'entity', array(
                'class' => 'KrdEvacuationBundle:Mark',
                'property' => 'title',
                'multiple' => false,
                'required' => false,
                'empty_value' => '--- выберите из списка ---',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.sort', 'ASC');
                },
            ))
            ->add('number', 'text', array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'А 777 АА 123',
                ),
            ))
            ->add('date', 'text', array(
                'required' => false,
                'attr' => array(
                    'date-picker' => '',
                    'placeholder' => 'дд.мм.гггг',
                ),
            ))
        ;
    }
}
