/**
 * Модуль загрузчика изображений
 */
angular.module('cms.filesuploader.images', ['ui.bootstrap', 'cms.filesuploader.api', 'ui.sortable'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="images-uploader abstract-uploader">'
            + '<div class="files-list clearfix" ui-sortable="{placeholder:\'item placeholder\', forcePlaceholderSize:true, items:\'>.item\', opacity:0.8, tolerance:\'pointer\'}" ng-model="files">'
                + '<images-list-item file="file" ng-repeat="file in files"></images-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Изображения не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить изображения</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-info btn-small" tooltip="Заголовки изображений, опция Главная и сортировка сохраняется сразу, без отправки формы">'
                + '<i class="icon-question-sign icon-white"></i>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.images', template);
    }])

    /**
     * Элемент загрузчика изображений
     */
    .directive('imagesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="img-polaroid">'
                        + '<div class="btn btn-inverse sort-icon"><i class="icon-move icon-white"></i></div>'
                        + '<img ng-src="{{file.size.cms}}">'
                    + '</a>'
                    + '<input type="text" placeholder="Заголовок" ng-model="file.title">'
                    + '<label>'
                        + '<input type="checkbox" ng-model="file.main">'
                        + '&nbsp;Главная'
                    + '</label>'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="{{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i> Скачать</a>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()"><i class="icon-trash icon-white"></i> Удалить</div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление изображения';
                    var msg = 'Вы уверены, что хотите удалить изображение?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                }
            },

            link: function($scope) {
                var fst = true;
                var fst2 = true;

                // Редактирование имени файла
                $scope.$watch('file.title', function(title) {
                    if (fst) {
                        fst = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {title:title},
                        timeout: this.canceler.promise
                    });
                });

                // Редактирование чекбокса "Главная"
                $scope.$watch('file.main', function(main) {
                    if (fst2) {
                        fst2 = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {main:main},
                        timeout: this.canceler.promise
                    });
                });
            }
        }
    }])
;
