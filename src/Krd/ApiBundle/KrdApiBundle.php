<?php

namespace Krd\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * API портала
 *
 * https://docs.google.com/a/very-good.ru/document/d/1LlScSLXNGssDiXqsU-nA_LX7aeeVK38s958fGrjghwI/edit#
 */
class KrdApiBundle extends Bundle
{
}
