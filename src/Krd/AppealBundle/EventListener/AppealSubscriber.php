<?php

namespace Krd\AppealBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;
use Krd\AppealBundle\Entity\Appeal;


/**
 * События для Appeal
 */
class AppealSubscriber implements EventSubscriber
{
    protected $container;
    protected $mailer;
    protected $logger;
    protected $deferred = array();

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Сбор ново-добавленных обращений
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (($entity instanceof Appeal) && ($entity->getStatus() == Appeal::STATUS_NEW)) {
            $this->deferred[] = $entity;
        }
    }

    /**
     * Отправка Email уведомлений о новых обращения
     * Если не требуется экспорт во вне сразу ставим соответствующий статус
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $list = $this->deferred;
        $this->deferred = array();

        foreach($list as $entity) {
            /** @var Appeal $entity */

            $subject = 'Обращение в интернет-приемную %title% через '.$entity->getGotFrom();

            $message = $this->getMailer()->newMessage($subject);

            if($entity instanceof Appeal && $entity->hasAppealFiles()) {

                $this->logger->info('Подготовка к отправке ' . __LINE__);

                try {
                    $message->addTo($entity::WITH_FILE_EMAIL);

                    $files = array();
                    try {
                        foreach ($entity->getAppealFiles() as $f) {

                            $files[] = $this->container->get('doctrine')->getManager()->getRepository('QFilesBundle:File')->find($f);
                        }
                    } catch(\Exception $e) {

                        $this->logger->crit($e->getMessage());
                    } catch(\RuntimeException $e) {

                        $this->logger->crit($e->getMessage());
                    }

                    $this->getMailer()->sendTemplate($message, 'KrdAppealBundle:Email:new-appeal.file.admin.html.twig', array('appeal' => $entity, 'filesMsg' => $entity->getHtmlFilesFromHtml($files)));

                    $this->logger->info('Отправлено сообщение с вложением');
                } catch (\Exception $e) {

                    $this->logger->crit('Ошибка отправки email уведомления обращения в интернет приемной (с вложением)', array($entity->getId(), $e->getMessage()));
                }
            } else {

                foreach($entity->getDestination()->getEmails() as $email) {
                    if (!empty($email)) {
                        $message->addTo($email);
                    }
                }

                try {
                    $this->getMailer()->sendTemplate($message, 'KrdAppealBundle:Email:new-appeal.admin.html.twig', array('appeal' => $entity));
                } catch (\Exception $e) {
                    $this->logger->crit('Ошибка отправки email уведомления обращения в интернет приемной', array($entity->getId(), $e->getMessage()));
                }
            }

            if (!$entity->getDestination()->getExport()) {
                $entity->setStatus(Appeal::STATUS_MAILED);
                $args->getEntityManager()->flush($entity);
            }
        }
    }
}

