<?php

namespace Q\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Валидатор системного имени нода
 */
class NodeNameValidator extends ConstraintValidator
{
    protected $em;

    /**
     * @param entityManager $entityManager Doctrine entitymanager
     */
    public function __construct($entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $parentNode = $this->context->getRoot()->get('parent')->getData();
        $node = $this->context->getRoot()->getData();

        // Проверяем главную страницу
        if (is_null($parentNode)) {
            if ($value != '') {
                $this->context->addViolation($constraint->messageIndex);
            }

            return;
        }

        if (empty($value) || !preg_match('/^[a-z0-9\-_]+$/', $value)) {
            $this->context->addViolation($constraint->message);
            return;
        }

        $qb = $this->em->createQueryBuilder()
            ->select('n')
            ->from('QCoreBundle:Node', 'n')
            ->andWhere('n.parent = :parent')
            ->andWhere('n.name = :name')
            ->setParameter('parent', $parentNode->getId())
            ->setParameter('name', $value);

        if ($nodeId = $node->getId()) {
            $qb->andWhere('n.id != :id')
                ->setParameter('id', $nodeId);
        }

        // Список соседних нод с таким же системным именем
        $siblings = $qb->getQuery()->getResult();

        if (!empty($siblings)) {
            $this->context->addViolation($constraint->messageUnique);
            return;
        }
    }
}
