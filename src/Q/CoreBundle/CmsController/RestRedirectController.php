<?php

namespace Q\CoreBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер Redirect
 *
 * @Route("/rest/q/core/redirect")
 * @PreAuthorize("hasRole('ROLE_CMS_REDIRECT')")
 */
class RestRedirectController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'QCoreBundle:Redirect';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_q_core_redirect_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_q_core_redirect_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_q_core_redirect", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        $qb->orderBy('e.id', 'DESC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_q_core_redirect_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_q_core_redirect_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_q_core_redirect_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_q_core_redirect_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_q_core_redirect_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }
}
