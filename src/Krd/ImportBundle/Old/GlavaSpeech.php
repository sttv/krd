<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;
use Krd\GlavaBundle\Entity\Speech;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Импорт раздела прямая речь с сайта glava.krd.ru на страницу главы на сайт krd.ru
 */
class GlavaSpeech
{
    protected $em;
	protected $gem;
	
	protected $uploadDir;

    public function __construct(EntityManager $em, EntityManager $gem)
    {
        $this->em = $em;
		$this->gem = $gem;
    }
	
	public function import($output) {
		$speechList = $this->loadSpeech();
		$speechNode = $this->em->find('QCoreBundle:Node', 15266);
		
		$speechProcessed = 0;
		$this->em->getConnection()->beginTransaction();
		foreach( $speechList as $oldSpeech ) {
			$name = 'speech_'.date('dmY', strtotime($oldSpeech['date'])).'_'.$oldSpeech['id'];
			$exists = false;
			$speech = $this->em->getRepository('KrdGlavaBundle:Speech')->findBy(array('name' => $name));
			
			if( !empty($speech) ) {
				$speech = array_pop($speech);
				$speech->setRefId((int)$oldSpeech['id']);
				$speech->setTitle(htmlspecialchars_decode( $oldSpeech['name'] ));
				
				$exists = true;
			} else {
				$speech = new Speech();
				$speech->setRefId((int)$oldSpeech['id']);
				$speech->setTitle(htmlspecialchars_decode( $oldSpeech['name'] ));
				
				// /administratsiya/struktura-administratsii/glava_goroda/ вот этот раздел у новостей родитель
				$speech->setParent($speechNode);
				$speech->setName($name);
				$speech->setDate(new \DateTime($oldSpeech['date']));
				$speech->setContent($oldSpeech['desc']);
				$speech->setActive(true);
				$this->em->persist($speech);
			}
			
			$this->em->flush();

			$speechProcessed++;
			if ($speechProcessed % 30 == 0) {
				$this->em->getConnection()->commit();
                $this->em->clear('KrdGlavaBundle:Speech');
                $output->writeln("Processed {$speechProcessed}");
                $this->em->getConnection()->beginTransaction();
			}
		}
		$this->em->getConnection()->commit();
		$output->writeln('imported '.$speechProcessed.' speeches...');
	}
	
	private function loadSpeech()
	{
		$list = $this->gem->getConnection()->prepare('SELECT * FROM evlanov__documents');
		$list->execute();
		
		$speeches = array();
		while( $row = $list->fetch() ) {
			$speeches[] = $row;
		}
		
		return $speeches;
	}
}