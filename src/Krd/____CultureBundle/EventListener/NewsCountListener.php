<?php

namespace Krd\GlavaBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Krd\NewsBundle\Entity\News;
use Krd\NewsBundle\Entity\NewsTheme;
use Q\CoreBundle\Entity\NodeUrlIntf;


/**
 * Listener тем новостей, который определяет количество новостей в теме
 */
class NewsCountListener
{
    /**
     * Symfony router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof NewsTheme) {
            $em = $args->getEntityManager();
			$query = $em->createQuery("SELECT count(t.id) FROM KrdNewsBundle:News t WHERE ?1 MEMBER OF t.themes");
			$query->setParameter( 1, $entity->getId() );
			$entity->setNewsCount($query->getSingleScalarResult());
        }
    }
}
