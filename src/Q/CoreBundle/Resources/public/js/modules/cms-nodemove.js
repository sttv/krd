/**
 * Перенос разделов
 */
angular.module('cms.nodemove', ['cms.entity'])
    .directive('nodeMoveBlock', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.url = '';
                $scope.loading = false;
                $scope.httpUrl = attrs.nodeMoveBlock;
                $scope.moveUrl = attrs.moveUrl;
                $scope.node = false;

                $scope.$watch('url', $scope.updateData);
            },

            controller: function($scope) {
                $scope.updateData = function() {
                    $scope.node = false;

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    if (!$scope.url) {
                        return;
                    }

                    this.canceler = $q.defer();

                    this.canceler = $q.defer();

                    $http({
                        method: 'GET',
                        url: $scope.httpUrl,
                        params: {url: $scope.url},
                        timeout: this.canceler.promise
                    }).success(function(response) {
                        if (response.success && !!response.node) {
                            $scope.node = response.node;
                        }
                    });
                };

                $scope.submit = function(movetype) {
                    $scope.loading = true;

                    $http({
                        method: 'POST',
                        url: $scope.moveUrl,
                        data: {id2: $scope.node.id, movetype:movetype}
                    }).success(function(response) {
                        $scope.loading = false;

                        if (response.success && !!response.redirect) {
                            $rootScope.$broadcast('ntf-success', 'Раздел успешно перенесен, сейчас вы будете перенаправлены', 5000);
                            window.location.href = response.redirect;
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела', 5000);
                        }
                    }).error(function(response, code) {
                        $scope.loading = false;

                        if (code > 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела', 5000);
                            console.error(response);
                        }
                    });
                };

                $scope.submitFirst = function() {
                    $scope.submit('innerfirst')
                };

                $scope.submitLast = function() {
                    $scope.submit('innerlast')
                };
            }
        }
    }])
;
