<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use KrdSiteBundle\Entity\Poll as PollEntity;
use KrdSiteBundle\Entity\PollAnswer;


/**
 * Модуль - опросы
 */
class Poll extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/poll.html.twig');
    }

    /**
     * Виджет для главной
     */
    public function renderWidget()
    {
        $polls = $this->getRepository()
            ->createQuery("SELECT e, RAND() as HIDDEN rand FROM __CLASS__ e WHERE e.active = 1 ORDER BY rand")
            ->setMaxResults(1)
            ->getResult();

        if (isset($polls[0])) {
            return $this->twig->render('KrdSiteBundle:Module:poll/widget.html.twig', array('poll' => $polls[0]));
        }

    }
}
