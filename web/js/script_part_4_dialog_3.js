/**
 * Всплывающие окна
 */
angular.module('dialog', [])
    .config(['$anchorScrollProvider', function($anchorScrollProvider) {
        $anchorScrollProvider.disableAutoScrolling();
    }])

    /**
     * Сервис диалогов
     * onClose - событие по закрытию диалога
     */
    .service('$dialog', ['$compile', '$timeout', '$rootScope', function($compile, $timeout, $rootScope) {

        return {
            create: function(template, options) {
                options = angular.extend({
                    onClose: function() {},
                    'class': ''
                }, options);

                var $dialog = $('<div dialog-window="'+template+'" ng-class="\''+options['class']+'\'"></div>');
                $dialog.appendTo('body');

                $timeout(function() {
                    $compile($dialog)($rootScope.$new(true));
                    $dialog.scope().onClose = options.onClose;
                });
            }
        }
    }])

    /**
     * Кнопки показа всплывающиего окна
     */
    .directive('dialogShow', ['$dialog', function($dialog) {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                $element.on('click', function(e) {
                    e.preventDefault();
                    $dialog.create(attrs.dialogShow, {
                        'class': attrs.dialogClass
                    });
                });
            },

            controller: function($scope, $element) {
            }
        }
    }])

    /**
     * Окно диалога
     */
    .directive('dialogWindow', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: {
                template: '@dialogWindow'
            },
            template: '<div class="dialog">'
                        + '<div class="icon-close close-btn" ng-click="hide()"></div>'
                        + '<div class="w" ng-include="template" onload="show()"></div>'
                    + '</div>',
            replace: true,

            link: function($scope, $element) {
                $element.hide();
                $scope.state = 'hidden';

                $(document).ready($scope.relocate);
                $(window).load($scope.relocate);
                $(window).resize($scope.relocate);
                $timeout($scope.relocate, 100);

                $scope.$overlay = $('<div class="dialog-overlay"></div>');
                $scope.$overlay.appendTo('body');
                $scope.$overlay.on('click', function() {
                    $scope.hide();
                });

                $timeout(function() {
                    if ($scope.state == 'hidden') {
                        $scope.$overlay.after('<s class="l"></s>');
                    }
                }, 300);
            },

            controller: function($scope, $element) {
                /**
                 * Обновляем позиционирование на экране
                 */
                $scope.relocate = function() {
                    if (document.body.clientHeight < $element.height()) {
                        $element.css({
                            position: 'absolute',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: 0,
                            top: $(window).scrollTop() + 15
                        });
                    } else {
                        $element.css({
                            position: 'fixed',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: -$element.height() / 2
                        });
                    }
                };

                $scope.show = function() {
                    $scope.relocate();
                    $scope.state = 'visible';

                    $element.stop(true, true).css({opacity:1}).fadeIn(300);
                    $scope.$overlay.stop(true, true).css({display:'block'}).animate({opacity:0.5}, 300, function() {
                        $timeout($scope.relocate);
                        $timeout($scope.relocate, 100);
                        $scope.$overlay.next('.l').remove();
                    });
                };

                $scope.hide = function() {
                    $scope.state = 'hidden';
                    $element.stop(true, true).fadeOut(200);
                    $scope.$overlay.stop(true, true).fadeOut(200, function() {
                        $scope.$overlay.remove();
                        $element.remove();
                        $scope.onClose();
                    });
                };
            }
        }
    }])
;
