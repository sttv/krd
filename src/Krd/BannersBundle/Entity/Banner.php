<?php

namespace Krd\BannersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\FilesBundle\Entity\Image;


/**
 * Баннер
 *
 * @ORM\Entity(repositoryClass="Krd\BannersBundle\Repository\BannerRepository")
 * @ORM\Table(name="banner",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="type", columns={"type"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="blank", columns={"blank"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     type={"hidden"},
 *     title={"text", {"label"="Заголовок"}},
 *     url={"text", {"label"="URL"}},
 *     blank={"checkbox", {
 *         "label"="В новом окне",
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображение",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     marginTop={"text", {"label"="Смещение картинки от верха"}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Banner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Тип баннера
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $type;

    /**
     * URL
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $url = '#';


    /**
     * Открыть в новом окне
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $blank = false;


    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="banner_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="banner_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * Смещение картинки от верха
     * @ORM\Column(type="integer", name="margin_top")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $marginTop = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getImage")
     */
    private $mainImage;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getBlank()
    {
        return $this->blank;
    }

    public function setBlank($blank)
    {
        $this->blank = (boolean)$blank;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    /**
     * Первое изображение
     */
    public function getImage()
    {
        foreach($this->getImages() as $image) {
            return $image;
        }
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function getMarginTop()
    {
        return $this->marginTop;
    }

    public function setMarginTop($marginTop)
    {
        $this->marginTop = $marginTop;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}


