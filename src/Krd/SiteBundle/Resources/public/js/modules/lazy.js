angular.module('lazy', [])
    /**
     * Загрузка изображения только если оно видимо на странице
     * Без учета скрола, очень проставя реализация
     */
    .directive('lazyImage', [function() {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var src = attrs.original;

                if (!$element.is('img')) {
                    return;
                }

                if (!!$.fn.appear) {
                    $element.appear();

                    $element.one('appear', function() {
                        $element.attr('src', src).removeClass('lazy-image');
                    });
                } else {
                    var interval = setInterval(function() {
                        if ($element.eq(0).is(':visible')) {
                            $element.attr('src', src).removeClass('lazy-image');
                            clearInterval(interval);
                        }
                    }, 100);
                }

            }
        }
    }])
;
