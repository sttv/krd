<?php

namespace Krd\UserBundle\Modules;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\ConferenceBundle\Modules\Online as OnlineModule;
use Krd\UserBundle\Entity\Widget;


/**
 * Модуль онлайн конференций личного кабинета
 */
class ConferenceModule extends AbstractModule
{
    protected $securityContext;

    public function setSecurityContext(SecurityContext $sc)
    {
        $this->securityContext = $sc;
    }

    public function getUser()
    {
        if ($this->securityContext) {
            return $this->securityContext->getToken()->getUser();
        }
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    /**
     * Построение списка
     * @return array
     */
    public function getItems()
    {
        return array('itemsFuture' => $this->getItemsFuture(), 'itemsPast' => $this->getItemsPast());
    }

    /**
     * Список будущих конференций
     * @return ArrayColelction
     */
    public function getItemsFuture()
    {
        return $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.dateworkstart <= :now')
            ->andWhere('d.dateworkend >= :now')
            ->addOrderBy('d.dateworkstart', 'DESC')
            ->addOrderBy('d.dateworkend', 'DESC')
            ->innerJoin('d.questions', 'q', 'WITH', 'q.createdBy = :user')
            ->setParameter('now', new \DateTime())
            ->setParameter('user', $this->getUser()->getId())
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();
    }

    /**
     * Список прошедших конференций
     * @return Paginator
     */
    public function getItemsPast()
    {
        $itemsPastQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.dateworkend <= :now')
            ->addOrderBy('d.dateworkstart', 'DESC')
            ->addOrderBy('d.dateworkend', 'DESC')
            ->innerJoin('d.questions', 'q', 'WITH', 'q.createdBy = :user')
            ->setParameter('user', $this->getUser()->getId())
            ->setParameter('now', new \DateTime())
            ->getQuery();

        return Paginator::createFromRequest($this->request, $itemsPastQuery);
    }

    public function renderContent()
    {
        return $this->twig->render('KrdUserBundle:Module:conference/index.html.twig', $this->getItems());
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget(Widget $widget)
    {
        $items = $this->getItemsFuture();

        return $this->twig->render('KrdUserBundle:Module:conference/widget.html.twig', array(
            'conference' => (isset($items[0]) ? $items[0] : null),
            'widget' => $widget,
        ));
    }
}
