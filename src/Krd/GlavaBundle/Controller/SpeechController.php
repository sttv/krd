<?php

namespace Krd\GlavaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Видео
 */
class SpeechController extends Controller implements ActiveSecuredController
{
    /**
     * Список видео
     *
     * @Route("/")
     * @Template()
     */
	// @Secure(roles="ROLE_CMS")
    public function listAction()
    {
    }
	
	/**
	 * Детальная страница речи
	 * @Route("/{name}.html", name="krd_glava_speech_detail")
	 * @Template()
	 */
	// @Secure(roles="ROLE_CMS")
	public function detailAction()
	{
	}
	
	/**
     * Список речей для постраничного вывода
     * @Route("/ajax/glava/speech/list.json", name="ajax_glava_speech_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
    	$queryBuilder = $this->get('krd.glava.modules.speech')->getRepository()
            ->createQueryBuilder('v')
            ->andWhere('v.active = 1')
            ->andWhere('v.parent = :parent')
            ->orderBy('v.date', 'DESC')
            ->setParameter('parent', $request->get('parent'));
		
		if ($date = $request->get('date')) {
            $date = new \DateTime($date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') = :date');
            $queryBuilder->setParameter('date', $date->format('Y-m-d'));
        }
		
		if ($date = $request->get('date_begin')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') >= :date1');
            $queryBuilder->setParameter('date1', $date->format('Y-m-d'));
        }
		
		if ($date = $request->get('date_end')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') <= :date2');
            $queryBuilder->setParameter('date2', $date->format('Y-m-d'));
        }
		
        return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 10);
    }
	
	/**
	 * Список тем по новостям
	 * @Route("/ajax/glava/speech/range.html", name="ajax_glava_speech_range", defaults={"_format"="html"})
	 * @Method({"GET"})
	 */
	public function rangeAction(Request $request) 
	{
		$dates = $this->getDatesList($request->get('parent'));

        $date = \DateTime::createFromFormat('d-m-Y', date('Y-m-d'));
		
		
        return $this->render('KrdGlavaBundle:Module:speech/range.html.twig', array(
                'availDates' => $dates,
				'date_begin' => $request->get('date_begin'),
				'date_end' => $request->get('date_end'),
                'nowDate' => $date,
            ));
	}
	
	/**
     * Список дат за которые существуют новости
     * @param  integer[optional] $parentId
     * @return array
     */
    public function getDatesList($parentId = null) {
        if (is_null($parentId)) {
            $dates = $this->getDoctrine()->getManager()->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM KrdGlavaBundle:Speech n WHERE n.active = 1 GROUP BY formatted")
            ->useResultCache(true, 15)
            ->getResult();
        } else {
            $dates = $this->getDoctrine()->getManager()->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM KrdGlavaBundle:Speech n WHERE n.active = 1 AND n.parent = :parent GROUP BY formatted")
            ->setParameter('parent', $parentId)
            ->useResultCache(true, 15)
            ->getResult();
        }

        foreach($dates as &$date) {
            $date = $date['date'];
        }
        unset($date);

        return $dates;
    }
}