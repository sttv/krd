<?php

namespace Krd\SiteBundle\CmsController;

use DateTime;
use Krd\SiteBundle\Entity\MSP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер MSP
 *
 * @Route("/rest/krd/msp")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestMSPController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdSiteBundle:MSP';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_msp_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_msp_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_msp", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->andWhere('e.archive = :archive');
        $qb->setParameter('archive', $request->get('archive', 0));

        $qb->addOrderBy('e.recNum1', 'DESC');
        $qb->addOrderBy('e.recNum2', 'DESC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_msp_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_msp_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_msp_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->request->get('active', false) === true || $request->request->get('active', false) == 'true');
        $entity->setArchive($request->request->get('archive', false) === true || $request->request->get('archive', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_msp_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        if ($entity instanceof MSP) {
            $entity->setDateInc(new DateTime());
            $entity->setDateApprove(new DateTime());
        }

        if (($parent = $request->get('parent')) && ($parent = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->find($parent))) {
            $entity->setParent($parent);
        }
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_msp_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Загрузка CSV файла и последующая подготовка его для импорта
     * @Route("/upload/{parent}", name="cms_rest_krd_msp_upload", requirements={"parent"="\d+", "format"="xml"})
     * @Method("POST")
     */
    public function uploadCSVFile(Request $request, $parent)
    {
        if (($file = $request->files->get('file')) && $file->isValid()) {
            if ($file->getMimeType() == 'application/zip') {
                $dir = sys_get_temp_dir() . '/mspimport/' . uniqid();

                if ($file = $file->move($dir, uniqid('archive-') . '.zip')) {
                    $zip = new \ZipArchive();

                    if ($zip->open($file->getRealPath())) {
                        $zip->extractTo($dir);
                        $zip->close();

                        $finder = new Finder();
                        $finder->files()->depth('== 0')->name('*.csv')->in($dir);

                        $this->getDoctrine()->getManager()->getConnection()->beginTransaction();
                        $this->get('krd.site.modules.msp')->deactivateAll();

                        foreach ($finder as $file) {
                            $res = $this->get('krd.site.modules.msp')->importCSV($file->getRealPath(), $parent);

                            if ($res) {
                                $this->get('session')->getFlashBag()->add('cms', 'Файл импортирован: ' . basename($file->getRealPath()));
                            } else {
                                $this->get('session')->getFlashBag()->add('cms-error', 'Не удалось импортировать файл ' . basename($file->getRealPath()));
                            }
                        }

                        $this->getDoctrine()->getManager()->getConnection()->commit();

                        return $this->redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $this->get('session')->getFlashBag()->add('cms-error', 'ZIP архив поврежден, не удается его открыть');
                        return $this->redirect($_SERVER['HTTP_REFERER']);
                    }
                } else {
                    $this->get('session')->getFlashBag()->add('cms-error', 'Ошибка. Не работает временная папка');
                    return $this->redirect($_SERVER['HTTP_REFERER']);
                }
            } else {
                $this->get('session')->getFlashBag()->add('cms-error', 'Файл должен быть ZIP архивом');
                return $this->redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $this->get('session')->getFlashBag()->add('cms-error', 'Файл не загружен');
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
