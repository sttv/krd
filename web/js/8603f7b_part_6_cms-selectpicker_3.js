angular.module('cms.selectpicker', []).directive('selectpicker', ['$timeout', function ($timeout) {
    var NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w\d]*)|(?:\(\s*([\$\w][\$\w\d]*)\s*,\s*([\$\w][\$\w\d]*)\s*\)))\s+in\s+(.*)$/;
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function postLink(scope, element, attrs, controller) {
            var options = scope.$eval(attrs.selectpicker) || {};

            $timeout(function () {
                element.selectpicker(options);
                element.next().removeClass('ng-scope');
            });

            if (controller) {
                scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        element.selectpicker('refresh');
                    }
                });
            }
        }
    };
}]);
