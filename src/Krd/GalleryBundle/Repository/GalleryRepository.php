<?php

namespace Krd\GalleryBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;

use Krd\GalleryBundle\Entity\Gallery;


/**
 * Репозиторий для галереи
 */
class GalleryRepository extends EntityRepository
{
    /**
     * Получение одной активной галереи для главной
     * @return Gallery
     */
    public function findOneByActive()
    {
        $items = $this->createQuery("SELECT g FROM __CLASS__ g WHERE g.active = 1 AND g.parent IN (58, 16) ORDER BY g.date DESC")
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (!empty($items) && isset($items[0])) {
            return $items[0];
        }
    }
}
