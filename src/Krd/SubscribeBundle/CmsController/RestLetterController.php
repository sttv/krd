<?php

namespace Krd\SubscribeBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;
use Krd\SubscribeBundle\Entity\Letter;
use Q\CoreBundle\Entity\Content;
use Krd\NewsBundle\Entity\News;


/**
 * Контроллер рассылок
 *
 * @Route("/rest/krd/subscribe/letter")
 * @PreAuthorize("hasRole('ROLE_CMS_SUBSCRIBE')")
 */
class RestLetterController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdSubscribeBundle:Letter';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_subscribe_letter_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_subscribe_letter_edit';
    }

    /**
     * Предпросмотр
     *
     * @Route("/preview/{id}", name="cms_rest_krd_subscribe_letter_preview", defaults={"_format"="json"})
     * @Method("GET")
     * @Template("KrdSubscribeBundle:Email:letter.html.twig")
     */
    public function previewAction(Request $request, $id)
    {
        $letter = $this->getRepository()->find($id);

        if ($letter) {
            return array('letter' => $letter);
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список
     * Можно передавать $_GET['parent'] для выборки по радителю
     *
     * @Route("/", name="cms_rest_krd_subscribe_letter", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        $qb->addOrderBy('e.date', 'DESC');
        $qb->addOrderBy('e.id');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_subscribe_letter_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $letter = new Letter();

        $letter->setNewsTitle($request->request->get('news_title'));
        $letter->setTitle($request->request->get('title'));
        $letter->setDate(new \DateTime($request->request->get('date')));
        $letter->setActive(true);
        $letter->setSent(false);

        // Добавляем новости
        $newsRepo = $this->getDoctrine()->getManager()->getRepository('KrdNewsBundle:News');

        $news = $request->request->get('news_list', array());
        if (!is_array($news)) {
            $news = array($news);
        }

        foreach ($news as $newsId) {
            $letter->addNew($newsRepo->find($newsId));
        }

        // Добавляем контентные блоки
        $blockContents = $request->request->get('block_content', array());

        foreach ($request->request->get('block_title', array()) as $i => $blockTitle) {
            if (empty($blockTitle) || empty($blockContents[$i])) {
                continue;
            }

            $content = new Content();
            $content->setTitle($blockTitle);
            $content->setName(uniqid('letter-'));
            $content->setContent($blockContents[$i]);

            $letter->addContent($content);
        }

        $this->getDoctrine()->getManager()->persist($letter);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true, 'entity' => $letter);
    }

    /**
     * Редактривание
     *
     * @Route("/{id}", name="cms_rest_krd_subscribe_letter_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        /** @var Letter $letter */
        $letter = $this->getRepository()->find($id);

        if (!$letter) {
            throw $this->createNotFoundException();
        }

        $this->getDoctrine()->getManager()->getConnection()->beginTransaction();

        foreach ($letter->getContents() as $content) {
            $this->getDoctrine()->getManager()->remove($content);
        }

        $letter->setNewsTitle($request->request->get('news_title'));
        $letter->setTitle($request->request->get('title'));
        $letter->setDate(new \DateTime($request->request->get('date')));
        $letter->clearContents();
        $letter->clearNews();

        // Добавляем новости
        $newsRepo = $this->getDoctrine()->getManager()->getRepository('KrdNewsBundle:News');

        $news = $request->request->get('news_list', array());
        if (!is_array($news)) {
            $news = array($news);
        }

        foreach ($news as $newsId) {
            $letter->addNew($newsRepo->find($newsId));
        }

        // Добавляем контентные блоки
        $blockContents = $request->request->get('block_content', array());

        foreach ($request->request->get('block_title', array()) as $i => $blockTitle) {
            if (empty($blockTitle) || empty($blockContents[$i])) {
                continue;
            }

            $content = new Content();
            $content->setTitle($blockTitle);
            $content->setName(uniqid('letter-'));
            $content->setContent($blockContents[$i]);

            $letter->addContent($content);
        }

        $this->getDoctrine()->getManager()->persist($letter);
        $this->getDoctrine()->getManager()->flush();
        $this->getDoctrine()->getManager()->getConnection()->commit();

        return array('success' => true, 'entity' => $letter);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_subscribe_letter_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Список новостей для подписок
     * @param array $include Добавить так же эти новости
     */
    protected function getFormNewsList($include = array())
    {
        $news = array();
        $em = $this->getDoctrine()->getManager();

        $nodesQuery = $em->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('n')
            ->addSelect('FIELD(n.id, 10358, 10359, 10489, 10490, 6, 7, 15188) as HIDDEN ord')
            ->andWhere('n.controller IN (3, 57)')
            ->addOrderBy('ord', 'DESC')
            ->getQuery();

        $newsQuery = $em->getRepository('KrdNewsBundle:News')
            ->createQueryBuilder('n')
            ->andWhere('n.parent = :parent AND n.date >= :date')
            ->addOrderBy('n.date', 'DESC')
            ->setMaxResults(50);

        if (!empty($include) && count($include) > 0) {
            $newsQuery->orWhere('n.id IN (:ids) AND n.parent = :parent');

            $ids = array();

            foreach ($include as $item) {
                $ids[] = $item->getId();
            }

            $newsQuery->setParameter('ids', $ids);
        }

        foreach ($nodesQuery->getResult() as $node) {
            $newsQuery->setParameter('parent', $node);
            $newsQuery->setParameter('date', date('Y-m-d 00:00:00', time() - 60*60*24*3));

            $news[] = array(
                'node' => $node,
                'items' => $newsQuery->getQuery()->getResult(),
            );
        }

        return $news;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_subscribe_letter_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("KrdSubscribeBundle:Admin:form/create.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return array('newslist' => $this->getFormNewsList(), 'date' => new \DateTime());
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_subscribe_letter_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("KrdSubscribeBundle:Admin:form/edit.html.twig")
     */
    public function editFormAction($id)
    {
        $letter = $this->getRepository()->find($id);

        if (!$letter) {
            throw $this->createNotFoundException();
        }

        return array('letter' => $letter, 'newslist' => $this->getFormNewsList($letter->getNews()));
    }
}
