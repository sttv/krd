<?php

namespace Krd\GlavaBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Krd\NewsBundle\Entity\News;
use Krd\NewsBundle\Entity\NewsTheme;
use Q\CoreBundle\Entity\NodeUrlIntf;


/**
 * Listener тем новостей, который определяет количество новостей в теме
 */
class CommentCountListener
{
    /**
     * Symfony router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            $em = $args->getEntityManager();
			$url = $entity->getParent()->getUrl(true) . $entity->getName() . '.html';
			$entity->setCountComment( $em->getRepository('KrdCommentsBundle:Comment')->getCount($url) );
        }
    }
}
