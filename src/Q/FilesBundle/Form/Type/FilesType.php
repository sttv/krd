<?php

namespace Q\FilesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Doctrine\ORM\EntityRepository;

use JMS\Serializer\Serializer;

use Q\FilesBundle\Entity\File;


/**
 * Тип поля для загрузки файлов
 * Поддерживает файлы, изображений и видео
 */
class FilesType extends AbstractType
{
    protected $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'multiple' => true,
            'required' => false,
            'class' => 'QFilesBundle:File',
            'widget' => 'files',
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('e')->setMaxResults(500);
            },
        ));

        $resolver->addAllowedValues(array(
            'widget' => array('files', 'images', 'video'),
            'multiple' => array(true),
        ));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($data = $form->getParent()->getData()) {
            $method = 'get'.ucfirst($view->vars['name']);
            $files = $data->$method();
        } else {
            $files = array();
        }

        $view->vars['files'] = $this->serializer->serialize($files, 'json');
        $view->vars['widget'] = $options['widget'];

        switch($options['widget']) {
            case 'files':
                $view->vars['route_name'] = 'cms_rest_qfiles_file_create';
                break;

            case 'images':
                $view->vars['route_name'] = 'cms_rest_qfiles_image_create';
                break;

            case 'video':
                $view->vars['route_name'] = 'cms_rest_qfiles_video_create';
                break;

            default:
                throw new \Exception('You should never see it! Ha-ha...');
        }
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'files';
    }
}
