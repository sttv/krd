<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Filesystem\Filesystem;

use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Krd\DocumentBundle\Entity\Document;


/**
 * Импорт контентных страниц в качестве нормо-документов
 */
class ImportContentAsNormaCommand extends ContainerAwareCommand
{
    protected $em;
    protected $oldStructure;
    protected $uploadDir;

    protected function configure()
    {
        $this
            ->setName('krd:import:content:as:norma')
            ->setDescription('Import content pages from old site krd.ru and insert it by Document entity')
            ->addArgument('source', InputArgument::REQUIRED, 'Old site absolute url')
            ->addArgument('destination', InputArgument::REQUIRED, 'New site absolute url')
            ->setHelp(<<<EOT
                      The <info>krd:import:structure</info> import content pages from old site krd.ru and insert it by Document entity.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->oldStructure = $this->getContainer()->get('krd_import.old.structure');
        $this->uploadDir = $this->getContainer()->getParameter('kernel.root_dir').'/../web/oldkrd';

        $sourceUrlRes = $input->getArgument('source');
        $destinationUrlRes = $input->getArgument('destination');

        $validator = Validation::createValidator();
        $assert = new Assert\Url(array('protocols' => array('http')));

        // Проверка валидности URL
        $violations = $validator->validateValue($sourceUrlRes, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Source url errors '.$violations);
        }

        $violations = $validator->validateValue($destinationUrlRes, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Destination url errors '.$violations);
        }

        $output->writeln('<info>------------------------------ Import start at '.date('H:i:s').' ------------------------------</info>');

        $sourceUrlRes = rtrim($sourceUrlRes, '/ ').'/';
        $destinationUrlRes = rtrim($destinationUrlRes, '/ ').'/';

        $output->write('<comment>Importing</comment> ['.$sourceUrlRes.'] to ['.$destinationUrlRes.']...');

        $sourceUrl = parse_url($sourceUrlRes);
        $destinationUrl = parse_url($destinationUrlRes);

        if ($sourceUrl['path'] == '/') {
            $output->writeln('<error>Source url too short</error>');
            usleep(200);
            return;
        }

        if ($destinationUrl['path'] == '/') {
            $output->writeln('<error>Destination url too short</error>');
            usleep(200);
            return;
        }

        $destinationNode = $this->em->getRepository('QCoreBundle:Node')->getNodeByPath($destinationUrl['path']);

        if (!$destinationNode) {
            $output->writeln('<error>Destination url not found on this site</error>');
            usleep(200);
            return;
        }

        // Фиксим обращение к странице по алиасу
        if ($destinationNode->getUrlAlias() == $destinationUrl['path']) {
            $destinationUrl['path'] = $destinationNode->getUrl(false);
            $destinationUrlRes = $destinationUrl['scheme'].'://'.$destinationUrl['host'].$destinationUrl['path'];
        }

        $tmp = explode('/', trim($destinationUrl['path'], '/ '));
        $expectedLevel = count($tmp);

        if ($expectedLevel - $destinationNode->getLevel() > 1) {
            $output->writeln('<error>Please create some structure of '.$destinationUrl['path'].'</error>');
            usleep(200);
            return;
        }

        $sourceNode = $this->oldStructure->getByPath($sourceUrl['path']);

        if (!$sourceNode) {
            $output->writeln('<error>Source url not found on krd.ru</error>');
            usleep(200);
            return;
        }

        $tmp = explode('/', trim($sourceNode['path'], '/ '));

        $document = new Document();

        $document->setTitle($sourceNode['title']);
        $document->setName(array_pop($tmp));
        $document->setContent($sourceNode['content']['content']);
        $document->setParent($destinationNode);
        if ($sourceNode['content']['date'] instanceof \DateTime) {
            $document->setDate($sourceNode['content']['date']);
        } else {
            $document->setDate(new \DateTime());
        }
        $document->setActive(true);

        $this->em->persist($document);
        $this->em->flush();

        if (!empty($sourceNode['files'])) {
            foreach($sourceNode['files'] as $fileArr) {
                $filesystem = new Filesystem();
                $savePath = $this->uploadDir.str_replace('/files/file', '', $fileArr['path']);
                $filesystem->dumpFile($savePath, file_get_contents($fileArr['url']));

                $fileInfo = new \Symfony\Component\HttpFoundation\File\File($savePath);

                $file = new File();
                $file->setTitle($fileArr['title']);
                $file->setDate($fileArr['date']);
                $uploadedArray = array(
                    'fileName' => str_replace('/files/file', '', $fileArr['path']),
                    'originalName' => basename($fileArr['file']),
                    'mimeType' => $fileInfo->getMimeType(),
                    'size' => $fileInfo->getSize(),
                    'path' => str_replace('/files/file', '/'.basename($this->uploadDir), $fileArr['path']),
                );

                $this->em->persist($file);
                $document->addFiles($file);
                $this->em->flush();

                $file->setFileArray($uploadedArray);
                $this->em->flush();
            }
        }

        $output->writeln('imported ['.$document->getId().']');

        $output->writeln('<info>----------------------------- Import complete at '.date('H:i:s').' ----------------------------</info>');
    }
}
