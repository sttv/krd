/**
 * Подписка на новости
 */
angular.module('subscribe', [])
    .controller('subscribeFormCtrl', ['$scope', '$element', '$http', function($scope, $element, $http) {
        $scope.onSuccess = function(response) {
            $('#subscriber-form-success').appendTo($element.parent()).slideUp(0).slideDown();
            $element.slideUp();
        };

        $scope.unsubscribe = function() {
            var email = $element.find('input[type="text"]').val();

            if (email) {
                $http({
                    method: 'POST',
                    url: $element.attr('action') + '?unsubscribe=1',
                    data: {email: email}
                });

                $('#subscriber-un-form-success').appendTo($element.parent()).slideUp(0).slideDown();
                $element.slideUp();
            }
        }
    }])
;
