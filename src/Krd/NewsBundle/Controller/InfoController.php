<?php

namespace Krd\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Новая информация
 */
class InfoController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * Страница подробно
     *
     * @Route("/{name}.html", name="krd_news_info_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.news.modules.info')->getCurrent()) {
            if ($item->hasRedirect()) {
                return $this->redirect($item->getRedirect());
            }

            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/news/info/list.json", name="ajax_news_info_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.news.modules.info')->getItemsList($request->query->get('parent', 1));
    }
}
