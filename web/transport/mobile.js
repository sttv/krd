(function (a) {
	a.fn.popup = function (a) {
		return new c(this[0], a)
	};
	var b = [], c = function () {
		var c = function (c, d) {
			typeof c === "string" || c instanceof String ? this.container = document.getElementById(c) : this.container = c;
			if (this.container)try {
				if (typeof d === "string" || typeof d === "number")d = {
					message: d,
					cancelOnly: "true",
					cancelText: "OK"
				};
				this.id = id = d.id = d.id || a.uuid();
				var e = this;
				this.title = d.suppressTitle ? "" : d.title || "", this.message = d.message || "", this.cancelText = d.cancelText || "Cancel", this.cancelCallback = d.cancelCallback || function () {
				}, this.cancelClass = d.cancelClass || "btn btn-primary btn-lg", this.doneText = d.doneText || "Done", this.doneCallback = d.doneCallback || function (a) {
					a.hide()
				}, this.doneClass = d.doneClass || "btn btn-primary btn-lg", this.cancelOnly = d.cancelOnly || !1, this.onShow = d.onShow || function () {
				}, this.autoCloseDone = d.autoCloseDone !== undefined ? d.autoCloseDone : !0;
				if (b.length && b[0].message == this.message)return;
				b.push(this), b.length == 1 && this.show()
			} catch (f) {
				console.log("error adding popup " + f)
			} else alert("Error finding container for popup " + c)
		};
		c.prototype = {
			id: null,
			title: null,
			message: null,
			cancelText: null,
			cancelCallback: null,
			cancelClass: null,
			doneText: null,
			doneCallback: null,
			doneClass: null,
			cancelOnly: !1,
			onShow: null,
			autoCloseDone: !0,
			supressTitle: !1,
			show: function () {
				var b = this, c = a("#" + b.id);
				if (!c.length || !c.parent().length || c.hasClass("hidden")) {
					var d = ["<div id=\"", this.id, "\" class=\"jqPopup hidden\">", "<header>", this.title, "</header>", "<div><div style=\"width:1px;height:1px;-webkit-transform:translate3d(0,0,0);float:right\"></div>", this.message, "</div>", "<footer style=\"clear:both;\">", "<a href=\"javascript:;\" class=\"", this.cancelClass, "\" id=\"cancel\">", this.cancelText, "</a>", "<a href=\"javascript:;\" class=\"", this.doneClass, "\" id=\"action\">", this.doneText, "</a>", "</footer>", "</div></div>"].join("");
					a(this.container).append(a(d)), a("#" + this.id).bind("close", function () {
						b.hide()
					}), this.cancelOnly && (a("#" + this.id).find("A#action").hide(), a("#" + this.id).find("A#cancel").addClass("center")), a("#" + this.id).find("A").each(function () {
						var c = a(this);
						c.bind("click", function (a) {
							c.attr("id") == "cancel" ? (b.cancelCallback.call(b.cancelCallback, b), b.hide()) : (b.doneCallback.call(b.doneCallback, b), b.autoCloseDone && b.hide()), a.preventDefault()
						})
					}), b.positionPopup(), a.blockUI(.5), a("#" + b.id).removeClass("hidden"), a("#" + b.id).bind("orientationchange", function () {
						b.positionPopup()
					}), this.onShow(this)
				}
			},
			hide: function () {
				var b = this;
				a("#" + b.id).addClass("hidden"), a.unblockUI(), setTimeout(function () {
					b.remove()
				}, 250)
			},
			remove: function () {
				var c = this, d = a("#" + c.id);
				d.find("BUTTON#action").unbind("click"), d.find("BUTTON#cancel").unbind("click"), d.unbind("orientationchange").remove(), d.parent().length && d.remove(), b.splice(0, 1), b.length > 0 && b[0].show()
			},
			positionPopup: function () {
				var b = a("#" + this.id);
				b.css("top", window.innerHeight / 2.5 + window.pageYOffset - b[0].clientHeight / 2 + "px"), b.css("left", window.innerWidth / 2 - b[0].clientWidth / 2 + "px")
			}
		};
		return c
	}(), d = !1;
	a.blockUI = function (b) {
		if (!d) {
			var c = jQuery("body").height(), e = jQuery(window).height(), f = e > c ? e : c;
			b = b ? " style='opacity:" + b + ";min-height:" + Math.ceil(f) + "px;'" : "", a("BODY").prepend(a("<div id='mask'" + b + "></div>")), a("BODY DIV#mask").bind("touchstart", function (a) {
				a.preventDefault()
			}), a("BODY DIV#mask").bind("touchmove", function (a) {
				a.preventDefault()
			}), d = !0
		}
	}, a.unblockUI = function () {
		d = !1, a("BODY DIV#mask").unbind("touchstart"), a("BODY DIV#mask").unbind("touchmove"), a("BODY DIV#mask").remove()
	}, cfg.isApp && (window._alert = window.alert, window.alert = function (b) {
		if (b === null || b === undefined)b = "null";
		a("#jQUi").length > 0 ? a("#jQUi").popup(b.toString()) : a(document.body).popup(b.toString())
	}, window.confirm = function (a) {
		throw"Due to iOS eating touch events from native confirms, please use our popup plugin instead"
	})
})(jq), function (a) {
	var b = {}, c = b.toString, d = [], e = d.filter;

	function f(a) {
		return a == null ? String(a) : b[c.call(a)] || "object"
	}

	function g(a) {
		return f(a) == "function"
	}

	function h(a, b, c, d) {
		return g(b) ? b.call(a, c, d) : b
	}

	function i(a, b) {
		var c = a.className, d = c && c.baseVal !== undefined;
		if (b === undefined)return d ? c.baseVal : c;
		d ? c.baseVal = b : a.className = b
	}

	a.fn.toggleClass = function (b, c) {
		return this.each(function (d) {
			var e = a(this), f = h(this, b, d, i(this));
			f.split(/\s+/g).forEach(function (a) {
				(c === undefined ? !e.hasClass(a) : c) ? e.addClass(a) : e.removeClass(a)
			})
		})
	}, a.inArray = function (a, b, c) {
		return d.indexOf.call(b, a, c)
	}, a.grep = function (a, b) {
		return e.call(a, b)
	}
}(jQuery), function (a) {
	typeof define === "function" && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
	var b = /\+/g;

	function c(a) {
		return a
	}

	function d(a) {
		return decodeURIComponent(a.replace(b, " "))
	}

	function e(a) {
		a.indexOf("\"") === 0 && (a = a.slice(1, -1).replace(/\\"/g, "\"").replace(/\\\\/g, "\\"));
		try {
			return f.json ? JSON.parse(a) : a
		} catch (b) {
		}
	}

	var f = a.cookie = function (b, g, h) {
		if (g !== undefined) {
			h = a.extend({}, f.defaults, h);
			if (typeof h.expires === "number") {
				var i = h.expires, j = h.expires = new Date;
				j.setDate(j.getDate() + i)
			}
			g = f.json ? JSON.stringify(g) : String(g);
			return document.cookie = [f.raw ? b : encodeURIComponent(b), "=", f.raw ? g : encodeURIComponent(g), h.expires ? "; expires=" + h.expires.toUTCString() : "", h.path ? "; path=" + h.path : "", h.domain ? "; domain=" + h.domain : "", h.secure ? "; secure" : ""].join("")
		}
		var k = f.raw ? c : d, l = document.cookie.split("; "), m = b ? undefined : {};
		for (var n = 0, o = l.length; n < o; n++) {
			var p = l[n].split("="), q = k(p.shift()), r = k(p.join("="));
			if (b && b === q) {
				m = e(r);
				break
			}
			b || (m[q] = e(r))
		}
		return m
	};
	f.defaults = {}, a.removeCookie = function (b, c) {
		if (a.cookie(b) !== undefined) {
			a.cookie(b, "", a.extend(c, {expires: -1}));
			return !0
		}
		return !1
	}
}), !function (a) {
	"use strict";
	var b = function (b, c) {
		this.$element = a(b);
		this.options = a.extend({}, a.fn.typeahead.defaults, c);
		this.waiting = this.options.waiting || this.waiting;
		this.matcher = this.options.matcher || this.matcher;
		this.sorter = this.options.sorter || this.sorter;
		this.highlighter = this.options.highlighter || this.highlighter;
		this.updater = this.options.updater || this.updater;
		this.source = this.options.source;
		this.$menu = a(this.options.menu);
		this.shown = !1;
		this.listen()
	};
	b.prototype = {
		constructor: b, select: function () {
			var a = this.$menu.find(".active").attr("data-value");
			this.$element.val(this.updater(a)).change();
			return this.hide()
		}, updater: function (a) {
			return a
		}, show: function () {
			var b = a.extend({}, this.$element.position(), {height: this.$element[0].offsetHeight});
			this.$menu.insertAfter(this.$element).css({top: b.top + b.height, left: b.left}).show();
			this.shown = !0;
			return this
		}, hide: function () {
			this.$menu.hide();
			this.shown = !1;
			return this
		}, lookup: function (b) {
			var c;
			this.query = this.$element.val();
			if (!this.query || this.query.length < this.options.minLength) {
				this.waiting();
				return this.shown ? this.hide() : this
			}
			c = a.isFunction(this.source) ? this.source(this.query, a.proxy(this.process, this)) : this.source;
			return c ? this.process(c) : this
		}, process: function (b) {
			var c = this;
			b = a.grep(b, function (a) {
				return c.matcher(a)
			});
			b = this.sorter(b);
			if (!b.length) {
				return this.shown ? this.hide() : this
			}
			return this.render(b.slice(0, this.options.items)).show()
		}, waiting: function () {
		}, matcher: function (a) {
			return ~a.toLowerCase().indexOf(this.query.toLowerCase())
		}, sorter: function (a) {
			var b = [], c = [], d = [], e;
			while (e = a.shift()) {
				if (!e.toLowerCase().indexOf(this.query.toLowerCase()))b.push(e); else if (~e.indexOf(this.query))c.push(e); else d.push(e)
			}
			return b.concat(c, d)
		}, highlighter: function (a) {
			var b = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
			return a.replace(new RegExp("(" + b + ")", "ig"), function (a, b) {
				return "<strong>" + b + "</strong>"
			})
		}, render: function (b) {
			var c = this;
			b = a(b).map(function (b, d) {
				b = a(c.options.item).attr("data-value", d);
				b.find("a").html(c.highlighter(d));
				return b[0]
			});
			b.first().addClass("active");
			this.$menu.html(b);
			return this
		}, next: function (b) {
			var c = this.$menu.find(".active").removeClass("active"), d = c.next();
			if (!d.length) {
				d = a(this.$menu.find("li")[0])
			}
			d.addClass("active")
		}, prev: function (a) {
			var b = this.$menu.find(".active").removeClass("active"), c = b.prev();
			if (!c.length) {
				c = this.$menu.find("li").last()
			}
			c.addClass("active")
		}, listen: function () {
			this.$element.on("focus", a.proxy(this.focus, this)).on("blur", a.proxy(this.blur, this)).on("keypress", a.proxy(this.keypress, this)).on("keyup", a.proxy(this.keyup, this));
			if (this.eventSupported("keydown")) {
				this.$element.on("keydown", a.proxy(this.keydown, this))
			}
			this.$menu.on("click", a.proxy(this.click, this)).on("mouseenter", "li", a.proxy(this.mouseenter, this)).on("mouseleave", "li", a.proxy(this.mouseleave, this))
		}, eventSupported: function (a) {
			var b = a in this.$element;
			if (!b) {
				this.$element.setAttribute(a, "return;");
				b = typeof this.$element[a] === "function"
			}
			return b
		}, move: function (a) {
			if (!this.shown)return;
			switch (a.keyCode) {
				case 9:
				case 13:
				case 27:
					a.preventDefault();
					break;
				case 38:
					a.preventDefault();
					this.prev();
					break;
				case 40:
					a.preventDefault();
					this.next();
					break
			}
			a.stopPropagation()
		}, keydown: function (b) {
			this.suppressKeyPressRepeat = ~a.inArray(b.keyCode, [40, 38, 9, 13, 27]);
			this.move(b)
		}, keypress: function (a) {
			if (this.suppressKeyPressRepeat)return;
			this.move(a)
		}, keyup: function (a) {
			switch (a.keyCode) {
				case 40:
				case 38:
				case 16:
				case 17:
				case 18:
					break;
				case 9:
				case 13:
					if (!this.shown)return;
					this.select();
					break;
				case 27:
					if (!this.shown)return;
					this.hide();
					break;
				default:
					this.lookup()
			}
			a.stopPropagation();
			a.preventDefault()
		}, focus: function (a) {
			this.focused = !0
		}, blur: function (a) {
			this.focused = !1;
			if (!this.mousedover && this.shown)this.hide()
		}, click: function (a) {
			a.stopPropagation();
			a.preventDefault();
			this.select();
			this.$element.focus()
		}, mouseenter: function (b) {
			this.mousedover = !0;
			this.$menu.find(".active").removeClass("active");
			a(b.currentTarget).addClass("active")
		}, mouseleave: function (a) {
			this.mousedover = !1;
			if (!this.focused && this.shown)this.hide()
		}
	};
	var c = a.fn.typeahead;
	a.fn.typeahead = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("typeahead"), f = typeof c == "object" && c;
			if (!e)d.data("typeahead", e = new b(this, f));
			if (typeof c == "string")e[c]()
		})
	};
	a.fn.typeahead.defaults = {
		source: [],
		items: 8,
		menu: "<ul class=\"typeahead dropdown-menu\"></ul>",
		item: "<li><a href=\"#\"></a></li>",
		minLength: 1
	};
	a.fn.typeahead.Constructor = b;
	a.fn.typeahead.noConflict = function () {
		a.fn.typeahead = c;
		return this
	};
	a(document).on("focus.typeahead.data-api", "[data-provide=\"typeahead\"]", function (b) {
		var c = a(this);
		if (c.data("typeahead"))return;
		c.typeahead(c.data())
	})
}(window.jQuery), Array.prototype.filter || (Array.prototype.filter = function (a) {
	var b = this.length >>> 0;
	if (typeof a != "function")throw new TypeError;
	var c = [], d = arguments[1];
	for (var e = 0; e < b; e++)if (e in this) {
		var f = this[e];
		a.call(d, f, e, this) && c.push(f)
	}
	return c
}), typeof console == "undefined" && (window.console = {
	log: function () {
	}
});
function toUpperCase1(a) {
	if (!a || !a.length)return a;
	return a.charAt(0).toUpperCase() + a.slice(1)
}
var favourites = {
	schedules: {}, stops: {}, getStops: function () {
		this.load();
		return this.stops
	}, getSchedules: function () {
		this.load();
		return this.schedules
	}, load: function () {
		var a = window.localStorage.favouriteStops, b = window.localStorage.favouriteSchedules;
		this.stops = a ? jQuery.parseJSON(a) : {}, this.schedules = b ? jQuery.parseJSON(b) : {}, console.log("load: ", a, b, "json: ", this.stops, this.schedules)
	}, save: function () {
		window.localStorage.favouriteStops = JSON.stringify(this.stops), window.localStorage.favouriteSchedules = JSON.stringify(this.schedules)
	}, add: function (a) {
		this.load();
		if (a.stopIds)this.stops[a.stopIds] = 1; else {
			this.schedules[a.city] || (this.schedules[a.city] = {});
			var b = this.schedules[a.city];
			b[a.transport] || (b[a.transport] = {});
			var c = b[a.transport];
			c[a.num] || (c[a.num] = {});
			var d = c[a.num];
			d[a.dirType] || (d[a.dirType] = {}), d[a.dirType][a.stopId] = 1
		}
		this.save()
	}, belongs: function (a, b) {
		b && this.load();
		if (a.stopIds)return a.stopIds in this.stops;
		if (this.schedules[a.city] && this.schedules[a.city][a.transport] && this.schedules[a.city][a.transport][a.num] && this.schedules[a.city][a.transport][a.num][a.dirType] && this.schedules[a.city][a.transport][a.num][a.dirType][a.stopId])return !0;
		return !1
	}, remove: function (a) {
		this.load(), a.stopIds ? delete this.stops[a.stopIds] : this.belongs(a) && delete this.schedules[a.city][a.transport][a.num][a.dirType][a.stopId], this.save()
	}, removeKey: function (a) {
		var b = a.split(";");
		b.length == 1 ? this.remove({stopIds: a}) : this.remove({
			city: b[0],
			transport: b[1],
			num: b[2],
			dirType: b[3],
			stopId: b[4]
		})
	}
}, mobile = {
	stops_active_tab: -1, previous_page: "", current_page: "", page_cache: {}, cache: function (a, b, c) {
		b.hashMobile = b.hashForMap ? b.hash.replace("/" + b.hashForMap, "") : b.hash, b.hashForMap = "";
		var d = c ? !1 : a in this.page_cache && b.hashMobile == this.page_cache[a].hashMobile;
		d || (this.page_cache[a] = b), this.current_page != a && this.current_page != "lang" && (this.previous_page = this.current_page), this.current_page = a;
		return !d
	}, render_footer: function () {
		return ["<div class=\"row-fluid\"><div class=\"span12 logos\"><a href=\"", cfg.city.logoURL[pg.language] || cfg.city.logoURL.en || "", "\"></a></div></div>"].join("")
	}, render: function (a) {
		var b = jQuery("body");
		b.length && (b[0].scrollLeft = 0);
		if ((mobile.renderedLanguage || cfg.defaultLanguage) == pg.language) {
			if (a.transport == "favourites")mobile.cache("favourites", a, !0) && mobile.renderFavourites(); else if (a.transport == "lang")mobile.cache("lang", a) && mobile.renderLang(); else if (a.transport == "search")mobile.cache("search", a) && mobile.renderSearch(); else if (a.transport == "stop")mobile.cache("stop", a, !0) && mobile.renderStop(); else if (a.transport == "plan") {
				var c = mobile.current_page == "plan";
				if (c) {
					mobile.cache("plan", a);
					return
				}
				mobile.cache("plan", a) && mobile.renderPlanner()
			} else a.hash == "" || a.hash == "/" + pg.language ? mobile.cache("index", a) && mobile.renderIndex() : a.schedule ? a.schedule && a.schedule.tripNum ? mobile.cache("schedule5", a, !0) && mobile.renderSchedule5() : a.schedule && a.schedule.stopId ? mobile.cache("schedule4", a, !0) && mobile.renderSchedule4() : a.schedule && a.schedule.dirUrl ? mobile.cache("schedule3", a) && mobile.renderSchedule3() : a.schedule && !a.schedule.dirUrl && (mobile.cache("schedule2", a) && mobile.renderSchedule2()) : mobile.cache("schedule", a) && mobile.renderSchedule();
			mobile.hideMenu(), jQuery("#mobile-screen").removeClass().addClass(mobile.current_page)
		} else mobile.renderedLanguage = pg.language, pg.fLoadLanguageScript(pg.language, function () {
			mobile.render(a)
		})
	}, back: function () {
		if (this.current_page != "schedule5" || this.previous_page && this.previous_page.substring(0, 8) != "schedule")if (this.current_page != "schedule4" || this.previous_page && this.previous_page.substring(0, 8) != "schedule")if (this.current_page != "schedule3" || this.previous_page && this.previous_page.substring(0, 8) != "schedule")if (this.current_page != "schedule2" || this.previous_page && this.previous_page.substring(0, 8) != "schedule")if (this.current_page == "plan" && jQuery("#plan .planner").hasClass("results"))jQuery("#plan .planner").removeClass("results"); else if (this.current_page == "search" || this.current_page == "plan")window.location.hash = "/" + pg.language; else if (this.previous_page == "index" || this.current_page == "schedule" && this.previous_page.substring(0, 8) == "schedule")window.location.hash = "/" + pg.language; else if (this.previous_page && this.previous_page in this.page_cache) {
			var a = this.page_cache[this.previous_page];
			a.language = pg.language, this.previous_page = "", this.current_page = "", pg.fUrlSet(a)
		} else pg.urlPrevious && pg.urlPrevious.indexOf("/map") == -1 ? (this.previous_page = "", this.current_page = "", history.back()) : window.location.hash = "/" + pg.language; else pg.fUrlSet({
			schedule: {
				city: pg.schedule.city,
				transport: pg.schedule.transport,
				num: "",
				dirUrl: "",
				stopId: ""
			}
		}); else pg.fUrlSet({
			schedule: {
				city: pg.schedule.city,
				transport: pg.schedule.transport,
				num: pg.schedule.num,
				dirUrl: "",
				stopId: ""
			}
		}); else pg.fUrlSet({
			schedule: {
				city: pg.schedule.city,
				transport: pg.schedule.transport,
				num: pg.schedule.num,
				dirUrl: pg.schedule.dirType,
				stopId: ""
			}
		}); else pg.fUrlSet({
			schedule: {
				city: pg.schedule.city,
				transport: pg.schedule.transport,
				num: pg.schedule.num,
				dirUrl: pg.schedule.dirType,
				stopId: pg.schedule.stopId
			}
		})
	}, renderFavourites: function () {
		ti.fGetAnyStopDetails([], function () {
			pg.fLoadRoutesList(function () {
				jQuery("#favourites").html(i18n.loading), function () {
					var a = favourites.getStops(), b = [];
					jQuery.each(a, function (a, c) {
						var d = ti.fGetAnyStopDetails(a), e = d.name + d.street ? [" (", d.street, ")"].join("") : "";
						b.push(["<li class=\"without-label\">", "<a href=\"#stop/", a, "/", pg.language, "\" alt=\"", e, "\" title=\"", e, "\">", "<strong>", d.name, "</strong><br>", d.street ? ["<span class=\"muted\">(", d.street, ")</span>"].join("") : "", "</a><a class=\"remove\" data-key=\"", a, "\" href=\"#remove\" alt=\"", i18n.btnFavouritesRemove, "\" title=\"", i18n.btnFavouritesRemove, "\"></a></li>"].join(""))
					});
					var c = [], d = favourites.getSchedules(), e = [];
					jQuery.each(d, function (a, d) {
						jQuery.each(d, function (d, f) {
							jQuery.each(f, function (f, g) {
								jQuery.each(g, function (g, h) {
									var i = ti.fGetRoutes(a, d, f, g);
									if (!i.length)return !0;
									var j = i[0];
									jQuery.each(h, function (h, i) {
										if (h != "$") {
											e.push(h);
											var k = ti.fGetStopDetails(h), l = pg.fUrlSet({
												schedule: {
													city: j.city,
													dirType: j.dirType,
													transport: j.transport,
													num: j.num,
													stopId: h
												}
											}, !0);
											b.push(["<li><a href=\"#", l, "\" alt=\"", j.num, " ", j.name, "\" title=\"", j.num, " ", j.name, "\"><table onclick=\"window.location.hash='", l, "'\"><tr><td><span class=\"icon icon-", d, "\"></span></td><td>", "<span class=\"label label-", d, " num", j.num.length, "\">", j.num, "</span></td><td>", j.name, "<br>", i18n.stop, ": <strong>", k.name, "</strong><div class=\"flive\" id=\"flive-", [h, j.transport, j.num].join("-"), "\">&nbsp;</div></td></tr></table><div id=\"flive-wait-", [h, j.transport, j.num].join("-"), "\" class=\"timetowait\"></div></a>", "<a class=\"remove\" data-key=\"", [a, d, f, g, h].join(";"), "\" href=\"#remove\" alt=\"", i18n.btnFavouritesRemove, "\" title=\"", i18n.btnFavouritesRemove, "\"></a></li>"].join(""))
										} else {
											var l = pg.fUrlSet({
												schedule: {
													city: j.city,
													dirType: j.dirType,
													transport: j.transport,
													num: j.num
												}
											}, !0);
											c.push(["<li onclick=\"window.location.hash='", l, "'\"><a href=\"#", l, "\" alt=\"", j.num, " ", j.name, "\" title=\"", j.num, " ", j.name, "\"><table><tr><td style=\"white-space:nowrap;\">", "<span class=\"icon icon-", d, "\"></span>", "<span class=\"label label-", d, " num", j.num.length, "\">", j.num, "</span></td><td>", j.name, "</td></tr></table></a><a class=\"remove\" data-key=\"", [a, d, f, g, h].join(";"), "\" href=\"#remove\" alt=\"", i18n.btnFavouritesRemove, "\" title=\"", i18n.btnFavouritesRemove, "\"></a></li>"].join(""))
										}
									})
								})
							})
						})
					}), document.title = i18n.menuFavorites;
					var f = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.menuFavorites, "</h1>",  "</div>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-favourites\">", "<li class=\"active\" data-target=\"stops\"><span title=\"", i18n.stops, "\"><span><p>", i18n.stops, "</p></span></span></li>", "<li data-target=\"lines\"><a alt=\"", i18n.routes, "\" title=\"", i18n.routes, "\"><span><p>", i18n.routes, "</p></span></a></li>", "</ul>", "</div>", "<div class=\"tab-content\">", "<div class=\"tab-pane active stops\">", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list with-labels with-icons favourites-list\">", b.join(""), "</ul>", "</div>", "</div>", "<div class=\"tab-pane lines\">", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list with-labels with-icons favourites-list favourites-list-lines\">", c.join(""), "</ul>", "</div>", "</div>", "</div>", mobile.render_footer(), "</div>"];
					jQuery("#favourites").html(f.join("")), mobile.renderMenu(), e.length ? (pg.favouriteStops = e.join(","), pg.fProcessVehicleDepartures(null, function (a) {
						jQuery("#favourites .favourites-list .flive").html("&nbsp;").removeClass("active");
						if (typeof a == "string")jQuery("#favourites .favourites-list .flive").html(a).addClass("active"); else {
							if (typeof a == "object" && "status"in a)return;
							jQuery.each(a, function (a, b) {
								var c = ["flive", b.route.stopId, b.route.transport, b.route.num].join("-"), d = ["flive-wait", b.route.stopId, b.route.transport, b.route.num].join("-");
								b.times.sort(function (a, b) {
									return a - b
								});
								var e = "";
								if (b.times.length) {
									var f = new Date, g = f.getHours() * 60 + f.getMinutes();
									e = b.times[0] - g + " min"
								}
								jQuery("#" + d).html(e), jQuery("#" + c).html([i18n.realTimeDepartures, ": ", jQuery.map(b.times, function (a) {
									return [ti.printTime(a), "&ensp;"].join("")
								}).get().join("")].join("")).addClass("active")
							})
						}
					}), pg.loadedDepartingRoutes = pg.favouriteStops) : pg.favouriteStops = "", jQuery("#favourites .nav-favourites li").bind("click", function () {
						var a = jQuery(this).attr("data-target");
						jQuery("#favourites .nav-favourites li").each(function (b, c) {
							var d = jQuery(this).children().attr("title"), e = jQuery(this).find("p").html();
							a == jQuery(this).attr("data-target") ? jQuery(this).addClass("active").html(["<span title=\"", d, "\"><span><p>", e, "</p></span></span>"].join("")) : jQuery(this).removeClass("active").html(["<a title=\"", d, "\" alt=\"", d, "\"><span><p>", e, "</p></span></a>"].join(""))
						}), jQuery(this).parent().parent().parent().find(".tab-pane").removeClass("active"), jQuery(this).parent().parent().parent().find(".tab-pane." + a).addClass("active")
					}), jQuery("#favourites .favourites-list a.remove").bind("click", function () {
						var a = jQuery(this).attr("data-key");
						favourites.removeKey(a), jQuery(this).parent().remove();
						return !1
					})
				}()
			})
		})
	}, renderLang: function () {
		jQuery("#lang").html(i18n.loading), function () {
			var a = [];
			jQuery.each(cfg.city.languages.split(","), function (b, c) {
				a.push(["<li data-lang=\"", c, "\"", c == pg.language ? " class=\"active\"" : "", "><a style=\"background-image:url(mobile/img/lang-", c, ".png)\" href=\"#lang/", c, "\" alt=\"", cfg.languages[c], "\" title=\"", cfg.languages[c], "\">", cfg.languages[c], "</a></li>"].join(""))
			}), document.title = i18n.headerTitleMobile;
			var b = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.headerTitleMobile, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\"><ul class=\"nav nav-list lang-list\">", a.join(""), "</ul></div>", mobile.render_footer(), "</div>"];
			jQuery("#lang").html(b.join("")), jQuery("#lang .lang-list li").click(function () {
				var a = jQuery(this).attr("data-lang");
				pg.language = a, mobile.back();
				return !1
			}), mobile.renderMenu()
		}()
	}, renderPlanner: function () {
		pg.fCreateNavigation(function (a) {
			jQuery("#plan").html(i18n.loading), function () {
				var b = new Date, c = [];
				for (var d = 2; d <= 6; d++) {
					var e = new Date(b.getFullYear(), b.getMonth(), b.getDate() + d);
					c.push(["<option class=\"inputDate", d, "\" value=\"", d, "\">", pg.formatDate(e), "</option>"].join(""))
				}
				var f = "", g = {};
				jQuery.each(a, function (a, b) {
					jQuery.each(b.transport, function (a, b) {
						g[b.transport] || (g[b.transport] = !0, f += "<label class=\"checkbox\"><input checked=\"checked\" class=\"checkbox" + b.transport + "\" value=\"" + b.transport + "\" type=\"checkbox\"> " + b.name + "</label>")
					})
				}), f = "<div class=\"control-group extended hidden\"" + (cfg.city.transport.length <= 1 ? " style=\"display:none;\"" : "") + "><label class=\"control-label\">" + i18n.optionsTransport + ":</label>" + f + "</div>", document.title = i18n.menuTripPlanner;
				var h = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.menuTripPlanner, "</h1>",  "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"planner\">", "<div class=\"control-group\">", "<div class=\"input-prepend\">", "<a class=\"add-on pin-start\" href=\"#plan/map\" alt=\"", i18n.myLocation, "\" title=\"", i18n.myLocation, "\"></a>", "<input id=\"inputStart\" value=\"", i18n.startStop, "\" type=\"text\" placeholder=\"\"><div class=\"locate start\"></div>", "</div>", "<div class=\"input-prepend\">", "<a class=\"add-on pin-end\" href=\"#plan/map\" alt=\"", i18n.myLocation, "\" title=\"", i18n.myLocation, "\"></a>", "<input id=\"inputFinish\" value=\"", i18n.finishStop, "\" type=\"text\" placeholder=\"\"><div class=\"locate finish\"></div>", "</div>", "</div>", "<div class=\"control-group\">", "<select class=\"inputReverse\"><option value=\"1\">", i18n.depart, "</option><option value=\"-1\">", i18n.arrive, "</option></select>", "<input class=\"inputTime\" type=\"text\" value=\"", ti.printTime(ti.dateToMinutes(new Date) % 1440), "\">", "<select class=\"inputDate\">", "<option class=\"inputDate0\" value=\"0\" selected=\"selected\">", toUpperCase1(i18n.today), "</option>", "<option class=\"inputDate1\" value=\"1\">", toUpperCase1(i18n.tomorrow), "</option>", c.join(""), "</select>", "</div>", cfg.city.planHandicappedOption === !1 ? "" : "<div class=\"control-group disabled\"><label class=\"checkbox\"><input class=\"checkHandicapped\" type=\"checkbox\">" + i18n.checkHandicapped + "</label></div>", "<div class=\"control-group expand\"><span class=\"more-options\" title=\"", toUpperCase1(i18n.extendedOptions), "\">", toUpperCase1(i18n.extendedOptions), "</span></div>", f, "<div class=\"control-group extended\">", "<label class=\"control-label\"><strong>", i18n.rideOnlyRoutes, ":</strong></label>", "<input class=\"inputRoutesFilter\" type=\"text\">", "</div>", "<div class=\"control-group extended\">", "<select class=\"inputChangeTime\">", "<option value=\"3\" selected=\"selected\">", i18n.timeForConnection, "</option>", "<option value=\"1\">1 min.</option>", "<option value=\"2\">2 min.</option>", "<option value=\"3\">3 min.</option>", "<option value=\"4\">4 min.</option>", "<option value=\"5\">5 min.</option>", "<option value=\"10\">10 min.</option>", "</select>", "<select class=\"inputWalkMax\">", "<option value=\"", cfg.city.walk_max || 500, "\" selected=\"selected\">", i18n.walkingMax, "</option>", "<option value=\"0\">0 m</option>", "<option value=\"100\">100 m</option>", "<option value=\"200\">200 m</option>", "<option value=\"500\">500 m</option>", "<option value=\"1000\">1 km</option>", "<option value=\"2000\">2 km</option>", "<option value=\"5000\">5 km</option>", "</select>", "<select class=\"inputWalkSpeed\">", "<option value=\"4\" selected=\"selected\">", i18n.walkingSpeed, "</option>", "<option value=\"1\">1 km/h</option>", "<option value=\"2\">2 km/h</option>", "<option value=\"3\">3 km/h</option>", "<option value=\"4\">4 km/h</option>", "<option value=\"5\">5 km/h</option>", "<option value=\"6\">6 km/h</option>", "</select>", "</div>", "<div class=\"form-actions\"><button type=\"submit\" class=\"btn btn-planner\" title=\"", i18n.searchRoute, "\">", i18n.searchRoute, "</button></div>", "<div class=\"accordion planner-results\"></div>", "</div>", "</div>", mobile.render_footer(), "</div>"];
				jQuery("#plan").html(h.join("")), pg.fLoadPlannerTab(), mobile.renderMenu(), jQuery("#plan .btn-planner").bind("click", function () {
					jQuery("#plan .planner").removeClass("extended"), pg.fLoadPlannerTab(!0, function (a, b) {
						mobile.renderPlannerResults(a, b)
					})
				}), jQuery("#plan .more-options").bind("click", function () {
					jQuery("#plan .planner").addClass("extended")
				}), jQuery("#plan .locate.start").bind("click", function () {
					navigator.geolocation && navigator.geolocation.getCurrentPosition(function (a) {
						pg.storeMyLocation(a), pg.fUrlSet({transport: "plan", inputStart: pg.myLocation})
					}, function (a) {
						alert("Can not determine location!")
					})
				}), jQuery("#plan .locate.finish").bind("click", function () {
					navigator.geolocation && navigator.geolocation.getCurrentPosition(function (a) {
						pg.storeMyLocation(a), pg.fUrlSet({transport: "plan", inputFinish: pg.myLocation})
					}, function (a) {
						alert("Can not determine location!")
					})
				}), jQuery("#inputStart").bind("focus", function () {
					pg.inputSuggestedStops_Focus(this)
				}).bind("click", function () {
					pg.inputSuggestedStops_Focus(this)
				}).bind("blur", function () {
					pg.inputSuggestedStops_Blur(this)
				}).bind("keypress", function (a) {
					pg.inputSuggestedStops_KeyDown(a)
				}), jQuery("#inputFinish").bind("focus", function () {
					pg.inputSuggestedStops_Focus(this)
				}).bind("click", function () {
					pg.inputSuggestedStops_Focus(this)
				}).bind("blur", function () {
					pg.inputSuggestedStops_Blur(this)
				}).bind("keypress", function (a) {
					pg.inputSuggestedStops_KeyDown(a)
				})
			}()
		})
	}, renderPlannerResults: function (a, b) {
		jq("#loading").hide(), jQuery("#plan .planner-results").html(i18n.loading), jQuery("#plan .planner").addClass("results");
		if (a.errors && a.errors.length) {
			var c = [];
			jQuery.each(a.errors, function (a, b) {
				c.push(["<p style=\"padding:10px;margin:0px;\">", b.text, "</p>"].join(""))
			}), jQuery("#plan .planner-results").html(c.join(""))
		} else {
			var d = pg.optimalResults = a.results;
			pg.map = {};
			var e = ti.fGetAnyStopDetails(pg.inputStart), f = ti.fGetAnyStopDetails(pg.inputFinish), g = ["<div class=\"row-fluid\">", "<div class=\"span12 toolbar\">", "<p class=\"planner-info\">", i18n.departingStop, ": <strong>", e.name, "</strong><br>", i18n.finishStop, ": <strong>", f.name, "</strong>", "</p>", "<p class=\"planner-info pull-right\">", a.reverse == 1 ? i18n.depart : i18n.arrive, " <strong>", pg.formatDate(a.date), "</strong><br>", "<strong class=\"time\">", ti.printTime(a.start_time), "</strong>", "</p>", "</div>", "</div>"];
			for (var h = 0; h < d.length; h++) {
				var i = d[h], j = d[h].legs, k = [], l = [];
				for (var m = 0; m < j.length; m++) {
					var n = j[m], o = n.route, p = (j[m + 1] || {route: null}).route;
					if (o && o.transport) {
						p && o.city === p.city && o.transport === p.transport && o.num === p.num && (n.finish_stop.name = j[m + 1].finish_stop.name, n.finish_time = j[m + 1].finish_time, ++m), l.push("<span class=\"icon icon_narrow icon-" + o.transport + "\" title=\"" + i18n.transport1[o.transport] + " " + o.num + " " + i18n.towards + " " + o.name + "\"></span>"), i.direct_trip && o.num.length <= 8 && (l.push("<span class=\"num num" + Math.min(n.route.num.length, 4) + " " + n.route.transport + "\">" + n.route.num + "</span>"), n.online_data && l.push(" " + n.online_data.code + " " + n.online_data.departureAsStr + " &rarr; " + n.online_data.arrivalAsStr)), o.stopId = n.start_stop.id, o.tripNum = (n.trip_num || -1) + 1;
						var q = pg.fUrlSet({schedule: o, mapHash: ""}, !0), r = n.finish_time - n.start_time;
						r = r >= 60 ? ti.printTime(r) : r + "&nbsp;" + i18n.minutesShort, k.push(["<li>", "<a href=\"#", q, "\" alt=\"", n.route.num, " ", n.route.name, "\" title=\"", n.route.num, " ", n.route.name, "\">", "<span class=\"icon icon-", n.route.transport, "\"></span>", "<span class=\"label label-", n.route.transport, " num", n.route.num.length, "\">", n.route.num, "</span>", n.route.name, "<p class=\"directions\">", "<strong>", ti.printTime(n.start_time), n.online_data ? "(" + n.online_data.departureAsStr + ")" : "", " ", n.start_stop.name, "</strong>&nbsp;", ti.printTime(n.finish_time), n.online_data ? "(" + n.online_data.arrivalAsStr + ")" : "", " ", n.finish_stop.name, "<br>", "<span class=\"muted\">(", i18n.ride, " ", r, ")</span>", "</p>", "</a>", "</li>"].join(""))
					} else {
						if (n.start_time == n.finish_time && parseInt(n.start_stop.id, 10) == parseInt(n.finish_stop.id, 10))continue;
						l.push("<span class=\"icon icon_narrow icon-walk\" title=\"" + i18n.walk + " " + (n.finish_time - n.start_time) + "&nbsp;" + i18n.minutesShort + "\"></span>"), k.push(["<li><a alt=\"", i18n.walk, "\" title=\"", i18n.walk, "\">", "<span class=\"icon icon-walk\"></span>", "<p class=\"directions\">", "<strong>", ti.printTime(n.start_time), " ", n.start_stop.name, "</strong>&nbsp;", ti.printTime(n.finish_time), " ", n.finish_stop.name, "<br>", "<span class=\"muted\">(", i18n.walk, " ", n.finish_time - n.start_time, "&nbsp;", i18n.minutesShort, ")</span>", "</p>", "</a></li>"].join(""))
					}
					if (n.taxi)for (var s = 0; s < n.taxi.length; ++s) {
						var t = n.taxi[s];
						k.push((s ? "<br />" : "") + "km: " + t.km + ", " + t.name + ", phone: " + t.phone)
					}
				}
				var u = [pg.city, "/", pg.transport, pg.transport == "plan" ? "/" + (pg.inputStart || "") + (pg.inputFinish ? "/" + pg.inputFinish : "") : "", "/map,,,", h + 1].join("");
				g.push(["<div class=\"accordion-group", h ? "" : " active", "\">", "<div class=\"accordion-heading\">", "<div class=\"accordion-toggle", h ? " collapsed" : "", "\" data-toggle=\"collapse\" title=\"", i18n.option + "&nbsp;" + (h + 1), "\">", "<p>", "<strong>", i18n.option + "&nbsp;" + (h + 1), "</strong> ", ti.printTime(i.start_time, null, "&#x2007;"), " - ", ti.printTime(i.finish_time, null, "&#x2007;"), "<br>", i18n.travelDuration, " <strong>", ti.printTime(i.travel_time), "</strong>", "</p>", "<div class=\"variant-map pull-right\" style=\"display:inline-block;position:absolute; right:5px;\">", "<a class=\"btn btn-map\" style=\"display:inline-block;\" title=\"", i18n.showInMap, "\" alt=\"", i18n.showInMap, "\" href=\"#", u, "\"></a>", "</div>", "<div class=\"icons pull-right\" style=\"display:inline-block;position:absolute; right:46px;\">", l.join(""), "</div>", "</div>", "</div>", "<div id=\"result", h, "\" class=\"accordion-body collapse\">", "<div class=\"accordion-inner\"><ul class=\"nav nav-list with-icons\">", k.join(""), "</ul></div></div></div>"].join(""))
			}
			if (d.length > 0) {
				b && document.body.className.indexOf("Map") >= 0 && (pg.mapShowAllStops = !1, pg.fUrlSetMap({optimalRoute: 1}));
				if (cfg.defaultCity === "latvia") {
					ti.TimeZoneOffset = 2;
					var v = "http://routelatvia.azurewebsites.net/?";
					v += "origin=" + a.start_stops, v += "&destination=" + a.finish_stops, v += "&departure_time=" + ti.toUnixTime(a.date, a.start_time), g.push("<br/><a target=\"_blank\" href=\"" + v + "\">" + v + "</a>"), g.push("<div id=\"online_results\">"), a.online_query_url ? g.push("<a target=\"_blank\" href=\"" + a.online_query_url + "\">" + a.online_query_url + "</a>") : b || g.push("<br/>Calculating alternative routes...");
					if (a.online_results_JSON) {
						var d = JSON.parse(a.online_results_JSON);
						g.push("<div style=\"white-space:pre;\">", JSON.stringify(d, null, 4), "</div>")
					}
					g.push("</div>")
				}
			} else g.push("<br/>" + i18n.noOptimalRoutes);
			jQuery("#plan .planner-results").html(g.join("")), jQuery("#plan .accordion-toggle").bind("click", function () {
				var a = jQuery(this).hasClass("collapsed");
				jQuery("#plan .accordion-toggle").addClass("collapsed").parent().parent().removeClass("active"), jQuery(this).toggleClass("collapsed", !a), jQuery(this).parent().parent().toggleClass("active", a)
			})
		}
	}, renderStop: function () {
		var a = pg.inputStop.split(","), b = a[0], c = a.sort(function (a, b) {
				return a < b
			}).join(",") + "/" + pg.language;
		mobile.stops_code && mobile.stops_code == c ? (jQuery("#stop .departing-routes .accordion-toggle").addClass("collapsed"), jQuery("#stop .departing-routes .accordion-body").removeClass("in"), jQuery("#stop .departing-routes .accordion-toggle.stop-" + b).removeClass("collapsed"), jQuery("#stop .departing-routes .accordion-body.stop-" + b).addClass("in"), favourites.belongs({stopIds: pg.inputStop}, !0) ? jQuery("#stop .btn-favourite").removeClass("add").attr("alt", i18n.btnFavouritesRemove).attr("title", i18n.btnFavouritesRemove) : jQuery("#stop .btn-favourite").addClass("add").attr("alt", i18n.btnFavouritesAdd).attr("title", i18n.btnFavouritesAdd), jQuery("#stop .toolbar .btn-map").attr("href", ["#stop/", pg.inputStop, "/map/", pg.language].join(""))) : (mobile.stops_code = c, document.title = i18n.routesAndTimetables, ti.fGetAnyStopDetails(pg.inputStop, function (a) {
			jQuery("#stop").html(i18n.loading), function () {
				var b = i18n.btnFavouritesAdd, c = " add";
				favourites.belongs({stopIds: pg.inputStop}, !0) && (b = i18n.btnFavouritesRemove, c = "");
				var d = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar\">", "<p class=\"with-stop\"><strong>", a.name, "</strong><br>", a.street ? ["<span class=\"muted\">(", a.street, ")</span>"].join("") : "", "</p>", "<div class=\"btn-group\">", "<a class=\"btn btn-favourite", c, "\" href=\"#\" alt=\"", b, "\" title=\"", b, "\"></a>", "<a class=\"btn btn-map\" href=\"#stop/", pg.inputStop, "/map/", pg.language, "\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a>", "</div>", "</div>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-departures\">", "<li class=\"caption\">", i18n.departuresFor, ":</li>", mobile.stops_active_tab == -1 ? ["<li class=\"active\" data-departure=\"-1\"><span title=\"", toUpperCase1(i18n.fromNow), "\"><span><p>", toUpperCase1(i18n.fromNow), "</p></span></span></li>"].join("") : ["<li class=\"\" data-departure=\"-1\"><a alt=\"", toUpperCase1(i18n.fromNow), "\" title=\"", toUpperCase1(i18n.fromNow), "\"><span><p>", toUpperCase1(i18n.fromNow), "</p></span></a></li>"].join(""), mobile.stops_active_tab == 0 ? ["<li class=\"active\" data-departure=\"0\"><span title=\"", toUpperCase1(i18n.today), "\"><span><p>", toUpperCase1(i18n.today), "</p></span></span></li>"].join("") : ["<li class=\"\" data-departure=\"0\"><a alt=\"", toUpperCase1(i18n.today), "\" title=\"", toUpperCase1(i18n.today), "\"><span><p>", toUpperCase1(i18n.today), "</p></span></a></li>"].join(""), mobile.stops_active_tab == 1 ? ["<li class=\"active\" data-departure=\"1\"><span title=\"", toUpperCase1(i18n.tomorrow), "\"><span><p>", toUpperCase1(i18n.tomorrow), "</p></span></span></li>"].join("") : ["<li class=\"\" data-departure=\"1\"><a alt=\"", toUpperCase1(i18n.tomorrow), "\" title=\"", toUpperCase1(i18n.tomorrow), "\"><span><p>", toUpperCase1(i18n.tomorrow), "</p></span></a></li>"].join(""), "</ul>", "</div>", "<div class=\"departing-routes\">", "<ul class=\"nav nav-list search-results all\">", "<li class=\"error\">" + i18n.loading, "</li>", "</ul>", "</div>", mobile.render_footer(), "</div>"];
				jQuery("#stop").html(d.join("")), mobile.renderMenu(), jQuery("#stop .btn-favourite").bind("click", function () {
					var a = jQuery(this);
					a.hasClass("add") ? (favourites.add({stopIds: pg.inputStop}), a.removeClass("add").attr("alt", i18n.btnFavouritesRemove).attr("title", i18n.btnFavouritesRemove)) : (favourites.remove({stopIds: pg.inputStop}), a.addClass("add").attr("alt", i18n.btnFavouritesAdd).attr("title", i18n.btnFavouritesAdd));
					return !1
				}), pg.fLoadDepartingRoutes(function (a) {
					mobile.renderDepartingRoutes(a)
				}, mobile.stops_active_tab), jQuery("#stop .nav-departures li").bind("click", function () {
					if (!jQuery(this).hasClass("caption")) {
						var a = jQuery(this).attr("data-departure");
						jQuery("#stop .nav-departures li").each(function (b, c) {
							if (jQuery(this).hasClass("caption"))return !0;
							var d = jQuery(this).children().attr("title"), e = jQuery(this).find("p").html();
							a == jQuery(this).attr("data-departure") ? jQuery(this).addClass("active").html(["<span title=\"", d, "\"><span><p>", e, "</p></span></span>"].join("")) : jQuery(this).removeClass("active").html(["<a title=\"", d, "\" alt=\"", d, "\"><span><p>", e, "</p></span></a>"].join(""))
						}), pg.fLoadDepartingRoutes(function (a) {
							mobile.renderDepartingRoutes(a)
						}, +a), mobile.stops_active_tab = +a
					}
				})
			}()
		}))
	}, renderDepartingRoutes: function (a) {
		if (typeof a == "string" && jQuery("#stop .nav-departures li.active").attr("data-departure") == "-1")jQuery("#stop .departing-routes").html(["<ul class=\"nav nav-list search-results all\"><li class=\"error\">", a, "</li></ul>"].join("")); else if (typeof a == "object" && "status"in a)jQuery("#stop .departing-routes").html(["<ul class=\"nav nav-list search-results all\"><li class=\"error\">", a.message, "</li></ul>"].join("")); else if (a.length) {
			var b = pg.inputStop.split(","), c = b[0];
			a = a.filter(function (a) {
				return typeof a.route.stopId != "undefined"
			});
			var d = jQuery("#stop .nav-departures li.active").attr("data-departure"), e = d == "-1", f = d == "0";
			a.sort(function (a, b) {
				if (a.route.stopId < b.route.stopId)return -1;
				if (a.route.stopId > b.route.stopId)return 1;
				if (e)if (a.route.departures && b.route.departures) {
					if (a.route.departures[0] < b.route.departures[0])return -1;
					if (a.route.departures[0] > b.route.departures[0])return 1
				}
				if (a.route.sortKey < b.route.sortKey)return -1;
				if (a.route.sortKey > b.route.sortKey)return 1;
				return ti.naturalSort(a.route.num, b.route.num)
			}), e = d == "-1";
			var g = "", h = 1, i = [], j = new Date, k = j.getHours() * 60 + j.getMinutes();
			jQuery.each(a, function (d, j) {
				var l = j.route.stopId;
				g && g != l && b.length > 1 && i.push("</ul></div></div></div>"), g != l && b.length > 1 && (i.push(["<div class=\"accordion-group\"><div class=\"accordion-heading\">", "<a class=\"accordion-toggle stop-", l, l == c ? "" : " collapsed", "\" data-toggle=\"collapse\" href=\"#stop/", l, ",", b.filter(function (a) {
					return a != l
				}).join(","), "/", pg.language, "\" alt=\"", i18n.direction, " ", h, "\" title=\"", i18n.direction, " ", h, "\"><p><strong>", i18n.direction, " ", h, "</strong></p></a></div>", "<div id=\"result1\" class=\"accordion-body collapse stop-", l, l == c ? " in" : "", "\"><div class=\"accordion-inner\">", "<ul class=\"nav nav-list with-labels with-icons\">"].join("")), h += 1);
				var m = pg.fUrlSet({schedule: j.route, hashForMap: !0}, !0);
				j.times.sort(function (a, b) {
					return a - b
				});
				var o = "";
				e && typeof j.timetowait != "undefined" && (o = j.timetowait + " min"), i.push("<li", e ? " class=\"live\"" : "", " onclick=\"window.location.hash='", j.hash, "'\"><a href=\"#", j.hash, "\" alt=\"", j.route.num, " ", j.route.name, "\" title=\"", j.route.num, " ", j.route.name, "\"><table><tr><td>", "<span class=\"icon icon-", j.route.transport, "\"></span></td><td><span class=\"label label-", j.route.transport, " num", j.route.num.length, "\">", j.route.num, "</span>", "</td><td><p>", j.route.name, "</p><br/><strong class=\"departures\">", jQuery.map(j.notes, function (a) {
					return ["<span>", a, "</span>"].join("")
				}).get().join(", &nbsp;")), j.times.length && i.push(jQuery.map(j.times, function (a) {
					return ["<span", f && a < k ? " style='color:grey;'" : "", ">", ti.printTime(a), "</span>"].join("")
				}).get().join("")), i.push("</strong></td></tr></table><div class=\"timetowait\">", o, "</div></a><div class=\"livemap\"><div class=\"btn-group pull-right\"><a href=\"#", m, "\" class=\"btn btn-map\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a></div></div></li>"), b.length > 1 && d == a.length - 1 && i.push("</ul></div></div></div>"), g = l
			});
			if (b.length == 1)var l = ["<div class=\"row-fluid\"><ul class=\"nav nav-list with-labels with-icons\">", i.join(""), "</ul></div>"]; else var l = ["<div class=\"row-fluid\"><div class=\"accordion stop-routes\">", i.join(""), "</div></div>"];
			jQuery("#stop .departing-routes").html(l.join("")), jQuery("#stop .departing-routes .accordion-toggle").bind("click", function () {
				jQuery(this).toggleClass("collapsed"), jQuery(this).parent().parent().find(".accordion-body").toggleClass("in")
			})
		} else jQuery("#stop .departing-routes").html("<ul class=\"nav nav-list search-results all\"><li class=\"error\">" + i18n.stopNoRealtimeDepartures + "</li></ul>")
	}, renderSearch: function () {
		pg.fCreateNavigation(function (a) {
			pg.fLoadRoutesList(function (b) {
				jQuery("#search").html(i18n.loading), function () {
					var b = [], c = "";
					jQuery.each(a, function (a, d) {
						d.city == pg.city || !pg.city && a == 0 ? b.push(["<li data-city=\"", d.city, "\" class=\"active ", d.city, "\"><span title=\"", d.name, "\"><p>", d.name, "</p></span></li>"].join("")) : b.push(["<li data-city=\"", d.city, "\" class=\"", d.city, "\"><a alt=\"", d.name, "\" title=\"", d.name, "\"><p>", d.name, "</p></a></li>"].join(""));
						if (d.city == pg.city || !pg.city && a == 0)c = d.city
					}), document.title = i18n.menuSearch;
					var d = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.menuSearch, "</h1>",  "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar search\">", "<div class=\"form-search\"><input id=\"search-input\" autofocus=\"autofocus\" type=\"text\" placeholder=\"" + i18n.searchRoutesAndStops + "\" data-provide=\"typeahead\">", "</div>", "</div>", "</div>", "<input class=\"selected-city\" type=\"hidden\" value=\"", c, "\">", cfg.city.region ? "<div class=\"row-fluid\"><ul class=\"nav nav-tabs nav-region\">" + b.join("") + "</ul></div>" : "", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-search\">", "<li class=\"active\" data-filter=\"all\"><span title=\"", i18n.allResults, "\"><span><p>", i18n.allResults, "<span class=\"total-number\"></span></p></span></span></li>", "<li data-filter=\"stops\"><a alt=\"", i18n.stops, "\" title=\"", i18n.stops, "\"><span><p>", i18n.stops, "<span class=\"stops-number\"></span></p></span></a></li>", "<li data-filter=\"lines\"><a alt=\"", i18n.routes, "\" title=\"", i18n.routes, "\"><span><p>", i18n.routes, "<span class=\"routes-number\"></span></p></span></a></li>", "</ul>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list search-results all\">", "</ul>", "</div>", "<div class=\"row-fluid\"><h2 class=\"search-results-placeholder\">" + i18n.searchExplanation + "</h2></div>", mobile.render_footer(), "</div>"];
					jQuery("#search").html(d.join("")), mobile.renderMenu(), jQuery("#search-input").get().focus(), jQuery("#search .form-search input").typeahead({
						source: function (a, b) {
							var c = ti.fGetRoutes(jQuery("#search .selected-city").val());
							return c
						}, waiting: function () {
							jQuery("#search .search-results").empty().html(["<li class=\"stop line error\"></li>"].join("")), jQuery("#search .total-number").empty(), jQuery("#search .stops-number").empty(), jQuery("#search .routes-number").empty()
						}, items: 100, minLength: 1, sorter: function (a) {
							a.length && jQuery("#search .container-fluid").addClass("results");
							var b = jQuery("#search .search-results").empty(), c = jQuery("#search .total-number").empty(), d = jQuery("#search .stops-number").empty(), e = jQuery("#search .routes-number").empty(), f = 0;
							if (this.query.length < 3)d.html("&nbsp;(0)"), b.append(["<li class=\"stop error\">", i18n.typeMoreLetters, ".</li>"].join("")); else {
								var g = ti.fGetStopsByName(this.query);
								jQuery.each(g, function (a, c) {
									var d = ti.fGetRoutesAtStop(c.id), e = [], f = "", g = "";
									jQuery.each(d, function (a, b) {
										e.push([!f || f != b.transport ? ["<span class=\"icon icon-", b.transport, "\"></span>"].join("") : "", f != b.transport || g != b.num ? ["<span class=\"transfer", b.transport, "\">", b.num, "</span>"].join("") : ""].join("")), f = b.transport, g = b.num
									});
									var h = "stop/" + c.id + "/" + pg.language, i = c.name + (c.street ? [" (", c.street, ")"].join("") : "");
									b.append(["<li class=\"stop\">", "<a href=\"#", h, "\" alt=\"", i, "\" title=\"", i, "\">", "<strong>", c.name, c.street ? ["</strong> (", c.street, ")"].join("") : "</strong>", "<br>", "<strong class=\"lines\">", e.join(""), "</strong>", "</a>", "</li>"].join(""))
								}), g.length || b.append(["<li class=\"stop error\">", i18n.noStopsFound, ".</li>"].join("")), d.html("&nbsp;(" + g.length + ")"), f += g.length
							}
							jQuery.each(a, function (a, c) {
								var d = pg.fUrlSet({schedule: {city: c.city, transport: c.transport, num: c.num}}, !0);
								b.append(["<li class=\"line\" onclick=\"window.location.hash='", d, "'\"><a href=\"#", d, "\" alt=\"", c.num, " ", c.name, "\" title=\"", c.num, " ", c.name, "\"><table><tr><td><span class=\"icon icon-", c.transport, "\"></span></td><td><span class=\"label label-", c.transport, " num", c.num.length, "\">", c.num, "</span></td><td>", c.name, "</td></tr></table></a></li>"].join(""))
							}), a.length || b.append(["<li class=\"line error\">", i18n.noRoutesFound, ".</li>"].join("")), f += a.length, e.html("&nbsp;(" + a.length + ")"), this.query.length < 3 ? (pg.loadGoogleMapsScript(function () {
							}), c.html("&nbsp;(" + f + ")")) : mobile.searchPlaces(this.query, function (a) {
								jQuery(".search-results li.place").remove(), jQuery.each(a, function (a, c) {
									b.append(["<li class=\"place\"><a id=\"", c.key, "\" onclick=\"return false;\" href=\"\" alt=\"", c.name, "\" title=\"", c.name, "\"><table><tr><td><span class=\"icon icon-marker\"></span></td><td>", c.name, "</td></tr></table></a></li>"].join(""))
								}), jQuery(".search-results li.place a").bind("click", function () {
									mobile.geocoder.getPlaceId(jQuery(this).attr("id"), function (a) {
										Hash.go("stop/" + a + "/map")
									})
								}), f += a.length, c.html("&nbsp;(" + f + ")")
							});
							return []
						}, matcher: function (a) {
							var b = ti.toAscii(a.num, !0), c = ti.toAscii(a.name, 2), d = ti.toAscii(this.query);
							if (this.query.length >= 1) {
								var e = -1;
								for (; ;) {
									e = c.indexOf(d, e + 1);
									if (e < 0)break;
									if (e == 0 || ti.wordSeparators.indexOf(c.charAt(e - 1)) >= 0)return !0
								}
							}
							if (b.indexOf(d) == 0) {
								if (b.length > d.length && "0123456789".indexOf(b.charAt(d.length)) >= 0)return !1;
								return !0
							}
							return !1
						}
					}), jQuery("#search .nav-search li").bind("click", function () {
						var a = jQuery(this).attr("data-filter");
						jQuery("#search .nav-search li").each(function (b, c) {
							var d = jQuery(this).children().attr("title"), e = jQuery(this).find("p").html();
							a == jQuery(this).attr("data-filter") ? jQuery(this).addClass("active").html(["<span title=\"", d, "\"><span><p>", e, "</p></span></span>"].join("")) : jQuery(this).removeClass("active").html(["<a title=\"", d, "\" alt=\"", d, "\"><span><p>", e, "</p></span></a>"].join(""))
						}), jQuery("#search .search-results").removeClass("all").removeClass("lines").removeClass("stops"), jQuery("#search .search-results").addClass(a)
					}), jQuery("#search .nav-region li").bind("click", function () {
						var a = jQuery(this).attr("data-city");
						jQuery("#search .nav-region li").each(function (b, c) {
							var d = jQuery(this).children().attr("title"), e = jQuery(this).find("p").html();
							a == jQuery(this).attr("data-city") ? jQuery(this).addClass("active").html(["<span title=\"", d, "\"><p>", e, "</p></span>"].join("")) : jQuery(this).removeClass("active").html(["<a title=\"", d, "\" alt=\"", d, "\"><p>", e, "</p></a>"].join(""))
						}), jQuery("#search .selected-city").val(a), jQuery("#search .form-search input").keyup()
					})
				}()
			})
		})
	}, searchPlaces: function (a, b) {
		pg.loadGoogleMapsScript(function () {
			!mobile.geocoder && cfg.defaultCity == "vilnius" && (mobile.geocoder = {
				bounds: new google.maps.LatLngBounds(new google.maps.LatLng(54.558526, 25.017815), new google.maps.LatLng(54.83248, 25.527247)),
				places: new google.maps.places.PlacesService($("divMapForPlaces")),
				autocomplete: new google.maps.places.AutocompleteService,
				references: {},
				index: {},
				interval: 300,
				timer: null,
				getPlaceId: function (a, b) {
					var c = this, d = {reference: this.references[a]};
					this.places.getDetails(d, function (a, d) {
						if (d == google.maps.places.PlacesServiceStatus.OK) {
							var e = Math.round(a.geometry.location.lat() * 1e5) / 1e5, f = Math.round(a.geometry.location.lng() * 1e5) / 1e5, g = [e, f].join(";");
							c.index[g] = a.name, b(g)
						}
					})
				},
				search: function (a, b) {
					var c = this;
					this.timer !== null && (window.clearTimeout(this.timer), this.timer = null), this.timer = window.setTimeout(function () {
						c.autocomplete.getPlacePredictions({
							input: a,
							bounds: c.bounds,
							componentRestrictions: {country: "lt"}
						}, function (a, d) {
							if (d == google.maps.places.PlacesServiceStatus.OK) {
								var e = [];
								for (var f = 0, g; g = a[f]; f++) {
									var h = g.description;
									if (g.description.toLowerCase().indexOf("vilni") == -1)continue;
									var i = h.split(",");
									i.length > 1 && (h = i[0], i[1] != " Vilnius" && (h += ", " + i[1]));
									var j = "@" + g.id;
									c.references[j] = g.reference, e.push({
										name: h,
										type: (g.types || []).join(", "),
										key: j
									})
								}
								b(e)
							} else b([])
						})
					}, c.interval)
				}
			}), mobile.geocoder && mobile.geocoder.search(a, b)
		})
	}, renderSchedule5: function () {
		pg.fScheduleLoad(function (a) {
			jQuery("#schedule5").html(i18n.loading), function () {
				var b = ti.fGetStopDetails(pg.schedule.stopId), c = pg.fUrlSet({
					schedule: pg.schedule,
					hashForMap: !0
				}, !0), d = [];
				jQuery.each(a.stops, function (a, c) {
					var e = jQuery.extend({}, pg.schedule);
					e.stopId = c.id, e.tripNum = "";
					var f = pg.fUrlSet({schedule: e}, !0);
					d.push(["<li><a href=\"#", f, "\"", c.id == b.id ? " class=\"strong\"" : "", " alt=\"", c.name, " ", c.time, "\" title=\"", c.name, " ", c.time, "\">", c.name, "<span class=\"time pull-right\">", c.time, "</span></a></li>"].join(""))
				});
				var e = a.trip[pg.schedule.stopId];
				if (e.previous_trip)var f = jQuery.extend({}, pg.schedule, {tripNum: e.previous_trip}), g = pg.fUrlSet({schedule: f}, !0);
				if (e.next_trip)var h = jQuery.extend({}, pg.schedule, {tripNum: e.next_trip}), i = pg.fUrlSet({schedule: h}, !0);
				document.title = i18n.routesAndTimetables;
				var j = i18n.btnFavouritesAdd, k = " add";
				favourites.belongs(pg.schedule, !0) && (j = i18n.btnFavouritesRemove, k = "");
				var l = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar toolbar-schedule toolbar-top\">", "<span class=\"icon icon-", pg.schedule.route.transport, "\"></span>", "<span class=\"label label-", pg.schedule.route.transport, "\">", pg.schedule.route.num, "</span>", "<p class=\"with-stop\">", pg.schedule.route.name, "<br>", "<strong>", b.name, "</strong></p>", "<div class=\"btn-group\">", "<a class=\"btn btn-favourite", k, "\" href=\"#\" alt=\"", j, "\" title=\"", j, "\"></a>", "<a class=\"btn btn-list\" href=\"#stop/", pg.schedule.stopId, "/", pg.language, "\" alt=\"", i18n.btnDepartures, "\" title=\"", i18n.btnDepartures, "\"></a>", "<a class=\"btn btn-map\" href=\"#", c, "\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a>", "</div>", "</div>", "</div>", cfg.city.urlVehicleDepartures ? ["<div class=\"row-fluid realtime-departures\">", "<div class=\"span12 toolbar toolbar-bottom next-departures\" role=\"region\"><h3>", i18n.realTimeDepartures, ":</h3><span>", i18n.loading, "</span>", "</div>", "</div>"].join("") : "", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-pager\">", "<li class=\"previous\">", e.previous_trip ? ["<a href=\"#", g, "\" alt=\"", i18n.prevTrip, "\" title=\"", i18n.prevTrip, "\"><p>", i18n.prevTrip, "</p></a>"].join("") : "<span>&nbsp;</span>", "</li>", "<li class=\"next\">", e.next_trip ? ["<a href=\"#", i, "\" alt=\"", i18n.nextTrip, "\" title=\"", i18n.nextTrip, "\"><div class=\"right\"><div class=\"cell\">", i18n.nextTrip, "</div></div></a>"].join("") : "<span>&nbsp;</span>", "</li>", "</ul>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list\">", d.join(""), "</ul>", "</div>", mobile.render_footer(), "</div>"];
				jQuery("#schedule5").html(l.join("")), mobile.renderMenu(), mobile.renderRealtimeDepartures(), jQuery("#schedule5 .btn-favourite").bind("click", function () {
					var a = jQuery(this);
					a.hasClass("add") ? (favourites.add(pg.schedule), a.removeClass("add").attr("alt", i18n.btnFavouritesRemove).attr("title", i18n.btnFavouritesRemove)) : (favourites.remove(pg.schedule), a.addClass("add").attr("alt", i18n.btnFavouritesAdd).attr("title", i18n.btnFavouritesAdd));
					return !1
				})
			}()
		})
	}, hideMenu: function () {
		jQuery("#menu").removeClass("open"), jQuery("#mobile-screen .container-fluid").removeClass("slide")
	}, renderMenu: function () {
		var a = jQuery("#menu");
		a.length && (jQuery("#mobile-screen .container-fluid").removeClass("slide").css({width: "auto"}), jQuery("#mobile-screen .btn-menu").unbind("click"), jQuery(window).unbind("resize"), a.remove());
		var b = cfg.city.ticketsURL[pg.language] || cfg.city.ticketsURL.en || "";
		b && (b = ["<li><a class=\"info\" href=\"", b, "\" alt=\"", i18n.menuTickets, "\" title=\"", i18n.menuTickets, "\"><p>", i18n.menuTickets, "</p></a></li>"].join("")), jQuery("#mobile-screen").prepend(["<div id=\"menu\"><ul class=\"nav\">", "<li><a class=\"home\" href=\"#/", pg.language, "\" alt=\"", i18n.menuHome, "\" title=\"", i18n.menuHome, "\"><p>", i18n.menuHome, "</p></a></li>", "<li><a class=\"schedule\" href=\"#", pg.language, "\" alt=\"", i18n.routesAndTimetables, "\" title=\"", i18n.routesAndTimetables, "\"><p>", i18n.routesAndTimetables, "</p></a></li>", "<li><a class=\"planner\" onclick=\"pg.fTabPlanner_Click();return false;\" href=\"#plan/", pg.language, "\" alt=\"", i18n.menuTripPlanner, "\" title=\"", i18n.menuTripPlanner, "\"><p>", i18n.menuTripPlanner, "</p></a></li>", "<li><a class=\"search\" href=\"#search/", pg.language, "\" alt=\"", i18n.menuSearch, "\" title=\"", i18n.menuSearch, "\"><p>", i18n.menuSearch, "</p></a></li>", "<li><a class=\"map\" onclick=\"pg.fTabShowMap_Click(event);return false;\" href=\"#map/", pg.language, "\" alt=\"", i18n.menuMap, "\" title=\"", i18n.menuMap, "\"><p>", i18n.menuMap, "</p></a></li>", b, "</ul></div>"].join("")), jQuery("#mobile-screen .btn-menu").bind("click", function () {
			if (jQuery("#menu").hasClass("open")) {
				jQuery("#menu").toggleClass("open");
				var a = jQuery("#mobile-screen .container-fluid");
				a.toggleClass("slide"), setTimeout(function () {
					jQuery("#menu").hide()
				}, 500)
			} else jQuery("#menu").show(), setTimeout(function () {
				jQuery("#menu").toggleClass("open");
				var a = jQuery("#mobile-screen .container-fluid");
				a.toggleClass("slide")
			}, 100)
		})
	}, renderSchedule4: function () {
		pg.fScheduleLoad(function (a) {
			pg.fScheduleLoadTimetable(function (a) {
				jQuery("#schedule4").html(i18n.loading), function () {
					var b = ti.fGetStopDetails(pg.schedule.stopId), c = pg.fUrlSet({
						schedule: pg.schedule,
						hashForMap: !0
					}, !0), d = [], e = a.workdays.length ? a.workdays[0] : null;
					jQuery.each(a.workdays, function (b, c) {
						var f = toUpperCase1(pg.fWeekdaysName(c));
						c == e ? d.push(["<li ", a.workdays.length == 2 && b == 1 ? "style='width:66%;' " : "", "class=\"active\" data-workdays=\"", c, "\"><span title=\"", f, "\"><span><p>", f, "</p></span></span></li>"].join("")) : d.push(["<li ", a.workdays.length == 2 && b == 1 ? "style='width:66%;' " : "", "class=\"\" data-workdays=\"", c, "\"><a alt=\"", f, "\" title=\"", f, "\"><span><p>", f, "</p></span></a></li>"].join(""))
					});
					var f = [];
					jQuery.each(a.timetables, function (b, c) {
						var d = null, g = null;
						f.push("<table class=\"table table-bordered timetable workdays-", b, b == e ? " active" : "", "\"><tbody>"), jQuery.each(a.timetables[b], function (c, e) {
							f.push("<tr><th>", e.hour, "</th><td>");
							var h = !1;
							for (var i = 0; i < a.maxlength[b]; i++)if (e.minutes.length > i) {
								var j = e.minutes[i], k = (j.min < 10 ? "0" : "") + j.min;
								j.classname == "highlighted" ? d = d || k : j.classname && (g = g || k);
								var l = j.classname == "highlighted" ? [" (", i18n.lowFloorDepartureTip, ")"].join("") : "";
								f.push(["<div class=\"cell\"><a alt=\"", k, l, "\" title=\"", k, j.title ? " " + j.title : l, "\" class=\"", j.classname, "\" href=\"#", j.hash, "\">", k, "</a></div>"].join(""))
							} else h || f.push("<div class=\"empty-cells\">"), h = !0, f.push("<div class=\"cell empty\"><span>00</span></div>");
							h && f.push("</div>"), f.push("</td></tr>")
						}), f.push("</tbody></table>"), (d || g) && f.push(["<div class=\"well legend workdays-", b, b == e ? " active" : "", "\">", d ? ["<p>", i18n.lowFloorDepartures.replace("00", d), "</p>"].join("") : "", g ? ["<p><span class=\"corner\">", g, "</span> ", i18n.scheduleModifiedRouteDepartures, "</p>"].join("") : "", "</div>"].join(""))
					}), document.title = i18n.routesAndTimetables;
					var g = i18n.btnFavouritesAdd, h = " add";
					favourites.belongs(pg.schedule, !0) && (g = i18n.btnFavouritesRemove, h = "");
					var i = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar toolbar-schedule toolbar-top\">", "<span class=\"icon icon-", pg.schedule.route.transport, "\"></span>", "<span class=\"label label-", pg.schedule.route.transport, "\">", pg.schedule.route.num, "</span>", "<p class=\"with-stop\">", pg.schedule.route.name, "<br/>", "<strong>", b.name, "</strong></p>", "<div class=\"btn-group pull-right\">", "<a class=\"btn btn-favourite", h, "\" href=\"#\" alt=\"", g, "\" title=\"", g, "\"></a>", "<a class=\"btn btn-list\" href=\"#stop/", pg.schedule.stopId, "/", pg.language, "\" alt=\"", i18n.btnDepartures, "\" title=\"", i18n.btnDepartures, "\"></a>", "<a class=\"btn btn-map\" href=\"#", c, "\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a>", "</div>", "</div>", "</div>", cfg.city.urlVehicleDepartures ? ["<div class=\"row-fluid realtime-departures\">", "<div class=\"span12 toolbar toolbar-bottom next-departures\" role=\"region\"><h3>", i18n.realTimeDepartures, ":</h3><span>", i18n.loading, "</span>", "</div>", "</div>"].join("") : "", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-timetable\">", d.join(""), "</ul>", "</div>", "<div class=\"row-fluid\">", f.join(""), "</div>", mobile.render_footer(), "</div>"];
					jQuery("#schedule4").html(i.join("")), mobile.renderMenu(), mobile.renderRealtimeDepartures(), jQuery("#schedule4 .btn-favourite").bind("click", function () {
						var a = jQuery(this);
						a.hasClass("add") ? (favourites.add(pg.schedule), a.removeClass("add").attr("alt", i18n.btnFavouritesRemove).attr("title", i18n.btnFavouritesRemove)) : (favourites.remove(pg.schedule), a.addClass("add").attr("alt", i18n.btnFavouritesAdd).attr("title", i18n.btnFavouritesAdd));
						return !1
					}), jQuery("#schedule4 ul.nav-timetable li").bind("click", function () {
						var a = jQuery(this).attr("data-workdays");
						jQuery("#schedule4 ul.nav-timetable li").each(function (b, c) {
							var d = jQuery(this).children().attr("title"), e = jQuery(this).find("p").html();
							a == jQuery(this).attr("data-workdays") ? jQuery(this).addClass("active").html(["<span title=\"", d, "\"><span><p>", e, "</p></span></span>"].join("")) : jQuery(this).removeClass("active").html(["<a title=\"", d, "\" alt=\"", d, "\"><span><p>", e, "</p></span></a>"].join(""))
						}), jQuery("#schedule4 .timetable").removeClass("active"), jQuery("#schedule4 .legend").removeClass("active"), jQuery("#schedule4 .workdays-" + a).addClass("active")
					})
				}()
			})
		})
	}, renderRealtimeDepartures: function () {
		pg.fProcessVehicleDepartures(null, function (a) {
			if (typeof a == "string")jQuery("#mobile-screen .realtime-departures span").html(a); else {
				if (typeof a == "object" && "status"in a) {
					jQuery("#mobile-screen .realtime-departures").hide(), jQuery("#mobile-screen .realtime-departures span").html(a.message);
					return
				}
				var b = jQuery.grep(a, function (a) {
					return a.route.num && pg.schedule && pg.schedule.num && a.route.num.toLowerCase() == pg.schedule.num && a.route.transport == pg.schedule.transport
				});
				if (b.length) {
					var c = [];
					jQuery.each(b, function (a, b) {
						c = c.concat(b.times)
					}), c.sort(function (a, b) {
						return a - b
					}), jQuery("#mobile-screen .realtime-departures span").html(["<strong>", jQuery.map(c, function (a, b) {
						var c = ti.printTime(a);
						b == 0 && (c = ["<span aria-live=\"polite\">", c, "</span>"].join(""));
						return c + "&ensp;"
					}).get().join(""), "</strong>"].join(""))
				} else jQuery("#mobile-screen .realtime-departures span").html((i18n.stopNoRealtimeDepartures + ".").split(".")[0]);
				jQuery("#mobile-screen .realtime-departures").show()
			}
		}), pg.loadedDepartingRoutes = pg.schedule.stopId
	}, renderSchedule3: function () {
		pg.fScheduleLoad(function (a) {
			jQuery("#schedule3").html(i18n.loading), function () {
				var b = pg.fUrlSet({
					schedule: {
						city: pg.schedule.city,
						dirType: pg.schedule.dirType,
						stopId: "",
						transport: pg.schedule.route.transport,
						num: pg.schedule.route.num
					}, hashForMap: !0
				}, !0), c = [];
				jQuery.each(a.stops, function (a, b) {
					c.push(["<li><a href=\"#", b.hash, "\" alt=\"", b.name, "\" title=\"", b.name, "\">", b.name, "</a></li>"].join(""))
				});
				var d = {
					city: pg.schedule.city,
					transport: pg.schedule.transport,
					num: pg.schedule.num,
					dirType: pg.schedule.dirType,
					stopId: "$"
				};
				document.title = i18n.routesAndTimetables;
				var e = i18n.btnFavouritesAdd, f = " add";
				favourites.belongs(d, !0) && (e = i18n.btnFavouritesRemove, f = "");
				var g = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar toolbar-schedule\">", "<span class=\"icon icon-", pg.schedule.route.transport, "\"></span>", "<span class=\"label label-", pg.schedule.route.transport, "\">", pg.schedule.route.num, "</span>", "<p class=\"with-buttons\">", pg.schedule.route.name, "</p>", "<div class=\"btn-group pull-right\">", "<a class=\"btn btn-favourite", f, "\" href=\"#\" alt=\"", e, "\" title=\"", e, "\"></a>", "<a class=\"btn btn-map\" href=\"#", b, "\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a>", "</div>", "</div>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list\">", c.join(""), "</ul>", "</div>", mobile.render_footer(), "</div>"];
				jQuery("#schedule3").html(g.join("")), mobile.renderMenu(), jQuery("#schedule3 .btn-favourite").bind("click", function () {
					var a = jQuery(this);
					a.hasClass("add") ? (favourites.add(d), a.removeClass("add").attr("alt", i18n.btnFavouritesRemove).attr("title", i18n.btnFavouritesRemove)) : (favourites.remove(d), a.addClass("add").attr("alt", i18n.btnFavouritesAdd).attr("title", i18n.btnFavouritesAdd));
					return !1
				})
			}()
		})
	}, renderSchedule2: function () {
		pg.fScheduleLoad(function (a) {
			jQuery("#schedule2").html(i18n.loading), function () {
				if (typeof pg.schedule.route == "undefined")window.location.hash = "/" + pg.schedule.transport + "/" + pg.language; else {
					var b = [];
					jQuery.each(a.directions[1].concat(a.directions[2]), function (a, c) {
						var d = "";
						b.push(["<li><a href=\"#", c.hash, "\" alt=\"", c.name, d, "\" title=\"", c.name, d, "\">", c.name, d, "</a></li>"].join(""))
					});
					var c = pg.fUrlSet({
						schedule: {
							city: pg.schedule.city,
							dirType: "",
							transport: (pg.schedule.route || pg.schedule).transport,
							num: (pg.schedule.route || {num: ""}).num
						}, hashForMap: !0
					}, !0);
					document.title = i18n.routesAndTimetables;
					var d = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", "<div class=\"row-fluid\">", "<div class=\"span12 toolbar toolbar-schedule\">", "<span class=\"icon icon-", pg.schedule.route.transport, "\"></span>", "<span class=\"label label-", pg.schedule.route.transport, " num", pg.schedule.route.num.length, "\">", pg.schedule.route.num, "</span>", "<p>", pg.schedule.route.name, "</p>", "<div class=\"btn-group pull-right\">", "<a class=\"btn btn-map\" href=\"#", c, "\" alt=\"", i18n.showInMap, "\" title=\"", i18n.showInMap, "\"></a>", "</div>", "</div>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list\">", b.join(""), "</ul>", "</div>", mobile.render_footer(), "</div>"];
					jQuery("#schedule2").html(d.join("")), mobile.renderMenu()
				}
			}()
		})
	}, renderSchedule: function () {
		pg.fCreateNavigation(function (a) {
			pg.fLoadRoutesList(function (b) {
				jQuery("#schedule").html(i18n.loading), function () {
					var c = [], d = [];
					jQuery.each(a, function (a, b) {
						b.city == pg.city || !pg.city && a == 0 ? c.push(["<li class=\"active ", b.city, "\"><span title=\"", b.name, "\"><p>", b.name, "</p></span></li>"].join("")) : c.push(["<li class=\"", b.city, "\"><a href=\"#", b.hash, "\" alt=\"", b.name, "\" title=\"", b.name, "\"><p>", b.name, "</p></a></li>"].join("")), jQuery.each(b.transport, function (a, c) {
							c.transport == pg.transport ? d.push(["<li class=\"active ", c.transport, " ", b.city, "\"><span title=\"", c.name, "\"><p>", c.name, "</p></span></li>"].join("")) : d.push(["<li class=\"", c.transport, " ", b.city, "\"><a href=\"#", c.hash, "\" alt=\"", c.name, "\" title=\"", c.name, "\"><p>", c.name, "</p></a></li>"].join(""))
						})
					});
					var e = [];
					jQuery.each(b, function (a, b) {
						var c = pg.fUrlSet({
							schedule: {city: b.city, transport: b.transport, num: b.num},
							hashForMap: ""
						}, !0);
						e.push(["<li><a href=\"#", c, "\" alt=\"", b.num, " ", b.name, "\" title=\"", b.num, " ", b.name, "\"><span class=\"label label-", b.transport, " num", b.num.length, "\">", b.num, "</span>", b.name, "</a></li>"].join(""))
					}), document.title = i18n.routesAndTimetables;
					var f = ["<div class=\"container-fluid\">", "<div class=\"row-fluid\">", "<div class=\"span12 header\">",  "<h1>", i18n.routesAndTimetables, "</h1>", "</div>", "</div>", cfg.city.region ? "<div class=\"row-fluid\"><ul class=\"nav nav-tabs nav-region\">" + c.join("") + "</ul></div>" : "", "<div class=\"row-fluid\">", "<ul class=\"nav nav-tabs nav-transport ", pg.city, "\">", d.join(""), "</ul>", "</div>", "<div class=\"row-fluid\">", "<ul class=\"nav nav-list with-labels\">", e.join(""), "</ul>", "</div>", mobile.render_footer(), "</div>"];
					jQuery("#schedule").html(f.join("")), mobile.renderMenu()
				}()
			})
		})
	}, renderIndex: function () {
		jQuery("#index").html(i18n.loading), function () {
			var a = pg.fUrlSet({city: pg.city, transport: pg.transport, hashForMap: null}, !0);
			document.title = i18n.headerTitleMobile;
			var b = cfg.city.ticketsURL[pg.language] || cfg.city.ticketsURL.en || "";
			b ? b = ["<a class=\"span6 shortcut shortcut-info\" href=\"", b, "\" alt=\"", i18n.menuTickets, "\" title=\"", i18n.menuTickets, "\">", i18n.menuTickets, "</a>"].join("") : b = "<a class=\"span6 shortcut\"></a>";
			var c = ["<div class=\"container-fluid\">", "<div class=\"row-fluid shortcuts\">", "<a class=\"span6 shortcut shortcut-schedule\" href=\"#", a, "\" alt=\"", i18n.routesAndTimetables, "\" title=\"", i18n.routesAndTimetables, "\">", i18n.routesAndTimetables, "</a>", "<a class=\"span6 shortcut shortcut-planner\" onclick=\"pg.fTabPlanner_Click();return false;\" href=\"#plan/", pg.language, "\" alt=\"", i18n.menuTripPlanner, "\" title=\"", i18n.menuTripPlanner, "\">", i18n.menuTripPlanner, "</a>", "</div>", "<div class=\"row-fluid shortcuts\">", "<a class=\"span6 shortcut shortcut-search\" href=\"#search/", pg.language, "\" alt=\"", i18n.menuSearch, "\" title=\"", i18n.menuSearch, "\">", i18n.menuSearch, "</a>", "<a class=\"span6 shortcut shortcut-map\" onclick=\"pg.fTabShowMap_Click(event);return false;\" href=\"#map/", pg.language, "\" alt=\"", i18n.menuMap, "\" title=\"", i18n.menuMap, "\">", i18n.menuMap, "</a>", "</div>", "<div class=\"row-fluid shortcuts\" style=\"display:none;\">", "<a class=\"span6 shortcut shortcut-favourites\" href=\"#favourites/", pg.language, "\" alt=\"", i18n.menuFavorites, "\" title=\"", i18n.menuFavorites, "\">", i18n.menuFavorites, "</a>", b, "</div>", mobile.render_footer(), "</div>"];
			jQuery("#index").html(c.join(""))
		}()
	}
}, ti = {
	stops: 0,
	routes: 0,
	taxi: [],
	specialDates: {},
	specialWeekdays: {},
	asciiStops: {},
	FLD_ID: 0,
	FLD_CITY: 1,
	FLD_AREA: 2,
	FLD_STREET: 3,
	FLD_NAME: 4,
	FLD_INFO: 5,
	FLD_LNG: 6,
	FLD_LAT: 7,
	FLD_STOPS: 8,
	FLD_DIRS: 9,
	RT_ROUTEID: 0,
	RT_ORDER: 1,
	RT_ROUTENUM: 2,
	RT_AUTHORITY: 3,
	RT_CITY: 4,
	RT_TRANSPORT: 5,
	RT_OPERATOR: 6,
	RT_VALIDITYPERIODS: 7,
	RT_SPECIALDATES: 8,
	RT_ROUTETAG: 9,
	RT_ROUTETYPE: 10,
	RT_COMMERCIAL: 11,
	RT_ROUTENAME: 12,
	RT_WEEKDAYS: 13,
	RT_ENTRY: 14,
	RT_STREETS: 15,
	RT_ROUTESTOPSPLATFORMS: 16,
	RT_ROUTESTOPS: 17,
	accent_map: {
		"Д…": "a",
		"Г¤": "a",
		"ДЃ": "a",
		"ДЌ": "c",
		"Д™": "e",
		"Д—": "e",
		"ДЇ": "i",
		"Еі": "u",
		"Е«": "u",
		"Гј": "u",
		"Еѕ": "z",
		"Д“": "e",
		"ДЈ": "g",
		"Д«": "i",
		"Д·": "k",
		"Дј": "l",
		"Е†": "n",
		"Г¶": "o",
		"Гµ": "o",
		"ЕЎ": "s",
		"Р°": "a",
		"Р±": "b",
		"РІ": "v",
		"Рі": "g",
		"Рґ": "d",
		"Рµ": "e",
		"С‘": "e",
		"Р¶": "zh",
		"Р·": "z",
		"Рё": "i",
		"Р№": "j",
		"Рє": "k",
		"Р»": "l",
		"Рј": "m",
		"РЅ": "n",
		"Рѕ": "o",
		"Рї": "p",
		"СЂ": "r",
		"СЃ": "s",
		"С‚": "t",
		"Сѓ": "u",
		"С„": "f",
		"С…": "x",
		"С†": "c",
		"С‡": "ch",
		"С€": "sh",
		"С‰": "shh",
		"СЉ": !0,
		"С‹": "y",
		"СЊ": !0,
		"СЌ": "je",
		"СЋ": "ju",
		"СЏ": "ja",
		"в„–": "n",
		"вЂ“": "-",
		"вЂ”": "-",
		"М¶": "-",
		"В­": "-",
		"Л—": "-",
		"вЂњ": !0,
		"вЂќ": !0,
		"вЂћ": !0,
		"'": !0,
		"\"": !0
	},
	wordSeparators: "вЂ“вЂ”М¶В­Л—вЂњвЂќвЂћ _-.()'\""
};
ti.SERVER = typeof window == "object" ? 1 : !0, typeof window == "object" && typeof console == "undefined" && (window.console = {
	log: function () {
	}
}), String.prototype.trim = function () {
	return this.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
}, ti.dateToMinutes = function (a, b) {
	var c = +a / 6e4;
	b || (c = Math.floor(c)), c -= a.getTimezoneOffset();
	return c
}, ti.dateToDays = function (a) {
	return Math.floor(ti.dateToMinutes(a) / 1440)
}, ti.printTime = function (a, b, c) {
	if (a < 0)return "";
	!b && b !== "" && (b = ":");
	var d = ~~a, e = ~~(d / 60);
	typeof cfg == "object" && cfg.defaultCity != "intercity" && (e %= 24), d = d % 60;
	return (c && e < 10 ? c : "") + e + b + (d < 10 ? "0" : "") + d
}, ti.toMinutes = function (a) {
	var b = a.trim(), c = b.length, d = parseInt(b.substr(c - 2, 2), 10);
	return c > 2 ? d + parseInt(b.substr(0, c - 2), 10) * 60 : d * 60
}, ti.fDownloadUrl = function (a, b, c, d) {
	if (a && b && c) {
		var e;
		if (!window.XMLHttpRequest || window.location.protocol === "file:" && window.ActiveXObject) {
			try {
				e = new ActiveXObject("MSXML2.XMLHTTP.6.0")
			} catch (f) {
			}
			if (!e)try {
				e = new ActiveXObject("MSXML2.XMLHTTP")
			} catch (f) {
			}
			if (!e)try {
				e = new ActiveXObject("Microsoft.XMLHTTP")
			} catch (f) {
			}
		} else e = new XMLHttpRequest, (d || b.indexOf("http") == 0) && !("withCredentials"in e) && typeof XDomainRequest != "undefined" && (e = new XDomainRequest, e.open(a, b), e.onload = function () {
			c(e.responseText)
		});
		e.open(a, b, !0), e.onreadystatechange = function () {
			if (e.readyState == 4)if (e.status == 200 || e.status == 0)typeof e.responseText == "string" ? c(e.responseText) : typeof e.responseXML == "string" ? c(e.responseXML) : c(e.responseText)
		};
		try {
			e.send(null)
		} catch (g) {
		}
	}
}, ti.toAscii = function (a, b) {
	var c = a.toLowerCase(), d = c.split(""), e, f = ti.accent_map;
	for (var g = d.length; --g >= 0;)(e = f[d[g]]) ? (d[g] = e === !0 ? "" : e, c = !1) : b === !0 && d[g] === " " && (d[g] = "", c = !1);
	b === 2 && (c = d.join("").trim().replace(/\s+-/g, "-").replace(/-\s+/g, "-"));
	return c || d.join("")
}, ti.cloneObject = function (a) {
	var b = a instanceof Array ? [] : {};
	for (var c in a)a[c] && typeof a[c] == "object" ? b[c] = a[c].clone() : b[c] = a[c];
	return b
}, ti.naturalSort = function (a, b) {
	var c = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi, d = /(^[ ]*|[ ]*$)/g, e = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/, f = /^0x[0-9a-f]+$/i, g = /^0/, h = a.toString().replace(d, "") || "", i = b.toString().replace(d, "") || "", j = h.replace(c, "\u0000$1\u0000").replace(/\0$/, "").replace(/^\0/, "").split("\u0000"), k = i.replace(c, "\u0000$1\u0000").replace(/\0$/, "").replace(/^\0/, "").split("\u0000"), l = parseInt(h.match(f)) || j.length != 1 && h.match(e) && Date.parse(h), m = parseInt(i.match(f)) || l && i.match(e) && Date.parse(i) || null;
	if (m) {
		if (l < m)return -1;
		if (l > m)return 1
	}
	for (var n = 0, o = Math.max(j.length, k.length); n < o; n++) {
		oFxNcL = !(j[n] || "").match(g) && parseFloat(j[n]) || j[n] || 0, oFyNcL = !(k[n] || "").match(g) && parseFloat(k[n]) || k[n] || 0;
		if (isNaN(oFxNcL) !== isNaN(oFyNcL))return isNaN(oFxNcL) ? 1 : -1;
		typeof oFxNcL !== typeof oFyNcL && (oFxNcL += "", oFyNcL += "");
		if (oFxNcL < oFyNcL)return -1;
		if (oFxNcL > oFyNcL)return 1
	}
	return 0
}, ti.loadData = function () {
	if (typeof cfg === "object" && cfg.city && cfg.city.datadir) {
		var a = new Date;
		a = a.setHours(a.getHours() - 2, 0, 0, 0), cfg.city.datadir.indexOf("http") == 0 ? (ti.fDownloadUrl("get", cfg.city.datadir + "routes.txt&timestamp=" + a, ti.loadRoutes, !0), ti.fDownloadUrl("get", cfg.city.datadir + "stops.txt&timestamp=" + a, ti.loadStops, !0)) : (ti.fDownloadUrl("get", cfg.city.datadir + "/routes.txt?" + a, ti.loadRoutes), ti.fDownloadUrl("get", cfg.city.datadir + "/stops.txt?" + a, ti.loadStops))
	} else ti.fDownloadUrl("get", "routes.txt", ti.loadRoutes), ti.fDownloadUrl("get", "stops.txt", ti.loadStops);
	cfg.defaultCity === "latvia" && ti.fDownloadUrl("get", "taxi.txt", ti.loadTaxi)
}, ti.loadStops = function (a, b) {
	a = a.split(b || "\n");
	var c = "", d = "", e = "", f = "", g = "", h = "", i = "", j = {}, k = {}, l = [], m = a.length, n = a[0].toUpperCase().split(";"), o = {};
	for (var p = n.length; --p >= 0;)o[n[p]] = p;
	o.ID = 0;
	for (var p = 1; p < m; p++)if (a[p].length > 1) {
		var q = a[p].split(";"), r = q[o.CITY];
		r && (d = r === "0" ? "" : r.trim());
		var s = c + ti.toAscii(q[o.ID], !0), t = q[o.SIRIID];
		if (r = q[o.AREA])e = r === "0" ? "" : r.trim();
		if (r = q[o.STREET])f = r === "0" ? "" : r.trim();
		if (r = q[o.NAME]) {
			g = r === "0" ? "" : r, h = ti.toAscii(r);
			var u = k[h];
			k[h] = u ? u + "," + s : s
		} else k[h] += "," + s;
		if (r = q[o.INFO])i = r === "0" ? "" : r;
		c && (q[o.STOPS] = c + (q[o.STOPS] || "").replace(/,/g, "," + c));
		var v = {
			id: s,
			siriID: t,
			lat: +q[o.LAT] / 1e5,
			lng: +q[o.LNG] / 1e5,
			name: g,
			city: d,
			info: i,
			raw_data: [s, d, e, f, g, i, q[o.LNG], q[o.LAT], q[o.STOPS]].join(";")
		};
		ti.SERVER && (v.routes = [], v.neighbours = q[o.STOPS] ? q[o.STOPS].split(",") : []), j[s] = v, l.push(v)
	}
	ti.stops = null, ti.stops = j, ti.asciiStops = k, l.sort(function (a, b) {
		return a.lat < b.lat ? -1 : a.lat > b.lat ? 1 : 0
	});
	for (p = l.length; --p > 0;)if (l[p].city === "kautra") {
		var w = l[p].lat;
		for (var x = p - 1; --x >= 0;) {
			var y = w - l[x].lat;
			if (y > .015)break;
			var z = l[p].lng - l[x].lng;
			z > -.015 && z < .015 && (l[p].neighbours.push(l[x].id), l[x].neighbours.push(l[p].id))
		}
	}
	typeof ti.routes == "string" && (ti.SERVER === !0 ? ti.loadRoutes(ti.routes) : window.setTimeout(function () {
		ti.loadRoutes(ti.routes)
	}, 10))
}, ti.loadRoutes = function (a, b) {
	if (typeof ti.stops !== "object")ti.routes = a; else {
		a = a.split(b || "\n");
		var c = [], d = ti.stops, e = {}, f = "", g = "", h = "", i = "", j = "", k = "", l = "", m = "", n = "", o = "", p = "", q = "", r = 0, s = a[0].toUpperCase().split(";"), t = {};
		for (var u = s.length; --u >= 0;)t[s[u]] = u;
		t.ROUTENUM = 0, t.ROUTESTOPSPLATFORMS || (t.ROUTESTOPSPLATFORMS = -1);
		var v = -1, w = a.length;
		for (var u = 1; u < w; u++)if (a[u].charAt(0) === "#") {
			var x = a[u].split("#"), y = null, z = null, A = new Date;
			x[1] !== "" && (y = new Date(x[1])), x[2] !== "" && (z = new Date(x[2]));
			if ((!y || y <= A) && (!z || z >= A)) {
				var B = {comment: x[3]};
				x[4] && (B.departures = x[4]), x[5] && (B.weekdays = x[5]), x[6] && (B.directions = x[6]);
				var C = c[v];
				C.comments ? C.comments.push(B) : C.comments = [B]
			}
		} else if (a[u].length > 1) {
			var x = a[u].split(";"), D;
			if (D = x[t.AUTHORITY])h = D === "0" ? "" : D;
			if (h === "SpecialDates") {
				var E = {}, F = x[t.VALIDITYPERIODS].split(","), G = 0, H = 0, I = x[t.ROUTENUM] == "";
				for (var J = -1, K = F.length; ++J < K;)F[J] && (G = +F[J]), H += G, I ? ti.specialWeekdays[H] = +x[t.WEEKDAYS] : E[H] = !0;
				I || (ti.specialDates[x[t.ROUTENUM]] = E);
				continue
			}
			++r, ++v;
			if (D = x[t.ROUTENUM])f = D === "0" ? "" : D, r = 1;
			if (D = x[t.ROUTENAME])g = D;
			if (D = x[t.CITY])i = D === "0" ? "" : D, l = i + "_" + k, r = 1;
			if (D = x[t.TRANSPORT])k = D === "0" ? "" : D, l = i + "_" + k, r = 1;
			l && (typeof pg === "object" && (pg.cityTransportRoutes[i + "_" + k] = !0), l = "");
			if (D = x[t.OPERATOR])m = D === "0" ? "" : D;
			if (D = x[t.VALIDITYPERIODS])n = D === "0" ? "" : D;
			if (D = x[t.SPECIALDATES])o = D === "0" ? "" : D;
			if (D = x[t.WEEKDAYS])p = D === "0" ? "" : D;
			q = t.STREETS ? x[t.STREETS] : "";
			var L = ti.toAscii(x[t.ROUTESTOPS], !0).split(","), M = !1;
			for (var N = 0, O = L.length; N < O; ++N) {
				var P = L[N];
				P.charAt(0) === "e" ? (M || (M = (new Array(N + 1)).join("0").split("")), M[N] = "1", P = P.substring(1), L[N] = P) : P.charAt(0) === "x" ? (M || (M = (new Array(N + 1)).join("0").split("")), M[N] = "2", P = P.substring(1), L[N] = P) : M && (M[N] = "0"), j && (P = L[N] = j + P);
				var Q = d[P];
				Q ? (e[P] = !0, Q.raw_data += ";" + v + ";" + N, (!0 || ti.SERVER) && Q.routes.push(v, N)) : (L.splice(N, 1), --O, --N)
			}
			var R = [v, r, f, h, i, k, m, n, o, x[t.ROUTETAG], ti.toAscii(x[t.ROUTETYPE]), x[t.COMMERCIAL], g, p, M && M.join("") || "", q, x[t.ROUTESTOPSPLATFORMS] || "", L.join(";")].join(";");
			++u, ti.SERVER === !0 ? c[v] = {
				id: v,
				authority: h,
				city: i,
				transport: k,
				num: f,
				name: g,
				stops: L,
				platforms: x[t.ROUTESTOPSPLATFORMS] || "",
				entry: M && M.join("") || "",
				specialDates: o.split(","),
				times: a[u],
				raw_data: R
			} : c[v] = {id: v, times: a[u], raw_data: R}
		}
		ti.routes = null, ti.routes = c;
		if (typeof cfg === "object" && cfg.defaultCity !== "helsinki" && cfg.defaultCity !== "pppskov" && cfg.defaultCity !== "kautra")for (var P in d)!e[P] && P.charAt(0) != "a" && (d[P].name = "");
		if (typeof cfg === "object" && cfg.defaultCity == "latvia")for (var P in d)d[P].city = ti.toAscii(d[P].city);
		typeof window === "object" && typeof pg === "object" && (pg.fCreateNavigation(), pg.fTabActivate()), typeof pg != "undefined" && typeof google != "undefined" && google.maps && google.maps.Geocoder && (pg.geocoder = {
			g: new google.maps.Geocoder,
			interval: 300,
			previousInput: "",
			timer: null,
			cache: {},
			index: {},
			search: function (a, b) {
				a = (a || "").trim();
				if (a.length < 3)return [];
				this.previousInput = a, this.timer !== null && (window.clearTimeout(this.timer), this.timer = null), a in this.cache ? b(this.cache[a]) : this.timer = window.setTimeout(function () {
					pg.geocoder.g.geocode({
						address: a,
						region: cfg.defaultLanguage,
						componentRestrictions: {
							country: cfg.defaultLanguage,
							locality: cfg.defaultCity == "intercity" ? "Lithuania" : cfg.defaultCity
						}
					}, function (c, d) {
						if (d == google.maps.GeocoderStatus.OK) {
							c.length && c[0].formatted_address.indexOf("Riga, ") == 0 && (c = []), c.length && c[0].formatted_address.indexOf("Vilnius, ") == 0 && (c = []);
							var e = [];
							for (var f = 0; f < c.length; ++f) {
								var g = c[f], h = g.formatted_address.split(","), i = h.length > 1 && (h[0] + h[1]).length <= 36 ? [h[0], h[1]].join(",") : h[0], j = Math.round(g.geometry.location.lat() * 1e5) / 1e5, k = Math.round(g.geometry.location.lng() * 1e5) / 1e5;
								pg.geocoder.index[[j, k].join(";")] = i, e.push({
									name: i,
									address: g.formatted_address,
									lat: j,
									lng: k
								})
							}
							pg.geocoder.cache[a] = e, b(e)
						} else b([])
					})
				}, this.interval)
			}
		})
	}
}, ti.loadTaxi = function (a, b) {
	ti.taxi = [], a = a.split(b || "\n");
	var c = a.length, d = a[0].toUpperCase().split(";"), e = {};
	for (var f = d.length; --f >= 0;)e[d[f]] = f;
	for (var f = 1; f < c; f++)if (a[f].length > 1) {
		var g = a[f].split(";"), h = {
			name: g[e.NAME],
			lat: parseFloat(g[e.LAT].replace(",", ".")),
			lng: parseFloat(g[e.LNG].replace(",", ".")),
			radius: parseInt(g[e.RADIUS]),
			phone: g[e.PHONE]
		};
		h.radius *= h.radius, ti.taxi.push(h)
	}
}, ti.fGetStopsByName = function (a) {
	if (typeof ti.stops !== "object")return [];
	var b = ti.toAscii(a), c = b.replace(/\W/g, ""), d = a.toLowerCase().replace(/\W/g, ""), e = [], f = ti.wordSeparators, g = ti.asciiStops;
	for (var h in g) {
		var i = -1;
		for (; ;) {
			i = h.indexOf(b, i + 1);
			if (i < 0)break;
			if (i === 0 || f.indexOf(h.charAt(i - 1)) >= 0) {
				var j = g[h].split(",");
				for (var k = j.length; --k >= 0;) {
					var l = ti.fGetStopDetails(j[k]);
					l.name && (d === c || l.name.toLowerCase().replace(/\W/g, "").indexOf(d) >= 0) && (l.indexOf = i, e.push(l))
				}
			}
		}
	}
	var m = {};
	for (var k = 0; k < e.length; k++) {
		var l = e[k], n = parseInt(l.id, 10) || l.id, o = m[n];
		o ? o.id += "," + l.id : (m[n] = o = l, o.streetIsIncluded = {}), l.street && l.street !== "-" && !o.streetIsIncluded[l.street] && (o.streetIsIncluded[l.street] = !0, o.streets = (o.streets ? o.streets + ", " : "") + l.street)
	}
	var p = {}, q = [];
	for (var r in m) {
		var l = m[r], n = l.name.replace(/[^a-zA-Z0-9\u0400-\u04FF]/g, "") + ";" + l.streets;
		typeof cfg == "object" && cfg.defaultCity == "intercity" && (n += ";" + l.area);
		var o = p[n];
		o ? o.id += "," + l.id : (p[n] = l, q.push(l))
	}
	q.sort(function (a, b) {
		if (typeof cfg == "object" && cfg.defaultCity == "intercity") {
			if (a.info && !b.info)return -1;
			if (!a.info && b.info)return 1;
			var c = /[a-zA-Z]/g;
			if (c.test(a.name)) {
				if (!c.test(b.name))return -1
			} else if (c.test(b.name))return 1
		}
		if (a.id.charAt(0) === "a" && b.id.charAt(0) !== "a")return -1;
		if (b.id.charAt(0) === "a" && a.id.charAt(0) !== "a")return 1;
		if (typeof pg !== "undefined") {
			if (a.city === pg.city && b.city !== pg.city)return -1;
			if (a.city !== pg.city && b.city === pg.city)return 1
		}
		if (a.indexOf === 0 && b.indexOf !== 0)return -1;
		if (b.indexOf === 0 && a.indexOf !== 0)return 1;
		if (a.name < b.name)return -1;
		if (b.name < a.name)return 1;
		if (a.area < b.area)return -1;
		if (b.area < a.area)return 1;
		if (a.streets < b.streets)return -1;
		if (b.streets < a.streets)return 1;
		return 0
	});
	return q
}, ti.fGetAnyStopDetails = function (a, b) {
	if (!a) {
		if (b) {
			b({});
			return
		}
		return {}
	}
	if (typeof ti.stops !== "object") {
		if (b) {
			setTimeout(function () {
				ti.fGetAnyStopDetails(a, b)
			}, 200);
			return
		}
		return {}
	}
	var c = typeof a == "string" ? a.split(",") : a, d, e, f, g;
	e = f = g = 0;
	for (var h = 0; h < c.length; ++h) {
		var i = ti.fGetStopDetails(c[h]);
		!d && i.id && (d = i), i && i.lat && i.lng && (e += i.lat, f += i.lng, ++g)
	}
	g && (d.latAvg = e / g, d.lngAvg = f / g);
	if (b)b(d || {}); else return d || {}
}, ti.fGetStopDetails = function (a) {
	if (typeof ti.stops !== "object" || !a)return {};
	var b = ti.stops[a], c;
	if (!b) {
		var d = a.indexOf(";");
		if (d > 0) {
			c = {
				id: a,
				name: typeof pg != "undefined" && a == pg.myLocation && i18n.myLocation || typeof mobile != "undefined" && mobile.geocoder && mobile.geocoder.index[a] || typeof pg != "undefined" && pg.geocoder && pg.geocoder.index[a] || (typeof i18n == "object" ? i18n.mapPoint : "Point on map"),
				neighbours: "",
				lat: parseFloat(a.substr(0, d)),
				lng: parseFloat(a.substr(d + 1)),
				raw_data: ""
			};
			return c
		}
		return {}
	}
	var e = b.raw_data.split(";");
	c = {
		id: e[ti.FLD_ID],
		city: e[ti.FLD_CITY],
		area: e[ti.FLD_AREA],
		street: e[ti.FLD_STREET],
		name: b.name,
		info: e[ti.FLD_INFO],
		neighbours: e[ti.FLD_STOPS],
		lng: ti.stops[a].lng,
		lat: ti.stops[a].lat,
		raw_data: b.raw_data
	};
	return c
}, ti.fGetTransfersAtStop = function (a, b, c) {
	var d = ti.stops, e = [a], f = parseInt(a, 10);
	if (f && "" + f !== "" + a && cfg.defaultCity !== "druskininkai" && cfg.defaultCity !== "riga")for (var g in d)f == parseInt(g, 10) && e.push(g);
	return ti.fGetRoutesAtStop(e, !1, b, c)
}, ti.fGetRoutesAtStop = function (a, b, c, d) {
	var e = d && d.dirType || "-", f = d && d.id || null, g = [], h = typeof a == "string" ? a.split(",") : a, i = e.split("-"), j = i[0], k = i[i.length - 1], l = j.charAt(0), m = k.charAt(0);
	for (var n = h.length; --n >= 0;) {
		var o = (ti.stops[h[n]] || {raw_data: ""}).raw_data.split(";"), p = o.length;
		for (var q = ti.FLD_DIRS; q < p; q += 2) {
			var r = ti.fGetRoutes(o[q]), s = +o[q + 1] < r.stops.length - 1;
			(s || c) && (b || !r.routeTag || r.id === f) && (r.stopId = h[n], e && (r.dirType.indexOf(e) < 0 && e.indexOf(r.dirType) < 0 && r.dirType.indexOf("-d") < 0 && j !== k && (r.dirType.indexOf(k) == 0 || r.dirType.indexOf(j) == r.dirType.length - 1 || r.dirType.indexOf("-" + m) < 0 && r.dirType.indexOf(j + "-") < 0 && r.dirType.indexOf(l + "-") < 0 && (r.dirType.indexOf("c") < 0 || r.dirType.indexOf("c") >= r.dirType.length - 2))) ? r.sortKey = "1" : r.sortKey = "0", r.sortKey = [cfg.transportOrder[r.transport] || "Z", ("000000" + parseInt(r.num, 10)).slice(-6), ("000000" + parseInt(r.num.substr(1), 10)).slice(-6), (r.num + "00000000000000000000").substr(0, 20), n === 0 ? "0" : "1", s ? "0" : "1", r.sortKey, ("000000" + r.order).slice(-6)].join(""), g.push(r))
		}
	}
	g.sort(function (a, b) {
		if (a.sortKey < b.sortKey)return -1;
		if (a.sortKey > b.sortKey)return 1;
		return 0
	});
	return g
}, ti.fGetRoutes = function (a, b, c, d, e, f) {
	var g = [], h = {}, i = -1, j = 0, k, l, m, n, o = ti.wordSeparators;
	f && (f = ti.toAscii("" + f, 2)), isNaN(a) ? a && typeof a == "object" ? l = a : (k = ti.routes, i = 0, j = k.length, m = c && ti.toAscii(c, !0), a = "," + a + ",") : l = ti.routes[+a];
	while (i < j) {
		i >= 0 && (l = k[i]);
		var p = l.raw_data.split(";"), q = p[ti.RT_CITY], r = p[ti.RT_TRANSPORT], s = p[ti.RT_ORDER], t = p[ti.RT_ROUTENUM], u;
		r == "expressbus" && t && t.charAt(t.length - 1) == "G" ? u = t.substring(0, t.length - 1) + "<sup>G</sup>" : u = t, t = ti.toAscii(t, !0);
		var v = p[ti.RT_ROUTETAG];
		if (i < 0 || ("," + q + ",").indexOf(a) >= 0 && (!b || b === r) && (!m || m === t && (!v || e === !0 || e === "0" && v.indexOf("0") < 0)) && (!d || d === p[ti.RT_ROUTETYPE])) {
			if (f) {
				var w = t.indexOf(f);
				w === 0 && t.length > f.length && "0123456789".indexOf(t.charAt(f.length)) >= 0 && (w = -1);
				if (w !== 0) {
					var x = ti.toAscii(p[ti.RT_ROUTENAME], 2);
					w = x.indexOf(f), w > 0 && o.indexOf(x.charAt(w - 1)) < 0 && o.indexOf(f.charAt(0)) < 0 && (w = -1)
				}
				if (w >= 0 || f === "*") {
					w = 0, n = ti.toAscii(q + ";" + r + ";" + t + ";" + p[ti.RT_ROUTENAME], !0);
					var y = h[n];
					y && (w = -1, y.weekdays += p[ti.RT_WEEKDAYS])
				}
				if (w < 0 || v) {
					++i;
					continue
				}
			} else if (i >= 0 && !m) {
				n = ti.toAscii(q + ";" + r + ";" + t, !0);
				var y = h[n];
				if (y) {
					var z = p[ti.RT_ROUTETYPE];
					if (z === "a-b" || z.indexOf("a-b_") === 0)p[ti.RT_ROUTENAME] !== y.name && (y = null);
					y && y.dirType.indexOf("_") < 0 && (y.weekdays += p[ti.RT_WEEKDAYS])
				}
				if (y && s !== "1") {
					++i;
					continue
				}
			}
			var A = (p[ti.RT_VALIDITYPERIODS] || "").split(",");
			for (var B = 0; B < 7; ++B)A[B] = parseInt(A[B], 10) || 0, B > 0 && (A[B] += A[B - 1]);
			var C = "Z";
			typeof cfg === "object" && cfg.transportOrder && (C = cfg.transportOrder[r] || "Z"), C += ("000000" + parseInt(t, 10)).slice(-6) + ("000000" + parseInt(t.substr(1), 10)).slice(-6) + (t + "00000000000000000000").substr(0, 20) + ("000000" + s).slice(-6), g.push({
				id: p[0],
				authority: p[ti.RT_AUTHORITY],
				city: q,
				transport: r,
				operator: p[ti.RT_OPERATOR],
				commercial: p[ti.RT_COMMERCIAL],
				num: p[ti.RT_ROUTENUM],
				numHTML: u,
				name: p[ti.RT_ROUTENAME],
				routeTag: v,
				dirType: p[ti.RT_ROUTETYPE],
				weekdays: p[ti.RT_WEEKDAYS],
				validityPeriods: A,
				specialDates: p[ti.RT_SPECIALDATES],
				entry: p[ti.RT_ENTRY],
				streets: p[ti.RT_STREETS],
				platforms: p[ti.RT_ROUTESTOPSPLATFORMS],
				stops: p.slice(ti.RT_ROUTESTOPS),
				times: l.times,
				order: s,
				sortKey: C,
				raw_data: l.raw_data
			}), n && (h[n] = g[g.length - 1])
		}
		++i
	}
	if (!j)return g[0];
	g.sort(function (a, b) {
		if (a.sortKey < b.sortKey)return -1;
		if (a.sortKey > b.sortKey)return 1;
		return ti.naturalSort(a.num, b.num) || (a.order < b.order ? -1 : a.order > b.order ? 1 : 0)
	});
	return g
}, ti.fOperatorDetails = function (a, b) {
	var c = cfg.operators[a || b];
	if (!c)return a;
	c = b && c[b] || c;
	return c[pg.language] || c.en || c
}, ti.encodeNumber = function (a) {
	a = a << 1, a < 0 && (a = ~a);
	var b = "";
	while (a >= 32)b += String.fromCharCode((32 | a & 31) + 63), a >>= 5;
	b += String.fromCharCode(a + 63);
	return b
}, ti.explodeTimes = function (a) {
	var b = [], c = [], d = [], e = [], f = [], g = [], h = [], i = [], j, k, l = a.split(","), m, n, o = l.length, p = [], q = "+", r = "-";
	for (m = -1, j = 0, k = 0, n = 0; ++m < o;) {
		var s = l[m];
		if (s == "")break;
		var t = s.charAt(0);
		t === q ? p[m] = s.charAt(1) === "0" && s !== "+0" ? "2" : "1" : t === r && s.charAt(1) === "0" && (p[m] = s.charAt(2) === "0" ? "2" : "1"), n += +s, b[j++] = n
	}
	for (var u = p.length; --u >= 0;)p[u] || (p[u] = "0");
	for (var u = 0; ++m < o;) {
		var v = +l[m], w = l[++m];
		w === "" ? (w = j - u, o = 0) : w = +w;
		while (w-- > 0)d[u++] = v
	}
	--m;
	for (var u = 0, o = l.length; ++m < o;) {
		var v = +l[m], w = l[++m];
		w === "" ? (w = j - u, o = 0) : w = +w;
		while (w-- > 0)e[u++] = v
	}
	--m;
	for (var u = 0, o = l.length; ++m < o;) {
		var x = l[m], w = l[++m];
		w === "" ? (w = j - u, o = 0) : w = +w;
		while (w-- > 0)c[u++] = x
	}
	if (ti.has_trips_ids) {
		--m;
		var o = l.length;
		for (var u = 0; ++m < o;) {
			if (l[m] === "")break;
			f[u] = +l[m], u > 0 && (f[u] += f[u - 1]), ++u
		}
		for (var u = 0; ++m < o;) {
			if (l[m] === "")break;
			g[u] = l[m], ++u
		}
		if (ti.has_trips_ids === 2) {
			for (var u = 0; ++m < o;) {
				if (l[m] === "")break;
				i[u] = l[m], ++u
			}
			for (var u = 0; ++m < o;) {
				if (l[m] === "")break;
				h[u] = l[m], ++u
			}
		}
		++m
	}
	--m, k = 1;
	for (var u = j, y = j, z = 5, o = l.length; ++m < o;) {
		z += +l[m] - 5;
		var w = l[++m];
		w !== "" ? (w = +w, y -= w) : (w = y, y = 0);
		while (w-- > 0)b[u] = z + b[u - j], ++u;
		y <= 0 && (y = j, z = 5, ++k)
	}
	final_data = {
		workdays: c,
		times: b,
		tag: p.join(""),
		valid_from: d,
		valid_to: e,
		trip_ids: f,
		trip_codes: g,
		trip_operators: h,
		trip_groups: i
	};
	return final_data
}, ti.fGetDirTag = function (a) {
	if (a.indexOf("-d") >= 0)return "0";
	if (a.indexOf("2") >= 0)return "2";
	if (a.indexOf("3") >= 0)return "3";
	var b = a.search(/[\dcefghijklmnopqrstuvwyz]/);
	if (b > 0) {
		var c = a.indexOf("_");
		if (c < 0 || c > b)return "1"
	}
	return ""
}, ti.parseParams = function (a) {
	var b = {};
	if (!a) {
		b.status = "UNDEFINED";
		return b
	}
	a.origin || (b.status = "NO_ORIGIN"), a.destination || (b.status += (b.status ? "," : "") + "NO_DESTINATION");
	var c, d, e = 1;
	if (a.departure_time || a.arrival_time) {
		var f = parseInt(a.departure_time || a.arrival_time, 10);
		ti.TimeZoneOffset && (f = f + (new Date).getTimezoneOffset() * 60 + ti.TimeZoneOffset * 3600), f = new Date(f * 1e3), c = new Date(f.getFullYear(), f.getMonth(), f.getDate()), d = f.getHours() * 60 + f.getMinutes(), e = a.departure_time ? 1 : -1
	}
	var g = {};
	if (a.transport) {
		var h = "," + a.transport + ",";
		g = {
			train: h.indexOf("train") >= 0,
			tram: h.indexOf("tram") >= 0 || h.indexOf("city") >= 0,
			trol: h.indexOf("trol") >= 0 || h.indexOf("city") >= 0,
			bus: h.indexOf("city") >= 0
		}, g.regionalbus = g.internationalbus = h.indexOf("bus") >= 0
	}
	b = {
		status: b.status || "OK",
		start_stops: a.origin,
		finish_stops: a.destination,
		reverse: e,
		date: c,
		start_time: d,
		walk_max: parseInt(a.walk_max || ti.walk_max || 1e3, 10),
		lowFloor: !1,
		transport: g,
		route_nums: "",
		walk_speed_kmh: parseInt(a.walk_speed || 4, 10),
		change_time: parseInt(a.change_time || 3, 10),
		operators: a.operators,
		added_trips: a.added_trips,
		removed_trips: a.removed_trips,
		max_changes: a.max_changes,
		route_nums: a.route_nums
	};
	return b
}, ti.printParameters = function (a) {
	if (!a.date) {
		var b = new Date;
		a.date = new Date(b.getFullYear(), b.getMonth(), b.getDate())
	}
	var c = {
		origin: a.start_stops,
		destination: a.finish_stops,
		walk_max: a.walk_max / 1e3 + " km",
		walk_speed: a.walk_speed_kmh + " km/h",
		change_time: a.change_time + " minutes",
		max_changes: a.max_changes,
		time_zone_offset: ti.TimeZoneOffset,
		operators: a.operators,
		added_trips: a.added_trips,
		removed_trips: a.removed_trips
	};
	a.date && (c.date = a.date.yyyymmdd(".")), a.reverse == 1 && (c.departure_time = ti.printTime(a.start_time)), a.reverse == -1 && (c.arrival_time = ti.printTime(a.start_time));
	if (a.transport) {
		var d = "";
		for (var e in a.transport)a.transport[e] && (d += ", " + e);
		d && (c.transport = d.substring(2))
	}
	return c
}, ti.toUnixTime = function (a, b) {
	var c = +a / 1e3;
	c = c - (new Date).getTimezoneOffset() * 60 - ti.TimeZoneOffset * 3600;
	return c + (b || 0) * 60
}, Date.prototype.yyyymmdd = function (a) {
	var b = this.getFullYear().toString(), c = (this.getMonth() + 1).toString(), d = this.getDate().toString();
	a || (a = "");
	return b + a + (c[1] ? c : "0" + c[0]) + a + (d[1] ? d : "0" + d[0])
}, ti.ToGoogleFormat = function (a) {
	if (!a || !a.status) {
		var b = {status: "UNDEFINED", parsed_parameters: {}, routes: []};
		return b
	}
	if (a.status != "OK") {
		var b = {status: a.status, parsed_parameters: ti.printParameters(a), routes: []};
		return b
	}
	var c = a.results, d = a.timeStarted ? +(new Date) - a.timeStarted : 0, b = {
		parsed_parameters: ti.printParameters(a),
		status: c.length ? "OK" : "ZERO_RESULTS",
		calculation_time: d + " ms",
		summary: undefined,
		routes: []
	};
	for (var e = 0; e < c.length; e++) {
		var f = c[e], g = c[e].legs, h = {
			bounds: {
				northeast: {lat: undefined, lng: undefined},
				southwest: {lat: undefined, lng: undefined}
			},
			overview_polyline: {points: undefined},
			legs: [],
			warnings: [],
			copyrights: undefined,
			waypoint_order: undefined
		};
		h.bounds = undefined, h.overview_polyline = undefined, h.warnings = undefined;
		var i = {
			duration: {value: undefined, text: undefined},
			walking_duration: {value: undefined, text: undefined},
			distance: {value: undefined, text: undefined},
			departure_time: {value: undefined, text: undefined, time_zone: undefined},
			arrival_time: {value: undefined, text: undefined, time_zone: undefined},
			start_location: {lat: undefined, lng: undefined},
			end_location: {lat: undefined, lng: undefined},
			start_address: undefined,
			end_address: undefined,
			steps: []
		};
		i.departure_time.value = ti.toUnixTime(a.date, f.start_time), i.departure_time.text = ti.printTime(f.start_time), i.departure_time.time_zone = ti.TimeZone, i.arrival_time.value = ti.toUnixTime(a.date, f.finish_time), i.arrival_time.text = ti.printTime(f.finish_time), i.arrival_time.time_zone = ti.TimeZone, i.duration.value = f.travel_time * 60, i.duration.text = f.travel_time + " min", i.walking_duration.value = f.walk_time * 60, i.walking_duration.text = f.walk_time + " min", i.distance = undefined;
		if (f.legs && f.legs[0]) {
			var j = g[0];
			i.start_location.lat = j.start_stop.lat, i.start_location.lng = j.start_stop.lng
		}
		if (f.legs && g.length && f.legs[g.length - 1]) {
			var k = g[g.length - 1];
			i.end_location.lat = k.finish_stop.lat, i.end_location.lng = k.finish_stop.lng
		}
		h.legs.push(i);
		for (var l = 0; l < g.length; l++) {
			var m = g[l], n = {
				travel_mode: undefined,
				duration: {value: undefined, text: undefined},
				distance: {value: undefined, text: undefined},
				start_location: {lat: undefined, lng: undefined},
				end_location: {lat: undefined, lng: undefined},
				polyline: {points: undefined},
				html_instructions: undefined,
				transit_details: {
					departure_time: {value: undefined, text: undefined, time_zone: undefined},
					arrival_time: {value: undefined, text: undefined, time_zone: undefined},
					weekdays: undefined,
					departure_stop: {
						name: undefined,
						location: {lat: undefined, lng: undefined},
						street: undefined,
						id: undefined,
						platform: undefined
					},
					arrival_stop: {
						name: undefined,
						location: {lat: undefined, lng: undefined},
						street: undefined,
						id: undefined,
						platform: undefined
					},
					headsign: undefined,
					num_stops: undefined,
					line: {
						name: undefined,
						short_name: undefined,
						vehicle: {name: undefined, type: undefined, icon: undefined},
						trip_id: undefined,
						trip_date: undefined,
						trip_num: undefined,
						trip_operator: undefined,
						trip_group: undefined,
						agencies: [],
						weekdays: undefined,
						url: undefined,
						color: undefined,
						icon: undefined
					}
				}
			};
			n.travel_mode = m.route ? "TRANSIT" : "WALKING", n.distance = undefined, n.polyline = undefined, n.start_location.lat = m.start_stop.lat, n.start_location.lng = m.start_stop.lng, n.end_location.lat = m.finish_stop.lat, n.end_location.lng = m.finish_stop.lng, n.duration.value = (m.finish_time - m.start_time) * 60, n.duration.text = m.finish_time - m.start_time + " min", n.transit_details.departure_time.value = ti.toUnixTime(a.date, m.start_time), n.transit_details.departure_time.text = ti.printTime(m.start_time), n.transit_details.departure_time.time_zone = ti.TimeZone, n.transit_details.arrival_time.value = ti.toUnixTime(a.date, m.finish_time), n.transit_details.arrival_time.text = ti.printTime(m.finish_time), n.transit_details.arrival_time.time_zone = ti.TimeZone, n.transit_details.departure_stop.location.lat = m.start_stop.lat, n.transit_details.departure_stop.location.lng = m.start_stop.lng, m.start_stop.id.indexOf(";") < 0 && (n.transit_details.departure_stop.name = m.start_stop.name, n.transit_details.departure_stop.id = m.start_stop.id, n.transit_details.departure_stop.street = m.start_stop.street, n.transit_details.departure_stop.platform = m.start_platform), n.transit_details.arrival_stop.location.lat = m.finish_stop.lat, n.transit_details.arrival_stop.location.lng = m.finish_stop.lng, m.finish_stop.id.indexOf(";") < 0 && (n.transit_details.arrival_stop.name = m.finish_stop.name, n.transit_details.arrival_stop.id = m.finish_stop.id, n.transit_details.arrival_stop.street = m.finish_stop.street, n.transit_details.arrival_stop.platform = m.finish_platform);
			if (m.route) {
				n.transit_details.headsign = m.route.name, n.transit_details.num_stops = m.finish_pos - m.start_pos, n.transit_details.line.name = m.route.name, n.transit_details.line.short_name = m.route.num, n.transit_details.line.vehicle.name = m.route.transport, n.transit_details.line.vehicle.type = ({
					bus: "LOCALBUS",
					regionalbus: "BUS",
					internationalbus: "BUS",
					trol: "TROLLEYBUS",
					train: "RAIL",
					tram: "TRAM"
				})[m.route.transport.toLowerCase()] || "OTHER", n.transit_details.line.trip_id = m.trip_id, n.transit_details.line.trip_date = m.trip_date && m.trip_date.yyyymmdd("."), n.transit_details.line.trip_num = m.trip_code, n.transit_details.line.trip_operator = m.trip_operator, n.transit_details.line.trip_group = m.trip_group, n.transit_details.weekdays = m.weekdays, n.transit_details.line.weekdays = m.route.weekdays, n.transit_details.line.track_id = ti.toAscii([m.route.city, m.route.transport, m.route.num, m.route.dirType].join("_"));
				if (m.route.operator) {
					var o = {name: undefined, phone: undefined, url: undefined};
					o.name = m.route.operator, n.transit_details.line.agencies.push(o)
				}
				m.online_data && (n.transit_details.online_data = m.online_data)
			} else n.transit_details.line = undefined;
			n.taxi = m.taxi, h.legs[0].steps.push(n)
		}
		b.routes.push(h)
	}
	return b
};
function SHA1(a) {
	function b(a, b) {
		var c = a << b | a >>> 32 - b;
		return c
	}

	function c(a) {
		var b = "", c, d, e;
		for (c = 0; c <= 6; c += 2)d = a >>> c * 4 + 4 & 15, e = a >>> c * 4 & 15, b += d.toString(16) + e.toString(16);
		return b
	}

	function d(a) {
		var b = "", c, d;
		for (c = 7; c >= 0; c--)d = a >>> c * 4 & 15, b += d.toString(16);
		return b
	}

	function e(a) {
		a = a.replace(/\r\n/g, "\n");
		var b = "";
		for (var c = 0; c < a.length; c++) {
			var d = a.charCodeAt(c);
			d < 128 ? b += String.fromCharCode(d) : d > 127 && d < 2048 ? (b += String.fromCharCode(d >> 6 | 192), b += String.fromCharCode(d & 63 | 128)) : (b += String.fromCharCode(d >> 12 | 224), b += String.fromCharCode(d >> 6 & 63 | 128), b += String.fromCharCode(d & 63 | 128))
		}
		return b
	}

	var f, g, h, i = new Array(80), j = 1732584193, k = 4023233417, l = 2562383102, m = 271733878, n = 3285377520, o, p, q, r, s, t;
	a = e(a);
	var u = a.length, v = new Array;
	for (g = 0; g < u - 3; g += 4)h = a.charCodeAt(g) << 24 | a.charCodeAt(g + 1) << 16 | a.charCodeAt(g + 2) << 8 | a.charCodeAt(g + 3), v.push(h);
	switch (u % 4) {
		case 0:
			g = 2147483648;
			break;
		case 1:
			g = a.charCodeAt(u - 1) << 24 | 8388608;
			break;
		case 2:
			g = a.charCodeAt(u - 2) << 24 | a.charCodeAt(u - 1) << 16 | 32768;
			break;
		case 3:
			g = a.charCodeAt(u - 3) << 24 | a.charCodeAt(u - 2) << 16 | a.charCodeAt(u - 1) << 8 | 128
	}
	v.push(g);
	while (v.length % 16 != 14)v.push(0);
	v.push(u >>> 29), v.push(u << 3 & 4294967295);
	for (f = 0; f < v.length; f += 16) {
		for (g = 0; g < 16; g++)i[g] = v[f + g];
		for (g = 16; g <= 79; g++)i[g] = b(i[g - 3] ^ i[g - 8] ^ i[g - 14] ^ i[g - 16], 1);
		o = j, p = k, q = l, r = m, s = n;
		for (g = 0; g <= 19; g++)t = b(o, 5) + (p & q | ~p & r) + s + i[g] + 1518500249 & 4294967295, s = r, r = q, q = b(p, 30), p = o, o = t;
		for (g = 20; g <= 39; g++)t = b(o, 5) + (p ^ q ^ r) + s + i[g] + 1859775393 & 4294967295, s = r, r = q, q = b(p, 30), p = o, o = t;
		for (g = 40; g <= 59; g++)t = b(o, 5) + (p & q | p & r | q & r) + s + i[g] + 2400959708 & 4294967295, s = r, r = q, q = b(p, 30), p = o, o = t;
		for (g = 60; g <= 79; g++)t = b(o, 5) + (p ^ q ^ r) + s + i[g] + 3395469782 & 4294967295, s = r, r = q, q = b(p, 30), p = o, o = t;
		j = j + o & 4294967295, k = k + p & 4294967295, l = l + q & 4294967295, m = m + r & 4294967295, n = n + s & 4294967295
	}
	var t = d(j) + d(k) + d(l) + d(m) + d(n);
	return t.toLowerCase()
}
var Hash = function () {
	var a = this, b = document.documentMode, c = a.history, d = a.location, e, f, g, h = function () {
		var a = d.href.indexOf("#");
		return a == -1 ? "" : decodeURI(d.href.substr(a + 1))
	}, i = function () {
		var a = h();
		a != f && (f = a, e(a, !1), pg.timeOfActivity = (new Date).getTime())
	}, j = function (a) {
		try {
			var b = g.contentWindow.document;
			b.open(), b.write("<html><body>" + a + "</body></html>"), b.close(), f = a
		} catch (c) {
			setTimeout(function () {
				j(a)
			}, 10)
		}
	}, k = function () {
		try {
			g.contentWindow.document
		} catch (a) {
			setTimeout(k, 10);
			return
		}
		j(f);
		var b = f;
		setInterval(function () {
			var a, c;
			try {
				a = g.contentWindow.document.body.innerText, a != b ? (b = a, d.hash = f = a, e(a, !0)) : (c = h(), c != f && j(c))
			} catch (i) {
			}
		}, 50)
	};
	return {
		getHash: h, init: function (d, j) {
			e || (e = d, f = h(), d(f, !0), a.ActiveXObject ? !b || b < 8 ? (g = j, k()) : a.attachEvent("onhashchange", i) : (c.navigationMode && (c.navigationMode = "compatible"), setInterval(i, 50)))
		}, go: function (a) {
			if (a != f) {
				if (top !== self && (typeof cfg != "object" || cfg.defaultCity != "jelgava")) {
					top.location.replace(self.location.href.split("#")[0] + "#" + a);
					return
				}
				g ? j(a) : (d.hash = f = a, e(a, !1))
			}
		}
	}
}();
function pikasRoute(a, b) {
	a = a || {origin: "3540", destination: "54.68561;25.28670", departure_time: "1355295600", walk_max: "1000"};
	var c = ti.parseParams(a);
	c.callback = function (a) {
		if (b === "JSON" || b === "json")document.body.innerHTML = JSON.stringify(ti.ToGoogleFormat(a), null, 4); else if (typeof b === "string") {
			var c = "";
			a.timeStarted && (c += "Search took " + (+(new Date) - a.timeStarted) + "ms<br /><br />"), c += "Optimal routes:";
			var d = a.results || [];
			for (var e = 0; e < d.length; e++) {
				var f = d[e], g = d[e].legs;
				c += ["<br />Option", e + 1, "travel time: " + ti.printTime(f.travel_time), "<br />"].join("&nbsp;");
				for (var h = 0; h < g.length; h++) {
					var i = g[h];
					c += [i.start_stop.name, ti.printTime(i.start_time), ti.printTime(i.finish_time), i.finish_stop.name, " "].join(" "), i.route ? c += [i.route.transport, i.route.num, i.route.name, i.weekdays, "<br />"].join(" ") : c += "walk<br />"
				}
			}
			document.body.innerHTML = c
		} else typeof b === "function" ? b(ti.ToGoogleFormat(a)) : window.JSClassObject.receiveResult(JSON.stringify(ti.ToGoogleFormat(a), null, 4))
	}, ti.findTrips(c)
}
ti.findTrips = function (a) {
	if (a && a.callback) {
		if (a.status && a.status != "OK") {
			a.callback(a);
			return
		}
		a.no_just_walking = !1, a.reverseOriginal = a.reverse;
		if (!a.attempt) {
			if (typeof pg === "object") {
				if (pg.optimalSearchRunning)return;
				pg.optimalSearchRunning = !0
			}
			a.timeStarted = +(new Date), ti.timeStarted = +(new Date), a.attempt = 1, a.direct_routes = [];
			var b = a.date;
			b || (b = new Date, b = new Date(b.getFullYear(), b.getMonth(), b.getDate()), a.date = b), a.transport || (a.transport = {}), a.transportOriginal = ti.cloneObject(a.transport), typeof a.reverse == "undefined" && (a.reverse = 1, a.reverseOriginal = a.reverse);
			if (typeof cfg === "object" && cfg.defaultCity === "latvia")a.transport.internationalbus = !1; else if (a.transport.bus || a.transport.trol || a.transport.tram)a.transport.regionalbus && (a.transport.regionalbus = !1, a.attempt = -1), a.transport.commercialbus && (a.transport.commercialbus = !1, a.attempt = -1), a.transport.train && (a.transport.train = !1, a.attempt = -1);
			dijkstra(a, a.start_time, a.reverse);
			return
		}
		if (a.attempt == -1) {
			a.attempt = 1;
			if (a.results.length <= 0) {
				a.transport = a.transportOriginal, dijkstra(a, a.start_time, a.reverse);
				return
			}
		}
		if (a.attempt == 1 && a.results.length <= 0) {
			a.attempt = 2, a.reverse = -a.reverse, a.sort = "no sort";
			if (typeof cfg !== "object" || cfg.defaultCity !== "intercity") {
				dijkstra(a, a.reverse == 1 ? 0 : 4320, a.reverse);
				return
			}
		}
		if (a.attempt == 2 && a.results.length > 0) {
			a.attempt = 999, a.reverse = -a.reverse;
			var c;
			for (var d = 0; d < a.results.length; d++)a.reverse == 1 && (d == 0 || c < a.results[d].start_time) && (c = a.results[d].start_time), a.reverse == -1 && (d == 0 || c > a.results[d].finish_time) && (c = a.results[d].finish_time);
			dijkstra(a, c, a.reverse);
			return
		}
		if (a.attempt == 1) {
			var c = null;
			for (var d = 0; d < a.results.length; d++) {
				if (a.results[d].code == "W")continue;
				a.reverse == 1 && (!c || c > a.results[d].finish_time) && (c = a.results[d].finish_time), a.reverse == -1 && (!c || c < a.results[d].start_time) && (c = a.results[d].start_time)
			}
			a.results0 = ti.filterSearchResults(a.results, a.reverse), a.results = a.results0.slice(0, 1), a.results = ti.finalizeSearchResults(a), a.callback1 && a.callback1(a), a.attempt = 3, a.no_just_walking = !0;
			if (c) {
				dijkstra(a, c, -a.reverse, a.start_time);
				return
			}
			a.results = []
		}
		if (a.attempt >= 3 && a.attempt <= 5) {
			a.results.push.apply(a.results, a.results0), a.results = ti.filterSearchResults(a.results, a.reverse);
			if (a.results.length > 0)if (!0 || a.results.length == 1 || a.results0.length >= a.results.length)if (a.results[0].legs.length != 1 || a.results[0].legs[0].route) {
				a.attempt = 6, a.results0 = a.results, a.no_just_walking = !0, dijkstra(a, a.reverse == 1 ? a.results[0].start_time + 1 : a.results[0].finish_time - 1, a.reverse);
				return
			}
		}
		a.attempt == 6 && a.results.push.apply(a.results, a.results0), a.results = ti.filterSearchResults(a.results, a.reverse);
		if (typeof cfg == "object" && cfg.defaultCity === "disable_temporary_latvia" && a.attempt <= 999) {
			typeof pg === "object" && (pg.optimalSearchRunning = !1, ($("online_results") || {}).innerHTML = ""), a.attempt = 1e3;
			var e = {};
			a.online_results = [], a.online_results_required_count = 0;
			for (var d = 0; d < a.results.length; d++) {
				var f = a.results[d], g = f.legs;
				for (var h = 0; h < g.length; h++) {
					var i = g[h];
					if (i.route) {
						var j = i.start_stop && ti.fGetStopDetails(i.start_stop.id), k = i.finish_stop && ti.fGetStopDetails(i.finish_stop.id);
						j && j.info && k && k.info && !e[j.info + ";separator;" + k.info] && (e[j.info + ";separator;" + k.info] = [j.info, k.info], ++a.online_results_required_count)
					}
				}
			}
			for (var d in e) {
				var j = e[d][0], k = e[d][1], l = "timetable" + a.date.yyyymmdd() + j + k;
				l += "7xk$n1Lp1*9E!3", l = SHA1(l);
				var m = "/api/ltc.php?action=timetable";
				m += "&date=" + a.date.yyyymmdd(), m += "&from=" + j, m += "&to=" + k, m += "&signature=" + l, ti.SERVER === !0 ? typeof http != "undefined" && http.get({
					host: "bezrindas.lv",
					port: 80,
					path: m
				}, function (b) {
					b.setEncoding("utf8");
					var c = "";
					b.on("data", function (a) {
						c += a
					}), b.on("end", function () {
						if (c) {
							var b = JSON.parse(c);
							b.contents && (b = b.contents), b && b.length && a.online_results.push.apply(a.online_results, [].concat(b)), --a.online_results_required_count == 0 && ti.findTrips(a)
						}
					})
				}) : (m = "http://bezrindas.lv" + m, a.online_query_url = m, m = "http://www.stops.lt/latviatest/proxy.php?url=" + encodeURIComponent(m), ($("online_results") || {}).innerHTML = "<br/>Waiting data from bezrindas.lv for stops pairs: " + a.online_results_required_count, ti.fDownloadUrl("get", m, function (b) {
					if (b) {
						a.online_results_JSON = b;
						var c = JSON.parse(b);
						c.contents && (c = c.contents), c && c.length && a.online_results.push.apply(a.online_results, [].concat(c)), ($("online_results") || {}).innerHTML = "<br/>Waiting data from bezrindas.lv for stops pairs: " + (a.online_results_required_count - 1), --a.online_results_required_count == 0 && ti.findTrips(a)
					}
				}, !0))
			}
			if (a.online_results_required_count > 0)return
		}
		a.results = ti.finalizeSearchResults(a, a.online_results), typeof pg === "object" && (pg.optimalSearchRunning = !1);
		if (a.callback)a.callback(a, !0); else {
			if (typeof pg === "object")return a;
			if (typeof window == "object")document.body.innerHTML = JSON.stringify(a.results); else return a
		}
	}
};
function dijkstra(a, b, c, d) {
	typeof cfg == "object" && cfg.defaultLanguage == "lt" && (ti.specialWeekdays[ti.dateToDays(new Date(2014, 1, 16))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 2, 11))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 3, 21))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 4, 1))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 5, 24))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 6, 6))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 7, 15))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 10, 1))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 24))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 25))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 26))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 0, 1))] = 7), typeof cfg == "object" && cfg.defaultLanguage == "ee" && (ti.specialWeekdays[ti.dateToDays(new Date(2014, 4, 1))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 5, 8))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 5, 23))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 5, 24))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 7, 20))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 24))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 25))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2014, 11, 26))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 0, 1))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 1, 24))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 3, 3))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 3, 5))] = 7, ti.specialWeekdays[ti.dateToDays(new Date(2015, 4, 1))] = 7);
	var e = "", f = "a";
	typeof cfg == "object" && cfg.defaultCity == "latvia" && (e = ";bus;trol;tram;minibus;", f = "nothing");
	var g = !1, h = c == -1 ? a.finish_stops.split(",") : a.start_stops.split(","), i = c == -1 ? a.start_stops.split(",") : a.finish_stops.split(","), j = a.finish_stops === "0;0";
	c || (g = !0, c = 1, a.direct_routes = []), a.results = [], b = typeof b != "undefined" ? b * c : 0, d = typeof d != "undefined" ? d * c : 7200;
	var k = b, l = c == 1 ? "1" : "2", m = c == 1 ? "2" : "1", n = a.route_nums ? "," + a.route_nums.toLowerCase().replace(/\s/g, "") + "," : "", o = a.lowFloor;
	n.indexOf(",z,") >= 0 && (o = !0, n = n.replace(/,z,/g, "")), a.date || (a.date = new Date);
	var p = ti.dateToDays(a.date), q = "" + (ti.specialWeekdays[p] || a.date.getDay() || 7), r = a.max_changes || Number.POSITIVE_INFINITY, s = a.change_time || 3, t = a.walk_speed_kmh || 4, u = a.walk_max || 500;
	u = g ? .05 : u / 1e3, u = u * u;
	var v = ti.stops, w = ti.routes, x = ti.specialDates, y = a.direct_routes || [], z = a.transport, A = a.operators, B = a.removed_trips ? "," + a.removed_trips.replace(/\s/g, "") + "," : "", C = a.added_trips ? "," + a.added_trips.replace(/\s/g, "") + "," : "", D = a.commercial, E = a.routetypes, F = E != 1, G = a.area, H = 0, I = a.middle_stops;
	if (I) {
		H = 10;
		for (var J in I) {
			var K = v[J].routes;
			for (var L = 0; L < K.length; L += 2)w[K[L]].available = 10
		}
	}
	var M, N, O = {}, P = {}, Q = {};
	for (var R = 1, S = h; R <= 2; ++R) {
		for (var L = S.length; --L >= 0;)if (S[L].charAt(0) == f) {
			var T = v[S[L]];
			if (T)for (var U = T.neighbours.length; --U >= 0;)S.push(T.neighbours[U]);
			S[L] = "removed stop"
		} else if (S[L].indexOf(";") > 0) {
			var V = S[L].split(";");
			R == 1 ? M = {id: S[L], lat: parseFloat(V[0]), lng: parseFloat(V[1]), neighbours: []} : (N = {
				id: S[L],
				lat: parseFloat(V[0]),
				lng: parseFloat(V[1])
			}, P[N.id] = !0, M && (Q[M.id] = !0))
		}
		S = i
	}
	var W = [], X = {};
	X[k] = [];
	for (var J in v) {
		var T = v[J];
		T.time = Number.POSITIVE_INFINITY, T.trip_date = null, j && (T.rideStart = T.rideEnd = 0);
		if (!T.lat || !T.lng)continue;
		if (M) {
			var Y = (M.lng - T.lng) * 58.1, Z = (M.lat - T.lat) * 111.2, $ = Y * Y + Z * Z;
			$ <= u && (M.neighbours.push(T.id), cfg.defaultCity == "latvia" && T.city && T.city.toLowerCase().indexOf("latvija") < 0 && (z.internationalbus = !0))
		}
		if (N) {
			var Y = (N.lng - T.lng) * 58.1, Z = (N.lat - T.lat) * 111.2, $ = Y * Y + Z * Z;
			if ($ <= u) {
				Q[T.id] = !0, cfg.defaultCity == "latvia" && T.city && T.city.toLowerCase().indexOf("latvija") < 0 && (z.internationalbus = !0);
				var _ = T.neighbours;
				for (var U = _.length; --U >= 0;)T.name === v[_[U]].name && (Q[_[U]] = !0)
			}
		}
	}
	for (var U = h.length; --U >= -1;) {
		var T = U >= 0 ? v[h[U]] : M;
		T && (T.prev_stop = !1, T.route = null, T.changes = 0, O[T.id] = !0, U == -1 && c == -1 && s ? (k -= s, X[k] = [M]) : X[k].push(T), T.time = k, typeof cfg == "object" && cfg.defaultCity == "latvia" && T.city && T.city.toLowerCase().indexOf("latvija") < 0 && (z.internationalbus = !0))
	}
	for (var U = i.length; --U >= 0;) {
		var J = i[U], T = v[J];
		T && (P[J] = !0, typeof cfg == "object" && cfg.defaultCity == "latvia" && T.city && T.city.toLowerCase().indexOf("latvija") < 0 && (z.internationalbus = !0))
	}
	if (!0 || g)for (var ba = w.length; --ba >= 0;) {
		var bb = ti.fGetRoutes(ba), bc = w[ba];
		bc.available = z && z[bb.transport] === !1 || H && H !== bc.available || n && n.indexOf("," + bb.num.toLowerCase() + ",") < 0 || D && D != bb.commercial || E && F != !_transport_data[bb.transport].region || G && G != bb.cities[0] ? 0 : 1
	}
	for (var U = y.length; --U >= 0;)y[U].available = 0;
	for (var ba in w) {
		var bc = w[ba];
		bc.trip_start_time = Number.POSITIVE_INFINITY
	}
	a.finish_stops || (i = !1);
	var bd = +(new Date), be, bf = 0, bg = function () {
		for (var b = 0; ;) {
			for (var f; !(f = X[k]) || !f.length;)if (++k > d) {
				if (!W.length) {
					a.results = [];
					if (g)return [];
					a.callback2 ? window.setTimeout(a.callback2, 10) : typeof window === "object" ? window.setTimeout(function () {
						ti.findTrips(a)
					}, 10) : ti.findTrips(a);
					return
				}
				f = !1;
				break
			}
			if (!f)break;
			f = f.pop();
			if (f.time < k || f.changes < 0)continue;
			if (++b == 3e3 && !g && typeof window == "object") {
				+(new Date) - bd > 3e4 ? (a.results = [], window.setTimeout(function () {
					ti.findTrips(a)
				}, 10)) : window.setTimeout(bg, 100);
				return
			}
			if (P[f.id]) {
				d > k + 60 && (d = k + 60);
				continue
			}
			var h = f.changes || 0, n = null, y = null;
			if (!g) {
				var z = f;
				while (!z.route && z.prev_stop)z = z.prev_stop;
				var D = z.lat, E = z.lng, F = f.neighbours;
				for (var G = F.length; --G >= -1;) {
					if (typeof cfg === "object" && cfg.defaultCity === "kaunas") {
						if (({
								805: !0,
								786: !0,
								787: !0,
								"787a": !0,
								397: !0,
								398: !0,
								403: !0,
								404: !0,
								405: !0,
								641: !0
							})[f.id] && ({
								196: !0,
								195: !0,
								664: !0,
								201: !0,
								202: !0,
								203: !0,
								204: !0,
								207: !0,
								208: !0,
								209: !0,
								210: !0
							})[F[G]])continue;
						if (({
								805: !0,
								786: !0,
								787: !0,
								"787a": !0,
								397: !0,
								398: !0,
								403: !0,
								404: !0,
								405: !0,
								641: !0
							})[F[G]] && ({
								196: !0,
								195: !0,
								664: !0,
								201: !0,
								202: !0,
								203: !0,
								204: !0,
								207: !0,
								208: !0,
								209: !0,
								210: !0
							})[f.id])continue
					}
					if (typeof cfg === "object" && cfg.defaultCity.indexOf("viln") == 0) {
						if (({
								"0906": !0,
								"0905": !0,
								"0904": !0,
								"0903": !0,
								"0902": !0,
								"0901": !0,
								2309: !0,
								2308: !0,
								2310: !0,
								2311: !0,
								2316: !0,
								2317: !0
							})[f.id] && ({
								1103: !0,
								1104: !0,
								1107: !0,
								1110: !0,
								1113: !0,
								1114: !0,
								1115: !0,
								1116: !0,
								2314: !0,
								2315: !0,
								2318: !0,
								2319: !0,
								2320: !0
							})[F[G]])continue;
						if (({
								"0906": !0,
								"0905": !0,
								"0904": !0,
								"0903": !0,
								"0902": !0,
								"0901": !0,
								2309: !0,
								2308: !0,
								2310: !0,
								2311: !0,
								2316: !0,
								2317: !0
							})[F[G]] && ({
								1103: !0,
								1104: !0,
								1107: !0,
								1110: !0,
								1113: !0,
								1114: !0,
								1115: !0,
								1116: !0,
								2314: !0,
								2315: !0,
								2318: !0,
								2319: !0,
								2320: !0
							})[f.id])continue
					}
					var H;
					if (G < 0)if (Q[f.id])H = N; else break; else H = v[F[G]] || {lat: 999, lng: 999};
					var J = (E - H.lng) * 58.1, K = (D - H.lat) * 111.2, L = J * J + K * K;
					if (H === z || H === f)continue;
					if (H === N) {
						if (L > u && z === M && H.id.indexOf(";") > 0)continue
					} else if (L > u && (!f.name || H.name !== f.name))continue;
					L = Math.sqrt(L);
					var R = Math.round(L / t * 60);
					R += z.time;
					if (!z.route || !z.prev_stop && c < 0)R += s;
					R < k && (R = k);
					if (R > d)continue;
					if (P[H.id])if (!1 && h === 1 && z.prev_stop && typeof cfg === "object" && cfg.defaultCity === "latvia")f = z.prev_stop, n = z.id, y = z.prev_stop_start_time; else {
						if (R > H.time)continue;
						if (R == H.time && h >= H.changes)continue;
						var S = {
							legs: [{
								start_stop: z,
								start_time: c * (z.time - (z.route ? s : 0)),
								finish_stop: H,
								finish_time: c * (R - s),
								route: null
							}]
						};
						W.push(S)
					} else {
						if (R > H.time)continue;
						if (R != H.time || h < H.changes)H.route = !1, H.prev_stop = z, H.prev_stop_start_time = z.time - (z.route ? s : 0); else continue
					}
					j && z.route && (H.rideStart = z.rideStart, H.rideEnd = z.rideEnd), H.time = R, H.changes = h;
					var T = X[R];
					T ? T[T.length] = H : X[R] = [H]
				}
			}
			h = f.changes || 0;
			if (h <= r) {
				var U = f.routes || [];
				for (var G = 0, V = U.length; G < V; G += 2) {
					var Y = w[U[G]];
					if (g) {
						if (Y.available === 0 && i)continue;
						a.direct_routes.push(Y), G + 2 < V && U[G + 2] == U[G] && (G += 2)
					} else if (!Y.available)continue;
					if (typeof Y.times === "string") {
						var Z = ti.fGetRoutes(Y.id);
						Y.times = ti.explodeTimes(Y.times), Y.city = Z.city, Y.transport = Z.transport, Y.num = Z.num, Y.stops = Z.stops, Y.platforms = Z.platforms, Y.entry = Z.entry, Y.specialDates = Z.specialDates
					}
					var $ = Y.times, _ = "";
					e && e.indexOf(Y.transport) < 0 && (f.city.indexOf("riga") < 0 ? f.city.indexOf("liep") < 0 ? f.city.indexOf("jelg") >= 0 && (_ = "jelg") : _ = "liep" : _ = "riga");
					var ba = U[G + 1], bb = Y.stops || Y.raw_data.split(";").slice(ti.RT_ROUTESTOPS);
					if (c == 1 && ba >= bb.length - 1 || c == -1 && ba == 0)continue;
					var bc;
					if ((bc = Y.entry).charAt(ba) == m)continue;
					bb[ba] == bb[ba + c] && (ba += c);
					if (!$)continue;
					var be = $.workdays, bh = $.valid_from, bi = $.valid_to, bj = $.trip_operators, bk = $.trip_groups, bl = $.trip_ids, bm = $.tag, bn = $.times;
					$ = null;
					var bo = be.length, bp = bo, bq, br, bs = 0, bt = 0;
					x = Y.specialDates;
					for (var bu = 0, bv = x.length; bu < x.length; ++bu) {
						if (!x[bu])continue;
						if (x[bu++][p]) {
							(bq = x[bu]) === "*" && (bq = q);
							break
						}
						x[bu] === "*" && (bq = "0")
					}
					var bw = y || k;
					if (f.route && Y.num === f.route.num && Y.transport === f.route.transport && Y.city === f.route.city)bw -= s; else if (h > 0 && typeof cfg == "object" && cfg.defaultCity == "latvia" && !y) {
						bw -= s;
						var bx = Y;
						c == -1 && (bx = f.route || f.prev_stop && f.prev_stop.route), bx && (bx.transport == "internationalbus" ? bw += Math.max(s, 20) : bx.transport == "train" ? bw += Math.max(s, ({
							1: 10,
							2: 15,
							3: 20
						})[bx.num.charAt(1)] || 10) : bw += Math.max(s, ({
							2: 5,
							3: 5,
							4: 10,
							5: 10,
							6: 10,
							7: 15,
							8: 20
						})[bx.num.charAt(0)] || s))
					}
					var by = bw * c;
					bw >= 1440 && (bs = bw - bw % 1440);
					var bz = !1;
					do {
						var bA = -1, bB = Number.POSITIVE_INFINITY, bC, bD, bE = null, bF = !g || !I, bG = c * bo, bH = +(new Date);
						for (var bI = bp + ba * bo; bp-- > 0;) {
							--bI;
							if (A) {
								var bJ = A[bj[bp]];
								if (typeof bJ != "string")continue;
								if (bJ != "" && ("," + bJ + ",").indexOf("," + bk[bp] + ",") < 0)continue
							}
							if (B && B.indexOf("," + bl[bp] + ",") >= 0)continue;
							bC = bD = bn[bI];
							if (bC < 0)continue;
							if (bC - by >= 1440 || bC * c - bw >= 1440)bC = by + (bC - by) % 1440;
							bC < by == (c == 1) && bC != by && (bC = (bw + 1440) * c - (by - bC) % 1440), bD = bC - bD, bC *= c;
							if (bC < bw)continue;
							bD ? (bq = ((+q + Math.floor(bD / 1440)) % 7 || 7).toString(), br = p + bD / 1440) : (bq = q, br = p);
							if ((bC < bB || g || bz) && (!q || be[bp].indexOf(bq) >= 0 || C && C.indexOf("," + bl[bp] + ",") >= 0) && (!o || bm.charAt(bp) === "1") && (!bi[bp] || bi[bp] >= br) && bh[bp] <= br) {
								if (bn[bI + bG] < 0)continue;
								bA = bI, bB = bC, bt = bD;
								if (bz || g) {
									if (!i) {
										var S = {
											route: ti.fGetRoutes(Y.id),
											start_time: bB,
											date: bE,
											trip_num: bA % bo
										};
										S.route.stopId = f.id, W.push(S), bA = -2;
										continue
									}
									break
								}
							}
						}
						bf += +(new Date) - bH;
						if (bA < 0) {
							if (bA != -2 && !i) {
								var S = {route: ti.fGetRoutes(Y.id), start_time: -1, trip_num: -1};
								S.route.stopId = f.id, W.push(S)
							}
							break
						}
						var bK, bL = c * bn[bA % bo];
						if (bs || bt)bE = new Date(a.date.valueOf()), bE.setDate(bE.getDate() + Math.floor(bt / 1440));
						g || bz || n ? bK = c === -1 ? 1 : bb.length : bL < Y.trip_start_time ? (bK = c == 1 ? bb.length : 1, Y.trip_start_time = bL, Y.pos_max = c * ba) : (bK = Y.pos_max, bK > c * ba && bL == Y.trip_start_time && (Y.pos_max = c * ba));
						var bG = c * bo;
						for (var bM = ba; c * (bM += c) < bK;) {
							bA += bG;
							if (bc.charAt(bM) == l)continue;
							var R;
							if ((R = bn[bA]) >= 0) {
								R = c * (R + bt) + s;
								if (R > d && !bz)break;
								if (R < k && !n)continue;
								var H;
								if (!(H = v[bb[bM]]))continue;
								if (_ && H.city.indexOf(_) >= 0)continue;
								var bN;
								g && !bF && (bF = H.id in I);
								if ((P[H.id] || H.id === n) && bF) {
									if (h > 0 && R > H.time)continue;
									if (h > 0 && R == H.time && h >= H.changes)continue;
									if (h == 0 && !bz && typeof cfg === "object" && cfg.defaultCity === "intercity") {
										bz = !0, bp = bo, a.direct_routes.push(Y);
										break
									}
									if (g || bz) {
										Y.available = 0;
										if (f.id.indexOf(";") < 0)for (var bO = 0; bO < bM; ++bO) {
											if (bc.charAt(bO) == l || bb[bO] == bb[bO + 1])continue;
											if (O[bb[bO]] && bn[bA + bG * (bO - bM)] >= 0) {
												f = v[bb[bO]], bB = bn[bA + bG * (bO - bM)] + bt;
												break
											}
										}
										for (var bO = bK; --bO > bM;) {
											if (bc.charAt(bO) == l || bb[bO] == bb[bO - 1])continue;
											if (P[bb[bO]] && bn[bA + bG * (bO - bM)] >= 0) {
												H = v[bb[bO]], R = bn[bA + bG * (bO - bM)] + s + bt;
												break
											}
										}
									}
									var S = {
										direct_trip: bz || g,
										legs: [{
											start_stop: f,
											start_time: c * bB,
											trip_date: bE,
											finish_stop: H,
											finish_time: c * (R - s),
											route: Y,
											trip_num: bA % bG,
											start_pos: c >= 0 ? ba : bM,
											finish_pos: c >= 0 ? bM : ba
										}]
									};
									W.push(S), bM = bK;
									if (bz && R >= H.time)break
								} else {
									if (g || bz || n)continue;
									if (R >= (bN = H.time)) {
										if (bN < k)break;
										continue
									}
									if (Y.available === 2) {
										H.time = R, H.changes = -1, H.trip_date = bE;
										continue
									}
									if (h < r)H.route = Y, H.prev_stop = f, H.prev_stop_start_time = bB, H.trip_num = bA % bG, H.start_pos = c >= 0 ? ba : bM, H.finish_pos = c >= 0 ? bM : ba; else continue
								}
								H.time = R, H.trip_date = bE, H.changes = h + 1, j && (H.rideStart = h > 0 ? f.rideStart : bB, H.rideEnd = R - s);
								var T = X[R];
								T ? T[T.length] = H : X[R] = [H]
							}
						}
					} while (g || bz);
					bn = null
				}
			}
		}
		if (!i) {
			W.sort(function (a, b) {
				if (a.route.sortKey < b.route.sortKey)return -1;
				if (a.route.sortKey > b.route.sortKey)return 1;
				if (a.start_time < b.start_time)return -1;
				if (a.start_time > b.start_time)return 1;
				return 0
			});
			return W
		}
		var bP = {}, bQ = Number.POSITIVE_INFINITY;
		for (var G = W.length; --G >= 0;) {
			var S = W[G], bR = S.legs[0].route ? ";" + S.legs[0].route.id : "", bS = S.legs[S.legs.length - 1];
			S.finish_time = bS.finish_time, S.walk_time = bS.route ? 0 : Math.abs(bS.finish_time - bS.start_time), R = S.departure_time;
			for (var bT = S.legs[0].start_stop; bT; bT = bT.prev_stop) {
				if (!bT.prev_stop)break;
				bS = {
					start_stop: bT.prev_stop,
					start_time: c * bT.prev_stop_start_time,
					finish_stop: bT,
					finish_time: c * (bT.time - s),
					route: bT.route,
					trip_num: bT.trip_num,
					trip_date: bT.trip_date,
					start_pos: bT.start_pos,
					finish_pos: bT.finish_pos
				}, bT.route ? bR = c == 1 ? ";" + bT.route.id + bR : bR + ";" + bT.route.id : (c < 0 && (!bT.prev_stop || !bT.prev_stop.prev_stop) && (bS.finish_time -= s), S.walk_time += Math.abs(bS.finish_time - bS.start_time)), S.legs.splice(0, 0, bS)
			}
			if (c == -1) {
				var bU = S.legs[0];
				if (!bU.route) {
					var bV = S.legs[1];
					bV && bV.route ? (bU.start_time += bV.start_time - bU.finish_time, bU.finish_time = bV.start_time) : (bU.start_time -= s, bU.finish_time -= s)
				}
				S.finish_time = S.legs[0].start_time, S.legs = S.legs.reverse();
				for (var bW = -1, bX = S.legs.length; ++bW < bX;) {
					bS = S.legs[bW];
					var R = bS.start_time - bS.finish_time;
					!bS.route && bW > 0 ? (bS.start_time = S.legs[bW - 1].finish_time, bS.finish_time = bS.start_time + R) : (bS.finish_time = bS.start_time, bS.start_time -= R);
					var f = bS.finish_stop;
					bS.finish_stop = bS.start_stop, bS.start_stop = f
				}
			}
			var bU = S.legs[0], bV = S.legs[1];
			if (!bU.route)if (bV && bV.route)bU.start_time += bV.start_time - s - bU.finish_time, bU.finish_time = bV.start_time - s; else if (a.no_just_walking)continue;
			S.start_time = S.legs[0].start_time, S.travel_time = S.finish_time - S.start_time, bR == "" ? (bR = "W", S.code = bR, bQ = S.walk_time) : (bR += ";", S.code = bR, S.direct_trip && (bR = S.legs[0].start_time + "T" + bR));
			var bY = bP[bR];
			if (!bY || c == 1 && S.finish_time < bY.finish_time || c != 1 && S.start_time > bY.start_time)bP[bR] = S
		}
		if (g)a.results = W; else {
			var bZ = [];
			for (var bR in bP) {
				var S = bP[bR], b$ = S.code;
				if (S.walk_time >= bQ && bR != "W")continue;
				for (var G = bZ.length; --G >= 0;) {
					var b_ = bZ[G];
					if (b_.code.indexOf(b$) >= 0 || b$.indexOf(b_.code) >= 0)if (c == 1 && b_.finish_time <= S.finish_time || c != 1 && b_.start_time >= S.start_time) {
						if (b_.walk_time + b_.travel_time <= S.walk_time + S.travel_time && b$.length >= b_.code.length)break
					} else!b_.direct_trip && b_.walk_time + b_.travel_time >= S.walk_time + S.travel_time && bZ.splice(G, 1)
				}
				(G < 0 || S.direct_trip) && bZ.push(S)
			}
			for (var G = bZ.length; --G >= 0;) {
				var S = bZ[G];
				a.reverseOriginal == -1 ? S.code = S.code + "T" + S.legs[S.legs.length - 1].finish_time : S.code = S.legs[0].start_time + "T" + S.code
			}
			a.results = bZ, typeof window === "object" ? window.setTimeout(function () {
				ti.findTrips(a)
			}, 10) : ti.findTrips(a)
		}
	};
	return bg()
}
ti.filterSearchResults = function (a, b) {
	for (var c = a.length; --c >= 0;) {
		var d = a[c];
		if (d.remove)continue;
		for (j = a.length; --j >= 0;) {
			if (c === j)continue;
			a[j].code.indexOf(d.code) < 0 ? d.direct_trip && !a[j].direct_trip && d.start_time >= a[j].start_time && d.finish_time <= a[j].finish_time && (a[j].remove = !0) : a[j].walk_time < d.walk_time ? d.remove = !0 : a[j].remove = !0
		}
	}
	if (cfg.defaultCity == "intercity") {
		for (var c = a.length; --c >= 0;) {
			var d = a[c];
			if (d.remove)continue;
			for (j = a.length; --j >= 0;) {
				if (c === j)continue;
				if (a[j].remove || a[j].legs.length <= 1)continue;
				d.legs.length <= a[j].legs.length && d.start_time > a[j].start_time && d.finish_time <= a[j].finish_time && (a[j].remove = !0), d.legs.length <= a[j].legs.length && d.start_time >= a[j].start_time && d.finish_time < a[j].finish_time && (a[j].remove = !0)
			}
		}
		for (var c = a.length; --c >= 0;) {
			var d = a[c];
			d.start_time >= 1440 && (d.remove = !0)
		}
	}
	var e = {};
	for (var c = a.length; --c >= 0;) {
		var d = a[c];
		if (d.remove)continue;
		if (d.start_time >= 1680 || d.finish_time < 0) {
			d.remove = !0;
			continue
		}
		d.penalty_time = d.travel_time + 5 * d.legs.length;
		var f = e[d.code];
		if (!f || f.penalty_time > d.penalty_time)e[d.code] = d
	}
	a = [];
	for (var g in e)a.push(e[g]);
	a.sort(function (a, b) {
		return a.penalty_time - b.penalty_time
	});
	var h = Number.POSITIVE_INFINITY;
	for (var c = a.length; --c >= 0;)a[c].ok = c < 5 ? 1 : 0, h > a[c].travel_time && (h = a[c].travel_time);
	a.sort(function (a, b) {
		return a.finish_time - b.finish_time
	}), b == -1 && a.sort(function (a, b) {
		return -(a.start_time - b.start_time)
	});
	if (cfg.defaultCity != "intercity" && cfg.defaultCity != "latvia") {
		for (var c = a.length; --c >= 0;) {
			if (a[c].direct_trip) {
				a[c].ok = 1;
				continue
			}
			var i = b == 1 ? a[c].finish_time - a[0].finish_time : a[0].start_time - a[c].start_time;
			i > a[0].travel_time / 2 + 60 ? a[c].ok = 0 : a[c].penalty_time > 2 * h && i > h && c >= 2 ? a[c].ok = 0 : a[c].walk_time > h && i > h && c >= 2 ? a[c].ok = 0 : c < 3 && (a[c].ok = 1)
		}
		a.sort(function (a, b) {
			return b.ok - a.ok
		});
		for (var c = a.length; --c > 0;) {
			if (a[c].ok == 1)break;
			a.pop()
		}
	}
	a.sort(function (a, b) {
		return a.finish_time - b.finish_time
	}), b == -1 && a.sort(function (a, b) {
		return -(a.start_time - b.start_time)
	});
	return a
}, ti.finalizeSearchResults = function (a, b) {
	var c = a.results, d = Array(c.length);
	for (var e = 0; e < c.length; e++) {
		var f = c[e], g = f.legs, h = 0;
		d[e] = {
			start_time: f.start_time - h,
			finish_time: f.finish_time - h,
			travel_time: f.travel_time,
			walk_time: f.walk_time,
			direct_trip: f.direct_trip,
			legs: [],
			code: f.code
		};
		var i = Number.POSITIVE_INFINITY;
		for (var j = 0; j < g.length; j++) {
			var k = g[j], l = k.route ? k.route.times.workdays[k.trip_num] : "", m = k.route ? k.route.times.trip_ids[k.trip_num] : 0, n = k.route ? k.route.times.trip_codes[k.trip_num] : 0, o = k.route ? k.route.times.trip_operators[k.trip_num] : "", p = k.route ? k.route.times.trip_groups[k.trip_num] : "", q = k.start_stop && ti.fGetStopDetails(k.start_stop.id), r = k.finish_stop && ti.fGetStopDetails(k.finish_stop.id), s = k.route && k.route.platforms && k.route.platforms.split(",") || [], t = {
				trip_num: k.trip_num,
				trip_id: m,
				trip_code: n,
				trip_date: k.trip_date,
				trip_operator: o,
				trip_group: p,
				start_pos: k.start_pos,
				finish_pos: k.finish_pos,
				start_time: k.start_time - h,
				start_platform: s[k.start_pos || 0],
				finish_platform: s[k.finish_pos || 0],
				finish_time: k.finish_time - h,
				weekdays: l,
				start_stop: q && {id: q.id, name: q.name, street: q.street, lat: q.lat, lng: q.lng},
				finish_stop: r && {id: r.id, name: r.name, street: r.street, lat: r.lat, lng: r.lng}
			};
			if (k.route) {
				t.route = ti.fGetRoutes(k.route);
				if (b) {
					var u = k.start_time - h, v = Number.POSITIVE_INFINITY;
					n = t.route.num + String("0000" + n).slice(-4);
					for (var w = 0; w < b.length; ++w) {
						var f = b[w];
						if (f.code == n && f.fromStopId == q.info && f.toStopId == r.info) {
							t.online_data = f;
							break
						}
						if (!1 && f.code && f.code.indexOf(t.route.num) === 0) {
							var x = Math.abs(u - ti.toMinutes(f.departureAsStr));
							v > x && x <= 5 && (v = x, t.online_data = f)
						}
					}
				}
			}
			if (ti.taxi)if (k.route && k.start_time - i > 30 && (j == g.length - 1 || j == g.length - 2 && !g[g.length - 1].route) || !k.route && k.finish_time - k.start_time >= 15 || j == 0 && k.start_time - a.start_time > 120) {
				for (var w = 0; w < ti.taxi.length; ++w) {
					var y = ti.taxi[w], z = (q.lng - y.lng) * 58.1, A = (q.lat - y.lat) * 111.2, B = z * z + A * A, z = (r.lng - y.lng) * 58.1, A = (r.lat - y.lat) * 111.2, C = z * z + A * A;
					B <= y.radius && C <= y.radius && (t.taxi || (t.taxi = []), t.taxi.push({
						name: y.name,
						phone: y.phone,
						km: Math.round(Math.sqrt(B))
					}))
				}
				if (t.taxi) {
					t.taxi.sort(function (a, b) {
						return a.km - b.km
					});
					var D = t.taxi[0].km;
					for (var w = 1; w < t.taxi.length; ++w)if (t.taxi[w].km > D) {
						t.taxi.length = w;
						break
					}
				}
			}
			d[e].legs.push(t), k.route && (i = k.finish_time)
		}
	}
	typeof cfg === "object" && cfg.defaultCity === "intercity" ? (d.sort(function (a, b) {
		return a.start_time - b.start_time
	}), a.reverse == -1 && d.sort(function (a, b) {
		return -(a.finish_time - b.finish_time)
	})) : (d.sort(function (a, b) {
		return a.finish_time - b.finish_time
	}), a.reverse == -1 && d.sort(function (a, b) {
		return -(a.start_time - b.start_time)
	}));
	return d
};
function $(a) {
	return document.getElementById(a)
}
var pg = {
	urlPrevious: "",
	urlLoaded: "",
	urlUnderSchedulePane: "",
	language: "",
	languageUnderSchedulePane: "",
	city: "",
	transport: "",
	schedule: null,
	optimalResults: [],
	transfers: [],
	cityTransportRoutes: {},
	showDeparturesWithNumbers: "",
	GMap: null,
	hashForMap: "",
	map: {},
	mapOverlays: [],
	mapStops: {},
	mapStopForZones: "",
	mapShowVehicles: -1,
	inputActive: null,
	stopsSuggested: [],
	stopsSuggestedForText: "",
	realTimeDepartures: {},
	inputStop: "",
	inputStopText: "",
	inputStart: "",
	inputFinish: "",
	timerSuggestedStopsHide: 0,
	timerSuggestedStopsShow: 0,
	imagesFolder: "_images/",
	translationFolder: "_translation/",
	browserVersion: 999
};
pg.addCSS = function (a) {
	var b = document.createElement("style");
	b.type = "text/css", b.styleSheet ? b.styleSheet.cssText = a : b.appendChild(document.createTextNode(a)), document.getElementsByTagName("head")[0].appendChild(b)
};
var ej = function (a) {
	var b = [], c = [], d = [], e = [], f = [], g = [], h = [], i = [], j, k, l = a.split(","), m, n, o = l.length, p = [], q = "+", r = "-", s = l[0], t = s.length, u = 0, v = [], w = 0;
	while (u < t) {
		var x, y = 0, z = 0;
		do x = s.charCodeAt(u++) - 63, z |= (x & 31) << y, y += 5; while (x >= 32);
		var A = z & 1 ? ~(z >> 1) : z >> 1;
		y = 0, z = 0, v.push(A)
	}
	H = 10, n = 240, t = v.length;
	for (m = -1, j = 0; ++m < t;) {
		var B = v[m];
		p[m] = B & 1 ? "1" : B & 2 ? "2" : B & 4 ? "4" : "0", B >>= 3, m ? (H += B, n += H) : n += B, b[j++] = n
	}
	for (var C = p.length; --C >= 0;)p[C] || (p[C] = "0");
	m = 1;
	for (var C = 0; ++m < o;) {
		var D = +l[m], E = l[++m];
		E === "" ? (E = j - C, o = 0) : E = +E;
		while (E-- > 0)d[C++] = D
	}
	--m;
	for (var C = 0, o = l.length; ++m < o;) {
		var D = +l[m], E = l[++m];
		E === "" ? (E = j - C, o = 0) : E = +E;
		while (E-- > 0)e[C++] = D
	}
	--m;
	for (var C = 0, o = l.length; ++m < o;) {
		var F = l[m], E = l[++m];
		E === "" ? (E = j - C, o = 0) : E = +E;
		while (E-- > 0)c[C++] = F
	}
	if (ti.has_trips_ids) {
		--m;
		var o = l.length;
		for (var C = 0; ++m < o;) {
			if (l[m] === "")break;
			f[C] = +l[m], C > 0 && (f[C] += f[C - 1]), ++C
		}
		for (var C = 0; ++m < o;) {
			if (l[m] === "")break;
			g[C] = l[m], ++C
		}
		if (ti.has_trips_ids === 2) {
			for (var C = 0; ++m < o;) {
				if (l[m] === "")break;
				i[C] = l[m], ++C
			}
			for (var C = 0; ++m < o;) {
				if (l[m] === "")break;
				h[C] = l[m], ++C
			}
		}
		++m
	}
	--m, k = 1;
	for (var C = j, G = j, H = 5, o = l.length; ++m < o;) {
		H += +l[m] - 5;
		var E = l[++m];
		E !== "" ? (E = +E, G -= E) : (E = G, G = 0);
		while (E-- > 0)b[C] = H + b[C - j], ++C;
		G <= 0 && (G = j, H = 5, ++k)
	}
	final_data = {
		workdays: c,
		times: b,
		tag: p.join(""),
		valid_from: d,
		valid_to: e,
		trip_ids: f,
		trip_codes: g,
		trip_operators: h,
		trip_groups: i
	};
	return final_data
};
(function () {
	navigator.appVersion.indexOf("MSIE") >= 0 && (pg.browserVersion = parseFloat(navigator.appVersion.split("MSIE")[1])), typeof document.body.style.transform != "undefined" ? pg.transformCSS = "transform" : typeof document.body.style.MozTransform != "undefined" ? pg.transformCSS = "-moz-transform" : typeof document.body.style.webkitTransform != "undefined" ? pg.transformCSS = "-webkit-transform" : typeof document.body.style.msTransform != "undefined" ? pg.transformCSS = "-ms-transform" : typeof document.body.style.OTransform != "undefined" && (pg.transformCSS = "-o-transform");
	if (window.location.host.indexOf("soiduplaan") < 0) {
		var a = ["stops.lt", "ridebus.org", "marsruty.ru"];
		for (var b = 0; b < a.length; ++b)if (window.location.host.indexOf(a[b]) >= 0) {
			pg.imagesFolder = "../_images/", pg.translationFolder = "../_translation/";
			break
		}
	} else pg.imagesFolder = "/_images/", pg.translationFolder = "/_translation/";
	cfg.transport_colors = {
		walk: "black",
		metro: "#ff6A00",
		bus: "#0073ac",
		trol: "#dc3131",
		tram: "#009900",
		nightbus: "#303030",
		regionalbus: "#004a7f",
		suburbanbus: "#004a7f",
		commercialbus: "purple",
		intercitybus: "purple",
		internationalbus: "purple",
		seasonalbus: "purple",
		expressbus: "green",
		minibus: "green",
		train: "#009900",
		plane: "#404040",
		festal: "orange"
	}, cfg.transport_icons = {};
	for (var b in cfg.transport_colors)cfg.transport_icons[b] = "'" + pg.imagesFolder + b + ".png'";
	cfg.transport_icons.expressbus = "'" + pg.imagesFolder + "bus_green.png'", pg.browserVersion >= 8 && (cfg.transport_icons.walk = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASBAMAAACk4JNkAAAABGdBTUEAALGPC/xhBQAAAA9QTFRFAAAAoKCggICAQEBAAAAAHhqGcwAAAAF0Uk5TAEDm2GYAAAA2SURBVHjaY2AgBTjDGIxwlosDlMFi4gJlKYsYO8FkFWAaXAygLGYnmKSIAYxlwgBjOSHswAkAmlwFFe0D65gAAAAASUVORK5CYII=", cfg.transport_icons.bus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD///8Ac6wQQkMlAAAAAXRSTlMAQObYZgAAAD1JREFUeF5lysEJACAMA8CAg7mPMzlBKLiCI/UtQi0WCmIC9wjBk2rTXVSgmG23sbuk/MYnHSbX3KHeMHIA4toiwKUahYMAAAAASUVORK5CYII=", cfg.transport_icons.trol = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD////cMTHYIE0PAAAAAXRSTlMAQObYZgAAAEFJREFUeF5lytEJACEMA9BAB3DPm+kmKAVXEFyoAwgxWPDHBN5HCIBMYXPI1gRILUYu+fkv3eO1PtfOON4dqZaVDa25JKCSbwixAAAAAElFTkSuQmCC", cfg.transport_icons.tram = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD///8Afw6EL87tAAAAAXRSTlMAQObYZgAAAEZJREFUeF5lzLEJQCEMBNBA2g8uJPyVnCuVIziHI1ilV4wXFBsv8LgiHBEPIvi5vxVoprBKgk3y6/659nn6XQgBUGTvittZX1shrS0B3oUAAAAASUVORK5CYII=", cfg.transport_icons.nightbus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAABGdBTUEAALGPC/xhBQAAAAxQTFRFAAAAwMDA////MDAwFsRlHAAAAAF0Uk5TAEDm2GYAAAA3SURBVHjaY2BABvb/DwDJX6s+MDAw////B0hmrZoDJFetWoOVhKiBkOv/r4GTYBGGD0AII0EAAOLaIsCN4ck7AAAAAElFTkSuQmCC", cfg.transport_icons.regionalbus = cfg.transport_icons.suburbanbus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD///8ASn8BHE3xAAAAAXRSTlMAQObYZgAAAD1JREFUeF5lysEJACAMA8CAg7mPMzlBKLiCI/UtQi0WCmIC9wjBk2rTXVSgmG23sbuk/MYnHSbX3KHeMHIA4toiwKUahYMAAAAASUVORK5CYII=", cfg.transport_icons.commercialbus = cfg.transport_icons.intercitybus = cfg.transport_icons.internationalbus = cfg.transport_icons.seasonalbus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD///+AAIDHvR5zAAAAAXRSTlMAQObYZgAAAD1JREFUeF5lysEJACAMA8CAg7mPMzlBKLiCI/UtQi0WCmIC9wjBk2rTXVSgmG23sbuk/MYnHSbX3KHeMHIA4toiwKUahYMAAAAASUVORK5CYII=", cfg.transport_icons.minibus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAABGdBTUEAALGPC/xhBQAAAAxQTFRFAAAAwMDA////AH8OhC/O7QAAAAF0Uk5TAEDm2GYAAAAzSURBVHjaY2DABPz/QaTd6gNA8v//D0Dy1qoCIPlq1QY4CRGHkK//bYCTEBEb5gNwEgEAnKMcHFzyiOMAAAAASUVORK5CYII=", cfg.transport_icons.train = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASAgMAAAAroGbEAAAADFBMVEUAAADAwMD///8Afw6EL87tAAAAAXRSTlMAQObYZgAAAEJJREFUeF5lzLEJACAMBMAXV7J0ImdyguAIFtnHxgHEGEhI4xdXPM/DI7KBLHLURl0lGr+2Cdedpjf+g5I4PhkVlgctPiXCztUWnAAAAABJRU5ErkJggg==", cfg.transport_icons.plane = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASBAMAAACk4JNkAAAAD1BMVEUAAABAQEBLS0uAgIA5OTlhl+UMAAAAAXRSTlMAQObYZgAAAEBJREFUeF5tzNEJACAIRVGtBXqjOEL7LxXesJ98oBwVtC++Siq5csqwCsRloptG/qTlql8D5a+JIjti3saSanMATQYDyukaLU8AAAAASUVORK5CYII="), cfg.defaultCity == "rostov" ? (cfg.transport_colors.bus = "#dc3131", cfg.transport_colors.trol = "#0073ac", cfg.transport_colors.minibus = "#ff6600", cfg.transport_colors.suburbanbus = "#9c1630") : cfg.defaultCity == "tallinna-linn" && (cfg.transport_colors.bus = "#00c7b8", cfg.transport_colors.trol = "#0064d7", cfg.transport_colors.tram = "#ff601e", cfg.transport_colors.regionalbus = "#9c1630", cfg.transport_colors.commercialbus = "#6900B8", cfg.transport_icons.bus = "_images_tallinn/bus.png", cfg.transport_icons.trol = "_images_tallinn/trol.png", cfg.transport_icons.tram = "_images_tallinn/tram.png", cfg.transport_icons.regionalbus = "_images_tallinn/regionalbus.png", cfg.transport_icons.commercialbus = "_images_tallinn/commercialbus.png", cfg.transport_icons.train = "_images_tallinn/train.png");
	if (cfg.defaultCity == "vilnius" || cfg.defaultCity == "rostov" || cfg.defaultCity == "krasnodar")ti["explodeTimes"] = ej;
	var c = "";
	for (var b in cfg.transport_colors) {
		var d = cfg.transport_colors[b];
		c += "." + b + "{ background-color:" + d + "; }", typeof mobile != "undefined" ? (c += ".nav-transport ." + b + " span, .nav-transport ." + b + " a, .icon-" + b + "{ background-image:url(" + cfg.transport_icons[b] + "); }", c += ".icon-" + b + "{ width:18px; height:18px;}", c += ".label-" + b + " {background-color:" + d + ";}") : cfg.city.defaultTransport != "tablo" && (c += ".icon.icon_" + b + "{ background-image:url(" + cfg.transport_icons[b] + ");}"), c += ".transfer" + b + ",a.transfer" + b + "{ color:" + d + "; font-weight:bold; }", c += ".departure" + b + "{ color:" + d + "; font-size:smaller; font-style:normal; }"
	}
	c += cfg.city.custom_css || "", pg.addCSS(c)
})(), pg.bodyKeyDown = function (a, b) {
	b || (b = window.event ? window.event.keyCode : a.keyCode);
	if (b == 27) {
		var c = $("ulScheduleDirectionsList");
		c && c.style && c.style.display != "none" ? c.style.display = "none" : pg.schedule && pg.aScheduleClose_Click(a)
	}
}, pg.fLang_Click = function (a) {
	var b = a && (a.target || a.srcElement);
	if (b && (b.tagName || "").toLowerCase() == "a") {
		if (b.innerHTML.length < 10) {
			pg.fUrlSet({schedule: pg.schedule, language: b.innerHTML});
			return pg.cancelEvent(a)
		}
	} else if (b && (b.tagName || "").toLowerCase() == "img") {
		pg.fUrlSet({schedule: pg.schedule, language: b.src.slice(-6, -4)});
		return pg.cancelEvent(a)
	}
	return !1
}, pg.divHeader_Click = function (a) {
	a = a || window.event;
	var b = a.target || a.srcElement;
	while (b && b.nodeName.toLowerCase() !== "a")b = b.parentNode;
	if (b && b.href && b.href.indexOf("http:") >= 0)return !0;
	pg.aScheduleClose_Click(a)
}, pg.aScheduleClose_Click = function (a) {
	a = a || window.event;
	if (pg.schedule)if (pg.urlUnderSchedulePane == "")pg.city = "nothing", pg.fUrlSet({
		city: pg.schedule.city,
		transport: pg.schedule.transport,
		schedule: null
	}); else {
		var b = pg.fUrlParse(pg.urlUnderSchedulePane);
		b.language != pg.language && (b.language = pg.language, pg.city = "nothing"), b.schedule = null, pg.fUrlSet(b)
	}
	return pg.cancelEvent(a)
}, pg.fCreateLanguagesBar = function () {
	var a = $("divLang"), b = "", c = cfg.city.languages.split(",");
	for (var d = 0; d < c.length; d++) {
		var e = c[d];
		cfg.city.languageFlags ? b += "<a title=\"" + cfg.languages[e] + "\"><img src=\"" + e + ".png\" style=\"width:32px; height:26px; padding:0 5px;\"></a>" : (b += "<a title=\"" + cfg.languages[e] + "\" class=\"underlined\">" + e + "</a>", cfg.city.navigation === "riga" && d % 3 === 2 ? b += " " : b += "&nbsp;")
	}
	(a || {}).innerHTML = b
}, pg.fTranslateStaticTexts = function () {
	if (cfg.defaultCity === "chelyabinsk" && (pg.language === "ru" || pg.language === "" && cfg.defaultLanguage === "ru")) {
		i18n.transport.minibus = i18n.transport1.minibus = cfg.city.minibus;
		var a = i18n.lowFloorVehicles.lastIndexOf(",");
		a > 1 && (i18n.lowFloorVehicles = i18n.lowFloorVehicles.substr(0, a)), a = i18n.lowFloorDepartures.lastIndexOf(","), a > 80 && (i18n.lowFloorDepartures = i18n.lowFloorDepartures.substr(0, a) + "."), a = i18n.checkHandicapped.lastIndexOf(",")
	}
	cfg.defaultCity === "rostov" && (pg.language === "ru" || pg.language === "" && cfg.defaultLanguage === "ru") && (i18n.transport.minibus = i18n.transport1.minibus = cfg.city.minibus), cfg.defaultCity === "klaipeda" && (i18n.lowFloorDepartures = i18n.miniBusDepartures || ""), cfg.defaultCity === "vilnius" && (i18n.menuTickets = i18n.transportNews || ""), document.title = i18n.headerTitle, ($("header") || {}).innerHTML = (cfg.city.logoInHeader || "") + i18n.headerTitle, ($("spanYouAreHere") || {}).innerHTML = i18n.youAreHere, ($("aYouAreHere") || {}).href = pg.hashHome = (pg.hashHome + "map").split("map")[0] + "map/" + pg.language, ($("spanRoutesFromStop") || {}).innerHTML = i18n.departingRoutes + ":", ($("spanPlan") || {}).innerHTML = cfg.city.navigation !== "top" ? i18n.tripPlanner : i18n.tripPlanner.replace("<br/>", " ").replace("<br>", " "), ($("spanShowMap") || {}).innerHTML = i18n.showStopsMap, ($("spanShowTraffic") || {}).innerHTML = i18n.mapShowTraffic, ($("aPlanShowMap") || {}).innerHTML = "<br/><br/>" + i18n.showStopsMap.toLowerCase(), ($("spanPrintSchedule") || {}).innerHTML = i18n.print, ($("spanReturnToRoutes") || {}).innerHTML = i18n.returnToRoutes, ($("spanShowDirectionsMap") || {}).innerHTML = i18n.showInMap, ($("buttonSearch") || {}).value = i18n.searchRoute, ($("inputReverseDepart") || {}).text = i18n.depart, ($("inputReverseArrive") || {}).text = i18n.arrive, ($("labelDepartureDate") || {}).innerHTML = i18n.departuresFor, ($("inputDepartureDate-1") || {}).text = i18n.fromNow, ($("inputDate0") || {}).text = ($("inputDepartureDate0") || {}).text = i18n.today, ($("inputDate1") || {}).text = ($("inputDepartureDate1") || {}).text = i18n.tomorrow, ($("mapShowAllStops") || {}).title = i18n.mapShowAllStops, ($("mapShowTraffic") || {}).title = i18n.mapShowTraffic, ($("mapShowVehicles") || {}).title = i18n.mapShowVehicles;
	var b = new Date;
	for (var a = 2; a <= 6; a++) {
		var c = new Date(b.getFullYear(), b.getMonth(), b.getDate() + a);
		($("inputDate" + a) || {}).text = pg.formatDate(c), ($("inputDepartureDate" + a) || {}).text = pg.formatDate(c)
	}
	($("labelHandicapped") || {}).title = i18n.checkHandicapped, ($("aExtendedOptions") || {}).innerHTML = ($("divContentPlannerOptionsExtended") || {style: {}}).style.display ? i18n.extendedOptions : i18n.extendedOptionsHide, ($("labelRoutes") || {}).innerHTML = i18n.rideOnlyRoutes + ":", ($("labelChangeTimeText") || {}).innerHTML = i18n.timeForConnection + ":", ($("labelWalkMaxText") || {}).innerHTML = i18n.walkingMax + ":", ($("labelWalkSpeedText") || {}).innerHTML = i18n.walkingSpeed + ":";
	var d = $("inputStop");
	d && (d.title = i18n.typeStartStop, d.className == "empty" && (d.value = i18n.startStop)), d = $("inputStart"), d && (d.title = i18n.typeStartStop, d.className == "empty" && (d.value = i18n.startStop)), d = $("inputFinish"), d && (d.title = i18n.typeFinishStop, d.className == "empty" && (d.value = i18n.finishStop)), d = $("inputRoutes"), d && (d.title = i18n.typeRouteNameOrNumber, d.className == "empty" && (d.value = i18n.typeRouteNameOrNumber)), ($("labelInputRoutes") || {}).innerHTML = i18n.filter + ":"
}, pg.fGetCity = function (a) {
	for (var b in cfg.cities)if (cfg.cities[b].region === a)return b;
	return a
}, pg.fCreateNavigation = function (a) {
	if (typeof mobile == "undefined" || typeof a != "undefined") {
		var b = "<dt class=\"splitter\"></dt><!-- -->", c = pg.fGetCity(pg.city), d = 0;
		if (typeof mobile != "undefined")var e = [];
		if (cfg.cities[c]) {
			var f = "", g = "", h = {};
			for (var i = 1; i <= 2; i++) {
				var j = pg.fUrlSet({city: c, transport: null, hashForMap: null}, !0);
				typeof mobile != "undefined" && e.push({
					hash: j,
					city: c,
					type: i == 1 ? "city" : "region",
					transport: [],
					timeout: cfg.cities[c].goHomeTimeout
				});
				if (!cfg.cities[c].goHomeTimeout) {
					f += "<dt><a id=\"" + (i == 1 ? "city" : "region") + "\" href=\"#" + j + "\">" + (cfg.cities[c].logo || "") + "<span class=\"hover\">";
					var k = cfg.cities[c].name;
					if (k)var l = k[pg.language] || k.en || (i == 1 ? i18n.cityRoutes : i18n.regionRoutes); else var l = i == 1 ? i18n.cityRoutes : i18n.regionRoutes;
					f += l, typeof mobile != "undefined" && (e[i - 1].name = l, e[i - 1].logo = cfg.cities[c].logo || ""), f += "</span></a></dt>"
				}
				for (var m = 0; m < cfg.cities[c].transport.length; m++) {
					var n = cfg.cities[c].transport[m];
					if ((cfg.cities[c].transportTemporary || {})[n] && !pg.cityTransportRoutes[c + "_" + n])continue;
					var o = " checked=\"checked\"";
					cfg.cities[c].transportPlannerUncheck && cfg.cities[c].transportPlannerUncheck[n] && (o = "");
					var p = pg.fUrlSet({
						city: c,
						transport: n,
						hashForMap: null
					}, !0), q = ((cfg.cities[c].transportTip || {})[n] || {})[pg.language];
					q && (q = " title=\"" + q + "\""), f += ("<dt><a id=\"" + c + "_{tr}\" href=\"#" + p + "\"" + q + "><span class=\"icon icon_{tr}\"></span><span class=\"hover\">" + i18n.transport[n] + "</span></a></dt>").replace(/{tr}/g, n), typeof mobile != "undefined" && e[i - 1].transport.push({
						hash: p,
						transport: n,
						name: i18n.transport[n]
					}), h[n] || (h[n] = !0, g += ("<label for=\"checkbox{tr}\"><input name=\"checkbox{tr}\" id=\"checkbox{tr}\" type=\"checkbox\" value=\"{tr}\"" + o + "/>").replace(/{tr}/g, n) + i18n.transport[n] + "</label> "), cfg.transportOrder[n] = ++d
				}
				c = cfg.cities[c].region;
				if (!c || !cfg.cities[c])break;
				f += b
			}
			if (cfg.defaultCity == "pskov") {
				f += b;
				var r = ["dedovichi", "nevel", "novorzhev", "ostrov", "porxov"];
				for (var m = 0; m < r.length; ++m) {
					var c = r[m];
					f += "<dt><a id=\"" + c + "_bus\" href=\"#" + c + "/bus\"><span class=\"hover\">" + cfg.cities[c].name[pg.language] + "</span></a></dt>"
				}
			}
			($("listTransports") || {}).innerHTML = f, ($("divContentPlannerOptionsTransport") || {}).innerHTML = i18n.optionsTransport + ":" + g
		}
		cfg.transportOrder.commercialbus && cfg.transportOrder.regionalbus && (cfg.transportOrder.commercialbus = cfg.transportOrder.regionalbus), a && a(e)
	}
}, pg.fLoadPage = function () {
	cfg.city.languages = cfg.city.languages || "en,ru", cfg.defaultLanguage = cfg.city.defaultLanguage || cfg.city.languages.split(",")[0], pg.showDeparturesWithNumbers !== !0 && pg.showDeparturesWithNumbers !== !1 && (pg.showDeparturesWithNumbers = cfg.city.showDeparturesWithNumbers, pg.toggleClass($("divScheduleContentInner"), "HideNumbers", !pg.showDeparturesWithNumbers)), pg.fTranslateStaticTexts(), pg.fCreateLanguagesBar(), pg.loadedRoutesHash = null, pg.loadedDepartingRoutes = null, pg.loadedPlannerParams = null, pg.fCreateNavigation(), pg.fTabActivate();
	pg.schedule && pg.fScheduleLoad()
}, pg.fLoadLanguageScript = function (a, b) {
	var c = $("scriptLanguage");
	c && document.getElementsByTagName("head")[0].removeChild(c);
	var d = document.createElement("script");
	d.setAttribute("id", "scriptLanguage"), d.setAttribute("type", "text/javascript"), d.setAttribute("src", pg.translationFolder + a + ".js"), b && d.addEventListener("load", function (a) {
		b(a)
	}, !1), document.getElementsByTagName("head")[0].appendChild(d)
}, pg.fTogglePlannerOptions = function (a) {
	var b = $("divContentPlannerOptionsExtended");
	b.style.display && a !== !1 ? (b.style.display = "", ($("aExtendedOptions") || {}).innerHTML = i18n.extendedOptionsHide) : (b.style.display = "none", ($("aExtendedOptions") || {}).innerHTML = i18n.extendedOptions);
	if (a)return pg.cancelEvent(a)
}, pg.storeMyLocation = function (a) {
	var b = a.coords.latitude = Math.round(a.coords.latitude * 1e6) / 1e6, c = a.coords.longitude = Math.round(a.coords.longitude * 1e6) / 1e6;
	pg.myLocation = b + ";" + c
}, pg.replaceHtml = function (a, b) {
	if (a) {
		var c = a.nextSibling, d = a.parentNode;
		d.removeChild(a), a.innerHTML = b, c ? d.insertBefore(a, c) : d.appendChild(a)
	}
}, pg.storeStyles = function () {
	pg.styles = {};
	var a = document.styleSheets || [];
	for (var b = 0; b < a.length; ++b) {
		var c = [];
		try {
			c = a[b].rules || a[b].cssRules || []
		} catch (d) {
		}
		for (var e = c.length; --e >= 0;) {
			var f = c[e].selectorText;
			if (f) {
				var g = c[e].style, h = f.split(",");
				for (var i = h.length; --i >= 0;)pg.styles[h[i].trim()] = g
			}
		}
	}
}, pg.getStyle = function (a) {
	if (a && a.indexOf("transport_icon_") == 0) {
		var b = a.substring("transport_icon_".length), c = (pg.styles[".icon.icon_" + b] || pg.styles[".icon_" + b + ".icon"] || pg.styles[".icon-" + b] || {backgroundImage: pg.imagesFolder + b + ".png"}).backgroundImage || "";
		c = c.replace(/\"/g, ""), c = c.replace(/\'/g, ""), c.indexOf("url(") == 0 && (c = c.substring(4, c.length - 1));
		return c
	}
	return pg.styles[a]
}, pg.styleRGBtoHex = function (a) {
	if (a == "green")return "008000";
	if (a == "purple")return "800080";
	if (a.indexOf("#") === 0)return a.substring(1);
	var b = a.replace("rgb(", "").replace(")", ""), c = b.split(",");
	b = ((1 << 24) + (+c[0] << 16) + (+c[1] << 8) + +c[2]).toString(16).slice(1);
	return b
}, pg.toggleClass = function (a, b, c) {
	if (a) {
		var d = " " + (a.className || "") + " ";
		c && d.indexOf(" " + b + " ") < 0 ? a.className = (d + b).trim() : !c && d.indexOf(" " + b + " ") >= 0 && (a.className = d.replace(" " + b + " ", "").trim())
	}
}, pg.cancelEvent = function (a) {
	a && (a.cancelBubble = !0, a.returnValue = !1, a.preventDefault && a.preventDefault(), a.stopPropagation && a.stopPropagation());
	return !1
}, pg.formatDate = function (a) {
	typeof a == "number" && (a = new Date(a * 1e3 * 60 * 60 * 24));
	var b = a.getDate(), c = 1 + a.getMonth(), d = a.getFullYear();
	if (pg.language == "ru" || pg.language == "ee")d = b, b = a.getFullYear();
	return (d < 10 ? "0" : "") + d + (c < 10 ? "-0" : "-") + c + (b < 10 ? "-0" : "-") + b
}, pg.nonEmptyCount = function (a) {
	var b = 0;
	for (var c in a)a.hasOwnProperty(c) && a[c] && ++b;
	return b
}, pg.fUrlSet = function (a, b) {
	if (a) {
		a.schedule && pg.schedule && (typeof a.schedule.city == "undefined" && (a.schedule.city = pg.schedule.city), typeof a.schedule.transport == "undefined" && (a.schedule.transport = pg.schedule.transport), typeof a.schedule.num == "undefined" && (a.schedule.num = pg.schedule.num), typeof a.schedule.dirType == "undefined" && (a.schedule.dirType = pg.schedule.dirType), typeof a.schedule.stopId == "undefined" && (a.schedule.stopId = pg.schedule.stopId));
		var c = ["city", "transport", "inputStop", "inputStart", "inputFinish", "hashForMap", "language"];
		for (var d = c.length; --d >= 0;)typeof a[c[d]] == "undefined" && (a[c[d]] = pg[c[d]])
	} else a = pg;
	var e = "";
	if (a.schedule)e = (a.schedule.tripNum || "") + (e ? "/" + e : ""), e = (a.schedule.stopId || "") + (e ? "/" + e : ""), e = (typeof mobile != "undefined" && !b ? a.schedule.dirUrl || "" : a.schedule.dirType || "") + (e ? "/" + e : ""), e = (a.schedule.num || "") + (e ? "/" + e : ""), e = (a.schedule.transport || "") + (e ? "/" + e : ""), a.schedule.city && a.schedule.city != cfg.defaultCity && (e = a.schedule.city + (e ? "/" + e : "")), e += a.hashForMap ? "/map" : ""; else {
		a.transport == "stop" ? (a.city = pg.fGetCity(a.city), e = "stop" + (a.inputStop ? "/" + a.inputStop : "")) : a.transport == "plan" ? (a.city = pg.fGetCity(a.city), e = "plan/" + (a.inputStart || "") + (a.inputFinish ? "/" + a.inputFinish : "")) : e = (a.transport || "") + (e ? "/" + e : "");
		if (!e || a.city !== cfg.defaultCity)e = a.city + (e ? "/" + e : "");
		e += a.hashForMap ? "/" + a.hashForMap : ""
	}
	e += a.language != cfg.defaultLanguage ? "/" + a.language : "", e = ti.toAscii(e, !0);
	if (b)return e;
	Hash.go(e);
	return e
}, pg.fUrlSetMap = function (a, b) {
	var c = pg.hashForMap || "map";
	a ? (typeof a != "object" && (a = {}), a.optimalRoute && (c = "map,,," + a.optimalRoute), a.maximized && c.indexOf(",max") < 0 && (c += ",max"), a.maximized === !1 && (c = c.replace(",max", "")), a.clusters && c.indexOf(",stops") < 0 && (c += ",stops"), a.clusters === !1 && (c = c.replace(",stops", ""))) : c = "";
	if (b)return c;
	pg.hashForMap = c, c = pg.fUrlSet(null, !0), c != Hash.getHash() ? Hash.go(c) : pg.fMapShow()
}, pg.fUrlParse = function (a) {
	a = decodeURI(a);
	var b = {}, c = a.indexOf("#");
	c >= 0 && (a = a.substring(c + 1)), a = a ? a.split("/") : [], a.length && ("," + cfg.city.languages + ",").indexOf("," + a[a.length - 1] + ",") >= 0 ? b.language = a.pop() : b.language = cfg.defaultLanguage, a.length && "map" === a[a.length - 1].substring(0, 3) ? b.hashForMap = a.pop() : b.hashForMap = "", b.transport = "", a[0] || (b.transport = typeof cfg.city.defaultTransport != "undefined" ? cfg.city.defaultTransport : cfg.city.transport[0]), a.length && cfg.cities[a[0]] ? b.city = a.shift() : b.city = cfg.defaultCity, a[0] && (b.transport = a[0], a[0] === "stop" ? b.inputStop = a[1] || "" : a[0] === "plan" ? (b.inputStart = a[1] || "", b.inputFinish = a[2] || "") : a[1] && (b.schedule = {
		city: b.city,
		transport: a[0],
		num: a[1],
		dirType: a[2] || "",
		stopId: a[3] || "",
		tripNum: isNaN(a[4]) ? 0 : +a[4],
		dirUrl: a[2] || ""
	}));
	return b
}, pg.fUrlExecute = function (a) {
	var b = pg.fUrlParse(a), c = pg.language;
	pg.language = b.language;
	var d = pg.city;
	pg.city = b.city;
	var e = pg.hashForMap;
	pg.hashForMap = b.hashForMap, pg.transport = b.transport, pg.inputStop = b.inputStop || pg.inputStop, pg.inputStart = b.inputStart || pg.inputStart, pg.inputFinish = b.inputFinish || pg.inputFinish, pg.urlPrevious = pg.urlLoaded, pg.urlLoaded = a, b.schedule ? pg.fScheduleShow(b.schedule) : (pg.fScheduleHide(), pg.fTabActivate()), c != pg.language && (c || pg.language != cfg.defaultLanguage) && pg.fLoadLanguageScript(pg.language), d !== pg.city && cfg.defaultCity != "pskov" && (cfg.cities[d] || {region: ""}).region !== pg.city && pg.fLoadPage(), pg.hashForMap ? (e !== pg.hashForMap, pg.fMapShow()) : document.body.className.indexOf("Map") >= 0 && pg.fMapHide(), typeof mobile != "undefined" && (b.hash = a, mobile.render(b))
};
var v3 = {
	ready: !1, setMap: function (a) {
		this.map = a;
		var b = new google.maps.OverlayView;
		b.draw = function () {
		}, b.onAdd = function () {
			v3.ready = !0
		}, b.setMap(a), this.overlay = b
	}, getPanes: function () {
		return this.overlay.getPanes()
	}, getZoomByBounds: function (a) {
		var b = this.map.mapTypes.get(this.map.getMapTypeId()), c = b.maxZoom || 21, d = b.minZoom || 0, e = this.map.getProjection().fromLatLngToPoint(a.getNorthEast()), f = this.map.getProjection().fromLatLngToPoint(a.getSouthWest()), g = Math.abs(e.x - f.x), h = Math.abs(e.y - f.y), i = 40;
		for (var j = c; j >= d; --j)if (g * (1 << j) + 2 * i < this.map.getDiv().clientWidth && h * (1 << j) + 2 * i < this.map.getDiv().clientHeight)return j;
		return 0
	}, decodeLevels: function (a) {
		var b = [];
		for (var c = 0; c < a.length; ++c) {
			var d = a.charCodeAt(c) - 63;
			b.push(d)
		}
		return b
	}, fromLatLngToDivPixel: function (a, b) {
		var c = this.map.getProjection().fromLatLngToPoint(a), d = Math.pow(2, b), e = new google.maps.Point(c.x * d, c.y * d);
		return e
	}, fromDivPixelToLatLng: function (a, b) {
		var c = Math.pow(2, b), d = new google.maps.Point(a.x / c, a.y / c), e = this.map.getProjection().fromPointToLatLng(d);
		return e
	}, fromLatLngToContainerPixel: function (a) {
		return this.overlay.getProjection().fromLatLngToContainerPixel(a)
	}, fromLatLngToDivPixel2: function (a) {
		return this.overlay.getProjection().fromLatLngToDivPixel(a)
	}
};
pg.fTabShowMap_Click = function (a, b) {
	b == "traffic" ? (pg.fShowTraffic(!0), pg.mapShowAllStops = !1) : (pg.fShowTraffic(!1), pg.mapShowAllStops = !1), pg.hashForMap == "map" ? pg.fMapShow() : (pg.hashForMap = "map", pg.fUrlSet());
	return pg.cancelEvent(a)
}, pg.fTabDrive_Click = function (a) {
	pg.mapShowAllStops = !1, pg.hashForMap === "map" ? (pg.hashForMap = "map,drive", pg.fMapShow()) : (pg.hashForMap = "map,drive", pg.fUrlSet());
	return pg.cancelEvent(a)
}, pg.fMapHide = function () {
	pg.mapShowVehiclesInterval && (clearInterval(pg.mapShowVehiclesInterval), pg.mapShowVehiclesInterval = 0), document.body.className = pg.schedule ? "ScheduleDisplayed" : "", pg.browserVersion <= 7 && ($("divContent").innerHTML = $("divContent").innerHTML), typeof mobile != "undefined" && jq("body").removeClass("ScheduleMapDisplayed").removeClass("MapDisplayed")
}, pg.loadGoogleMapsScript = function (a) {
	if (pg._googleMapsStatus === 0)pg._googleMapsStatus = null; else {
		if (pg._googleMapsStatus == 2) {
			a();
			return
		}
		if (pg._googleMapsStatus == 1) {
			window.setTimeout(function () {
				pg.loadGoogleMapsScript(a)
			}, 300);
			return
		}
		pg._googleMapsStatus = 1;
		var b = String("." + window.location.host).split(".");
		b = b[b.length - 2] + "." + b[b.length - 1];
		var c = {
			"marsrutai.info": "ABQIAAAA1ScCs8FhCgcezEz08rROsxQju4QTY177ZUqtiHd-QtBfjDmWeBTlPLYbFmcJsp5WVjYOKK7pxhVUGA",
			"stops.lt": "ABQIAAAArqWDOAEUwW-f4DAlPS3CFhSFiohBI8HDKcqZbSL9Blnl2N0P6xQIY1qdKcarEC3K5F1xlRHMvP2zsw",
			"stops.lt:8001": "ABQIAAAArqWDOAEUwW-f4DAlPS3CFhRS_AytSF_iFi1JaeVfdJQz3w8fixRgzniHC4NzA78m-sHMkAVpJDQZPQ",
			"ridebus.org": "ABQIAAAArqWDOAEUwW-f4DAlPS3CFhQtcvUe0k-acda2umcpBWexvLqe7hTh8mwk-hjXKhW0nwqlbxJBU1WcfA",
			"marsrutai.lt": "ABQIAAAA1ScCs8FhCgcezEz08rROsxQGlT3sMZUx4ELiDsrzQfh3fbE5khQ-VtHZgCpCq3rMgF4qEPGT_fD-Yw",
			"marsruty.ru": "ABQIAAAA1ScCs8FhCgcezEz08rROsxTgEjs8cB5ffNAZuuA5xYZVRMEC_RSmeQpvGIjEAeBKsPO8v9KmXGeJdg",
			"marsruti.lv": "ABQIAAAA1ScCs8FhCgcezEz08rROsxTYmbSir-xnBQyBRKLAM3-zg8wZKhQcw0RK6Q3vOXEUtA6VwVQNO9N9hg",
			"rigassatiksme.lv": "ABQIAAAA1ScCs8FhCgcezEz08rROsxSSbFstyt1J_asSxeW1gR9mGgEedBTJBpX_QLqdBGmCpjyPTj8ODBn8oQ",
			"tallinn.ee": "ABQIAAAAY5IFnRsLfXSjZuFUXBlQxxRmcNvZLfb84ObGC-suP-X_C6yqjBSwz9_gsefSDa8JbazmtMODjhF-SQ",
			"mupatp1.ru": "ABQIAAAArqWDOAEUwW-f4DAlPS3CFhQtkVZq0yHlxGB1DBtmGoZBafDtWhSa1kgxMwsdoRr4QNd_h1rXefARdA",
			"muptu.ru": "ABQIAAAArqWDOAEUwW-f4DAlPS3CFhRG_nk74X5v3utE3dwuERMNPIBz4RRVfJ9UhZonHQJyljheWX-f5oBVhg",
			other: "AIzaSyA_6V_Khj4mysHNI15QGgHi0EgYlVUBIKM"
		}, d = c[b] || c.other;
		cfg.defaultCity.indexOf("tallinn") >= 0 && (d = c["tallinn.ee"]);
		var e = document.createElement("script");
		e.type = "text/javascript", e.async = !0, e.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&libraries=geometry,places&callback=pg.GMapScriptLoaded&language=" + pg.language;
		if (e.onprogress) {
			var f = "";
			e.onprogress = function () {
				f += ".", $("divMap").innerHTML = "<div style=\"margin:10px;\">" + i18n.loadingMap + f + "</div>"
			}
		}
		var g = function () {
			typeof google != "object" && (pg._googleMapsStatus = 0, $("divMap").innerHTML = "<div style=\"margin:10px;\">" + i18n.failedMap + "</div>", pg.GMap = null), e.onload = e.onreadystatechange = e.onerror = null
		};
		e.onreadystatechange = function () {
			(this.readyState == "complete" || this.readyState == "loaded") && setTimeout(g, 10)
		}, e.onload = e.onerror = g, document.getElementsByTagName("head")[0].appendChild(e), window.setTimeout(function () {
			pg.loadGoogleMapsScript(a)
		}, 300)
	}
}, pg.GMapScriptLoaded = function () {
	pg._googleMapsStatus = 2
}, pg.fMapShow = function () {
	var a = pg.hashForMap.split(","), b, c = !1;
	a[a.length - 1] == "max" && (b = !0, a.pop()), a[a.length - 1] == "drive" && (b = !0, a.pop()), a[a.length - 1] == "stops" && (c = !0, a.pop());
	var d = a[1] || cfg.defaultCity, e, f, g, h;
	pg.schedule ? (document.body.className = b ? "ScheduleMapDisplayedMax" : "ScheduleMapDisplayed", d = pg.schedule.city, e = pg.schedule.transport, f = pg.schedule.num, g = pg.schedule.dirType, h = pg.schedule.stopId) : (document.body.className = b ? "MapDisplayedMax" : "MapDisplayed", pg.browserVersion <= 7 && ($("divContent").innerHTML = $("divContent").innerHTML), d = a[1] || cfg.defaultCity, e = a[2] || "", f = a[3] || "", g = a[4] || "", h = a[5] || "");
	if (pg.GMap) {
		if (typeof ti.stops !== "object" || typeof ti.routes !== "object" || typeof pg.GMap.getProjection() == "undefined" || !pg.stopLabelSelected._ready) {
			setTimeout(pg.fMapShow, 200);
			return
		}
		var i, j;
		if (pg.transport == "plan")pg.stopLabelSelected.hide(), pg.fToggleVehicles(-Math.abs(pg.mapShowVehicles)); else {
			pg.fToggleVehicles(pg.mapShowVehicles), h || pg.transport == "stop" && (h = pg.inputStop, pg.mapShowAllStops = !0);
			if (h) {
				j = ti.fGetAnyStopDetails(h), pg.schedule ? A = pg.fUrlSet({schedule: {stopId: j.id}}, !0) : A = pg.fUrlSet({
					transport: "stop",
					stopId: j.id
				}, !0);
				if (typeof j.latAvg == "number" && typeof j.lngAvg == "number") {
					i = new google.maps.LatLng(j.latAvg, j.lngAvg), pg.stopLabelSelected.setContents(j.name, A), pg.stopLabelSelected.setPoint(i), pg.stopLabelSelected.show();
					if (pg.transport == "stop" && cfg.defaultCity === "vilnius" && pg.mapStopForZones != h && !cfg.city.goHomeTimeout) {
						pg.mapStopForZones = h;
						var k = new Date, l = k.getDay() || 7, m = {
							start_stops: h,
							finish_stops: "0;0",
							date: k,
							weekday: l,
							transport: {},
							walk_max: 500,
							callback2: function () {
								pg.clusterManager.refresh()
							}
						};
						setTimeout(function () {
							dijkstra(m, 720, 1)
						}, 100)
					}
				} else pg.stopLabelSelected.hide()
			} else pg.stopLabelSelected.hide()
		}
		if (f || pg.transport == "plan") {
			if (pg.map.city == d && pg.map.transport == e && pg.map.num == f && pg.map.dirType == g) {
				i && !pg.GMap.getBounds().contains(i) && pg.GMap.panTo(i), cfg.isMobilePage && google.maps.event.trigger(pg.GMap, "resize");
				return
			}
			pg.map.city = d, pg.map.transport = e, pg.map.num = f, pg.map.dirType = g, pg.mapStops = {}, pg.mapMarkerStart.setMap(null), pg.mapMarkerFinish.setMap(null);
			while (pg.mapOverlays.length)pg.mapOverlays.pop().setMap(null);
			var n = 999, o = -999, p = 999, q = -999, r = "", s = "";
			if (pg.transport == "plan") {
				var t;
				if (f && pg.optimalResults && pg.optimalResults.length) {
					w = f ? +f - 1 : 0, w >= pg.optimalResults.length && (w = 0), t = pg.optimalResults[w].legs || [], s = i18n.option + " " + (w + 1);
					for (var u = 0; u < pg.optimalResults.length; ++u)r += "<a href=\"#map,,," + (u + 1) + "\"><span class=\"icon icon_narrow" + (u == w ? " icon_checked" : "") + "\"></span>" + i18n.option + " " + (u + 1) + "</a>"
				} else pg.mapShowAllStops = !0, t = [{
					start_stop: ti.fGetAnyStopDetails(pg.inputStart),
					finish_stop: ti.fGetAnyStopDetails(pg.inputFinish)
				}];
				var v;
				for (var w = 0; w <= t.length; ++w) {
					var x, j, y, z;
					w == t.length ? (x = t[w - 1], j = x.finish_stop, z = x.finish_time, typeof j.lat == "number" && typeof j.lng == "number" && (pg.mapMarkerFinish.setPosition(new google.maps.LatLng(j.latAvg || j.lat, j.lngAvg || j.lng)), pg.mapMarkerFinish.setMap(pg.GMap))) : (x = t[w], j = x.start_stop, typeof j.lat == "number" && typeof j.lng == "number" && (w == 0 && (pg.mapMarkerStart.setPosition(new google.maps.LatLng(j.latAvg || j.lat, j.lngAvg || j.lng)), pg.mapMarkerStart.setMap(pg.GMap))), z = x.start_time);
					if (!j || !j.id)continue;
					n > j.lat && (n = j.lat), p > j.lng && (p = j.lng), o < j.lat && (o = j.lat), q < j.lng && (q = j.lng);
					var y = x.route || {}, A, B = v && y.num === v.num && y.transport === v.transport && y.city === v.city;
					z = z && ti.printTime(z) + " " || "", j.id.indexOf(";") < 0 && (x.route ? A = pg.fUrlSet({
						schedule: {
							city: y.city,
							transport: y.transport,
							num: y.num,
							dirType: y.dirType,
							stopId: j.id,
							tripNum: (x.trip_num || -1) + 1
						}
					}, !0) : j.id ? A = "stop/" + j.id + "/map" : A = "map", pg.mapStops[j.id] = {
						lat: j.latAvg || j.lat,
						lng: j.lngAvg || j.lng,
						href: A,
						img: !y.transport && f && (w == 0 || w == t.length) ? "stopGray" : B ? "stop" : "stopOnRoute",
						name: z + j.name
					});
					if (!f || pg.optimalSearchRunning || !pg.optimalResults || !pg.optimalResults.length)continue;
					B || j.id.indexOf(";") < 0 && w !== t.length && (y.transport || w == t.length - 1) && (pg.mapStops["transport" + j.id] = {
						lat: j.latAvg || j.lat,
						lng: j.lngAvg || j.lng,
						href: A,
						img: "MarkerStart",
						name: z + i18n.stop.toLowerCase() + "&nbsp;" + j.name,
						transport: y.transport || "walk",
						num: y.numHTML || ""
					});
					if (w < t.length)if (y.transport) {
						var C = {};
						C[y.dirType] = !0, pg.loadPolyline(y.city, y.transport, y.num, C, x.start_stop.lat, x.start_stop.lng, x.finish_stop.lat, x.finish_stop.lng), v = y;
						if (!isNaN(x.start_pos) && !isNaN(x.finish_pos)) {
							var D = typeof y.times === "string" ? ti.explodeTimes(y.times) : y.times, E = D.workdays.length;
							D = D.times;
							for (var F = x.start_pos; ++F < x.finish_pos;) {
								var j = ti.fGetStopDetails(y.stops[F]), A = pg.fUrlSet({
									schedule: {
										city: y.city,
										transport: y.transport,
										num: y.num,
										dirType: y.dirType,
										stopId: j.id,
										tripNum: (x.trip_num || -1) + 1
									}
								}, !0);
								pg.mapStops[j.id] = {
									lat: j.lat,
									lng: j.lng,
									href: A,
									img: "stop",
									name: ti.printTime(D[x.trip_num + F * E]) + " " + j.name
								}
							}
						}
					} else if (pg.optimalResults) {
						v = null;
						var G = new google.maps.Polyline({
							path: [new google.maps.LatLng(x.start_stop.lat, x.start_stop.lng), new google.maps.LatLng(x.finish_stop.lat, x.finish_stop.lng)],
							strokeColor: "#000000",
							strokeOpacity: .8,
							strokeWeight: 5,
							clickable: !1,
							map: pg.GMap
						});
						pg.mapOverlays.push(G)
					}
				}
			} else if (e) {
				if (pg.mapShowVehicles > 0 || f && pg.mapShowVehicles == -1)pg.fToggleVehicles(Math.abs(pg.mapShowVehicles)), pg.fShowVehicles();
				var H = ti.fGetRoutes(d, e, f, pg.schedule ? g : !1, !0), C = {};
				if (H.length) {
					var I = {};
					for (var w = H.length; --w >= 0;) {
						var y = H[w];
						if (y.routeTag && y.dirType != g)continue;
						C[y.dirType] = !g || y.dirType == g;
						var J = "map," + y.city + "," + y.transport + "," + y.num + "," + y.dirType;
						J = ti.toAscii(J, !0), r = "<a href=\"#" + J + "\"><span class=\"icon icon_narrow" + (y.dirType == g ? " icon_checked" : "") + "\"></span>" + y.name + "</a>" + r;
						if (!C[y.dirType])continue;
						var K = [];
						for (var u = y.stops.length; --u >= 0;) {
							j = ti.fGetStopDetails(y.stops[u]);
							if (!j.lat || !j.lng)continue;
							i = new google.maps.LatLng(j.lat, j.lng), K.push(i);
							var A = pg.fUrlSet({
								schedule: {
									city: y.city,
									transport: y.transport,
									num: y.num,
									dirType: y.dirType,
									stopId: j.id
								}
							}, !0);
							pg.mapStops[j.id] = {
								lat: j.lat,
								lng: j.lng,
								href: A,
								img: "stopOnRoute" + (!g && w ? "2" : ""),
								name: j.name,
								hidden: !0
							}, I[j.name] = j.id, n > j.lat && (n = j.lat), p > j.lng && (p = j.lng), o < j.lat && (o = j.lat), q < j.lng && (q = j.lng), (w == 0 || g) && (u == 0 || u == y.stops.length - 1) && (pg.mapStops[u == 0 ? "MarkerStart" : "MarkerFinish"] = {
								lat: j.lat,
								lng: j.lng,
								href: A,
								img: u == 0 ? "MarkerStart" : "MarkerRed",
								transport: e,
								num: y.numHTML,
								name: (u == 0 ? i18n.stop.toLowerCase() + "&nbsp;" : "") + j.name
							})
						}
						if (cfg.defaultCity === "latvia" && y.stops) {
							var L = "http://routelatvia.azurewebsites.net/?shape=" + y.stops.join(",");
							L = "http://www.stops.lt/latviatest/proxy.php?url=" + encodeURIComponent(L), ti.fDownloadUrl("get", L, function (a) {
								var b = JSON.parse(a);
								b.contents && (b = b.contents), b = b.split(",");
								var c = [];
								for (var d = 0; d < b.length; d += 2) {
									var f = new GLatLng(parseFloat(b[d]), parseFloat(b[d + 1]));
									c.push(f)
								}
								if (c.length > 1) {
									var g = pg.getStyle("." + e), h = d > 0 ? .6 : .8, i = g && g.backgroundColor || "#0000FF", j = 5, k = new GPolyline(c, i, j, h);
									pg.GMap.addOverlay(k), pg.mapOverlays.push(k)
								}
							}, !0)
						}
					}
					for (var M in I)pg.mapStops[I[M]].hidden = !1;
					H.length > 1 && (J = "map," + y.city + "," + y.transport + "," + y.num, r = "<a href=\"#" + J + "\"><span class=\"icon icon_narrow" + (g ? "" : " icon_checked") + "\"></span>" + i18n.mapShowAllDirections + "</a>" + r);
					var N = pg.getStyle("transport_icon_" + e);
					s = "<img class=\"icon icon_narrow\" src=\"" + N + "\"/><span class=\"transfer" + e + "\">&nbsp;" + H[0].numHTML + "</span>";
					if (cfg.defaultCity !== "latvia")if (cfg.defaultCity === "helsinki") {
						var O = pg.getStyle("." + e), P = w > 0 ? .6 : .8, Q = O && O.backgroundColor || "#0000FF", R = 5, G = new GPolyline(K, Q, R, P);
						pg.GMap.addOverlay(G), pg.mapOverlays.push(G)
					} else pg.loadPolyline(d, e, f, C)
				}
			}
			pg.$mapRoutesDropdown && (r ? (r = "<div style=\"float:left; height:17px;\"><a href=\"#\">" + s + "&nbsp;<span class=\"arrow-down\"></span><span id=\"mapRemoveRoute\">&times;</span><!--[if gte IE 7]><!--></a><!--<![endif]--><table class=\"dropdown\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + r + "<a id=\"mapShowStopsNames\" href=\"#map\" style=\"border-top:solid 1px #CCCCCC; margin-top:3px;\"><span class=\"icon icon_narrow stopsnames" + (pg.mapShowStopsNames ? " icon_checked" : "") + "\"></span>" + i18n.mapShowRouteStopsNames + "</a>" + (pg.schedule ? "" : "<a href=\"#map\"><span class=\"icon icon_narrow\"></span>" + i18n.mapClearRoute + "</a>") + "</td></tr></table><!--[if lte IE 6]></a><![endif]--></div>", pg.$mapRoutesDropdown.innerHTML = r, pg.$mapRoutesDropdown.style.display = "") : pg.$mapRoutesDropdown.style.display = "none"), f && pg.clusterManager.hideMarkers(), f && window.setTimeout(function () {
				if (n == o && p == q)pg.GMap.panTo(new google.maps.LatLng(n, p)); else {
					var a = new google.maps.LatLngBounds(new google.maps.LatLng(n, p), new google.maps.LatLng(o, q));
					pg.GMap.fitBounds(a)
				}
			}, 500)
		}
		if (!f) {
			pg.$mapRoutesDropdown && (pg.$mapRoutesDropdown.style.display = "none"), pg.transport !== "plan" && (pg.mapMarkerStart.setMap(null), pg.mapMarkerFinish.setMap(null), pg.mapStops = {});
			while (pg.mapOverlays.length)pg.mapOverlays.pop().setMap(null);
			pg.map = {}, pg.clusterManager.draw(), h && j && j.id && typeof j.lat == "number" && typeof j.lng == "number" ? (pg.mapStops[h] = {
				lat: j.latAvg,
				lng: j.lngAvg,
				href: "stop/" + h,
				img: "stopOnRoute",
				name: j.name
			}, i = new google.maps.LatLng(j.lat, j.lng)) : i = null, i ? window.setTimeout(function () {
				pg.GMap.panTo(i)
			}, 300) : pg.clusterManager.refresh()
		}
		setTimeout(function () {
			google.maps.event.trigger(pg.GMap, "resize")
		}, 100)
	} else pg.GMap === null ? (pg.GMap = !1, $("divMap").innerHTML = "<div style=\"margin:10px;\">" + i18n.loadingMap + "</div>", ($("preload") || {}).innerHTML = "<img src=\"" + pg.imagesFolder + "stop.png\" width=\"1\" height=\"1\" /><img src=\"" + pg.imagesFolder + "cluster.png\" width=\"1\" height=\"1\" /><img src=\"" + pg.imagesFolder + "stopGray.png\" width=\"1\" height=\"1\" /><img src=\"" + pg.imagesFolder + "stopGray.png\" width=\"1\" height=\"1\" />", pg.loadGoogleMapsScript(pg.GMapScriptLoadedMap)) : pg.GMap !== !1 && pg.GMapScriptLoadedMap()
}, pg.GMapScriptLoadedMap = function () {
	if (!google) {
		alert("Sorry, the Google Maps API is not compatible with this browser");
		return !1
	}
	if (typeof ti.stops !== "object" || typeof ti.routes !== "object")setTimeout(pg.GMapScriptLoadedMap, 200); else {
		pg.storeStyles(), pg.GMap = 0;
		if ((document.body.className || "").indexOf("Map") < 0)return;
		var a, b = cfg.cities[pg.city] || {};
		b.streetMap = b.streetMap || "GMaps";
		var c = {
			zoom: b.zoom || 12,
			center: new google.maps.LatLng(b.lat || 59.43923, b.lng || 24.7588),
			scaleControl: !1,
			panControl: !1,
			mapTypeControl: !0,
			mapTypeControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			streetViewControl: !0,
			streetViewControlOptions: {position: google.maps.ControlPosition.LEFT_BOTTOM},
			zoomControl: !0,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.DEFAULT,
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		v3.mapTypeIds = [];
		for (var d in google.maps.MapTypeId)v3.mapTypeIds.push(google.maps.MapTypeId[d]);
		b.streetMap.indexOf("OSM") >= 0 && (v3.mapTypeIds.push("OSM"), c.mapTypeControlOptions.mapTypeIds = v3.mapTypeIds, c.mapTypeId = "OSM");
		var e = pg.GMap = new google.maps.Map($("divMap"), c);
		b.streetMap.indexOf("OSM") >= 0 && e.mapTypes.set("OSM", new google.maps.ImageMapType({
			getTileUrl: function (a, b) {
				return "http://tile.openstreetmap.org/" + b + "/" + a.x + "/" + a.y + ".png"
			}, tileSize: new google.maps.Size(256, 256), name: "OpenStreetMap", maxZoom: 18
		})), v3.setMap(e);
		var f = 150;
		if (b.streetMap != "OSMlocal") {
			var g;
			cfg.isMobilePage && (f = 150, pg.mapShowVehicles = 2)
		}
		pg.$mapClose = document.createElement("div"), pg.$mapClose.id = "divMapHide", pg.$mapClose.className = "map_hide", pg.$mapClose.style.left = "5px", pg.$mapClose.style.top = "5px", e.getDiv().appendChild(pg.$mapClose), pg.$mapClose.addEventListener("click", function (a) {
			pg.divMapHide_Click(a)
		}), pg.$mapShowAllStops = document.createElement("div"), pg.$mapShowAllStops.id = "divMapShowAllStops", pg.$mapShowAllStops.style.right = "5px", pg.$mapShowAllStops.style.top = "0px", pg.$mapShowAllStops.className = "map_button", pg.$mapShowAllStops.innerHTML = "<div id=\"mapShowAllStops\" class=\"map_button_icon\" title=\"" + i18n.mapShowAllStops + "\"></div>", pg.$mapYouAreHere = document.createElement("div"), pg.$mapYouAreHere.id = "divMapYouAreHere", pg.$mapYouAreHere.style.right = cfg.city.urlGPS ? "128px" : "87px", pg.$mapYouAreHere.style.top = "0px", pg.$mapYouAreHere.className = "map_button", pg.$mapYouAreHere.innerHTML = "<div id=\"mapYouAreHere\" class=\"map_button_icon\" title=\"Nustatyti dabartinД™ vietД…!\"></div>", e.getDiv().appendChild(pg.$mapYouAreHere), pg.$mapYouAreHere.addEventListener("click", function (a) {
			var b = window.setInterval(function () {
				jq("#mapYouAreHere").toggleClass("wait")
			}, 500);
			navigator.geolocation && navigator.geolocation.getCurrentPosition(function (a) {
				window.clearInterval(b), jq("#mapYouAreHere").removeClass("wait");
				var c = new google.maps.LatLng(a.coords.latitude, a.coords.longitude), d = {
					coords: {
						latitude: c.lat(),
						longitude: c.lng()
					}
				};
				pg.storeMyLocation(d);
				if (typeof pg.GMap == "object" && typeof pg.youAreHere == "object") {
					pg.youAreHere.setPosition(c), pg.youAreHere.setMap(pg.GMap);
					var e = new google.maps.LatLngBounds(new google.maps.LatLng(c.lat() - .003, c.lng() - .003), new google.maps.LatLng(c.lat() + .003, c.lng() + .003));
					pg.GMap.fitBounds(e), window.setTimeout(function () {
						k({pixel: v3.fromLatLngToContainerPixel(c), latLng: c})
					}, 300)
				}
			}, function (a) {
				window.clearInterval(b), jq("#mapYouAreHere").removeClass("wait"), typeof pg.GMap == "object" && typeof pg.youAreHere == "object" && pg.youAreHere.setMap(null), alert("Can not determine location!")
			}, {maximumAge: 6e5, timeout: 5e3, enableHighAccuracy: !0}), window.setTimeout(function () {
				window.clearInterval(b), jq("#mapYouAreHere").removeClass("wait")
			}, 1e4)
		}), b.streetMap !== "OSMlocal" && (f += 27, cfg.city.showTraffic && (pg.$mapShowTraffic = document.createElement("div"), pg.$mapShowTraffic.id = "divMapShowTraffic", pg.$mapShowTraffic.style.right = "46px", pg.$mapShowTraffic.style.top = "0px", pg.$mapShowTraffic.className = "map_button", pg.$mapShowTraffic.innerHTML = "<div id=\"mapShowTraffic\" class=\"map_button_icon\" title=\"" + i18n.mapShowTraffic + "\"></div>", f += 27, e.getDiv().appendChild(pg.$mapShowTraffic)), cfg.city.urlGPS && (pg.$mapShowVehicles = document.createElement("div"), pg.$mapShowVehicles.id = "divMapShowVehicles", pg.$mapShowVehicles.style.right = "87px", pg.$mapShowVehicles.style.top = "0px", pg.$mapShowVehicles.className = "map_button", pg.$mapShowVehicles.innerHTML = "<div id=\"mapShowVehicles\" class=\"map_button_icon\" title=\"" + i18n.mapShowVehicles + "\"></div>", f += 27, e.getDiv().appendChild(pg.$mapShowVehicles), pg.$mapShowVehicles.addEventListener("click", function (a) {
			pg.mapShowVehicles > 0 ? pg.hashForMap == "map" ? pg.fToggleVehicles(-1) : pg.fToggleVehicles(0) : pg.hashForMap == "map" ? pg.fToggleVehicles(2) : pg.fToggleVehicles(1);
			return pg.cancelEvent(a)
		}))), e.getDiv().appendChild(pg.$mapShowAllStops), typeof pg.mapShowAllStops == "undefined" && (pg.mapShowAllStops = !1), cfg.isMobilePage || (pg.$mapRoutesDropdown = document.createElement("div"), pg.$mapRoutesDropdown.className = "dropdown", pg.$mapRoutesDropdown.style.position = "absolute", pg.$mapRoutesDropdown.style.top = "10px", pg.$mapRoutesDropdown.style.right = "300px", e.getDiv().appendChild(pg.$mapRoutesDropdown)), pg.$mapMenu = document.createElement("div"), pg.$mapMenu.style.display = "none", pg.$mapMenu.style.position = "absolute", pg.$mapMenu.style.zIndex = 2, pg.$mapMenu.className = "mapMenu", e.getDiv().appendChild(pg.$mapMenu), pg.mapShowTraffic && pg.fShowTraffic(!0), pg.mapShowVehicles > 0 && pg.fToggleVehicles(pg.mapShowVehicles);
		var h;
		google.maps.event.addDomListener(e, "rightclick", function (a) {
			h = null, $("divSuggestedStops").style.display = "none", a || (a = window.event);
			var b = a && (a.target || a.srcElement);
			if (!b || b.id == "mapShowAllStops")return pg.cancelEvent(a);
			var c = b && (b.tagName || "").toLowerCase() || "";
			b && c !== "a" && c !== "img" && (b = b.parentNode, c = b && (b.tagName || "").toLowerCase() || "");
			var d = b && (c === "a" && b.href || c === "img" && b.id || "") || "";
			if (b && b.parentNode && (b.parentNode.tagName || "").toLowerCase() == "td")return pg.cancelEvent(a);
			if (d && d.indexOf("#") >= d.length - 1)return pg.cancelEvent(a);
			d ? h = b : h = {}
		});
		var j = e.getStreetView();
		google.maps.event.addListener(j, "visible_changed", function () {
			j.getVisible() ? (($("divMapHide") || {style: {}}).style.display = "none", ($("divMapShowTraffic") || {style: {}}).style.display = "none", ($("divMapShowVehicles") || {style: {}}).style.display = "none", ($("divMapShowAllStops") || {style: {}}).style.display = "none", ($("divMapYouAreHere") || {style: {}}).style.display = "none") : (($("divMapHide") || {style: {}}).style.display = "block", ($("divMapShowTraffic") || {style: {}}).style.display = "block", ($("divMapShowVehicles") || {style: {}}).style.display = "block", ($("divMapShowAllStops") || {style: {}}).style.display = "block", ($("divMapYouAreHere") || {style: {}}).style.display = "block")
		});
		var k = function (a) {
			var b = a.pixel.x, c = a.pixel.y, d = Math.round(1e6 * a.latLng.lat()) / 1e6 + ";" + Math.round(1e6 * a.latLng.lng()) / 1e6;
			pg.openMapInfoWindow(b, c, "", d)
		};
		google.maps.event.addListener(e, "rightclick", k);
		function l(a, b) {
			this.length_ = b;
			var c = this;
			c.map_ = a, c.timeoutId_ = null, google.maps.event.addListener(a, "mousedown", function (a) {
				c.onMouseDown_(a)
			}), google.maps.event.addListener(a, "mouseup", function (a) {
				c.onMouseUp_(a)
			}), google.maps.event.addListener(a, "drag", function (a) {
				c.onMapDrag_(a)
			})
		}

		l.prototype.onMouseUp_ = function (a) {
			clearTimeout(this.timeoutId_)
		}, l.prototype.onMouseDown_ = function (a) {
			pg.hideMapInfoWindow(), clearTimeout(this.timeoutId_);
			var b = this.map_, c = a;
			this.timeoutId_ = setTimeout(function () {
				google.maps.event.trigger(b, "longpress", c)
			}, this.length_)
		}, l.prototype.onMapDrag_ = function (a) {
			pg.hideMapInfoWindow(), clearTimeout(this.timeoutId_)
		}, new l(e, 500), google.maps.event.addListener(e, "longpress", k), google.maps.event.addDomListener(e.getDiv(), "click", function (a) {
			pg.timeOfActivity = (new Date).getTime(), pg.inputSuggestedStops_Blur(), a = a || window.event;
			var b = a && (a.target || a.srcElement);
			pg.realTimeDepartures.vehicleID = b && b.getAttribute && b.getAttribute("data-vehicle-id");
			if (pg.realTimeDepartures.vehicleID) {
				pg.realTimeDepartures.mapStop = "", pg.realTimeDepartures.vehicleTransport = b.getAttribute("data-transport"), pg.realTimeDepartures.vehicleRouteNum = b.getAttribute("data-route"), pg.realTimeDepartures.$mapPopup = {}, pg.fProcessVehicleDepartures();
				return pg.cancelEvent(a)
			}
			if (b.id == "mapShowTraffic") {
				pg.fShowTraffic(!pg.mapShowTraffic);
				return pg.cancelEvent(a)
			}
			if (b.id == "mapShowVehicles") {
				pg.mapShowVehicles > 0 ? pg.hashForMap == "map" ? pg.fToggleVehicles(-1) : pg.fToggleVehicles(0) : pg.hashForMap == "map" ? pg.fToggleVehicles(2) : pg.fToggleVehicles(1);
				return pg.cancelEvent(a)
			}
			if (b.id == "mapShowAllStops") {
				pg.mapShowAllStops = !pg.mapShowAllStops, pg.clusterManager.refresh();
				return pg.cancelEvent(a)
			}
			if (b.id == "mapShowStopsNames") {
				pg.mapShowStopsNames = !pg.mapShowStopsNames;
				var c = pg.$mapRoutesDropdown.innerHTML.replace("stopsnames icon_checked", "stopsnames");
				pg.$mapRoutesDropdown.innerHTML = pg.mapShowStopsNames ? c.replace("stopsnames", "stopsnames icon_checked") : c, pg.clusterManager.refresh();
				return pg.cancelEvent(a)
			}
			var d = b && (b.tagName || "").toLowerCase() || "", e;
			b && b.id == "mapRemoveRoute" ? (e = "#map", (pg.mapShowVehicles < 0 || pg.mapShowVehicles == 1) && pg.fToggleVehicles(-Math.abs(pg.mapShowVehicles))) : (b && d !== "a" && d !== "img" && (b = b.parentNode), e = b && (d === "a" && b.href || d === "img" && b.id || "") || "");
			if (e.indexOf("#") < 0 || e.indexOf("#") >= e.length - 1)return pg.cancelEvent(a);
			var f = pg.fUrlParse(e);
			if (f.schedule && d !== "img")e = pg.fUrlSet({schedule: f.schedule}); else if (f.transport == "stop" || f.schedule) {
				var g = f.schedule && f.schedule.stopId || f.inputStop;
				if ((b.className || "").toLowerCase().indexOf("cluster") < 0)if (d === "img") {
					if (g) {
						pg.realTimeDepartures.mapStop = g, pg.realTimeDepartures.vehicleID = null;
						var h = [];
						g.indexOf(";") == -1 && h.push("<br/><a href=\"#stop/" + g + (cfg.isMobilePage ? "" : "/map") + "\"><span class=\"icon icon_narrow\"></span>" + i18n.mapShowRoutesFromStop + "</a>"), h.push("<a href=\"#stop/" + g + "/map\" class=\"cluster\"><span class=\"icon icon_narrow\"></span>" + i18n.mapZoomIn + "</a>");
						var i = ti.fGetRoutesAtStop(g, !1, !0), c = [], j = null, k = 16, l = 1, m = null;
						for (var n = 0; n < i.length; n++) {
							route = i[n], j != route.transport && (j = route.transport, c.push("<br/><span class=\"icon icon_narrow icon_" + route.transport + "\"></span>"), l = 1, m = null);
							var o = {
								id: route.id,
								city: route.city,
								transport: route.transport,
								num: ti.toAscii(route.num, !0),
								dirType: route.dirType,
								routeTag: route.stopId,
								stopId: route.stopId
							};
							if (o.num === m)continue;
							m = o.num;
							var p = pg.fUrlSet({schedule: o}, !0), q = "<a class=\"hover " + (cfg.defaultCity == "riga" ? "activetransfer " : "transfer") + route.transport + "\" href=\"#" + p + "\" title=\"" + (route.name || "").replace(/"/g, "") + "\">" + route.numHTML.replace(/\s/g, "&nbsp;") + "</a> ";
							c.push(q), l % k || c.push("<br/><span style=\"margin-left:22px;\"></span>"), l += 1
						}
						var r = ti.fGetAnyStopDetails(g), s = r.name, t = "<span class=\"baloon_title\">" + s + "</span>" + c.join("") + h.join(""), u = v3.fromLatLngToContainerPixel(new google.maps.LatLng(r.lat, r.lng));
						pg.openMapInfoWindow(u.x || a.clientX, u.y || a.clientY, t, g)
					}
				} else f.transport == "stop" && (pg.hashForMap = cfg.isMobilePage ? "" : "map", pg.map = {}, pg.fTabStop_Click(f.inputStop)); else {
					var v = g.split(","), w = new google.maps.LatLngBounds;
					for (var x = 0; x < v.length; x++) {
						var y = ti.fGetStopDetails(v[x]);
						y.lat && y.lng && w.extend(new google.maps.LatLng(y.lat, y.lng))
					}
					pg.GMap.fitBounds(w)
				}
			} else f.transport == "plan" ? (pg.hashForMap = f.hashForMap, pg.map = {}, pg.optimalResults = null, pg.fTabPlanner_Click(f.inputStart, f.inputFinish)) : (pg.hashForMap = f.hashForMap, pg.hashForMap == "map" && (pg.mapShowAllStops = !0), pg.fUrlSet(pg));
			return pg.cancelEvent(a)
		}), google.maps.event.addDomListener(e.getDiv(), "mouseout", function (a) {
			a || (a = window.event);
			var b = a && (a.target || a.srcElement), c = e.getDiv();
			b && b == c, b = a.relatedTarget || a.toElement;
			while (b && (b.tagName || "").toLowerCase() != "body") {
				if (b === c)return;
				b = b.parentNode
			}
		}), ELabel = function (a, b, c, d, e, f, g, h) {
			this.div_ = null, this.map_ = a, this.point = b, this.html = c, this.href = d, this.classname = e || "", this.pixelOffset = f || new google.maps.Size(0, 0), g && (g < 0 && (g = 0), g > 100 && (g = 100)), this.percentOpacity = g, this.overlap = h || !1, this.hidden = !1, this._ready = !1
		}, ELabel.prototype = new google.maps.OverlayView, ELabel.prototype.onAdd = function () {
			var a = document.createElement("a");
			a.style.position = "absolute", a.className = this.classname, a.href = "#" + this.href, this.div_ = a, this.percentOpacity && (typeof a.style.filter == "string" && (a.style.filter = "alpha(opacity:" + this.percentOpacity + ")"), typeof a.style.KHTMLOpacity == "string" && (a.style.KHTMLOpacity = this.percentOpacity / 100), typeof a.style.MozOpacity == "string" && (a.style.MozOpacity = this.percentOpacity / 100), typeof a.style.opacity == "string" && (a.style.opacity = this.percentOpacity / 100));
			if (this.overlap) {
				var b = 1e3 * (90 - this.point.lat());
				this.div_.style.zIndex = parseInt(b)
			}
			this.hidden && this.hide();
			var c = this.getPanes();
			c.overlayMouseTarget.style.zIndex = 102, c.overlayMouseTarget.appendChild(a), this._ready = !0
		}, ELabel.prototype.onRemove = function () {
			this.div_.parentNode.removeChild(this.div_)
		}, ELabel.prototype.draw = function (a) {
			var b = this.getProjection(), c = b.fromLatLngToDivPixel(this.point), d = parseInt(this.div_.clientHeight);
			this.div_.style.left = c.x + this.pixelOffset.width + "px", this.div_.style.top = c.y + this.pixelOffset.height - d + "px"
		}, ELabel.prototype.show = function () {
			this.div_ && (this.div_.style.display = "", this.draw()), this.hidden = !1
		}, ELabel.prototype.hide = function () {
			this.div_ && (this.div_.style.display = "none"), this.hidden = !0
		}, ELabel.prototype.copy = function () {
			return new ELabel(this.point, this.html, this.classname, this.pixelOffset, this.percentOpacity, this.overlap)
		}, ELabel.prototype.isHidden = function () {
			return this.hidden
		}, ELabel.prototype.supportsHide = function () {
			return !0
		}, ELabel.prototype.setContents = function (a, b) {
			this.div_.innerHTML = this.html = a, typeof b != "undefined" && (this.div_.href = "#" + b), this.draw(!0)
		}, ELabel.prototype.setPoint = function (a) {
			this.point = a;
			if (this.overlap) {
				var b = GOverlay.getZIndex(this.point.lat());
				this.div_.style.zIndex = b
			}
			this.draw(!0)
		}, ELabel.prototype.setOpacity = function (a) {
			a && (a < 0 && (a = 0), a > 100 && (a = 100)), this.percentOpacity = a, this.percentOpacity && (typeof this.div_.style.filter == "string" && (this.div_.style.filter = "alpha(opacity:" + this.percentOpacity + ")"), typeof this.div_.style.KHTMLOpacity == "string" && (this.div_.style.KHTMLOpacity = this.percentOpacity / 100), typeof this.div_.style.MozOpacity == "string" && (this.div_.style.MozOpacity = this.percentOpacity / 100), typeof this.div_.style.opacity == "string" && (this.div_.style.opacity = this.percentOpacity / 100))
		}, ELabel.prototype.getPoint = function () {
			return this.point
		}, ClusterManager = function (a, b) {
			this._map = a, this._mapMarkers = [], this._markersHidden = !1, this._ready = !1, b = b || {}, this.fitMapToMarkers = b.fitMapToMarkers === !0, b.fitMapMaxZoom && (this.fitMapMaxZoom = b.fitMapMaxZoom), this.clusterMaxZoom = b.clusterMaxZoom ? b.clusterMaxZoom : 99, b.markers && (this._mapMarkers = b.markers), this.borderPadding = b.borderPadding || 256, this.intersectPadding = b.intersectPadding || 0, this.clusteringEnabled = b.clusteringEnabled !== !1, this.ZoomLevel = this._map.getZoom()
		}, ClusterManager.prototype = new google.maps.OverlayView, ClusterManager.prototype.onAdd = function () {
			typeof this._div == "undefined" && (this._div = document.createElement("div"), this._div.id = "ClusterManagerStopsPane", this.getPanes().overlayMouseTarget.style.zIndex = 103, this.getPanes().overlayMouseTarget.appendChild(this._div));
			var a = this._mapMarkers;
			for (var b = a.length; --b >= 0;) {
				var c = a[b], d = v3.fromLatLngToDivPixel(new google.maps.LatLng(c.lat, c.lng), 19);
				c._x = d.x, c._y = d.y
			}
			a.sort(function (a, b) {
				return b._y - a._y
			}), this._mapMarkers = a, this._ready = !0
		}, ClusterManager.prototype.showLayer = function () {
			this._div && (this._div.style.display = "block")
		}, ClusterManager.prototype.hideLayer = function () {
			this._div && (this._div.style.display = "none")
		}, ClusterManager.prototype.refresh = function () {
			if (this._ready) {
				pg.timeOfActivity = (new Date).getTime(), pg.toggleClass($("divMapShowAllStops"), "pressed", pg.mapShowAllStops);
				var a = this._markersHidden ? "Gray" : "", b = this._map, c = this.getProjection(), d = [], e = v3.fromLatLngToDivPixel(this._map.getBounds().getSouthWest(), 19), f = v3.fromLatLngToDivPixel(this._map.getBounds().getNorthEast(), 19), g = this._mapMarkers, h = pg.mapStops, j, k, l, m = {};
				if (pg.mapShowAllStops) {
					var n = 19 - this._map.getZoom(), o = 1 << n + 8, p = e.x - o, q = f.x + o, s = e.y + o, t = f.y - o, u = cfg.defaultCity === "xxxkautra" ? 1 : 12, v = this._map.getZoom(), w = this.clusteringEnabled && v <= this.clusterMaxZoom, x = cfg.defaultCity === "vilnius" && pg.transport === "stop";
					o = 1 << n + 3 + (v < 12 ? 1 : 0);
					for (i = g.length; --i >= 0;) {
						j = g[i];
						var y = j._y;
						if (y < t || !j.name)continue;
						if (y > s)break;
						var z = j._x;
						if (z >= p && z <= q) {
							if (j.id in m)continue;
							var A = z, B = y, C = [], D = [], E = 1, F = j.rideEnd - j.rideStart;
							if (w) {
								var G = h[j.id], H = j.name;
								H.length > 1 && !isNaN(H.charAt(H.length - 1)) && (H = H.substring(0, H.length - 1));
								for (var I = i; --I >= 0;) {
									var J = g[I];
									if (J._y > B + o)break;
									if (J.id in m)continue;
									J._x <= A + o && J._x >= A - o && (v < u || J.name.indexOf(H) == 0) && (G = G || h[J.id], m[J.id] = !0, C.push(J.id), D.push(J.name), F < j.rideEnd - j.rideStart && J.time < 4320 && (F = j.rideEnd - j.rideStart), E++, A = (z += J._x) / E, B = (y += J._y) / E)
								}
							}
							if (E > 1) {
								C.push(j.id), r = v3.fromDivPixelToLatLng(new google.maps.Point(A, B), 19), k = c.fromLatLngToDivPixel(r), cfg.isMobilePage && (k.x = k.x - 10, k.y = k.y - 10);
								var K, L = 1;
								if (v < 12) {
									D.sort();
									for (var I = D.length; --I > 0;)D[I] != D[I - 1] && ++L;
									K = L > 2 ? i18n.totalStops + ": " + L : (D[0] + (L > 1 ? ", " + D[D.length - 1] : "")).replace(/"/g, "")
								} else K = H.replace(/"/g, "");
								if (!G || L > 1)x && F >= 0 && (a = F <= 30 ? "30" : F <= 60 ? "60" : "", K = K + ", " + i18n.ride + " " + F + " " + i18n.minutesShort), d.push((L > 1 ? "<img class=\"cluster\" style=\"width:9px; height:9px;" : "<img style=\"width:8px; height:8px;") + " cursor:pointer; vertical-align:top; position:absolute;  top:" + (k.y - 4) + "px; left:" + (k.x - 4) + "px;\" id=\"#stop/" + C.join(",") + "/map\" src=\"" + pg.imagesFolder + (L > 1 ? "cluster" : "stop") + a + ".png\" title=\"" + (window.chrome ? "" : K) + "\" />")
							} else if (!G) {
								var K = window.chrome ? "" : (j.name || "").replace(/"/g, "");
								x && F >= 0 && (a = F <= 30 ? "30" : F <= 60 ? "60" : "", K = K + ", " + i18n.ride + " " + F + " " + i18n.minutesShort), k = c.fromLatLngToDivPixel(new google.maps.LatLng(j.lat, j.lng)), cfg.isMobilePage && (k.x = k.x - 10, k.y = k.y - 10), d.push("<img id=\"#stop/" + j.id + "/map\" style=\"cursor:pointer; vertical-align:top; position:absolute; width:8px; height:8px; top:" + (k.y - 4) + "px; left:" + (k.x - 4) + "px;\" src=\"" + pg.imagesFolder + "stop" + a + ".png\" title=\"" + K + "\" />")
							}
						}
					}
				}
				pg.mapLabelHeight = pg.mapLabelHeight || parseInt(pg.stopLabelSelected.div_.clientHeight, 10);
				for (l in h) {
					j = h[l], k = c.fromLatLngToDivPixel(new google.maps.LatLng(j.lat, j.lng)), cfg.isMobilePage && (k.x = k.x - 10, k.y = k.y - 10);
					var K = pg.browserVersion < 7 && (!pg.mapShowStopsNames || j.hidden) ? " title=\"" + j.name.replace(/"/g, "") + "\"" : "", M = "";
					if (j.img == "MarkerStart") {
						var N = pg.getStyle("transport_icon_" + j.transport);
						cfg.isMobilePage && (k.x = k.x + 10, k.y = k.y + 10), d.push("<a href=\"#" + j.href + "\" class=\"label_transport\" style=\"position:absolute; left:" + (k.x + 11) + "px; top:" + (k.y - 29) + "px;\"><img class=\"icon_narrow\" src=\"" + N + "\" />" + (j.num && "<span class=\"transfer" + j.transport + "\" style=\"line-height:18px; vertical-align:top;\">" + j.num + "</span>&nbsp;") + "<span" + (pg.mapShowStopsNames ? "" : " class=\"unhide\"") + " style=\"line-height:18px; vertical-align:top; border:0 none;\">" + j.name + "&nbsp;</span></a><img src=\"" + pg.imagesFolder + "tip.png\" class=\"tip\" style=\"position:absolute; z-index:105; left:" + (k.x + 4) + "px; top:" + (k.y - 11) + "px;\" />")
					} else j.img == "MarkerRed" ? d.push("<a class=\"mapStopOnRoute\" href=\"#" + j.href + "\" style=\"position:absolute; left:" + (k.x - 6) + "px; top:" + (k.y - 20) + "px;\">") : (j.img || "").indexOf("stopOnRoute") < 0 ? j.img && d.push("<a class=\"mapStop\" href=\"#" + j.href + "/map\" style=\"position:absolute; left:" + (k.x - 4) + "px; top:" + (k.y - 4) + "px;\">") : d.push("<a class=\"mapStopOnRoute\" href=\"#" + j.href + "\" style=\"position:absolute; left:" + (k.x - 5) + "px; top:" + (k.y - 5) + "px;\">");
					j.img != "MarkerStart" && (d.push("<img id=\"#" + j.href + "/map\" src=\"" + pg.imagesFolder + j.img + ".png\"" + K + " style=\"vertical-align:top;\" /></a>"), K || (cfg.isMobilePage && (k.x = k.x + 10, k.y = k.y + 10), d.push("<a href=\"#" + j.href + "\" style=\"position:absolute; left:" + (k.x + 4) + "px; top:" + (k.y - 4 - pg.mapLabelHeight) + "px;\" class=\"mapStopName" + (pg.mapShowStopsNames && !j.hidden ? "" : "Hidden") + "\">" + j.name + "</a>")))
				}
				this._div.innerHTML = d.join("")
			} else window.setTimeout(function () {
				pg.clusterManager.refresh()
			}, 100)
		}, ClusterManager.prototype.onRemove = function () {
			this._div.innerHTML = "", this._mapMarkers = []
		}, ClusterManager.prototype.hideMarkers = function () {
			this._markersHidden || (this._markersHidden = !0, this.refresh())
		}, ClusterManager.prototype.draw = function () {
			this._markersHidden !== !1 && (this._markersHidden = !1, this.refresh())
		}, pg.hideMapInfoWindow = function () {
			pg.$mapMenu && (pg.$mapMenu.style.display = "none")
		}, pg.openMapInfoWindow = function (a, b, c, d) {
			var e = ["<div class=\"content\">", c];
			if (typeof d != "undefined") {
				var f = pg.fUrlSet({
					transport: "plan",
					inputStart: d,
					hashForMap: "map"
				}, !0), g = pg.fUrlSet({transport: "plan", inputFinish: d, hashForMap: "map"}, !0);
				e.push("<div class=\"a\" id=\"start-set\"><span class=\"icon icon_stopGreen\"></span>" + (d.indexOf(";") == -1 ? i18n.mapDirectionsFromStop : i18n.mapDirectionsFromHere) + "</div>"), e.push("<div class=\"a\" id=\"finish-set\"><span class=\"icon icon_stopRed\"></span>" + (d.indexOf(";") == -1 ? i18n.mapDirectionsToStop : i18n.mapDirectionsToThere) + "</div>")
			}
			e.push("</div>"), pg.$mapMenu.innerHTML = e.join(""), typeof d != "undefined" && (jq("#start-set").bind("click", function () {
				pg.hideMapInfoWindow(), pg.inputStart = "", pg.loadedPlannerParams = "", pg.map.num = "", pg.optimalResults = null, pg.fUrlSet({
					transport: "plan",
					inputStart: d
				})
			}), jq("#finish-set").bind("click", function () {
				pg.hideMapInfoWindow(), pg.inputFinish = "", pg.loadedPlannerParams = "", pg.map.num = "", pg.optimalResults = null, pg.fUrlSet({
					transport: "plan",
					inputFinish: d
				})
			})), pg.$mapMenu.style.display = "block", pg.$mapMenu.style.left = a + "px", pg.$mapMenu.style.top = b + "px";
			var h = pg.GMap.getDiv(), i = pg.$mapMenu.offsetWidth, j = pg.$mapMenu.offsetHeight, k = i / 2;
			pg.$mapMenu.style.top = pg.$mapMenu.offsetTop - j + "px", pg.$mapMenu.style.left = pg.$mapMenu.offsetLeft - k + "px";
			if (pg.$mapMenu.offsetLeft < 0)k = k + pg.$mapMenu.offsetLeft - 5, pg.$mapMenu.style.left = "5px"; else if (pg.$mapMenu.offsetLeft + i > h.offsetWidth) {
				var l = pg.$mapMenu.offsetLeft + i - h.offsetWidth;
				k = k + l + 5, pg.$mapMenu.style.left = pg.$mapMenu.offsetLeft - l - 5 + "px"
			}
			var m = document.createElement("div");
			m.style.left = k - 7 + "px", m.className = "baloon_arrow", pg.$mapMenu.offsetTop < 0 && (pg.$mapMenu.style.top = pg.$mapMenu.offsetTop + pg.$mapMenu.offsetHeight + "px", m.className = "baloon_arrow top", pg.$mapMenu.insertBefore(m, pg.$mapMenu.firstChild)), pg.$mapMenu.appendChild(m)
		}, pg.splitEncodedPolyline = function (a, b, c, d, e, f) {
			var g = a.length, h = 0, i = [], j = 0, k = 0, l = Number.POSITIVE_INFINITY, m, n, o = 0, p = 0, q = 0;
			c *= 1e5, e *= 1e5, d *= 1e5, f *= 1e5;
			while (h < g) {
				var r, s = 0, t = 0;
				do r = a.charCodeAt(h++) - 63, t |= (r & 31) << s, s += 5; while (r >= 32);
				var u = t & 1 ? ~(t >> 1) : t >> 1;
				j += u, s = 0, t = 0;
				do r = a.charCodeAt(h++) - 63, t |= (r & 31) << s, s += 5; while (r >= 32);
				var v = t & 1 ? ~(t >> 1) : t >> 1;
				k += v, m = (j - c) * (j - c) + (k - d) * (k - d), l > m && (l = m, o = p = q, n = Number.POSITIVE_INFINITY), m = (j - e) * (j - e) + (k - f) * (k - f), n > m && (n = m, p = q), i[q++] = j, i[q++] = k
			}
			var w = 0, x = 0, y = [];
			h = o;
			while (h <= p)j = i[h++], k = i[h++], y.push(pg.encodeNumber(j - w), pg.encodeNumber(k - x)), w = j, x = k;
			o /= 2, p /= 2;
			var z = "R" + (o < p ? b.substring(o + 1, p) + "R" : "");
			return {points: y.join(""), levels: z}
		}, pg.encodeNumber = function (a) {
			a = a << 1, a < 0 && (a = ~a);
			var b = "";
			while (a >= 32)b += String.fromCharCode((32 | a & 31) + 63), a >>= 5;
			b += String.fromCharCode(a + 63);
			return b
		}, pg.loadPolyline = function (a, b, c, d, e, f, g, h) {
			var i = cfg.city.datadir + "/" + ti.toAscii([a, b, c].join("_"), !0) + ".txt";
			ti.fDownloadUrl("get", i, function (a) {
				a.indexOf("\r\n") < 0 ? a = a.split("\n") : a = a.split("\r\n");
				var c = pg.getStyle("." + b), i = .8;
				for (var j = 2; j < a.length; j += 3) {
					if (!d[a[j - 2]])continue;
					var k = {points: a[j - 1], levels: a[j]};
					e && f && (k = pg.splitEncodedPolyline(k.points, k.levels, e, f, g, h)), k.color = c && (c.backgroundColor || c.color) || "#0000FF", k.opacity = i, i = .6, k.weight = 5, k.zoomFactor = 2, k.numLevels = 20;
					var l = google.maps.geometry.encoding.decodePath(k.points), m = new google.maps.Polyline({
						path: l,
						strokeColor: k.color,
						strokeOpacity: k.opacity,
						strokeWeight: k.weight,
						clickable: !1,
						map: pg.GMap
					});
					pg.mapOverlays.push(m)
				}
			})
		}, pg.stopLabelSelected = new ELabel(pg.GMap, new google.maps.LatLng(b.lat, b.lng), cfg.defaultCity, "map", "mapStopSelected", new google.maps.Size(4, -4)), pg.stopLabelSelected.setMap(pg.GMap), pg.stopLabelSelected.hide(), pg.mapMarkerStart = new google.maps.Marker({
			position: new google.maps.LatLng(0, 0),
			icon: pg.imagesFolder + "MarkerStart.png",
			title: i18n.mapDragToChangeStart,
			draggable: !0,
			dragCrossMove: !1,
			bouncy: !1,
			zIndexProcess: function (a, b) {
				return 104
			}
		}), pg.mapMarkerFinish = new google.maps.Marker({
			position: new google.maps.LatLng(0, 0),
			icon: pg.imagesFolder + "MarkerFinish.png",
			title: i18n.mapDragToChangeFinish,
			draggable: !0,
			dragCrossMove: !1,
			bouncy: !1,
			zIndexProcess: function (a, b) {
				return 104
			}
		}), pg.mapMarkerStart.setMap(null), pg.mapMarkerFinish.setMap(null), google.maps.event.addListener(pg.mapMarkerStart, "dragend", function () {
			pg.map = {}, pg.optimalResults = null;
			var a = pg.mapMarkerStart.getPosition(), b = m(a), c = b.length ? b.join(",") : a.toUrlValue().replace(",", ";");
			pg.fTabPlanner_Click(c, pg.inputFinish)
		}), google.maps.event.addListener(pg.mapMarkerFinish, "dragend", function () {
			pg.map = {}, pg.optimalResults = null;
			var a = pg.mapMarkerFinish.getPosition(), b = m(a), c = b.length ? b.join(",") : a.toUrlValue().replace(",", ";");
			pg.fTabPlanner_Click(pg.inputStart, c)
		}), google.maps.event.addListener(pg.mapMarkerStart, "dragstart", function () {
			pg.mapShowAllStops || (pg.mapShowAllStops = !0, setTimeout(function () {
				pg.clusterManager.refresh()
			}, 100))
		}), google.maps.event.addListener(pg.mapMarkerFinish, "dragstart", function () {
			pg.mapShowAllStops || (pg.mapShowAllStops = !0, pg.clusterManager.refresh())
		});
		function m(a) {
			var b = v3.fromLatLngToDivPixel(a, 19), c = 19 - pg.GMap.getZoom(), d = 1 << c + 2, e = b.x - d, f = b.x + d, g = b.y - d, h = b.y + d, j = pg.clusterManager._mapMarkers, k = [];
			for (i = j.length; --i >= 0;) {
				marker = j[i];
				var l = marker._x, m = marker._y;
				if (m > h)break;
				m >= g && l >= e && l <= f && k.push(marker.id)
			}
			return k
		}

		var n = [], o = ti.stops;
		for (var p in o)n.push(o[p]);
		pg.clusterManager = new ClusterManager(e, {markers: n, clusterMaxZoom: 14}), pg.clusterManager.setMap(pg.GMap);
		var q = new google.maps.MarkerImage(pg.imagesFolder + "location2_wait.png", new google.maps.Size(20, 20), new google.maps.Point(0, 0), new google.maps.Point(10, 10));
		pg.youAreHere = new google.maps.Marker({
			title: "You are here!",
			position: new google.maps.LatLng(0, 0),
			icon: q,
			draggable: !1,
			clickable: !1
		}), google.maps.event.addListener(e, "zoom_changed", function () {
			pg.clusterManager._div.innerHTML = "", (pg.getMapVehicles() || {}).innerHTML = "", pg.hideMapInfoWindow()
		}), google.maps.event.addListener(e, "idle", function () {
			setTimeout(function () {
				pg.clusterManager.refresh(), pg.fShowVehicles()
			}, 100)
		}), google.maps.event.addListener(e, "maptypeid_changed", function () {
			setTimeout(function () {
				pg.clusterManager.refresh(), pg.clusterManager.showLayer(), pg.fShowVehicles()
			}, 100)
		}), pg.fMapShow()
	}
}, pg.getMapVehicles = function () {
	if (typeof pg.$mapVehicles == "undefined") {
		var a = v3.getPanes();
		if (a)pg.$mapVehicles = document.createElement("div"), pg.$mapVehicles.id = "divMapVehicles", a.overlayMouseTarget.appendChild(pg.$mapVehicles); else return {}
	}
	return pg.$mapVehicles
}, pg.divMapHide_Click = function () {
	pg.fMapHide(), pg.hashForMap = "";
	if (typeof mobile != "undefined" && mobile.current_page == "schedule4" && mobile.previous_page == "stop")mobile.back(); else {
		if (typeof mobile != "undefined" && mobile.current_page == "stop" && pg.inputStop && pg.inputStop.indexOf(";") != -1) {
			pg.fUrlSet({transport: "search"});
			return
		}
		if (typeof mobile != "undefined" && (pg.urlPrevious == "" || pg.urlPrevious == "/" + pg.language)) {
			pg.fUrlSet({transport: "", city: ""});
			return
		}
		pg.fUrlSet()
	}
}, pg.divMapMaximize_Click = function (a) {
	var b = pg.GMap && pg.GMap.getCenter();
	pg.fUrlSetMap({maximized: !0}), b && (google.maps.event.trigger(pg.GMap, "resize"), pg.GMap.setCenter(b));
	return pg.cancelEvent(a)
}, pg.divMapRestore_Click = function (a) {
	var b = pg.GMap && pg.GMap.getCenter();
	pg.fUrlSetMap({maximized: !1}), b && (google.maps.event.trigger(pg.GMap, "resize"), pg.GMap.setCenter(b));
	return pg.cancelEvent(a)
}, pg.fShowTraffic = function (a) {
	pg.mapShowTraffic = a, pg.GMap && (pg.trafficOverlay || (pg.trafficOverlay = new google.maps.TrafficLayer), a ? (pg.GMap.getMapTypeId() == "OSM" && (pg.clusterManager.hideLayer(), pg.GMap.setMapTypeId("roadmap")), pg.trafficOverlay.setMap(pg.GMap)) : (v3.mapTypeIds.indexOf("OSM") >= 0 && (pg.clusterManager.hideLayer(), pg.GMap.getMapTypeId() == "roadmap" && pg.GMap.setMapTypeId("OSM")), pg.trafficOverlay.setMap(null)), pg.toggleClass($("divMapShowTraffic"), "pressed", a))
}, pg.fToggleVehicles = function (a) {
	pg.mapShowVehicles = a, pg.GMap && cfg.city.urlGPS && (pg.toggleClass($("divMapShowVehicles"), "pressed", a > 0), a > 0 && !pg.mapShowVehiclesInterval ? (pg.mapShowVehiclesInterval = setInterval(pg.fShowVehicles, 5e3), window.setTimeout(function () {
		pg.fShowVehicles()
	}, 200)) : a <= 0 && ((pg.getMapVehicles() || {}).innerHTML = ""))
}, pg.fShowVehicles = function () {
	if (v3.ready) {
		if (!cfg.city.urlGPS || pg.mapShowVehicles <= 0) {
			pg.mapShowVehiclesInterval && (clearInterval(pg.mapShowVehiclesInterval), pg.mapShowVehiclesInterval = 0), (pg.getMapVehicles() || {}).innerHTML = "";
			return
		}
		if (pg.hashForMap == "map" && pg.mapShowVehicles != 2) {
			pg.fToggleVehicles(-1);
			return
		}
		var a = cfg.city.urlGPS;
		a += a.indexOf("?") >= 0 ? "&time=" : "?", a += +(new Date), ti.fDownloadUrl("GET", a, pg.fProcessGPSData)
	}
}, pg.fProcessGPSData = function (a) {
	if (pg.mapShowVehicles > 0) {
		a = a.split("\n");
		var b = [], c = "," + pg.hashForMap + ",", d = pg.transport || pg.schedule && pg.schedule.transport;
		d === "stop" && (d = "");
		var e = pg.schedule && pg.schedule.num || "", f = cfg.city.courseOrigin || 0, g = cfg.city.courseCounterClockwise ? -1 : 1, h = pg.GMap.getBounds(), i = h.getSouthWest(), j = h.getNorthEast();
		for (var k = a.length; k--;) {
			var l = a[k].split(",");
			if (l.length >= 4) {
				var m = l[1];
				if (m === "0")continue;
				var n = ({1: "trol", 3: "tram", 4: "minibus", 5: "seasonalbus"})[l[0]] || "bus";
				l[1].length && l[1].charAt(l[1].length - 1).toLowerCase() == "g" && (n = "expressbus");
				if (d != "plan")if (pg.hashForMap === "map") {
					if (d && (e || cfg.defaultCity == "rostov") && n !== d)continue;
					if (e && ti.toAscii(m) !== e)continue
				} else if (c.indexOf("," + n + "," + ti.toAscii(m) + ",") < 0)continue;
				var o = l[6], p = l[4] ? " " + l[4] + "km/h" : "", q;
				l[5] && +l[5] < 999 && (q = f + +l[5] * g);
				var r = +l[2], s = +l[3];
				if (cfg.defaultCity !== "klaipeda")r = r / 1e6, s = s / 1e6; else {
					r = r / 1e4, s = s / 1e4;
					var t = r | 0, u = t / 100 | 0, v = t - u * 100 + r - t;
					r = u + v / 60;
					var t = s | 0, u = t / 100 | 0, v = t - u * 100 + s - t;
					s = u + v / 60
				}
				if (s < i.lat() || s > j.lat() || (r > j.lng() || r < i.lng()))continue;
				var w = new google.maps.LatLng(s, r), x = v3.fromLatLngToDivPixel2(w), y = pg.getStyle("." + n), z = y && (y.backgroundColor || y.color);
				z = z && pg.styleRGBtoHex(z) || "#0000FF";
				var A = cfg.defaultCity == "tallinna-linn" ? p : (o || "") + p, B = "";
				(cfg.city.lowFloorVehicles || "").indexOf("," + o + ",") >= 0 && (B = "; color:yellow; border-color:yellow;"), pg.transformCSS ? (q && b.push("<div class=\"arrow\" style=\"left:" + (x.x - 10) + "px; top:" + (x.y - 10) + "px; background-color:#" + z + "; " + pg.transformCSS + ":rotate(" + (45 + q) + "deg);\"></div>"), b.push("<div class=\"circle\"  style=\"left:" + (x.x - 9) + "px; top:" + (x.y - 9) + "px; background-color:#" + z + B + (m.length > 2 ? ";font-size:smaller" : "") + ";\" title=\"" + A + "\" data-vehicle-id=\"" + o + "\" data-transport=\"" + n + "\" data-route=\"" + m + "\">" + m + "</div>")) : b.push("<img src=\"http://chart.apis.google.com/chart?cht=it&chs=20x20&chco=" + z + ",00000000,00000000&chx=ffffff&chf=bg,s,00000000&ext=.png&chl=" + m + "\" title=\"" + A + "\" style=\"z-index:110; position:absolute; width:20px; height:20px; left:" + (x.x - 10) + "px; top:" + (x.y - 10) + "px; cursor:pointer;\" />")
			}
		}
		a = null, (pg.getMapVehicles() || {}).innerHTML = b.join("")
	}
}, pg.fScheduleShow = function (a) {
	a.num && (a.num = a.num.toLowerCase()), pg.schedule || (pg.schedulePane = 1, ($("spanReturnToRoutes") || {}).href = pg.urlPrevious, pg.urlUnderSchedulePane = pg.urlPrevious, pg.languageUnderSchedulePane = pg.language), document.body.className.indexOf("Schedule") < 0 && (cfg.isMobilePage && pg.hashForMap ? document.body.className = "ScheduleMapDisplayed" : document.body.className = "ScheduleDisplayed"), $("aDir1") && setTimeout(function () {
		try {
			$("aDir1").focus()
		} catch (a) {
		}
	}, 100);
	pg.schedule && pg.schedule.city == a.city && pg.schedule.transport == a.transport && pg.schedule.num == a.num && pg.schedule.dirType == a.dirType && pg.schedule.tripNum == a.tripNum ? (pg.schedule.dirType = a.dirType, pg.schedule.stopId = a.stopId, typeof mobile == "undefined" && pg.fScheduleStopActivate()) : (pg.schedule = a, ($("spanDir1") || {}).innerHTML = "&nbsp;", ($("spanDir2") || {}).innerHTML = "&nbsp;", ($("dlDirStops1") || {}).innerHTML = "&nbsp;", ($("dlDirStops2") || {}).innerHTML = "&nbsp;", ($("divScheduleContentInner") || {}).innerHTML = "<br/>" + i18n.loading, pg.fScheduleLoad())
}, pg.fScheduleHide = function () {
	pg.schedule = null, document.body.className.indexOf("Schedule") >= 0 && (document.body.className = "", $("divMap").style.width = "100%", $("divMap").style.height = "100%")
}, pg.fScheduleLoad = function (a) {
	if (typeof mobile == "undefined" || typeof a != "undefined") {
		pg.schedules = null, cfg.city.doNotShowTimetables = cfg.city.doNotShowTimetables || {}, ($("ulScheduleDirectionsList") || {style: {}}).style.display = "none";
		if (typeof ti.routes !== "object" || typeof ti.stops !== "object") {
			setTimeout(function () {
				pg.fScheduleLoad(a)
			}, 200);
			return
		}
		var b = null, c;
		cfg.city.showAllDirections ? (c = ti.fGetRoutes(pg.schedule.city, pg.schedule.transport, pg.schedule.num, pg.schedule.dirType), $("aDir2").style.display = "none", $("aDir1").innerHTML = $("aDir1").innerHTML.replace("в–ј", "")) : c = ti.fGetRoutes(pg.schedule.city, pg.schedule.transport, pg.schedule.num, null, "0", null);
		if (typeof mobile != "undefined")var d = {directions: {1: [], 2: []}, stops: [], trip: {}};
		if (!c.length) {
			a && a(d), $("divScheduleContentInner").innerHTML = "Error: route not found.";
			return
		}
		var e = {}, f = {1: "", 2: ""};
		for (var g = 0; g < c.length; g++) {
			var h = c[g], i = h.name, j = "";
			!b && pg.schedule.dirType && pg.schedule.dirType == h.dirType && (b = h, j = "strong");
			if (!e[i + h.dirType]) {
				e[i + h.dirType] = !0;
				var k = h.dirType.split("-"), l = k[0], m = k[k.length - 1];
				c.length > 1 && l !== "a" && m !== "b" ? l.charAt(0) === "b" || m.charAt(0) === "a" || l.charAt(0) !== "a" && m.charAt(0) !== "b" ? (h.dirNum = 2, j = "indented" + (j ? " " + j : "")) : h.dirNum = 1 : h.dirNum = 1;
				var n = pg.fUrlSet({schedule: {dirType: h.dirType}}, !0);
				f[h.dirNum] += "<a href=\"#" + n + "\"" + (j ? " class=\"" + j + "\"" : "") + ">" + i + "</a>", typeof mobile != "undefined" && d.directions[h.dirNum].push({
					hash: n,
					name: i
				})
			}
		}
		($("ulScheduleDirectionsList") || {}).innerHTML = f[1] + f[2], b || (b = c[0]), pg.schedule.dirType = b.dirType, pg.schedule.dirTypes = {}, pg.schedule.route = b;
		var o = pg.schedulePane == 2 ? 2 : 1, p = [];
		for (var q = 1; q <= 2; q++) {
			pg.schedule.dirTypes[b.dirType] = o, ($("spanDir" + o) || {}).innerHTML = (b.num && b.num.length <= 5 ? "<span class=\"num num3 " + b.transport + "\">" + b.numHTML + "</span>" : "") + b.name, p = [];
			var r = null, s = 0, t = (b.streets || "").split(",") || [], u, v = null, w, x = pg.schedule.tripNum && q == 1 && !cfg.city.doNotShowTimetables[pg.schedule.transport] ? pg.schedule.tripNum : 0;
			x && (u = typeof b.times === "string" ? ti.explodeTimes(b.times) : b.times, w = u.workdays.length, v = u.times);
			for (g = 0; g < b.stops.length; g++) {
				var y = ti.fGetStopDetails(b.stops[g]), n = pg.fUrlSet({
					schedule: {
						dirType: b.dirType,
						stopId: y.id,
						tripNum: x
					}
				}, !0);
				p.push("<dt><a class=\"hover\" href=\"#" + n + "\">" + (v ? ti.printTime(v[x - 1 + g * w], null, "&#x2007;") + "&nbsp;&nbsp;" : "") + y.name + "</a>"), cfg.defaultCity == "latvia" && g < b.stops.length - 1 && p.push("<a class=\"draw\" target=\"_blank\" href=\"http://www.stops.lt/latviatest/latvia/editor.html#" + b.stops[g] + "," + b.stops[g + 1] + "\">draw</a>"), p.push("</dt>");
				if (typeof mobile != "undefined" && q == 1) {
					var z = v ? ti.printTime(v[x - 1 + g * w], null, "&#x2007;") : "";
					d.stops.push({hash: n, id: y.id, name: y.name, time: z}), d.trip[y.id] = {
						time: z,
						workdays: u ? u.workdays[x - 1] : "",
						previous_trip: u && x > 1 && u.workdays[x - 2] == u.workdays[x - 1] ? x - 1 : "",
						next_trip: u && x < u.workdays.length && u.workdays[x] == u.workdays[x - 1] ? x + 1 : ""
					}
				}
				if (q == 1 && y.street) {
					while (t[s])++s;
					t[s] = {name: y.street, stops: y.name, hash: n}, ++s
				}
			}
			($("dlDirStops" + o) || {}).innerHTML = p.join(""), ($("dlDirStops" + o) || {style: {}}).style.display = "";
			if (q == 2)break;
			for (s = t.length; --s >= 0;)r = t[s], typeof r == "string" && (r = t[s] = {name: r}), r.name.replace(/"/g, "&quote;").replace(/\s/, "&nbsp;"), s + 1 < t.length && r.name == t[s + 1].name && (r.stops += ", " + t[s + 1].stops, r.hash = r.hash || t[s + 1].hash, t[s + 1].name = null);
			var A = "";
			for (s = 0; s < t.length; ++s)r = t[s], r.name && (A ? A += ", " : A = i18n.routeStreets + ": ", r.hash ? A += "<a href=\"#" + r.hash + "\" class=\"hover\" title=\"" + i18n.stops + ": " + r.stops.replace(/"/g, "") + "\">" + r.name + "</a>" : A += r.name);
			($("divScheduleRoute") || {}).innerHTML = "<span class=\"icon icon_" + b.transport + "\"></span><span class=\"num num3 " + b.transport + "\">" + b.numHTML + "</span>&nbsp;&nbsp; " + A + "<div class=\"RouteDetails\"" + (pg.scheduleDetailsExpanded ? "" : " style=\"display:none;\"") + ">" + i18n.operator + ": " + ti.fOperatorDetails(b.operator, b.transport) + "</div>";
			if (c.length <= 1)break;
			o = 3 - o;
			var k = b.dirType.split("-"), l = k[0], m = k[k.length - 1], B = m + "-" + l, C = b.dirNum;
			b = null;
			for (g = 0; g < c.length; g++) {
				if (!b || C == b.dirNum && C != c[g].dirNum)b = c[g];
				if (c[g].dirType === B) {
					b = c[g];
					break
				}
			}
			if (!b || l == m || cfg.defaultCity == "latvia") {
				$("aDir2" || {}).style.display = "none", $("dlDirStops2" || {}).style.display = "none";
				break
			}
		}
		a ? a(d) : pg.fScheduleStopActivate(), pg.schedule.tripNum || (($("divScheduleBody") || {}).scrollTop = 0)
	}
}, pg.aDir_Click = function (a) {
	setTimeout(function () {
		try {
			a.focus()
		} catch (b) {
		}
	}, 100);
	var b = $("ulScheduleDirectionsList");
	(a.id || "").indexOf("2") >= 0 && a.offsetLeft > 100 ? (pg.scheduleProposedPane = 2, b.style.right = "10px", b.style.left = "") : (pg.scheduleProposedPane = 1, b.style.left = a.offsetLeft + "px", b.style.right = ""), b.style.display = "block"
}, pg.aDir_Blur = function () {
	$("ulScheduleDirectionsList").style.display = "none"
}, pg.ulScheduleDirectionsList_Click = function (a) {
	a = a || window.event;
	var b = a.target || a.srcElement;
	if (b.nodeName.toLowerCase() == "a") {
		var c = b.href.split("#")[1], d = pg.fUrlParse(c);
		pg.schedulePane = pg.scheduleProposedPane || 1, c = pg.fUrlSet({
			schedule: {
				dirType: d.schedule.dirType,
				stopId: null,
				tripNum: 0
			}
		}, !0), c != Hash.getHash() ? Hash.go(c) : pg.fScheduleLoad();
		return pg.cancelEvent(a)
	}
}, pg.fScheduleStops_Click = function (a, b) {
	a = a || window.event;
	var c = a.target || a.srcElement;
	if (c.nodeName.toLowerCase() == "a") {
		pg.schedulePane = b;
		var d = c.href.split("#")[1], e = pg.fUrlParse(d);
		pg.fUrlSet({schedule: {dirType: e.schedule.dirType, stopId: e.schedule.stopId, tripNum: e.schedule.tripNum}});
		return pg.cancelEvent(a)
	}
}, pg.fTransferHideMenu = function () {
	if (pg.transfersMenuHide) {
		var a = $("divTransfersMenu");
		a.style.display = "none"
	}
}, pg.fTransfer_MouseOver = function (a) {
	a = a || window.event;
	var b = a.target || a.srcElement;
	if (b.id == "divTransfersMenu" || (b.parentNode || {}).id == "divTransfersMenu" || b.id == "checkTransfer" || b.id == "spanCheckTransfer")pg.transfersMenuHide = !1; else {
		var c = b.getAttribute("data-transport");
		pg.transfersMenuHide = !0;
		if (cfg.defaultCity != "tallinna-linn" && cfg.defaultCity != "riga" && cfg.defaultCity != "rostov" || typeof b.className != "string" || b.className.indexOf("transfer") < 0 || !b.href) {
			if (c && pg.transfersDisplayed) {
				pg.addSchedule = c;
				var d = !0;
				if (pg.schedules)for (var e in pg.transfersDisplayed) {
					d = pg.transfersDisplayed[e];
					if (d && d.transport == c && !pg.schedules[e]) {
						d = !0;
						break
					}
				}
				$("checkTransfer").checked = d !== !0, $("spanCheckTransfer").innerHTML = i18n.transport[c.replace("-remove", "")], pg.transfersMenuHide = !1
			}
		} else {
			pg.addSchedule = pg.fUrlParse(b.href).schedule;
			if (pg.addSchedule) {
				var d = ti.fGetRoutes(pg.addSchedule.city, pg.addSchedule.transport, pg.addSchedule.num, pg.addSchedule.dirType, !0)[0];
				$("checkTransfer").checked = b.className.indexOf("active") >= 0, $("spanCheckTransfer").innerHTML = i18n.transport1[d.transport] + (d.num.length > 15 ? "" : " " + d.numHTML) + " " + i18n.towards + " " + d.name, pg.transfersMenuHide = !1
			}
		}
		var f = $("divTransfersMenu");
		pg.transfersMenuHide ? f.style.display == "block" && pg.fTransfer_MouseOut() : (f.style.left = b.offsetLeft + "px", f.style.top = b.offsetTop + b.offsetHeight + "px", f.style.display = "block")
	}
}, pg.fTransfer_MouseOut = function () {
	pg.transfersMenuHide = !0, setTimeout(pg.fTransferHideMenu, 200)
}, pg.fScheduleStopActivate = function () {
	var a = "/" + pg.schedule.dirType + "/" + pg.schedule.stopId + "/", b = pg.schedule.dirTypes[pg.schedule.dirType], c;
	for (var d = 1; d <= 2; d++) {
		c = $("dlDirStops" + d).getElementsByTagName("a");
		for (var e = 0; e < c.length; ++e) {
			var f = c[e];
			d == b && a && pg.schedule.stopId && ("/" + f.href + "/").indexOf(a) >= 0 ? (f.className = "current" + ti.fGetDirTag(pg.schedule.dirType), a = "") : f.className.indexOf("current") >= 0 && (f.className = "")
		}
	}
	if (a) {
		c = $("dlDirStops" + (b || 1)).getElementsByTagName("a");
		if (c && (c[0] || {}).href) {
			a = c[0].href.split("#")[1], pg.fUrlExecute(a);
			return
		}
	}
	$("aDir1").className = $("divScheduleLeft").className = b == 1 ? "active" : "", $("aDir2").className = $("divScheduleRight").className = b == 2 ? "active" : "", pg.browserVersion >= 8 && pg.toggleClass($("divScheduleContentInner"), "Right", b == 2), pg.fScheduleLoadTimetable()
}, pg.fScheduleLoadTimetable = function (a) {
	if (typeof mobile == "undefined" || typeof a != "undefined") {
		var b, c, d, e = [pg.schedule.city, pg.schedule.transport, pg.schedule.num].join("_"), f = pg.schedules || {};
		pg.schedules || (f[e] = pg.schedule);
		var g = pg.nonEmptyCount(f) > (f[e] ? 1 : 0), h = ti.fGetTransfersAtStop(pg.schedule.stopId, !0, pg.schedule.route);
		if (typeof mobile != "undefined")var i = {workdays: [], timetables: {}, timetables_html: [], maxlength: {}};
		pg.transfersDisplayed = {};
		var j = null, k = null, l = [], m = [];
		for (d = 0; d < h.length; d++) {
			b = h[d], e = ti.toAscii([b.city, b.transport, b.num].join("_"), !0);
			if (pg.transfersDisplayed[e])continue;
			var n = {
				id: b.id,
				city: b.city,
				transport: b.transport,
				num: ti.toAscii(b.num, !0),
				numHTML: b.numHTML,
				dirType: b.dirType,
				routeTag: b.stopId,
				stopId: b.stopId
			};
			pg.transfersDisplayed[e] = n;
			if (pg.city === "druskininkai" || pg.city === "liepaja")parseInt(pg.schedule.num, 10) === parseInt(b.num, 10) && pg.schedule.num.toLowerCase().indexOf("s") < 0 && b.num.toLowerCase().indexOf("s") < 0 && (f[e] = n, g = g || pg.schedule.num !== b.num); else if (pg.city === "klaipeda")if (pg.schedule.num + "e" === b.num.toLowerCase() || pg.schedule.num.replace("(", "e(") === b.num.toLowerCase().replace(/\s/g, ""))f[e] = n, g = g || pg.schedule.num !== b.num;
			c = pg.fUrlSet({schedule: n}, !0), j !== b.transport && (j = b.transport, l.push(" <span class=\"icon icon_narrow icon_" + b.transport + "\" data-transport=\"" + b.transport + "\"></span>&nbsp;"));
			var o = "<a class=\"hover " + (f[e] ? "activetransfer " : "transfer") + j + "\" href=\"#" + c + "\" title=\"" + (b.name || "").replace(/"/g, "") + "\">" + h[d].numHTML.replace(/\s/g, "&nbsp;") + "</a> ";
			l.push(o), f[e] && (k !== b.transport && (k = b.transport, o = " <span class=\"icon icon_narrow icon_" + b.transport + "\" data-transport=\"" + b.transport + "-remove\"></span>&nbsp;" + o), m.push(o), f[e].stopId = b.stopId)
		}
		l.push("<span style=\"display:inline-block; width:2px;\"></span>");
		var p = ti.fGetStopDetails(pg.schedule.stopId), q = (p.street ? ", " + p.street : "") + (p.area && !cfg.cities[pg.city].skipStopArea ? ", " + p.area : "") + (p.city && !cfg.cities[pg.city].skipStopCity ? ", " + p.city : "");
		p[cfg.cities[pg.city].stopFareZone || "noFareZone"] && (q += ", " + i18n.fareZone + " " + p[cfg.cities[pg.city].stopFareZone]), q = q.length > 0 ? "<span class=\"details\"> (" + q.substring(2) + ")</span>" : "", ($("divScheduleStop") || {}).innerHTML = i18n.stop + "<strong> " + p.name + "</strong>" + q + "&nbsp;&nbsp; " + l.join("");
		if (p.street) {
			var r = p.street.replace(/"/g, "&quote;").replace(/\s/, "&nbsp;"), s = $("divScheduleRoute") && $("divScheduleRoute").getElementsByTagName("a");
			if (s)for (d = s.length; --d >= 0;)s[d].innerHTML.indexOf(r) < 0 ? s[d].className == "hover strong" && (s[d].className = "hover") : s[d].className = "hover strong"
		}
		var t = [], u = 0, v = Number.POSITIVE_INFINITY, w = cfg.city.doNotMergeTimetables;
		pg.schedule.stopId != pg.schedule.route.stops[0] && cfg.city.doNotShowTimetables && cfg.city.doNotShowTimetables[pg.transport] && (f = null);
		for (var e in f) {
			var n = f[e];
			if (!n || !n.stopId)continue;
			if (!pg.transfersDisplayed[e])continue;
			var x = ti.fGetStopDetails(n.stopId), y = {}, z = (x || {raw_data: ""}).raw_data.split(";"), A = n.dirType.split("-"), B = A[0], C = A[A.length - 1], D = 2;
			B === "a" || C === "b" ? D = 1 : B.charAt(0) !== "b" && C.charAt(0) !== "a" && (B.charAt(0) === "a" || C.charAt(0) === "b") && (D = 1);
			var E = ti.toAscii(pg.schedule.route.name, !0);
			for (var d = ti.FLD_DIRS; d < z.length; d += 2) {
				b = ti.fGetRoutes(z[d]);
				if (b.city === n.city && b.transport === n.transport && ti.toAscii(b.num, !0) === n.num && b.times && (!pg.schedule.route.routeTag || b.id === pg.schedule.route.id || ti.toAscii(b.name, !0) === E)) {
					A = b.dirType.split("-"), B = A[0], C = A[A.length - 1];
					var F = 2;
					B === "a" || C === "b" ? F = 1 : B.charAt(0) !== "b" && C.charAt(0) !== "a" && (B.charAt(0) === "a" || C.charAt(0) === "b") && (F = 1);
					if (D !== F)continue;
					if (y[b.id])continue;
					y[b.id] = !0, b.tag = (!g && b.dirType != pg.schedule.dirType && ti.toAscii(b.name, !0) !== E ? "other" : "current") + ti.fGetDirTag(b.dirType);
					if (b.tag == "current" || b.tag == "other" && B.charAt(0) == "d")b.tag = "";
					(pg.city === "druskininkai" || pg.city === "liepaja") && pg.schedule.num === b.num && (b.tag = ""), pg.city === "klaipeda" && (b.num.substring(b.num.length - 1).toLowerCase() === "e" || b.num.toLowerCase().indexOf("e (") > 0) && (b.tag = "express");
					var G = typeof b.times === "string" ? ti.explodeTimes(b.times) : b.times, H = +z[d + 1], I = G.workdays, J = G.tag, K = G.times, L = I.length, M = L;
					for (var N = L + H * L; M--;) {
						var O = K[--N];
						v > O && O >= 0 && (v = O);
						var P = b.tag, Q = !1, R = !1;
						J.charAt(M) === "1" ? P = (P ? P + " " : "") + "highlighted" : J.charAt(M) === "4" ? (P = (P ? P + " " : "") + "smallbus", R = !0) : J.charAt(M) === "2" && (Q = !0, pg.city == "druskininkai" && (P = (P ? P + " " : "") + "highlighted")), pg.schedule.tripNum && b.dirType === pg.schedule.dirType && pg.schedule.tripNum - 1 == M && (P = (P ? P + " " : "") + "clicked");
						if (w)t[u++] = {
							time: O,
							workday: I[M],
							route: b,
							tag: P,
							bicycle: Q,
							small_capacity: R,
							tripNum: M
						}; else for (var S = 1; S <= 7; S++)I[M].indexOf(S) >= 0 && (t[u++] = {
							time: O,
							workday: S,
							route: b,
							tag: P,
							bicycle: Q,
							small_capacity: R,
							tripNum: M
						})
					}
				}
			}
		}
		t.sort(function (a, b) {
			if (a.workday < b.workday)return -1;
			if (a.workday > b.workday)return 1;
			if (a.time < b.time)return -1;
			if (a.time > b.time)return 1;
			if (a.route.id < b.route.id)return -1;
			if (a.route.id > b.route.id)return 1;
			return 0
		});
		var T = "";
		$("divScheduleRoute") && (g ? (T = "<div style=\"width:100%; margin-top:10px;\">" + m.join(" &nbsp;") + " &nbsp;<label id=\"labelShowDeparturesWithNumbers\" for=\"showDeparturesWithNumbers\"><input name=\"showDeparturesWithNumbers\" id=\"showDeparturesWithNumbers\" type=\"checkbox\" value=\"showDeparturesWithNumbers\"" + (pg.showDeparturesWithNumbers ? " checked=\"checked\"" : "") + " onclick=\"pg.fToggleNumbersAtDepartures();\" />" + i18n.showDeparturesWithRouteNumbers + "</label></div>", $("divScheduleRoute").style.display = "none") : $("divScheduleRoute").style.display = ""), l = [];
		if (t.length) {
			var U, V = v = ~~(v / 60) - 1, W = [], X = [], Y;
			for (d = 0, N = t.length; d <= N; d++) {
				if (d > 0 && (d === N || t[d].workday != t[d - 1].workday)) {
					var Y = l.join(";"), L = t[d - 1].workday;
					for (kk = 1; kk <= 7; ++kk)if (W[kk] === Y) {
						X[kk] += L;
						break
					}
					kk > 7 && (W[L] = Y, X[L] = "" + L);
					if (d === N)break;
					l = []
				}
				Y = t[d];
				var b = Y.route;
				l.push(Y.time, b.city, b.transport, b.num, b.dirType)
			}
			l = [];
			for (d = 0, N = t.length; d <= N; d++) {
				if (d < N) {
					Y = t[d];
					if (w)X[Y.workday] = Y.workday; else if (!X[Y.workday])continue
				}
				if (d > 0 && (d === N || Y.workday != t[d - 1].workday)) {
					V != -999 && l.push("</td></tr>"), l.push("</tbody></table>"), T += l.join(""), typeof mobile != "undefined" && i.timetables_html.push(l.join("")), l = [];
					if (d === N)break
				}
				if (d == 0 || Y.workday != t[d - 1].workday)V = v, l.push("<table class=\"table table-bordered timetable\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><th></th><th class=\"workdays\">" + pg.fWeekdaysName(X[t[d].workday]) + "</th></tr>"), typeof mobile != "undefined" && (i.workdays.push(X[t[d].workday]), i.timetables[X[t[d].workday]] = [], i.maxlength[X[t[d].workday]] = 0);
				var Z = Y.time;
				if (Z < 0)continue;
				var _ = ~~(Z / 60);
				Z = Z % 60;
				if (V !== _) {
					if (V != v) {
						l.push("</td></tr>");
						while (++V < _)l.push("<tr><th>-</th><td></td></tr>")
					} else while (++V < _)l.push("<tr><th>&nbsp;</th><td></td></tr>");
					V = _, l.push("<tr><th>" + _ % 24 + "</th><td" + (g ? " style=\"white-space:normal;\"" : "") + ">"), typeof mobile != "undefined" && i.timetables[X[t[d].workday]].push({
						hour: _ % 24,
						minutes: []
					})
				}
				var ba = Y.route;
				c = pg.fUrlSet({
					schedule: {
						city: ba.city,
						transport: ba.transport,
						num: ba.num,
						dirType: ba.dirType,
						stopId: ba.stopId,
						tripNum: Y.tripNum + 1
					}
				}, !0), l.push("<a href=\"#" + c + "\" title=\"" + (g ? i18n.transport1[ba.transport] + (ba.num.length > 15 ? "" : " " + ba.numHTML) + " " + i18n.towards + " " : "") + ba.name.replace(/"/g, "") + "\"" + (Y.tag ? "class=\"" + Y.tag + "\"" : "") + ">" + (Z < 10 ? "0" : "") + Z + (g ? "<span class=\"departure" + ba.transport + "\">\\" + ba.numHTML + "</span>" : "") + (Y.bicycle ? "<img class=\"icon\" style=\"margin:0 1px;\" src=\"" + pg.imagesFolder + "bicycle.png\">" : "") + (g ? "</a>&#x200A;" : "</a>"));
				if (typeof mobile != "undefined") {
					var bb = i.timetables[X[t[d].workday]], bc = bb[bb.length - 1];
					bc.minutes.push({
						hash: c,
						min: Z,
						title: g ? i18n.transport1[ba.transport] + (ba.num.length > 15 ? "" : " " + ba.num) + " " + i18n.towards + " " : "",
						classname: Y.tag || ""
					}), bc.minutes.length > i.maxlength[X[t[d].workday]] && (i.maxlength[X[t[d].workday]] = bc.minutes.length)
				}
			}
		}
		if (pg.schedule.route && pg.schedule.route.transport) {
			b = pg.schedule.route, l = [];
			var bd = cfg.city["notes_" + b.transport + "_" + b.num.toLowerCase()] || cfg.city["notes_" + b.operator] || cfg.city["notes_" + b.transport];
			bd && (typeof mobile != "undefined" && (i.notes = bd), T = "<div style=\"margin-top:10px; clear:both;\">" + (bd[pg.language] || bd.en || bd) + "</div>" + T), bd = cfg.city.skipOperator ? "" : ti.fOperatorDetails(b.operator, b.transport), bd && l.push("<p class=\"noindent\"><strong>" + i18n.operator + ":</strong> " + bd + "</p>"), bd = (cfg.operators[b.operator] || cfg.operators[b.transport] || cfg.operators[b.transport + "_" + b.num] || {notes: ""}).notes, bd && l.push("<p>" + (bd[pg.language] || bd.en || bd).replace("%operator", b.operator) + "</p><br /><br />"), f && (l.push("<p class=\"noindent\"><strong>" + i18n.scheduleCommentsInfo + ":</strong>"), T.indexOf("bicycle") >= 0 && l.push("<p>" + i18n.transferBicyclesDepartures), bd = (cfg.operators[b.operator] || cfg.operators[b.transport] || {comments: ""}).comments, bd ? l.push("<p>" + (bd[pg.language] || bd.en || bd).replace("%operator", b.operator) + "</p>") : (i18n.scheduleDelaysWarning && i18n.scheduleDelaysWarning.length > 10 && l.push("<p>" + i18n.scheduleDelaysWarning), l.push("<p>" + i18n.scheduleComments)), T.indexOf("smallbus") >= 0 && l.push("<p>" + i18n.smallBus12Departures), T.indexOf("highlighted") >= 0 && l.push("<p>" + i18n.lowFloorDepartures), T.indexOf("other") >= 0 && l.push("<p>" + i18n.scheduleChangedDepartures)), ($("divScheduleContentBottom") || {}).innerHTML = l.join("</p>") + "</p>"
		}
		pg.replaceHtml($("divScheduleContentInner"), T + "<div style=\"clear:both;\"></div>"), a && a(i)
	}
}, pg.fCheckTransfer_Click = function () {
	if (!pg.addSchedule)return !1;
	$e = $("checkTransfer");
	var a;
	pg.schedules || (a = [pg.schedule.city, pg.schedule.transport, pg.schedule.num].join("_"), pg.schedules = {}, pg.schedules[a] = pg.schedule);
	if (typeof pg.addSchedule == "object")a = ti.toAscii([pg.addSchedule.city, pg.addSchedule.transport, pg.addSchedule.num].join("_")), pg.schedules[a] = $e.checked ? pg.addSchedule : null; else {
		pg.addSchedule = (pg.addSchedule || "").replace("-remove", "");
		for (var a in pg.transfersDisplayed) {
			var b = pg.transfersDisplayed[a];
			b.transport == pg.addSchedule && (pg.schedules[a] = $e.checked ? b : null)
		}
	}
	pg.fScheduleLoadTimetable();
	return
}, pg.fToggleNumbersAtDepartures = function () {
	pg.showDeparturesWithNumbers = $("showDeparturesWithNumbers").checked, pg.toggleClass($("divScheduleContentInner"), "HideNumbers", !pg.showDeparturesWithNumbers)
}, pg.fWeekdaysName = function (a) {
	var b = i18n["weekdays" + a] || "";
	if (b)return b;
	var c = a.split("");
	for (var d = c.length; --d >= 0;)b = c[d], c[d] = i18n["weekdays" + b] || b;
	b = c.join(", ");
	return b
}, pg.inputSuggestedStops_Focus = function (a) {
	pg.inputActive !== a && (pg.inputActive = a, pg.stopsSuggestedForText = pg[pg.inputActive.id] ? pg.inputActive.value : null);
	if (pg.inputActive.className === "empty" || pg.inputActive.value == i18n.mapPoint || pg.inputActive.value == "Point on map")pg.inputActive.className = "", pg.inputActive.value = "";
	pg.timerSuggestedStopsShow === !1 ? pg.timerSuggestedStopsShow = 0 : (pg.fSuggestedStopsShow(!0), pg.timerSuggestedStopsShow === 0 && (pg.timerSuggestedStopsShow = setInterval(pg.fSuggestedStopsShow, 200)))
}, pg.inputSuggestedStops_Blur = function (a) {
	if (!document.activeElement || document.activeElement.id != "divSuggestedStops")pg.timerSuggestedStopsShow && clearInterval(pg.timerSuggestedStopsShow), pg.timerSuggestedStopsShow = 0, a && !a.value && (a.value = a.id == "inputFinish" ? i18n.finishStop : i18n.startStop, a.className = "empty"), pg.timerSuggestedStopsHide || (pg.timerSuggestedStopsHide = setTimeout(function () {
		pg.timerSuggestedStopsHide = 0, a && a.id == "inputStop" && a.value != pg.inputStopText && pg.fSuggestedStopsSelectFirst(a), pg.timerSuggestedStopsShow || pg.fSuggestedStopsHide()
	}, 200))
}, pg.divSuggestedStops_Blur = function () {
	(!document.activeElement || !pg.inputActive || document.activeElement.id !== pg.inputActive.id) && pg.inputSuggestedStops_Blur(pg.inputActive)
}, pg.fSuggestedStopsShow = function (a) {
	if (pg.inputActive) {
		var b = pg.inputActive.value || "", c = $("divSuggestedStops");
		if (a !== !0 && pg.stopsSuggestedForText === b && c.style.display === "block")return;
		if (a !== !0 && pg.stopLastTyped !== b) {
			pg.stopLastTyped = b;
			return
		}
		pg.stopsSuggestedForText != b && pg.inputStopText != pg.stopSuggestedForMap && (pg[pg.inputActive.id] = ""), pg.stopLastTyped = b, typeof ti.stops === "object" && (pg.stopsSuggestedForText = b);
		var d = [];
		if (b.length < 2 || typeof ti.stops != "object")d.push("<a id=\"aMoreChars\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_info\"></span>" + (typeof ti.stops != "object" ? i18n.receivingData : i18n.typeSomeChars) + "</a>"); else {
			var e = ti.fGetStopsByName(pg.stopSuggestedForMap || b);
			if (e.length == 0)d.push("<a id=\"aMoreChars\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_info\"></span>" + i18n.noStopsFound + "</a>"); else {
				var f = "," + pg[pg.inputActive.id] + ",";
				for (var g = 0; g < e.length; g++) {
					var h = e[g], i = [];
					h.city && !cfg.cities[pg.city].skipStopCity && i.push(h.city), h.area && !cfg.cities[pg.city].skipStopArea && i.push(h.area), h.streets && i.push(h.streets), i = i.length > 0 ? "<span class=\"details\"> (" + i.join(", ") + ")</span>" : "", i = "<a id=\"" + h.id + "\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_map\" title=\"" + i18n.showInMap + "\"></span>" + (f.indexOf("," + h.id + ",") >= 0 ? "<strong>" + h.name + "</strong>" : h.name) + i + "</a>", !1 && f.indexOf("," + h.id + ",") >= 0 ? d.splice(0, 0, i) : d.push(i)
				}
			}
		}
		b.length >= 3 && (pg.geocoder || typeof mobile != "undefined") && d.push("<div id=\"geocaching-results\"><span style=\"display:block; padding-left:10px;line-height:26px;\"><b>" + i18n.addressesAndPlaces + ":</b><br/>" + i18n.loading + "</span></div>"), d.push("<a id=\"aSuggestShowMap\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_stops\"></span>" + i18n.selectFromMap + "</a>"), (c || {}).innerHTML = d.join(""), b && b.length >= 3 ? typeof mobile != "undefined" ? mobile.searchPlaces(b, function (a) {
			var b = $("geocaching-results");
			if (b) {
				var c = [];
				a.length ? c.push(["<span style=\"display:block; padding-left:10px;line-height:26px;\"><b>", i18n.addressesAndPlaces, ":</b></span>"].join("")) : c.push(["<a id=\"aMoreChars\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_info\"></span>", i18n.noAddressesAndPlacesFound, "</a>"].join("")), jQuery.each(a, function (a, b) {
					c.push(["<a id=\"", b.key, "\" onclick=\"return false;\" href=\"\">", b.name, "</a>"].join(""))
				}), b.innerHTML = c.join(""), jQuery("#geocaching-results a").bind("click", function () {
					mobile.geocoder.getPlaceId(jQuery(this).attr("id"), function (a) {
						var b = mobile.geocoder.index[a];
						b.indexOf(",") != -1 && (b = b.substring(0, b.indexOf(","))), pg.inputActive.value = b, pg.inputActive.className = "", pg.stopsSuggestedForText = b, pg[pg.inputActive.id] = a, pg.fSuggestedStopsHide(), pg.timerSuggestedStopsShow = !1, pg.inputSuggestedStops_KeyDown(null, -13)
					})
				})
			}
		}) : pg.geocoder && pg.geocoder.search(b, function (a) {
			var b = $("geocaching-results");
			if (b) {
				var c = "";
				a.length ? c = "<span style=\"display:block; padding-left:10px;line-height:26px;\"><b>" + i18n.addressesAndPlaces + ":</b></span>" : c = "<a id=\"aMoreChars\" href=\"\" onclick=\"return false;\"><span class=\"icon icon_info\"></span>" + i18n.noAddressesAndPlacesFound + "</a>";
				for (var d = 0; d < a.length; d++)c += "<a id=\"" + a[d].lat + ";" + a[d].lng + "\" onclick=\"return false;\" href=\"\"><span class=\"icon icon_map\" title=\"" + i18n.showInMap + "\"></span>" + a[d].name + "</a>";
				b.innerHTML = c
			}
		}) : typeof mobile != "undefined" && pg.loadGoogleMapsScript(function () {
		});
		if (c && pg.inputActive.offsetHeight) {
			var j = $("divContentWrapper") || {
					offsetLeft: 0,
					offsetTop: 0
				}, k = pg.inputActive.offsetLeft, l = pg.inputActive.offsetTop + pg.inputActive.offsetHeight + 1;
			getComputedStyle(pg.inputActive.parentNode).position == "relative" && (k += pg.inputActive.parentNode.offsetLeft, l += pg.inputActive.parentNode.offsetTop), $("tblContentPlannerOptions") && pg.inputActive.id !== "inputStop" && (j.offsetLeft === 0 || k < j.offsetLeft || cfg.searchOnly) && (l += $("tblContentPlannerOptions").offsetTop, k += $("tblContentPlannerOptions").offsetLeft), pg.inputActive.id === "inputStop" && j.offsetLeft === 0 && (l = 0), pg.inputActive.id !== "inputStop" && k < j.offsetLeft && (k += j.offsetLeft, l += j.offsetTop), c.style.top = l + "px", c.style.left = k + "px"
		}
		pg.inputActive.offsetWidth > 2 && (typeof mobile != "undefined" ? c.style.right = window.innerWidth - (pg.inputActive.offsetWidth + k) + "px" : c.style.minWidth = pg.inputActive.offsetWidth - 2 + "px"), c.scrollTop = 0, c.style.overflowX = "hidden", c.style.overflowY = d.length > 6 ? "scroll" : "hidden", c.style.height = d.length > 6 ? "156px" : "auto", c.style.display = "block"
	}
}, pg.fSuggestedStopsHide = function () {
	pg.stopSuggestedForMap = "", $("divSuggestedStops").style.display != "none" && ($("divSuggestedStops").style.display = ($("frameHideSelects") || {style: {}}).style.display = "none")
}, pg.divSuggestedStops_MouseDown = function (a) {
	var b = a && (a.target || a.srcElement);
	return !b || b.id !== "divSuggestedStops"
}, pg.eSuggestedStops_Click = function (a) {
	pg.timerSuggestedStopsHide && (clearTimeout(pg.timerSuggestedStopsHide), pg.timerSuggestedStopsHide = 0);
	var b = a && (a.target || a.srcElement), c = b && (b.className || "").toLowerCase();
	b && !b.id && (b = b.parentNode);
	if (!b)return pg.cancelEvent(a);
	if (c && c.indexOf("map") >= 0) {
		pg.inputStopText = pg.stopSuggestedForMap = pg.stopSuggestedForMap || pg.stopsSuggestedForText;
		if (pg.transport == "plan") {
			var d;
			pg.inputActive.id === "inputStart" ? (pg.loadedPlannerParams = "clear start", d = "plan/" + b.id + "/" + (pg.inputFinish || "")) : (pg.loadedPlannerParams = "clear finish", d = "plan/" + (pg.inputStart || "") + "/" + b.id), Hash.go(d + "/map")
		} else Hash.go("stop/" + b.id + "/map");
		setTimeout(function () {
			try {
				pg.inputActive.focus()
			} catch (a) {
			}
		}, 100)
	}
	if (b.id && b.id.indexOf("ShowMap") >= 0) {
		pg.fSuggestedStopsHide(), pg.fUrlSetMap({});
		return pg.cancelEvent(a)
	}
	if (b.id && b.id.indexOf("MoreChars") < 0) {
		if (b.id.charAt(0) != "@") {
			var e = ti.fGetAnyStopDetails(b.id);
			pg.inputActive.value = e.name, pg.inputActive.className = "", pg.stopsSuggestedForText = e.name, pg[pg.inputActive.id] = b.id, pg.fSuggestedStopsHide(), pg.timerSuggestedStopsShow = !1, pg.inputSuggestedStops_KeyDown(null, -13)
		}
	} else {
		try {
			pg.inputActive.focus()
		} catch (f) {
		}
		pg[pg.inputActive.id] = ""
	}
	return pg.cancelEvent(a)
}, pg.inputKeyTab = function (a) {
	var b = window.event ? window.event.keyCode : a.keyCode;
	b == 9 && pg.inputSuggestedStops_KeyDown(null, 13)
}, pg.inputSuggestedStops_KeyDown = function (a, b) {
	pg.stopSuggestedForMap = "", b || (b = window.event ? window.event.keyCode : a.keyCode);
	b == 27 ? (pg.timerSuggestedStopsShow && clearInterval(pg.timerSuggestedStopsShow), pg.timerSuggestedStopsShow = 0, pg.fSuggestedStopsHide()) : b == 13 || b == -13 || b == -13 ? (pg.timerSuggestedStopsShow && clearInterval(pg.timerSuggestedStopsShow), pg.timerSuggestedStopsShow = 0, b == 13 && pg.fSuggestedStopsSelectFirst(), pg[pg.inputActive.id] && pg.fSuggestedStopsHide(), pg.inputActive.id === "inputStop" ? pg.inputStop && pg.fTabStop_Click(pg.inputStop) : (pg.loadedPlannerParams != pg.inputStart + "/" + pg.inputFinish && (pg.loadedPlannerParams = "clear results"), pg.fTabPlanner_Click(pg.inputStart, pg.inputFinish), pg.inputActive.id === "inputStart" && pg.inputStart && !pg.inputFinish ? setTimeout(function () {
		try {
			$("inputFinish").focus()
		} catch (a) {
		}
	}, 100) : pg.inputActive.id === "inputFinish" && pg.inputFinish && !pg.inputStart ? setTimeout(function () {
		try {
			$("inputStart").focus()
		} catch (a) {
		}
	}, 100) : pg.inputStart && pg.inputFinish && setTimeout(function () {
		try {
			$("buttonSearch").focus()
		} catch (a) {
		}
	}, 100))) : b != 9 && (pg.inputActive.className == "empty" && (pg.inputActive.value = "", pg.inputActive.className = ""), pg.fSuggestedStopsShow(), pg.timerSuggestedStopsShow || (pg.timerSuggestedStopsShow = setInterval(pg.fSuggestedStopsShow, 200)))
}, pg.fSuggestedStopsSelectFirst = function (a) {
	a = a || pg.inputActive;
	if (a) {
		pg[a.id] = "";
		if (a.value && a.value.length >= 2) {
			var b = ti.fGetStopsByName(a.value);
			b.length > 0 && (a.value != b[0].name && (a.value = b[0].name), pg.stopsSuggestedForText = b[0].name, pg[a.id] = b[0].id, a.id === "inputStop" && pg.fLoadDepartingRoutes())
		}
	}
}, pg.fTabStop_Click = function (a) {
	pg.fUrlSet({transport: "stop", inputStop: a || pg.inputStop, hashForMap: pg.hashForMap && "map"});
	return !1
}, pg.fTabPlanner_Click = function (a, b) {
	pg.fUrlSet({
		transport: "plan",
		inputStart: a || pg.inputStart || pg.inputStop,
		inputFinish: b || pg.inputFinish,
		hashForMap: pg.hashForMap && "map"
	});
	return !1
}, pg.fTabActivate = function () {
	var a = pg.city + "_" + pg.transport;
	pg.transport || (a = "city", cfg.cities[pg.city] && pg.city !== pg.fGetCity(pg.city) && (a = "region"));
	var b = $("divNav") && $("divNav").getElementsByTagName("a");
	if (b)for (var c = b.length; --c >= 0;)b[c].id === a ? b[c].className = "active" : b[c].className.indexOf("active") >= 0 && (b[c].className = "");
	($("dt_stop") || {}).className = pg.transport === "stop" ? "active" : "";
	if (pg.transport === "stop")pg.loadedDepartingRoutes !== pg.inputStop && setTimeout(pg.fLoadDepartingRoutes, 10); else if (pg.transport === "plan") {
		typeof mobile == "undefined" && (($("plan") || {}).className = "active"), pg.loadedPlannerParams !== pg.inputStart + "/" + pg.inputFinish && setTimeout(pg.fLoadPlannerTab, 10);
		var d = "" + (($("inputTime") || {}).value || "");
		d.trim() === "" && (d = ti.dateToMinutes(new Date) % 1440, ($("inputTime") || {}).value = ti.printTime(d))
	} else if (!pg.loadedRoutesHash || pg.loadedRoutesHash.indexOf(pg.city + "/" + pg.transport + "/") != 0)($("inputRoutes") || {}).value = pg.routesFilter = "", pg.inputRoutes_Blur(), pg.fLoadRoutesList();
	($("divContentRoutes") || {style: {}}).style.display = pg.transport === "stop" || pg.transport === "plan" ? "none" : "block", ($("divContentDepartingRoutes") || {style: {}}).style.display = pg.transport === "stop" ? "block" : "none", ($("divContentPlanner") || {style: {}}).style.display = pg.transport === "plan" ? "block" : "none"
}, pg.fLoadRoutesList = function (a) {
	if (typeof mobile == "undefined" || typeof a != "undefined") {
		var b = $("divContentRoutesResults");
		if (typeof ti.routes !== "object") {
			pg.loadedRoutesHash = "", (b || {}).innerHTML = "<br/>" + i18n.receivingData, setTimeout(function () {
				pg.fLoadRoutesList(a)
			}, 200);
			return
		}
		var c = $("inputRoutes") && ($("inputRoutes").className == "empty" ? "" : ti.toAscii($("inputRoutes").value, 2));
		if (c && pg.routesFilter != c) {
			pg.routesFilter = c, setTimeout(function () {
				pg.fLoadRoutesList(a)
			}, 200);
			return
		}
		pg.routesFilter = c;
		if (pg.loadedRoutesHash == pg.city + "/" + pg.transport + "/" + c && typeof mobile == "undefined")return;
		pg.loadedRoutesHash = pg.city + "/" + pg.transport + "/" + c;
		var d = ti.fGetRoutes(pg.city, pg.transport, null, null, null, c || (cfg.city.showAllDirections ? "*" : ""));
		a && a(d);
		if (!d || !d.length) {
			(b || {}).innerHTML = "<br/>&nbsp;" + i18n.noRoutesFound;
			return
		}
		var e = function () {
			var a = [];
			a.push("<table id=\"tblRoutes\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tbody>");
			for (var b = 0; b < d.length; b++)a.push(pg.fMakeRouteRowHTML(d[b], "tblRoutes", b));
			a.push("</tbody></table><br/>");
			var c = cfg.cities[pg.city].footer;
			c = c && (c[pg.language] || c.en), c && a.push(c);
			if (!cfg.isMobilePage) {
				cfg.programmedBy && a.push("<p id=\"programmedBy\" class=\"smalltext graytext\">" + (cfg.programmedBy[pg.language] || cfg.programmedBy.en || "") + "</p>");
				var e = cfg.cities[cfg.defaultCity].webcounter;
				e && (a.push("<a id=\"webcounter\" href=\"http://whos.amung.us/stats/" + e + "\" target=\"_blank\" style=\"float:right; position:relative; bottom:20px; padding:10px;\">"), a.push("<img width=\"80\" height=\"15\" border=\"0\" title=\"web tracker\" alt=\"web tracker\" src=\"http://whos.amung.us/swidget/" + e + ".gif\"></a>"))
			}
			pg.replaceHtml($("divContentRoutesResults"), a.join(""))
		};
		if (pg.browserVersion <= 8 && d.length > 25 && !c) {
			(b || {}).innerHTML = "<br/>" + i18n.loading, setTimeout(e, 100);
			return
		}
		e()
	}
}, pg.fLoadDepartingRoutes = function (a, b) {
	pg.loadedDepartingRoutes = null, pg.realTimeDepartures.timer && a && (clearTimeout(pg.realTimeDepartures.timer), pg.realTimeDepartures.timer = 0);
	var c = $("divContentDepartingRoutesResults");
	pg.inputStop && pg.stopsByIP && pg.inputStop.indexOf("192.168.") >= 0 && cfg.city.defaultTransport == "tablo" && (pg.IP = pg.inputStop, pg.tablo = pg.stopsByIP[pg.IP] || {stops: "unknown"}, pg.inputStop = pg.tablo.stops, reloadTabloContent());
	var d = ti.fGetAnyStopDetails(pg.inputStop);
	if (a)var e = [];
	if (cfg.city.defaultTransport != "tablo")if (d.id)$("inputStop") && ($("inputStop").value = pg.inputStopText = d.name || "", $("inputStop").className = ""), pg.startStop || (pg.startStop = pg.inputStop); else if (c && !pg.inputStop && typeof ti.stops == "object") {
		var f = pg.fUrlSet({hashForMap: "map"}, !0);
		$("divContentDepartingRoutesHeader").style.display = "none", (c || {}).innerHTML = ("<p class=\"help\">" + i18n.searchDeparturesHelp + "<p/><p class=\"help\">" + i18n.tripPlannerHelpMap).replace(/<a>/g, "<a class=\"underlined map\" href=\"#" + f + "\">"), document.activeElement && document.activeElement.id !== "inputStop" && ($("inputStop").value = i18n.startStop, $("inputStop").className = "empty", setTimeout(function () {
			try {
				$("inputStop").focus()
			} catch (a) {
			}
		}, 100));
		return
	}
	if (typeof ti.routes !== "object" || typeof ti.stops !== "object")cfg.city.defaultTransport != "tablo" && ((c || {}).innerHTML = "<br/>" + i18n.receivingData), setTimeout(function () {
		pg.fLoadDepartingRoutes(a, b)
	}, 200); else {
		pg.loadedDepartingRoutes = pg.inputStop, pg.stopsSuggestedForText = d.name;
		if (cfg.city.defaultTransport == "tablo")($("spanContentDepartingRoutesStop") || {}).innerHTML = d.name || "", d.name && d.name.length >= 30 && (($("spanContentDepartingRoutesStop") || {style: {}}).style.fontSize = "54px"), ($("spanDepartureDate") || {}).innerHTML = ti.printTime(ti.dateToMinutes(new Date)); else {
			var g = (d.street ? ", " + d.street : "") + (d.area && !cfg.cities[pg.city].skipStopArea ? ", " + d.area : "") + (d.city && !cfg.cities[pg.city].skipStopCity ? ", " + d.city : "");
			d[cfg.cities[pg.city].stopFareZone || "noFareZone"] && (g += ", " + i18n.fareZone + " " + d[cfg.cities[pg.city].stopFareZone]), g = g.length > 0 ? "<span class=\"details\"> (" + g.substring(2) + ")</span>" : "";
			var h = [], f = pg.fUrlSet({hashForMap: "map"}, !0), i = pg.transfers = ti.fGetRoutesAtStop(pg.inputStop, !1), j = {}, k = null, h = [];
			for (var l = 0; l < i.length; l++) {
				var m = i[l], n = ti.toAscii([m.city, m.transport, m.num].join(","), !0);
				if (j[n])continue;
				var o = {
					city: m.city,
					transport: m.transport,
					num: ti.toAscii(m.num, !0),
					dirType: m.dirType,
					stopId: m.stopId
				};
				j[n] = o;
				var p = pg.fUrlSet({schedule: o}, !0);
				k !== m.transport && (k = m.transport, h.push(" <span class=\"icon icon_narrow icon_" + m.transport + "\" data-transport=\"" + m.transport + "\"></span>&nbsp;"));
				var q = "<a class=\"hover transfer" + k + "\" href=\"#" + p + "\" title=\"" + (m.name || "").replace(/"/g, "") + "\">" + i[l].numHTML.replace(/\s/g, "&nbsp;") + "</a> ";
				h.push(q)
			}
			h.push("<span style=\"display:inline-block; width:2px;\"></span>"), ($("spanContentDepartingRoutesStop") || {}).innerHTML = "<a href=\"#" + f + "\" class=\"icon icon_map\" title=\"" + i18n.showInMap + "\"></a>" + i18n.stop + " <strong>" + d.name + "</strong>" + g + h.join("") + "<br />"
		}
		h = [];
		var r = new Date, s = -1;
		typeof mobile != "undefined" && typeof b != "undefined" ? s = b : $("inputDepartureDate") && (s = +$("inputDepartureDate").value);
		var t = {start_stops: pg.inputStop, finish_stops: "", transport: {}};
		s < 0 ? (t.date = r, startTime = ti.dateToMinutes(r) % 1440) : (t.date = new Date(r.getFullYear(), r.getMonth(), r.getDate() + s), startTime = -1);
		var u = dijkstra(t, cfg.city.defaultTransport == "tablo" || typeof mobile != "undefined" ? startTime : 0 * startTime, 0);
		if (cfg.city.urlVehicleDepartures && s < 0) {
			c && ($("divContentDepartingRoutesHeader").style.display = "", (c || {}).innerHTML = "<br/>" + i18n.loading), pg.departuresBySchedule = u, pg.fProcessVehicleDepartures(null, a);
			return
		}
		if (!u || !u.length) {
			(c || {}).innerHTML = "<br/>" + i18n.noDepartingRoutes;
			return
		}
		if (cfg.city.defaultTransport == "tablo")pg.fMakeTabloHTML(u); else {
			h.push("<table id=\"tblDepartingRoutes\" cellpadding=\"0\" cellspacing=\"0\"><tbody>");
			for (var v = 0, w = 0, x = ""; v < u.length; v++) {
				var m = u[v].route, y = ti.toAscii(m.city + ";" + m.transport + ";" + m.num + ";" + m.dirType.split("_")[0].slice(-2), !0);
				x != y ? (x = y, w = v, u[w].route.departures = [u[v].start_time], u[w].route.tripNums = [u[v].tripNum]) : (u[w].route.departures.push(u[v].start_time), u[w].route.tripNums.push(u[v].tripNum))
			}
			for (var v = 0, w = 0; v < u.length; v++)u[v].route.departures && (u[v].route.departures.sort(function (a, b) {
				if (a < b)return -1;
				if (a > b)return 1;
				return 0
			}), u[v].route.departures[0] < 0 && u[v].route.num.indexOf("(") >= 0 ? v = v : (a ? e.push(pg.fMakeRouteRowHTML(u[v].route, "tblDepartingRoutes", w, startTime, !0)) : h.push(pg.fMakeRouteRowHTML(u[v].route, "tblDepartingRoutes", w, startTime)), ++w));
			c && ((c || {}).innerHTML = h.join("") + "</tbody></table>", $("divContentDepartingRoutesHeader").style.display = ""), a && a(e)
		}
	}
}, pg.fMakeTabloHTML = function (a) {
	if (pg.inputStop == "unknown")setTimeout(function () {
		location.reload(!0)
	}, 3e4), $("divContentDepartingRoutes").innerHTML = "<p style=\"font-size:96px; padding-top:128px; text-align:center; text-transform:none;\">Vyksta sistemos derinimo<br />darbai</p><div style=\"position:absolute; bottom:20px; right:20px; color:#606060; font-size:34px; text-transform:none;\"><img src=\"" + pg.imagesFolder + "sisp_logo.png\" /><br />www.vilniustransport.lt</div>"; else {
		var b = [];
		for (var c = a.length; --c >= 0;) {
			var d = a[c].route;
			if (d.departures)for (var e = d.departures.length; --e >= 0;)b.push({route: d, start_time: d.departures[e]})
		}
		b.sort(function (a, b) {
			if (a.start_time < b.start_time)return -1;
			if (a.start_time > b.start_time)return 1;
			return ti.naturalSort(a.route.num, b.route.num)
		});
		var f = new Date, g = (+f - f.setHours(0, 0, 0, 0)) / 1e3;
		for (var h = 0; h < 6;) {
			var i = {}, j = !1;
			for (var c = 0; c < b.length; c++) {
				var k = b[c], d = k.route, l = ti.toAscii(d.city + ";" + d.transport + ";" + d.num + ";", !0);
				if (i[l] || k.html || k.start_time < 0)continue;
				if (k.start_time <= g - 30)continue;
				i[l] = !0;
				var m = d.name, n = ["вЂ“", "вЂ”", "- ", " -", "-"];
				for (var o = 0; o < n.length; ++o)e = m.lastIndexOf(n[o]), e >= 0 && (m = m.substring(e + 1).trim());
				j = !0, k.html = "<tr><td><div><span class=\"icon icon_" + d.transport + "\"></span></div></td><td><div class=\"num " + d.transport + "\">" + d.numHTML + "</div></td><td class=\"name\"><div>" + m + "</div></td><td class=\"time\"><div>", k.start_time < g + 3600 ? k.html += Math.floor((k.start_time - g + 30) / 60) + " min" : k.html += ti.printTime(Math.floor((k.start_time + 30) / 60)), k.html += "</div></td></tr>";
				if (++h >= 6)break
			}
			if (!j)break
		}
		var p = "<table id=\"tblDepartingRoutes\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><th colspan=\"2\">MarЕЎrutas</th><th class=\"name\">Kryptis</th><th class=\"time\">Laikas</th></tr>";
		for (var c = 0; c < b.length; c++)b[c].html && (p += b[c].html);
		p += "</tbody></table>", ($("spanDepartureDate") || {}).innerHTML = ti.printTime(ti.dateToMinutes(new Date)), ($("divContentDepartingRoutesResults") || {}).innerHTML = p, ($("divContentDepartingRoutesHeader") || {style: {}}).style.display = ""
	}
}, pg.fProcessVehicleDepartures = function (a, b) {
	if (typeof mobile == "undefined" || typeof b != "undefined") {
		if (pg.realTimeDepartures.timer) {
			clearTimeout(pg.realTimeDepartures.timer), pg.realTimeDepartures.timer = 0;
			if (typeof mobile != "undefined" && ["favourites", "schedule4", "schedule5", "stop"].indexOf(mobile.current_page) == -1)return
		}
		if (!cfg.city.urlVehicleDepartures)return;
		pg.realTimeDepartures.timer = setTimeout(function () {
			pg.fProcessVehicleDepartures(null, b)
		}, 1e4);
		if (cfg.isApp && a && typeof a == "object") {
			b(a);
			return
		}
		if (typeof a !== "string") {
			if (typeof mobile == "object" && mobile.current_page == "favourites")var c = (pg.favouriteStops || "").split(","); else var c = typeof mobile == "object" && pg.schedule && pg.schedule.stopId ? [pg.schedule.stopId] : (pg.inputStop || "").split(",");
			if (c.join(",").indexOf(";") != -1)return;
			pg.stopsBySiriID = {};
			for (var d = 0; d < c.length; ++d) {
				var e = (ti.stops[c[d]] || {}).siriID || 0;
				e && (pg.stopsBySiriID[e] = c[d], c[d] = e)
			}
			ti.fDownloadUrl("GET", cfg.city.urlVehicleDepartures + "?stopid=" + c.join(",") + "&time=" + +(new Date), function (a) {
				pg.fProcessVehicleDepartures(a, b)
			}, cfg.city.defaultTransport == "tablo" ? !0 : !1);
			return
		}
		if (a && a.substring(0, 6) == "reload") {
			location.reload(!0);
			return
		}
		a = a || pg.realTimeData || "", pg.realTimeData = a, a = a.split("\n");
		if (pg.realTimeDepartures.$mapPopup)if (!pg.realTimeDepartures.mapStop)if (pg.realTimeDepartures.vehicleID) {
			var f = {};
			for (var d = 0; d < a.length; d++) {
				var g = a[d].trim().split(",");
				while (g[g.length - 1] == "")g.pop();
				if (g.length < 2)continue;
				var h = ({1: "trol", 3: "tram", 4: "minibus"})[g[0]] || "bus";
				f[h + ";" + g[3]] = {
					transport_number: g[0],
					route_number: g[1],
					direction_type: g[2].replace(/>/g, "-"),
					stops: g.slice(4)
				}
			}
			var i = f[pg.realTimeDepartures.vehicleTransport + ";" + pg.realTimeDepartures.vehicleID], j = "<br/>No data available";
			if (i) {
				j = [];
				for (var d = 0; d < i.stops.length - 1; d += 2)j.push(ti.printTime((+i.stops[d + 1] + 30) / 60) + "&nbsp;&nbsp;<a href=\"#stop/" + i.stops[d] + "/map\">" + ti.fGetStopDetails(i.stops[d]).name) + "</a>";
				j = j.join("</br>")
			}
			var k = pg.realTimeDepartures.vehicleTransport, l = pg.realTimeDepartures.vehicleRouteNum, m = ti.fGetRoutes(cfg.defaultCity, k, l, i ? i.direction_type : !1), n = "";
			m && m.length && m[0].name && (n = "<strong>" + m[0].name + "</strong></br>");
			var o = "<div class=\"baloon_close\"></div><div class=\"baloon_content\"><span class=\"baloon_title\"><span class=\"icon icon_" + k + "\"></span><span class=\"num num3 " + k + "\">" + (l || "?") + "</span>" + pg.realTimeDepartures.vehicleID + "</span><br/>" + n + "<div style=\"padding:8px 20px 0 0; height:150px; overflow-y:auto; overflow-x:hidden;\">" + j + "</div></div>";
			pg.openMapInfoWindow(o), pg.realTimeDepartures.vehicleID = null
		}
		if (pg.transport == "stop" && pg.loadedDepartingRoutes && +($("inputDepartureDate") || {value: 0}).value === -1 || cfg.city.defaultTransport == "tablo" && pg.loadedDepartingRoutes || typeof mobile == "object" && pg.schedule && pg.schedule.stopId && pg.loadedDepartingRoutes || typeof mobile == "object" && pg.loadedDepartingRoutes && pg.transport === "stop" && jQuery("#stop .nav-departures li.active").attr("data-departure") == "-1" || typeof mobile == "object" && mobile.current_page == "favourites" && pg.loadedDepartingRoutes) {
			var p = {}, q = pg.loadedDepartingRoutes.split(",");
			for (var d = 0; d < q.length; ++d)p[q[d]] = !0;
			var r = [];
			r.push("<table id=\"tblDepartingRoutes\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tbody>");
			var s = {}, t = [], u = ti.dateToMinutes(new Date, !0) % 1440;
			u < 180 && (u += 1440);
			if (cfg.defaultCity != "rostov") {
				for (var d = 0; d < a.length; d++) {
					var g = a[d].trim().split(","), v, w;
					if (d == 0 && g[0] !== "stop")continue;
					if (g.length >= 2 && g[0] === "stop") {
						B = g[1], cfg.defaultCity == "tallinna-linn" && (B = pg.stopsBySiriID[B]);
						continue
					}
					if (g.length <= 3)continue;
					if (cfg.defaultCity === "tallinna-linn") {
						v = {city: cfg.defaultCity, transport: g[0], num: g[1], name: g[4] || ""}, w = +g[2];
						for (var x = 0; x < pg.transfers.length; ++x)if (v.num === pg.transfers[x].num && v.transport === pg.transfers[x].transport && v.city === pg.transfers[x].city && B === pg.transfers[x].stopId) {
							v = pg.transfers[x];
							break
						}
					} else {
						v = (ti.fGetRoutes(cfg.defaultCity, g[0], g[1], g[2], !0) || [])[0];
						if (!v)continue;
						w = +g[3]
					}
					cfg.city.defaultTransport != "tablo" && (w = w / 60);
					var y = ti.toAscii(v.city + ";" + v.transport + ";" + v.num + ";" + v.name, !0);
					typeof mobile != "undefined" && (y += ";" + B);
					var z = s[y];
					w < u - .5 ? w = w : z ? (cfg.defaultCity != "tallinna-linn" || w <= u + 30) && z.route.departures.push(w) : (v.departures = [w], v.stopId = B, s[y] = {route: v})
				}
				for (var y in s) {
					var v = s[y];
					v.route.departures.sort(function (a, b) {
						return a - b
					}), v.start_time = v.route.departures[0], t.push(v)
				}
				t.sort(function (a, b) {
					return ti.naturalSort(a.route.num, b.route.num)
				})
			} else {
				for (var d = 0; d < a.length; d++) {
					var g = a[d].trim().split(","), A = g.length - 1;
					if (A < 4)continue;
					var v = {
						city: cfg.defaultCity,
						transport: ({1: "trol", 3: "tram", 4: "minibus"})[g[0]] || "bus",
						num: g[1],
						name: g[2].replace(/>/g, "-")
					}, m = ti.fGetRoutes(v.city, v.transport, v.num, v.name) || [];
					v = m[0] || v;
					var y = ti.toAscii(v.city + ";" + v.transport + ";" + v.num + ";" + v.name, !0);
					for (var x = 4; x < A; x += 2) {
						var B = g[x];
						if (p[B] && g[x + 1]) {
							var z = s[y], C = +g[x + 1];
							z ? z.route.departures.push(C) : (v.departures = [C], v.stopId = B, s[y] = {route: v})
						}
					}
				}
				for (var y in s) {
					var v = s[y];
					v.route.departures.sort(), t.push(v)
				}
				t.sort(function (a, b) {
					if (a.route.departures[0] < b.route.departures[0])return -1;
					if (a.route.departures[0] > b.route.departures[0])return 1;
					return 0
				})
			}
			if (typeof mobile == "object" && b) {
				var D = [];
				for (var d = 0; d < t.length; d++)D[d] = pg.fMakeRouteRowHTML(t[d].route, "tblDepartingRoutes", 0, u, !0), D[d].timetowait = Math.floor(t[d].route.departures[0] - u + .5);
				b(D)
			} else if (cfg.city.defaultTransport == "tablo")pg.fMakeTabloHTML(t); else {
				for (var d = 0; d < t.length; d++)r.push(pg.fMakeRouteRowHTML(t[d].route, "tblDepartingRoutes", 0, -1));
				($("divContentDepartingRoutesResults") || {}).innerHTML = r.join("") + "</tbody></table>"
			}
		}
	}
}, pg.fLoadPlannerTab = function (a, b) {
	a === !0 && (pg.optimalResults = null, pg.loadedPlannerParams = null, pg.hashForMap = "");
	if (b)var c = {errors: []};
	var d = "" + ((typeof mobile != "undefined" ? jQuery(".inputTime").val() : ($("inputTime") || {}).value) || "");
	d === "" ? (d = ti.dateToMinutes(new Date) % 1440, ($("inputTime") || {}).value = ti.printTime(d), typeof mobile != "undefined" && jQuery(".inputTime").val(ti.printTime(d))) : d = ti.toMinutes(d);
	var e = ti.fGetAnyStopDetails(pg.inputStart), f = ti.fGetAnyStopDetails(pg.inputFinish);
	f.id ? (($("inputFinish") || {}).value = f.name || "", ($("inputFinish") || {}).className = "") : !pg.inputFinish && typeof ti.stops == "object" && (($("divContentPlannerResults") || {}).innerHTML = i18n.typeFinishStop, b && c.errors.push({text: i18n.typeFinishStop}), document.activeElement && document.activeElement.id !== "inputFinish" && (($("inputFinish") || {}).value = i18n.finishStop, ($("inputFinish") || {}).className = "empty"));
	if (e.id)($("inputStart") || {}).value = e.name || "", ($("inputStart") || {}).className = ""; else if (!pg.inputStart || typeof ti.stops == "object")($("divContentPlannerResults") || {}).innerHTML = i18n.typeStartStop, b && c.errors.push({text: i18n.typeStartStop}), document.activeElement && document.activeElement.id !== "inputStart" && (($("inputStart") || {}).value = i18n.startStop, ($("inputStart") || {}).className = "empty");
	if (typeof ti.routes !== "object" || typeof ti.stops !== "object")($("divContentPlannerResults") || {}).innerHTML = "<br/>" + i18n.receivingData, setTimeout(function () {
		pg.fLoadPlannerTab(!1, b)
	}, 200); else {
		if (!pg.inputStart && !pg.inputFinish || (pg.loadedPlannerParams || "").indexOf("clear") >= 0) {
			pg.loadedPlannerParams = pg.inputStart + "/" + pg.inputFinish, pg.optimalResults = null, pg.hashForMap && pg.hashForMap != "map" && (pg.map = {}, pg.hashForMap = "map", pg.fMapShow());
			var g = pg.fUrlSet({hashForMap: "map"}, !0);
			($("divContentPlannerResults") || {}).innerHTML = "<p class=\"help\">" + i18n.tripPlannerHelp + "</p><p class=\"help\">" + i18n.tripPlannerHelpMap.replace(/<a>/g, "<a class=\"underlined map\" href=\"#" + g + "\">") + "</p>", b && (c.errors.push({text: i18n.tripPlannerHelp}), b(c));
			return
		}
		pg.loadedPlannerParams = pg.inputStart + "/" + pg.inputFinish;
		if (!e.id || !f.id) {
			b && b(c);
			return
		}
		var h = new Date, i = new Date(h.getFullYear(), h.getMonth(), h.getDate() + +(typeof mobile != "undefined" ? jQuery(".inputDate").val() : $("inputDate").value)), j = {
			start_stops: pg.inputStart,
			finish_stops: pg.inputFinish,
			reverse: typeof mobile != "undefined" ? parseInt(jQuery(".inputReverse").val(), 10) : parseInt($("inputReverse").value, 10),
			date: i,
			start_time: d,
			lowFloor: typeof mobile != "undefined" ? jQuery(".checkHandicapped").is(":checked") : $("checkHandicapped").checked,
			transport: {},
			route_nums: ((typeof mobile != "undefined" ? jQuery(".inputRoutesFilter").val() : ($("inputRoutesFilter") || {}).value) || "").trim(),
			walk_speed_kmh: typeof mobile != "undefined" ? parseInt(jQuery(".inputWalkSpeed").val() || 4, 10) : parseInt($("inputWalkSpeed").value || 4, 10),
			walk_max: typeof mobile != "undefined" ? jQuery(".inputWalkMax").val() : $("inputWalkMax").value,
			change_time: typeof mobile != "undefined" ? parseInt(jQuery(".inputChangeTime").val() || 3, 10) : parseInt($("inputChangeTime").value || 3, 10),
			callback1: b || (typeof mobile != "undefined" ? mobile.renderPlannerResults : pg.fPrintOptimalTrips),
			callback: b || (typeof mobile != "undefined" ? mobile.renderPlannerResults : pg.fPrintOptimalTrips)
		}, k = pg.fGetCity(pg.city);
		for (var l = 1; l <= 2; l++) {
			for (var m = 0, n = cfg.cities[k].transport; m < n.length; m++)j.transport[n[m]] = typeof mobile != "undefined" ? jQuery(".checkbox" + n[m]).is(":checked") : ($("checkbox" + n[m]) || {checked: !0}).checked;
			k = cfg.cities[k].region;
			if (!k || !cfg.cities[k])break
		}
		($("divContentPlannerResults") || {}).innerHTML = "<br/>" + i18n.calculating;
		if (typeof mobile == "undefined" || typeof mobile != "undefined" && pg.hashForMap || a)typeof mobile != "undefined" && jq("#loading").show(), setTimeout(function () {
			ti.findTrips(j)
		}, 100)
	}
}, pg.fPrintOptimalTrips = function (a, b, c) {
	var d = pg.optimalResults = a.results;
	pg.map = {};
	var e = [];
	for (var f = 0; f < d.length; f++) {
		var g = d[f], h = d[f].legs, i = [], j = [];
		for (var k = 0; k < h.length; k++) {
			var l = h[k], m = l.route, n = (h[k + 1] || {route: null}).route;
			if (m && m.transport) {
				n && m.city === n.city && m.transport === n.transport && m.num === n.num && (cfg.defaultCity != "intercity" && (l.finish_stop.name = h[k + 1].finish_stop.name, l.finish_time = h[k + 1].finish_time, ++k)), j.push("<span class=\"icon icon_narrow icon_" + m.transport + "\" title=\"" + i18n.transport1[m.transport] + " " + m.num + " " + i18n.towards + " " + m.name + "\"></span>"), g.direct_trip && m.num.length <= 8 && (j.push("<span class=\"num num" + Math.min(l.route.num.length, 4) + " " + l.route.transport + "\">" + l.route.numHTML + "</span>"), l.online_data && j.push(" " + l.online_data.code + " " + l.online_data.departureAsStr + " &rarr; " + l.online_data.arrivalAsStr)), m.stopId = l.start_stop.id, m.tripNum = (l.trip_num || -1) + 1;
				var o = pg.fUrlSet({schedule: m, mapHash: ""}, !0), p = l.finish_time - l.start_time;
				p = p >= 60 ? ti.printTime(p) : p + "&nbsp;" + i18n.minutesShort, i.push("<p class=\"results\"><span class=\"icon icon_" + l.route.transport + "\"></span><span class=\"num num" + Math.min(l.route.num.length, 4) + " " + l.route.transport + "\">" + l.route.numHTML + "</span>" + (cfg.searchOnly ? "" : "<a class=\"hover\" href=\"#" + o + "\" title=\"" + i18n.showSchedule + "\">") + i18n.transport1[l.route.transport] + " " + i18n.towards + "&nbsp;" + l.route.name + (cfg.searchOnly ? "" : "</a>") + " <br/><strong>" + (cfg.defaultCity == "xxxvilnius2" ? "" : ti.printTime(l.start_time)) + (l.online_data ? "(" + l.online_data.departureAsStr + ")" : "") + " " + l.start_stop.name + (l.start_platform && "(" + l.start_platform + ")" || "") + "</strong> &rarr; " + (cfg.defaultCity == "xxxvilnius2" ? "" : ti.printTime(l.finish_time)) + (l.online_data ? "(" + l.online_data.arrivalAsStr + ")" : "") + " " + l.finish_stop.name + (l.finish_platform && "(" + l.finish_platform + ")" || "") + (cfg.defaultCity == "xxxvilnius2" ? "" : "<span class=\"graytext\"> (" + i18n.ride + " " + p + (cfg.city.has_trips_ids ? " trip ID=" + l.trip_id + (l.trip_date ? "(" + l.trip_date.yyyymmdd("-") + ")" : "") + ", trip num=" + l.trip_code + (l.online_data ? ", bezrindas trip ID=" + l.online_data.code : "") : "") + (cfg.city.has_trips_ids === 2 ? ", trip operator=" + l.trip_operator + ", trip group=" + l.trip_group : "") + ")</span>") + "</p>")
			} else {
				if (l.start_time == l.finish_time && parseInt(l.start_stop.id, 10) == parseInt(l.finish_stop.id, 10))continue;
				j.push("<span class=\"icon icon_narrow icon_walk\" title=\"" + i18n.walk + " " + (l.finish_time - l.start_time) + "&nbsp;" + i18n.minutesShort + "\"></span>"), i.push("<p class=\"results\"><span class=\"icon icon_walk\"></span><strong>" + (cfg.defaultCity == "xxxvilnius2" ? "" : ti.printTime(l.start_time)) + " " + l.start_stop.name + "</strong> &rarr; " + (cfg.defaultCity == "xxxvilnius2" ? "" : ti.printTime(l.finish_time)) + " " + l.finish_stop.name + "<span class=\"graytext\"> (" + i18n.walk + " " + (l.finish_time - l.start_time) + "&nbsp;" + i18n.minutesShort + ")</span></p></a>")
			}
			if (l.taxi)for (var q = 0; q < l.taxi.length; ++q) {
				var r = l.taxi[q];
				i.push((q ? "<br />" : "") + "km: " + r.km + ", " + r.name + ", phone: " + r.phone)
			}
		}
		e.push("<div" + (f % 2 ? "" : " class=\"grey\"") + " style=\"border-bottom: solid 1px gray; padding:5px 0 5px 5px;\"><table><tbody><tr><td><a href=\"\" onclick=\"return false;\" title=\"" + (f ? i18n.showDetails : i18n.hideDetails) + "\" class=\"" + (f ? "expand" : "collapse") + "\"><span class=\"icon\"></span><strong class=\"hover\">" + i18n.option + "&nbsp;" + (f + 1) + ".</strong></a> <a href=\"#" + pg.city + "/" + pg.transport + "/map,,," + (f + 1) + "\" class=\"icon icon_map\" title=\"" + i18n.showInMap + "\"></a> " + (cfg.defaultCity == "xxxvilnius2" ? "" : ti.printTime(g.start_time, null, "&#x2007;") + " &mdash; " + ti.printTime(g.finish_time, null, "&#x2007;")) + ",</td><td style=\"white-space:pre-wrap;\">" + (cfg.defaultCity == "xxxvilnius2" ? "" : i18n.travelDuration + "&nbsp;<strong>" + ti.printTime(g.travel_time)) + "</strong>  <span style=\"white-space:nowrap;\">" + j.join("") + "</span></td></tr></tbody></table><div class=\"RouteDetails\" style=\"" + (f ? "display:none;" : "") + "\">"), e.push(i.join("") + "</a></div></div>")
	}
	if (d.length > 0) {
		pg.fTogglePlannerOptions(!1), b && document.body.className.indexOf("Map") >= 0 && (pg.mapShowAllStops = !1, pg.fUrlSetMap({optimalRoute: 1}));
		if (cfg.defaultCity === "latvia") {
			ti.TimeZoneOffset = 2;
			var s = "http://routelatvia.azurewebsites.net/?";
			s += "origin=" + a.start_stops, s += "&destination=" + a.finish_stops, s += "&departure_time=" + ti.toUnixTime(a.date, a.start_time), e.push("<br/><a target=\"_blank\" href=\"" + s + "\">" + s + "</a>"), e.push("<div id=\"online_results\">"), a.online_query_url ? e.push("<a target=\"_blank\" href=\"" + a.online_query_url + "\">" + a.online_query_url + "</a>") : b || e.push("<br/>Calculating alternative routes...");
			if (a.online_results_JSON) {
				var d = JSON.parse(a.online_results_JSON);
				e.push("<div style=\"white-space:pre;\">", JSON.stringify(d, null, 4), "</div>")
			}
			e.push("</div>")
		}
	} else e.push("<br/>" + i18n.noOptimalRoutes);
	var t = $("divContentPlannerResults");
	(t || {}).innerHTML = e.join("")
}, pg.fMakeRouteRowHTML = function (a, b, c, d, e) {
	var f, g = "map," + a.city + "," + a.transport + "," + a.num;
	cfg.city.showAllDirections && (g = g + "," + a.dirType), g = ti.toAscii(g, !0), b == "tblRoutes" ? (f = pg.fUrlSet({
		schedule: {
			city: a.city,
			transport: a.transport,
			num: a.num,
			dirType: a.dirType
		}, hashForMap: ""
	}, !0), pg.routesFilter && (g += "," + a.dirType), g = pg.fUrlSet({hashForMap: g}, !0)) : (f = pg.fUrlSet({
		schedule: {
			city: a.city,
			transport: a.transport,
			num: a.num,
			dirType: a.dirType,
			stopId: a.stopId
		}, hashForMap: ""
	}, !0), g = pg.fUrlSet({hashForMap: g + "," + a.dirType + "," + a.stopId}, !0));
	var h = "<a style=\"display:inline-block\" href=\"#" + f + "\" title=\"" + i18n.showSchedule + "\">", i = "";
	for (var j = 1; j <= 7; j++)if ((a.weekdays || "").indexOf(j) < 0)i += "<span class=\"blankday\" title=\"" + i18n["weekdays" + j] + ": " + i18n.routeNotOperate + "\">" + i18n.weekdaysShort[j] + "</span>"; else {
		var k = a.validityPeriods[j - 1];
		k = k ? ": " + i18n.validFrom + " " + pg.formatDate(k) : "", i += "<span" + (j >= 6 ? "" : " class=\"weekend\"") + " title=\"" + i18n["weekdays" + j] + k + "\">" + i18n.weekdaysShort[j] + "</span>"
	}
	cfg.city.planHandicappedOption !== !1 && (a.weekdays && a.weekdays.indexOf("z") >= 0 && (i += "<img src=\"" + pg.imagesFolder + "handicapped.png\" alt=\"low floor\" title=\"" + i18n.lowFloorVehicles + "\" />")), a.weekdays && a.weekdays.indexOf("s") >= 0 && (i += "<img src=\"" + pg.imagesFolder + "minibus_16_1E90FF.png\" alt=\"small bus\" title=\"" + i18n.smallBus12Service + "\" />"), a.weekdays && a.weekdays.indexOf("b") >= 0 && (i += "<img src=\"" + pg.imagesFolder + "bicycle16.png\" alt=\"transfer bicycle\" title=\"" + i18n.transferBicycles + "\" />");
	var l = h + (!0 || b == "tblDepartingRoutes" ? "" : "<span class=\"icon icon_expand\" title=\"" + i18n.showDetails + "\"></span>") + "<span class=\"icon icon_" + a.transport + "\"></span>";
	a.transport == "train" || a.transport == "metro" ? l += "<span style=\"display:none;\">" + a.num + "</span>" : l += "<span class=\"num num" + Math.min(a.num.length, 4) + " " + a.transport + "\">" + a.numHTML + "</span>";
	if (e)var m = {route: a, hash: f, times: [], notes: []};
	var n = "<span class=\"hover\">" + a.name + ((a.commercial || "").indexOf("E") >= 0 ? " (" + i18n.express + ")" : "") + "</span>";
	n = "<tr" + (b != "tblDepartingRoutes" && c % 2 != 0 ? " class=\"white\"" : "") + "><td class=\"routeName\"><a class=\"icon icon_map\" title=\"" + i18n.showInMap + "\" href=\"#" + g + "\"></a>" + l + n + "</a>", n += "</td><td class=\"weekdays\"><a href=\"#" + f + "\">" + i + "</a></td><td class=\"lastcol\"></td></tr>";
	if (b === "tblDepartingRoutes") {
		if (cfg.city.doNotShowTimetables && cfg.city.doNotShowTimetables[a.transport] && a.departures.length && a.departures[0] >= 0)if (("," + pg.inputStop + ",").indexOf("," + a.stops[0] + ",") < 0) {
			n += "<tr class=\"white\"><td class=\"DeparturesRow\" colspan=\"4\">", n += "</td></tr>";
			return n
		}
		n += "<tr class=\"white\"><td class=\"DeparturesRow\" colspan=\"4\"><span><span class=\"icon icon_collapse\"></span><span class=\"icon";
		var o = Infinity, p = Infinity, q = 0, r = 18;
		for (var s = a.departures.length; --s >= 0;) {
			var t = a.departures[s];
			if (t < 0)continue;
			if (t < d)break;
			++q, t = ~~t;
			var u = ~~(t / 60);
			if (o != u) {
				if (++q > r && t < d)break;
				o = u
			}
			p = t
		}
		s < 0 && q < r ? n += "\">" : n += " icon_expand\" title=\"" + i18n.stopShowAllDepartures + "\">";
		var v = -1;
		q = 0;
		for (s = 0; s < a.departures.length; ++s) {
			var t = a.departures[s];
			if (t < 0)continue;
			e && m.times.push(~~(.5 + t)), t = ~~t;
			var u = ~~(t / 60);
			t >= p && ++q, v != u && (v = u, t >= p && ++q, n += "</span></span><span style=\"display:inline-block;\"><span class=\"DeparturesHour" + (u < o || q > r ? " collapse" : "") + "\">&nbsp;" + u % 24 + "</span><span style=\"vertical-align:top\"" + (t < p || q > r ? " class=\"collapse\"" : "") + ">&#x200A;"), t == p && (n += "</span><span style=\"vertical-align:top\">"), q == r + 1 && (n += "</span><span style=\"vertical-align:top\" class=\"collapse\">"), t = t % 60, n += (t < 10 ? "0" : "") + t + " "
		}
		v === -1 ? (e && m.notes.push(i18n.routeNotOperate), n += "</span><span>" + i18n.routeNotOperate) : !q && a.departures.length ? (a.departures.sort(function (a, b) {
			return a - b
		}), e && m.times.length == 0 && m.notes.push(i18n.stopLatestDeparture + "&nbsp;" + ti.printTime(a.departures[a.departures.length - 1])), n += "</span><span style=\"cursor:default;\" class=\"hideWhenExpanded\">" + i18n.stopLatestDeparture + "&nbsp;" + ti.printTime(a.departures[a.departures.length - 1])) : q > r && (n += "</span><span style=\"cursor:default;\" class=\"hideWhenExpanded\">..."), n += "</span></span></td></tr>", (v === -1 || !q) && a.dirType.indexOf("d") >= 0 && (n = "")
	}
	return e ? m : n
}, pg.fContent_Click = function (a) {
	pg.stopSuggestedForMap && (pg.stopSuggestedForMap = "", pg.fSuggestedStopsHide());
	var b = a && (a.target || a.srcElement);
	if (!b)return !0;
	var c, d, e;
	for (var f = b; f; f = f.parentNode) {
		if ((f.tagName || "").toLowerCase() === "tr")break;
		d || (c = f && (f.className || "").toLowerCase(), c.indexOf("expand") < 0 ? c.indexOf("collapse") < 0 ? (f.href || "").indexOf("#") >= 0 && (e = pg.fUrlParse(f.href), f.className.indexOf("map") < 0 ? e.schedule ? d = pg.fUrlSet({schedule: e.schedule}, !0) : (d = "hash", e.language = pg.language, e.hashForMap || pg.hashForMap && (pg.city === e.city && pg.transport === e.transport ? e.hashForMap = pg.hashForMap : e.hashForMap = "map")) : d = pg.fUrlSet({hashForMap: e.hashForMap}, !0)) : (d = "collapse", b = f) : (d = "expand", b = f));
		if ((f.tagName || "").toLowerCase() === "a")break;
		if ((f.className || "").toLowerCase() === "departuresrow" && d === "expand") {
			d = "", f.className = "DeparturesRowFull";
			break
		}
		if ((f.className || "").toLowerCase() === "departuresrowfull" && d === "collapse") {
			d = "", f.className = "DeparturesRow";
			break
		}
	}
	var g = [];
	while (f) {
		f = f.parentNode, g = f && f.getElementsByTagName("div") || [];
		if (g.length)break
	}
	d == "expand" ? (b.className = b.className.replace("expand", "collapse"), b.title = i18n.hideDetails, (g[0] || {style: {}}).style.display = "", pg.schedule && (pg.scheduleDetailsExpanded = !0), d = "") : d == "collapse" ? (b.className = b.className.replace("collapse", "expand"), b.title = i18n.showDetails, (g[0] || {style: {}}).style.display = "none", pg.schedule && (pg.scheduleDetailsExpanded = !1), d = "") : d == "hash" && (pg.fUrlSet(e), d = "");
	if (d || d === "") {
		d && Hash.go(d);
		return pg.cancelEvent(a)
	}
	return !0
}, pg.inputRoutes_KeyDown = function (a, b) {
	var c = $("inputRoutes");
	b || (b = window.event ? window.event.keyCode : a.keyCode), b == 27 ? (c.value = "", setTimeout(pg.fLoadRoutesList, 200)) : b != 9 && (c.className == "empty" && (c.value = "", c.className = ""), pg.routesFilter = "", setTimeout(pg.fLoadRoutesList, 200))
}, pg.inputRoutes_Focus = function () {
	$e = $("inputRoutes"), $e.className === "empty" && ($e.className = "", $e.value = "")
}, pg.inputRoutes_Blur = function () {
	$e = $("inputRoutes"), $e && !$e.value && ($e.value = i18n.typeRouteNameOrNumber, $e.className = "empty")
}
