<?php

namespace Q\CoreBundle\EventListener;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Q\CoreBundle\Entity\Node;


/**
 * Добавление SecurityContext к нодам
 */
class NodeSCListener
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Node) {
            $entity->setSecurityContext($this->container->get('security.context'));
        }
    }
}
