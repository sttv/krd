angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap'])
    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', '$rootScope', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache, $rootScope) {

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var columnsGetter = $parse(tAttrs.columns);
                var template = '';
                columns = columnsGetter({});

                template += '<div loading-container="loading">';

                template += '<div class="btn-group">';
                    if (!!tAttrs.btnAdd) {
                        template += '<a class="btn btn-success" href="#!'+(tAttrs.url + '/').replace('//', '/')+'create/"><i class="icon-plus icon-white"></i> '+tAttrs.btnAdd+'</a>';
                    }
                template += '</div>';

                template += '<div ng-show="itemList.length > 0"><table ng-table="tableParams" class="table table-hover table-striped">'+
                    '<tr ng-repeat="item in itemList">';

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\'';
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'" sortable="item.'+i+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label';
                    }

                    template += '</td>';
                }

                template += '<td data-title="Действия" width="200">';
                    template += '<a class="btn btn-warning btn-small" tooltip="Редактировать" ng-href="#!'+((tAttrs.url + '/').replace('//', '/') + 'edit/')+'{{item.id}}"><i class="icon-white icon-edit"></i></a>&nbsp;&nbsp;';
                    template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                template += '</td>';

                template += '</tr></table></div>';

                template += '<div class="well well-small" ng-show="itemList.length == 0 && !loading">Список пуст</div>';

                template += '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                var columnsGetter = $parse($attrs.columns);
                $scope.columns = columnsGetter({});

                $scope.tableParams = new ngTableParams({
                    page: 1,
                    total: 0,
                    count: parseInt($attrs.count, 10)
                });

                $scope.itemList = [];

                // Обновление списка
                $scope.update = function() {
                    $scope.loading = true;

                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        $scope.loading = false;
                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                    }, function(response, errorData){
                        console.log(response);
                        $scope.loading = false;
                        $scope.itemList = [];
                        $scope.tableParams.total = 0;
                        var msg = '';
                        try {
                            msg = response.data[0].message;
                        } catch(e) {};
                        $rootScope.$broadcast('ntf-error', response.status + ' - Произошла ошибка при загрузке списка. ' + msg, 5000);
                    });
                }

                // Наблюдаем за параметрами таблицы
                $scope.$watch('tableParams', $scope.update, true);
                $scope.$on('needUpdate', $scope.update, true);

                // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                var hasBoolean = false;
                for(var i in $scope.columns) {
                    if (!$scope.columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if ($scope.columns[i].isBoolean === true) {
                        hasBoolean = true;
                        break;
                    }
                }

                if (hasBoolean) {
                    $scope.$watch('itemList', function(itemList, oldList) {
                        for(var i = 0; i < oldList.length; i++) {
                            if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                                continue;
                            }

                            if (!angular.equals(itemList[i], oldList[i])) {
                                $http.post(($attrs.entityList + '/').replace('//', '/') + 'set/' + itemList[i].id, itemList[i])
                                    .success(function(response) {
                                        $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                    })
                                    .error(function(response) {
                                        console.log(response);
                                        $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                                    });
                            }
                        }
                    }, true);
                }

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function() {
                                alert('Произошла ошибка при удалении');
                            });
                        }
                    });
                }

                /**
                 * Событие успешного добавления/редактировния элемента
                 */
                $scope.$on('entityFormClosed', function() {
                    $scope.$broadcast('needUpdate');
                });
            },

            compile: function($element, $attrs) {
                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    var url = ($attrs.entityList + '/').replace('//', '/');

                    if (url.indexOf(':id') === -1) {
                        url += ':id';
                    }

                    $scope.entityResource = $resource(url, {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'}
                        });
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                $scope.$emit('entityFormClosed');
                            } else {
                                $rootScope.$broadcast('ntf-error', 'Форма содержит ошибки заполнения', 5000);
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            console.log(response);
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка');
                        });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"], input[type="email"], input[type="password"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"], input[type="email"], input[type="password"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - список текстовых полей
     */
    .directive('formCollectionText', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            controller: function($scope, $element, $attrs) {
                var $controls = $element.find('.controls');
                var prototype = $attrs.prototype + removeButtonTpl;

                $scope.nextIndex = $controls.find('input').length + 1;

                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var input = prototype.replace(/__name__/g, $scope.nextIndex);
                    $controls.children().last().before($compile(input)($scope));

                    $scope.nextIndex = $controls.children('input').length;
                    $scope.updateNames();
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.target);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.add($removeButton.prev('input')).remove();
                    $scope.updateNames();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var name = $controls.children('input').first().attr('name').replace(/[0-9\[\]]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', name+'_'+i);

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });
                        $compile($this)($scope);

                        i++;
                    });
                };
            },

            compile: function($element, $attrs) {
                if (!$attrs.prototype) {
                    $element.find('.controls').html('<span class="label label-important">Empty prototype</span>');
                }

                $element.find('.controls input').not(':first').after(removeButtonTpl);
                $element.find('.btn-add').attr('ng-click', 'addElement()');

                return function($scope) {
                    $scope.updateNames();
                }
            }
        }
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Инициализация tinyMCE
     */
    .directive('tinymce', ['$timeout', function($timeout) {
        return {
            restrict: 'C',
            link: function() {
                $timeout(function() {
                    initTinyMCE(window.tinyMCEConfig);
                });
            }
        };
    }])

    /**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Фильтр отображения объекта
     */
    .filter('el_object', [function() {
        return function(value, name) {
            return value[name];
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'">'+value+'</a>';
        }
    }])
;
