angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap'])
    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache) {

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var columnsGetter = $parse(tAttrs.columns);
                var template = '';
                columns = columnsGetter({});

                template += '<div loading-container="loading">';

                template += '<div class="btn-group">';
                    if (!!tAttrs.btnAdd) {
                        template += '<div class="btn btn-success" ng-click="addEntity()"><i class="icon-plus icon-white"></i> '+tAttrs.btnAdd+'</div>';
                    }
                template += '</div>';

                template += '<div ng-show="itemList"><table ng-table="tableParams" class="table table-hover table-striped">'+
                    '<tr ng-repeat="item in itemList">';

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\'';
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'" sortable="item.'+i+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label';
                    }

                    template += '</td>';
                }

                template += '<td data-title="Действия">';
                    template += '<button class="btn btn-warning btn-small" tooltip="Редактировать" ng-click="editEntity(item)"><i class="icon-white icon-edit"></i></button>&nbsp;&nbsp;';
                    template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                template += '</td>';

                template += '</tr></table></div>';
                template += '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    total: 0,
                    count: parseInt($attrs.count, 10)
                });

                $scope.itemList = [];

                // Диалог добавления элемента
                $scope.addDialog = $dialog.dialog({
                    backdrop: true,
                    keyboard: true,
                    backdropClick: true,
                    backdropFade: true,
                    dialogFade: true,
                    templateUrl: ($attrs.entityList + '/').replace('//', '/') + 'form/'
                });

                // Обновление списка
                $scope.update = function() {
                    $scope.loading = true;

                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        $scope.loading = false;
                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                    });
                }

                // Наблюдаем за параметрами таблицы
                $scope.$watch('tableParams', $scope.update, true);
                $scope.$on('needUpdate', $scope.update, true);

                // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                $scope.$watch('itemList', function(itemList, oldList) {
                    for(var i = 0; i < oldList.length; i++) {
                        if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                            continue;
                        }

                        if (!angular.equals(itemList[i], oldList[i])) {
                            $http.post(($attrs.entityList + '/').replace('//', '/') + 'set/' + itemList[i].id, itemList[i], function(response) {
                                $scope.itemList[i] = response;
                            });
                        }
                    }
                }, true);

                /**
                 * Показываем форму добавления элемента
                 */
                $scope.addEntity = function() {
                    $templateCache.remove($scope.addDialog.options.templateUrl);
                    $scope.addDialog.open();
                };

                /**
                 * Показываем форму редактирования элемента
                 */
                $scope.editEntity = function(item) {
                    $scope.editDialog = $dialog.dialog({
                        backdrop: true,
                        keyboard: true,
                        backdropClick: true,
                        backdropFade: true,
                        dialogFade: true,
                        templateUrl: ($attrs.entityList + '/').replace('//', '/') + 'form/'+item.id
                    });

                    $templateCache.remove($scope.editDialog.options.templateUrl);
                    $scope.editDialog.open();
                };

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function() {
                                alert('Произошла ошибка при удалении');
                            });
                        }
                    });
                }

                /**
                 * Событие успешного добавления/редактировния элемента
                 */
                $scope.$on('entityFormClosed', function() {
                    $scope.addDialog.close();
                    if ($scope.editDialog !== undefined) {
                        $scope.editDialog.close();
                    }

                    $scope.$broadcast('needUpdate');
                });
            },

            compile: function($element, $attrs) {
                // Список колонок таблицы
                var columnsGetter = $parse($attrs.columns);
                var columnSetter = columnsGetter.assign;

                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    $scope.columns = columnsGetter($scope);
                    $scope.entityResource = $resource(($attrs.entityList + '/').replace('//', '/') + ':id', {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'}
                        });
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject()).success(function(response) {
                        $scope.loading = false;

                        if (response.success) {
                            $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                            $rootScope.$broadcast('entityFormClosed');
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Форма содержит ошибки заполнения', 5000);
                            if (!!response.errors) {
                                $scope.errors = response.errors;
                            }
                        }
                    });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - список текстовых полей
     */
    .directive('formCollectionText', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            controller: function($scope, $element, $attrs) {
                var $controls = $element.find('.controls');
                var prototype = $attrs.prototype + removeButtonTpl;

                $scope.nextIndex = $controls.find('input').length + 1;

                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var input = prototype.replace(/__name__/g, $scope.nextIndex);
                    $controls.children().last().before($compile(input)($scope));

                    $scope.nextIndex = $controls.children('input').length;
                    $scope.updateNames();
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.srcElement);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.add($removeButton.prev('input')).remove();
                    $scope.updateNames();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var name = $controls.children('input').first().attr('name').replace(/[0-9\[\]]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', name+'_'+i);

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });
                        $compile($this)($scope);

                        i++;
                    });
                };
            },

            compile: function($element, $attrs) {
                if (!$attrs.prototype) {
                    $element.find('.controls').html('<span class="label label-important">Empty prototype</span>');
                }

                $element.find('.controls input').not(':first').after(removeButtonTpl);
                $element.find('.btn-add').attr('ng-click', 'addElement()');

                return function($scope) {
                    $scope.updateNames();
                }
            }
        }
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'">'+value+'</a>';
        }
    }])
;
