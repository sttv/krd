<?php

namespace Q\UserBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Контроллер пользователей
 *
 * @Route("/profile")
 * @PreAuthorize("hasRole('ROLE_CMS_ADMIN')")
 */
class ProfileController extends Controller
{
    /**
     * Страница управления пользователями и группами
     *
     * @Route("/", name="cms_profile_index")
     * @Template("QUserBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {

    }

    /**
     * Статистика
     *
     * @Route("/statistic", name="cms_profile_statistic")
     * @Template("QUserBundle:Admin:statistic.html.twig")
     */
    public function statisticAction()
    {
        return array(
            'userWithComments' => $this->getUsersWithComments(),
            'userWithAppeals' => $this->getUsersWithAppeals()
        );
    }

    /**
     * Список пользователей писавших обращения или черновики
     *
     * @return ArrayCollection
     */
    protected function getUsersWithAppeals()
    {
        $userWithAppealsIdList = $this->getDoctrine()->getManager()->createQuery(
            'SELECT IDENTITY(e.createdBy) FROM KrdAppealBundle:Appeal e'
        )->getArrayResult();
        foreach ($userWithAppealsIdList as &$item) {
            $item = array_pop($item);
        }
        $userWithAppealsIdList = array_unique($userWithAppealsIdList);

        if (empty($userWithAppealsIdList)) {
            $userWithAppealsIdList = array(-1);
        }

        return $this->getDoctrine()->getManager()->createQuery(
            'SELECT e FROM QUserBundle:User e WHERE e.id IN (:ids)'
        )->setParameter(ids, $userWithAppealsIdList)->getResult();
    }

    /**
     * Список пользователей оставлявших комментарии
     *
     * @return ArrayCollection
     */
    protected function getUsersWithComments()
    {
        $userWithCommentsIdList = $this->getDoctrine()->getManager()->createQuery(
            'SELECT IDENTITY(e.createdBy) FROM KrdCommentsBundle:Comment e'
        )->getArrayResult();
        foreach ($userWithCommentsIdList as &$item) {
            $item = array_pop($item);
        }
        $userWithCommentsIdList = array_unique($userWithCommentsIdList);

        if (empty($userWithCommentsIdList)) {
            $userWithCommentsIdList = array(-1);
        }

        return $this->getDoctrine()->getManager()->createQuery(
            'SELECT e FROM QUserBundle:User e WHERE e.id IN (:ids)'
        )->setParameter(ids, $userWithCommentsIdList)->getResult();
    }

    /**
     * Генерация и скачивание XLS
     * @param  array $data Данные для генерации
     * @param  string $title Заголовок документа
     */
    protected function genNdownXLS(array $data, $title)
    {
        ini_set('memory_limit', '1G');
        $columns = range('A', 'Z');

        \PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
        $xls = new \PHPExcel();
        $xls->getProperties()->setTitle($title);
        $xls->setActiveSheetIndex(0);

        $row = 0;
        foreach ($data as $rowData) {
            $row++;

            $cell = -1;
            foreach ($rowData as $cellData) {
                $cell++;

                $xls->getActiveSheet()->SetCellValue($columns[$cell] . $row, $cellData);
            }
        }

        foreach ($columns as $columnID) {
            $xls->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="krd-report.xls"');
        header('Cache-Control: max-age=0');
        \PHPExcel_IOFactory::createWriter($xls, 'Excel5')->save('php://output');
        exit;
    }

    /**
     * Статистика - Приемная
     * @Route("/statistic/download/appeal", name="cms_profile_statistic_appeal")
     */
    public function statisticAppealAction(Request $request)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $result = array();
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('KrdAppealBundle:Appeal')
            ->createQueryBuilder('e')
            ->addOrderBy('e.created', 'DESC');

        if ($from = $request->get('date_from')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $from)) {
                $queryBuilder->andWhere('e.date >= :from')
                    ->setParameter('from', \DateTime::createFromFormat('d.m.Y H:i:s', $from . ' 00:00:00'));
            }
        }

        if ($to = $request->get('date_to')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $to)) {
                $queryBuilder->andWhere('e.date <= :to')
                    ->setParameter('to', \DateTime::createFromFormat('d.m.Y H:i:s', $to . ' 23:59:00'));
            }
        }

        if ($user = $request->get('user')) {
            $queryBuilder->andWhere('e.createdBy = :user')->setParameter('user', $user);
        }

        if ($status = $request->get('status')) {
            $queryBuilder->andWhere('e.status = :status')->setParameter('status', $status);
        }

        $result[] = array(
            "Дата",
            "Статус",
            "Адресат",
            "Создано зарегистрированным пользователем",
            "Фамилия",
            "Имя",
            "Отчество",
            "E-mail",
            "Дата регистрации",
            "Телефон",
            "Индекс",
            "Город",
            "Район",
            "Улица",
            "Дом",
            "Квартира",
            "Сообщение",
        );

        foreach ($queryBuilder->getQuery()->iterate() as $item) {
            /** @var \Krd\AppealBundle\Entity\Appeal $item */
            $item = $item[0];
            /** @var \Q\UserBundle\Entity\User $user */
            $user = $item->getCreatedBy();

            $result[] = array(
                $item->getDate()->format('d.m.Y H:i'),
                $item->getStatus(),
                $item->getDestination() ? $item->getDestination()->getTitle() : '---',
                is_object($user) ? 'да' : 'нет',
                $item->getLastname(),
                $item->getName(),
                $item->getMidname(),
                $item->getEmail(),
                is_object($user) ? $user->getCreated()->format('d.m.Y H:i') : 'нет',
                $item->getPhone(),
                $item->getPostcode(),
                $item->getCity(),
                $item->getDistrict(),
                $item->getStreet(),
                $item->getHouse(),
                $item->getRoom(),
                $item->getMessage(),
            );
        }

        $this->genNdownXLS($result, 'Интернет-приемная');
    }

    /**
     * Статистика - Комментарии
     *
     * @Route("/statistic/download/comments", name="cms_profile_statistic_comments")
     */
    public function statisticCommentsAction(Request $request)
    {
        $result = array();
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('KrdCommentsBundle:Comment')
            ->createQueryBuilder('e')
            ->addOrderBy('e.created', 'DESC');

        if ($from = $request->get('date_from')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $from)) {
                $queryBuilder->andWhere('e.created >= :from')
                    ->setParameter('from', \DateTime::createFromFormat('d.m.Y H:i:s', $from . ' 00:00:00'));
            }
        }

        if ($to = $request->get('date_to')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $to)) {
                $queryBuilder->andWhere('e.created <= :to')
                    ->setParameter('to', \DateTime::createFromFormat('d.m.Y H:i:s', $to . ' 23:59:00'));
            }
        }

        if ($user = $request->get('user')) {
            $queryBuilder->andWhere('e.createdBy = :user')->setParameter('user', $user);
        }

        $result[] = array(
            "Раздел сайта",
            "Ссылка на комментарий",
            "Дата комментария",
            "Текст комментария",
            "Комментарий опубликован",
            "Пользователь. Фамилия",
            "Пользователь. Имя",
            "Пользователь. Отчество",
            "Пользователь. E-mail",
            "Пользователь. Дата регистрации",
        );

        $baseLink = $this->container->getParameter('krd.url');

        foreach ($queryBuilder->getQuery()->iterate() as $item) {
            /** @var \Krd\CommentsBundle\Entity\Comment $item */
            $item = $item[0];
            $user = $item->getCreatedBy();

            $result[] = array(
                $item->getPageTitle(),
                $baseLink . $item->getPage() . '#comment-' . $item->getId(),
                $item->getCreated()->format('d.m.Y H:i'),
                $item->getContent(),
                $item->getActive() ? 'да' : 'нет',
                $user->getLastname(),
                $user->getName(),
                $user->getSurname(),
                $user->getEmail(),
                $user->getCreated()->format('d.m.Y H:i'),
            );
        }

        $this->genNdownXLS($result, 'Комментарии');
    }

    /**
     * Статистика - зарегистрированные пользователи
     *
     * @Route("/statistic/download/registered-list", name="cms_profile_statistic_registered_list")
     */
    public function statisticRegisteredListAction(Request $request)
    {
        $result = array();
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('QUserBundle:User')
            ->createQueryBuilder('e')
            ->addOrderBy('e.created', 'DESC');

        if ($from = $request->get('date_from')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $from)) {
                $queryBuilder->andWhere('e.created >= :from')
                    ->setParameter('from', \DateTime::createFromFormat('d.m.Y H:i:s', $from . ' 00:00:00'));
            }
        }

        if ($to = $request->get('date_to')) {
            if (preg_match('#^\d{1,2}\.\d{1,2}\.\d{4}$#', $to)) {
                $queryBuilder->andWhere('e.created <= :to')
                    ->setParameter('to', \DateTime::createFromFormat('d.m.Y H:i:s', $to . ' 23:59:00'));
            }
        }

        $result[] = array(
            "Фамилия",
            "Имя",
            "Отчество",
            "E-mail",
            "Индекс",
            "Город",
            "Район",
            "Улица",
            "Дом",
            "Квартира",
            "Подписка на новости",
            "Подписка на гор.репортер",
            "Активность",
            "Заблокирован",
            "Дата регистрации",
            "Дата последней авторизации",
            "Группа",
        );

        foreach ($queryBuilder->getQuery()->iterate() as $user) {
            /** @var \Q\UserBundle\Entity\User $user */
            $user = $user[0];
            $subscriber = $em->getRepository('KrdSubscribeBundle:Subscriber')->findOneBy(
                array('email' => $user->getEmail(), 'active' => 1)
            );
            $rsubscriber = $em->getRepository('KrdSubscribeBundle:RSubscriber')->findOneBy(
                array('email' => $user->getEmail(), 'active' => 1)
            );

            $groups = array();
            foreach ($user->getGroups() as $group) {
                $groups[] = $group->getName();
            }

            $result[] = array(
                $user->getLastname(),
                $user->getName(),
                $user->getSurname(),
                $user->getEmail(),
                $user->getAddressIndex(),
                $user->getAddressCity(),
                $user->getAddressRegion(),
                $user->getAddressStreet(),
                $user->getAddressHome(),
                $user->getAddressRoom(),
                is_object($subscriber) ? 'да' : 'нет',
                is_object($rsubscriber) ? 'да' : 'нет',
                $user->isEnabled() ? 'да' : 'нет',
                $user->isLocked() ? 'да' : 'нет',
                $user->getCreated()->format('d.m.Y H:i'),
                $user->getLastLogin() ? $user->getLastLogin()->format('d.m.Y H:i') : '---',
                implode(', ', $groups),
            );
        }

        $this->genNdownXLS($result, 'Зарегистрированные пользователи');
    }
}
