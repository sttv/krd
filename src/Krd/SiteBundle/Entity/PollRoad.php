<?php

namespace Krd\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\Annotation as JMS;
use Q\FilesBundle\Entity\Image;
use Q\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * Вариант опроса по ремонту дорог
 *
 * @ORM\Entity
 * @ORM\Table(name="poll_road",
 *      indexes={
 *          @ORM\Index(name="sort", columns={"sort"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Заголовок"}},
 *      announce={"textarea", {
 *         "label"="Описание",
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class PollRoad
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * @ORM\Column(type="string")
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $announce;

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="poll_road_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="poll_road_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $vote1 = 0;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $vote2 = 0;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $vote3 = 0;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $vote4 = 0;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $vote5 = 0;

    /**
     * Общая сумма голосов
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\SerializedName("votes")
     * @JMS\Accessor(getter="getVotesSumm")
     */
    private $votes = 0;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Сумма голосов
     */
    public function getVotesSumm()
    {
        return $this->vote1 + $this->vote2 * 2 + $this->vote3 * 3 + $this->vote4 * 4 + $this->vote5 * 5;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Poll
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Poll
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Poll
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Poll
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param Node $parent
     * @return Poll
     */
    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     * @return Poll
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     * @return Poll
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set announce
     *
     * @param string $announce
     * @return PollRoad
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * Get announce
     *
     * @return string
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * Add images
     *
     * @param Image $images
     * @return PollRoad
     */
    public function addImage(Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param Image $images
     */
    public function removeImage(Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set vote1
     *
     * @param integer $vote1
     * @return PollRoad
     */
    public function setVote1($vote1)
    {
        $this->vote1 = $vote1;

        return $this;
    }

    /**
     * Get vote1
     *
     * @return integer
     */
    public function getVote1()
    {
        return $this->vote1;
    }

    /**
     * Set vote2
     *
     * @param integer $vote2
     * @return PollRoad
     */
    public function setVote2($vote2)
    {
        $this->vote2 = $vote2;

        return $this;
    }

    /**
     * Get vote2
     *
     * @return integer
     */
    public function getVote2()
    {
        return $this->vote2;
    }

    /**
     * Set vote3
     *
     * @param integer $vote3
     * @return PollRoad
     */
    public function setVote3($vote3)
    {
        $this->vote3 = $vote3;

        return $this;
    }

    /**
     * Get vote3
     *
     * @return integer
     */
    public function getVote3()
    {
        return $this->vote3;
    }

    /**
     * Set vote4
     *
     * @param integer $vote4
     * @return PollRoad
     */
    public function setVote4($vote4)
    {
        $this->vote4 = $vote4;

        return $this;
    }

    /**
     * Get vote4
     *
     * @return integer
     */
    public function getVote4()
    {
        return $this->vote4;
    }

    /**
     * Set vote5
     *
     * @param integer $vote5
     * @return PollRoad
     */
    public function setVote5($vote5)
    {
        $this->vote5 = $vote5;

        return $this;
    }

    /**
     * Get vote5
     *
     * @return integer
     */
    public function getVote5()
    {
        return $this->vote5;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return PollRoad
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }
}