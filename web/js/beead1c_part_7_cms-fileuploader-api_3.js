/**
 * Основные функции загрузчика файлов
 */
angular.module('cms.filesuploader.api', [])
    /**
     * Директива загрузчика
     */
    .directive('filesList', ['uploaderObject', '$rootScope', '$http', function(uploaderObject, $rootScope, $http) {
        return {
            restrict: 'E',
            scope: true,

            templateUrl: function(tElement, tAttrs) {
                return 'cms.filesuploader.' + tAttrs.widget;
            },
            replace: true,

            controller: function($scope, $element) {
                /**
                 * Иконка к кнопке загрузки
                 */
                $scope.uploadButtonIcon = function() {
                    if ($scope.inProgress > 0) {
                        return 'icon-refresh infiinite-rotate icon-white';
                    } else {
                        return 'icon-upload icon-white';
                    }
                };


                /**
                 * Эмуляция клика по файловому инпуту
                 */
                $scope.emitClick = function() {
                    $scope.$fileInput.click();
                };


                /**
                 * Загрузка файла
                 */
                $scope.upload = function(file) {
                    $scope.inProgress++;

                    uploaderObject.upload({
                        file: file,
                        url: $scope.url,
                        fieldName: 'file',
                        oncomplete: function(uploadSuccess, response) {
                            $scope.$apply(function() {
                                $scope.inProgress--;

                                if (uploadSuccess && !!response.entity) {
                                    $scope.files.push(response.entity);
                                } else {
                                    var errMsg = 'Произошла ошибка при загрузке файла: ' + file.name;

                                    if (!!response.errors && !!response.errors.file) {
                                        errMsg += ': ' + response.errors.file.join(', ');
                                    }

                                    $rootScope.$broadcast('ntf-error', errMsg, 10000);
                                }
                            });
                        }
                    });
                };
            },

            link: function($scope, $element, attrs) {
                $scope.url = attrs.url;
                $scope.name = attrs.name;
                $scope.inProgress = 0;
                $scope.files = [];
                $scope.$fileInput = $element.find('.hidden-input');

                /**
                 * Ловля изменения файловго инпута
                 */
                $scope.$fileInput.on('change', function() {
                    if (this.files === undefined) return false;

                    for (var i = 0, file; file = this.files[i]; i++){
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    $scope.$fileInput.val('');

                    return false;
                });

                // Обработка драг-н-дроп
                var _dropCallback = function(e){
                    e.preventDefault();

                    var dt = e.originalEvent.dataTransfer;

                    $element.removeClass('dd-active');

                    if (dt.files.length == 0) return false;

                    for (var i = 0, file; file = dt.files[i]; i++) {
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    return false;
                };

                $element.on({
                    dragenter: function(){
                        $element.addClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                $element.children('.dd-helper').on({
                    dragleave: function(){
                        $element.removeClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                /**
                 * Событие удаления файла
                 */
                $scope.$on('remove', function(event, file) {
                    event.stopPropagation();

                    for(var i = 0; i < $scope.files.length; i++) {
                        if ($scope.files[i].id == file.id) {
                            // $http({
                            //     method: 'DELETE',
                            //     url: $scope.url + file.id,
                            // });

                            $scope.files.splice(i, 1);

                            break;
                        }
                    }
                });

                if (!!attrs.files) {
                    $scope.files = $.parseJSON(attrs.files);
                }
            }
        }
    }])

    .factory('uploaderObject', [function() {
        /*
         * Загрузчик файла на сервер.
         * file       - объект File (обязателен)
         * url        - строка, указывает куда загружать (обязателен)
         * fieldName  - имя поля, содержащего файл (как если задать атрибут name тегу input)
         * onprogress - функция обратного вызова, вызывается при обновлении данных
         * oncomplete - функция обратного вызова, вызывается при завершении загрузки, принимает два параметра:
         *      uploaded - содержит true, в случае успеха и false, если возникли какие-либо ошибки;
         *      data - в случае успеха в него передается ответ сервера
         *
         *      если в процессе загрузки возникли ошибки, то в свойство lastError объекта помещается объект ошибки, содержащий два поля: code и text
         */
        var uploaderObject = function(params) {

            if(!params.file || !params.url) {
                return false;
            }

            this.xhr = new XMLHttpRequest();
            this.reader = new FileReader();

            this.progress = 0;
            this.uploaded = false;
            this.successful = false;
            this.lastError = false;

            var self = this;
            var uploadCanceled = false;

            self.cancelUpload = function() {
                uploadCanceled = true;
                this.xhr.abort();
            }

            self.reader.onload = function(){
                self.xhr.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable){
                        if(params.onprogress instanceof Function){
                            params.onprogress.call(self, e.loaded, e.total);
                        }
                    }
                }, false);

                self.xhr.upload.addEventListener("load", function(){
                    self.progress = 100;
                    self.uploaded = true;
                }, false);

                self.xhr.upload.addEventListener("error", function(e){
                    self.lastError = {
                        code: 1,
                        text: 'Error uploading on server'
                    };
                }, false);

                self.xhr.onreadystatechange = function(){
                    var callbackDefined = params.oncomplete instanceof Function;

                    if (this.readyState == 4){
                        if(this.status == 200){
                            if(!self.uploaded){
                                if(callbackDefined){
                                    params.oncomplete.call(self, false);
                                }
                            }else{
                                self.successful = true;

                                if(callbackDefined){
                                    params.oncomplete.call(self, true, $.parseJSON(this.responseText), uploadCanceled);
                                }
                            }
                        }else{
                            self.lastError = {
                                code: this.status,
                                text: 'HTTP response code is not OK ('+this.status+')'
                            };

                            if(callbackDefined){
                                params.oncomplete.call(self, false, null, uploadCanceled);
                            }
                        }
                    }
                };

                var boundary = "xxxxxxxxx";

                var body = "--" + boundary + "\r\n";
                body += "Content-Disposition: form-data; name='"+(params.fieldName || 'file')+"'; filename='" + unescape(encodeURIComponent(params.file.name)) + "'\r\n";
                body += "Accept: application/json\r\n"
                body += "Content-Type: application/octet-stream\r\n\r\n";
                body += self.reader.result + "\r\n";
                body += "--" + boundary + "--";

                self.xhr.open("POST", params.url);
                self.xhr.setRequestHeader("Cache-Control", "no-cache");
                self.xhr.setRequestHeader("Accept", "application/json");

                if(self.xhr.sendAsBinary){
                    self.xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary="+boundary);
                    self.xhr.sendAsBinary(body);
                }else{
                    var formData = new FormData();
                    formData.append(params.fieldName || 'file', params.file);
                    self.xhr.send(formData);
                }
            };

            self.reader.readAsBinaryString(params.file);
        };

        return {
            upload: function(params) {
                return new uploaderObject(params);
            }
        }
    }])
;
