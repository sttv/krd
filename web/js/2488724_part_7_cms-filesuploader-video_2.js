/**
 * Модуль загрузчика видео
 */
angular.module('cms.filesuploader.video', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="video-uploader abstract-uploader">'
            + '<div class="files-list clearfix">'
                + '<video-list-item file="file" ng-repeat="file in files"></video-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Видео не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-small btn-info" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить видео</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '&nbsp;&nbsp;'
            + '<div class="btn btn-small btn-info" ng-controller="videoFtpSelectCtrl" ng-click="ftpShowWindow()">'
                + '<i class="icon-white icon-hdd"></i>&nbsp;'
                + 'Выбрать по FTP'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.video', template);

        var ftpTemplate = '<div style="width:1000px; height:450px;"><iframe src="'+window.elFinderBrowserUrl+'" width="1000" height="450" border="0" style="border:0 none; padding:0; margin:0;"></iframe></div>';
        $templateCache.put('cms.filesuploader.video.ftp', ftpTemplate);
    }])

    /**
     * Контроллер для выбора файла по FTP
     */
    .controller('videoFtpSelectCtrl', ['$scope', '$dialog', '$http', '$rootScope', function($scope, $dialog, $http, $rootScope) {
        /**
         * Диалог выбора файла по FTP
         */
        $scope.ftpShowWindow = function() {
            var d = $dialog.dialog({
                modalFade:true
            });

            var temp = window.tinymce;

            window.tinymce = {
                activeEditor: {
                    windowManager: {
                        getParams: function() {
                            return {
                                setUrl: function(url) {
                                    var self = window.location.protocol + '//' + window.location.host;

                                    if (url.toString().indexOf(self) === 0) {
                                        url = url.toString().substr(self.length);
                                    }

                                    $scope.$apply(function() {
                                        $scope.uploadByFtp(url);
                                    });
                                }
                            }
                        },

                        close: function(){
                            d.close();
                            window.tinymce = temp;
                        }
                    }
                }
            };

            d.open('cms.filesuploader.video.ftp');
        };

        /**
         * Загрузка видео по FTP
         */
        $scope.uploadByFtp = function(url) {
            $scope.$parent.inProgress++;

            $http({
                method:'POST',
                data: {url:url},
                url: $scope.$parent.url + 'byftp/'
            }).success(function(response) {
                $scope.$parent.inProgress--;

                if (response.success&& !!response.entity) {
                    $scope.$parent.files.push(response.entity);
                } else {
                    $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке видео' + response.msg, 10000);
                }
            }).error(function(response) {
                $scope.$parent.inProgress--;
                console.log(response);
                $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке видео' + response.msg, 10000);
            });
        };
    }])

    /**
     * Элемент загрузчика видео
     */
    .directive('videoListItem', ['$dialog', '$http', 'uploaderObject', '$parse', '$q', function($dialog, $http, uploaderObject, $parse, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix" url-image="url">'
                    + '<a ng-href="{{file.image.file.path}}" target="_blank" class="img-polaroid">'
                        + '<img ng-src="{{file.image.size.cms}}">'
                    + '</a>'
                    + '<input type="file" name="image" title="" class="hidden-input" />'
                    + '<input type="text" placeholder="Заголовок" ng-model="file.title">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="Скачать: {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-info btn-mini" tooltip="Изменить обложку" ng-click="changeImage()"><i class="icon-white" ng-class="uploadImageButtonIcon()"></i></div>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()" tooltip="Удалить"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление видео';
                    var msg = 'Вы уверены, что хотите удалить видео?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Клик по кнопке смены обложки
                 */
                $scope.changeImage = function() {
                    if (!$scope.inProgress) {
                        $scope.$imageInput.click();
                    }
                };

                /**
                 * Загрузка новой обложки
                 */
                $scope.uploadImage = function(file) {
                    $scope.inProgress = true;

                    uploaderObject.upload({
                        file: file,
                        url: $scope.urlImage + $scope.file.id,
                        fieldName: 'file',
                        oncomplete: function(uploadSuccess, response) {
                            $scope.$apply(function() {
                                $scope.inProgress = false;

                                if (uploadSuccess && !!response.image) {
                                    $scope.file.image = response.image;
                                } else {
                                    var errMsg = 'Произошла ошибка при загрузке файла: ' + file.name;

                                    if (!!response.errors && !!response.errors.file) {
                                        errMsg += ': ' + response.errors.file.join(', ');
                                    }

                                    $rootScope.$broadcast('ntf-error', errMsg, 10000);
                                }
                            });
                        }
                    });
                };

                /**
                 * Иконка кнопки смены обложки
                 */
                $scope.uploadImageButtonIcon = function() {
                    if ($scope.inProgress) {
                        return 'icon-refresh infiinite-rotate';
                    } else {
                        return 'icon-camera';
                    }
                };
            },

            link: function($scope, $element, attrs) {
                $scope.urlImage = $parse(attrs.urlImage)($scope.$parent) + 'change-image/';

                $scope.$imageInput = $element.find('input[type="file"]');
                $scope.inProgress = false;

                /**
                 * Ловля изменения файловго инпута для обложки
                 */
                $scope.$imageInput.on('change', function() {
                    if (this.files === undefined) return false;

                    for (var i = 0, file; file = this.files[i]; i++){
                        $scope.$apply(function() {
                            $scope.uploadImage(file);
                        });
                    }

                    $scope.$imageInput.val('');

                    return false;
                });

                // Редактирование имени файла
                var fst = true;
                $scope.$watch('file.title', function(title) {
                    if (fst) {
                        fst = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {title:title},
                        timeout: this.canceler.promise
                    });
                });
            }
        }
    }])
;
