<?php

namespace Krd\ApiBundle\Controller;

use DateTimeZone;
use Doctrine\ORM\EntityManager;
use Krd\EvacuationBundle\Entity\Avto;
use Krd\EvacuationBundle\Entity\Evacuator;
use Krd\EvacuationBundle\Entity\Mark;
use Krd\EvacuationBundle\Entity\Organization;
use Krd\EvacuationBundle\Entity\Parking;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Эвакуированные авто
 *
 * @Route("/evacuation")
 */
class EvacuationController extends Controller
{
    /**
     * Список эвакуированных авто
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * Фильтрация:
     *  date - фильтрация по дате в формате d.m.Y (22.05.2014)
     *  mark - ID марки автомобиля
     *  number - номер автомобиля в формате А 123 АА 123
     *
     * @Route("/list.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->get('krd.evacuation.modules.avto')->getRepository()
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->orderBy('e.date', 'DESC');


        if ($mark = $request->get('mark')) {
            $queryBuilder->andWhere('e.mark = :mark');
            $queryBuilder->setParameter('mark', $mark);
        }

        if ($markTitle = $request->get('mark_title')) {
            $queryBuilder->innerJoin('e.mark', 'm', 'WITH', 'm.title = :mark_title');
            $queryBuilder->setParameter('mark_title', $markTitle);
        }

        if ($number = $request->get('number')) {
            $number = mb_strtolower($number);
            $number = trim($number);

            $queryBuilder->andWhere(
                'LOWER(e.number) = :number OR LOWER(e.number) LIKE :number_like OR LOWER(e.number) LIKE :number_like_r'
            );

            $queryBuilder->setParameter('number', $number);
            $queryBuilder->setParameter('number_like', '%' . $number . '%');

            $numberR = str_replace(' ', '', $number);
            $numberR = mb_substr($numberR, 0, 1) . ' ' . mb_substr($numberR, 1, 3) . ' ' . mb_substr(
                    $numberR,
                    4,
                    2
                ) . ' ' . mb_substr($numberR, 6, 3);
            $numberR = trim($numberR);
            $queryBuilder->setParameter('number_like_r', '%' . $numberR . '%');
        }

        if ($date = $request->get('date')) {
            if (preg_match('#^\d{2}\.\d{1}\.\d{4}$#', $date)) {
                $dates = explode('.', $date);
                $date = $dates[0] . '.0' . $dates[1] . '.' . $dates[2];
            }

            $queryBuilder->andWhere('DATE_FORMAT(e.date, \'%d.%m.%Y\') = :date');
            $queryBuilder->setParameter('date', $date);
        }

        return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true);
    }

    /**
     * Отслеживание эвакуированных авто
     *
     * Возможные параметры:
     *  lastid - последний ID, который был получен до этого
     *
     * @Route("/track.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function trackAction(Request $request)
    {
        $queryBuilder = $this->get('krd.evacuation.modules.avto')->getRepository()
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->orderBy('e.sort');


        if ($lastId = $request->get('lastid')) {
            $queryBuilder->andWhere('e.id > :lastid');
            $queryBuilder->setParameter('lastid', $lastId);
        } else {
            $queryBuilder->setMaxResults(100);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Список марок авто
     *
     * @Route("/marks.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function marksListAction()
    {
        return $this->getDoctrine()->getManager()
            ->createQuery('SELECT e FROM KrdEvacuationBundle:Mark e ORDER BY e.sort')
            ->getResult();
    }

    /**
     * Получение или создание марки авто
     *
     * @param $mark
     */
    protected function ex___getMark($mark)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $markItem = $em->getRepository('KrdEvacuationBundle:Mark')->findOneBy(array('title' => $mark));

        if ($markItem) {
            return $markItem;
        }

        $markItem = new Mark();
        $markItem->setTitle($mark);

        $em->persist($markItem);
        $em->flush($markItem);

        return $markItem;
    }

    /**
     * Получение или создание организации эвакуатора
     *
     * @param $organization
     */
    protected function ex___getOrganization($organization)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $orgItem = $em->getRepository('KrdEvacuationBundle:Organization')->findOneBy(array('title' => $organization));

        if ($orgItem) {
            return $orgItem;
        }

        $orgItem = new Organization();
        $orgItem->setTitle($organization);

        $em->persist($orgItem);
        $em->flush($orgItem);

        return $orgItem;
    }

    /**
     * Получение или создание эвакуатора
     *
     * @param $evak
     */
    protected function ex___getEvacuator($evak)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $evakItem = $em->getRepository('KrdEvacuationBundle:Evacuator')->findOneBy(array('number' => $evak));

        if ($evakItem) {
            return $evakItem;
        }

        $evakItem = new Evacuator();
        $evakItem->setNumber($evak);

        $em->persist($evakItem);
        $em->flush($evakItem);

        return $evakItem;
    }

    /**
     * Получение или создание эвакуатора
     *
     * @param $parking
     */
    protected function ex___getParking($parking)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $parkingItem = $em->getRepository('KrdEvacuationBundle:Parking')->findOneBy(array('title' => $parking));

        if ($parkingItem) {
            return $parkingItem;
        }

        $parkingItem = new Parking();
        $parkingItem->setTitle($parking);

        $em->persist($parkingItem);
        $em->flush($parkingItem);

        return $parkingItem;
    }

    /**
     * Показываем тестовые авто
     *
     * @Route("/external/show")
     */
    public function externalShowAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $list = $em->createQuery("SELECT e FROM KrdEvacuationBundle:Avto e ORDER BY e.id DESC");

        echo '<table border="1">';
        echo '<td align="center" nowrap>Дата и время погрузки на эвакуатор</td>
                    <td align="center">Место погрузки <br>на эвакуатор</td>
                    <td align="center">Марка, <br>модель транспортного средства</td>
                    <td align="center">Государственный регистрационный <br>номер транспортного средства</td>
                    <td align="center">Сведения об организации, <br>осуществившей транспортировку</td>
                    <td align="center">Государственный регистрационный номер эвакуатора</td>
                    <td align="center">Место стоянки,куда помещено <br>транспортное средство</td>
                    <td align="center">Внешний ID</td>';
        foreach ($list->iterate() as $item) {
            /** @var Avto $item */
            $item = $item[0];

            echo '<tr>';
            echo '<td>' . $item->getDate()->format('d.m.Y H:i') . '</td>';
            echo '<td>' . $item->getFromPlace() . '</td>';
            echo '<td>' . $item->getMark()->getTitle() . '</td>';
            echo '<td>' . $item->getNumber() . '</td>';
            echo '<td>' . $item->getOrganization()->getTitle() . '</td>';
            echo '<td>' . ($item->hasEvacuator() ? $item->getEvacuator()->getNumber() : '---') . '</td>';
            echo '<td>' . $item->getParking()->getTitle() . '</td>';
            echo '<td>' . $item->getExternalId() . '</td>';
            echo '</tr>';
        }
        echo '</table>';
        die();
    }

    /**
     * Проверка наличия полей. Возвращает список отсутствующих полей
     * @param $data
     * @return array
     */
    protected function checkExDataFields($data)
    {
        $fields = array('dateTime', 'place', 'gosNumberTs', 'model', 'organization', 'gosnumberEvacuator', 'parkingAdress');
        $result = array();

        foreach ($fields as $field) {
            if (!isset($data[$field]) || empty($data[$field])) {
                $result[] = $field;
            }
        }

        return $result;
    }

    /**
     * Внешние запросы на добавление/изменение эвакуированных авто
     *
     * @Route("/external")
     * @Method({"POST"})
     */
    public function externalAction(Request $request)
    {
//        $inputData = $_POST['request'];

//        if (empty($inputData)) {
            $inputData = $request->get('request');
            $inputData = @json_decode($inputData, true);
            $response = array();

            if (!$inputData) {
                return new JsonResponse(array('status' => false, 'errorText' => 'Json parse error'));
            }

            if (!is_array($inputData)) {
                return new JsonResponse(array('status' => false, 'errorText' => 'Input data must be an array'));
            }
//        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $parentNode = $em->getRepository('QCoreBundle:Node')->find(45);

        foreach ($inputData as $inputItem) {
            if (empty($inputItem['id']) || empty($inputItem['method'])) {
                continue;
            }

            $this->get('logger')->debug($inputItem['method'] . ' item ' . $inputItem['id']);

            switch ($inputItem['method']) {
                // Добавление эвакуированного авто
                case 'create':
                    if (empty($inputItem['data'])) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Data field not found',
                        );
                        break;
                    }

                    $checkFields = $this->checkExDataFields($inputItem['data']);
                    if (!empty($checkFields)) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Data must contains fields: ' . implode(', ', $checkFields),
                        );
                        break;
                    }

                    /** @var Avto $itemExists */
                    $itemExists = $em->getRepository('KrdEvacuationBundle:Avto')->findOneBy(
                        array('externalId' => $inputItem['id'])
                    );

                    if ($itemExists) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Item with the same id already exist',
                        );
                        break;
                    }

                    $avto = new Avto();
                    $avto->setParent($parentNode);
                    $avto->setDate(\DateTime::createFromFormat('U', $inputItem['data']['dateTime'], new DateTimeZone(date_default_timezone_get()))->modify('+3 hours'));
                    $avto->setFromplace($inputItem['data']['place']);
                    $avto->setNumber($inputItem['data']['gosNumberTs']);
                    $avto->setMark($this->ex___getMark($inputItem['data']['model']));
                    $avto->setOrganization($this->ex___getOrganization($inputItem['data']['organization']));
                    $avto->setEvacuator($this->ex___getEvacuator($inputItem['data']['gosnumberEvacuator']));
                    $avto->setParking($this->ex___getParking($inputItem['data']['parkingAdress']));
                    $avto->setExternalId($inputItem['id']);
                    $avto->setActive(true);
                    $avto->setToindex(true);

                    $em->persist($avto);


                    if ($status = $this->flushDeadLock()) {
//                        $em->getRepository('KrdEvacuationBundle:Avto')->setFirstSortEntity($avto);
                    }

                    $response[] = array(
                        'id' => $inputItem['id'],
                        'method' => $inputItem['method'],
                        'status' => $status,
                    );

                    break;

                // Редактирование авто
                case 'edit':
                    if (empty($inputItem['data'])) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Data field not found',
                        );
                        break;
                    }

                    /** @var Avto $itemToEdit */
                    $itemToEdit = $em->getRepository('KrdEvacuationBundle:Avto')->findOneBy(
                        array('externalId' => $inputItem['id'])
                    );

                    if (!$itemToEdit) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Item not found',
                        );
                        break;
                    }

                    $checkFields = $this->checkExDataFields($inputItem['data']);
                    if (!empty($checkFields)) {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Data must contains fields: ' . implode(', ', $checkFields),
                        );
                        break;
                    }

                    $itemToEdit->setParent($parentNode);
                    $itemToEdit->setDate(\DateTime::createFromFormat('U', $inputItem['data']['dateTime'], new DateTimeZone(date_default_timezone_get()))->modify('+3 hours'));
                    $itemToEdit->setFromplace($inputItem['data']['place']);
                    $itemToEdit->setNumber($inputItem['data']['gosNumberTs']);
                    $itemToEdit->setMark($this->ex___getMark($inputItem['data']['model']));
                    $itemToEdit->setOrganization($this->ex___getOrganization($inputItem['data']['organization']));
                    $itemToEdit->setEvacuator($this->ex___getEvacuator($inputItem['data']['gosnumberEvacuator']));
                    $itemToEdit->setParking($this->ex___getParking($inputItem['data']['parkingAdress']));
                    $itemToEdit->setActive(true);

                    $status = $this->flushDeadLock();

                    $response[] = array(
                        'id' => $inputItem['id'],
                        'method' => $inputItem['method'],
                        'status' => $status,
                    );
                    break;

                // Удаление авто
                case 'delete':
                    $itemToDelete = $em->getRepository('KrdEvacuationBundle:Avto')->findOneBy(
                        array('externalId' => $inputItem['id'])
                    );

                    if ($itemToDelete) {
                        $em->remove($itemToDelete);
                        $status = $this->flushDeadLock();

                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => $status,
                        );
                    } else {
                        $response[] = array(
                            'id' => $inputItem['id'],
                            'method' => $inputItem['method'],
                            'status' => false,
                            'errorText' => 'Item not found',
                        );
                    }
                    break;

                default:
                    $response[] = array(
                        'id' => $inputItem['id'],
                        'method' => $inputItem['method'],
                        'status' => false,
                        'errorText' => 'Unknown method'
                    );
                    break;
            }
        }

        $this->get('logger')->debug('Finish request with response ' . serialize($response));

        return new JsonResponse($response);
    }

    /**
     * Пытаемся сделать flush с учетом дедлоков
     * @param int $times
     * @return bool
     */
    protected function flushDeadLock($times = 3)
    {
        $retry = 0;
        $done = false;

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $em->getConnection()->beginTransaction();

        $this->get('logger')->debug('Start flushDeadLock');

        while (!$done and $retry < $times) {
            try {
                $em->flush();
                $em->getConnection()->commit();
                $done = true;
            } catch (\Exception $e) {
                $em->getConnection()->rollback();
                $retry++;
            }
        }

        $this->get('logger')->debug('Finish flushDeadLock with ' . ($done ? 'true' : 'false') . ' and ' . $retry . ' retries');

        return $done;
    }
}
