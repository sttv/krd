<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\KrdUserEvents;
use Krd\UserBundle\Form\Type\UserProfileType;


/**
 * Закладки в личном кабинете
 */
class BookmarkController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/", name="krd_user_bookmarks")
     * @Template("KrdUserBundle:Index:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Директории текущего пользователя
     * @Route("/directories.{_format}", name="krd_user_bookmarks_list", defaults={"_format"="json"}, requirements={"_format"="^json$"})
     */
    public function listAction()
    {
        $result = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory')->getMyDirectories();

        if (empty($result)) {
            $result[] = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory')->createMyDirectory(array('title' => 'Мои закладки'));
        }

        return $result;
    }

    /**
     * Создание директории
     *
     * @Route("/directory/", name="krd_user_bookmarks_directory_create", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function createDirectoryAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory');

        if ($directory = $repo->createMyDirectory($request->request->all())) {
            return $directory;
        } else {
            throw new \Exception('Error while creating directory');
        }
    }

    /**
     * Создание закладки
     *
     * @Route("/bookmark/", name="krd_user_bookmarks_bookmark_create", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function createBookmarkAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Bookmark');
        $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory');

        if ($bookmark = $repo->createMyBookmark($request->request->all())) {
            return $bookmark;
        } else {
            throw new \Exception('Error while creating bookmark');
        }
    }

    /**
     * Редактирование директорий
     *
     * @Route("/directory/{id}", name="krd_user_bookmarks_directory_edit", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function editDirectoryAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory');

        if ($directory = $repo->editMyDirectory($id, $request->request->all())) {
            return $directory;
        } else {
            throw new \Exception('Directory not found');
        }
    }

    /**
     * Редактирование закладок
     *
     * @Route("/bookmark/{id}", name="krd_user_bookmarks_bookmark_edit", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function editBookmarkAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Bookmark');

        if ($bookmark = $repo->editMyDocument($id, $request->request->all())) {
            return $bookmark;
        } else {
            throw new \Exception('Bookmark not found');
        }
    }

    /**
     * Удаление закладок или папок
     *
     * @Route("/bookmark/remove/", name="krd_user_bookmarks_bookmark_remove", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"DELETE"})
     */
    public function removeBookmarkAction(Request $request)
    {
        if ($directories = $request->request->get('directories')) {
            $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:BookmarkDirectory');

            foreach ($directories as $id) {
                $repo->removeMyDirectory($id);
            }
        }

        if ($bookmarks = $request->request->get('bookmarks')) {
            $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Bookmark');

            foreach ($bookmarks as $id) {
                $repo->removeMyBookmark($id);
            }
        }
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
