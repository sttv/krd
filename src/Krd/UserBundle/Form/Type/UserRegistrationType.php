<?php

namespace Krd\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Форма регистрации пользователя
 */
class UserRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastname', 'text', array(
            'label' => 'Фамилия',
            'required' => false,
            'attr' => array('placeholder' => 'Иванов'),
        ));

        $builder->add('name', 'text', array(
            'label' => 'Имя',
            'required' => false,
            'attr' => array('placeholder' => 'Иван'),
        ));

        $builder->add('surname', 'text', array(
            'label' => 'Отчество',
            'required' => false,
            'attr' => array('placeholder' => 'Иванович'),
        ));

        $builder->add('phone', 'text', array(
            'label' => 'Телефон',
            'required' => false,
            'attr' => array('placeholder' => '+7-900-111-22-33'),
        ));

        $builder->add('email', 'text', array(
            'label' => 'E-mail',
            'required' => false,
            'attr' => array('placeholder' => 'myaddress@mail.ru'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы не указали email', 'groups' => array('registration'))),
                new Assert\Email(array('message'=> 'Вы неверно указали email адрес', 'groups' => array('registration'))),
            ),
        ));

        $builder->add('address_index', 'text', array(
            'label' => 'Индекс',
            'required' => false,
            'attr' => array('placeholder' => '350002'),
        ));

        $builder->add('address_city', 'text', array(
            'label' => 'Город',
            'required' => false,
            'attr' => array('placeholder' => 'Краснодар'),
        ));

        $builder->add('address_region', 'text', array(
            'label' => 'Округ',
            'required' => false,
            'attr' => array('placeholder' => 'Западный'),
        ));

        $builder->add('address_street', 'text', array(
            'label' => 'Улица',
            'required' => false,
            'attr' => array('placeholder' => 'Красная'),
        ));

        $builder->add('address_home', 'text', array(
            'label' => 'Дом/корпус',
            'required' => false,
            'attr' => array('placeholder' => '12/4'),
        ));

        $builder->add('address_room', 'text', array(
            'label' => 'Квартира',
            'required' => false,
            'attr' => array('placeholder' => '12'),
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Пароли не совпадают',
            'first_options' => array('label' => 'Пароль'),
            'second_options' => array('label' => 'Повтор пароля'),
            'required' => false,
        ));

        $builder->add('cquestion', 'text', array(
            'label' => 'Контрольный вопрос',
            'required' => false,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы не указали контрольный вопрос', 'groups' => array('registration'))),
            ),
        ));

        $builder->add('creply', 'text', array(
            'label' => 'Ответ',
            'required' => false,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы не указали ответ', 'groups' => array('registration'))),
            ),
        ));

        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Q\UserBundle\Entity\User',
            'validation_groups' => array('registration'),
        ));
    }

    public function getName()
    {
        return 'user_registration';
    }
}
