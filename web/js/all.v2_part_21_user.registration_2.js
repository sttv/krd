/**
 * Регистрация на сайте
 */
angular.module('user.registration', [])
    /**
     * Контроллер формы регистрации
     */
    .controller('UserRegistrationFormCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.onSuccess = function(response) {
            $('#registration-submit-form-success').appendTo($element.parent()).slideUp(0).slideDown();
            $element.add($element.prev('.text-block')).slideUp(250);

            setTimeout(function() {
                if (!!response.successText) {
                    $('#registration-submit-form-success').html(response.successText);

                    if (!!$.scrollTo) {
                        $.scrollTo($('#registration-submit-form-success'), 200, {offset:{top:-150}});
                    }
                }
            }, 300);
        };
    }])
;
