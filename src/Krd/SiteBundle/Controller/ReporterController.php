<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Городской репортер
 */
class ReporterController extends Controller implements ActiveSecuredController
{
    /**
     * Получение публикации с городского репортера
     * @Route("/ajax/reporter/last.json", name="krd_ajax_reporter_last", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function getLastReporterPublication(Request $request)
    {
        $publications = $this->get('krd.site.modules.reporter')->getLastPublications($request->get('offset', 0));

        foreach($publications as &$item) {
            $item['date'] = date('d/m/Y (H:i)', strtotime($item['date']));
        }

        return array(
            'publications' => $publications,
        );
    }
}
