<?php

namespace Krd\CommentsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Форма создания нового комментария
 */
class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('parent', 'hidden_comment');

        $builder->add('pageHash', 'hidden');

        $page = $builder->create('page', 'hidden')->addModelTransformer(new FilterHtmlTransformer());
        $builder->add($page);

        $pageTitle = $builder->create('pageTitle', 'hidden')->addModelTransformer(new FilterHtmlTransformer());
        $builder->add($pageTitle);

        $content = $builder->create('content', 'textarea', array(
            'label' => false,
            'required' => true,
            'attr' => array('placeholder' => 'Ваш комментарий'),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($content);

        $builder->add('submit', 'submit', array(
            'label' => 'Отправить',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\CommentsBundle\Entity\Comment',
            'csrf_protection' => true,
        ));
    }

    public function getName()
    {
        return 'comments_comment_create';
    }
}
