<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;

/**
 * Разделы
 *
 * @Route("/node")
 */
class NodeController extends Controller
{
    /**
     * Подготовка списка разделов для отдачи
     * @param  array $list
     * @return array
     */
    protected function prepareList($list)
    {
        $result = array();

        foreach ($list as $node) {
            $result[] = array(
                'id' => $node->getId(),
                'title' => $node->getTitle(),
                'controller_id' => $node->getController()->getId(),
            );
        }

        return $result;
    }

    /**
     * Список возможных контроллеров
     *
     * @Route("/controllers.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function controllersAction()
    {
        $list = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:NodeController')
            ->createQueryBuilder('e')
            ->addOrderBy('e.id')
            ->getQuery()
                ->getResult();

        $result = array();

        foreach ($list as $item) {
            $result[] = array(
                'id' => $item->getId(),
                'title' => $item->getName(),
                'controller' => $item->getController(),
            );
        }

        return $result;
    }

    /**
     * Список всех подразделов указанного раздела
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAllAction($parent)
    {
        $list = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1 AND e.parent = :parent')
            ->addOrderBy('e.left')
            ->setParameter('parent', $parent)
            ->getQuery()
                ->getResult();

        return $this->prepareList($list);
    }

    /**
     * Список подразделов из указанного меню указанного раздела
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * @Route("/list/{menu}-{parent}.{_format}", requirements={"menu"="^\w+$", "parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listMenuAction($menu, $parent)
    {
        switch($menu) {
            case 'contentblue':
                $menu = 2;
                break;

            case 'linksbottom':
                $menu = 6;
                break;

            case 'linkstop':
                $menu = 7;
                break;

            case 'menubottom':
                $menu = 8;
                break;

            case 'menutop':
                $menu = 9;
                break;

            case 'contentblueex':
                $menu = 10;
                break;

            default:
                throw $this->createNotFoundException();
        }

        $list = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1 AND e.parent = :parent')
            ->innerJoin('e.menutype', 'm', 'WITH', 'm.id IN (:menutype)')
            ->addOrderBy('e.left')
            ->setParameter('parent', $parent)
            ->setParameter('menutype', array($menu))
            ->getQuery()
                ->getResult();

        return $this->prepareList($list);
    }
}
