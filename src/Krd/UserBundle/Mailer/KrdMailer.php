<?php

namespace Krd\UserBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Q\CoreBundle\Swift\Mailer;


class KrdMailer implements MailerInterface
{
    protected $mailer;
    protected $router;
    protected $parameters;

    public function __construct(Mailer $mailer, UrlGeneratorInterface $router, $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->parameters = $parameters;
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template_confirmation'];
        $url = $this->router->generate('krd_user_registration_comfirmation', array('token' => $user->getConfirmationToken()), true);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        $this->sendMessage($template, $context, $user->getEmail());
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template_resetting'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        $this->sendMessage($template, $context, $user->getEmail());
    }

    /**
     * @param string $templateName
     * @param array  $context
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $toEmail)
    {
        $message = $this->mailer->newMessage('Подтверждение регистрации на сайте %title%');
        $message->setTo($toEmail);
        $this->mailer->sendTemplate($message, $templateName, $context);
    }
}
