<?php

namespace Q\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Универсальный контроллер для административной части
 */
abstract class RestUniversalController extends Controller
{
    /**
     * Имя управляемой сущности
     */
    abstract protected function getEntityName();

    /**
     * Экшн формы создания сущности
     */
    abstract protected function getCreateFormAction();

    /**
     * Экшн формы редактирования сущности
     */
    abstract protected function getEditFormAction();

    /**
     * Генерация имени формы
     * @param  string $prefix
     * @return string
     */
    protected function getFormName($prefix = '')
    {
        return $prefix.preg_replace('/[^a-z]/', '_', mb_strtolower($this->getEntityName()));
    }

    /**
     * Получение списка элементов
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('e');
        $this->updateListQueryBuilder($request, $queryBuilder);

        Paginator::disableQueryCache();

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Эту функцию можно переопределить для задания дополнительных параметров запроса
     * @param  Request $request
     * @param  QueryBuilder $qb
     * @return QueryBuilder
     */
    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        //
    }

    /**
     * Создание сущности
     */
    public function createAction(Request $request)
    {
        $entity = $this->getRepository()->createNew();

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, $this->getRepository()->getManyToManyFields(), $this->getFormName('create_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'entity' => $entity);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование сущности
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, $this->getRepository()->getManyToManyFields(), $this->getFormName('edit_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'entity' => $entity);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Удаление сущности
     */
    public function removeAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $this->getDoctrine()->getManager()->remove($entity);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }

    /**
     * Рендер формы добавления
     */
    public function createFormAction(Request $request)
    {
        $entity = $this->getRepository()->createNew();

        $this->beforeCreateForm($request, $entity);

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->setAction($this->generateUrl($this->getCreateFormAction()))
            ->getForm();

        return array('form' => $form->createView());
    }

    /**
     * Вызывается перед созданием формы новой сущности
     * @param  Request $request
     * @param  mixed  $entity
     */
    public function beforeCreateForm(Request $request, $entity)
    {
        //
    }

    /**
     * Рендер формы редактирования
     */
    public function editFormAction($id)
    {
        $entity = $this->getRepository()->findOne($id);

        $this->beforeEditForm($this->get('request'), $entity);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->setAction($this->generateUrl($this->getEditFormAction(), array('id' => $id)))
            ->getForm();

        return array('form' => $form->createView(), 'entity' => $entity);
    }

    /**
     * Вызывается перед созданием формы редактирования сущности
     * @param  Request $request
     * @param  mixed  $entity
     */
    public function beforeEditForm(Request $request, $entity)
    {
        //
    }

    /**
     * Сортировка элемента
     */
    public function actionSortOfParent(Request $request, $order)
    {
        $id = $request->request->get('id');
        $parent = $request->get('parent');

        if (!$id) {
            throw new \Exception('Id not setted');
        }

        if ($parent) {
            $entity = $this->getRepository()->findOneBy(array('id' => $id, 'parent' => $parent));
        } else {
            $entity = $this->getRepository()->findOneBy(array('id' => $id));
        }

        if (!$entity) {
            throw new \Exception('Entity not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        // Поставить элемент перед каким-то конкретным
        if (preg_match('/^before-\d+$/', $order)) {
            $destEntity = $this->getRepository()->find(str_replace('before-', '', $order));

            if (!$destEntity) {
                throw new \Exception('Destination entity not found [before]');
            }

            $this->getRepository()->sortEntity($entity, $destEntity, 'prev');
            $em->getConnection()->commit();

            return array('success' => true, 'moved' => true);
        }

        // Поставить элемент после какого-то конкретного
        if (preg_match('/^after-\d+$/', $order)) {
            $destEntity = $this->getRepository()->find(str_replace('after-', '', $order));

            if (!$destEntity) {
                throw new \Exception('Destination entity not found [after]');
            }

            $this->getRepository()->sortEntity($entity, $destEntity, 'next');
            $em->getConnection()->commit();

            return array('success' => true, 'moved' => true);
        }

        switch($order) {
            case 'up':
                $destEntity = $this->getRepository()->getPrevItem($entity, $parent);
                if (!$destEntity) {
                    return array('success' => true, 'moved' => false);
                } else {
                    $this->getRepository()->sortEntity($entity, $destEntity, 'prev');
                    $em->getConnection()->commit();
                    return array('success' => true, 'moved' => true);
                }

            case 'down':
                $destEntity = $this->getRepository()->getNextItem($entity, $parent);
                if (!$destEntity) {
                    return array('success' => true, 'moved' => false);
                } else {
                    $this->getRepository()->sortEntity($entity, $destEntity, 'next');
                    $em->getConnection()->commit();
                    return array('success' => true, 'moved' => true);
                }

            default:
                return array('success' => false, 'moved' => false);
        }
    }

    /**
     * Перенос сущности в другой раздел
     * @param  integer $entityId ID сущности
     * @param  integer $nodeId   ID раздела
     * @param  string $moveType Тип перемещения
     * @return array
     */
    public function actionMoveEntity($entityId, $nodeId, $moveType = 'last')
    {
        $em = $this->getDoctrine()->getManager();

        $node = $em->getRepository('QCoreBundle:Node')->find($nodeId);
        $entity = $this->getRepository()->find($entityId);

        if (!$node || !$entity) {
            throw $this->createNotFoundException('Node or entity not exists');
        }

        if (!method_exists($entity, 'setParent')) {
            throw $this->createNotFoundException('Method setParent not exists');
        }

        $em->getConnection()->beginTransaction();
        $entity->setParent($node);

        if (method_exists($entity, 'setSort')) {
            switch($moveType) {
                case 'first':
                    $entity->setSort(-10);
                    $em->flush();

                    $query = $this->getRepository()
                        ->createQueryBuilder('e')
                        ->addOrderBy('e.sort');

                    if (method_exists($entity, 'getParent')) {
                        $query->andWhere('e.parent = :parent');
                        $query->setParameter('parent', $entity->getParent()->getId());
                    }

                    $query = $query->getQuery();

                    $i = 10;
                    foreach($query->iterate() as $item) {
                        $item[0]->setSort($i);
                        $i += 10;
                    }
                break;

                case 'last':
                    if (method_exists($entity, 'getParent')) {
                        $entity->setSort($this->getRepository()->getNextSort($entity->getParent()->getId()));
                    } else {
                        $entity->setSort($this->getRepository()->getNextSort());
                    }
                break;
            }
        }

        $em->flush();
        $em->getConnection()->commit();

        return $entity;
    }

    /**
     * Репозиторий управляемой сущности
     * @return EntityRepository
     */
    protected function getRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository($this->getEntityName());
    }
}
