<?php
include_once 'conf.php';

$item = new items();
$object = $item->get_all();

foreach($object as $house) {
    $status = intval($house['status']);
    switch($status){
        case 1:
            $icon = 'icon_blue';
            $iconblock = '<span class="status_icon"></span>';
            $txtstatus = '<li id="house_status">'.$arrStatus[$status].'</li>';
            break;
        case 2:
            $icon = 'icon_orange';
            $iconblock = '<span class="status_icon"></span>';
            $txtstatus = '<li id="house_status">'.$arrStatus[$status].'</li>';
            break;
        case 3:
            $icon = 'icon_red';
            $iconblock = '<span class="status_icon"></span>';
            $txtstatus = '<li id="house_status">'.$arrStatus[$status].'</li>';
            break;
        case 4:
            $icon = 'icon_green';
            $iconblock = '<span class="status_icon"></span>';
            $txtstatus = '<li id="house_status">'.$arrStatus[$status].'</li>';
            break;
        default:
            $icon = '';
            $iconblock = '';
            $txtstatus = '';
    }
    ?><div class="house_item" id="<?php echo $icon;?>">
        <?php echo $iconblock;?>
        <div class="item_descr">
            <h8><?php echo $house['title'];?></h8>
            <p><?php echo $house['address'];?></p>
            <ul><?php
                echo $txtstatus;
                foreach($house['fields'] as $valoption) {
                    echo '<li>'.$valoption['name'].': '.$valoption['body'].'</li>';
                }
          ?></ul>
        </div>
    </div><?php
}?>