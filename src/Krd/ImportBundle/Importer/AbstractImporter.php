<?php

namespace Krd\ImportBundle\Importer;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Абстрактный импортер
 */
abstract class AbstractImporter
{
    protected $em;
    protected $oldStructure;

    protected $output;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em->getConfiguration()->setResultCacheImpl(new \Krd\ImportBundle\Doctrine\Cache\NoCache());
    }

    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function outputWriteln($str)
    {
        if ($this->output instanceof OutputInterface) {
            $this->output->writeln($str);
        }
    }

    public function outputWrite($str)
    {
        if ($this->output instanceof OutputInterface) {
            $this->output->write($str);
        }
    }
}
