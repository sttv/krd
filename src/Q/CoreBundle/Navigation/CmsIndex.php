<?php

namespace Q\CoreBundle\Navigation;

/**
 * Пункт меню главной страницы CMS
 */
class CmsIndex extends Item
{
    public function getTitle()
    {
        return 'Администрирование';
    }

    public function getLink()
    {
        return $this->router->generate('cms_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS_ADMIN');
    }
}
