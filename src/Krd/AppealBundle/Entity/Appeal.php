<?php

namespace Krd\AppealBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;


/**
 * Обращение
 *
 * @ORM\Entity
 * @ORM\Table(name="appeal",
 *      indexes={
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="status", columns={"status"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     destination={"entity", {
 *         "label"="Адресат обращения",
 *         "class"="KrdAppealBundle:Destination",
 *         "property"="title",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     name={"text", {"label"="Имя"}},
 *     lastname={"text", {"label"="Фамилия"}},
 *     midname={"text", {"label"="Отчество"}},
 *     phone={"text", {"label"="Телефон", "required"=false}},
 *     email={"text", {"label"="Email", "required"=false}},
 *     postcode={"text", {"label"="Индекс"}},
 *     city={"text", {"label"="Город"}},
 *     district={"text", {"label"="Район", "required"=false}},
 *     street={"text", {"label"="Улица"}},
 *     house={"text", {"label"="Дом/корпус"}},
 *     room={"text", {"label"="Квартира"}},
 *     message={"textarea", {"label"="Описание"}},
 *     replyType={"text", {"label"="Как направить ответ"}},
 *     appealFiles={"files", {"class"="QFilesBundle:File", "label"="Файлы", "required"=false}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Appeal
{
    const STATUS_NEW = 'новое';
    const STATUS_MAILED = 'отправлено';
    const STATUS_EXPORTED = 'экспортировано';
    const STATUS_DRAFT = 'черновик';

//    const WITH_FILE_EMAIL = '2672230@mail.ru';
    const WITH_FILE_EMAIL = 'oog2@krd.ru';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @JMS\Expose
     * @JMS\Groups({"user"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Назначение обращения
     *
     * @ORM\ManyToOne(targetEntity="Destination")
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $destination;

    /**
     * Имя
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $name;

    /**
     * Фамилия
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $lastname;

    /**
     * Отчество
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $midname;

    /**
     * Телефон
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $phone;

    /**
     * Email
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $email;

    /**
     * Индекс
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $postcode;

    /**
     * Город
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $city;

    /**
     * Район
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $district;

    /**
     * Улица
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $street;

    /**
     * Номер дома
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $house;

    /**
     * Номер квартиры
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $room;

    /**
     * Сообщение
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $message;

    /**
     * Статус
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     */
    private $status;

    /**
     * Дата обращения
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     * @Assert\DateTime
     */
    private $date;

    /**
     * Информация о запросе
     *
     * @ORM\Column(type="array", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $request = array();

    /**
     * IP адрес пользователя
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $ip = '0.0.0.0';

    /**
     * Откуда пришло обращение
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $gotFrom = 'Сайт';

    /**
     * Как направить ответ
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $replyType;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="appeal_files_relation",
     *      joinColumns={@ORM\JoinColumn(name="appeal_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $appealFiles;

    /**
     * Экспорт обращения
     * @throws \Exception
     * @return boolean
     */
    public function export()
    {
        if($this->hasAppealFiles()) {

            //
        } else {
            $options = array();
            $options[CURLOPT_URL] = 'http://krdtools.very-good.ru/appeal.php';
            $options[CURLOPT_RETURNTRANSFER] = true;
            $options[CURLOPT_HEADER] = false;
            $options[CURLOPT_HTTPHEADER] = array('Accept-Language: ru,en-us;q=0.7,en;q=0.3');
            $options[CURLOPT_POST] = true;

            $options[CURLOPT_POSTFIELDS] =
                "fio1=".urlencode(iconv('utf-8', 'windows-1251', $this->getName()))
                ."&fio2=".urlencode(iconv('utf-8', 'windows-1251', $this->getLastname()))
                ."&fio3=".urlencode(iconv('utf-8', 'windows-1251', $this->getMidname()))
                ."&adresat=".urlencode(iconv('utf-8', 'windows-1251', "Адресат: ".$this->getDestination()->getTitle().PHP_EOL.$this->getMessage()))
                ."&index=".urlencode(iconv('utf-8', 'windows-1251', $this->getPostcode()))
                ."&city=".urlencode(iconv('utf-8', 'windows-1251', $this->getCity()))
                ."&area=".urlencode(iconv('utf-8', 'windows-1251', $this->getDistrict()))
                ."&street=".urlencode(iconv('utf-8', 'windows-1251', $this->getStreet()))
                ."&house=".urlencode(iconv('utf-8', 'windows-1251', $this->getHouse()))
                ."&flat=".( $this->getRoom() != '' ? urlencode(iconv('utf-8', 'windows-1251', $this->getRoom())) : 0 )
                ."&phone=".urlencode(iconv('utf-8', 'windows-1251', $this->getPhone()))
                ."&email=".urlencode(iconv('utf-8', 'windows-1251', $this->getEmail()))
            ;

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);
            curl_close($ch);

            if ($result == 'success export') {
                $this->setStatus(self::STATUS_EXPORTED);
                return true;
            } else {
                throw new \Exception($result);
            }
        }
    }

    /**
     * @return string
     */
    public function getHtmlFilesFromHtml($files = null) {

        if(empty($files)) {

            $files = $this->getAppealFiles();
        }

        $result = '';
        foreach ($files as $f) {

            $name = $f->getTitle();
            $file = $f->getFile();
            $path = 'http://krd.ru'.$file['path'];

            $result.='<a href="' . $path . '">' . $name . '</a><br>';
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAppealFiles() {

        return $this->appealFiles ?: $this->appealFiles=new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function hasAppealFiles() {

        return count($this->getAppealFiles()) > 0;
    }

    /**
     * @param File $file
     */
    public function removeAppealFiles(File $file) {

        if ($this->getAppealFiles()->contains($file)) {

            $this->getAppealFiles()->removeElement($file);
        }
    }

    /**
     * @param File $file
     */
    public function addAppealFiles(File $file) {

        if (!$this->getAppealFiles()->contains($file)) {

            $this->getAppealFiles()->add($file);
        }
    }

    /**
     * @param type $files
     */
    public function setAppealFiles($files) {

       if(!empty($files)) {

            if($files instanceof ArrayCollection) {

                $this->appealFiles = $files;
            } elseif(is_string($files)) {

                $_f = json_decode($files);

                if(!json_last_error()) {

                    $_f = (array)$_f;

                    $this->appealFiles = new ArrayCollection($_f);
                }
            }
        }
    }

    public function clearAppealFiles() {

        $this->appealFiles = new ArrayCollection();
    }

    /**
     * Черновик?
     * @return boolean
     *
     * @JMS\Groups({"user"})
     * @JMS\VirtualProperty
     */
    public function isDraft()
    {
        return $this->getStatus() == self::STATUS_DRAFT;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGotFrom()
    {
        return $this->gotFrom;
    }

    /**
     * @param string $gotFrom
     */
    public function setGotFrom($gotFrom)
    {
        $this->gotFrom = $gotFrom;
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Appeal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Appeal
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set midname
     *
     * @param string $midname
     * @return Appeal
     */
    public function setMidname($midname)
    {
        $this->midname = $midname;

        return $this;
    }

    /**
     * Get midname
     *
     * @return string
     */
    public function getMidname()
    {
        return $this->midname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Appeal
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Appeal
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set postcode
     *
     * @param integer $postcode
     * @return Appeal
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return integer
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Appeal
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Appeal
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Appeal
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set house
     *
     * @param integer $house
     * @return Appeal
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set room
     *
     * @param integer $room
     * @return Appeal
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Appeal
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Appeal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Appeal
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set request
     *
     * @param array $request
     * @return Appeal
     */
    public function setRequest(array $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Appeal
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set destination
     *
     * @param Destination $destination
     * @return Appeal
     */
    public function setDestination(Destination $destination = null)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    public function hasDestination()
    {
        return $this->getDestination() instanceof Destination;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Полное имя отправителя
     * @return string
     */
    public function getFullName()
    {
        $r = array();

        if ($this->getLastname()) {
            $r[] = $this->getLastname();
        }

        if ($this->getName()) {
            $r[] = $this->getName();
        }

        if ($this->getMidname()) {
            $r[] = $this->getMidname();
        }

        return implode(' ', $r);
    }

    /**
     * Полный адрес отправителя
     * @return string
     */
    public function getAddress()
    {
        $r = '';

        if ($this->getPostcode()) {
            $r .= $this->getPostcode();
        }

        if ($this->getCity()) {
            $r .= (!empty($r) ? ', г. ' : 'г. ').$this->getCity();
        }

        if ($this->getDistrict()) {
            $r .= (!empty($r) ? ', район ' : 'район ').$this->getDistrict();
        }

        if ($this->getStreet()) {
            $r .= (!empty($r) ? ', ул. ' : 'ул. ').$this->getStreet();
        }

        if ($this->getHouse()) {
            $r .= (!empty($r) ? ', д. ' : 'д. ').$this->getHouse();
        }

        if ($this->getRoom()) {
            $r .= (!empty($r) ? ', кв. ' : 'кв. ').$this->getRoom();
        }

        return $r;
    }

    /**
     * @return mixed
     */
    public function getReplyType()
    {
        return $this->replyType;
    }

    /**
     * @param mixed $replyType
     */
    public function setReplyType($replyType)
    {
        $this->replyType = $replyType;
    }
}
