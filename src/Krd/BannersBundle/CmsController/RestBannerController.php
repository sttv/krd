<?php

namespace Krd\BannersBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер баннеров
 *
 * @Route("/rest/krd/banners/banner")
 * @PreAuthorize("hasRole('ROLE_CMS_BANNER')")
 */
class RestBannerController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdBannersBundle:Banner';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_banners_banner_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_banners_banner_edit';
    }

    /**
     * Список
     * Можно передавать $_GET['parent'] для выборки по радителю
     *
     * @Route("/", name="cms_rest_krd_banners_banner", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($type = $request->get('type')) {
            $qb->andWhere('e.type = :type')
                ->setParameter('type', $type);
        }
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_banners_banner_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_banners_banner_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_banners_banner_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_banners_banner_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        $entity->setType($request->get('type', ''));
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_banners_banner_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }
}
