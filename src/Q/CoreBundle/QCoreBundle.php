<?php

namespace Q\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Q\CoreBundle\DependencyInjection\Compiler\CMSNavigationPass;

class QCoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new CMSNavigationPass());
    }
}
