<?php

namespace Krd\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\CoreBundle\Entity\Node;


/**
 * Поиск
 */
class SearchController extends Controller implements ActiveSecuredController
{
    /**
     * Экшн всегда возвращает html, т.к. лень дублировать пачку шаблонов в ангуляр
     *
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $searchword = $request->query->get('q', '');
        $page = (int)$request->query->get('page', 1);
        $onpage = 20;

        if (!$searchword) {
            return $this->render('KrdSearchBundle:Search:index.html.twig', array(
                        'results' => false,
                        'searchword' => $searchword
                    ));
        }

        $sphinxSearch = $this->get('iakumai.sphinxsearch.search');

        $sphinxSearch->SetFieldWeights(array(
            'seo_title' => 200,
            'title' => 200,
            'fio' => 500,
            'announce' => 90,
            'seo_desription' => 90,
            'content' => 130,
            'contentMobile' => 130,
            'reply' => 60,
            'address' => 60,
            'question' => 60,
            'updated' => 1,
        ));

        $sphinxSearch->SetIndexWeights(array(
            'QCoreNodeIndex' => 1,
            'QCoreContentIndex' => 25,
            'KrdSiteFaqIndex' => 1,
            'KrdSiteSuborganizationIndex' => 1,
            'KrdSiteVacancyIndex' => 1,
            'KrdNewsNewsIndex' => 7,
            'KrdGlavaSpeechIndex' => 7,
            'KrdGalleryGalleryIndex' => 1,
            'KrdGlavaVideoIndex' => 2,
            'KrdEvacuationAvtoIndex' => 1,
            'KrdDocumentDocumentIndex' => 2,
            'KrdConferenceOnlineIndex' => 1,
            'KrdConferenceQuestionIndex' => 1,
            'KrdCityCityobjectIndex' => 1,
            'KrdAdministrationHumanIndex' => 10,
            'KrdAdministrationMayorIndex' => 1,
        ));

        $sphinxSearch->setMatchMode(SPH_MATCH_EXTENDED2);
        $sphinxSearch->setRankingMode(SPH_RANK_EXPR, 'sum((4*lcs+2*(min_hit_pos==1)+exact_hit*200)*user_weight)*1000+bm25');
        $sphinxSearch->setLimits(($page - 1)*$onpage, $onpage);

        switch($request->query->get('sort', 'relevance')) {
            default:
            case 'relevance':
                $sphinxSearch->setSortMode(SPH_SORT_EXTENDED, '@weight DESC');
                $request->query->set('sort', 'relevance');

                break;

            case 'date':
                $sphinxSearch->setSortMode(SPH_SORT_EXTENDED, 'updated DESC');

                break;
        }

        if ($datestart = $request->query->get('date-start')) {
            $datestart = \DateTime::createFromFormat('d.m.Y', $datestart);
        }

        if (!$datestart) {
            $datestart = null;
        }

        if ($dateend = $request->query->get('date-end')) {
            $dateend = \DateTime::createFromFormat('d.m.Y', $dateend);
        }

        if (!$dateend) {
            $dateend = null;
        }

        if ($datestart > $dateend) {
            $datestart = null;
            $request->query->set('date-start', null);

            $dateend = null;
            $request->query->set('date-end', null);
        }

        $sphinxSearch->setFilterBetweenDates('updated', $datestart, $dateend);

        $indexes = $this->container->getParameter('sphinx.indexes');

        $searchResults = $sphinxSearch->searchEx($searchword, $indexes);
        $searchResults['page'] = $page;
        $searchResults['onpage'] = $onpage;

        return $this->render('KrdSearchBundle:Search:index.html.twig', array(
                        'results' => $searchResults,
                        'searchword' => $searchword
                    ));
    }
}
