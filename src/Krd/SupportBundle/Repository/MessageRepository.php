<?php

namespace Krd\SupportBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\UserBundle\Entity\User;
use Krd\SupportBundle\Entity\Message;
use Krd\SupportBundle\Entity\Notify;


/**
* Репозиторий Message
*/
class MessageRepository extends EntityRepository
{
    /**
     * Помечает ветку сообщений как прочитанную
     * @param  Message $message
     * @param  User $user
     */
    public function markAsReaded(Message $message, User $user)
    {
        $notifyRepo = $this->_em->getRepository('KrdSupportBundle:Notify');

        $query = $this->createQuery("SELECT m FROM __CLASS__ m WHERE m.left >= :left AND m.right <= :right AND m.root = :root");
        $query->setParameters(array(
            'left' => $message->getLeft(),
            'right' => $message->getRight(),
            'root' => $message->getRoot(),
        ));

        foreach ($query->iterate() as $msg) {
            $msg = $msg[0];

            $notify = $notifyRepo->findOneBy(array('message' => $msg->getId(), 'user' => $user->getId()));

            if (!$notify) {
                $notify = new Notify($msg, $user);
                $this->_em->persist($notify);
                $this->_em->flush($notify);
            }
        }
    }

    /**
     * Проверяет состояние прочитанности сообщения
     * @param  Message $message
     * @param  User    $user
     */
    public function updateReaded(Message $message, User $user)
    {
        $notifyRepo = $this->_em->getRepository('KrdSupportBundle:Notify');

        $query = $this->createQuery("SELECT m FROM __CLASS__ m WHERE m.left >= :left AND m.right <= :right AND m.root = :root");
        $query->setParameters(array(
            'left' => $message->getLeft(),
            'right' => $message->getRight(),
            'root' => $message->getRoot(),
        ));

        $message->setReaded(true);
        foreach ($query->iterate() as $msg) {
            $msg = $msg[0];

            $notify = $notifyRepo->findOneBy(array('message' => $msg->getId(), 'user' => $user->getId()));

            if (!$notify) {
                $message->setReaded(false);
                return;
            }
        }
    }
}
