<?php

namespace Krd\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;


/**
 * Сотрудник администрации
 *
 * @ORM\Entity
 * @ORM\Table(name="adm_human",
 *      indexes={
 *          @ORM\Index(name="showlink", columns={"showlink"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     fio={"text", {"label"="ФИО"}},
 *     fior={"text", {"label"="ФИО (кем?)"}},
 *     name={"text", {"label"="Системное имя"}},
 *     post={"textarea", {
 *         "label"="Должность",
 *         "required"=false
 *     }},
 *     postMobile={"textarea", {
 *         "label"="Должность (моб. приложение)",
 *         "required"=false
 *     }},
 *     postlink={"text", {
 *         "label"="Возглавляемое подразделение<br /><em>относительная ссылка</em>",
 *         "required"=false
 *     }},
 *     parentHuman={"entity", {
 *         "label"="Подчиненный у",
 *         "class"="KrdAdministrationBundle:Human",
 *         "property"="fio",
 *         "multiple"=false,
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     links={"text_collection", {"label"="Дополнительные ссылки"}},
 *     contacts={"textarea", {
 *         "label"="Контактная информация",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     sheduleTitle={"text", {"label"="Заголовок графика приема"}},
 *     shedule={"text_collection", {
 *         "label"="График приема<br /><em>Прим.: 21.06.2013 с 16:00 по 19:00</em>"
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     showlink={"checkbox", {
 *         "label"="Выводить ссылку на профиль в онлайн-конференциях?",
 *         "required"=false
 *     }},
 *     linktitle={"text", {"label"="Текст кнопки для онлайн-конференций", "required"=false}},
 *     bigimages={"files", {
 *         "label"="Большое фото",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     images={"files", {
 *         "label"="Среднее фото",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     whiteimages={"files", {
 *         "label"="Мини-фото<br /><em>прозрачный фон</em>",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Human implements NodeUrlIntf
{
    /**
     * Репозиторий для ленивой загрузки Node ссылок
     * @var EntityRepository
     */
    protected $nodeRepository;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="Human", inversedBy="childrenHuman")
     * @ORM\JoinColumn(name="parent_human_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @JMS\Expose
     */
    private $parentHuman;

    /**
     * @ORM\OneToMany(targetEntity="Human", mappedBy="parentHuman")
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $childrenHuman;

    /**
     * ФИО
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $fio;

    /**
     * ФИО в родительном падеже
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $fior;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;

    /**
     * Должность
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $post = '';

    /**
     * Должность для моб. приложения
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $postMobile = '';

    /**
     * Ссылка на возглавляемое подразделение
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $postlink = '';

    /**
     * Node относящийся к указанной ссылке
     */
    private $postlinknode;

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';

    /**
     * Дополнительные ссылки
     * @ORM\Column(type="array")
     *
     * @Gedmo\Versioned
     */
    private $links;

    /**
     * Список Node относящихся к дополнительным ссылкам
     */
    private $linksNodes;

    /**
     * Контактная информация
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $contacts = '';

    /**
     * Заголовок графика приема
     * @ORM\Column(type="string", name="shedule_title", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $sheduleTitle;

    /**
     * График приема граждан
     * @ORM\Column(type="array")
     *
     * @Gedmo\Versioned
     */
    private $shedule = array();

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * Выводить ссылку на профиль в онлайн-конференциях?
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $showlink = false;

    /**
     * Подпись на кнопке
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $linktitle;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="adm_human_bigimage_relation",
     *      joinColumns={@ORM\JoinColumn(name="human_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $bigimages;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="adm_human_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="human_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="adm_human_whiteimage_relation",
     *      joinColumns={@ORM\JoinColumn(name="human_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $whiteimages;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getMainImage")
     */
    private $mainImage;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("whiteimage_main")
     * @JMS\Accessor(getter="getMainWhiteImage")
     */
    private $mainWhiteImage;

    public function setNodeRepository(EntityRepository $rep)
    {
        $this->nodeRepository = $rep;
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParentHuman(Human $parentHuman = null)
    {
        $this->parentHuman = $parentHuman;
    }

    public function getParentHuman()
    {
        return $this->parentHuman;
    }

    public function getChildrenHuman()
    {
        return $this->childrenHuman ?: $this->childrenHuman = new ArrayCollection();
    }

    public function hasChildrenHuman()
    {
        return count($this->getChildrenHuman()) > 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Инициалы + фамилия
     */
    public function getFioShort()
    {
        $tmp = explode(' ', $this->fio, 3);
        return mb_substr($tmp[1], 0, 1).'. '.mb_substr($tmp[2], 0, 1).'. '.$tmp[0];
    }

    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    public function getFior()
    {
        return $this->fior;
    }

    public function setFior($fior)
    {
        $this->fior = $fior;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function hasPost()
    {
        return !empty($this->post);
    }

    public function setPost($post)
    {
        $this->post = $post;
    }

    public function getPostMobile()
    {
        return $this->postMobile;
    }

    public function hasPostMobile()
    {
        return !empty($this->postMobile);
    }

    public function setPostMobile($postMobile)
    {
        $this->postMobile = $postMobile;
    }

    public function getPostLink()
    {
        return $this->postlink;
    }

    public function hasPostLink()
    {
        return !empty($this->postlink);
    }

    public function setPostLink($postlink)
    {
        $this->postlink = $postlink;
    }

    public function getPostLinkNode()
    {
        if ($this->hasPostLink() && is_null($this->postlinknode) && !is_null($this->nodeRepository)) {
            $this->postlinknode = $this->nodeRepository->getNodeByPath($this->getPostLink());
        }

        return $this->postlinknode;
    }

    public function hasPostLinkNode()
    {
        return $this->getPostLinkNode() instanceof Node;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function hasContent()
    {
        return !empty($this->content);
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getLinks()
    {
        return $this->links ?: array();
    }

    public function hasLinks()
    {
        return !empty($this->links);
    }

    public function setLinks(array $links)
    {
        $this->links = $links;
    }

    public function getLinksNodes()
    {
        if ($this->hasLinks() && is_null($this->linksNodes) && !is_null($this->nodeRepository)) {
            $this->linksNodes = array();

            foreach($this->getLinks() as $link) {
                $node = $this->nodeRepository->getNodeByPath($link);

                if ($node) {
                    $this->linksNodes[] = $node;
                }
            }
        }

        return $this->linksNodes ?: array();
    }

    public function hasLinksNodes()
    {
        return count($this->getLinksNodes()) > 0;
    }

    public function setLinksNodes(array $linksNodes)
    {
        $this->linksNodes = $linksNodes;
    }

    public function getContacts()
    {
        return $this->contacts;
    }

    public function hasContacts()
    {
        return !empty($this->contacts);
    }

    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
    }

    public function getSheduleTitle()
    {
        return $this->sheduleTitle;
    }

    public function setSheduleTitle($sheduleTitle)
    {
        $this->sheduleTitle = $sheduleTitle;
    }

    public function getShedule()
    {
        return $this->shedule ?: array();
    }

    /**
     * Распарсенный график работы
     * @return array
     */
    public function getSheduleAsDate()
    {
        $result = array();

        foreach($this->getShedule() as $rawItem) {
            $tmp = explode(' ', $rawItem, 2);
            $date = \DateTime::createFromFormat('d.m.Y', $tmp[0])->format('d/m/Y');
            $result[$date] = $tmp[1];
        }

        return $result;
    }

    /**
     * Отсортированный список месяцев расписания
     * @return array
     */
    public function getSheduleMonths()
    {
        $list = array();

        foreach($this->getShedule() as $rawItem) {
            $tmp = explode(' ', $rawItem, 2);
            $list[] = (int)\DateTime::createFromFormat('d.m.Y', $tmp[0])->format('m');
        }

        $list = array_unique($list);
        sort($list);

        return $list;
    }

    /**
     * Первый месяц графика
     * @return integer
     */
    public function getSheduleFirstMonth()
    {
        $list = $this->getSheduleMonths();
        return current($list);
    }

    /**
     * Средний месяц графика
     * @return integer
     */
    public function getSheduleSecondMonth()
    {
        $list = $this->getSheduleMonths();

        if (count($list) % 2 === 0) {
            $index = (int)(count($list) - 1) / 2;
        } else {
            $index = (int)count($list) / 2;
        }

        if (isset($list[$index])) {
            return $list[$index];
        } else {
            return end($list);
        }
    }

    /**
     * Последний месяц графика
     * @return integer
     */
    public function getSheduleThirdMonth()
    {
        $list = $this->getSheduleMonths();
        return end($list);
    }

    /**
     * Массив со списком месяцев для приема. Используется для избедания дублей
     */
    public function getSheduledMonths()
    {
        $list = array($this->getSheduleFirstMonth(), $this->getSheduleSecondMonth(), $this->getSheduleThirdMonth());

        $list = array_unique($list);
        sort($list);

        return $list;
    }

    public function setShedule(array $shedules)
    {
        $this->shedules = array();

        foreach($shedules as $item) {
            $this->addShedule($item);
        }
    }

    public function hasShedule($shedule = null)
    {
        if (is_null($shedule)) {
            return !empty($this->shedule);
        } else {
            return in_array($shedule, $this->shedule, true);
        }
    }

    public function addShedule($shedule)
    {
        if (!empty($shedule) && !$this->hasShedule($shedule)) {
            $this->shedule[] = $shedule;
        }
    }

    public function removeShedule($shedule)
    {
        if (($index = array_search($shedule, $this->shedule, true)) !== false) {
            unset($this->shedule[$index]);
            $this->shedule = array_values($this->shedule);
        }
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getShowlink()
    {
        return $this->showlink;
    }

    public function setShowlink($showlink)
    {
        $this->showlink = (boolean)$showlink;
    }

    public function getBigImages()
    {
        return $this->bigimages ?: $this->bigimages = new ArrayCollection();
    }

    public function hasBigImages()
    {
        return count($this->getBigImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainBigImage()
    {
        foreach($this->getBigImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getBigImages();

        return $images[0];
    }

    public function addBigImages(Image $image)
    {
        if (!$this->getBigImages()->contains($image)) {
            $this->getBigImages()->add($image);
        }
    }

    public function removeBigImages(Image $image)
    {
        if ($this->getBigImages()->contains($image)) {
            $this->getBigImages()->removeElement($image);
        }
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function getWhiteImages()
    {
        return $this->whiteimages ?: $this->whiteimages = new ArrayCollection();
    }

    public function hasWhiteImages()
    {
        return count($this->getWhiteImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainWhiteImage()
    {
        foreach($this->getWhiteImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $whiteimages = $this->getWhiteImages();

        return $whiteimages[0];
    }

    public function addWhiteImages(Image $image)
    {
        if (!$this->getWhiteImages()->contains($image)) {
            $this->getWhiteImages()->add($image);
        }
    }

    public function removeWhiteImages(Image $image)
    {
        if ($this->getWhiteImages()->contains($image)) {
            $this->getWhiteImages()->removeElement($image);
        }
    }

    public function getLinkTitle()
    {
        return $this->linktitle;
    }

    public function hasLinkTitle()
    {
        return !empty($this->linktitle);
    }

    public function setLinkTitle($linktitle)
    {
        $this->linktitle = $linktitle;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getDetailRoute()
    {
        return 'krd_human_detail';
    }
}


