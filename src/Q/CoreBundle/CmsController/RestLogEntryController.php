<?php

namespace Q\CoreBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Контроллер QCoreBundle:ShowLogEntry
 *
 * @Route("/rest/gedmo/loggable/log_entry")
 * @PreAuthorize("hasRole('ROLE_CMS_ADMIN')")
 */
class RestLogEntryController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'QCoreBundle:ShowLogEntry';
    }

    protected function getCreateFormAction()
    {
        return 'disabled';
    }

    protected function getEditFormAction()
    {
        return 'disabled';
    }

     /**
     * Список
     *
     * @Route("/", name="cms_rest_gedmo_loggable_log_entry", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.objectClass NOT IN (:disabled)')
            ->setParameter('disabled', array('Krd\SiteBundle\Entity\Poll', 'Krd\SiteBundle\Entity\PollAnswer'))
            ->addOrderBy('e.loggedAt', 'DESC');

        $result = Paginator::createFromRequest($request, $queryBuilder->getQuery());

        foreach ($result as &$item) {
            $entity = $this->getDoctrine()->getManager()->find($item->getObjectClass(), $item->getObjectId());

            if ($entity) {
                $item->setEntity($entity);
            }
        }

        return $result;
    }
}
