<?php

namespace Krd\AppealBundle\Modules;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;
use Q\CoreBundle\Entity\Node;
use Q\UserBundle\Entity\User;
use Krd\AppealBundle\Entity\Appeal as AppealEntity;


/**
 * Модуль обращений
 */
class Appeal extends AbstractModule
{
    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var User
     */
    protected $user;

    /**
     * @param SecurityContext $securityContext
     */
    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdAppealBundle:Admin:Module/appeal.html.twig');
    }

    /**
     * Форма обращения
     */
    public function renderContent()
    {
        $appeal = new AppealEntity();
        $appeal->setParent($this->node->getNode());

        if ($this->user instanceof User) {
            $appeal->setName($this->user->getName());
            $appeal->setLastname($this->user->getLastname());
            $appeal->setMidname($this->user->getSurname());
            $appeal->setPhone($this->user->getPhone());
            $appeal->setEmail($this->user->getEmail());

            $appeal->setPostcode($this->user->getAddressIndex());
            $appeal->setCity($this->user->getAddressCity());
            $appeal->setDistrict($this->user->getAddressRegion());
            $appeal->setStreet($this->user->getAddressStreet());
            $appeal->setHouse($this->user->getAddressHome());
            $appeal->setRoom($this->user->getAddressRoom());
        }

        if ($this->request->getSession()->getFlashBag()->has('appeal.form.data')) {
            $appealAr = $this->request->getSession()->getFlashBag()->get('appeal.form.data');

            if (isset($appealAr['appeal'])) {
                $appealAr = $appealAr['appeal'];

                if (isset($appealAr['destination'])) {
                    $appeal->setDestination($this->em->getRepository('KrdAppealBundle:Destination')->find($appealAr['destination']));
                    unset($appealAr['destination']);
                }

                foreach ($appealAr as $key => $val) {
                    $method = 'set'.ucfirst($key);

                    if (method_exists($appeal, $method)) {
                        if ($method == 'setParent') {
                            if ($val instanceof Node) {
                                $appeal->setParent($val);
                            }
                        } else {
                            $appeal->$method($val);
                        }
                    }
                }
            }
        }

        $formBuilder = $this->getAppealFormBuilder($appeal);
        $formBuilder->setAction($this->router->generate('krd_appeal_appeal_submit_form'));
        $formView = $formBuilder->getForm()->createView();

        return $this->twig->render('KrdAppealBundle:Module:appeal/form.html.twig', array('form' => $formView));
    }

    /**
     * Фронтэнд форма для обращений в интернет приемной
     * @param  mixed $appeal
     * @return FormBuilder
     */
    public function getAppealFormBuilder(AppealEntity $appeal = null, $options = array())
    {
        return $this->formFactory->createNamedBuilder('appeal', 'appeal', $appeal, $options);
    }

    public function onNodeRemove(Node $node)
    {
    }
}
