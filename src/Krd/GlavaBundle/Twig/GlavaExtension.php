<?php

namespace Krd\GlavaBundle\Twig;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;


/**
 * Расширение Twig
 */
class GlavaExtension extends Twig_Extension
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig()
    {
        return $this->container->get('twig');
    }
	
	public function getFilters()
    {
        return array(
            'declension' => new \Twig_Filter_Function(array($this, 'declension')),
        );
    }
	
	public function getName()
    {
        return 'glava_extension';
    }
	
	public function declension($number, $forms, $implode = false)
    {
        $cases = array(2, 0, 1, 1, 1, 2);

        return ($implode ? $number . ' ' : '') . 
            $forms[(($number % 100 > 4) && ($number % 100 < 20)) ? 2 : $cases[($number % 10 < 5) ? ($number % 10) : 5]];
    }
}