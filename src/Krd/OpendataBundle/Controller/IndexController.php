<?php

namespace Krd\OpendataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Открытые данные
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * Главная страница
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        // Информация для окна экспорта
        if ($ew = $request->query->get('exportwindow')) {
            $result = $this->get('krd.opendata.modules.opendata')->getExportWindow($ew);

            return new JsonResponse($result);
        }

        // Экспорт данных
        if ($export = $request->query->get('export')) {
            $format = $request->query->get('format', 'csv');

            switch ($format) {
                default:
                    throw $this->createNotFoundException();

                case 'xml':
                    $serializer = new Serializer(array(), array(new XmlEncoder('opendata')));

                    $result = $this->get('krd.opendata.modules.opendata')->getExport($export);
                    $response = new Response($serializer->serialize($result, 'xml'));
                    $response->headers->set('Content-Type', 'text/xml');
                    $response->headers->set('Content-Disposition', 'attachment; filename=opendata.xml');

                    return $response;

                case 'json':
                    $result = $this->get('krd.opendata.modules.opendata')->getExport($export);
                    return new JsonResponse($result);

                case 'csv':
                    $temp_file = tempnam(sys_get_temp_dir(), 'csv');
                    $outputBuffer = fopen($temp_file, 'w+');

                    $result = $this->get('krd.opendata.modules.opendata')->getExport($export);

                    array_unshift($result, array('0000', 'Заголовок', 'Описание', 'Адрес', 'Телефон', 'Районы', 'Точка на карте', 'Категория', 'Подкатегория', 'Рейтинг'));

                    foreach($result as $p) {
                        fputcsv($outputBuffer, $p, ';');
                    }
                    fclose($outputBuffer);

                    $data = file_get_contents($temp_file);
                    @unlink($temp_file);

                    $response = new Response($data);
                    $response->headers->set('Content-Type', 'text/csv');
                    $response->headers->set('Content-Disposition', 'attachment; filename=opendata.csv');

                    return $response;
            }
        }
    }

    /**
     * Карта учреждений
     *
     * @Route("/list/")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Список учреждений', 'url' => '#'));

        // Сообщение об ошибке
        if ($request->query->get('errorreport') == 1) {
            $name = htmlspecialchars(strip_tags($request->request->get('name')));
            $page = htmlspecialchars(strip_tags($request->request->get('page')));
            $description = htmlspecialchars(strip_tags($request->request->get('description')));
            $type = (int)$request->request->get('type');

            switch($type) {
                case 1:
                    $type = 'Неверная информация';
                    break;

                case 2:
                    $type = 'Орфографическая ошибка';
                    break;

                case 3:
                    $type = 'Функциональная ошибка';
                    break;

                case 4:
                    $type = 'Просто совет';
                    break;

                case 5:
                    $type = 'Сообщение для администрации проекта';
                    break;

                default:
                    $type = 'Неизвестная ошибка';
                    break;
            }

            if (!empty($page) && !empty($type) && !empty($description)) {
                $this->get('krd.opendata.modules.opendata')->errorReport($name, $page, $description, $type);

                $mailer = $this->container->get('qcore.mailer');

                $message = $mailer->newMessage('Новое сообщение об ошибке на сайте %title%');

                $message->addTo('krdru@yandex.ru');
                $message->addTo('2672230@mail.ru');

                $mailer->sendTemplate($message, 'KrdOpendataBundle:Email:error-report.admin.html.twig', array('name' =>$name, 'type' => $type, 'link' => $page, 'description' => $description));

                return new JsonResponse(array('success' => true));
            } else {
                return new JsonResponse(array('success' => false));
            }
        }

        // Голосование за заведение
        if ($request->query->get('action') == 'vote') {
            $id = (int)$request->request->get('id');
            $rate = (int)$request->request->get('rate');

            if (!$id || !$rate || $rate < 1 || $rate > 10) {
                return new JsonResponse(array('success' => false));
            }

            if ($request->cookies->has('voted-'.$id)){
                return new JsonResponse(array('success' => false));
            }

            $result = $this->get('krd.opendata.modules.opendata')->voteForPlace($id, $rate);

            $response = new JsonResponse(array('success' => $result !== false, 'rate' => $result));

            $response->headers->setCookie(new Cookie('voted-'.$id, '1', time()+60*60*60*60*60, '/'));

            return $response;
        }
    }

    /**
     * ПОдробная страница учреждения
     *
     * @Route("/list/{name}.htm")
     * @Template()
     */
    public function detailAction()
    {
        $current = $this->get('krd.opendata.modules.opendata')->getCurrent();

        if ($current) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $current['title'], 'url' => '#'));
        } else {
            throw $this->createNotFoundException();
        }
    }


    /**
     * Инфографика
     *
     * @Route("/infographic/")
     * @Template()
     */
    public function infographicAction(Request $request)
    {
        $type = $request->query->get('type');
        $valid = array('health', 'social', 'culture', 'sport');

        if (!in_array($type, $valid)) {
            throw $this->createNotFoundException();
        }

        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Инфографика', 'url' => '#'));
    }
}
