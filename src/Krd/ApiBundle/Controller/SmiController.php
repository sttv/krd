<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * СМИ города
 *
 * @Route("/smi")
 */
class SmiController extends Controller
{
    /**
     * Список
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdCityBundle:Smi')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->setParameter('parent', $parent)
            ->orderBy('n.sort', 'ASC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }
}
