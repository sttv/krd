<?php

namespace Q\CoreBundle\Swift;

use Swift_Mailer;
use Swift_Message;
use Twig_Environment;

use Q\CoreBundle\Entity\Config;


/**
 * Удобная отправкаи писем через Swift Mailer
 * Автоматически подставляет настройки сайта в отправляемые письма
 *
 * Для использования в Listser'ах доктрины лениво подгружайте сервис из контейнера
 */
class Mailer
{
    protected $mailer;
    protected $config;
    protected $twig;

    public function __construct(Swift_Mailer $mailer, Config $config, Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->config = $config;
        $this->twig = $twig;
    }

    /**
     * Создание нового пустого сообщения
     * К сообщению применяются стандартные параметры
     *
     * Заменяет %title% в теме на название приложения
     *
     * @param string[optional] $subject Тема письма
     *
     * @return Swift_Message
     */
    public function newMessage($subject = null)
    {
        $message = Swift_Message::newInstance()
            ->setFrom(array($this->config->getEmail() => $this->config->getTitleEmail()))
            ->setTo($this->config->getAlertEmails())
        ;

        if (!is_null($subject)) {
            $subject = str_replace('%title%', $this->config->getTitleEmail(), $subject);
            $message->setSubject($subject);
        }

        return $message;
    }

    /**
     * Отправка шаблона Twig в качестве сообщений
     *
     * В шаблон можно передавать параметры
     *
     * @param  Swift_Message $message    Письмо
     * @param  string $template   Шаблон twig
     * @param  array $parameters параметры для шаблона
     *
     * @return  integer Количество отправленных писем
     */
    public function sendTemplate(Swift_Message $message, $template, array $parameters = null)
    {
        $parameters['__message'] = $message;
        $message->setBody($this->twig->render($template, $parameters), 'text/html');

        return $this->sendNotAware($message);
    }

    /**
     * Отсылает письма для всех адресатов из setTo() отдельно друг от друга
     *
     * @param  Swift_Message $message
     * @return integer Количество отправленных писем
     */
    public function sendNotAware(Swift_Message $message)
    {
        $failedRecipients = array();
        $numSent = 0;

        $to = $message->getTo();

        foreach ($to as $address => $name) {
            if (is_int($address)) {
                $message->setTo($name);
            } else {
                $message->setTo(array($address => $name));
            }

            $numSent += $this->mailer->send($message, $failedRecipients);
        }

        return $numSent;
    }
}
