<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\KrdUserEvents;
use Krd\UserBundle\Form\Type\UserProfileType;


/**
 * Документы в личном кабинете
 */
class DocumentController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/", name="krd_user_documents")
     * @Template("KrdUserBundle:Index:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Редактирование документа
     *
     * @Route("/{name}.html", name="krd_user_documents_item")
     * @Template()
     */
    public function detailAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($item = $this->get('krd.user.modules.document')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Директории текущего пользователя
     * @Route("/directories.{_format}", name="krd_user_documents_list", defaults={"_format"="json"}, requirements={"_format"="^json$"})
     */
    public function listAction()
    {
        $result = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory')->getMyDirectories();

        if (empty($result)) {
            $result[] = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory')->createMyDirectory(array('title' => 'Мои документы'));
        }

        return $result;
    }

    /**
     * Создание директории
     *
     * @Route("/directory/", name="krd_user_documents_directory_create", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function createDirectoryAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory');

        if ($directory = $repo->createMyDirectory($request->request->all())) {
            return $directory;
        } else {
            throw new \Exception('Error while creating directory');
        }
    }

    /**
     * Создание документа
     *
     * @Route("/document/", name="krd_user_documents_document_create", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function createDocumentAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Document');
        $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory');

        if ($document = $repo->createMyDocument($request->request->all())) {
            return $document;
        } else {
            throw new \Exception('Error while creating document');
        }
    }

    /**
     * Редактирование директорий
     *
     * @Route("/directory/{id}", name="krd_user_documents_directory_edit", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function editDirectoryAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory');

        if ($directory = $repo->editMyDirectory($id, $request->request->all())) {
            return $directory;
        } else {
            throw new \Exception('Directory not found');
        }
    }

    /**
     * Редактирование документов
     *
     * @Route("/document/{id}", name="krd_user_documents_document_edit", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function editDocumentAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Document');

        if ($document = $repo->editMyDocument($id, $request->request->all())) {
            return $document;
        } else {
            throw new \Exception('Directory not found');
        }
    }

    /**
     * Удаление документов или папок
     *
     * @Route("/document/remove/", name="krd_user_documents_document_remove", requirements={"id"="^\d+$"}, defaults={"_format"="json"})
     * @Method({"DELETE"})
     */
    public function removeDocumentAction(Request $request)
    {
        if ($directories = $request->request->get('directories')) {
            $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:DocumentDirectory');

            foreach ($directories as $id) {
                $repo->removeMyDirectory($id);
            }
        }

        if ($documents = $request->request->get('documents')) {
            $repo = $this->getDoctrine()->getManager()->getRepository('KrdUserBundle:Document');

            foreach ($documents as $id) {
                $repo->removeMyDocument($id);
            }
        }
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
