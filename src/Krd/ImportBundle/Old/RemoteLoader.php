<?php

namespace Krd\ImportBundle\Old;

use Symfony\Component\DomCrawler\Crawler;
use Doctrine\Common\Cache\ApcCache;


/**
 * Загрузчик удаленного контента с old.krd.ru
 */
class RemoteLoader
{
    /**
     * Кеш запросов. используется не везде
     */
    protected $cache;


    public function __construct()
    {
        $this->cache = new ApcCache();
        $this->cache->setNamespace('krd.import.remote_loader');
    }

    /**
     * Поисковый запрос по параметрам
     * Возвращает массив с ID найденных документов
     * @param  array  $params
     * @return array
     */
    public function searchRequest(array $params)
    {
        $url = 'http://old.krd.ru/www/norma.nsf/BaseSearch?SearchView&Start=1';

        if (isset($params['searchfuzzy']) && $params['searchfuzzy'] == 'TRUE') {
            $url .= '&SearchFuzzy=TRUE';
            unset($params['searchfuzzy']);
        }

        if (isset($params['searchorder']) && !empty($params['searchorder'])) {
            $url .= '&SearchOrder='.$params['searchorder'];
            unset($params['searchorder']);
        } else {
            $url .= '&SearchOrder=4';
        }

        $query = array();

        $subject = array();

        if (isset($params['sword1']) && !empty($params['sword1'])) {
            $subject[] = '%5BSubject%5D%20CONTAINS%20'.urlencode($params['sword1']);
            unset($params['sword1']);
        }

        if (isset($params['sword2']) && !empty($params['sword2'])) {
            $subject[] = '%5BSubject%5D%20CONTAINS%20'.urlencode($params['sword2']);
            unset($params['sword2']);
        }

        if (isset($params['sword3']) && !empty($params['sword3'])) {
            $subject[] = '%5BSubject%5D%20CONTAINS%20'.urlencode($params['sword3']);
            unset($params['sword3']);
        }


        if (!empty($subject)) {
            $glue = $params['selector1'] == 'AND' ? '%20AND%20' : '%20OR%20';
            $query[] = implode($glue, $subject);
        }

        $body = array();

        if (isset($params['bword1']) && !empty($params['bword1'])) {
            $body[] = '%5BBody%5D%20CONTAINS%20'.urlencode($params['bword1']);
            unset($params['bword1']);
        }

        if (isset($params['bword2']) && !empty($params['sword2'])) {
            $body[] = '%5BBody%5D%20CONTAINS%20'.urlencode($params['bword2']);
            unset($params['bword2']);
        }

        if (isset($params['bword3']) && !empty($params['bword3'])) {
            $body[] = '%5BBody%5D%20CONTAINS%20'.urlencode($params['bword3']);
            unset($params['bword3']);
        }


        if (!empty($body)) {
            $glue = $params['selector2'] == 'AND' ? '%20AND%20' : '%20OR%20';
            $query[] = implode($glue, $body);
        }

        unset($params['selector1']);
        unset($params['selector2']);

        if (!empty($params['startdate'])) {
            $query[] = 'FIELD%20SignDate>%3D'.$params['startdate'];
            unset($params['startdate']);
        }

        if (!empty($params['enddate'])) {
            $query[] = 'FIELD%20SignDate<%3D'.$params['enddate'];
            unset($params['enddate']);
        }

        if (!empty($params)) {
            foreach($params as $k => $v) {
                if (!empty($v)) {
                    $query[] = 'FIELD%20'.$k.'%3D'.urlencode($v);
                }
            }
        }

        if (!empty($query)) {
            $query = implode('%20AND%20', $query);
            $url .= '&Query='.$query;
        }

        $html = $this->getRemoteContent($url, 'windows-1251', true);

        $result = array();

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($html, 'utf-8');

            foreach($crawler->filter('td.main a') as $link) {
                $link = new Crawler($link);
                $tmp = explode('/', $link->attr('href'));
                $hash = end($tmp);

                if (!empty($hash) && preg_match('/^[a-z0-9]{32}$/i', $hash)) {
                    $result[] = $hash;
                }
            }
        } catch (\InvalidArgumentException $e) {
            // none
        }

        return $result;
    }

    /**
     * Страница со список всех документов
     * @return string
     */
    public function getNormasListPage()
    {
        return $this->getRemoteContent('http://old.krd.ru/www/norma.nsf/BaseSearch', 'windows-1251');
    }

    /**
     * Получение заголовка нормы
     * @param  string $hash
     * @return string
     */
    public function getNormaTitle($hash)
    {
        $html = $this->getRemoteContent('http://old.krd.ru/www/norma.nsf/BaseSearch/'.$hash, 'windows-1251', true);

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($html, 'utf-8');
            $title = $crawler->filter('body')->filter('div:first-child');
        } catch (\InvalidArgumentException $e) {
            return '';
        }

        $title = strip_tags($title->html());
        $title = mb_strtolower($title);
        $title = mb_strtoupper(mb_substr($title, 1, 1)).mb_substr($title, 2);

        $title2 = $crawler->filter('body')->filter('div:first-child + table tr:first-child td:first-child')->html();

        $title .= ' '.$title2;

        return $title;
    }

    /**
     * Получение даты нормы
     * @param  string $hash
     * @return string
     */
    public function getNormaDate($hash)
    {
        $html = $this->getRemoteContent('http://old.krd.ru/www/norma.nsf/BaseSearch/'.$hash, 'windows-1251', true);

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($html, 'utf-8');
            $date = $crawler->filter('body > div:first-child + table > tr:first-child > td');
        } catch (\InvalidArgumentException $e) {
            return '';
        }

        $date = strip_tags($date->html());
        $date = mb_strtolower($date);

        preg_match_all('/\d{2}[.\/]\d{2}[.\/]\d{4}/', $date, $matches);

        if (isset($matches[0]) && isset($matches[0][0])) {
            $date = \DateTime::createFromFormat('m/d/Y', $matches[0][0]);
        }

        return $date;
    }

    /**
     * Получение анонса нормы
     * @param  string $hash
     * @return string
     */
    public function getNormaAnnounce($hash)
    {
        $html = $this->getRemoteContent('http://old.krd.ru/www/norma.nsf/BaseSearch/'.$hash, 'windows-1251', true);

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($html, 'utf-8');
            $crawler = $crawler->filter('body')->filter('div:first-child + table');

            return '<table class="no-design">'.$crawler->html().'</table>';
        } catch (\InvalidArgumentException $e) {
            return '';
        }
    }

    /**
     * Получение детального контента нормы
     * @param  string $hash
     * @return string
     */
    public function getNormaDetail($hash)
    {
        $html = $this->getRemoteContent('http://old.krd.ru/www/norma.nsf/BaseSearch/'.$hash, 'windows-1251', true);

        try {
            $crawler = new Crawler();
            $crawler->addHtmlContent($html, 'utf-8');
            $crawler = $crawler->filter('body');

            $html = $crawler->html();
        } catch (\InvalidArgumentException $e) {
            return '';
        }

        $html = str_replace('<a', '<a target="_blank"', $html);
        $html = str_replace('href="/', 'href="http://old.krd.ru/', $html);
        $html = str_replace('src="/', 'src="http://old.krd.ru/', $html);
        $html = str_replace('face="Times New Roman CYR"', '', $html);
        $html = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);


        $tmp = explode('</table>', $html, 2);
        $html = $tmp[1];

        $html = strip_tags($html, '<p>, <b>, <i>, <u>, <em>, <strong>, <br>, <ul>, <li>, <ol>, <a>, <img>, <table>, <tr>, <td>, <th>');

        $html = trim($html);
        $html = str_replace(PHP_EOL, '', $html);
        $html = str_replace('<p></p>', '', $html);

        if (mb_substr($html, -mb_strlen('<br><br>Ссылки на другие документы<br><br>')) === '<br><br>Ссылки на другие документы<br><br>') {
            $html = str_replace('<br><br>Ссылки на другие документы<br><br>', '', $html);
        }

        if (mb_strpos($html, '<ul>') === 0 && mb_strpos($html, '<ul><li>') !== 0) {
            $html = strip_tags($html, '<p>, <b>, <i>, <u>, <em>, <strong>, <br>, <a>, <img>, <table>, <tr>, <td>, <th>');
        }

        if (mb_strpos($html, '<p') !== 0 && mb_strpos($html, '<') !== 0) {
            $tmp = explode('<p', $html, 2);
            $html = '<p>'.$tmp[0].'</p><p'.$tmp[1];
        }

        return $html;
    }

    /**
     * Загрузка удаленного контента
     * При желании можно кешировать запросы на 15 минут, но по умолчанию эта фича выключена
     * @param  string $url
     * @param  string[optional] $charset
     * @param  boolean[optional] $cache
     * @return string
     */
    public function getRemoteContent($url, $charset = 'utf-8', $cache = false)
    {
        // На время пока сервера лежат
//        return '';

        if ($cache) {
            $cacheHash = md5($url);
            if ($this->cache->contains($cacheHash)) {
                return $this->cache->fetch($cacheHash);
            }
        }

        $tries = 0;

        do {
            $html = file_get_contents($url);
        } while(mb_strlen($html) < 50 && $tries < 5);

        if (!$html) {
            return;
        }

        if ($charset != 'utf-8') {
            $html = iconv($charset, 'utf-8', $html);
            $html = str_replace($charset, 'utf-8', $html);
        }

        if ($cache) {
            $this->cache->save($cacheHash, $html, 30);
        }

        return $html;
    }
}
