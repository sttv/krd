/**
 * Модуль работы с формами
 */
angular.module('forms', [])
    .run([function() {
        if (!$.fn.serializeObject) {
            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });

                return o;
            };
        }
    }])

    /**
     * Всплывающие сообщения
     */
    .service('$bubble', [function() {
        /**
         * Класс сообщения в зависимости от типа
         */
        var types = {
            'default': '',
            'error': 'error'
        };

        var bubble = {
            /**
             * Удаление всплывающего сообщения у элемента
             */
            remove: function($element) {
                var $message;

                if ($element.length > 1) {
                    $element.each(function() {
                        bubble.remove($(this));
                    });

                    return;
                }

                if ($message = $element.data('forms.bubble.message')) {
                    $message.fadeOut(150, function() {
                        $message.remove();
                    });

                    $element.data('forms.bubble.message', null);
                }
            },

            /**
             * Показ сообщения об ошибка
             * @param  {string} message Сообщение
             * @param  {object} $element  Элемент jQuery (не обязательно)
             */
            error: function(message, $element) {
                if ($element) {
                    $element.each(function() {
                        bubble.showOnElement(message, $(this), 'error');
                    });
                }
            },

            /**
             * Показывает сообщение над элементом
             * Если элемент не виден, то ищет первого видимого родителя
             *
             * @param  {string} message Сообщение
             * @param  {objecy} $element  Элемент
             * @param  {string} type Тип сообщения
             */
            showOnElement: function(message, $element, type) {
                var $exists;

                if ($exists = $element.data('forms.bubble.message')) {
                    bubble.remove($element);
                    setTimeout(function() {
                        bubble.showOnElement(message, $element, type);
                    }, 150);
                    return;
                }

                var $message = $('<div class="bubble"></div>');

                if (type && types[type]) {
                    $message.addClass(types[type]);
                }

                $message.html(message);

                $message.remove().appendTo('body').css({visibility:'hidden', top:0, left:0});

                $message.on('click', function() {
                    bubble.remove($element);
                });

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                $message.addClass('gravity-'+gravity).fadeIn(150);
                $message.append('<div class="arrow"></div>');

                $element.data('forms.bubble.message', $message);
                bubble.updatePosition($element);

                var interval = setInterval(function() {
                    if ($element.data('forms.bubble.message')) {
                        bubble.updatePosition($element);
                    } else {
                        clearInterval(interval);
                    }
                }, 100);
            },

            /**
             * Обновляет позицию всплывающего сообщения у элемента
             */
            updatePosition: function($element) {
                var $message = $element.data('forms.bubble.message');

                if (!$message) {
                    return;
                }

                var $el = $element;

                if (!$element.is(':visible')) {
                    $el = $element.closest(':visible').first();
                }

                var pos = $.extend({}, $el.offset(), {
                    width: $el[0].offsetWidth,
                    height: $el[0].offsetHeight
                });

                var options = {
                    offset: 0
                };

                var actualWidth = $message[0].offsetWidth,
                    actualHeight = $message[0].offsetHeight;

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                var tp = {opacity:1, visibility:'visible'};

                switch (gravity.charAt(0)) {
                    case 'n':
                        tp.top = pos.top + pos.height + options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 's':
                        tp.top = pos.top - actualHeight - options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 'e':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left - actualWidth - options.offset;
                        break;

                    case 'w':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left + pos.width + options.offset;
                        break;
                }

                if (gravity.length > 1) {
                    if (gravity.charAt(1) == 'w') {
                        tp.left = pos.left + pos.width / 2 - 15;
                    } else {
                        tp.left = pos.left + pos.width / 2 - actualWidth + 15;
                    }
                }

                $message.css(tp);
            }
        };

        return bubble;
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Аяксовая форма
     * Можно добавить свой контроллер с методами onSuccess, onError, onFatalError
     */
    .directive('ajaxForm', ['$bubble', '$http', function($bubble, $http) {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.find('input, textarea').each(function() {
                    $(this).attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.action;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input, textarea').on('change keyup', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors = {};
                        });
                    });

                    $element.on('submit', function(e) {
                        e.preventDefault();

                        return false;
                    });

                    $scope.$watch('errors', $scope.updateBubbles, true);
                }
            },

            controller: function($scope, $element) {
                $scope.submit = function() {
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                if (!!$scope.onSuccess) {
                                    $scope.onSuccess(response);
                                }
                            } else {
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }

                                if (!!$scope.onError) {
                                    $scope.onError();
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            if (!!$scope.onFatalError) {
                                $scope.onFatalError(response);
                            }

                            console.error(response);
                        });
                };

                /**
                 * Обновление всплывающих ошибок
                 */
                $scope.updateBubbles = function() {
                    $bubble.remove($element.find('input, select, textarea'));

                    var i = 0;
                    for(var name in $scope.errors) {
                        if (!$scope.errors.hasOwnProperty(name)) {
                            continue;
                        }

                        var errors = $scope.errors[name];

                        if (!errors || errors.length == 0) {
                            continue;
                        }

                        var $field = $element.find('*[name="'+name+'"]').first();

                        $bubble.error(errors.join(', '), $field);

                        i++;

                        if (i == 1 && $.scrollTo) {
                            var $showEl = $field;

                            if (!$showEl.is(':visible')) {
                                $showEl = $showEl.closest(':visible').first();
                            }

                            if ($showEl.length > 0) {
                                $.scrollTo($showEl, 200, {offset:{top:-150}});
                            }
                        }
                    }
                };

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                };
            }
        }
    }])
;
