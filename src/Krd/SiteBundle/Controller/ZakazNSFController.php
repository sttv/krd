<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Скачивание файлов www/zakaz.nsf
 */
class ZakazNSFController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/www/zakaz.nsf/{type}/{hash}/$file/{filename}", requirements={"type"="[a-zA-Z]+", "hash"="[A-F0-9]+", "filename"=".*"})
     */
    public function downloadAction($type, $hash, $filename)
    {
        $link = '/oldkrd/municipalnyy_zakaz/'.mb_strtolower($type).'/'.$hash.'/'.$this->rewriteFilename($filename);

        return $this->redirect($link);
    }

    /**
     * Транслитерация имени файла
     * @param  string $filename
     * @return string
     */
    protected function rewriteFilename($filename)
    {
        $tr = array('А'=>'A','а'=>'a','Б'=>'B','б'=>'b','В'=>'V','в'=>'v','Г'=>'G','г'=>'g',
            'Д'=>'D','д'=>'d','Е'=>'E','е'=>'e','Ё'=>'Yo','ё'=>'yo','Ж'=>'Zh','ж'=>'zh',
            'З'=>'Z','з'=>'z','И'=>'I','и'=>'i','Й'=>'Y','й'=>'y','К'=>'K','к'=>'k',
            'Л'=>'L','л'=>'l','М'=>'M','м'=>'m','Н'=>'N','н'=>'n','О'=>'O','о'=>'o',
            'П'=>'P','п'=>'p','Р'=>'R','р'=>'r','С'=>'S','с'=>'s','Т'=>'T','т'=>'t',
            'У'=>'U','у'=>'u','Ф'=>'F','ф'=>'f','Х'=>'Kh','х'=>'kh','Ц'=>'Ts','ц'=>'ts',
            'Ч'=>'Ch','ч'=>'ch','Ш'=>'Sh','ш'=>'sh','Щ'=>'Sch','щ'=>'sch','Ъ'=>'"','ъ'=>'"',
            'Ы'=>'Y','ы'=>'y','Ь'=>"'",'ь'=>"'",'Э'=>'E','э'=>'e','Ю'=>'Yu','ю'=>'yu',
            'Я'=>'Ya','я'=>'ya'
        );

        $filename = strtr($filename, $tr);
        $filename = mb_strtolower($filename);
        $filename = strtr($filename, ' -/', '___');
        $filename = preg_replace("/[^a-zа-я_0-9ё\.]/i", '_', $filename);
        $filename = mb_substr($filename, 0, 100);

        return $filename;
    }
}
