<?php

namespace Krd\CommentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\UserBundle\Entity\User;


/**
 * Комментарии
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Krd\CommentsBundle\Repository\CommentRepository")
 * @ORM\Table(name="comment",
 *      indexes={
 *          @ORM\Index(name="root", columns={"root"}),
 *          @ORM\Index(name="leftkey", columns={"`left`"}),
 *          @ORM\Index(name="rightkey", columns={"`right`"}),
 *          @ORM\Index(name="lvl", columns={"level"}),
 *          @ORM\Index(name="l_r", columns={"`left`", "`right`"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="pageHash", columns={"pageHash"}),
 *          @ORM\Index(name="page", columns={"page"}),
 *      }
 * )
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     content={"textarea", {
 *         "label"="Комментарий",
 *         "required"=true
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Comment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @JMS\Expose
     */
    private $id;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="`left`", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     * @JMS\Expose
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="`right`", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @ORM\Column(type="string", length=1200, nullable=true)
     * @JMS\Expose
     */
    private $page;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     */
    private $pageTitle;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $pageHash;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     * @ORM\OrderBy({"created" = "ASC"})
     */
    private $children;

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Нужно написать комментарий")
     * @JMS\Expose
     */
    private $content = '';

    /**
     * Сокращенное содержание
     * @JMS\Expose
     * @JMS\Accessor(getter="getContentMini")
     */
    private $contentMini;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @JMS\Expose
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     */
    private $ip;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y (H:i)'>")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set left
     *
     * @param integer $left
     * @return Comment
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return integer
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Comment
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set right
     *
     * @param integer $right
     * @return Comment
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return integer
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return Comment
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return str_replace('&amp;', '&', $this->content);
    }

    public function getContentMini()
    {
        if (mb_strlen($this->getContent()) > 50) {
            return mb_substr($this->getContent(), 0, 46) . '...';
        } else {
            return $this->getContent();
        }
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Comment
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Comment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Comment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Krd\CommentsBundle\Entity\Comment $parent
     * @return Comment
     */
    public function setParent(\Krd\CommentsBundle\Entity\Comment $parent = null)
    {
        $this->parent = $parent;
        $this->setPageHash($parent->getPageHash());

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Krd\CommentsBundle\Entity\Comment
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \Krd\CommentsBundle\Entity\Comment $children
     * @return Comment
     */
    public function addChildren(\Krd\CommentsBundle\Entity\Comment $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Krd\CommentsBundle\Entity\Comment $children
     */
    public function removeChildren(\Krd\CommentsBundle\Entity\Comment $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get children with active == 1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActiveChildren(User $user = null)
    {
        if ($user instanceof User) {
            $condition = Criteria::expr()->orX(Criteria::expr()->eq('createdBy', $user->getId()), Criteria::expr()->eq('active', true));
        } else {
            $condition = Criteria::expr()->eq('active', true);
        }

        $criteria = Criteria::create()
            ->andWhere($condition)
            ->orderBy(array('created' => Criteria::ASC));

        return $this->getChildren()->matching($criteria);
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Comment
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Comment
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set pageHash
     *
     * @param string $pageHash
     * @return Comment
     */
    public function setPageHash($pageHash)
    {
        $this->pageHash = $pageHash;

        return $this;
    }

    /**
     * Get pageHash
     *
     * @return string
     */
    public function getPageHash()
    {
        return $this->pageHash;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return Comment
     */
    public function setPage($page)
    {
        $this->page = $page;
        $this->setPageHash(md5($page));

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Ссылка
     * @return string
     *
     * @JMS\VirtualProperty
     */
    public function getUrl()
    {
        return $this->getPage().'#comment-'.$this->getId();
    }

    /**
     * Set pageTitle
     *
     * @param string $pageTitle
     * @return Comment
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * Get pageTitle
     *
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Comment
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }
}