<?php

namespace Q\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Q\CoreBundle\Form\Type\NodeType;

/**
 * Главный контроллер CMS
 */
class IndexController extends Controller
{
    /**
     * Генерация имени формы для конфига сайта
     * @param  string $prefix
     * @return string
     */
    protected function getFormName($prefix = '')
    {
        return $prefix.'qcorebundle_config';
    }

    /**
     * Главная страница CMS
     *
     * @Route("/", name="cms_index")
     * @Template()
     * @Secure(roles="ROLE_CMS")
     */
    public function indexAction()
    {
        if ($this->get('security.context')->isGranted('ROLE_CMS_ADMIN')) {
            $configForm = $this->get('form_metadata.mapper')
                ->createFormBuilder($this->get('qcore.config'), null, array('csrf_protection' => false), $this->getFormName('edit_'))
                ->setAction($this->generateUrl('cms_rest_q_core_config_edit', array('id' => $this->get('qcore.config')->getId())))
                ->getForm();

            return array('configForm' => $configForm->createView());
        } else {
            return $this->redirect($this->get('router')->generate('cms_struct_index'));
        }
    }

    /**
     * Выбор страницы для переадресации пользователя после авторизации
     * Всегда редиректит куда-либо
     *
     * @Route("/classkeeper_account_index/", name="classkeeper_account_index")
     */
    public function accountIndexAction()
    {
        try {
            if ($this->get('security.context')->isGranted('ROLE_CMS')) {
                return $this->redirect($this->get('router')->generate('cms_struct_index'));
            } else {
                return $this->redirect('/');
            }
        } catch (\Exception $e) {
            return $this->redirect('/');
        }
    }
}
