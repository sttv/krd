/**
 * Техподдержка
 */
angular.module('support', ['forms', 'comments'])
    /**
     * Контроллер формы создания сообщения
     */
    .controller('SupportCreateFormCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.onSuccess = function() {
            $element.slideUp(function() {
                $('#support-create-success').slideDown();

                setTimeout(function() {
                    window.location.href = window.location.pathname + '#tab-support-list';
                    window.location.reload();
                }, 3000);
            })
        };
    }])

    /**
     * Контроллер формы ответа
     */
    .controller('SupportMessageReplyFormCtrl', ['$scope', 'notifications', function($scope, notifications) {
        $scope.onSuccess = function(response) {
            notifications.user.info(response.message || 'Ваш запрос получен, мы ответим в ближайшее время.');

            if (!!$scope.$parent.toggleOpen) {
                $scope.$parent.toggleOpen(false);
            }

            $scope.content = '';
        };

        $scope.onFatalError = function() {
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Список сообщений
     */
    .directive('supportMessagesList', ['commentsData', function(commentsData) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                commentsData.registerList(attrs.supportMessagesList);
            }
        }
    }])

    /**
     * Элемент сообщений
     */
    .directive('supportMessageItem', ['user', 'commentsData', '$timeout', function(user, commentsData, $timeout) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.children('.links')
                    .after('<div class="well" ng-include="formTemplate" ng-if="isOpen()"></div>')
                    .children('.action-reply').attr('ng-click', 'toggleOpen()')
                    .parent()
                    .children('.action-delete')
                        .attr('ng-if', 'isAdmin')
                        .attr('ng-click', 'remove()')
                ;

                return function($scope, $element, attrs) {
                    $scope.id = parseInt($element.data('id'), 10);
                    $scope.formTemplate = 'support-message-create-well';
                    $scope.shared = {};
                    $scope.isAdmin = user.isGranted('ROLE_COMMENT_ADMIN');
                    $timeout(function() {
                        $scope.shared = commentsData.get(attrs.supportMessageItem);
                    });

                    $scope.$watch('isOpen()', function(open) {
                        if (open) {
                            $element.addClass('open');
                        } else {
                            $element.removeClass('open');
                        }
                    });
                }
            },

            controller: ['$scope', '$element', '$http', 'router', 'notifications', function($scope, $element, $http, router, notifications) {
                // Проверка открыто ли сообщение
                $scope.isOpen = function() {
                    return $scope.shared.openedId == $scope.id;
                };

                // Переключение состояния сообщения
                $scope.toggleOpen = function(id) {
                    if (id === false || $scope.isOpen()) {
                        $scope.shared.openedId = -1;
                    } else {
                        $scope.shared.openedId = $scope.id;
                    }
                };
            }]
        }
    }])
;
