/**
 * Эвакуация автотранспорта
 */
angular.module('evacuation', [])
    /**
     * Виджет на главной
     */
    .directive('widgetEvacuation', ['$http', function($http) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var $btn = $element.find('.btn-update');

                $scope.widgetItems = [];
                $scope.widgetLoading = false;

                $btn.on('click', function(e) {
                    e.preventDefault();
                    $scope.$apply(function() {
                        $scope.widgetLoading = true;

                        $http({
                            method:'GET',
                            url: attrs.url
                        }).success(function(response) {
                            $scope.widgetLoading = false;
                            $scope.widgetItems = response.items;
                        }).error(function() {
                            $scope.widgetLoading = false;
                            alert('Произошла ошибка');
                        });
                    });
                });
            },
        }
    }])
;
