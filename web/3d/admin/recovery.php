<?
include_once 'conf.php';
$page = 1;

$reg = new auth();
$htmlText = '';
if (isset($_POST['send'])) {
	$reply = $reg->recovery_pass($_POST['login'], $_POST['mail']);
	if ($reply===true) {
		$htmlText = '<label class="text-success">Новый пароль был выслан вам на почту</label>';
	} else {
		$htmlText = $reply;
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<form action="" method="post">
				<input type="hidden" name="send" value="1" />
				<div class="control-group">
					<?=$htmlText?>
				</div>
				<div class="control-group">
					<label for="login">Логин:</label>
					<input id="login" type="text" name="login" value="<?=@$_POST['login']?>" class="span6">
				</div>
				<div class="control-group">
					<label for="mail">E-mail:</label>
					<input id="mail" type="email" name="mail" class="span6">
				</div>
				<div class="control-group">
					<button type="submit" name="send" class="btn btn-primary input-large">Восстановить пароль</button>&nbsp;<a href="index.php" class="btn">Отмена</a>
				</div>
			</form>
		</div>
	</body>
</html>