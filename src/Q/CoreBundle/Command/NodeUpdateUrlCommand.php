<?php

namespace Q\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Ручное обновление URL нода
 */
class NodeUpdateUrlCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('q:core:node:update:url')
            ->setDescription('Manual update node url')
            ->addArgument('id', InputArgument::REQUIRED, 'Node ID');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('QCoreBundle:Node');

        $node = $repository->find($input->getArgument('id'));

        if (!$node) {
            $output->writeln('<error>Node not found</error>');
            return;
        }

        $em->beginTransaction();
        $repository->updateUrl($node);
        $em->commit();

        $output->writeln('New URL: '.$node->getUrl(true));
    }
}
