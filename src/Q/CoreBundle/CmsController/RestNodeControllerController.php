<?php

namespace Q\CoreBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер типов страниц (контроллеров)
 *
 * @Route("/rest/q/core/nodecontroller")
 * @PreAuthorize("hasRole('ROLE_IDDQD')")
 */
class RestNodeControllerController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'QCoreBundle:NodeController';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_nodecontroller_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_nodecontroller_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_nodecontroller", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->addOrderBy('e.id', 'DESC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_nodecontroller_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_nodecontroller_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_nodecontroller_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        $nodes = $this->getDoctrine()->getManager()
            ->createQuery("SELECT n FROM QCoreBundle:Node n WHERE n.controller = :controller")
            ->setParameter('controller', (int)$id)
            ->setMaxResults(1)
            ->getResult();

        if ($nodes) {
            return array('success' => false, 'msg' => 'Существуют разделы с данным типом страницы. Удаление невозможно');
        }

        return parent::removeAction($request, $id);
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_nodecontroller_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        $entity->setName('Новый тип страницы');
        $entity->setController('@KrdSiteBundle:ContentController');
        $entity->addModule('krd.site.modules.content');
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_nodecontroller_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }
}
