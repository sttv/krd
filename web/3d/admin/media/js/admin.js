$(function(){
  // Open file upload dialog
  $('.file-upload').click(function(){
    var id = '#' + $(this).attr('id');
    id = id.slice(0, -('-upload'.length));
    $(id).click();
  });
  // Display attached file name
  $('input.file').change(function(){
    var id = '#' + $(this).attr('id') + '-upload';
    $(id).val($(this).val());
  });
  $(".fancybox").fancybox();
})