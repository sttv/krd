<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Срочные сообщения
 *
 * @Route("/emergency")
 */
class EmergencyController extends Controller
{
    /**
     * Список
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * Сортировка всегда происходит по дате по убыванию
     *
     * @Route("/list.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdNewsBundle:Emergency')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->orderBy('n.date', 'DESC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Детально
     *
     * @Route("/item/{id}.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function itemAction($id)
    {
        $items = $this->getDoctrine()->getRepository('KrdNewsBundle:Emergency')
            ->createQueryBuilder('n')
            ->andWhere('n.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->setMaxResults(1)
            ->useResultCache(true, 60)
            ->getResult();

        $response = new Response();
        $response->headers->set('Content-type', 'application/json');

        if (empty($items)) {
            $response->setContent(json_encode(array('title' => 'Запись не найдена')));
        } else {
            $item = array_pop($items);
            $json = $this->get('jms_serializer')->serialize($item, 'json');
            $json = json_decode($json, true);
            $json['url'] = 'http://krd.ru' . $item->getUrl();
            $json = json_encode($json);

            $response->setContent($json);
        }

        return $response;
    }
}
