<?php

$token = $_POST['token'];

if (!$token) {
    $rawData = file_get_contents("php://input");
    $data = @json_decode($rawData, true);
    $token = $data['token'];
}

if (!$token) {
    die('no token');
}

$mysqli = new mysqli("127.0.0.1", "krdru_krdru13", "8UHuM1WY", "krdru_krdru13");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$token = $mysqli->real_escape_string($token);
$updated = $mysqli->real_escape_string(date('Y-m-d H:i:s'));

$mysqli->query('REPLACE INTO `krdru13__apn_token` (`token`, `updated`) VALUES ("'.$token.'", "'.$updated.'")');