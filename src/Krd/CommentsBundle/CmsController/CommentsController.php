<?php

namespace Krd\CommentsBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;


/**
 * Контроллер CMS - комментарии
 *
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class CommentsController extends Controller
{
    /**
     * @Route("/comments/", name="cms_krd_comments_index")
     * @Template("KrdCommentsBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {

    }
}
