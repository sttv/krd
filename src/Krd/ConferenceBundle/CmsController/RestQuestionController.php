<?php

namespace Krd\ConferenceBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер Question
 *
 * @Route("/rest/krd/conference/question")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestQuestionController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdConferenceBundle:Question';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_conference_question_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_conference_question_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_conference_question", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->orderBy('e.dateask', 'ASC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_conference_question_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_conference_question_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_conference_question_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_conference_question_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        if (($parent = $request->get('parent')) && ($parent = $this->getDoctrine()->getManager()->getRepository('KrdConferenceBundle:Online')->find($parent))) {
            $entity->setParent($parent);
        }
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_conference_question_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    public function beforeEditForm(Request $request, $entity)
    {
        if (!$entity->getDateReply()) {
            $entity->setDateReply(new \DateTime());

            if ($conference = $entity->getParent()) {
                foreach ($conference->getHumans() as $human) {
                    $entity->addHumans($human);
                }
            }
        }
    }

    /**
     * Шаблон списка элементов
     * @Route("/list_tpl/{id}", name="cms_rest_krd_conference_question_list", defaults={"_format"="html", "id"=""}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("KrdConferenceBundle:Admin:Module/question-list.html.twig")
     */
    public function listByParentAction($id)
    {
        return array('parentId' => $id);
    }
}
