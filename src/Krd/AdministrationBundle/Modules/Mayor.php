<?php

namespace Krd\AdministrationBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль руководителей города
 */
class Mayor extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdAdministrationBundle:Admin:Module/mayor.html.twig');
    }

    /**
     * Текущий человек
     * @return Mayor
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Построение списка
     * @param  integer $root
     * @return array
     */
    public function getItems($root)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array();
        }

        $itemsQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            ->orderBy('d.sort')
            ->setParameter('parent', $rootNode->getId())
            ->getQuery();

        return $itemsQuery->useResultCache(true, 15)->getResult();
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItems($this->node->getNode()->getId());

        return $this->twig->render('KrdAdministrationBundle:Module:mayor/list.html.twig', array('items' => $result));
    }
}
