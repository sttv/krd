<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\AppealBundle\Entity\Appeal;


/**
 * Интернет-приемная
 *
 * @Route("/appeal")
 */
class AppealController extends Controller
{
    /**
     * Список адресатов
     *
     * @Route("/destinations.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function destinationsAction()
    {
        $list = $this->getDoctrine()->getRepository('KrdAppealBundle:Destination')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1 AND e.parent = 6444')
            ->addOrderBy('e.sort')
            ->getQuery()
                ->getResult();

        return $list;

//        $result = array();
//
//        foreach ($list as $item) {
//            $result[] = array('id' => $item->getId(), 'title' => $item->getTitle(), 'url' => $item->getUrl(true));
//        }
//
//        return $result;
    }

    /**
     * Сабмит формы обращения
     *
     * @Route("/submit.{_format}")
     * @Method({"POST"})
     */
    public function submitAction(Request $request)
    {
        $request->request->set('parent', 6444);

        $form = $this->get('form.factory')->createNamedBuilder(null, 'appeal', new Appeal(), array('csrf_protection' => false))->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }

        $appeal = $form->getData();

        if (($appeal instanceof Appeal) && $appeal->getMessage()) {
            $appeal->setDate(new \DateTime());
            $appeal->setStatus(Appeal::STATUS_NEW);

            $requestArr = (array)$request->server->all();

            if (isset($requestArr['HTTP_COOKIE'])) {
                unset($requestArr['HTTP_COOKIE']);
            }

            $appeal->setRequest($requestArr);
            $appeal->setIp($request->getClientIp());

            if ($request->get('mobile')) {
                $appeal->setGotFrom('Мобильное приложение');
            }

            $this->getDoctrine()->getManager()->persist($appeal);
            $this->getDoctrine()->getManager()->flush();

            return array('success' => true, 'successText' => $appeal->getDestination()->getSuccess());
        } else {
            return array('success' => false, 'error' => 'Ошибка сервера');
        }
    }
}
