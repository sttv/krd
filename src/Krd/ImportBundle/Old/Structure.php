<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;


/**
 * Взаимодействие со структурой старого сайта
 */
class Structure
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Получение страницы по сути к ней
     * @param  string $path
     * @return array|NULL
     */
    public function getByPath($path)
    {
        $pathSegments = explode('/', trim($path, '/ '));

        $parent = 1;

        while($request = array_shift($pathSegments)) {
            $statement = $this->em->getConnection()->prepare("SELECT `id` FROM `krd__structure` WHERE `parent` = :parent AND `request` = :request LIMIT 1");
            $statement->execute(array('parent' => $parent, 'request' => $request));
            $structure = $statement->fetch();

            if (!$structure) {
                return false;
            }

            if (!empty($pathSegments)) {
                $parent = (int)$structure['id'];
            }
        }

        if (isset($structure)) {
            return $this->getById($structure['id']);
        }
    }

    /**
     * Получение информации о странице сайта
     * @param integer $id
     * @return array|NULL
     */
    public function getById($id)
    {
        $statement = $this->em->getConnection()->prepare("SELECT * FROM `krd__structure` WHERE `id` = :id");
        $statement->execute(array('id' => $id));
        $structure = $statement->fetch();

        if (!$structure) {
            return null;
        }

        $statement = $this->em->getConnection()->prepare("SELECT `name` FROM `krd__page` WHERE `structure` = :parent LIMIT 1");
        $statement->execute(array('parent' => $structure['id']));

        if ($page = $statement->fetch()) {
            if (!empty($page['name'])) {
                $structure['title'] = $page['name'];
            }
        }

        $statement = $this->em->getConnection()->prepare("SELECT `value` FROM `krd__param` WHERE `structure` = :parent AND `name` = 'links' LIMIT 1");
        $statement->execute(array('parent' => $structure['id']));
        if ($links = @unserialize($statement->fetchColumn(0))) {
            $structure['links'] = $links;
        }

        if ($params = @unserialize($structure['params'])) {
            $structure['params'] = $params;
        }

        $structure['path'] = $this->getPathById($structure['id']);
        $structure['content'] = $this->getContent($structure['id']);
        $structure['files'] = $this->getFiles($structure['id']);
        $structure['images'] = $this->getImages($structure['id']);
        $structure['news'] = $this->getNews($structure['id']);
        $structure['documents'] = $this->getDocuments($structure['id']);

        return $structure;
    }

    /**
     * Получение информации о странице без всего остального
     * @param  integer $id
     * @return array
     */
    public function getByIdSimple($id)
    {
        $statement = $this->em->getConnection()->prepare("SELECT * FROM `krd__structure` WHERE `id` = :id");
        $statement->execute(array('id' => $id));
        $structure = $statement->fetch();

        if (!$structure) {
            return null;
        } else {
            $structure['path'] = $this->getPathById($structure['id']);
            return $structure;
        }
    }

    /**
     * Потомки указанного раздела
     * @param  integer $parentId
     * @return array
     */
    public function getChildrens($parentId)
    {
        $statement = $this->em->getConnection()->prepare("SELECT `id` FROM `krd__structure` WHERE `parent` = :parent ORDER BY `sort` DESC, `title` ASC, `id` ASC");
        $statement->execute(array('parent' => $parentId));

        $result = array();
        while($tmp = $statement->fetch()) {
            $result[] = $this->getById($tmp['id']);
        }

        return $result;
    }

    /**
     * Полный список потомков раздела
     * @param  integer $parentId
     * @return array
     */
    public function getChildrensSimple($parentId)
    {
        $statement = $this->em->getConnection()->prepare("SELECT `id` FROM `krd__structure` WHERE `parent` = :parent ORDER BY `sort` DESC, `title` ASC, `id` ASC");
        $statement->execute(array('parent' => $parentId));

        $result = array();
        while($tmp = $statement->fetch()) {
            $result[] = $this->getByIdSimple($tmp['id']);
        }

        return $result;
    }

    /**
     * URL раздела
     * @param  integer $id
     * @return string
     */
    public function getPathById($id)
    {
        $segments = array();

        $statement = $this->em->getConnection()->prepare("SELECT `id`, `parent`, `request` FROM `krd__structure` WHERE `id` = :id");

        while($id > 0) {
            $statement->execute(array('id' => $id));
            $structure = $statement->fetch();
            $segments[] = $structure['request'];
            $id = (int)$structure['parent'];
        }

        return implode('/', array_reverse($segments)).'/';
    }

    /**
     * Контент страницы
     * @param  integer $parentId ID страницы
     * @param integer $asIs Отдавать контент как есть без дополнительной обработки
     * @return array|NULL
     */
    public function getContent($parentId, $asIs = false)
    {
        $statement = $this->em->getConnection()
            ->prepare("SELECT C.*
                        FROM `krd__content` as C
                        JOIN `krd__relation` as R
                        ON R.`object` = C.`id`
                        WHERE (R.`var` = 'content' OR R.`var` = '') AND R.`structure` = :parent
                        LIMIT 1");
        $statement->execute(array('parent' => $parentId));
        $content = $statement->fetch();

        if (!$content) {
            return NULL;
        }

        $content['date'] = \DateTime::createFromFormat('U', $content['date']);
        $content['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));

        if (!$asIs) {
            $content['content'] = trim($content['content']);
            $content['content'] = str_replace(PHP_EOL, '', $content['content']);
            $content['content'] = str_replace('<p></p>', '', $content['content']);
            $content['content'] = str_replace('<p> </p>', '', $content['content']);
            $content['content'] = str_replace('<span> </span>', ' ', $content['content']);
            $content['content'] = str_replace('<p>'.chr(49824).'</p>', '', $content['content']);
            $content['content'] = str_replace('<p>&nbsp;</p>', '', $content['content']);
            $content['content'] = str_replace('<p><br />', '<p>', $content['content']);
            $content['content'] = str_replace('<p><br /> <br />', '<p>', $content['content']);
            $content['content'] = str_replace('<p><br /><br />', '<p>', $content['content']);
            $content['content'] = str_replace('<p> ', '<p>', $content['content']);
            $content['content'] = str_replace('<p><b><br /></b></p>', '', $content['content']);
            $content['content'] = str_replace('<p><br /></p>', '', $content['content']);
            $content['content'] = str_replace('<p><br class="_mce_marker" /></p>', '', $content['content']);
            $content['content'] = str_replace('<p><br class="_mce_marker" /><br class="_mce_marker" /></p>', '', $content['content']);
            $content['content'] = str_replace('<div>', '', $content['content']);
            $content['content'] = str_replace('</div>', '', $content['content']);
            $content['content'] = preg_replace('/[^\x20-\xFF]/', '', $content['content']);
        }

        return $content;
    }

    /**
     * Список файлов страницы
     * @param  integer $parentId
     * @return array
     */
    public function getFiles($parentId)
    {
        $statement = $this->em->getConnection()->prepare("SELECT * FROM `krd__category` WHERE `module` = 5");
        $statement->execute();

        $categories = array();
        while ($category = $statement->fetch()) {
            $categories[$category['id']] = $category;
        }

        $statement = $this->em->getConnection()
            ->prepare("SELECT F.*
                      FROM `krd__file` as F
                      JOIN `krd__relation` as R
                      ON R.`object` = F.`id`
                      WHERE R.`structure` = :parent AND `module` = 5 AND `type` = 6
                      ORDER BY F.`sort`");

        $statement->execute(array('parent' => $parentId));

        $files = $statement->fetchAll();

        foreach($files as &$file) {
            $file['path'] = "/files/file/".$categories[$file['parent']]['name']."/".$file['file'];
            $file['url'] = 'http://krd.ru'.$file['path'];
            if (isset($file['date']) && $file['date'] > 0) {
                $file['date'] = \DateTime::createFromFormat('U', $file['date']);
            } elseif (is_file('/home/krdru/data/www/krd.ru'.$file['path']) && is_readable('/home/krdru/data/www/krd.ru'.$file['path'])) {
                $file['date'] = \DateTime::createFromFormat('U', filectime('/home/krdru/data/www/krd.ru'.$file['path']));
            }

            if ($file['date'] instanceof \DateTime) {
                $file['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            } else {
                $file['date'] = new \DateTime();
            }
        }

        return $files;
    }

    /**
     * Список изображений на странице
     * @param  integer $parentId
     * @return array
     */
    public function getImages($parentId)
    {
        $stRelation = $this->em->getConnection()->prepare("SELECT `object` FROM `krd__relation` WHERE `structure` = :parent AND `var` = 'gallery_picture' AND `type` = 12");
        $stRelation->execute(array('parent' => $parentId));

        $images = array();
        while($relation = $stRelation->fetch()) {
            $stPicture = $this->em->getConnection()
                ->prepare("SELECT P.*, I.`title`, I.`parent`
                          FROM `krd__picture` as P
                          JOIN `krd__image` as I
                          ON I.`pic` = P.`id`
                          WHERE I.`id` = :id
                          LIMIT 1");
            $stPicture->execute(array('id' => $relation['object']));
            $picture = $stPicture->fetch();

            $picture['path'] = "/file/gallery/content/content/full/".$picture['file'].'.'.$picture['mime'];
            $picture['url'] = 'http://krd.ru'.$picture['path'];

            if (is_file('/home/krdru/data/www/krd.ru'.$picture['path'])) {
                $picture['date'] = \DateTime::createFromFormat('U', filectime('/home/krdru/data/www/krd.ru'.$picture['path']));
            }

            if ($picture['date'] instanceof \DateTime) {
                $picture['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            }

            $images[] = $picture;
        }

        return $images;
    }

    /**
     * Список новостей на странице
     * @param  integer $parentId
     * @return array
     */
    public function getNews($parentId)
    {
        $news = array();

        // Получаем прикрепленную ленту новостей
        $stList = $this->em->getConnection()->prepare("SELECT `object` FROM `krd__relation` WHERE `structure` = :parent AND `module` = 3");
        $stList->execute(array('parent' => $parentId));

        while ($list = $stList->fetch()) {
            $stNews = $this->em->getConnection()->prepare("SELECT * FROM `krd__article` WHERE `parent` = :parent AND `lang` IN (0,1) ORDER BY `date`");
            $stNews->execute(array('parent' => (int)$list['object']));

            while($item = $stNews->fetch()) {
                $item['date_orig'] = $item['date'];
                $item['date'] = \DateTime::createFromFormat('U', $item['date']);
                $item['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                $item['tags'] = $this->getNewsTags($item['id']);
                $item['images'] = $this->getNewsImages($item['id']);

                if (!empty($item['video'])) {
                    $item['video'] = json_decode($item['video'], true);
                    if (!isset($item['video']['src']) || !is_file($item['video']['src'])) {
                        unset($item['video']);
                    }
                }

                if (!empty($item['icon']) && !empty($item['video']) && is_file('/home/krdru/data/www/krd.ru'.$item['icon'].'/full.jpg')) {
                    $item['video']['icon'] = array(
                        'path' => $item['icon'].'/full.jpg',
                        'url' => 'http://krd.ru'.$item['icon'].'/full.jpg',
                        'date' => \DateTime::createFromFormat('U', filectime('/home/krdru/data/www/krd.ru'.$item['icon'].'/full.jpg')),
                    );
                    $item['video']['icon']['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                }

                // Сверям иконку видео и первую картинку
                if (!empty($item['video']) && !empty($item['video']['icon']) && !empty($item['images'])) {
                    $md51 = md5_file('/home/krdru/data/www/krd.ru'.$item['video']['icon']['path']);
                    $md52 = md5_file('/home/krdru/data/www/krd.ru'.$item['images'][0]['path']);
                    if ($md51 == $md52) {
                        array_pop($item['images']);
                    }
                }

                $news[] = $item;
            }
        }



        return $news;
    }

    /**
     * Список тегов новости
     * @param  integer $newsId
     * @return array
     */
    public function getNewsTags($newsId)
    {
        $st = $this->em->getConnection()
            ->prepare("SELECT T.`title`
                      FROM `krd__tag` as T
                      JOIN `krd__relation_tag` as RT
                      ON T.`id` = RT.`tag`
                      WHERE RT.`object` = :parent");
        $st->execute(array('parent' => $newsId));

        $tags = array();

        while ($tag = $st->fetch()) {
            $tag = trim($tag['title']);
            if (!empty($tag) && !in_array($tag, $tags)) {
                $fc = mb_strtoupper(mb_substr($tag, 0, 1));
                $tags[] = $fc.mb_substr($tag, 1);
            }
        }

        return $tags;
    }

    /**
     * Список изображений новости
     * @param  integer $newsId
     * @return arary
     */
    public function getNewsImages($newsId)
    {
        $st = $this->em->getConnection()->prepare("SELECT * FROM `krd__foto` WHERE `parent` = :parent AND `module` = 3 ORDER BY `sort`");
        $st->execute(array('parent' => $newsId));

        $images = array();

        while ($image = $st->fetch()) {
            $image['path'] = "/file/article/".$image['section1']."/".$image['section2']."/".$image['name']."/full.".$image['type'];

            if (!is_file('/home/krdru/data/www/krd.ru'.$image['path'])) {
                continue;
            }

            $image['url'] = 'http://krd.ru'.$image['path'];
            $image['date'] = \DateTime::createFromFormat('U', filectime('/home/krdru/data/www/krd.ru'.$image['path']));
            $image['date']->setTimezone(new \DateTimeZone(date_default_timezone_get()));

            $images[] = $image;
        }

        return $images;
    }

    /**
     * Список документов(норм) на странице
     *
     * @param  integer $parentId
     * @return array
     */
    public function getDocuments($parentId)
    {
        $st = $this->em->getConnection()
            ->prepare("SELECT n.* FROM `krd__norma` n

                      JOIN `krd__relation` r
                      ON r.`object` = n.`id`

                       WHERE r.`structure` = :parent AND r.`lang` IN (0,1) AND r.`module` = 8
                       ORDER BY n.`date` ASC");
        $st->execute(array('parent' => $parentId));

        $docs = array();

        while ($doc = $st->fetch()) {
            $doc['date'] = new \DateTime($doc['date']);

            $docs[] = $doc;
        }

        return $docs;
    }

    /**
     * Получение списка открытых конкурсов
     */
    public function getCompetitions($parentId)
    {
        $st = $this->em->getConnection()
            ->prepare("SELECT * FROM `krd__competitions`

                        WHERE `parent_id` = :parent
                        ORDER BY `sort` DESC");
        $st->execute(array('parent' => $parentId));

        $list = array();

        while ($item = $st->fetch()) {
            $list[] = $item;
        }

        return $list;
    }
}
