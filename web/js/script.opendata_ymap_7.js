var searchMapParams = Backbone.Model.extend({
	defaults:{
		center: [45.034942, 38.976032],
		zoom: 13,
		fullscreen: false
	}
});

/**
 * ������������� ��� �����
 */
var searchMapPlacemarkView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;
		self.mapView = options.mapView;
		self.ymap = options.mapView.ymap;

		self.createPlacemark();

		self.on('change', function(){
			self.placemark.properties.set({place: self.model.toJSON()});
		});

		self.model.on('change:yIcon', function(){
			self.placemark.properties.set({iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>'});
		});

		self.model.on('change:yColor change:yState', function(){
			self.placemark.options.set({iconImageHref: self.getIconSrc()});
		});

		// ������� ���������� �����
		self.model.on('change:yState', function(model, yState){
			// if (window.socialMapSearch.hasOne){
			// 	return;
			// }

			if (yState == 'active'){
				self.mapView.addPlaceMarkToBuffer(self.placemark);
			}else{
				self.mapView.removePlacemark(self.placemark);
			}
		});
	},

	/**
	 * ���������� ����� ��� ������
	 */
	getIconClass: function(){
		return 'i-'+this.model.get('yIcon');
	},

	/**
	 * ���������� ������ �� ��� ����������
	 */
	getIconSrc: function(){
		var self = this;

		if (self.model.get('yState') == 'active'){
			return '/images/socialmap/placemark-bg-'+self.model.get('yColor')+'.png';
		}else{
			return '/images/socialmap/placemark-bg.png';
		}
	},

	// �������������� �������� ����� � ��������� �� �� �����
	createPlacemark: function(){
		var self = this;

		self.placemark = new ymaps.Placemark(self.model.get('yCoords'),
			{
				iconContent: '<i class="sm-map-icon '+self.getIconClass()+'"></i>',
				place: self.model.toJSON()
			},
			{
				iconLayout:'default#imageWithContent',
				iconImageHref: self.getIconSrc(),
				iconImageSize: [37, 36],
				iconImageOffset: [0, -36],

				hideIconOnBalloonOpen: true,
				balloonPane: 'movableOuters',
				balloonShadowPane: 'movableOuters',
				balloonShadow: false,
				balloonOffset: [100, 13],
				balloonMaxWidth: 196,
				balloonMinWidth: 196,
				balloonContentLayout: self.mapView.yPlacemarkTLF,

				// overlayFactory: 'default#interactiveGraphics'
			}
		);

		self.mapView.addPlaceMarkToBuffer(self.placemark);
	}
});

/**
 * ������������� �����
 */
var searchMapView = Backbone.View.extend({
	// �������� ������� ������
	regions: {},

	initialize: function(){
		var self = this;

		// ��������� ����������� �����
		self.params = new searchMapParams();

		// ��� ���������� ���������� � ����� ��������� - ������� ��������� ��� �����
		self.on('placeAdd', function(model){
			var view = new searchMapPlacemarkView({model:model, mapView:self});
		});

		// ������� ����� ��� ������������ ��������
		self.on('highlight', function(){
			self.$el.closest('.sm-map-wrap').addClass('highlighted');
			!!this.__timer && clearTimeout(this.__timer);
			this.__timer = setTimeout(function(){
				self.$el.closest('.sm-map-wrap').removeClass('highlighted');
			}, 300);
		});

        // ��������� �����
        self.on('renderMap', function() {
            self.ymap.geoObjects.add(self.clusterer);
        });

		window.___ymapsLoadCallback = function(){self.initYMap()};
		$.getScript('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU&onload=___ymapsLoadCallback');
	},

	// ���������� ������ �������������� �� ���� �����
	initFullScreen: function(){
		var self = this;

		var $button = $('<a href="#" class="full-screen-btn"></a>');
		$button.appendTo('.sm-map-wrap');

		var __onFSChange = function(){
			if (self.params.get('fullscreen')){
				$button.text('��������');
			}else{
				$button.text('����������');
			}
		};

		// ���� �� ������
		$button.on('click', function(e){
			self.params.set({fullscreen:!self.params.get('fullscreen')});
		});

		__onFSChange();

		// ��������� �������������� ������
		self.params.on('change:fullscreen', function(model, fullscreen){
			__onFSChange();

			if (fullscreen){
				$('body').addClass('fullscreen');
			}else{
				$('body').removeClass('fullscreen');
			}

			self.ymap.container.fitToViewport();
		});
	},

	/**
	 * ��������� �������
	 */
	initEvents: function(){
		var self = this;

		// ��������� ������ �����
		self.params.on('change:center', function(params, center){
			self.ymap.setCenter(center);
		});

		// ��������� ���� �����
		self.params.on('change:zoom', function(params, zoom){
			self.ymap.setZoom(zoom);
		});

		// ����������� ������� � ���� �����
		self.ymap.events.add('boundschange', function(e){
			if (e.get('oldCenter') != e.get('newCenter')){
				self.params.set({center:e.get('newCenter')}, {silent:true});
			}

			if (e.get('oldZoom') != e.get('newZoom')){
				self.params.set({zoom:e.get('newZoom')}, {silent:true});
			}
		});

		// ��� ������ ������ ������ - ���������� ���� �����

		window.socialMapSearch.searchParams.on('change:regions', function(model, regions){
			var region = regions[0];

			if (self.regions[region] === undefined){
				return;
			}

			var pol = new ymaps.Polygon([self.regions[region]]);
			self.ymap.geoObjects.add(pol);
			self.ymap.setBounds(pol.geometry.getBounds());
			self.ymap.geoObjects.remove(pol);
		});
	},

	// �������� ������ ������ ��� ����� �� ������ ����� �����
	initCloseOnEmptyClick: function(){
		var self = this;

		self.ymap.events.add('click', function(){
			if (!!self.ymap.balloon) self.ymap.balloon.close();
		});
	},

	/**
	 * ������� ������ ����������
	 */
	precachePlacemark: function(){
		var self = this;

		self.yPlacemarkTLF = ymaps.templateLayoutFactory.createClass(
			'<span class="ttl">$[properties.place.title]</span>' +
			'<div class="descr">' +
				'<div class="descr-row"><span>���������:</span> $[properties.place.subcategories_txt]</div>' +
				'<div class="descr-row"><span>�����:</span> $[properties.place.address]</div>' +
				'<div class="descr-row"><span>�������:</span> $[properties.place.phone]</div>' +
				'<a href="$[properties.place.href]" class="button">���������<i></i></a>' +
			'</div>',
			{
				build: function() {
						var layout = this;
						layout.constructor.superclass.build.call(this);
						var $this = $(this.getParentElement());
						var $body = $('<div class="sm-balloon-wrap-outer"><div class="sm-balloon-wrap-inner"></div></div>');
						var $oldBalloonBody = $this.closest('.ymaps-b-balloon');


						$oldBalloonBody.after($body);
						$this.appendTo($body.find('.sm-balloon-wrap-inner'));
						$oldBalloonBody.remove();
						setTimeout(function(){
							$body.width(236);
						}, 100);
					}
			}
		);
	},

	/**
	 * ������������� �������� �����
	 */
	initClusterer: function(){
		var self = this;

		self.clusterer = new ymaps.Clusterer({
			gridSize: 48,
			maxZoom: 16,
			clusterOpenBalloonOnClick: false,
	        clusterNumbers: [1000]
		});
	},

	/**
	 * ��������� ����� � ������, �� �� ����� ��� ����������� � ��������, ��� ��������� ��������������
	 * * ������. ���� �� �����������
	 */
	addPlaceMarkToBuffer: function(placemark){
		var self = this;

		self.clusterer.add(placemark);
	},

	/**
	 * ������� ����� � �����
	 */
	removePlacemark: function(placemark){
		var self = this;

		self.clusterer.remove(placemark);
	},

	/**
	 * ��������� ������������� �����
	 */
	initYMap: function(){
		var self = this;

		// �������� �����
		self.ymap = new ymaps.Map(self.$el.attr('id'), {
			center: self.params.get('center'),
			zoom: self.params.get('zoom'),
			behaviors: ['default', 'scrollZoom'],
			overlayFactory: ymaps.geoObject.overlayFactory.interactiveGraphics
		});

		// ���������� ���������
		self.ymap.controls
			.add('typeSelector', {top:30, right:7})
			.add('scaleLine')
			.add('mapTools')
			.add('zoomControl')
			// .add('trafficControl')
			.add(new ymaps.control.RouteEditor())
			.add(new ymaps.control.MiniMap({type: 'yandex#publicMap'}));

		self.initFullScreen();
		self.initEvents();
		self.precachePlacemark();
		self.initCloseOnEmptyClick();
		self.initClusterer();

		self.trigger('initialized');
	},

	/**
	 * ��������� ������� ������ ������ � ������
	 */
	appendRegion: function(region){
		var self = this;

		if (!$.isArray(region.polygon)){
			return;
		}

		$.each(region.polygon, function(key, val){
			$.each(val, function(k2, v2){
				val[k2] = parseFloat(v2, 10);
			});

			region.polygon[key] = val;
		});


		self.regions[region.id] = region.polygon;
	}
});
