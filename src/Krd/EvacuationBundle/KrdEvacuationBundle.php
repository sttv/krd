<?php

namespace Krd\EvacuationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Эвакуация автотранспорта
 */
class KrdEvacuationBundle extends Bundle
{
}
