<?php

namespace Krd\SubscribeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\SubscribeBundle\Entity\RSubscriber;


/**
 * Запуск интернет рассылки репортера
 */
class ReporterSendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:subscribe:send:reporter')
            ->setDescription('Send a letters to subscribers reporter.krd.ru')
            ->setHelp(<<<EOT
                      The <info>krd:subscribe:send</info> will send all delayed letters to the subscribers of reporter.krd.ru.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.reporter_entity_manager');

        $statement = $em->getConnection()->prepare("SELECT PB.`id`, PB.`title`, PB.`date`, PB.`announce`,
                CONCAT(S.`url_ru`, REPLACE(REPLACE(LOWER(PB.`title`), ' ', '_'), '-', '_'), '/') as 'url'

            FROM `reporter__publications` as PB

            INNER JOIN `reporter__structure` as S
            ON S.`id` = PB.`parent_id`

            WHERE PB.`publish` = 1 AND PB.`deleted` = 0 AND PB.`wasmailed_krd` = 0 AND PB.`parent_id` != 164

            ORDER BY PB.`date` DESC

            LIMIT 15");

        $statement->execute();
        $publications = $statement->fetchAll();

        if (empty($publications)) {
            return;
        }

        foreach($publications as &$pub){
            $pub['url'] = 'http://reporter.krd.ru'.preg_replace('/[^a-zA-Z0-9а-яА-ЯЁё\/_]/u', '', $pub['url']);
            $em->getConnection()->exec("UPDATE `reporter__publications` SET `wasmailed_krd` = 1 WHERE `id` = ".(int)$pub['id']);
        }

        //

        $subscriberRepo = $this->getManager()->getRepository('KrdSubscribeBundle:RSubscriber');
        $emails = array();

        foreach ($subscriberRepo->findBy(array('active' => true)) as $subscriber) {
            $email = $subscriber->getEmail();
            $email = trim($email);

            if (!empty($email)) {
                $emails[] = $email;
            }
        }

        if (empty($emails)) {
            return;
        }

        //

        $lastDate = new \DateTime();

        $mailer = $this->getContainer()->get('qcore.mailer');
        $message = $mailer->newMessage('Рассылка новых публикаций Городского репортера за '.$lastDate->format('d/m/Y'));
        $message->setTo($emails);
        $mailer->sendTemplate($message, 'KrdSubscribeBundle:Email:r-letter.html.twig', array('publications' => $publications, 'last_date' => $lastDate));
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
