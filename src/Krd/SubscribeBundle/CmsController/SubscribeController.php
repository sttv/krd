<?php

namespace Krd\SubscribeBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;


/**
 * Контроллер CMS - рассылки
 *
 * @PreAuthorize("hasRole('ROLE_CMS_SUBSCRIBE')")
 */
class SubscribeController extends Controller
{
    /**
     * Страница управления рассылками
     *
     * @Route("/subscribe/", name="cms_krd_subscribe_index")
     * @Template("KrdSubscribeBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {

    }
}
