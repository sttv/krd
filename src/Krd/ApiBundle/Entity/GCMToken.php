<?php

namespace Krd\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Токены GCM
 *
 * @ORM\Entity
 * @ORM\Table(name="gcm_token",
 *      indexes={
 *          @ORM\Index(name="updated", columns={"updated"})
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 */
class GCMToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     */
    private $token;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Set token
     *
     * @param string $token
     * @return GCMToken
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set updated
     *
     * @param DateTime $updated
     * @return GCMToken
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}