<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Импорт подкатегорий
 */
class ImportStructureCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:structure')
            ->setDescription('Import content pages from old site krd.ru')
            ->addArgument('source', InputArgument::REQUIRED, 'Old site absolute url')
            ->addArgument('destination', InputArgument::REQUIRED, 'New site absolute url')
            ->addOption('update', null, InputOption::VALUE_NONE, 'If set, the existing nodes will be updated')
            ->addOption('recursive', null, InputOption::VALUE_NONE, 'Import structure recursively')
            ->addOption('akuma', null, InputOption::VALUE_NONE, 'You should never know this key')
            ->setHelp(<<<EOT
                      The <info>krd:import:structure</info> imports simple page from old krd.ru site to new cms.
EOT
                    );
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('source')) {
            $source = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please enter a source URL: ',
                function($source) {
                    if (empty($source)) {
                        throw new \Exception('Can not be empty');
                    }

                    return $source;
                }
            );
            $input->setArgument('source', $source);
        }

        if (!$input->getArgument('destination')) {
            $destination = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please enter a destination URL: ',
                function($destination) {
                    if (empty($destination)) {
                        throw new \Exception('Can not be empty');
                    }

                    return $destination;
                }
            );
            $input->setArgument('destination', $destination);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sourceUrl = $input->getArgument('source');
        $destinationUrl = $input->getArgument('destination');

        $validator = Validation::createValidator();
        $assert = new Assert\Url(array('protocols' => array('http')));

        // Проверка валидности URL
        $violations = $validator->validateValue($sourceUrl, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Source url errors '.$violations);
        }

        $violations = $validator->validateValue($destinationUrl, $assert);

        if (count($violations) > 0) {
            throw new \Exception('Destination url errors '.$violations);
        }

        if (ini_get('memory_limit') != -1) {
            $output->writeln('<error>Memory limit too low</error>');
            exit;
        }

        if (!$input->getOption('akuma')) {
            $output->writeln('<error>Import not work, relax and have a tea...</error>');
            exit;
        }

        $output->writeln('<info>------------------------------ Import start at '.date('H:i:s').' ------------------------------</info>');

        if ($input->getOption('update')) {
            $output->writeln('Update mode <info>ON</info>');
        } else {
            $output->writeln('Update mode <error>OFF</error>');
        }

        if ($input->getOption('recursive')) {
            $output->writeln('Recursive mode <info>ON</info>');
        } else {
            $output->writeln('Recursive mode <error>OFF</error>');
        }

        $this->getContainer()->get('krd_import.importer.structure')->setOutput($output);

        $this->getContainer()->get('krd_import.importer.structure')->import($sourceUrl, $destinationUrl, false, $input->getOption('update'), $input->getOption('recursive'));

        $output->writeln('<info>----------------------------- Import complete at '.date('H:i:s').' ----------------------------</info>');
    }
}
