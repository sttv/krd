/**
 * Представление результата поиска
 * @type {[type]}
 */
var placeResultView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;

		self.setElement($('#sm-r-item-tpl').tmpl(self.model.toJSON()));
	}
});


/**
 * Пагинатор результатов поиска
 */
var pagesModel = Backbone.Model.extend({
	defaults: {
		onPage: 3,
		totalCount: 0,
		page: 1,

		pagesCount: 1,
		slice: [0, 0],

		extended: true,
		slideLeft: 0
	},

	initialize: function(){
		var self = this;

		self.on('change:onPage change:totalCount change:page', self.reCalc, self);
	},

	/**
	 * Пересчет параметров пагинации
	 */
	reCalc: function(){
		var self = this;

		var page = self.get('page');
		var onPage = self.get('onPage');
		var totalCount = self.get('totalCount');

		var pagesCount = Math.ceil(totalCount/onPage);

		var slice = [page*onPage - onPage, page*onPage];

		self.set({
			pagesCount: pagesCount,
			slice: slice
		});
	}
});

/**
 * Представление пагинатора
 */
var resultPaginator = Backbone.View.extend({
	maxWidth: 10*37,

	initialize: function(){
		var self = this;

		self.$list = self.$('.pages-list .float-wrap .w');
		self.$arLeft = self.$('.page-link.l');
		self.$arRight = self.$('.page-link.r');

		self.model = new pagesModel();

		// Изменение параметров пагинации - перерисовка всего пагинатора
		self.model.on('change:totalCount change:onPage', self.render, self);

		// Изменение текущей страницы - выделяем ее
		self.model.on('change:page', self.markActivePage, self);

		// Изменение текущей страницы - генерация внещнего события
		self.model.on('change:page', function(model, page){
			self.trigger('pageChanged', page, self.model.get('slice'));
		});

		// Изменение представления пагинатора
		self.model.on('change:extended', function(model, extended){
			if (extended){
				self.showExtended();
			}else{
				self.hideExtended();
			}
		});

		// Щелчки по "стрелочкам"
		self.$arLeft.on('click', function(e){
			e.preventDefault();

			self.slideLeft();
		});

		self.$arRight.on('click', function(e){
			e.preventDefault();

			self.slideRight();
		});

		// Анимация прокрутки пагинатора
		self.model.on('change:slideLeft', function(model, slideLeft){
			self.$list.stop().animate({marginLeft:slideLeft}, 100);
		});
	},

	/**
	 * Установка параметров пагинации
	 */
	setParams: function(totalCount, onPage){
		var self = this;

		self.model.set({
			totalCount: totalCount,
			onPage: onPage,
			page: 1
		});
	},

	/**
	 * Обычное представление пагинатора
	 */
	hideExtended: function(){
		var self = this;

		self.$arLeft.hide();
		self.$arRight.hide();
	},

	/**
	 * Расширенное представление пагинатора - "стрелочки побокам"
	 */
	showExtended: function(){
		var self = this;

		self.$arLeft.hide();
		self.$arRight.hide();
	},

	/**
	 * Прокрутка пагинатора влево
	 */
	slideLeft: function(){
		var self = this;

		var sl = self.model.get('slideLeft');

		if (sl < 0)
			self.model.set({slideLeft:sl + 37});
	},

	/**
	 * Прокрутка пагинатора вправо
	 */
	slideRight: function(){
		var self = this;

		var sl = self.model.get('slideLeft');
		if (sl/37 + (self.model.get('pagesCount') - Math.floor(self.maxWidth/37)) > 0)
			self.model.set({slideLeft:sl - 37});
	},

	/**
	 * Отрисовка пагинатора в соответствии с количеством элементов
	 */
	render: function(){
		var self = this;

        self.$el.show();
		self.$list.empty();

		if (self.model.get('pagesCount') <= 1){
            self.$('.pages-title, .pages-list').hide();
			return;
		}else{
			self.$('.pages-title, .pages-list').show();
		}

		for(var i = 0, count = self.model.get('pagesCount'); i < count; i++){
			var $a = $('<a class="page-link btn btn-blue" href="#">'+(i+1)+'</a>');

			(function(page){
				$a.on('click', function(e){
					e.preventDefault();

					self.model.set({page: page});
				});
			}(i+1))

			$a.appendTo(self.$list);
		}

		var width = self.model.get('pagesCount')*37;

		if (width > self.maxWidth){
			width = self.maxWidth;
			self.model.set({extended: true});
		}else{
			self.model.set({extended: false});
		}

		self.$list.parent().width(width);

		self.markActivePage();
		self.trigger('pageChanged', self.model.get('page'), self.model.get('slice'));
	},

	/**
	 * Отмечает активную страницу
	 */
	markActivePage: function(){
		var self = this;

		self.$list.find('.page-link').removeClass('page-active');
		self.$list.find('.page-link').eq(self.model.get('page')-1).addClass('page-active');
	}
});


/**
 * Список результатов поиска
 */
var resultsView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.$title = self.$el.children('.ttl');
		self.$title.data('success', self.$title.html());

		self.$list = self.$('.sm-r-list');
		self.$paginator = new resultPaginator({el:self.$('.sm-r-pages')});

		self.on('placeAdd', function(model){
			self.onPlaceAdd(model);
			self.resortResults();
		});

		// Список элементов-результатов
		self.$resultsList = $('');
		self.on('resortResults', self.resortResults, self);
		self.$paginator.on('pageChanged', function(page, slice){
			self.$resultsList.hide();
			self.$resultsList.slice(slice[0], slice[1]).show();
		});
	},

	// Добавление учреждения
	onPlaceAdd: function(model){
		var self = this;

		var view = new placeResultView({model:model});
		model._resultView = view;
		view.$el.appendTo(self.$list);
	},

	/**
	 * Скрытие неактивных элементов и назначение классов активным
	 */
	resortResults: function(){
		var self = this;

		if (window.socialMapSearch.placesLoaded !== true)
			return;

		_.each(window.socialMapSearch.places.where({yState:'inactive'}), function(place){
			place._resultView.$el.hide();
		});

		var i = 0, c = 0;;
		var $lr1 = self.$list.children('.sm-r-item:first');
		var height = 0;
		var $prev1 = null;
		var $prev2 = null;
		var $prev3 = null;
		var showOnPage = 9;

		self.$resultsList = $('');

		var activePlaces = window.socialMapSearch.places.where({yState:'active'});

		self.$list.children('.sm-r-item').removeClass('r-1 r-2 r-3 r-lr');

		_.each(activePlaces, function(place){
			self.$resultsList = self.$resultsList.add(place._resultView.$el);
			place._resultView.$el.show().css({height:'auto'});
			height = Math.max(height, place._resultView.$el.height());

			i++;
			c++;
			place._resultView.$el.addClass('r-'+i);

			if (i == 1){
				$lr1 = place._resultView.$el;
			}

			if (i == 3){
				place._resultView.$el.height(height);
				if (!!$prev1) $prev1.height(height);
				if (!!$prev2) $prev2.height(height);

				i = 0;
				$prev1 = null;
				$prev2 = null;
				$prev3 = null;
				height = 0;
			}else{
				$prev3 = $prev2;
				$prev2 = $prev1;
				$prev1 = place._resultView.$el;
			}

			if (c % (showOnPage - 2) == 0){
				// place._resultView.$el.addClass('r-lr');
				// place._resultView.$el.nextAll().slice(0,2).addClass('r-lr');
			}
		});

		if (!!$prev1) $prev1.height(height);
		if (!!$prev2) $prev2.height(height);
		if (!!$prev3) $prev3.height(height);

		var i = 1;
		self.$resultsList.each(function(){
			if (i%7 == 0 || i%8 == 0 || i%9 == 0){
				$(this).addClass('r-lr');
			}

			if (i%9 == 0){
				i = 1;
			}else{
				i++;
			}
		});

		$lr1.addClass('r-lr');
		$lr1.nextAll().addClass('r-lr');

		// Смена заголовка
		self.$title.html( self.$title.data(activePlaces.length > 0 ? 'success' : 'fail') );

		self.$paginator.setParams(activePlaces.length, showOnPage);
	},

	/**
	 * Скрывает весь блок
	 */
	instantHide: function(){
		var self = this;

		self.$el.hide();
	}
});
