/**
 * Слайдер подробного вида галереи
 */
angular.module('gallery.slider', ['horizontal.scroll'])
    .directive('gallerySlider', ['$compile', '$timeout', function($compile, $timeout) {
        return {
            restrict: 'A',
            scrope: true,

            template: function($element) {
                var tpl = '<div class="big-preview no-selection">'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                        + '<div class="loading"></div>'
                        + '<a class="icon" ng-href="">'
                            + '<img ng-src="" />'
                            + '<div class="title">Пример описания</div>'
                        + '</a>'
                    + '</div>'
                    + '<div class="preview-list no-selection">'
                        + '<div class="items-list-wrap" horizontal-scroll="gs" horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active">'
                            + '<div class="items-list clearfix">'
                                + $element.html()
                            + '</div>'
                        + '</div>'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                    + '</div>';

                return tpl;
            },

            link: function($scope, $element) {
                $scope.current = 0;
                $scope.$items = $element.find('.preview-list .items-list > .item');

                $scope.$items.each(function(i) {
                    $(this).on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.setCurrent(i);
                        });
                    });
                });

                $scope.$watch('current', function(current) {
                    if (current > $scope.$items.length - 1) {
                        $scope.current = $scope.$items.length - 1;
                        return;
                    }

                    if (current < 0) {
                        $scope.current = 0;
                        return;
                    }

                    $scope.$broadcast('setPosition', 'gs', current);
                    $scope.$items.removeClass('active');
                    $scope.$items.eq(current).addClass('active');
                });
            },

            controller: function($scope, $element) {
                $scope.next = function() {
                    $scope.current++;
                };

                $scope.prev = function() {
                    $scope.current--;
                };

                $scope.setCurrent = function(current) {
                    $scope.current = current;
                }
            }
        }
    }])
;
