<?php

namespace Krd\AppealBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * Адресат обращения
 *
 * @ORM\Entity
 * @ORM\Table(name="appeal_destination",
 *      indexes={
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="export", columns={"export"}),
 *          @ORM\Index(name="group_", columns={"group_"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="ФИО/Заголовок"}},
 *     announce={"textarea", {
 *         "label"="Описание",
 *         "required"=false
 *     }},
 *     success={"textarea", {
 *         "label"="Сообщение при успешной отправке",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     emails={"text_collection", {"label"="Email"}},
 *     group={"choice", {
 *         "label"="Группа",
 *         "required"=false,
 *         "choices"={
 *             "0"="Без группы",
 *             "1"="Заместители главы",
 *             "2"="Главы округов"
 *         }
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     export={"checkbox", {
 *         "label"="Экспортировать",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Destination
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Заголовок
     *
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     * @JMS\Groups({"user"})
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Описание
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $announce = '';

    /**
     * Сообщение при успешной отправке обращения
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $success = '';

    /**
     * Список Email для уведомлений
     *
     * @ORM\Column(type="array")
     *
     * @Gedmo\Versioned
     *
     * @JMS\Expose
     */
    private $emails = array();

    /**
     * Группа
     *
     * @ORM\Column(type="integer", name="group_")
     *
     * @Gedmo\Versioned
     *
     * @JMS\Expose
     */
    private $group;

    /**
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("group_name")
     * @JMS\Accessor(getter="getGroupName")
     */
    private $groupName;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Экспортировать во вне
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $export = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getSuccess()
    {
        return $this->success;
    }

    public function setSuccess($success)
    {
        $this->success = $success;
    }

    public function getEmails()
    {
        return $this->emails;
    }

    public function setEmails(array $emails)
    {
        $this->emails = array();

        foreach($emails as $email) {
            $this->addEmail($email);
        }
    }

    public function hasEmail($email)
    {
        return in_array(mb_strtolower($email), $this->emails, true);
    }

    public function addEmail($email)
    {
        $email = mb_strtolower($email);

        if (!empty($email) && !$this->hasEmail($email)) {
            $this->emails[] = $email;
        }
    }

    public function removeEmail($email)
    {
        if (($index = array_search($email, $this->emails, true)) !== false) {
            unset($this->emails[$index]);
            $this->emails = array_values($this->emails);
        }
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function getGroupName()
    {
        $groups = array(
            0 => 'Без группы',
            1 => 'Заместители главы',
            2 => 'Главы округов',
        );

        if (isset($groups[$this->group])) {
            return $groups[$this->group];
        } else {
            return '';
        }
    }

    public function getGroupFullName()
    {
        $groups = array(
            0 => '',
            1 => 'Заместители главы муниципального образования город Краснодар',
            2 => 'Главы внутригородских округов г. Краснодара',
        );

        if (isset($groups[$this->group])) {
            return $groups[$this->group];
        } else {
            return '';
        }
    }

    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getExport()
    {
        return $this->export;
    }

    public function setExport($export)
    {
        $this->export = (boolean)$export;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}


