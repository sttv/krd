<?php

namespace Q\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\UserBundle\Entity\UserGroup;


/**
 * Контроллер групп пользователей
 *
 * @Route("/rest/usergroup")
 * @PreAuthorize("hasRole('ROLE_CMS_ADMIN')")
 */
class RestGroupController extends Controller
{
    /**
     * Генерация имени формы
     * @param  string $prefix
     * @return string
     */
    protected function getFormName($prefix = '')
    {
        return $prefix.'quserbundle_usergroup';
    }

    /**
     * Список групп
     *
     * @Route("/", name="cms_rest_usergroup", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restListAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('g')
            ->from('QUserBundle:UserGroup', 'g');

        if ($sorting = $request->get('sorting')) {
            foreach($sorting as $field => $order) {
                $field = explode('.', $field);
                $field[0] = 'g';
                $field = implode('.', $field);

                $query->orderBy($field, $order);
            }
        }

        return Paginator::createFromRequest($request, $query->getQuery());
    }

    /**
     * Получение группы
     *
     * @Route("/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restGetAction($id)
    {
        try {
            return $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QUserBundle:UserGroup g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('User group not found');
        }
    }

    /**
     * Создание группы
     *
     * @Route("/", name="cms_rest_usergroup_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddAction(Request $request)
    {
        $group = new UserGroup('');
        $form = $this->get('form_metadata.mapper')->createFormBuilder($group, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($group);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'group' => $group);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование группы
     *
     * @Route("/{id}", name="cms_rest_usergroup_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditAction(Request $request, $id)
    {
        try {
            $group = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QUserBundle:UserGroup g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('User group not found');
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($group, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($group);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'group' => $group);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Удаление группы
     *
     * @Route("/{id}", name="cms_rest_usergroup_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function restPostDeleteAction(Request $request, $id)
    {
        try {
            $group = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QUserBundle:UserGroup g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('User group not found');
        }


        $this->getDoctrine()->getManager()->remove($group);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }

    /**
     * Рендер формы добавления
     * @Route("/form/", name="cms_rest_usergroup_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetAddForm()
    {
        $group = new UserGroup('Название группы');
        $group->addRole('ROLE_USER');

        $form = $this->get('form_metadata.mapper')->createFormBuilder($group, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->setAction($this->generateUrl('cms_rest_usergroup_create'))
            ->getForm();

        return array('form' => $form->createView());
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_usergroup_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetEditForm($id)
    {
        try{
            $group = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QUserBundle:UserGroup g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('User group not found');
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($group, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->setAction($this->generateUrl('cms_rest_usergroup_edit', array('id' => $id)))
            ->getForm();

        return array('form' => $form->createView(), 'group' => $group);
    }
}
