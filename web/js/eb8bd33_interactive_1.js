/**
 * Интерактивность сайта
 */
angular.module('interactive', [])
    /**
     * Контроллер шапки сайта
     */
    .controller('headerPanelCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.showSearch = function() {
            $element.addClass('opened');
        };

        $scope.hideSearch = function() {
            $element.removeClass('opened');
        };

        $scope.toggleSearch = function() {
            $element.toggleClass('opened');
        };

        $(window).on('resize load scroll', function() {
            if ($(this).scrollTop() > 0) {
                $element.addClass('shadow');
            } else {
                $element.removeClass('shadow');
            }
        });
    }])

    /**
     * Выпадающее верхнее меню
     */
    .directive('dropdownTopmenu', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                var $subLevel = $element.children('.sub-level');

                if ($subLevel.length == 0) {
                    return;
                }

                $element.hover(function() {
                    $subLevel.stop(true, true).addClass('is-visible').slideDown(200);
                }, function() {
                    $subLevel.stop(true, true).removeClass('is-visible').slideUp(200);
                });
            }
        };
    }])

    /**
     * Список табов
     * Очень простой. Без запоминания таба и т.п.
     */
    .directive('tabsList', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                $element.find('.pane-list > .item').each(function(index) {
                    $(this).attr({
                        'ng-click': 'setActive('+index+')',
                        'ng-class': '{active:active == '+index+'}'
                    });
                });

                var $tabs = $element.children('.tab').hide();

                // Link
                return function($scope, $element) {
                    var $activePane = $element.find('.pane-list > .item.active');
                    if ($activePane.length > 0) {
                        $scope.active = $activePane.index();
                    } else {
                        $scope.active = 0;
                    }

                    $scope.$watch('active', function(active) {
                        $tabs.hide();
                        $tabs.eq(active).show();
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.setActive = function(index) {
                    $scope.active = index;
                };
            }
        }
    }])


    /**
     * Слайдер новостей
     */
    .directive('newsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowTop = $('<div class="arrow top" ng-click="scrollUp()" ng-class="{hidden: position <= 0 || $items.length == 1}"><i></i></div>');
                var $arrowBottom = $('<div class="arrow bottom" ng-click="scrollDown()" ng-class="{hidden: position >= ($items.length - onPage + 1) || $items.length == 1}"><i></i></div>');

                var $iconsSliders = $element.find('.icon > .icons-slider');

                $element.find('.items')
                    .append($arrowTop)
                    .append($arrowBottom)
                    .find('.item').each(function(i) {
                        $(this).attr({
                            'ng-class': '{active: active == '+i+'}',
                            'ng-click': 'setActive('+i+')'
                        });
                    });

                $element.find('.items > .btn-block').attr({
                    'ng-class': '{hidden: position < $items.length - onPage + 1}'
                });

                return function($scope, $element) {
                    $scope.$itemsWrap = $element.find('.items > .wrap');
                    $scope.$itemsIcons = $element.find('.icon > .icons-slider');
                    $scope.$items = $element.find('.items > .wrap > .item');
                    $scope.$btnLink = $element.find('.items > .btn-block');
                    $scope.onPage = 3;
                    $scope.position = 0;

                    $scope.$watch('position', function(position) {
                        var $item = $scope.$items.eq(position);

                        if ($item.length != 1) {
                            return;
                        }

                        var mt = $item.position().top;

                        $scope.$itemsWrap.stop().animate({marginTop:-mt}, 300);
                    });

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$itemsIcons.eq(active);
                        $scope.$itemsIcons.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;
                $scope.position = 0;

                $scope.setActive = function(active) {
                    $scope.active = active;
                    $scope.setPosition(active - 1);
                };

                $scope.setPosition = function(position) {
                    if (position < 0) {
                        position = 0;
                    } else if (position >= ($scope.$items.length - 1)) {
                        position = $scope.$items.length - $scope.onPage;
                    }

                    $scope.position = position;
                };

                $scope.scrollUp = function() {
                    if ($scope.position > 0) {
                        $scope.position--;
                    }
                };

                $scope.scrollDown = function() {
                    if ($scope.position < ($scope.$items.length - $scope.onPage + 1)) {
                        $scope.position++;
                    }
                };
            }
        }
    }])


    /**
     * Слайдер изображений/видео новости
     */
    .directive('iconsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowPrev = $('<div class="arrow prev" ng-click="prev()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-prev"></i><i class="icon-arrow-prev-mini"></i></div>');
                var $arrowNext = $('<div class="arrow next" ng-click="next()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-next"></i><i class="icon-arrow-next-mini"></i></div>');

                $element.append($arrowPrev).append($arrowNext);

                return function($scope, $element) {
                    $scope.$items = $element.children('.item');

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$items.eq(active);
                        $scope.$items.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                }
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.next = function() {
                    if ($scope.active < $scope.$items.length - 1) {
                        $scope.active++;
                    } else {
                        $scope.active = 0;
                    }
                };

                $scope.prev = function() {
                    if ($scope.active > 0) {
                        $scope.active--;
                    } else {
                        $scope.active = $scope.$items.length - 1;
                    }
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей высоте
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiHeight', [function() {
        var heightList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiHeight:'@'
            },

            link: function($scope, $element) {
                if (heightList[$scope.multiHeight] === undefined) {
                    heightList[$scope.multiHeight] = $scope.heightList = {};
                } else {
                    $scope.heightList = heightList[$scope.multiHeight];
                }

                $scope.index = i++;

                $scope.heightList[$scope.index] = $element.height();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('heightList', function(heightList) {
                    var arr = [];

                    for(var ii in heightList) {
                        arr.push(parseInt(heightList[ii], 10));
                    }
                    var height = Math.max.apply(Math, arr);

                    if (height > 0 && !isNaN(height)) {
                        $element.height(height);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.height();
                    $element.css('height', 'auto');
                    $scope.heightList[$scope.index] = $element.height();
                    $element.height(h);
                };
            }
        }
    }])

    /**
     * Кнопка показа большего количества элементов
     */
    .directive('moreBtn', ['$compile', '$http', function($compile, $http) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element, attrs) {
                var $listBody = $element.prev('.items-list');

                var $listTemplate = $($.trim($('#'+attrs.moreBtn).html()));

                $listTemplate.attr('ng-repeat', 'item in items');

                $listBody.append($listTemplate);

                return function($scope, $element, attrs) {
                    $compile($listTemplate)($scope);

                    $scope.url = attrs.url;

                    $element.on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.nextPage();
                        });
                    });

                    $scope.$watch('loading', function(loading) {
                        if (loading) {
                            $element.html('Загрузка...');
                        } else {
                            $element.html($scope.originalHtml);
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                $scope.items = [];
                $scope.currentPage = 1;
                $scope.loading = false;
                $scope.originalHtml = $element.html();

                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if (response.page_count <= $scope.currentPage) {
                            $element.hide();
                        }

                        for(var i = 0; i < response.items.length; i++) {
                            $scope.items.push(response.items[i]);
                        }

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])
;
