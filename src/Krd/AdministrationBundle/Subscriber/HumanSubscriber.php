<?php

namespace Krd\AdministrationBundle\Subscriber;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Entity\Node;
use Krd\AdministrationBundle\Entity\Human;


/**
 * События для Human
 */
class HumanSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postLoad',
        );
    }

    /**
     * Внедряем репозиторий QCoreBundle:Node в сущности Human для ленивой загрузки ссылок
     *
     * @param  LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Human) {
            $em = $args->getEntityManager();

            $entity->setNodeRepository($em->getRepository('QCoreBundle:Node'));
        }
    }
}
