<?php

namespace Q\UserBundle\Navigation;

use Q\CoreBundle\Navigation\Item;

/**
 * Пункт меню управления пользователями
 */
class User extends Item
{
    public function getTitle()
    {
        return 'Пользователи';
    }

    public function getLink()
    {
        return $this->router->generate('cms_profile_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS_ADMIN');
    }

    public function getSubItems()
    {
        return array(
            array('url' => $this->router->generate('cms_profile_index'), 'title' => 'Управление пользователями'),
            array('url' => $this->router->generate('cms_profile_statistic').'#!/registered-list/', 'title' => 'Статистика'),
        );
    }
}
