/**
 * Горизонтальный скролл, который прокручивает по элементам
 *     horizontal-scroll - имя скролла (необязательное, используется для $scope.$broadcast('setPosition', 'gs', 1);)
 *     horizontal-scroll-items - селектор для жлементов
 *     horizontal-scroll-wrap - селектор для враппера
 *     step - числовое значение шага скрола, либо "item" для пров\мотки по элементам
 *     item-active = селектор активного элементв
 *     btn-all - Текст для кнопки Свернуть/Развернуть
 *
 * Пример вызова:
 *     <div class="items-wrap" horizontal-scroll horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active" btn-all="Смотреть все альбомы">
 *     </div>
 */
angular.module('horizontal.scroll', [])
    /**
     * Скролл
     */
    .directive('horizontalScroll', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element, attrs) {
                var itemsSelector = attrs.horizontalScrollItems;
                var wrapSelector = attrs.horizontalScrollWrap;

                var $wrap = $element.find(wrapSelector);
                var $items = $element.find(itemsSelector);

                if ($wrap.length == 0 || $items.length < 2) {
                    return;
                }

                var itemsWidth = 0;
                var itemsWidthArr = [];
                $items.each(function() {
                    itemsWidth += $(this).outerWidth(true);
                    itemsWidthArr.push(itemsWidth);
                });

                var scrollTmpl = '<div class="horizontal-scroll" ng-show="viewMode">';
                    scrollTmpl += '<div class="arrow left"></div>';
                    scrollTmpl += '<div class="horizontal-slider"></div>';
                    scrollTmpl += '<div class="arrow right"></div>';
                scrollTmpl += '</div>';

                // Кнопка "Смотреть все"
                if (!!attrs.btnAll) {
                    scrollTmpl += '<div class="hs-btn-all">';
                        scrollTmpl += '<div class="btn btn-deep-blue" ng-click="toggleView()">{{ viewMode ? btnAllText : \'Свернуть\' }}</div>';
                    scrollTmpl += '</div>';
                }

                var $scroll = $(scrollTmpl);
                var $slider = $scroll.find('.horizontal-slider');

                $element.append($scroll);
                $wrap.attr('without-wrap', 'viewMode');

                return function($scope, $element, attrs) {
                    $scope.position = 0;
                    $scope.viewMode = true;
                    $scope.btnAllText = attrs.btnAll;
                    $scope.leftOffset = 0;
                    $scope.id = attrs.horizontalScroll;

                    var updateSlider = function() {};
                    var onPositionUpdate = function() {};
                    var onOffsetUpdate = function() {};

                    switch(attrs.step) {
                        // Перемотка по элементам
                        case 'item':
                            updateSlider = function() {
                                var max = $items.length-1;

                                for(var i = 0; i <= itemsWidthArr.length-1; i++) {
                                    var left = itemsWidthArr[i];
                                    var diff = (itemsWidth - $element.width()) - left;

                                    if (diff <= 0) {
                                        max = i + 1;
                                        break;
                                    }
                                }

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $slider.slider('value', position);

                                var left = $items.eq(position).position().left;

                                if (left > (itemsWidth - $element.width())) {
                                    left = (itemsWidth - $element.width());
                                }

                                $scope.leftOffset = -left;
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 200);
                            };

                            break;

                        // Перемотка фиксированным шагом
                        default:
                            updateSlider = function() {
                                var step = !!attrs.step ? parseInt(attrs.step, 10) : 1;
                                var max = (itemsWidth - $element.width());

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    step: step,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $scope.leftOffset = -position;
                                $slider.slider('value', position);
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 13);
                            };

                            break;
                    }

                    updateSlider();

                    $(window).on('load', function() {
                        itemsWidth = 0;
                        itemsWidthArr = [];

                        $items.each(function() {
                            itemsWidth += $(this).outerWidth(true);
                            itemsWidthArr.push(itemsWidth);
                        });

                        $scope.$apply(function() {
                            updateSlider();
                        });

                        onPositionUpdate($scope.position);
                        onOffsetUpdate($scope.leftOffset);

                        if (($wrap.width() <= $element.width()) || ($scope.maxPosition <= 1)) {
                            $wrap.stop().css({marginLeft:0});
                            $scope.viewMode = false;
                        }
                    });

                    $scope.$watch('viewMode', function(viewMode) {
                        if (viewMode) {
                            $wrap.stop().css('marginLeft', $scope.leftOffset);
                        } else {
                            $wrap.stop().css('marginLeft', 0);
                        }
                    });

                    $scope.$on('setPosition', function(event, id, position) {
                        if (id == $scope.id) {
                            $scope.position = position;
                        }
                    });

                    $scope.$watch('position', onPositionUpdate);
                    $scope.$watch('position', function(position) {
                        $scope.$emit('positionUpdated', position);
                    });

                    $scope.$watch('leftOffset', onOffsetUpdate);

                    if (!!attrs.itemActive && attrs.step == 'item') {
                        var $activeItem = $items.filter(attrs.itemActive).eq(0);
                        $scope.position = $activeItem.index();
                        if ($scope.position < 0) {
                            $scope.position = 0;
                        }
                    }

                };
            },

            controller: function($scope, $element) {
                $scope.toggleView = function() {
                    $element.stop(true, true).animate({opacity:0.2}, 200, function() {
                        $scope.$apply(function() {
                            $scope.viewMode = !$scope.viewMode;
                        });
                        $element.stop(true, true).animate({opacity:1}, 200);
                    });
                }
            }
        }
    }])

    /**
     * Автоматическое подстраивание ширины родителя под дочерние элементы
     */
    .directive('withoutWrap', [function() {
        return {
            restrict: 'A',
            scope:{
                viewMode:'=withoutWrap'
            },
            link: function($scope, $element, attrs) {
                var $childrens = $element.children();
                var defPosition = $element.css('position');
                var defLeft = $element.css('left');
                var defRight = $element.css('right');

                $scope.update = function() {
                    if (!$scope.viewMode) {
                        return;
                    }

                    var width = 1;

                    $element.css({width:'auto'});

                    $childrens.each(function() {
                        width += $(this).outerWidth(true);
                    });

                    $element.width(width);
                };

                $(window).on('load resize', $scope.update);
                $scope.$on('updateWithoutWrap', $scope.update);

                $scope.$watch('viewMode', function(viewMode) {
                    $element.removeClass('ww-on ww-off');

                    if (viewMode) {
                        $element.addClass('ww-on');
                        $scope.update();
                    } else {
                        var height = $element.height();
                        $element.addClass('ww-off');
                        $element.css({width:'auto', height:'auto'});
                        var heightTo = $element.height();
                        $element.css({height:height});
                        $element.stop(true, true).animate({height:heightTo}, 200, function() {
                            $element.css({height:'auto'});
                        });
                    }
                });
            }
        }
    }])
;
