<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Общие настройки сайта
 *
 * @ORM\Entity(repositoryClass="Q\CoreBundle\Repository\ConfigRepository")
 * @ORM\Table(name="config")
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Название сайта"}},
 *     email={"email", {"label"="Email сайта<br /><small>Будет указан в поле ОТ писем с сайта</small>"}},
 *     titleemail={"text", {"label"="Имя Email сайта<br /><small>Заголовок в поле ОТ писем с сайта</small>"}},
 *     alertEmails={"text_collection", {"label"="Список Email для уведомлений с сайта"}},
 *     alert={"text", {"label"="Сообщение на всех страницах", "required"=false}},
 *     twitter={"text", {"label"="Twitter", "required"=false}},
 *     facebook={"text", {"label"="Facebook", "required"=false}},
 *     vkcom={"text", {"label"="ВКонтакте", "required"=false}},
 *     gplus={"text", {"label"="Google+", "required"=false}},
 *     odnoklassniki={"text", {"label"="Одноклассники", "required"=false}},
 *     youtube={"text", {"label"="YouTube", "required"=false}},
 *     instagram={"text", {"label"="Instagram", "required"=false}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Название проекта
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     */
    private $title = '';

    /**
     * Email проекта
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     */
    private $email = '';

    /**
     * Email ОТ
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     */
    private $titleemail = '';

    /**
     * Список email'ов для отправки уведомлений
     *
     * @ORM\Column(type="array", name="alert_emails")
     *
     * @JMS\Expose
     *
     * @Assert\Count(min=1)
     */
    private $alertEmails = array();

    /**
     * Уведомление для пользователей
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $alert;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $twitter = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $facebook = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $vkcom = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $gplus = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $odnoklassniki = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $youtube = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     */
    private $instagram = '';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getAlertEmails()
    {
        return $this->alertEmails;
    }

    public function setAlertEmails(array $alertEmails)
    {
        $this->alertEmails = array();

        foreach($alertEmails as $email) {
            $this->addAlertEmail($email);
        }
    }

    public function hasAlertEmail($email)
    {
        return in_array(mb_strtolower($email), $this->alertEmails, true);
    }

    public function addAlertEmail($email)
    {
        $email = mb_strtolower($email);

        if (!empty($email) && !$this->hasAlertEmail($email)) {
            $this->alertEmails[] = $email;
        }
    }

    public function removeAlertEmail($email)
    {
        if (($index = array_search($email, $this->alertEmails, true)) !== false) {
            unset($this->alertEmails[$index]);
            $this->alertEmails = array_values($this->alertEmails);
        }
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Config
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    public function hasTwitter()
    {
        return !empty($this->twitter);
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Config
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    public function hasFacebook()
    {
        return !empty($this->facebook);
    }

    /**
     * Set vkcom
     *
     * @param string $vkcom
     * @return Config
     */
    public function setVkcom($vkcom)
    {
        $this->vkcom = $vkcom;

        return $this;
    }

    /**
     * Get vkcom
     *
     * @return string
     */
    public function getVkcom()
    {
        return $this->vkcom;
    }

    public function hasVkcom()
    {
        return !empty($this->vkcom);
    }

    /**
     * Set gplus
     *
     * @param string $gplus
     * @return Config
     */
    public function setGplus($gplus)
    {
        $this->gplus = $gplus;

        return $this;
    }

    /**
     * Get gplus
     *
     * @return string
     */
    public function getGplus()
    {
        return $this->gplus;
    }

    public function hasGplus()
    {
        return !empty($this->gplus);
    }

    /**
     * Set odnoklassniki
     *
     * @param string $odnoklassniki
     * @return Config
     */
    public function setOdnoklassniki($odnoklassniki)
    {
        $this->odnoklassniki = $odnoklassniki;

        return $this;
    }

    /**
     * Get odnoklassniki
     *
     * @return string
     */
    public function getOdnoklassniki()
    {
        return $this->odnoklassniki;
    }

    public function hasOdnoklassniki()
    {
        return !empty($this->odnoklassniki);
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Config
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    public function hasYoutube()
    {
        return !empty($this->youtube);
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    public function hasInstagram()
    {
        return !empty($this->instagram);
    }

    /**
     * @param mixed $instagram
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Config
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Config
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Config
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Config
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Set alert
     *
     * @param string $alert
     * @return Config
     */
    public function setAlert($alert)
    {
        $this->alert = $alert;

        return $this;
    }

    /**
     * Get alert
     *
     * @return string
     */
    public function getAlert()
    {
        return $this->alert;
    }

    public function hasAlert()
    {
        return !empty($this->alert);
    }

    /**
     * Set titleemail
     *
     * @param string $titleemail
     * @return Config
     */
    public function setTitleemail($titleemail)
    {
        $this->titleemail = $titleemail;

        return $this;
    }

    /**
     * Get titleemail
     *
     * @return string
     */
    public function getTitleemail()
    {
        return $this->titleemail;
    }
}
