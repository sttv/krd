<?php

class mysql {

	protected static $_instance;
	
	private function __construct(){}
 
    private function __clone(){}

    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }
	
	function connect($db_host, $db_login, $db_passwd, $db_name) {
		mysql_connect($db_host, $db_login, $db_passwd) or die ("MySQL Error: " . mysql_error());
		mysql_query("set names utf8") or die ("<br>Invalid query: " . mysql_error());
		mysql_select_db($db_name) or die ("<br>Invalid query: " . mysql_error());
	}

	function query($query, $type='', $num=0) {
		if ($q=mysql_query($query)) {
			switch ($type) {
				case 'num_row' : return mysql_num_rows($q); break;
				case 'result' : return mysql_result($q, $num); break;
				case 'assoc' : return mysql_fetch_assoc($q); break;
				case 'none' : return $q;
				default: return $q;
			}
		} else {
			//print 'Mysql error: '.mysql_error();
			return false;
		}
	}

	function screening($data) {
		$data = trim(str_replace(array("\r\n", "\n", "\r"), ' ', $data));
		return mysql_real_escape_string($data);
	}
}

class auth {

	function check_new_user($login, $passwd, $passwd2, $mail) {
		// Проверка валидности данных
		if (empty($login) or empty($passwd) or empty($passwd2)) $error[]='Все поля обязательны для заполнения';
		if ($passwd != $passwd2) $error[]='Введенные пароли не совпадают';
		if (strlen($login)<3 or strlen($login)>30) $error[]='Длинна логина должна быть от 3 до 30 символов';
		if (strlen($passwd)<3 or strlen($passwd)>30) $error[]='Длинна пароля должна быть от 3 до 30 символов';
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $error[]='Не корректный email';
		$db = mysql::getInstance();
		$login = $db->screening($login);
		$mail = $db->screening($mail);
		if ($db->query("SELECT id_user FROM users WHERE login_user='".$login."';", 'num_row', '')!=0) $error[]='Пользователь с таким именем уже существует';
		if ($db->query("SELECT id_user FROM users WHERE mail_user='".$mail."';", 'num_row', '')!=0) $error[]='Пользователь с таким email уже существует';
		if (isset($error)) return $error;
		else return true;
	}
	
	function check_old_user($uid, $login, $passwd, $passwd2, $mail) {
		// Проверка валидности данных
		$uid = intval($uid);

		if ($passwd != $passwd2) $error[]='Введенные пароли не совпадают';
		if (strlen($login)<3 or strlen($login)>30) $error[]='Длинна логина должна быть от 3 до 30 символов';
		if (!empty($passwd)) {
			if (strlen($passwd)<3 or strlen($passwd)>30) $error[]='Длинна пароля должна быть от 3 до 30 символов';
		}
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $error[]='Не корректный email';
		$db = mysql::getInstance();
		$login = $db->screening($login);
		$mail = $db->screening($mail);
		if ($db->query("SELECT id_user FROM users WHERE id_user<>".$uid." AND login_user='".$login."';", 'num_row', '')!=0) $error[]='Пользователь с таким именем уже существует';
		if ($db->query("SELECT id_user FROM users WHERE id_user<>".$uid." AND mail_user='".$mail."';", 'num_row', '')!=0) $error[]='Пользователь с таким email уже существует';
		if ($db->query("SELECT id_user FROM users WHERE id_user=".$uid.";", 'num_row', '')==0) $error[]='Вы пытаетесь изменить несуществующего пользователя';
		if (isset($error)) return $error;
		else return true;
	}

	function reg($login, $passwd, $passwd2, $mail, $is_admin = 0) {
		if (($this->check_new_user($login, $passwd, $passwd2, $mail)) === true) {
			$db = mysql::getInstance();
			$passwd = md5($db->screening($passwd).'$SDR$%iuFh5sdaf82');
			$login = $db->screening($login);
			$mail = $db->screening($mail);
			if ($db->query("INSERT INTO `users` (`id_user`, `login_user`, `passwd_user`, `mail_user`, `is_admin`) VALUES (NULL, '".$login."', '".$passwd."', '".$mail."', ".$is_admin.");", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при регистрации нового пользователя. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_new_user($login, $passwd, $passwd2, $mail));
			return false;
		}
	}
	
	function update($uid, $login, $passwd, $passwd2, $mail, $is_admin = 0) {
		if (($this->check_old_user($uid, $login, $passwd, $passwd2, $mail)) === true) {
			$db = mysql::getInstance();
			$passwd = $db->screening($passwd);
			if (!empty($passwd)) {
				$passwd_sql = "`passwd_user`='".md5($passwd.'$SDR$%iuFh5sdaf82')."', ";
			} else {
				$passwd_sql = '';
			}
			$login = $db->screening($login);
			$mail = $db->screening($mail);
			if ($db->query("UPDATE `users` SET `login_user`='".$login."', ".$passwd_sql."`mail_user`='".$mail."', `is_admin`=".$is_admin." WHERE `id_user`=".intval($uid).";", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при изменении пользователя. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_old_user($uid, $login, $passwd, $passwd2, $mail));
			return false;
		}
	}

	function check() {
		if (isset($_SESSION['id_user']) and isset($_SESSION['login_user'])) return true;
		else {
			if (isset($_COOKIE['id_user']) and isset($_COOKIE['code_user'])) {
				$db = mysql::getInstance();
				$id_user=$db->screening($_COOKIE['id_user']);
				$code_user=$db->screening($_COOKIE['code_user']);
				if ($db->query("SELECT * FROM `session` WHERE `id_user`=".$id_user.";", 'num_row', '')==1) {
					// Есть запись в таблице сессий, сверяем данные
					$data = $db->query("SELECT * FROM `session` WHERE `id_user`=".$id_user.";", 'assoc', '');
					if ($data['code_sess']==$code_user and $data['user_agent_sess']==$_SERVER['HTTP_USER_AGENT']) {
						$usr_tmp = $db->query("SELECT login_user, is_admin FROM `users` WHERE  `id_user` = '".$id_user."';", 'assoc', '');
						$_SESSION['id_user']=$id_user;
						$_SESSION['login_user']=$usr_tmp['login_user'];
						$_SESSION['admin_user']=intval($usr_tmp['is_admin']);
						setcookie("id_user", $_SESSION['id_user'], time()+3600*24*14);
						setcookie("code_user", $code_user, time()+3600*24*14);
						return true;
					} else return false; // данные в таблице сессий не совпадают с куками
				} else return false; // в таблице сессий не найден такой пользователь
			} else return false;
		}
	}

	function authorization() {
		if (empty($_POST['login']) or empty($_POST['passwd'])) {
			$error[]='Все поля обязательны для заполнения';
			$_SESSION['error'] = $this->error_print($error);
			return false;
		} else {
			$db = mysql::getInstance();
			$login = $db->screening($_POST['login']);
			$passwd = md5($db->screening($_POST['passwd']).'$SDR$%iuFh5sdaf82');
			if ($db->query("SELECT * FROM `users` WHERE  `login_user` =  '".$login."' AND  `passwd_user` = '".$passwd."';", 'num_row', '')==1) {
				$usr_tmp=$db->query("SELECT id_user, is_admin FROM `users` WHERE  `login_user` =  '".$login."' AND  `passwd_user` = '".$passwd."';", 'assoc', '');
				$_SESSION['id_user'] = $usr_tmp['id_user'];
				$_SESSION['login_user']=$login;
				$_SESSION['admin_user']=intval($usr_tmp['is_admin']);
				$r_code = $this->generateCode(15);
				if ($db->query("SELECT * FROM `session` WHERE `id_user`=".$_SESSION['id_user'].";", 'num_row', '')==1) {
					$db->query("UPDATE `session` SET `code_sess` = '".$r_code."', `user_agent_sess` = '".$_SERVER['HTTP_USER_AGENT']."' WHERE `id_user` = ".$_SESSION['id_user'].";", '', '');
				} else {
					$db->query("INSERT INTO `session` (`id_user`, `code_sess`, `user_agent_sess`) VALUES ('".$_SESSION['id_user']."', '".$r_code."', '".$_SERVER['HTTP_USER_AGENT']."');", '', '');
				}
				setcookie("id_user", $_SESSION['id_user'], time()+3600*24*14);
				setcookie("code_user", $r_code, time()+3600*24*14);
				return true;
			} else {
				if ($db->query("SELECT * FROM  `users` WHERE  `login_user` =  '".$login."';", 'num_row', 0)==1) $error[]='Введен не верный пароль';
				else $error[]='Такой пользователь не существует';
				$_SESSION['error'] = $this->error_print($error);
				return false;
			}
		}
	}

	function exit_user() {
		session_destroy();
		setcookie("id_user", '', time()-3600);
		setcookie("code_user", '', time()-3600);
		header("Location: index.php");
	}

	function recovery_pass($login, $mail) {
		if (empty($login) or empty($mail)) {
			$error[]='Все поля обязательны для заполнения';
			return $this->error_print($error);
		} else {
			$db = mysql::getInstance();
			$login = $db->screening($login);
			$db_inf = $db->query("SELECT * FROM `users` WHERE `login_user`='".$login."';", 'assoc', '');
			if ($db->query("SELECT * FROM `users` WHERE `login_user`='".$login."';", 'num_row', '')!=1) {
				$error[]='Пользователь с таким именем не найден';
				return $this->error_print($error);
			} else {
				if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $error[]='Введен не корректный email';
				if ($mail != $db_inf['mail_user']) $error[]='Введенный email не соответствует введенному при регистрации ';
				if (!isset($error)) {
					$new_passwd = $this->generateCode(8);
					$new_passwd_sql = md5($new_passwd.'$SDR$%iuFh5sdaf82');
					$message = "Вы запросили восстановление пароля на сайте %sitename% для учетной записи ".$db_inf['login_user']." \nВаш новый пароль: ".$new_passwd."\n\n С уважением администрация сайта %sitename%.";
					if (mail($mail, "Восстановление пароля", $message, "From: webmaster@sitename.ru\r\n"."Reply-To: webmaster@sitename.ru\r\n"."X-Mailer: PHP/" . phpversion())) {
						$db->query("UPDATE `users` SET `passwd_user`='".$new_passwd_sql."' WHERE `id_user` = ".$db_inf['id_user'].";", '', '');
						return true;
					} else {
						$error[]='В данный момент восстановление пароля не возможно, свяжитесь с администрацией сайта';
						return $this->error_print($error);
					}
				} else return $this->error_print($error);
			}
		}
	}

	function generateCode($length) { 
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789"; 
		$code = ""; 
		$clen = strlen($chars) - 1;   
		while (strlen($code) < $length) { 
			$code .= $chars[mt_rand(0,$clen)];   
		} 
		return $code; 
	}

	//function error_print($error) {
	//	$r='<ul>';
	//	foreach($error as $key=>$value) {
	//		$r.='<li>'.$value.'</li>';
	//	}
	//	return $r.'</ul>';
	//}
	
	function error_print($error) {
		$r='';
		foreach($error as $key=>$value) {
			$r.='<label class="text-error">'.$value.'</label>';
		}
		return $r;
	}
	
}

class nodes {

	function check_new_node($idmap, $address) {
		// Проверка валидности данных
		if (empty($idmap) || empty($address)) $error[]='Все поля обязательны для заполнения';
		$db = mysql::getInstance();
		$idmap = intval($idmap);
		$address = $db->screening($address);
		if ($db->query("SELECT id FROM nodes WHERE id_map='".$idmap."';", 'num_row', '')!=0) $error[]='Объект с таким идентификатором карты уже существует';
		if ($db->query("SELECT id FROM nodes WHERE address='".$address."';", 'num_row', '')!=0) $error[]='Объект с таким адресом уже существует';
		if (isset($error)) return $error;
		else return true;
	}
	
	function check_old_node($idnode, $idmap, $address) {
		// Проверка валидности данных
		$idnode = intval($idnode);
		if (empty($idmap) || empty($address)) $error[]='Все поля обязательны для заполнения';
		$db = mysql::getInstance();
		$idmap = intval($idmap);
		$address = $db->screening($address);
		if ($db->query("SELECT id FROM nodes WHERE id<>".$idnode." AND id_map='".$idmap."';", 'num_row', '')!=0) $error[]='Объект с таким идентификатором карты уже существует';
		if ($db->query("SELECT id FROM nodes WHERE id<>".$idnode." AND address='".$address."';", 'num_row', '')!=0) $error[]='Объект с таким адресом уже существует';
		if ($db->query("SELECT id FROM nodes WHERE id=".$idnode.";", 'num_row', '')==0) $error[]='Вы пытаетесь изменить несуществующий объект';
		if (isset($error)) return $error;
		else return true;
	}

	function add($idmap, $address) {
		if (($this->check_new_node($idmap, $address)) === true) {
			$db = mysql::getInstance();
			$idmap = intval($idmap);
			$address = $db->screening($address);
			if ($db->query("INSERT INTO `nodes` (`id`, `id_map`, `address`, `title`) VALUES (NULL, '".$idmap."', '".$address."', '');", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при создании нового объекта. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_new_node($idmap, $address));
			return false;
		}
	}
	
	function update($idnode, $idmap, $address) {
		if (($this->check_old_node($idnode, $idmap, $address)) === true) {
			$db = mysql::getInstance();
			$idmap = intval($idmap);
			$address = $db->screening($address);
			if ($db->query("UPDATE `nodes` SET `id_map`='".$idmap."', `address`='".$address."' WHERE `id`=".intval($idnode).";", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при изменении объекта. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_old_node($idnode, $idmap, $address));
			return false;
		}
	}

	function get_node($idnode = 0) {
		$idnode = intval($idnode);
		$db = mysql::getInstance();
		if ($idnode > 0) {
			$data = $db->query("SELECT id_map, address FROM `nodes` WHERE `id`='".$idnode."';", 'assoc', '');
			$data['address'] = stripslashes($data['address']);
		} else {
			$data = array('id_map' => '', 'address' => '');
		}
		return $data;
	}
	
	function list_nodes() {
		$nodesList = array();
		$db = mysql::getInstance();
		$query = $db->query("SELECT * FROM nodes ORDER BY id ASC");
		while ($row = mysql_fetch_assoc($query)) {
			$row['title'] = stripcslashes($row['title']);
			$row['address'] = stripcslashes($row['address']);
			$nodesList[$row['id']] = $row;
		}
		return $nodesList;
	}
	
	//function list_nodes($select_name = 'node', $idnode = 0) {
	//	$idnode = intval($idnode);
	//	$nodesList = '<select name="'.$select_name.'">';
	//	$nodesList .= '<option value="0"'.($idnode==0?' selected="selected"':'').'>-</option>';
	//	$db = mysql::getInstance();
	//	$query = $db->query("SELECT id, address FROM nodes ORDER BY id ASC");
	//	while ($row = mysql_fetch_assoc($query)) {
	//		$nodesList .= '<option value="'.$row['id'].'"'.($idnode==intval($row['id'])?' selected="selected"':'').'>'.$row['address'].'</option>';
	//	}
	//	$nodesList .= '</select>';
	//	return $nodesList;
	//}
	
	function error_print($error) {
		$r='';
		foreach($error as $key=>$value) {
			$r.='<label class="text-error">'.$value.'</label>';
		}
		return $r;
	}
}

class fields {

	function check_new_field($title) {
		// Проверка валидности данных
		if (empty($title)) $error[]='Название поля обязательно для заполнения';
		$db = mysql::getInstance();
		$title = $db->screening($title);
		if ($db->query("SELECT id FROM fields WHERE title='".$title."';", 'num_row', '')!=0) $error[]='Поле с таким названием уже существует';
		if (isset($error)) return $error;
		else return true;
	}
	
	function check_old_field($idfield, $title) {
		// Проверка валидности данных
		$idfield = intval($idfield);
		if (empty($title)) $error[]='Название поля обязательно для заполнения';
		$db = mysql::getInstance();
		$title = $db->screening($title);
		if ($db->query("SELECT id FROM fields WHERE id<>".$idfield." AND title='".$title."';", 'num_row', '')!=0) $error[]='Поле с таким названием уже существует';
		if ($db->query("SELECT id FROM fields WHERE id=".$idfield.";", 'num_row', '')==0) $error[]='Вы пытаетесь изменить несуществующее поле';
		if (isset($error)) return $error;
		else return true;
	}

	function add($title) {
		if (($this->check_new_field($title)) === true) {
			$db = mysql::getInstance();
			$title = $db->screening($title);
			if ($db->query("INSERT INTO `fields` (`id`, `title`) VALUES (NULL, '".$title."');", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при создании нового поля для объектов. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_new_field($title));
			return false;
		}
	}
	
	function update($idfield, $title) {
		if (($this->check_old_field($idfield, $title)) === true) {
			$db = mysql::getInstance();
			$idfield = intval($idfield);
			$title = $db->screening($title);
			if ($db->query("UPDATE `fields` SET `title`='".$title."' WHERE `id`=".$idfield.";", '', '')) return true;
			else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при изменении поля для объектов. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_old_field($idfield, $title));
			return false;
		}
	}

	function get_field($idfield = 0) {
		$idfield = intval($idfield);
		$db = mysql::getInstance();
		if ($idfield > 0) {
			$data = $db->query("SELECT title FROM `fields` WHERE `id`='".$idfield."';", 'assoc', '');
			$data['title'] = stripslashes($data['title']);
		} else {
			$data = array('title' => '');
		}
		return $data;
	}
	
	function delete($idfield = 0) {
		$idfield = intval($idfield);
		$db = mysql::getInstance();
		if ($idfield > 0) {
			$row_count = $db->query("SELECT `id` FROM `item_fields` WHERE `field_id`=".$idfield.";", 'num_row');
			if ($row_count != 0) $error[]='У '.$row_count.' обектов существует это поле, удаление не возможно.';
			if (isset($error)) {
				$_SESSION['error'] = $this->error_print($error);
				return false;
			} else {
				$db->query("DELETE FROM `fields` WHERE `id`=".$idfield);
				return true;
			}
		} else {
			return false;
		}
	}
	
	function list_fields() {
		$fieldsList = array(0 => '-');
		$db = mysql::getInstance();
		$query = $db->query("SELECT id, title FROM fields ORDER BY id ASC");
		while ($row = mysql_fetch_assoc($query)) {
			$fieldsList[$row['id']] = stripslashes($row['title']);
		}
		return $fieldsList;
	}
	
	function select_fields($arrayField, $select_name = 'field', $select_id = 'field', $idfield = 0) {
		$idfield = intval($idfield);
		$fieldList = '<select name="'.$select_name.'" id="'.$select_id.'">';
		foreach ($arrayField as $id => $title) {
			$fieldList .= '<option value="'.$id.'"'.($idfield==$id?' selected="selected"':'').'>'.$title.'</option>';
		}
		$fieldList .= '</select>';
		return $fieldList;
	}
	
	function error_print($error) {
		$r='';
		foreach($error as $key=>$value) {
			$r.='<label class="text-error">'.$value.'</label>';
		}
		return $r;
	}
}

class items {
	
	function get($idnode = 0) {
		$idnode = intval($idnode);
		$nodeObj = array();
		if ($idnode > 0) {
			$db = mysql::getInstance();
			$node = $db->query("SELECT `id`, `address`, `title`, `status_obj`, `img_preview`, `img_detail` FROM `nodes` WHERE `id`='".$idnode."';", 'assoc');
			if (intval($node['id']) > 0) {
				$query = $db->query("SELECT * FROM `item_fields` WHERE `node_id`='".intval($node['id'])."' ORDER BY `order` ASC");
				$fields = array();
				while ($row = mysql_fetch_assoc($query)) {
					$fields[] = $row;
				}
				$nodeObj['node'] = $node;
				$nodeObj['fields'] = $fields;
				return $nodeObj;
			} else {
				$_SESSION['error'] = $this->error_print(array('Такого объекта не существует'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print(array('Такого объекта не существует'));
			return false;
		}
	}
	
	function check_items($nodeid, $title) {
		// Проверка валидности данных
		//if (empty($title)) $error[]='Название поля обязательно для заполнения';
		$db = mysql::getInstance();
		$nodeid = intval($nodeid);
		if ($db->query("SELECT `id` FROM `nodes` WHERE `id`='".$nodeid."';", 'num_row', '')==0) $error[]='Вы пытаетесь изменить несуществующий объект';
		if (isset($error)) return $error;
		else return true;
	}
	
	function update($idnode, $title, $arrFields, $imgfile, $imgdel = 0, $status_obj = 0) {
		if (($this->check_items($idnode, $title)) === true) {
			$db = mysql::getInstance();
			$idnode = intval($idnode);
			$status_obj = intval($status_obj);
			$imgdel = intval($imgdel);
			$title = $db->screening($title);
			// delete or save image
			if ($imgdel == 1) {
				$images = $db->query("SELECT `img_preview`, `img_detail` FROM `nodes` WHERE `id`='".$idnode."' LIMIT 1", "assoc");
				$imgsql = "";
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$images['img_preview'])) {
					if (unlink($_SERVER['DOCUMENT_ROOT'].$images['img_preview']))
						$imgsql .= ", `img_preview`=''";
				}
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$images['img_detail'])) {
					if (unlink($_SERVER['DOCUMENT_ROOT'].$images['img_detail']))
						$imgsql .= ", `img_detail`=''";
				}
			} else {
				if ($imgfile['files']['error'] == UPLOAD_ERR_OK) {
					umask(000);
					$ext = strrchr($imgfile['files']['name'], ".");
					$thumbnail_prev = $idnode.time()."_sm".$ext;
					$thumbnail = $idnode.time().$ext;
					if(@move_uploaded_file($imgfile['files']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['full']['src'].$thumbnail)){
						chmod($_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['full']['src'].$thumbnail, 0644);
						
						$resizeObj = new resize($_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['full']['src'].$thumbnail);
						$resizeObj -> resizeImage($imgfile['settings']['full']['width'], $imgfile['settings']['full']['height']);
						$resizeObj -> saveImage($_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['full']['src'].$thumbnail, 100);
						
						$resizeObj = new resize($_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['full']['src'].$thumbnail);
						$resizeObj -> resizeImage($imgfile['settings']['preview']['width'], $imgfile['settings']['preview']['height']);
						$resizeObj -> saveImage($_SERVER['DOCUMENT_ROOT'].$imgfile['settings']['preview']['src'].$thumbnail_prev, 100);
						
						$imgsql = ", `img_preview`='".$imgfile['settings']['preview']['src'].$thumbnail_prev."', `img_detail`='".$imgfile['settings']['full']['src'].$thumbnail."'";
					} else {
						$imgsql = "";
					}
				} else {
					$imgsql = "";
				}
			}
			
			if ($db->query("UPDATE `nodes` SET `title`='".$title."', `status_obj`='".$status_obj."'".$imgsql." WHERE `id`=".$idnode.";")){
				if (count($arrFields['del']) > 0) {
					$db->query("DELETE FROM `item_fields` WHERE `node_id`=".$idnode." AND `id` IN (".implode(',', $arrFields['del']).")");
				}
				if (count($arrFields['old']) > 0) {
					foreach ($arrFields['old'] as $id => $val) {
						$db->query("UPDATE `item_fields` SET `field_id`=".$val['field_id'].", `body`='".$db->screening($val['body'])."', `order`=".intval($val['order'])." WHERE `id`=".$id." AND `node_id`=".$idnode);
					}
				}
				if (count($arrFields['new']) > 0) {
					foreach ($arrFields['new'] as $val) {
						$db->query("INSERT INTO `item_fields` VALUES (NULL, ".$idnode.", ".$val['field_id'].", '".$db->screening($val['body'])."', ".intval($val['order']).");");
					}
				}
				return true;
			} else {
				$_SESSION['error'] = $this->error_print(array('Возникла ошибка при изменении объекта. Свяжитесь с администрацией'));
				return false;
			}
		} else {
			$_SESSION['error'] = $this->error_print($this->check_items($idnode, $title));
			return false;
		}
	}
	
	function get_all($order = 'ASC') {
		$db = mysql::getInstance();
		$order = ($order === 'ASC') ? 'ASC' : 'DESC';
		$allNodes = array();
		$query = $db->query("SELECT `id`, `id_map`, `title`, `address`, `status_obj`, `img_preview`, `img_detail` FROM `nodes` ORDER BY `id_map` ASC");
		while($node = mysql_fetch_assoc($query)) {
			$items = array();
			$qw = $db->query("SELECT f.title as name, i.body FROM `item_fields` i LEFT JOIN `fields` f ON f.`id`=i.`field_id` WHERE i.`node_id` = ".intval($node['id'])." ORDER BY i.`order` ".$order);
			while($item = mysql_fetch_assoc($qw)) {
				$item['name'] = stripslashes($item['name']);
				$item['body'] = stripslashes($item['body']);
				$items[] = $item;
			}
			$allNodes[$node['id_map']] = array(
				'title' => stripslashes($node['title']),
				'address' => stripslashes($node['address']),
				'status' => intval($node['status_obj']),
				'images' => array(
					'preview' => ($node['img_preview'] ? 'http://'.$_SERVER['HTTP_HOST'].stripslashes($node['img_preview']) : ''),
					'full' => ($node['img_detail'] ? 'http://'.$_SERVER['HTTP_HOST'].stripslashes($node['img_detail']) : '')
				),
				'fields' => $items
			);
		}
		return $allNodes;
	}
	
	function select_status($arrayStatus, $select_name = 'status_obj', $select_id = 'status_obj', $idoption = 0) {
		$idoption = intval($idoption);
		$optionList = '<select name="'.$select_name.'" id="'.$select_id.'">';
		foreach ($arrayStatus as $id => $title) {
			$optionList .= '<option value="'.$id.'"'.($idoption==$id?' selected="selected"':'').'>'.$title.'</option>';
		}
		$optionList .= '</select>';
		return $optionList;
	}
	
	function error_print($error) {
		$r='';
		foreach($error as $key=>$value) {
			$r.='<label class="text-error">'.$value.'</label>';
		}
		return $r;
	}
}

class resize
{
	// *** Class variables
	private $image;
	private $width;
	private $height;
	private $imageResized;

	function __construct($fileName)
	{
		// *** Open up the file
		$this->image = $this->openImage($fileName);
		// *** Get width and height
		$this->width  = imagesx($this->image);
		$this->height = imagesy($this->image);
	}

	private function openImage($file)
	{
		// *** Get extension
		if (!function_exists('exif_imagetype')){
			$extension = strtolower(strrchr($file, '.'));
			switch($extension)
			{
				case '.jpg':
				case '.jpeg':
					$img = @imagecreatefromjpeg($file);
					break;
				case '.gif':
					$img = @imagecreatefromgif($file);
					break;
				case '.png':
					$img = @imagecreatefrompng($file);
					break;
				default:
					$img = false;
					break;
			}
		} else {
			switch(exif_imagetype($file))
			{
				case IMAGETYPE_GIF:
					$img = @imagecreatefromgif($file);
					break;
				case IMAGETYPE_JPEG:
					$img = @imagecreatefromjpeg($file);
					break;
				case IMAGETYPE_PNG:
					$img = @imagecreatefrompng($file);
					break;
				default:
					$img = false;
					break;
			}
		}
		return $img;
	}

	public function resizeImage($newWidth, $newHeight, $option="auto")
	{
		// *** Get optimal width and height - based on $option
		$optionArray = $this->getDimensions($newWidth, $newHeight, $option);
		$optimalWidth  = $optionArray['optimalWidth'];
		$optimalHeight = $optionArray['optimalHeight'];
		// *** Resample - create image canvas of x, y size
		$this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
		imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
		// *** if option is 'crop', then crop too
		if ($option == 'crop') {
			$this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
		}
	}

	public function getWH(){
		return array($this->width, $this->height);
	}
	
	private function getDimensions($newWidth, $newHeight, $option)
	{

	   switch ($option)
		{
			case 'exact':
				$optimalWidth = $newWidth;
				$optimalHeight= $newHeight;
				break;
			case 'portrait':
				$optimalWidth = $this->getSizeByFixedHeight($newHeight);
				$optimalHeight= $newHeight;
				break;
			case 'landscape':
				$optimalWidth = $newWidth;
				$optimalHeight= $this->getSizeByFixedWidth($newWidth);
				break;
			case 'auto':
				$optionArray = $this->getSizeByAuto($newWidth, $newHeight);
				$optimalWidth = $optionArray['optimalWidth'];
				$optimalHeight = $optionArray['optimalHeight'];
				break;
			case 'crop':
				$optionArray = $this->getOptimalCrop($newWidth, $newHeight);
				$optimalWidth = $optionArray['optimalWidth'];
				$optimalHeight = $optionArray['optimalHeight'];
				break;
		}
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}

	private function getSizeByFixedHeight($newHeight)
	{
		$ratio = $this->width / $this->height;
		$newWidth = $newHeight * $ratio;
		return $newWidth;
	}

	private function getSizeByFixedWidth($newWidth)
	{
		$ratio = $this->height / $this->width;
		$newHeight = $newWidth * $ratio;
		return $newHeight;
	}

	private function getSizeByAuto($newWidth, $newHeight)
	{
		if ($this->height < $this->width)
		// *** Image to be resized is wider (landscape)
		{
			$optimalWidth = $newWidth;
			$optimalHeight= $this->getSizeByFixedWidth($newWidth);
		}
		elseif ($this->height > $this->width)
		// *** Image to be resized is taller (portrait)
		{
			$optimalWidth = $this->getSizeByFixedHeight($newHeight);
			$optimalHeight= $newHeight;
		}
		else
		// *** Image to be resizerd is a square
		{
			if ($newHeight < $newWidth) {
				$optimalWidth = $newWidth;
				$optimalHeight= $this->getSizeByFixedWidth($newWidth);
			} else if ($newHeight > $newWidth) {
				$optimalWidth = $this->getSizeByFixedHeight($newHeight);
				$optimalHeight= $newHeight;
			} else {
				// *** Sqaure being resized to a square
				$optimalWidth = $newWidth;
				$optimalHeight= $newHeight;
			}
		}
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}

	private function getOptimalCrop($newWidth, $newHeight)
	{
		$heightRatio = $this->height / $newHeight;
		$widthRatio  = $this->width /  $newWidth;
		if ($heightRatio < $widthRatio) {
			$optimalRatio = $heightRatio;
		} else {
			$optimalRatio = $widthRatio;
		}
		$optimalHeight = $this->height / $optimalRatio;
		$optimalWidth  = $this->width  / $optimalRatio;
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}

	private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
	{
		// *** Find center - this will be used for the crop
		$cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
		$cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );
		$crop = $this->imageResized;
		//imagedestroy($this->imageResized);
		
		// *** Now crop from center to exact requested size
		$this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
		imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);
	}

	public function saveImage($savePath, $imageQuality="100")
	{
		// *** Get extension
		$extension = strrchr($savePath, '.');
		$extension = strtolower($extension);
		switch($extension)
		{
			case '.jpg':
			case '.jpeg':
				if (imagetypes() & IMG_JPG) {
					imagejpeg($this->imageResized, $savePath, $imageQuality);
				}
				break;
			case '.gif':
				if (imagetypes() & IMG_GIF) {
					imagegif($this->imageResized, $savePath);
				}
				break;
			case '.png':
				// *** Scale quality from 0-100 to 0-9
				$scaleQuality = round(($imageQuality/100) * 9);
				// *** Invert quality setting as 0 is best, not 9
				$invertScaleQuality = 9 - $scaleQuality;
				if (imagetypes() & IMG_PNG) {
					 imagepng($this->imageResized, $savePath, $invertScaleQuality);
				}
				break;
			// ... etc
			default:
				// *** No extension - No save.
				break;
		}
		imagedestroy($this->imageResized);
	}
}