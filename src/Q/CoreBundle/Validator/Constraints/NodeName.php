<?php

namespace Q\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Валидатор системного имени нода
 *
 * @Annotation
 */
class NodeName extends Constraint
{
    public $message = 'Системное имя может состоять из: a-z 0-9 - _';
    public $messageIndex = 'Главная страница не может содержать системное имя';
    public $messageUnique = 'Подраздел с таким системным именем существует';

    public function validatedBy()
    {
        return 'node_name';
    }
}
