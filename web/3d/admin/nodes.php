<?
include_once 'conf.php';
$page = 3;

$auth = new auth();
if (!$auth->check() || $_SESSION['admin_user'] != 1){
	header("Location: index.php");
    exit;
}

$db = mysql::getInstance();
$node = new nodes();
switch($_REQUEST['action']) {
    case 'add':
		if (isset($_REQUEST['send'])) {
			if ($node->add($_REQUEST['idmap'], $_REQUEST['addr'])) {
				header('Location: nodes.php');
				exit;
			}
		}
		$addact = 1;
		$titleForm = "Новый объект";
    case 'edit':
        if (!isset($addact)) {
			$titleForm = "Изменить объект";
            $nid = intval($_REQUEST['nid']);
            if (isset($_REQUEST['send']) && $nid > 0) {
                if ($node->update($nid, $_REQUEST['idmap'], $_REQUEST['addr'])) {
                    header('Location: nodes.php');
                    exit;
                }
            } else {
                $data = $node->get_node($nid);
                $_REQUEST['idmap'] = $data['id_map'];
				$_REQUEST['addr'] = $data['address'];
            }
        }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1><?=$titleForm?></h1>
					<hr>
					<form method="post">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<div class="control-group">
							<label for="idmap">ID map:</label>
							<input id="idmap" type="text" name="idmap" value="<?=@$_REQUEST['idmap']?>" class="span6">
						</div>
						<div class="control-group">
							<label for="addr">Адрес:</label>
							<input id="addr" type="text" name="addr" value="<?=@$_REQUEST['addr']?>" class="span6">
						</div>
						<div class="control-group">
							<input type="hidden" name="action" value="<?=$_REQUEST['action']?>" />
							<input type="hidden" name="nid" value="<?=$nid?>" />
							<input type="hidden" name="send" value="1" />
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary input-large">Сохранить</button>&nbsp;&nbsp;<a href="nodes.php" class="btn">Отмена</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
        <?
        break;
    case 'delete':
        if (!empty($_REQUEST['nodeid']) && count($_REQUEST['nodeid'])>0) {
            $nodeids = implode(',', $_REQUEST['nodeid']);
            $db->query("DELETE FROM nodes WHERE id IN ({$nodeids})");
			$db->query("DELETE FROM item_fields WHERE node_id IN ({$nodeids})");
            header('Location: nodes.php');
            exit;
        } else {
            header('Location: nodes.php');
            exit;
        }
        break;
    case 'ndel':
        if (isset($_REQUEST['nid'])){
            $nid = intval($_REQUEST['nid']);
            $db->query("DELETE FROM nodes WHERE id={$nid} LIMIT 1");
			$db->query("DELETE FROM item_fields WHERE node_id={$nid} LIMIT 1");
            header('Location: nodes.php');
            exit;
        }
        break;
    default:
		$nodes = $node->list_nodes();
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
			.table th.text-center, td.text-center {
				text-align: center;   
			}
        </style>
		<script type="text/javascript"  language="javascript">
            function toggle(source, name) {
                checkboxes = document.getElementsByName(name);
                for(var i=0, n=checkboxes.length;i<n;i++) {
                  checkboxes[i].checked = source.checked;
                }
            }
        </script>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Объекты</h1>
					<ul class="nav nav-pills">
						<li><a href="nodes.php?action=add">Новый объект</a></li>
					</ul>
					<hr>
						<form action="" method="post">
							<input type="hidden" name="action" value="delete" />
							<table class="table table-striped table-bordered table-hover">
								<tbody>
									<tr>
										<th class="text-center">
												<label for="chdel">Выбрать всё</label>
												<input id="chdel" type="checkbox" onClick="toggle(this, 'nodeid[]')" />
										</th>
										<th>ID map</th>
										<th>Адрес</th>
										<th>Действие</th>
									</tr>
									<?
									if (count($nodes) > 0) {
										foreach($nodes as $nid => $nodeval){
											echo '<tr>';
											echo '<td class="text-center"><input type="checkbox" name="nodeid[]" value="'.$nid.'" /></td>';
											echo '<td>'.$nodeval['id_map'].'</td>';
											echo '<td>'.$nodeval['address'].'</td>';
											echo '<td><a href="nodes.php?action=edit&nid='.$nid.'"><i class="icon-pencil"></i>&nbsp;Edit</a>&nbsp;&nbsp;&nbsp;<a href="nodes.php?action=ndel&nid='.$nid.'"><i class="icon-trash"></i>&nbsp;Delete</a></td>';
											echo '</tr>';
										}
										echo '<tr><td colspan="4"><button type="submit" name="send" class="btn btn-primary">Удалить выбранные объекты</button></td></tr>';
									} else {
										echo '<tr><td colspan="4">Нет объектов</td></tr>';
									}
									?>
								</tbody>
							</table>
						</form>
				</div>
			</div>
		</div>
    </body>
</html>
        <?
        break;
}