/**
 * �����������
 */
var starsView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.id = self.$el.data('id');
		self.rate = self.$el.data('rate');

		self.appendStars();

		if (self.$el.hasClass('disabled')){
			self.disable();
		}

		self.$stars.hover(function(){
			if (!self.disabled){
				$(this).prevAll().andSelf().addClass('hovered').removeClass('nothovered');
				$(this).nextAll().removeClass('hovered').addClass('nothovered');
			}
		});

	},

	/**
	 * ��������� ����������� �����������
	 */
	disable: function(){
		var self = this;

		self.disabled = true;
		self.$stars.removeClass('hovered nothovered').removeAttr('title');
	},

	/**
	 * ������������ �������
	 */
	setRate: function(rate){
		var self = this;

		self.rate = rate;
		self.$stars.addClass('inactive');
		self.$stars.slice(0, self.rate).removeClass('inactive');
	},

	/**
	 * ��������� 10 �����
	 */
	appendStars: function(){
		var self = this;

		var $star = $('<span class="star inactive"></span>');

		for(var i = 0; i < 10; i++){
			var $s = $star.clone();

			if (!self.disabled){
				$s.attr('title', '������: '+(i+1));

				(function(i){
					$s.on('click', function(e){
						e.preventDefault();

						$.post('/opendata/list/?action=vote', {id:self.id, rate:i+1}, function(r){
							if (r.success){
								self.disable();
								self.setRate(r.rate);
							}else{
								alert('��������� ������');
							}
						});
					});
				}(i))

			}

			self.$el.append($s);
		}

		self.$stars = self.$('.star');

		self.setRate(self.rate);
	}
});
