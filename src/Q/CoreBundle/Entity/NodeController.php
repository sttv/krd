<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Привязанный контроллер страницы сайта
 *
 * @ORM\Entity
 * @ORM\Table(name="node_controller")
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     name={"text", {"label"="Название"}},
 *     controller={"text", {"label"="Контроллер"}},
 *     modules={"text_collection", {"label"="Модули"}},
 *     info={"textarea", {"label"="Описание"}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class NodeController
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Текстовое название
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     */
    private $name;

    /**
     * Контроллер в формате "@QCoreBundle:Structure"
     * без указания экшена
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^@[a-zA-Z0-9]+Bundle:[a-zA-Z0-9]+$/", match=true, message="Должно быть вида ExampleBundle:Controller")
     */
    private $controller;

    /**
     * Список модулей в панели администратора
     *
     * @ORM\Column(type="array")
     */
    private $modules;

    /**
     * Описание
     *
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     */
    private $info;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function __construct()
    {
        $this->modules = array();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setController($controller)
    {
        $this->controller = $controller;
    }

    public function getModules()
    {
        return $this->modules;
    }

    public function setModules(array $modules)
    {
        $this->modules = array();

        foreach($modules as $module) {
            $this->addModule($module);
        }
    }

    public function hasModule($module)
    {
        return in_array(mb_strtolower($module), $this->modules, true);
    }

    public function addModule($module)
    {
        $module = mb_strtolower($module);

        if (!empty($module) && !$this->hasModule($module)) {
            $this->modules[] = $module;
        }
    }

    public function removeModule($module)
    {
        if (($index = array_search($module, $this->modules, true)) !== false) {
            unset($this->modules[$index]);
            $this->modules = array_values($this->modules);
        }
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function setInfo($info)
    {
        $this->info = $info;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return $this->getName();
    }
}


