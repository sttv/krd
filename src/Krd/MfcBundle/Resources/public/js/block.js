function getPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
      cursor.x = e.pageX;
      cursor.y = e.pageY;
    }
    else {
      cursor.x = e.clientX +
        (document.documentElement.scrollLeft ||
         document.body.scrollLeft) -
         document.documentElement.clientLeft;
      cursor.y = e.clientY +
        (document.documentElement.scrollTop ||
         document.body.scrollTop) -
         document.documentElement.clientTop;
    }
    return cursor;
}

var MFC = {
	$map: null,
	$map_scroller: null,
	$infobox: {
		block: null,
		info: null
	},
	items: {},
	colors: {
		'state-1': ['#ffe49d', '#ffe49d'],
		state0: ['#85ea00', '#c2f580'],
		state1: ['#ffcc00', '#ffe26e'],
		state2: ['#ea2d32', '#f59699']
	},
	statuses: {
		'state-1': 'не известно',
		state0: 'земельный участок свободен',
		state1: 'на участок подана заявка',
		state2: 'участок в стадии оформления'
	},
	prev_selected: null,
	prev_color: '#c0ad00',
	current_selected: null,
	feature_clickHandler: function() {
		var buf = this.getBBox();
		MFC.$infobox.block.css({top:buf.y+buf.height/2+365, left:buf.x+buf.width/2+415}).show();
	},
	feature_overHandler: function() {

		var title = this.data('title');
		if (!title) return;

		var buf = this.getBBox();
		MFC.$infobox.block.css({top:buf.y+buf.height/2+365, left:buf.x+buf.width/2+415}).show();
		this.attr('fill', this.data('color_hl'));

		MFC.$infobox.info.html(this.data('title'));
		//MFC.$infobox.block.css({top:buf.y, left:buf.x}).show();
	},
	feature_outHandler: function() {
		this.attr('fill', this.data('color_default'));
		MFC.$infobox.block.hide();
	},
	window_resizeHanlder: function() {

		var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],
			x=w.innerWidth||e.clientWidth||g.clientWidth,
			y=w.innerHeight||e.clientHeight||g.clientHeight;

		MFC.$map_scroller.css('height', y - 100 - $('#mfc-block-fullscreen .mfc-block-description').height());
		MFC.$map.css('left', -(1366-MFC.$map_scroller.width())/2);
	},
	init: function() {

		MFC.$map = $('#mfc-map');
		MFC.$infobox.block = $('#mfc-infobox');
		MFC.$infobox.info = $('#mfc-infobox-info');
		MFC.$map_scroller = $('#mfc-map-scroller');

		//$(window).resize(MFC.window_resizeHanlder);
		$('.mfc-collapse').click(function(){
			$('#mfc-block-fullscreen').removeClass('mfc-block-fullscreen');
			$('.mfc-expand').show();
			$('.mfc-collapse').hide();

			MFC.window_resizeHanlder();
			return false;
		});
		$('.mfc-expand').click(function(){
			$('#mfc-block-fullscreen').addClass('mfc-block-fullscreen');
			$('.mfc-collapse').show();
			$('.mfc-expand').hide();

			MFC.window_resizeHanlder();
			return false;
		});

		var buf, title;
		MFCMap.init(894, 628);
		MFCMap.rsr.forEach(function(item){

			title = null;
			if (MFC.items[item.data('id')]) {
				buf = MFC.items[item.data('id')];
				title = buf.settlement+', '+buf.street+', '+buf.number + '<br/>'+MFC.statuses['state'+buf.descr];
				buf = buf.descr;
				item[0].style.cursor = "pointer";
			}
			else
				buf = '-1';

			item.data('color_default', MFC.colors['state'+buf][0]);
			item.data('color_hl', MFC.colors['state'+buf][1]);
			item.data('title', title);
			item.attr('fill', MFC.colors['state'+buf][0]);

			/*item[0].onmouseover = function(e) {
				//item.animate({'fill-opacity': '0.5'}, 200);
				item.attr('fill', item.data('color_hl'));

				buf = getPosition(e);
				MFC.$infobox.info.html(item.data('title')+'<br/>&nbsp;');
				//MFC.$infobox.block.css({top:buf.y, left:buf.x}).show();
				//MFC.$infobox
			};
			item[0].onmouseout = function() {
				//item.animate({'fill-opacity': '1'}, 200);
				//item.attr('fill', '#c0ad00');
				item.attr('fill', item.data('color_default'));
				//MFC.$infobox.block.hide();
			};*/

			//item.click(MFC.feature_clickHandler);
			item.hover(MFC.feature_overHandler, MFC.feature_outHandler);
		});

		$('.mfc-map-mt, .mfc-map-mb, .mfc-map-mr, .mfc-map-ml').click(function(){
			var $c = $(this);
			var dirx = 1, diry = 1;
			var step = 50;
			var mx = 0, my = 0;
			if ($c.hasClass('mfc-map-mr')) {
				dirx = -1;
				mx = step;
			}
			if ($c.hasClass('mfc-map-mb')) {
				diry = -1;
				my = step;
			}
			//alert((parseInt(MFC.$map.css('margin-left')) + dirx * step)+', '+(parseInt(MFC.$map.css('margin-top')) + diry * step));

			var newx = parseInt(MFC.$map.css('left')), newy = parseInt(MFC.$map.css('top'));
			if ($c.hasClass('mfc-map-mr') || $c.hasClass('mfc-map-ml'))
				newx = newx + dirx * step;
			if ($c.hasClass('mfc-map-mt') || $c.hasClass('mfc-map-mb'))
				newy = newy + diry * step;

			if (newx < -660 || newx > 50 || newy < -850 || newy > -50)
				return;

			MFC.$map.css('left', newx);
			MFC.$map.css('top', newy);
		});
	}
}

$(function(){

	$.ajax({
		url: '/ajax/?type=mfc&action=load',
		dataType: 'json',
		success: function(data) {
			MFC.items = data;
			MFC.init();
		}
	});

	$lists_free = $('#mfc-list-free');
	$.ajax({
		url: '/ajax/?type=mfc&action=loadFree',
		dataType: 'json',
		success: function(data) {

			if (!data.length) {
				$('#mfc-list-free-title').text('Свободные участки отсутствуют');
				return;
			}

			for (var i = 0; i < data.length; i++)
				$lists_free.append('<li>'+data[i].settlement+', '+data[i].street+', '+data[i].number+'</li>');
		}
	});
	//

});
