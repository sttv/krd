<?php

namespace Q\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Q\CoreBundle\Form\Type\NodeType;

class RedirectController extends Controller
{
    /**
     * Страница управления редиректами
     *
     * @Route("/redirect/", name="cms_redirect_index")
     * @Template()
     * @Secure(roles="ROLE_CMS_REDIRECT")
     */
    public function indexAction()
    {
    }
}
