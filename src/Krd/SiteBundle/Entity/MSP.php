<?php

namespace Krd\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * Элемент списка реестра МСП
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="msp",
 *      indexes={
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="archive", columns={"archive"}),
 *          @ORM\Index(name="active_archive_parent_id", columns={"active", "archive", "parent_id"}),
 *          @ORM\Index(name="recNum", columns={"recNum"}),
 *          @ORM\Index(name="recNum_ord", columns={"recNum1", "recNum2"}),
 *          @ORM\Index(name="dateInc", columns={"dateInc"}),
 *          @ORM\Index(name="orgTitle", columns={"orgTitle"}),
 *          @ORM\Index(name="dateApprove", columns={"dateApprove"}),
 *          @ORM\Index(name="llcTitle", columns={"llcTitle"}),
 *          @ORM\Index(name="inn", columns={"inn"}),
 *          @ORM\Index(name="view", columns={"view"}),
 *          @ORM\Index(name="form", columns={"form"}),
 *          @ORM\Index(name="amount", columns={"amount"}),
 *          @ORM\Index(name="yearOf", columns={"yearOf"}),
 *          @ORM\Index(name="category", columns={"category"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     recNum={"text", {"label"="Номер реестровой записи","required"=false}},
 *     dateInc={"datetime", {
 *         "label"="Дата включения сведений в реестр",
 *         "required"=false,
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     orgTitle={"text", {"label"="Наименование органа, предоставившего поддержку","required"=false}},
 *     dateApprove={"datetime", {
 *         "label"="Дата принятия решения о предоставлении или прекращении оказания поддержки",
 *         "required"=false,
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     llcTitle={"text", {"label"="Наименование юридического лица или фамилия, имя и (при наличии) отчество индивидуального  предпринимателя","required"=false}},
 *     inn={"text", {"label"="ИНН","required"=false}},
 *     form={"text", {"label"="Форма","required"=false}},
 *     view={"text", {"label"="Вид","required"=false}},
 *     amount={"text", {"label"="Размер, рублей","required"=false}},
 *     yearOf={"text", {"label"="Срок оказания","required"=false}},
 *     information={"textarea", {"label"="Информация","required"=false}},
 *     category={"choice", {
 *         "label"="Категория субъекта",
 *         "required"=false,
 *         "choices"={
 *             "1"="Субъект малого предпринимательства (за исключением микропредприятий)",
 *             "2"="Субъект среднего предпринимательства",
 *             "3"="Микропредприятие"
 *         }
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     archive={"checkbox", {
 *         "label"="Архив",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class MSP
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Номер реестровой записи
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $recNum;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $recNum1;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $recNum2;

    /**
     * Дата включения сведений в реестр
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y'>")
     * @Gedmo\Versioned
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $dateInc;

    /**
     * Наименование органа, предоставившего поддержку
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $orgTitle;

    /**
     * Дата принятия решения о предоставлении или прекращении оказания поддержки
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y'>")
     * @Gedmo\Versioned
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $dateApprove;

    /**
     * Наименование юридического лица или фамилия, имя и (при наличии) отчество индивидуального  предпринимателя
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $llcTitle;

    /**
     * ИНН
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $inn;

    /**
     * Вид
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $view;

    /**
     * Форма
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $form;

    /**
     * Размер, рублей
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $amount;

    /**
     * Срок оказания
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $yearOf;

    /**
     * Информация
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $information;

    /**
     * Категория субъекта
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $category;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Архив
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $archive = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recNum
     *
     * @param string $recNum
     * @return MSP
     */
    public function setRecNum($recNum)
    {
        $this->recNum = $recNum;

        return $this;
    }

    /**
     * Get recNum
     *
     * @return string
     */
    public function getRecNum()
    {
        return $this->recNum;
    }

    /**
     * Set dateInc
     *
     * @param \DateTime $dateInc
     * @return MSP
     */
    public function setDateInc($dateInc)
    {
        $this->dateInc = $dateInc;

        return $this;
    }

    /**
     * Get dateInc
     *
     * @return \DateTime
     */
    public function getDateInc()
    {
        return $this->dateInc;
    }

    /**
     * Set orgTitle
     *
     * @param string $orgTitle
     * @return MSP
     */
    public function setOrgTitle($orgTitle)
    {
        $this->orgTitle = $orgTitle;

        return $this;
    }

    /**
     * Get orgTitle
     *
     * @return string
     */
    public function getOrgTitle()
    {
        return $this->orgTitle;
    }

    /**
     * Set dateApprove
     *
     * @param \DateTime $dateApprove
     * @return MSP
     */
    public function setDateApprove($dateApprove)
    {
        $this->dateApprove = $dateApprove;

        return $this;
    }

    /**
     * Get dateApprove
     *
     * @return \DateTime
     */
    public function getDateApprove()
    {
        return $this->dateApprove;
    }

    /**
     * Set llcTitle
     *
     * @param string $llcTitle
     * @return MSP
     */
    public function setLlcTitle($llcTitle)
    {
        $this->llcTitle = $llcTitle;

        return $this;
    }

    /**
     * Get llcTitle
     *
     * @return string
     */
    public function getLlcTitle()
    {
        return $this->llcTitle;
    }

    /**
     * Set inn
     *
     * @param string $inn
     * @return MSP
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn
     *
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set view
     *
     * @param string $view
     * @return MSP
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set form
     *
     * @param string $form
     * @return MSP
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return MSP
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set yearOf
     *
     * @param string $yearOf
     * @return MSP
     */
    public function setYearOf($yearOf)
    {
        $this->yearOf = $yearOf;

        return $this;
    }

    /**
     * Get yearOf
     *
     * @return string
     */
    public function getYearOf()
    {
        return $this->yearOf;
    }

    /**
     * Set information
     *
     * @param string $information
     * @return MSP
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Set category
     *
     * @param integer $category
     * @return MSP
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return MSP
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return MSP
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return MSP
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Q\CoreBundle\Entity\Node $parent
     * @return MSP
     */
    public function setParent(\Q\CoreBundle\Entity\Node $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Q\CoreBundle\Entity\Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return MSP
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return MSP
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     * @return MSP
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    
        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set recNum1
     *
     * @param integer $recNum1
     * @return MSP
     */
    public function setRecNum1($recNum1)
    {
        $this->recNum1 = $recNum1;
    
        return $this;
    }

    /**
     * Get recNum1
     *
     * @return integer 
     */
    public function getRecNum1()
    {
        return $this->recNum1;
    }

    /**
     * Set recNum2
     *
     * @param integer $recNum2
     * @return MSP
     */
    public function setRecNum2($recNum2)
    {
        $this->recNum2 = $recNum2;
    
        return $this;
    }

    /**
     * Get recNum2
     *
     * @return integer 
     */
    public function getRecNum2()
    {
        return $this->recNum2;
    }

    protected function updateRecNumOrders()
    {
        $parts = explode('.', $this->getRecNum(), 2);

        if (!isset($parts[0])) {
            $parts[0] = 0;
        }

        if (!isset($parts[1])) {
            $parts[1] = 0;
        }

        $this->setRecNum1($parts[0]);
        $this->setRecNum2($parts[1]);
    }

    /**
     * @ORM\PrePersist
     */
    public function beforePersist()
    {
        $this->updateRecNumOrders();
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeUpdate()
    {
        $this->updateRecNumOrders();
    }
}