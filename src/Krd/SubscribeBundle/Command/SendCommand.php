<?php

namespace Krd\SubscribeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\SubscribeBundle\Entity\Subscriber;


/**
 * Запуск интернет рассылки
 */
class SendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:subscribe:send')
            ->setDescription('Send a letters to subscribers')
            ->setHelp(<<<EOT
                      The <info>krd:subscribe:send</info> will send all delayed letters to the subscribers.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('qcore.mailer');
        $letterRepo = $this->getManager()->getRepository('KrdSubscribeBundle:Letter');

        $subscriberRepo = $this->getManager()->getRepository('KrdSubscribeBundle:Subscriber');

        $emails = array();

        foreach ($subscriberRepo->findBy(array('active' => true)) as $subscriber) {
            $email = $subscriber->getEmail();
            $email = trim($email);

            if (!empty($email) && !in_array($email, $emails)) {
                $emails[] = $email;
            }
        }

        $letters = $letterRepo->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.sent = 0')
            ->andWhere('e.date <= :date')
            ->orderBy('e.id')
            ->setParameter('date', new \DateTime())
            ->getQuery()
                ->getResult();

        foreach ($letters as $letter) {
            $letter->setSent(true);
            $this->getManager()->flush($letter);

            try {
                $message = $mailer->newMessage('Информационная рассылка за '.$letter->getDate()->format('d.m.Y'));
                $message->setTo($emails);
                $mailer->sendTemplate($message, 'KrdSubscribeBundle:Email:letter.html.twig', array('letter' => $letter));
            } catch (\Exception $e) {
                $this->getContainer()->get('logger')->crit('Ошибка рассылки', array($letter->getId(), $subscriber->getId(), $e->getMessage()));
            }
        }
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
