<?php

namespace Q\FilesBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Output\OutputInterface;

use Q\FilesBundle\Entity\Video;
use Q\FilesBundle\Entity\VideoFormat;
use Q\FilesBundle\Entity\Image;


/**
 * Репозиторий видео
 */
class VideoRepository extends EntityRepository
{
    /**
     * Корневая папка
     */
    protected $rootDir;

    /**
     * Путь к папке с кешем форматов видео
     */
    protected $cacheDir;

    /**
     * Ссылка на папку с кешем форматов видео
     */
    protected $cachePath;

    /**
     * Pathes to binaries
     */
    protected $ffmpegBin;
    protected $ffprobeBin;
    protected $qtfaststartBin;

    /**
     * @JMSDI\InjectParams({
     *     "container" = @JMSDI\Inject("service_container")
     * })
     */
    public function setParameters($container)
    {
        $this->rootDir = $container->getParameter('kernel.root_dir').'/../web';
        $this->cacheDir = $container->getParameter('q_files.video.cache_dir');
        $this->cachePath = $container->getParameter('q_files.video.cache_path');
        $this->ffmpegBin = $container->getParameter('q_files.video.ffmpeg_bin');
        $this->ffprobeBin = $container->getParameter('q_files.video.ffprobe_bin');
        $this->qtfaststartBin = $container->getParameter('q_files.video.qtfaststart_bin');
    }

    public function getFFMpeg()
    {
        return \FFMpeg\FFMpeg::create(array(
            'ffmpeg.binaries'  => $this->ffmpegBin,
            'ffprobe.binaries' => $this->ffprobeBin,
            'timeout' => 3600,
            'ffmpeg.threads' => 12,
        ));
    }

    public function getQtFaststart()
    {
        return \FFMpeg\QtFaststart::create(array(
            'qtfaststart.binaries'  => $this->qtfaststartBin,
        ));
    }

    /**
     * Создание Video из файла
     * При этом указанный файл удалится
     * @param  string $filePath
     * @return Image
     */
    public function createVideoFromFile($filePath)
    {
        $fs = new Filesystem();
        if (!$fs->exists($filePath)) {
            throw new \Exception('File does not exists');
        }

        $video = new Video();
        $video->setTitle('');
        $video->setFile(new UploadedFile($filePath, basename($filePath), null, null, null, true));

        return $video;
    }

    /**
     * Создание превью для видео
     * @param  Video  $video
     */
    public function createPreview(Video $video)
    {
        if (!$video->hasFile()) {
            return;
        }

        if ($video->hasImage()) {
            $this->_em->remove($video->getImage());
            $this->_em->flush();
        }

        $imagePreviewPath = $this->cacheDir.'/'.basename($video->getFilePath()).'.jpg';

        $fs = new Filesystem();
        if (!$fs->exists(dirname($imagePreviewPath))) {
            $fs->mkdir(dirname($imagePreviewPath), 0777);
        }

        $videoFile = $this->getFFMpeg()->open($this->rootDir.$video->getFilePath());

        $duration = (int)$videoFile->getFFProbe()
            ->streams($this->rootDir.$video->getFilePath())
            ->videos()
            ->first()
            ->get('duration');
        $fromTime = $duration / 2;

        $videoFile->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($fromTime>0 ? $fromTime : 5))->save($imagePreviewPath);

        $image = $this->_em->getRepository('QFilesBundle:Image')->createImageFromFile($imagePreviewPath);

        $video->setImage($image);

        $this->_em->flush();
    }

    /**
     * Массовое конвертирование видео через консоль
     * @param  boolean $updateMode Конвертировать уже сконвертированные видео?
     * @param  OutputInterface  $output     Вывод в консоль
     */
    public function convertVideos($updateMode = false, OutputInterface $output = null)
    {
        if ($updateMode) {
            $videos = $this->findAll();
        } else {
            $videos = $this->findBy(array('converted' => false));
        }

        if (!$videos) {
            $output->writeLn('<info>All video are converted</info>');
            return;
        }

        $output->writeLn('Found <info>'.count($videos).'</info> videos');

        foreach($videos as $item) {
            $output->write('['.$item->getId().'] '.$item->getTitle().' process...');
            $res = $this->convertVideo($item, 'sd', array(640, 360));
			//$res = $this->convertVideo($item, 'hd', array(1280, 720));
            if ($res === true) {
                $output->writeLn(' <info>completed</info>');
            } else {
                $output->writeLn(' <error>'.$res.'</error>');
            }
        }
    }

    /**
     * @return \FFMpeg\Format\Video\X264
     */
    public function getX264Format()
    {
        $format = new \FFMpeg\Format\Video\X264();
        $format->setKiloBitrate(250);
        $format->setAudioKiloBitrate(56);

        return $format;
    }

    /**
     * @return \FFMpeg\Format\Video\WebM
     */
    public function getWebMFormat()
    {
        $format = new \FFMpeg\Format\Video\WebM();
        $format->setKiloBitrate(250);
        $format->setAudioKiloBitrate(32);

        return $format;
    }

    /**
     * Конвертирование видео
     * @param  Video  $video
     * @return  string|true
     */
    public function convertVideo(Video $video, $formatType, $dim = array(640, 360))
    {
        $ffmpeg = $this->getFFMpeg();

        try {
            $videoFile = $ffmpeg->open($this->rootDir.$video->getFilePath());
            $videoFile
                ->filters()
                ->resize(new \FFMpeg\Coordinate\Dimension($dim[0], $dim[1]))
                ->synchronize();
        } catch (\FFMpeg\Exception\RuntimeException $e) {
            return $e->getMessage();
        }

        $formats = array();

        try {
            // x264
            $filePath = $this->cacheDir.'/'.$dim[1].'-'.uniqid('vc-', true).'.mp4';
            $videoFile->save($this->getX264Format(), $filePath);
            $this->getQtFaststart()->process($filePath);
            $formats['mp4'] = $filePath;

            // WebM
            $filePath = $this->cacheDir.'/'.$dim[1].'-'.uniqid('vc-', true).'.webm';
            $videoFile->save($this->getWebMFormat(), $filePath, array('-ar', '22050'));
            $formats['webm'] = $filePath;
        } catch (\Exception $e) {
            foreach($formats as $file) {
                @unlink($file);
            }

            return $e->getMessage();
        }

        // Удаление старых файлов формата
        foreach($video->getFormat() as $format) {
            $video->removeFormat($format);
            $this->_em->remove($format);
        }

        // Добавляем новые файлы формата
        foreach($formats as $type => $filePath) {
            $format = new VideoFormat();
            $format->setFormat($type);
			$format->setType($formatType);
            $format->setFile(new UploadedFile($filePath, basename($filePath), null, null, null, true));
            $video->addFormat($format);
        }

        $video->setConverted(true);

        $this->_em->flush();

        foreach($formats as $file) {
            @unlink($file);
        }

        return true;
    }
}
