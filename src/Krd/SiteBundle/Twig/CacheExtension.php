<?php

namespace Krd\SiteBundle\Twig;

use Doctrine\Common\Cache\ApcCache;
use Asm89\Twig\CacheExtension\CacheProvider\DoctrineCacheAdapter;
use Asm89\Twig\CacheExtension\CacheStrategy\LifetimeCacheStrategy;


/**
 * Twig кеширование
 */
class CacheExtension extends \Asm89\Twig\CacheExtension\Extension
{
    public function __construct($prefix = '')
    {
        $cache = new ApcCache();

        if ($prefix) {
            $cache->setNamespace($prefix);
        }

        $cacheProvider = new DoctrineCacheAdapter($cache);
        $cacheStrategy = new LifetimeCacheStrategy($cacheProvider);

        parent::__construct($cacheStrategy);
    }
}
