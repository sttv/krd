<?php

namespace Q\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\NodeController;
use Q\CoreBundle\Form\Type\NodeType;


/**
 * Контроллер CMS структуры сайта
 *
 * @Route("/struct")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class StructureController extends Controller
{
    /**
     * Генерация имени формы
     * @param  string $prefix
     * @return string
     */
    protected function getFormName($prefix = '')
    {
        return $prefix.'qcorebundle_node';
    }

    /**
     * Страница редактирования структуры сайта
     *
     * @Route("{url}", name="cms_struct_index", defaults={"url"="/"}, requirements={"url"=".*"})
     * @Template()
     */
    public function indexAction(Request $request, $url)
    {
        $uri = $request->getPathInfo();

        if ($uri{mb_strlen($uri)-1} != '/') {
            $query = $request->getQueryString();
            if (!empty($query)) {
                $query = '?'.$query;
            }
            return $this->redirect($uri.'/'.$query);
        }

        // Подменяем УРЛ для определения текущего нода
        $this->get('qcore.routing.current')->setCurrentPath(str_replace('/cms/struct', '', $uri));

        $currentNode = $this->get('qcore.routing.current')->getNode();

        if (!$currentNode) {
            throw $this->createNotFoundException();
        }

        $user = $this->get('security.context')->getToken()->getUser();

        // Если нужно, редиректим пользователя на первый разрешенный раздел
        if ($currentNode->getId() == 1 && !$this->get('security.context')->isGranted('ROLE_CMS_ADMIN') && !$currentNode->isGranted() && $user->hasCmsIndex() && $user->getCmsIndex() != $uri) {
            return $this->redirect($user->getCmsIndex());
        }

        // Получение пути к текущей странице
        $nodeRepo = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node');
        $treeExpanded = $nodeRepo->getParentBranch($currentNode, true);

        foreach($treeExpanded as &$node) {
            $node = $node->getId();
        }

        $treeExpanded = $this->get('jms_serializer')->serialize($treeExpanded, 'json');

        // Форма редактирования текущего нода
        $form = $this->get('form_metadata.mapper')->createFormBuilder($currentNode, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->setAction($this->generateUrl('cms_rest_node_edit', array('id' => $currentNode->getId())))
            ->getForm();

        // Форма управления доступом
        if ($this->get('security.context')->isGranted('ROLE_CMS_ADMIN')) {
            $securityFormView = $this->getSecurityForm($currentNode)->createView();
        } else {
            $securityFormView = null;
        }

        return array(
            'treeExpanded' => $treeExpanded,
            'editForm' => $form->createView(),
            'securityForm' => $securityFormView,
            'nodeChildrenCount' => $nodeRepo->getChildrenCount($currentNode),
        );
    }

    /**
     * Форма для редактирования доступа к разделу
     */
    public function getSecurityForm(Node $node)
    {
        $builder = $this->get('form.factory')
                ->createNamedBuilder('node_security', 'form', $node, array('csrf_protection' => true));

            $builder->add('usergroups', 'entity', array(
                'required' => false,
                'class' => 'QUserBundle:UserGroup',
                'multiple' => true,
                'property' => 'name',
                'label' => 'Группы, имеющие доступ к разделу'
            ));

            $builder->add('users', 'entity', array(
                'required' => false,
                'class' => 'QUserBundle:User',
                'multiple' => true,
                'property' => 'cmstitle',
                'label' => 'Пользователи, имеющие доступ к разделу',
                'query_builder' => function ($repo) {
                    return $repo->createQueryBuilder('u')
                    ->join('u.groups', 'g')
                    ->where('g.roles LIKE :roles')
                    ->setParameter('roles', '%"ROLE_CMS"%');
                }
            ));

            $builder->add('submit', 'submit', array(
                'label' => 'Отправить'
            ));

            $builder->setAction($this->get('router')->generate('cms_rest_node_sucurity_edit', array('id' => $node->getId())));

            return $builder->getForm();
    }
}
