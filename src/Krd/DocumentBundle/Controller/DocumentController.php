<?php

namespace Krd\DocumentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Документы
 */
class DocumentController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {
        $items = $this->get('krd.document.modules.document')->getRepository()
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.parent = :parent')
            ->setParameter('parent', $this->get('qcore.routing.current')->getNode()->getId())
            ->setMaxResults(2)
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        if (count($items) == 1) {
            return $this->redirect($items[0]->getUrl());
        }
    }

    /**
     * Детальная страница
     *
     * @Route("/{name}.html", name="krd_document_document_detail")
     * @Template()
     */
    public function detailAction()
    {
        $this->get('krd_import.listener.normacache')->enable();

        if ($item = $this->get('krd.document.modules.document')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
            $this->get('qcore.routing.breadcrumb')->removeLastDuplicates();
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/document/list.json", name="ajax_document_document_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        $parent = $request->get('parent', 1);

        if (count($request->query->keys()) > 10) {
            $result = $this->get('krd.document.modules.document')->getItemsNSR($parent);
        } else {
            $result = $this->get('krd.document.modules.document')->getItems($parent);
        }

        return $result['items'];
    }
}
