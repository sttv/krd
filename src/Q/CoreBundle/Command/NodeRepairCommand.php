<?php

namespace Q\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\NodeRepair;


/**
 * Восстановление ключей left/right по полю parent_id
 */
class NodeRepairCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('q:core:node:repair')
            ->setDescription('Repair left/right keys by parent id');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('QCoreBundle:Node');
        $repositoryRepair = $em->getRepository('QCoreBundle:NodeRepair');

        $output->writeln('Removing old data...');
        $tableNameRepair = $em->getClassMetadata('QCoreBundle:NodeRepair')->getTableName();
        $statement = $em->getConnection()->prepare("TRUNCATE `{$tableNameRepair}`");
        $statement->execute();

        // Смотрим на преддущую структуру
        $nodeQuery = $repository->createQueryBuilder('n')
            ->orderBy('n.parent', 'ASC')
            ->orderBy('n.left', 'ASC')
            ->getQuery();
        $deferredNodes = array();

        $em->getConnection()->beginTransaction();

        $i = 0;
        foreach($nodeQuery->iterate() as $node) {
            $node = $node[0];
            $nodeRepair = new NodeRepair();
            $nodeRepair->setOldId($node->getId());

            $nodeParent = $node->getParent();
            if ($nodeParent) {
                $nodeParentRepair = $repositoryRepair->findBy(array('oldid' => $nodeParent->getId()));
                $nodeRepair->setParent(array_pop($nodeParentRepair));
            } elseif ($node->getId() != 1) {
                $deferredNodes[] = $node;
                $output->writeln('Deferred node <message>'.$node->getId().'</message>');
                continue;
            }

            $em->persist($nodeRepair);
            $em->flush();
            $output->writeln('Create structure '.(++$i));

            if ($i % 50 == 0) {
                $em->getConnection()->commit();
                $em->clear();
                $em->getConnection()->beginTransaction();
            }
        }

        foreach($deferredNodes as $node) {
            $nodeRepair = new NodeRepair();
            $nodeRepair->setOldId($node->getId());

            $nodeParent = $node->getParent();
            if ($nodeParent) {
                $nodeParentRepair = $repositoryRepair->findBy(array('oldid' => $nodeParent->getId()));
                $nodeRepair->setParent(array_pop($nodeParentRepair));
            } elseif ($node->getId() != 1) {
                $output->writeln('Error import <error>'.$node->getId().'</error>');
                continue;
            }

            $em->persist($nodeRepair);
            $em->flush();
            $output->writeln('Create structure '.$i++.'/');

            if ($i % 50 == 0) {
                $em->getConnection()->commit();
                $em->clear();
                $em->getConnection()->beginTransaction();
            }
        }

        $em->getConnection()->commit();
        $em->clear();

        // Дополнительно восстанавливаем связи
        $output->writeln('Check to NULL-parent nodes...');

        $nullParentNodesRepairQuery = $repositoryRepair->createQueryBuilder('n')
            ->andWhere('n.parent IS NULL')
            ->andWhere('n.oldid != 1')
            ->getQuery();

        $em->getConnection()->beginTransaction();

        $i = 0;
        foreach($nullParentNodesRepairQuery->iterate() as $nodeRepair) {
            $nodeRepair = $nodeRepair[0];
            $node = $repository->find($nodeRepair->getOldId());

            if ($node && $nodeParent = $node->getParent()) {
                $nodeParentRepair = $repositoryRepair->findBy(array('oldid' => $nodeParent->getId()));
                $nodeRepair->setParent(array_pop($nodeParentRepair));
            } else {
                $em->remove($nodeRepair);
            }

            $em->flush();
            $i++;
        }

        $em->getConnection()->commit();
        $em->clear();

        $output->writeln('Complete. Found: '.$i);

        // Восстанавливаем left/right по созданной структуре
        $tableName = $em->getClassMetadata('QCoreBundle:Node')->getTableName();
        $repairAllQuery = $repositoryRepair->createQueryBuilder('n')->getQuery();

        $em->getConnection()->beginTransaction();

        $i = 0;
        foreach($repairAllQuery->iterate() as $nodeRepair) {
            $nodeRepair = $nodeRepair[0];

            if (!$nodeRepair->getOldId()) {
                continue;
            }

            $statement = $em->getConnection()->prepare("UPDATE `{$tableName}` SET `left` = {$nodeRepair->getLeft()}, `right` = {$nodeRepair->getRight()}, `level` = {$nodeRepair->getLevel()} WHERE `id` = {$nodeRepair->getOldId()} LIMIT 1");
            $statement->execute();
            $output->writeln('Repair structure..'.$i++);
        }

        $em->getConnection()->commit();


        $output->writeln('<info>Success</info>');
    }
}
