/**
 * Модуль работы с формами
 */
angular.module('forms', ['locale'])
    .run([function() {
        if (!$.fn.serializeObject) {
            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });

                return o;
            };
        }
    }])

    /**
     * Событие на Enter
     */
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })

    /**
     * Событие на ESC
     */
    .directive('ngEsc', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 27) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEsc);
                    });

                    event.preventDefault();
                }
            });
        };
    })

    /**
     * Всплывающие сообщения
     */
    .service('$bubble', [function() {
        /**
         * Класс сообщения в зависимости от типа
         */
        var types = {
            'default': '',
            'error': 'error'
        };

        var bubble = {
            /**
             * Удаление всплывающего сообщения у элемента
             */
            remove: function($element) {
                var $message;

                if ($element.length > 1) {
                    $element.each(function() {
                        bubble.remove($(this));
                    });

                    return;
                }

                if ($message = $element.data('forms.bubble.message')) {
                    $message.fadeOut(150, function() {
                        $message.remove();
                    });

                    $element.data('forms.bubble.message', null);
                }
            },

            /**
             * Показ сообщения об ошибка
             * @param  {string} message Сообщение
             * @param  {object} $element  Элемент jQuery (не обязательно)
             */
            error: function(message, $element) {
                if ($element) {
                    $element.each(function() {
                        bubble.showOnElement(message, $(this), 'error');
                    });
                }
            },

            /**
             * Показывает сообщение над элементом
             * Если элемент не виден, то ищет первого видимого родителя
             *
             * @param  {string} message Сообщение
             * @param  {objecy} $element  Элемент
             * @param  {string} type Тип сообщения
             */
            showOnElement: function(message, $element, type) {
                var $exists;

                if ($exists = $element.data('forms.bubble.message')) {
                    bubble.remove($element);
                    setTimeout(function() {
                        bubble.showOnElement(message, $element, type);
                    }, 150);
                    return;
                }

                var $message = $('<div class="bubble"></div>');

                if (type && types[type]) {
                    $message.addClass(types[type]);
                }

                $message.html(message);

                $message.remove().appendTo('body').css({visibility:'hidden', top:0, left:0});

                $message.on('click', function() {
                    bubble.remove($element);
                });

                $('body').on('click', '.dialog-overlay, .dialog .close-btn', function() {
                    bubble.remove($element);
                });

                $element.on('remove', function() {
                    bubble.remove($element);
                });

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                $message.addClass('gravity-'+gravity).fadeIn(150);
                $message.append('<div class="arrow"></div>');

                $element.data('forms.bubble.message', $message);
                $element.data('forms.bubble.message.step', $element.closest('*[data-step]'));
                bubble.updatePosition($element);

                var interval = setInterval(function() {
                    if ($element.data('forms.bubble.message')) {
                        bubble.updatePosition($element);
                    } else {
                        clearInterval(interval);
                    }
                }, 100);
            },

            /**
             * Обновляет позицию всплывающего сообщения у элемента
             */
            updatePosition: function($element) {
                var $message = $element.data('forms.bubble.message');
                var $step = $element.data('forms.bubble.message.step');
                var $scope = $element.scope();

                if (!$message) {
                    return;
                }

                var $el = $element;
                var visibility = 'visible';

                if (!$el.is(':visible')) {
                    if ($scope.stepForm && $step.length > 0) {
                        if ($scope.step == $step.data('step')) {
                            visibility = 'visible';
                        } else {
                            visibility = 'hidden';
                        }
                    } else {
                        $el = $el.closest(':visible').first();
                        visibility = 'visible';
                    }
                }

                var pos = $.extend({}, $el.offset(), {
                    width: $el[0].offsetWidth,
                    height: $el[0].offsetHeight
                });

                var options = {
                    offset: 0
                };

                var actualWidth = $message[0].offsetWidth,
                    actualHeight = $message[0].offsetHeight;

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                var tp = {opacity:1, visibility:visibility};

                switch (gravity.charAt(0)) {
                    case 'n':
                        tp.top = pos.top + pos.height + options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 's':
                        tp.top = pos.top - actualHeight - options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 'e':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left - actualWidth - options.offset;
                        break;

                    case 'w':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left + pos.width + options.offset;
                        break;
                }

                if (gravity.length > 1) {
                    if (gravity.charAt(1) == 'w') {
                        tp.left = pos.left + pos.width / 2 - 15;
                    } else {
                        tp.left = pos.left + pos.width / 2 - actualWidth + 15;
                    }
                }

                $message.css(tp);
            }
        };

        return bubble;
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Аяксовая форма
     * Можно добавить свой контроллер с методами onSuccess, onError, onFatalError
     */
    .directive('ajaxForm', ['$bubble', '$http', function($bubble, $http) {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.find('input, textarea').each(function() {
                    $(this).attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.action;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input, textarea').on('change keyup', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors = {};
                        });
                    });

                    $element.on('submit', function(e) {
                        e.preventDefault();

                        return false;
                    });

                    $scope.$watch('errors', $scope.updateBubbles, true);
                }
            },

            controller: function($scope, $element) {
                $scope.submit = function() {
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response, status, headers, config) {
                            $scope.loading = false;

                            if (!!$scope.onResponse) {
                                $scope.onResponse(response, status, headers, config);
                            }

                            if (response.success) {
                                if (!!$scope.onSuccess) {
                                    $scope.onSuccess(response, status, headers, config);
                                }
                            } else {
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }

                                if (!!$scope.onError) {
                                    $scope.onError(response, status, headers, config);
                                }
                            }
                        })
                        .error(function(response, status, headers, config) {
                            $scope.loading = false;

                            if (!!$scope.onFatalError) {
                                $scope.onFatalError(response, status, headers, config);
                            }

                            console.error(response);
                        });
                };

                /**
                 * Обновление всплывающих ошибок
                 */
                $scope.updateBubbles = function() {
                    $bubble.remove($element.find('input, select, textarea'));

                    var i = 0;
                    for(var name in $scope.errors) {
                        if (!$scope.errors.hasOwnProperty(name)) {
                            continue;
                        }

                        var errors = $scope.errors[name];

                        if (!errors || errors.length == 0) {
                            continue;
                        }

                        var $field = $element.find('*[name="'+name+'"]').first();

                        $bubble.error(errors.join(', '), $field);

                        i++;

                        if (i == 1 && !!$.scrollTo && $field.closest('.dialog').length==0) {
                            var $showEl = $field;

                            if ($showEl.closest('*[data-step]').length > 0 && $scope.stepForm) {
                                var $step = $showEl.closest('*[data-step]');
                                $scope.step = $step.data('step');
                                $.scrollTo($step, 200, {offset:{top:-150}});
                            } else {
                                if (!$showEl.is(':visible')) {
                                    $showEl = $showEl.closest(':visible').first();
                                }

                                if ($showEl.length > 0) {
                                    $.scrollTo($showEl, 200, {offset:{top:-150}});
                                }
                            }

                        }
                    }
                };

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                };

                /**
                 * Полная очистка формы
                 */
                $scope.clearForm = function() {
                    $element.find('input, select, textarea').each(function() {
                        var type = this.type.toString().toLowerCase();

                        switch(type) {
                            case "text":
                            case "password":
                            case "textarea":
                            case "hidden":
                                $(this).val('');
                                break;

                            case "radio":
                            case "checkbox":
                                $(this).prop('checked', false);
                                var $that = $(this);
                                setTimeout(function() {
                                    $that.trigger('change');
                                });
                                break;

                            case "select-one":
                            case "select-multi":
                                this.selectedIndex = -1;
                                break;

                            default:
                                break;
                          }
                    });
                };
            }
        }
    }])

    /**
     * Пошаговая форма
     */
    .directive('stepByStep', [function() {
        return {
            restrict: 'C',
            compile: function($element) {
                $element.find('.step-header').children('.step').each(function(i) {
                    $(this).attr('ng-class', '{active: step == '+(i+1)+', inpast: step > '+(i+1)+'}');
                    $(this).attr('ng-click', 'setStep('+(i+1)+')');
                });

                $element.find('*[data-step]').each(function() {
                    $(this).attr('ng-show', 'step == '+$(this).data('step'));
                });

                return function($scope, $element) {
                    $scope.step = 1;
                    $scope.stepForm = true;
                };
            },

            controller: function($scope) {
                $scope.nextStep = function() {
                    $scope.step++;
                };

                $scope.prevStep = function() {
                    $scope.step--;
                };

                // Установка предыдущего шага
                $scope.setStep = function(step) {
                    if (step < $scope.step) {
                        $scope.step = step;
                    }
                };
            }
        }
    }])

    .factory('klardConfig', [function() {
        return {
            token: '5368b939fca916fe2f0e2747',
            key: 'b30f05cd6b7774ca856a0034bc8a4ddff3828c35'
        }
    }])

    /**
     * Автодополнение города из КЛАДР
     */
    .directive('kladrCity', ['klardConfig', function(klardConfig) {
        if ($.kladr === undefined) {
            throw new Exception('КЛАДР не установлен');
        }

        return {
            restrict: 'A',
            compile: function($element, attrs) {
                $element.kladr({
                    token: klardConfig.token,
                    key: klardConfig.key,
                    type: $.kladr.type.city,
                    showSpinner: false,
                    select: function(obj) {
                        $element.data('cityId', obj.id);

                        if (attrs.kladrCity !== undefined) {
                            var $regionInput = $('#'+attrs.kladrCity);
                            $regionInput.kladr('parentId', obj.id);
                        }
                    }
                });
            }
        }
    }])

    /**
     * Автодополнение улицы из КЛАДР
     */
    .directive('kladrStreet', ['klardConfig', function(klardConfig) {
        if ($.kladr === undefined) {
            throw new Exception('КЛАДР не установлен');
        }

        return {
            restrict: 'A',
            compile: function($element) {
                $element.kladr({
                    token: klardConfig.token,
                    key: klardConfig.key,
                    parentType: $.kladr.type.city,
                    type: $.kladr.type.street,
                    showSpinner: false
                });
            }
        }
    }])

    /**
     * Автодополнение районов города краснодара
     */
    .directive('autocompleteRegion', ['locale', function(locale) {
        return {
            restrict: 'A',
            compile: function($element, attrs) {
                var list = [
                    "Прикубанский",
                    "Западный",
                    "Центральный",
                    "Карасунский"
                ];

                list = locale.trans(list);

                var $cityInput = $('#'+attrs.autocompleteRegion);

                $element.autocomplete({
                    source: list
                });

                $element.autocomplete('disable');

                $cityInput.on('change click keyup', function() {
                    if ($cityInput.data('cityId') == '2300000100000' || $cityInput.val().toString().toLowerCase() == 'краснодар') {
                        $element.autocomplete('enable');
                    } else {
                        $element.autocomplete('disable');
                    }
                });
            }
        }
    }])

    /**
     * Маска ввода телефона
     */
    .directive('phoneMask', function() {
        return {
            restrict: 'A',
            compile: function($element) {
                if (!!$.fn.mask) {
                    $($element).mask('+7-999-999-99-99', {placeholder:'_'});
                }
            }
        }
    })

    /**
     * Авто-высота textarea
     */
    .directive('textareaAdjust', function() {
        return {
            restrict: 'A',
            compile: function($element) {
                $element.on('keydown keyup focus change', function() {
                    this.style.height = '1px';
                    this.style.height = (this.scrollHeight) + 'px'
                })
            }
        }
    })
;
