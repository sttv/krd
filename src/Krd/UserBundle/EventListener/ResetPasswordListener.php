<?php

namespace Krd\UserBundle\EventListener;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Krd\UserBundle\KrdUserEvents;
use Q\CoreBundle\Swift\Mailer;
use Q\UserBundle\Entity\User;


class ResetPasswordListener implements EventSubscriberInterface
{
    private $userManager;
    private $tokenGenerator;
    private $mailer;
    private $router;

    public function __construct(UserManager $userManager, TokenGeneratorInterface $tokenGenerator, Mailer $mailer, UrlGeneratorInterface $router)
    {
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            KrdUserEvents::RESET_PASSWORD_REQUEST => 'onResetPasswordRequest',
            KrdUserEvents::RESET_PASSWORD_SUCCESS => 'onResetPasswordSuccess',
        );
    }

    /**
     * Запрос на смену пароля. Генерим токен и высылаем письмо.
     * @param  UserEvent $event
     */
    public function onResetPasswordRequest(UserEvent $event)
    {
        $user = $event->getUser();

        if ($user instanceof User) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
            $user->setPasswordRequestedAt(new \DateTime());
            $this->userManager->updateUser($user);

            $message = $this->mailer->newMessage('Сброс пароля на сайте %title%');
            $message->addTo($user->getEmail());

            $context = array();
            $context['user'] = $user;
            $context['confirmationUrl'] = $this->router->generate('krd_user_authorization_reset_password', array('token' => $user->getConfirmationToken()), true);

            $this->mailer->sendTemplate($message, 'KrdUserBundle:Email:resetpassword.html.twig', $context);
        }
    }

    /**
     * Пароль успешно изменен. Уведомляем пользователя письмом
     * @param  UserEvent $event
     */
    public function onResetPasswordSuccess(UserEvent $event)
    {
        $user = $event->getUser();

        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);

        $message = $this->mailer->newMessage('Ваш новый пароль на сайте %title%');
        $message->setTo($user->getEmail());

        $context = array();
        $context['user'] = $user;

        $this->mailer->sendTemplate($message, 'KrdUserBundle:Email:newpassword.html.twig', $context);

        $this->userManager->updateUser($user);
    }
}
