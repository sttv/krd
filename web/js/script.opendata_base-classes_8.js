var defaultModel = Backbone.Model.extend({
	// Приводит массив к целым числам
	arrToInt: function(name){
		var self = this;

		var arr = self.get(name);
		_.each(arr, function(value, i){
			arr[i] = parseInt(arr[i], 10);
		});

		return arr;
	},

	// Приводит массив к float`ам
	arrToFloat: function(name){
		var self = this;

		var arr = self.get(name);
		_.each(arr, function(value, i){
			arr[i] = parseFloat(arr[i], 10);
		});

		return arr;
	}
});

/**
 * Хранилище параметров поиска
 * @type {[type]}
 */
var searchParamsModel = defaultModel.extend({
	defaults:{
		text: '',
		categories: [],
		subcategories: [],
		regions: []
	}
});

/** ****************************************************************************** **/

/**
 * Представление для селектора категории поиска
 */
var searchCategoriesView = Backbone.View.extend({
	initialize: function(){
		var self = this;

		self.$itemTpl = $('#sm-fl-item-tpl');
		self.$list = self.$('.lst');
		self.collection = new searchCategoriesCollection();

		self.collection.on('add', self.onAddItem, self);
	},

	/**
	 * Добавляет категорию в список
	 * @param  {object} category Обьект категории или массив с категориями
	 */
	appendNew: function(category){
		var self = this;

		self.collection.add(category);
	},

	/**
	 * Каллбек на добавление категории
	 */
	onAddItem: function(model){
		var self = this;

		var $tpl = self.$itemTpl.tmpl(model.toJSON());
		var view = new searchCategoriesItemView({el:$tpl, model:model});

		self.$list.append($tpl);

		self.$list.find('.sm-fl-item').removeClass('last');
		self.$list.find('.sm-fl-item:nth-child(4n)').addClass('last');
	}
});

/**
 * Представление элемента в селекторе категорий
 */
var searchCategoriesItemView = Backbone.View.extend({
	initialize: function(options){
		var self = this;

		self.model = options.model;

		// Клик по кнопке
		self.$('.sm-icon, .sm-title').on('click', function(e){
			e.preventDefault();

			if (!window.socialMapSearch.searchEnabled){
				window.location.href = '/opendata/list/#!/category-'+self.model.id;
			}else{
				// self.model.set({selected:!self.model.get('selected')});
				self.model.set({selected:true});
			}

			window.socialMapSearch.resetAll();
		});

		self.model.on('change:selected', self.render, self);
	},

	/**
	 * Отрисовка состояния
	 */
	render: function(){
		var self = this;

		if (self.model.get('selected')){
			self.$el.addClass('current');
		}else{
			self.$el.removeClass('current');
		}
	}
});

/**
 * Модель для категории поиска
 */
var searchCategoriesModel = Backbone.Model.extend({
	defaults:{
		title: 'Заголовок',
		color: 'blue',
		icon: 'health',
		selected: false
	},

	initialize: function(){
		var self = this;

		self.set({
			id: parseInt(self.get('id'), 10)
		});
	}
});

/**
 * Коллекция моделей категорий поиска
 * @type {[type]}
 */
var searchCategoriesCollection = Backbone.Collection.extend({
	model: searchCategoriesModel,

	initialize: function(){
		var self = this;

		self.noOneMore();
	},

	/**
	 * Включает состояние при кором может быть выбрана только одна категория
	 */
	noOneMore: function(){
		var self = this;

		self.on('change:selected', function(model){
			if (!model.get('selected'))
				return;

			self.each(function(item){
				if (item.cid == model.cid)
					return;

				item.set({selected:false});
			});
		});
	}
});

/** ****************************************************************************** **/

/**
 * Модель одного учреждения
 */
var placeModel = defaultModel.extend({
	defaults:{
		title: '',
		categories: [],
		subcategories: [],
		region: -1,
		subcategories_txt: '',
		address: '',
		phone: '',
		href: '',
		content: '',

		yState: 'active',
		yCoords: [45.034942, 38.976032],
		yIcon: 'health',
		yColor: 'blue',
	},

	initialize: function(){
		var self = this;

		self.set({
			region: parseInt(self.get('region'), 10),
			categories: self.arrToInt('categories'),
			subcategories: self.arrToInt('subcategories'),
			yCoords: self.arrToFloat('yCoords'),
		});

		self.on('change:subcategories', self.subCatsTxtUpdate, self);
		self.subCatsTxtUpdate();
	},

	// Обновление текстового представления категорий
	subCatsTxtUpdate: function(){
		var self = this;

		var txt = [];

		_.each(self.get('subcategories'), function(subcat){
			txt.push(window.socialMapSearch.subcategoriesList[subcat]);
		});

		self.set({subcategories_txt:txt.join(', ')});
	}
});

/**
 * Коллекция учреждений
 */
var placeCollection = Backbone.Collection.extend({
	model: placeModel
});

/** ****************************************************************************** **/

/**
 * Приложение для поиска по карте
 */
var SMSearchApp = Backbone.Router.extend({
	searchEnabled: true,
	hasOne: false,

	routes:{
		'!/category-:id': 'categorySelect'
	},

	initialize: function(){
		var self = this;

		self.searchParams = new searchParamsModel();
		self.initCategories();
		self.initFilters();
		self.initTextSearch();

		// Коллекция учреждений
		self.places = new placeCollection();
		self.searchParams.on('change', self.search, self);

		// Карта
		if ($('#socialmap').length > 0){
			self.searchMap = new searchMapView({el:$('#socialmap')});
				// Совместная инициализация
				self.searchMap.on('initialized', function(){

					self.search();
					self.trigger('initialized');
				});

				// Событие по добавлению учреждения
				self.places.on('add', function(model){
					self.searchMap.trigger('placeAdd', model);
				});

                // Отрисовка меток на карте по инициализации
                self.on('full-init', function() {
                    self.searchMap.trigger('renderMap');
                });
		}

		// Список результатов поиска
		self.results = new resultsView({el:$('.sm-results')});
			// Событие по добавлению учреждения
			self.places.on('add', function(model){
				self.results.trigger('placeAdd', model);
			});

			// Событие для инициации поиска
			self.searchParams.on('change', function(){
				self.results.trigger('resortResults');

                // Сохранение параметров поиска в куках
                if (!self.searchParams.get('text') || self.searchParams.get('text') == '') {
                    $.cookie('sm_params_text', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_text');
                } else {
                    $.cookie('sm_params_text', self.searchParams.get('text'), {expires:1, path:'/'});
                }

                if (!self.searchParams.get('subcategories') || self.searchParams.get('subcategories').length == 0) {
                    $.cookie('sm_params_subcategories', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_subcategories');
                } else {
                    $.cookie('sm_params_subcategories', self.searchParams.get('subcategories').join('|'), {expires:1, path:'/'});
                }
                if (!self.searchParams.get('regions') || self.searchParams.get('regions').length == 0) {
                    $.cookie('sm_params_regions', '', {expires:-111, path:'/'});
                    $.removeCookie('sm_params_regions');
                } else {
                    $.cookie('sm_params_regions', self.searchParams.get('regions').join('|'), {expires:1, path:'/'});
                }
			});
	},

	/**
	 * Указывает на карте одно учреждение
	 */
	onePlace: function(placeId){
		var self = this;

		placeId = parseInt(placeId, 10);

		if (!placeId){
			return false;
		}

		self.searchEnabled = false;
		self.hasOne = true;

		self.places.each(function(place){  place.set({yState:'inactive'}); });
		self.places.get(placeId).set({yState:'active'});

		self.searchMap.params.set({
			center: self.places.get(placeId).get('yCoords'),
			zoom: 17
		});

		self.searchCategories.collection.get( self.places.get(placeId).get('categories')[0] ).set({selected:true});

		self.results.instantHide();
	},

	/**
	 * Инициализация строки поиска
	 */
	initTextSearch: function(){
		var self = this;

		// Текстовый инпут
		self.$textSearchInput = $('input.sm-text-search').on('textchange', function(){
			var text = $(this).val();

			!!this.__timeout && clearInterval(this.__timeout);
			this.__timeout = setTimeout(function(){
				self.searchParams.set({
					text: text
				});
			}, (text == '' ? 0 : 250));
		});

		// Кнопка очищения
		self.$textSearchClearCross = $('.sm-t-clear').on('click', function(e){
			e.preventDefault();

			self.$textSearchInput.val('').trigger('textchange');
		});

		// Скрытие этой кнопки при пустом инпуте
		self.$textSearchInput.on('textchange', function(){
			if ($(this).val() == ''){
				self.$textSearchClearCross.addClass('hidden');
			}else{
				self.$textSearchClearCross.removeClass('hidden');
			}
		});

		// Всплывающие подсказки
		self.$textSearchHintList = $('.sm-t-hint-list').on('clickoutside', function(){
			self.$textSearchHintList.hideList();
		});

		// Скрытие всплывающих подсказок
		self.$textSearchHintList.hideList = function(){
			self.$textSearchHintList.addClass('hidden');
			self.$textSearchHintList.children('a.hover').removeClass('hover');
		};

		// Ховер всплывающих подсказок
		$('.sm-t-hint-list').on('mouseover', 'a', function(){
			$('.sm-t-hint-list').children('a.hover').removeClass('hover');
			$(this).addClass('hover');
		});

		$('.sm-t-hint-list').on('mouseleave', 'a', function(){
			$('.sm-t-hint-list').children('a.hover').removeClass('hover');
		});

		// Навигация стрелками
		self.$textSearchInput.on('keyup', function(e){
			if (e.keyCode == 38 || e.keyCode == 40){
				if (!self.$textSearchHintList.is(':empty')){
					self.$textSearchHintList.removeClass('hidden');
				}

				var $curr = self.$textSearchHintList.children('.hover');

			}

			switch(e.keyCode){
				// UP
				case 38:
					var $currNew = $curr.removeClass('hover').prev('a');

					if ($curr.length == 0 || $currNew.length == 0){
						self.$textSearchHintList.children('a:last').addClass('hover');
					}else{
						$currNew.addClass('hover');
					}
				break;

				// DOWN
				case 40:
					var $currNew = $curr.removeClass('hover').next('a');

					if ($curr.length == 0 || $currNew.length == 0){
						self.$textSearchHintList.children('a:first').addClass('hover');
					}else{
						$currNew.addClass('hover');
					}
				break;

				// ENTER
				case 13:
					if (!self.$textSearchHintList.is(':empty') && !self.$textSearchHintList.hasClass('hidden')){
						self.$textSearchHintList.children('a.hover').click();
					}
				break;
			}
		});

		self.$textSearchInput.on('textchange', function(){
			var text = $(this).val();

			if (text == ''){
				self.$textSearchHintList.hideList();
			}else{
				self.$textSearchHintList.removeClass('hidden').empty();
				var counter = 0;
				self.places.each(function(place){
					var title = place.get('title');
					if (counter <= 10 && title.toLowerCase().indexOf(text) != -1){
						var $a = $('<a href="#">'+title.replace(new RegExp(text, 'ig'), '<b>$&</b>')+'</a>');

						(function($a, title){
							$a.on('click', function(e){
								e.preventDefault();

								self.$textSearchInput.val(title).trigger('textchange');
								$(this).removeClass('hover');
							});
						}($a, title));


						$a.appendTo(self.$textSearchHintList);
						counter++;
					}
				});

				if (counter == 0){
					self.$textSearchHintList.hideList();
				}
			}
		}).on('dblclick', function(){
			if (!self.$textSearchHintList.is(':empty')){
				self.$textSearchHintList.removeClass('hidden');
			}
		});

        var _cached = $.cookie('sm_params_text');
        if (!!_cached && _cached != '') {
            self.on('full-init', function() {
                self.$textSearchInput.val(_cached).trigger('textchange');
                self.$textSearchHintList.hideList();
            });
        }
	},

	/**
	 * Инициализация селектора категорий
	 */
	initCategories: function(){
		var self = this;

		self.searchCategories = new searchCategoriesView({el:$('.sm-filter-list')});

		self.searchCategories.collection.on('change:selected', function(){
			var ids = [];
			var selectedList = self.searchCategories.collection.where({selected:true});
			_.each(selectedList, function(item){
				ids.push(item.get('id'));
			});

			self.searchParams.set({categories:ids});
            $('.sm-r-pages .export.button').attr('href', '#!/export-' + ids[0]);
		});

		// Список наименований категорий и подкатегорий
		self.categoriesList = {};
		self.subcategoriesList = {};
		self.searchCategories.collection.on('add', function(model){
			self.categoriesList[model.get('id')] = model.get('title');

			_.each(model.get('subcategories'), function(subcat){
				self.subcategoriesList[subcat.id] = subcat.title;
			});
		});

		// Роут смены выбранной категории
		self.on('route:categorySelect', function(id){
			self.searchCategories.collection.mustBe = id;
			self.searchCategories.collection.get(id).set({selected:true});
		});

		// Выбор первой категории поумолчанию
		self.on('full-init', function(){
			if (self.searchEnabled && self.searchCategories.collection.where({selected:true}).length == 0){
				self.navigate('!/category-'+self.searchCategories.collection.at(0).get('id'), {trigger:true});
			}
		});
	},

	/**
	 * Инициализация под-фильтров
	 */
	initFilters: function(){
		var self = this;

		// Фильтр подкатегорий
		self.$selectSubcategory = $('.sm-filter-select select[name="subcategory"]').on('change', function(){
			if ($(this).val() != -1){
				self.searchParams.set({
					subcategories: [parseInt($(this).val(), 10)]
				});
			}else{
				self.searchParams.set({subcategories: []});
			}
		});

		self.searchParams.on('change:categories', self._fillSubcategories, self);
		self._fillSubcategories();

        var _cached = $.cookie('sm_params_subcategories');
        if (!!_cached && _cached != '') {
            _cached = parseInt(_cached.split('|').pop(), 10);
            self.on('full-init', function() {
                self.$selectSubcategory.val(_cached).trigger('change');
            });
        }

		// Фильтр районов
		self.$selectRegion = $('.sm-filter-select select[name="region"]').on('change', function(){
			var value = $(this).val();
			if (value != -1){
				self.searchParams.set({
					regions: [parseInt(value, 10)]
				});
			}else{
				self.searchParams.set({regions: []});
			}
		});

		// Убираем регионы, в которых нет ни одного учреждения
		self.on('full-init', function(){
			var allowedRegions = [];
			self.places.each(function(place){
				allowedRegions.push(place.get('region'));
			});

			self.$selectRegion.find('option').each(function(){
				var $this = $(this),
					id = parseInt($this.attr('value'), 10);
				if (id != -1 && $.inArray(id, allowedRegions) === -1){
					$this.remove();
				}
			});
		});

        var _cached = $.cookie('sm_params_regions');
        if (!!_cached && _cached != '') {
            _cached = parseInt(_cached.split('|').pop(), 10);
            self.on('full-init', function() {
                self.$selectRegion.val(_cached).trigger('change');
            });
        }

		// Кнопка сброса фильтра
		self.$filterResetBtn = $('.reset-filter').on('click', function(e){
			e.preventDefault();

			self.$selectSubcategory.val(-1).change();
			self.$selectRegion.val(-1).change();
		}).hide();

		// При смене глобальной категории - сбрасываем фильтр подкатегорий
		self.searchParams.on('change:categories', function(){
			self.searchParams.set({subcategories:[]});
		});

		// При смене фильтрации показываем/скрываем кнопку
		self.searchParams.on('change:subcategories change:regions', function(){
			if (self.searchParams.get('subcategories').length == 0 && self.searchParams.get('regions').length == 0){
				self.$filterResetBtn.stop().css({opacity:1}).fadeOut(200);
			}else{
				self.$filterResetBtn.stop().css({opacity:1}).fadeIn(200);
			}
		});
	},

	/**
	 * Заполняет список подкатегорий
	 */
	_fillSubcategories: function(){
		var self = this;

		self.$selectSubcategory.empty();
		self.$selectSubcategory.append('<option value="-1">-- Выберите подраздел --</option>');

		_.each(self.searchCategories.collection.where({selected:true}), function(item){
			_.each(item.get('subcategories'), function(subcat){
				self.$selectSubcategory.append('<option value="'+subcat.id+'">'+subcat.title+'</option>');
			});
		});
	},

	/**
	 * Сброс всех параметров поиска
	 */
	resetAll: function(){
		var self = this;

		self.$selectSubcategory.val(-1).change();
		self.$selectRegion.val(-1).change();
		self.$textSearchInput.val('').trigger('textchange');
	},

	/**
	 * Добавляет новое учреждение
	 */
	appendPlace: function(place){
		var self = this;

		if (!place) return;

		if ($.isArray(place)){
			_.each(place, self.appendPlace, self);
		}else{
			self.places.add(place);
		}
	},

	/**
	 * Добавляет категорию поиска
	 */
	appendCategory: function(category){
		var self = this;

		self.searchCategories.appendNew(category);
	},

	/**
	 * Добавляет регион города в список фильтров
	 */
	appendRegion: function(region){
		var self = this;

		if (!region) return;

		if ($.isArray(region)){
			_.each(region, self.appendRegion, self);
		}else{
			self.searchMap.appendRegion(region);
			self.$selectRegion.append('<option value="'+region.id+'">'+region.title+'</option>');
		}
	},

	/**
	 * Производит поиск простым перебором коллекции
	 */
	search: function(){
		var self = this;

		if (!self.searchEnabled){
			return false;
		}

		var selectedCategories = self.searchParams.get('categories');
		var selectedSubCategories = self.searchParams.get('subcategories');
		var selectedRegions = self.searchParams.get('regions');
		var selectedText = $.trim(self.searchParams.get('text').toLowerCase());

		// Перебираем все учреждения
		self.places.each(function(place){
			// Сравнение категории
			var categoriesActivity = true;
			if (selectedCategories.length > 0){
				categoriesActivity = false;
				var placeCategories = place.get('categories');

				for(var i = 0; i < placeCategories.length; i++){
					if ($.inArray(placeCategories[i], selectedCategories) != -1){
						categoriesActivity = true;
						break;
					}
				}
			}

			// Сравнение подкатегории
			var subcatActivity = true;
			if (selectedSubCategories.length > 0){
				subcatActivity = false;

				var placeSubCategories = place.get('subcategories');

				for(var i = 0; i < placeSubCategories.length; i++){
					if ($.inArray(placeSubCategories[i], selectedSubCategories) != -1){
						subcatActivity = true;
						break;
					}
				}
			}

			// Сравнение местоположения
			var regionActivity = true;
			if (selectedRegions.length > 0){
				regionActivity = false;

				if ($.inArray(place.get('region'), selectedRegions) != -1){
					regionActivity = true;
				}
			}

			// Поиск по текстовому запросу
			var textActivity = true;
			var searchIn = ['title', 'address', 'phone', 'content'];
			if (selectedText != ''){
				textActivity = false;

				_.each(searchIn, function(parameter){
					if (place.get(parameter).toLowerCase().indexOf(selectedText) != -1){
						textActivity = true;
						return false;
					}
				});
			}

			place.set({
				yState: (categoriesActivity && subcatActivity && regionActivity && textActivity) ? 'active' : 'inactive'
			});
		});

		self.searchMap.trigger('highlight');
	}
});
