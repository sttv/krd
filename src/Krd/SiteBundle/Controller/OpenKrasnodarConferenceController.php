<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Конференция «Открытый Краснодар: общественный транспорт»
 */
class OpenKrasnodarConferenceController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $formBuilder = $this->getCForm();
        $formBuilder->setAction($this->get('router')->generate('krd_okconference_submit'));
        $formView = $formBuilder->getForm()->createView();

        return array('form' => $formView);
    }

    /**
     * Сабмит формы подписки на новости
     *
     * @Route("/ajax/krd/site/okconference/form/submit/", name="krd_okconference_submit")
     */
    public function submitAction(Request $request)
    {
        $formBuilder = $this->getCForm();
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $data = $form->getData();

        if ($data) {
            $mailer = $this->container->get('qcore.mailer');

            $message = $mailer->newMessage('Заявка на выступление на конференции с сайта %title%');
            $message->setTo(array('open_krasnodar@mail.ru', 'bchernakov@krd.ru', '2672230@mail.ru'));

            $mailer->sendTemplate($message, 'KrdSiteBundle:OpenKrasnodarConference:email/request.html.twig', $data);

            return new JsonResponse(array('success' => true));
        } else {
            $this->get('logger')->err('Ошибка при получении данных формы OpenKrasnodarConferenceController. Результат формы не массив.');
            return new JsonResponse(array('success' => false));
        }
    }

    /**
     * Фронтэнд форма для заявки на участие в конференции
     *
     * @return FormBuilder
     */
    public function getCForm()
    {
        $builder = $this->get('form.factory')
            ->createNamedBuilder('okconference', 'form', array(), array('csrf_protection' => true));

        $name = $builder->create('name', 'text', array(
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли указать Имя')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $lastname = $builder->create('lastname', 'text', array(
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли указать Фамилию')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $midname = $builder->create('midname', 'text', array(
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли указать Отчество')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $email = $builder->create('email', 'text', array(
            'required' => true,
            'constraints' => array(
                new Assert\Email(array('message' => 'Email имеет не верный формат')),
                new Assert\NotBlank(array('message' => 'Вы забыли указать Email')),
            ),
        ));

        $phone = $builder->create('phone', 'text', array(
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли указать Телефон')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $theme = $builder->create('theme', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли описать тему выступления')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($name);
        $builder->add($lastname);
        $builder->add($midname);
        $builder->add($email);
        $builder->add($phone);
        $builder->add($theme);

        return $builder;
    }
}
