<?php

namespace Q\CoreBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Loader\AnnotationFileLoader;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;


class RoutingListener
{
    protected $kernel;
    protected $router;
    protected $currentNode;
    protected $annotationFileReader;
    protected $securityContext;
    protected $em;

    public function __construct(\AppKernel $kernel, Router $router, CurrentNode $currentNode, AnnotationFileLoader $annotationFileReader, SecurityContext $securityContext, EntityManager $em)
    {
        $this->kernel = $kernel;
        $this->router = $router;
        $this->currentNode = $currentNode;
        $this->annotationFileReader = $annotationFileReader;
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    /**
     * Роутинг на основе нодов
     * @param  GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();

        if ($request->attributes->has('_controller')) {
            return;
        }

        $redirect = $this->em->getRepository('QCoreBundle:Redirect')->getByRequest($request);

        if ($redirect) {
            $event->setResponse(new RedirectResponse($redirect->getRedirect()));
            return;
        }


        if (!$this->currentNode->hasCurrentPath()) {
            $this->currentNode->setCurrentPath(mb_strtolower($request->getPathInfo()));
        }

        if (!$this->currentNode->hasNode()) {
            return;
        }

        if ($this->currentNode->getNode()->hasRedirect()) {
            $r = $this->currentNode->getNode()->getRedirect();

            if ($r == '#') {
                throw new NotFoundHttpException();
            }

            $event->setResponse(new RedirectResponse($r));
            return;
        }

        if ($this->currentNode->getNode()->hasUrlAlias() && $this->currentNode->getCurrentPath().'/' == $this->currentNode->getNode()->getUrl(false)) {
            $r = $this->currentNode->getNode()->getUrlAlias();
            if ($request->getQueryString()) {
                $r .= '?'.$request->getQueryString();
            }
            
            $event->setResponse(new RedirectResponse($r));
            return;
        }

        if ($this->currentNode->getNode()->isSsl() && $request->getScheme() != 'https') {
            $event->setResponse(new RedirectResponse('https://'.$request->getHost().$request->getRequestUri()));
            return;
        }

        try{
            list($bundle, $controller) = explode(':', $this->currentNode->getNode()->getController()->getController());
            $controllerFilePath = $this->kernel->locateResource($bundle).'Controller/'.basename($controller).'.php';

            $routeCollection = $this->annotationFileReader->load($controllerFilePath);
            $routeCollection->addPrefix($this->currentNode->getNode()->getUrl(true));
        } catch (\Exception $e) {
            return;
        }

        $matcher = new UrlMatcher($routeCollection, $this->router->getMatcher()->getContext());

        try {
            $params = $matcher->match($this->currentNode->getCurrentPath().'/');
        } catch (ResourceNotFoundException $e) {
            try {
                $params = $matcher->match($this->currentNode->getCurrentPath());
            } catch (ResourceNotFoundException $e) {
                return;
            }
        }

        $params['node'] = $this->currentNode->getNode();

        $request->attributes->add($params);
        unset($params['_route']);
        unset($params['_controller']);
        $request->attributes->set('_route_params', $params);
    }

    /**
     * Событие перед передачей управления в контроллер
     * Проверка разрешения на просмотр текущего нода
     * Для проверки текущий контроллер должен наследовать ActiveSecuredController
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if ($this->securityContext->getToken()) {
            /** @var User $user */
            if (($user = $this->securityContext->getToken()->getUser()) && $user instanceof User) {
                if ($user->isLocked() || !$user->isEnabled() || $user->isExpired()) {
                    $this->securityContext->setToken(null);

                    if ($session = $event->getRequest()->getSession()) {
                        $session->invalidate();
                    }
                }
            }
        }

        if (!is_array($controller) || !($controller[0] instanceof ActiveSecuredController)) {
            return;
        }

        if (!$this->currentNode->hasNode() || $this->securityContext->isGranted('ROLE_VIEW_ALL')) {
            return;
        }


        $node = $this->currentNode->getNode();


        if (!$this->em->getRepository('QCoreBundle:Node')->isFullyActive($node)) {
            if (method_exists($controller[0], 'notFoundAction')) {
                $controller[1] = 'notFoundAction';
                $event->setController($controller);
            } else {
                throw new NotFoundHttpException('Not found');
            }
        }
    }

    /**
     * Убираем флаг новичка пользователя если он есть
     * @param  FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $user = false;
        $response = $event->getResponse();

        if (!($response instanceof RedirectResponse)) {
            if ($token = $this->securityContext->getToken()) {
                $user = $token->getUser();
            }

            if (($user instanceof User) && ($user->getNewbie())) {
                $user->setNewbie(false);
                $this->em->flush($user);
            }
        }
    }
}
