<?php

namespace Krd\ImportBundle\Listener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Krd\DocumentBundle\Entity\Document;
use Krd\ImportBundle\Old\RemoteLoader;


/**
 * Listener для сущностей Document
 * При загрузке сущности, если требуется обновляем контент с сервера КРД
 */
class NormaCacheListener
{
    protected $remote;
    protected $disabled = true;

    public function __construct(RemoteLoader $remote)
    {
        $this->remote = $remote;
    }

    /**
     * Отключение обновления норм
     */
    public function disable()
    {
        $this->disabled = true;
    }

    /**
     * Включение обновления норм
     */
    public function enable()
    {
        $this->disabled = false;
    }


    public function postLoad(LifecycleEventArgs $args)
    {
        if ($this->disabled) {
            return;
        }

        $entity = $args->getEntity();

        if ($entity instanceof Document && ($entity->getAutoUpdate() || $entity->getForceUpdate()) && $entity->hasHash()) {

            if ($entity->getForceUpdate() || (time() - (int)$entity->getUpdated()->format('U') > 3600)) {
                $html = $this->remote->getNormaDetail($entity->getHash());
                $entity->setContent('<p>'.trim($entity->getAnnounce(),'"').'</p>'.$html);
                $entity->setForceUpdate(false);
                $args->getEntityManager()->flush();
            }
        }
    }
}
