/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader.files', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="files-uploader abstract-uploader">'
            + '<div class="files-list clearfix">'
                + '<files-list-item file="file" ng-repeat="file in files"></files-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Файлы не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить файлы</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.files', template);
    }])

    /**
     * Элемент загрузчика файлов
     */
    .directive('filesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item">'
                    + '<input type="text" ng-model="file.title" class="input-title">'
                    + '<input type="text" ng-model="file.date" class="input-date">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-small" tooltip="Скачать {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-danger btn-small" tooltip="Удалить" ng-click="remove()"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление файла';
                    var msg = 'Вы уверены, что хотите удалить файл?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Обновление полей
                 */
                var fst = 0;
                $scope.updateFields = function() {
                    if (fst < 2) {
                        fst++;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {title: $scope.file.title, date: $scope.file.date},
                        timeout: this.canceler.promise
                    });
                };
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', $scope.updateFields);

                // Редактирование даты файла
                $scope.$watch('file.date', $scope.updateFields);
            }
        }
    }])
;
