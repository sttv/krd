<?php

namespace Krd\CommentsBundle\Navigation;

use Q\CoreBundle\Navigation\Item;


/**
 * Пункт меню управления комментариями
 */
class Comments extends Item
{
    public function getTitle()
    {
        return 'Комментарии';
    }

    public function getLink()
    {
        return $this->router->generate('cms_krd_comments_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS');
    }
}
