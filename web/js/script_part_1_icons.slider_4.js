angular.module('icons.slider', [])
    /**
     * Слайдер изображений/видео новости
     */
    .directive('iconsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowPrev = $('<div class="arrow prev" ng-click="prev()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-prev"></i><i class="icon-arrow-prev-mini"></i></div>');
                var $arrowNext = $('<div class="arrow next" ng-click="next()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-next"></i><i class="icon-arrow-next-mini"></i></div>');

                $element.append($arrowPrev).append($arrowNext);

                return function($scope, $element) {
                    $scope.$items = $element.children('.item');

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$items.eq(active);
                        $scope.$items.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                }
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.next = function() {
                    if ($scope.active < $scope.$items.length - 1) {
                        $scope.active++;
                    } else {
                        $scope.active = 0;
                    }
                };

                $scope.prev = function() {
                    if ($scope.active > 0) {
                        $scope.active--;
                    } else {
                        $scope.active = $scope.$items.length - 1;
                    }
                };
            }
        }
    }])
;
