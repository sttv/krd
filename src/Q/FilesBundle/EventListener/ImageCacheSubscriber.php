<?php

namespace Q\FilesBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\ImageCache;
use Q\FilesBundle\Manager\ImageCacheUrl;


class ImageCacheSubscriber implements EventSubscriber
{
    protected $manager;
    protected $uploadDir;

    public function __construct(ImageCacheUrl $manager, $uploadDir)
    {
        $this->manager = $manager;
        $this->uploadDir = $uploadDir;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postLoad',
            'postUpdate',
            'postRemove',
        );
    }

    /**
     * Вставка новых картинок - автоматически заполняем размеры
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->postLoad($args);
    }

    /**
     * Для всех изображений автоматически генерируем ссылки на альтернативные размеры
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Image) {
            try{
                $entity->setSize($this->manager->getAllImageCacheUrl($entity));
            } catch (\Exception $e) {
                //
            }
        }
    }

    /**
     * Удаление кешированного файла изображения
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Image) {
            $file = $entity->getFile();
            @unlink($this->uploadDir.$file['path']);
        } elseif ($entity instanceof ImageCache) {
            $file = $entity->getFile();
            @unlink($this->uploadDir.$file['path']);
        }
    }
}
