<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль сообщений об ошибке
 */
class Senderror extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/senderror.html.twig');
    }
}


