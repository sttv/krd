/**
 * Поиск на сайте
 */
angular.module('search', [])
    .directive('searchMoreBtn', ['$http', function($http) {
        return {
            restrict: 'A',
            scope: {
                url: '@searchMoreBtn'
            },

            link: function($scope, $element, attrs) {
                if (!!attrs.moreBtnBody) {
                    $scope.$listBody = $('#'+attrs.moreBtnBody);
                } else {
                    $scope.$listBody = $element.prev('.items-list');
                }

                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $scope.nextPage();
                    });
                });

                $scope.loading = false;
                $scope.currentPage = 1;
                $scope.originalHtml = $element.html();

                $scope.$watch('loading', function(loading) {
                    if (loading) {
                        $element.html('Загрузка...');
                    } else {
                        $element.html($scope.originalHtml);
                    }
                });
            },

            controller: function($scope, $element) {
                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    // В ответ не смотрян и на что приходит html
                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if ($(response).find('#search-results').data('has-more') == undefined) {
                            $element.hide();
                        }

                        $scope.$listBody.append($(response).find('#search-results').html());

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])
;
