<?php

namespace Q\CoreBundle\Form\Tools;

use Symfony\Component\Form\Form;


/**
 * Рекурсивный сбор ошибок формы
 */
class ErrorHelper
{

    /**
     * Полный список ошибок валидации формы
     * в виде ассоциативного массива с ключами по полям формы
     * @param  Form $form
     * @return array
     */
    public function getErrors(Form $form)
    {
        $result = array();

        foreach ($form->getErrors() as $error) {
            $result[] = $error->getMessage();
        }

        foreach ($form as $child) {
            if (!($child instanceof Form)) {
                continue;
            }

            $errors = $this->getErrors($child);

            if (!empty($errors)) {
                $selfKey = (string)$form->getPropertyPath();
                $childKey = (string)$child->getPropertyPath();

                if (!preg_match('/^\[.*\]$/', $childKey)) {
                    $childKey = '['.$childKey.']';
                }

                $key = $selfKey.$childKey;
                $result = array_merge($result, array($key => (array)$errors));
            }
        }

        foreach ($result as $key => $val) {
            if (preg_match('/^\[.+\]$/', $key) && is_array($val)) {
                foreach ($val as $vk => $__unused) break;
                if (mb_strpos($vk, trim($key, '[] ')) === 0) {
                    unset($result[$key]);

                    foreach ($val as $k => $v) {
                        $result[$k] = $v;
                    }
                }
            }

            if (preg_match('/]$/', $key) && is_array($val)) {
                preg_match_all('/\[([a-zA-Z0-9_]+)]$/U', $key, $matches);

                if (isset($matches[1]) && isset($matches[1][0])) {
                    $fName = $matches[1][0];
                } else {
                    continue;
                }

                $tempResult = array();

                foreach ($val as $valKey => $valVal) {
                    if (mb_strpos($valKey, $fName) === 0 && is_array($valVal)) {
                        preg_match_all('/^[a-zA-Z0-9_]+\[([a-zA-Z0-9_]+)]$/U', $valKey, $matches);
                        if (isset($matches[1]) && isset($matches[1][0])) {
                            $sName = $matches[1][0];

                            $realName = $key.'['.$sName.']';
                            $tempResult[$realName] = $valVal;
                        }
                    }
                }

                if ((count($tempResult) == count($val)) && count($tempResult > 0)) {
                    unset($result[$key]);
                    $result = array_merge($result, $tempResult);
                }
            }
        }

        return $result;
    }
}
