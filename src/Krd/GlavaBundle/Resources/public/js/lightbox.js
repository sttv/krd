/**
 * @author User
 */
angular.module('glava.lightbox',[])
	.directive('lightBox', function($timeout) {
		return {
			restrict : 'A',
			scope : true,
			
			template : function($element) {
				return '<div class="glava-flybox" ng-class="{disableprev: current==0, disablenext: current===items.length-1}">'
			+'<div class="glava-flybox-big-preview">'
				+'<div class="preview-count"><span>{{ current + 1 }}</span>/{{ items.length }}</div>'
				+'<div class="preview-title">{{ previewTitle }}</div>'
				+'<a class="preview-download disable-fancybox" href="/ajax/glava/gallery/download/?r={{ originalUrl }}">'
					+'<div class="dw-title">скачать оригинал</div>'
					+'<div class=""><b>{{ previewExt }}</b> <span>{{ previewDims }}px</span> <span>({{ previewSize|filesize }})</span></div>'
				+'</a>'
				+'<div class="preview-social">'
					+'<div class="item dd-list">'
			           +'<i class="icon-share-panel-share" title="Рассказать друзьям"></i>'
			           +'<div class="dd-wrap">'
			               +'<i class="icon-share-dd-share"></i>'
							+'<div class="yashare-auto-init" id="ya_share{{ $id }}">'
							+'</div>'
			            +'</div>'
			        +'</div>'
				+'</div>'
				+'<div class="arrow left" ng-click="prev()"><i></i></div>'
				+'<div class="arrow right" ng-click="next()"><i></i></div>'
				+'<div class="glava-flybox-preview" ng-class="{loading:loading}">'
					+'<img ng-src="{{ previewUrl }}" />'
					+'<div class="loading"></div>'
				+'</div>'
			+'</div>'
			+'<div class="glava-flybox-small-preview">'
				+'<div class="arrow left" ng-click="prev()"><i></i></div>'
				+'<div class="arrow right" ng-click="next()"><i></i></div>'
				+'<div class="glava-flybox-slider clearfix"><div class="glava-flybox-preview-list flow clearfix"><div class="viewport clearfix">'+$element.html()+'</div></div></div>'
			+'</div>'
		+'</div>';
			},
			
			link : function($scope, $element, attrs) {
				$scope.$items = $element.find('.glava-flybox-slider .item');
				$scope.items = [];
				$scope.previewTitle = "preview-title";
				$scope.originalUrl = '';
				$scope.previewUrl = '';
				$scope.fancyboxUrl = '';
				$scope.previewDms = '0x0';
				$scope.previewSize = '0.0 mb';
				$scope.previewExt = '';
				$scope.previewExt2 = '';
				$scope.current = parseInt($element.data('current')) || 0;

				$scope.$flow = $element.find('.flow');
				$scope.$viewport = $element.find('.viewport');
				$scope.$slider = $element.find('.glava-flybox-slider');
				$scope.noShare = attrs.noshare;
				
				$timeout(function(){
					$scope.share = new Ya.share({
						element: 'ya_share' + $scope.$id,
						elementStyle: {
							'type': 'none',
							'border': false,
							'quickServices': ['twitter', 'facebook', 'vkontakte']
						}
					});
				}, 20);
				
				$scope.$items.each(function(i) {
					$scope.items.push({
						preview : $(this).attr('preview'),
						fancybox : $(this).attr('fancybox'),
						socialimg : $(this).attr('socialimg'),
						original : $(this).attr('original'),
						title : $(this).attr('title'),
						size : parseInt($(this).attr('size')),
						dims : $(this).attr('dimension')
					});
					$(this).on('click', function() {
						$scope.$apply(function() {
							$scope.setCurrent(i);
						})
					});
				});
				
				$scope.$watch('current', function(current) {
					if( current > $scope.items.length -1 ) {
						return;
					}
					if( current < 0 ) {
						return;
					}
					
					$timeout(function() {
						$scope.share.updateShareLink(window.location.href+'detail-'+$element.data('id')+'.html', $scope.previewTitle, {
							facebook : {
								image : 'http:' + item.socialimg
							},
							twitter : {
								image : 'http:' + item.socialimg
							},
							vkontakte : {
								image : 'http:' + item.socialimg
							}
						});
					}, 20);
					
					$scope.$items.removeClass('active');
					var activeItem = $scope.$items.eq(current);
					activeItem.addClass('active');
					
					var item = $scope.items[current];
					$scope.previewUrl = 'http:'+item.preview;
					$scope.previewTitle = item.title.replace(/\&quot;/gi, '"');
					$scope.previewDims = item.dims;
					$scope.previewSize = item.size;
					$scope.originalUrl = item.original;
					$scope.previewExt = item.preview.replace(/(.*)\.([a-z]+)$/gi, '.$2');
					
					$scope.scrollItems(current);
				});
				
				$scope.$watch('previewUrl', function() {
					$scope.loading = true;
				});
				
				$scope.$watch('originalUrl', function(url) {
					var i = new Image();
					i.onload = function() {
						$scope.previewDims = i.width+'x'+i.height;
					}
					i.src = 'http:'+url;
				});
				
				$element.find('.glava-flybox-preview img').load(function() {
					$scope.loading = false;
					$element.find('.glava-flybox-preview').stop().animate({
						height : $(this).height()
					})
				})
				
				$element.find('.preview-social .item.dd-list').hover(function() {
					$(this).children('.dd-wrap').show();
				}, function() {
					$(this).children('.dd-wrap').hide();
				})
			},
			
			controller : function($scope, $element) {
				$scope.next = function() {
					$scope.current++;
				};
				
				$scope.prev = function() {
					$scope.current--;
				}
				
				$scope.setCurrent = function(index) {
					$scope.current = index;
				}
				
				$scope.offsetof = function(index) {
					var $item = $scope.$viewport.find('.item').eq(index);
					return $item[0] ? $item.position()['left'] : 0;
				}
				
				$scope.scrollItems = function(index) {
					var offset = $scope.offsetof(index) - $scope.offsetof(0);
					if ($scope.$viewport.width() - offset < $scope.$slider.width()) {
						offset = Math.max(0, $scope.$viewport.width() - $scope.$slider.width());
					}
					
					$scope.$flow.stop().animate({
						marginLeft : -offset
					}, 400);
				};
			}
		};
	});
