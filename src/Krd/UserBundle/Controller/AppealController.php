<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;

use Q\CoreBundle\Controller\ActiveSecuredController;
use Krd\AppealBundle\Entity\Appeal;


/**
 * Интернет-приемная в личном кабинете
 */
class AppealController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/", name="krd_user_appeal")
     * @Template("KrdUserBundle:Index:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($removeId = $request->get('remove')) {
            $this->get('krd.user.modules.appeal')->removeMyDraft($removeId);
        }
    }

    /**
     * @Route("/ajax/user/appeal/list-{parent}.json", name="ajax_user_appeal_list", defaults={"_format"="json"}, requirements={"parent"="^\d+$"})
     * @Method({"GET"})
     */
    public function getItemsAction($parent)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            throw $this->createNotFoundException();
        }

        $list = $this->get('krd.user.modules.appeal')->getItemsList($parent);

        $content = $this->get('jms_serializer')->serialize($list, 'json', SerializationContext::create()->setGroups(array('user')));
        $response = new Response($content);
        $response->headers->set('Content_Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/view/{id}", requirements={"id"="^\d+$"}, name="krd_user_appeal_view")
     * @Method({"GET"})
     * @Template()
     */
    public function detailAction($id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $appeal = $this->getDoctrine()->getManager()->getRepository('KrdAppealBundle:Appeal')->findOneBy(array('id' => $id, 'createdBy' => $this->get('security.context')->getToken()->getUser()->getId()));

        if (!($appeal instanceof Appeal)) {
            throw $this->createNotFoundException();
        }

        if ($appeal->isDraft()) {
            $data = array();

            if ($parent = $appeal->getParent()) {
                $data['parent'] = $parent->getId();
            } else {
                throw $this->createNotFoundException();
            }

            if ($destination = $appeal->getDestination()) {
                $data['destination'] = $destination->getId();
            }

            $data['name'] = $appeal->getName();
            $data['lastname'] = $appeal->getLastname();
            $data['midname'] = $appeal->getMidname();
            $data['phone'] = $appeal->getPhone();
            $data['email'] = $appeal->getEmail();
            $data['postcode'] = $appeal->getPostcode();
            $data['city'] = $appeal->getCity();
            $data['district'] = $appeal->getDistrict();
            $data['street'] = $appeal->getStreet();
            $data['house'] = $appeal->getHouse();
            $data['room'] = $appeal->getRoom();
            $data['message'] = $appeal->getMessage();

            $this->get('session')->getFlashBag()->set('appeal.form.data', array('appeal' => $data));

            return $this->redirect($parent->getUrl(true));
        } else {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $this->get('translator')->trans('Ваше обращение'), 'url' => '#'));

            return array('appeal' => $appeal);
        }
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
