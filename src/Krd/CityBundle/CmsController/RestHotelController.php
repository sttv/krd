<?php

namespace Krd\CityBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * @Route("/rest/krd/city/hotel")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestHotelController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdCityBundle:Hotel';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_city_hotel_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_city_hotel_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_city_hotel", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->addOrderBy('e.sort');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_city_hotel_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $res = parent::createAction($request);

        if (isset($res['entity'])) {
            $res['entity']->setSort($this->getRepository()->getNextSort());
        }

        $this->getDoctrine()->getManager()->flush();

        return $res;
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_city_hotel_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_city_hotel_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_city_hotel_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        if (($parent = $request->get('parent')) && ($parent = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->find($parent))) {
            $entity->setParent($parent);
        }
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_city_hotel_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Сортировка элемента
     *
     * @Route("/sort/{order}", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function actionSortOfParent(Request $request, $order)
    {
        return parent::actionSortOfParent($request, $order);
    }

    /**
     * Перемещение
     *
     * @Route("/move/{id}", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function moveAction(Request $request, $id)
    {
        $nodeId = $request->request->get('node');
        $movetype = $request->request->get('movetype', 'last');

        return parent::actionMoveEntity($id, $nodeId, $movetype);
    }
}
