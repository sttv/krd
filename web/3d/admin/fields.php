<?
include_once 'conf.php';
$page = 4;

$auth = new auth();
if (!$auth->check()){
	header('Location: index.php');
    exit;
}

$db = mysql::getInstance();
$field = new fields();
switch($_REQUEST['action']) {
    case 'add':
		if (isset($_REQUEST['send'])) {
			if ($field->add($_REQUEST['title'])) {
				header('Location: fields.php');
				exit;
			}
		}
		$addact = 1;
		$titleForm = "Новое поле для объектов";
    case 'edit':
        if (!isset($addact)) {
			$titleForm = "Изменить поле для объектов";
            $fid = intval($_REQUEST['fid']);
            if (isset($_REQUEST['send']) && $fid > 0) {
                if ($field->update($fid, $_REQUEST['title'])) {
                    header('Location: fields.php');
                    exit;
                }
            } else {
                $data = $field->get_field($fid);
                $_REQUEST['title'] = $data['title'];
            }
        }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1><?=$titleForm?></h1>
					<hr>
					<form method="post">
						<div class="control-group">
							<?
							if (isset($_SESSION['error'])) {
								echo $_SESSION['error'];
							unset($_SESSION['error']);
							}?>
						</div>
						<div class="control-group">
							<label for="title">Название:</label>
							<input id="title" type="text" name="title" value="<?=@$_REQUEST['title']?>" maxlength="50" class="span6">
						</div>
						<div class="control-group">
							<input type="hidden" name="action" value="<?=$_REQUEST['action']?>" />
							<input type="hidden" name="fid" value="<?=$fid?>" />
							<input type="hidden" name="send" value="1" />
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary input-large">Сохранить</button>&nbsp;&nbsp;<a href="fields.php" class="btn">Отмена</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
        <?
        break;
    case 'fdel':
        if (isset($_REQUEST['fid'])){
            $fid = intval($_REQUEST['fid']);
            $field->delete($fid);
            header('Location: fields.php');
            exit;
        }
    default:
		$fields = array();
        $query = $db->query("SELECT * FROM fields ORDER BY id ASC");
        while($row = mysql_fetch_assoc($query)){
            $fields[$row['id']] = $row;
        }
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
			.table th.text-center, td.text-center {
				text-align: center;   
			}
        </style>
    </head>
    <body>
		<div class="container">
			<?include_once 'menu_template.php';?>
			<div class="row">
				<div class="span12">
					<h1>Поля для объектов</h1>
					<ul class="nav nav-pills">
						<li><a href="fields.php?action=add">Новое поле для объектов</a></li>
					</ul>
					<hr>
						<form action="" method="post">
							<div class="control-group">
								<?
								if (isset($_SESSION['error'])) {
									echo $_SESSION['error'];
								unset($_SESSION['error']);
								}?>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<tbody>
									<tr>
										<th>Название</th>
										<th>Действие</th>
									</tr>
									<?
									if (count($fields) > 0) {
										foreach($fields as $fid => $field){
											echo '<tr>';
											echo '<td>'.$field['title'].'</td>';
											echo '<td><a href="fields.php?action=edit&fid='.$fid.'"><i class="icon-pencil"></i>&nbsp;Edit</a>&nbsp;&nbsp;&nbsp;<a href="fields.php?action=fdel&fid='.$fid.'"><i class="icon-trash"></i>&nbsp;Delete</a></td>';
											echo '</tr>';
										}
									} else {
										echo '<tr><td colspan="2">Нет ни одного поля для объектов</td></tr>';
									}
									?>
								</tbody>
							</table>
						</form>
				</div>
			</div>
		</div>
    </body>
</html>
        <?
        break;
}
