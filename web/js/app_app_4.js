angular.module('cms', ['entity', 'ui.bootstrap', 'utils'])
    .config(['$locationProvider', function($locationProvider){
		$locationProvider.hashPrefix('!');
		$locationProvider.html5Mode(false);
	}]);
