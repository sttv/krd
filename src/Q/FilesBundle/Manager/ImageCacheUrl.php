<?php

namespace Q\FilesBundle\Manager;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Q\FilesBundle\Entity\Image;


/**
 * Менеджер URL кешированных изображений
 */
class ImageCacheUrl
{
    /**
     * Symfony router
     */
    protected $router;

    /**
     * Методы изменения размера изображений
     * q_files.image_cache.sizes
     */
    protected $cacheSizes;

    /**
     * Ключ для создания подписи
     * q_files.image_cache.secret
     */
    protected $cacheSecret;

    /**
     * Domain base url
     */
    protected $baseUrl;

    public function __construct(Router $router, array $cacheSizes, $cacheSecret, $baseUrl)
    {
        $this->router = $router;
        $this->cacheSizes = $cacheSizes;
        $this->cacheSecret = $cacheSecret;

        if (mb_strpos($baseUrl, 'https') === 0) {
            $baseUrl = mb_substr($baseUrl, 6);
        } elseif (mb_strpos($baseUrl, 'http') === 0) {
            $baseUrl = mb_substr($baseUrl, 5);
        }

        $this->baseUrl = $baseUrl;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Получение массива всех возможных ссылок для изображения
     * @param  Image  $image
     * @return array
     */
    public function getAllImageCacheUrl(Image $image)
    {
        $result = array();

        foreach($this->cacheSizes as $paramskey => $null) {
            $result[$paramskey] = $this->getImageCacheUrl($image, $paramskey);
        }

        return $result;
    }

    /**
     * Генерация ссылки на кешированное изобаржение
     * @param  Image $image
     * @param  string $paramskey
     * @return string
     */
    public function getImageCacheUrl(Image $image, $paramskey)
    {
        $id = $image->getId();
        $filePath = $image->getFile();
        $filePath = $filePath['path'];
        $format = pathinfo($filePath, PATHINFO_EXTENSION);

        $sign = $this->getSign(array($paramskey, $id, $format));

        return $this->baseUrl.$this->router->generate('q_files_image_cache', array(
            'sign0to2' => mb_substr($sign, 0, 2),
            'sign2to4' => mb_substr($sign, 2, 2),
            'paramskey' => $paramskey,
            'id' => $id,
            'sign' => mb_substr($sign, 4),
            'format' => $format,
        ));
    }

    /**
     * Генерация подписи изображения
     * @param  array $data
     * @return string
     */
    public function getSign(array $data)
    {
        $md5 = md5($this->cacheSecret);

        foreach($data as $k => $v) {
            $md5 = md5($k.$v.$md5);
        }

        return mb_substr($md5, 3, 12);
    }
}
