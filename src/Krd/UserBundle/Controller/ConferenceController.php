<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Онлайн-конференции в личном кабинете
 */
class ConferenceController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/", name="krd_user_conference")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/user/conference/online/list.json", name="ajax_user_conference_online_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.user.modules.conference')->getItemsPast();
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
