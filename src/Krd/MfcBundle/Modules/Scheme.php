<?php

namespace Krd\MfcBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль MFC - Схема
 */
class Scheme extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    public function renderContent()
    {
        return $this->twig->render('KrdMfcBundle:Module:mfc/scheme.html.twig');
    }
}


