<?php

namespace Krd\UserBundle;

final class KrdUserEvents
{
    /**
     * Запрос смены пароля
     */
    const RESET_PASSWORD_REQUEST = 'krd_user.reset_password.request';

    /**
     * пароль пользователя успешно изменен
     */
    const RESET_PASSWORD_SUCCESS = 'krd_user.reset_password.success';
}
