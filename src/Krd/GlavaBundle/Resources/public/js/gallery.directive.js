/**
 * @author User
 */
var galleryApp = angular.module('glava.gallery',[]);
var galleryCache = {};

galleryApp.directive('previewSlider', function($http, $timeout, $rootScope, $dialog) {
	var tpl = '<div class="preview-container" style="display:none"> \
	<div class="title-container clearfix"> \
		<div class="counter"><span>{{ current+1 }}</span> / {{ total }}</div> \
		<div class="title">{{ title }}</div> \
		<div class="pos-right"> \
			<div class="item dd-list"> \
	            <i class="icon-share-panel-share" title="Рассказать друзьям"></i> \
	            <div class="dd-wrap"> \
	                <i class="icon-share-dd-share"></i> \
					<div class="yashare-auto-init" id="ya_share{{ $id }}" data-yasharel10n="ru" data-yasharetype="none" data-yasharequickservices="twitter,facebook,vkontakte"> \
					</div> \
	            </div> \
	        </div> \
		</div> \
	</div> \
	<a href="#" class="next" ng-click="next($event)" ng-class="{hidden: disableNext}"><i></i></a> \
	<a href="#" class="prev" ng-click="prev($event)" ng-class="{hidden: disablePrev}"><i></i></a> \
	<div class="slider"> \
		<div class="flow"> \
			<div class="viewport"> \
				<div class="slide" ng-repeat="image in images"> \
					<a href="#" ng-click="showPopup($event, $index, id)"> \
						<i></i> \
						<img ng-src="{{ image.src }}" /> \
					</a> \
				</div> \
			</div> \
		</div> \
	</div> \
</div>';
	return {
		scope : true,
		restrict : 'A',
		template : tpl,
		
		link : function(scope, $element)
		{
			scope.current = 0;
			scope.total = 0;
			scope.title = '';
			scope.images = [];
			scope.$row = $element.parent();
			scope.$container = scope.$row.parent();
			scope.$flow = $element.find('.flow');
			scope.$slider = $element.find('.slider');
			scope.$viewport = $element.find('.viewport');
			scope.scroll = true;
			
            scope.showPopup = function( $event, $index, id) {
				scope.setCurrent($index);
                $dialog.create('/ajax/glava/gallery/popup.html?id='+id+'&current='+$index, {
                    'class': 'gallery-popup',
					opacity : 0.8
                });
            };
			
			$timeout(function(){
				scope.share = new Ya.share({
					element: 'ya_share' + scope.$id,
					elementStyle: {
						'type': 'none',
						'border': false,
						'quickServices': ['twitter', 'facebook', 'vkontakte']
					}
				});
			}, 20);
			
			scope.disableNext = false;
			scope.disablePrev = false;
			scope.disableArrow = function() {
				if( scope.current >= scope.total - 1 ) {
					scope.disableNext = true;
				}
				if( scope.current <= 0 ) {
					scope.disablePrev = true;
				}
				
				var $slide =  scope.$flow.find('.slide');
				$slide.removeClass('active');
				$slide.eq(scope.current).addClass('active');
			}
			
			scope.prev = function($event) {
				$event.preventDefault();
				scope.scroll = true;
				
				if( scope.current > 0 ) {
					scope.current -= 1;
					scope.disableNext = false;
				}
				
				scope.disableArrow();
			};
			
			scope.next = function($event){
				$event.preventDefault();
				scope.scroll = true;
				if (scope.current < scope.total - 1) {
					scope.current += 1;
					scope.disablePrev = false;
				}
				
				scope.disableArrow();
			};
			
			scope.setCurrent = function(index) {
				scope.scroll = true;
				scope.disableNext = false;
				scope.disablePrev = false;
				scope.current = index;
				scope.disableArrow();
			}
			
			scope.toCache = function( ) {
				if( scope.id ) {
					galleryCache[scope.id].position = scope.current;
				}
			}
			
			scope.offsetof = function(index) {
				var $slide =  scope.$flow.find('.slide');
				return $slide.eq(index)[0] ? $slide.eq(index).offset().left : 0;
			}
			
			scope.$watch('current', function() {
				
				if (scope.images.length) {
					scope.title = scope.images[scope.current].title;
					scope.picture = scope.images[scope.current].share;
					
					var offset = scope.offsetof(scope.current) - scope.offsetof(0);
					if (scope.$viewport.width() - offset < scope.$slider.width()) {
						offset = Math.max(0, scope.$viewport.width() - scope.$slider.width());
					}
					
					scope.toCache();
					if (scope.scroll === true) {
						scope.$flow.stop().animate({
							marginLeft: -offset
						}, 500);
					} else {
						scope.scroll = true;
						scope.$flow.css({
							marginLeft: -offset
						});
					}
				}
			});
			
			scope.$watch('item', function() {
				if (scope.item) {
					scope.$container.find('.item').removeClass('active');
					scope.item.addClass('active');
					
					scope.share.updateShareLink(window.location.href+'detailt-'+scope.id+'.html', scope.title, {
						facebook : {
							image : 'http:'+scope.picture
						},
						twitter : {
							image : 'http:'+scope.picture
						},
						vkontakte : {
							image : 'http:'+scope.picture
						}
					});
					
					scope.$flow.find('.slide:eq(0)').addClass('active');
					scope.$container.find('.list-row:not(#'+scope.$row.attr('id')+') .previewSlider .preview-container').slideUp(200);
					$element.children('.preview-container').slideDown(200, function() {
						$('html,body').stop().animate({
							scrollTop : $element.offset().top
						}, 500);
					});
				}
			});
			
			scope.$row.find('.item.dd-list').hover(function() {
				$(this).children('.dd-wrap').show();
			}, function() {
				$(this).children('.dd-wrap').hide();
			})
			
			scope.$row.on('click', '.list-view .item', function(e) {
				e.preventDefault();
				
				var self = $(this);
				scope.id = self.attr('id');
				
				scope.disableNext = false;
				scope.disablePrev = false;
				
				//window.location.hash = 'show:' + scope.id;
				
				if( galleryCache[scope.id] ) {
					scope.$apply(function() {
						var cache = galleryCache[scope.id];
						
						scope.images = cache.images;
						scope.title = scope.images[0].title;
						scope.total = scope.images.length;
						scope.item = self;
						scope.scroll = false;
						$timeout(function() {
							scope.current = cache.position;
							scope.disableArrow();
						}, 10);
					});
				} else {
					self.addClass('clicked');

					$http({
						method: 'GET',
						url: scope.$container.data('source') + '-' + scope.id + '.json'
					}).success(function(data){
						scope.scroll = false;
						scope.current = 0;
						scope.item = self;
						//scope.picture = data.image_main.size.gallery_preview;
						
						self.removeClass('clicked');
						
						var images = [];
						for(var i = 0; i < data.images.length; i++) {
							images.push({
								src : data.images[i].size['glava400x300'],
								share : data.images[i].size['gallery_preview'],
								title : data.images[i].title || data.title,
								href : '#'
							});
						}
						scope.images = images;
						scope.total = images.length;
						
						scope.title = data.title;
						scope.picture = images[0].share;
						
						galleryCache[scope.id] = {
							images: images,
							position: 0,
							offset : 0
						};
						scope.disableArrow();
					}).error(function(data){
					
					});
				}
				return false;
			});
		}
	};
})
