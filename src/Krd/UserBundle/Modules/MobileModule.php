<?php

namespace Krd\UserBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль мобильных приложений
 */
class MobileModule extends AbstractModule
{
    public function renderAdminContent()
    {
        // Nothing
    }

    public function renderContent()
    {
        return $this->twig->render('KrdUserBundle:Module:mobile/index.html.twig');
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget()
    {
        return $this->twig->render('KrdUserBundle:Module:mobile/widget.html.twig');
    }
}
