<?php

namespace Krd\MfcBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль MFC 2
 */
class Mfc2 extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    public function renderContent()
    {
        return $this->twig->render('KrdMfcBundle:Module:mfc/map2.html.twig');
    }
}


