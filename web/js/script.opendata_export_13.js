$(function() {
    $('.socialmap-page').on('click', 'a[href^="#!/export-"]', function(e) {
        e.preventDefault();

        var category = $(this).attr('href').split('-')[1];

        $.get('/opendata/', {exportwindow:category}, function(data) {
            var $overlay = $('<div class="export-overlay"></div>').appendTo('body');
            var $window = $('#export-window-tpl').tmpl(data).appendTo('body');

            $overlay.css({opacity:0, display:'block'}).animate({opacity:0.5});
            $window.css({opacity:1}).fadeIn(200);

            var __close = function() {
                $overlay.fadeOut(200);
                $window.fadeOut(200, function() {
                    $window.remove();
                    $overlay.remove();
                });

                return false;
            };

            $overlay.on('click', __close);
            $window.find('.close').on('click', __close);
        });
    });
});
