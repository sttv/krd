<?php

namespace Krd\GlavaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\Video as VideoEx;

/**
 * Видео
 * 
 * @ORM\Entity
 * @ORM\Table(name="glava_video",
 *      indexes={
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 * 
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 * 
 * @Form\Fields(
 * 	   parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     poster={"files", {
 *         "label"="Постер для видео",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     webmsd = {"files", {
 *         "label"="Видео формат SD расширение webm"
 *     }},
 *     mp4sd = {"files", {
 *         "label"="Видео формат SD расширение mp4"
 *     }},
 *     webmhd = {"files", {
 *     		"label" = "Видео формат HD расширение webm"
 *     }},
 *     mp4hd = {"files", {
 *     		"label" = "Видео формат HD расширение mp4"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Video {
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;
	
	 /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;
	
	/**
     * Название
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;


    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;
	
	/**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="glava_video_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="glava_video_poster_relation",
     *      joinColumns={@ORM\JoinColumn(name="poster_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $poster;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Video", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="glava_video_relation",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $video;
	
	/**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="video_webmsd_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $webmsd;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="video_mp4sd_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $mp4sd;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="video_webmhd_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $webmhd;
	
	/**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="video_mp4hd_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $mp4hd;
	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $refid;
	
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setParent($parent)
	{
		$this->parent = $parent;
	}
	
	public function getParent()
	{
		return $this->parent;
	}
	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setDate(\DateTime $date)
	{
		$this->date = $date;
	}
	
	public function getDate()
	{
		return $this->date;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    } 
	
    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }
	
	/// 
	public function getPoster()
    {
        return $this->poster ?: $this->poster = new ArrayCollection();
    }

    public function hasPoster()
    {
        return count($this->getPoster()) > 0;
    }

    public function addPoster(Image $poster)
    {
        if (!$this->getPoster()->contains($poster)) {
            $this->getPoster()->add($poster);
        }
    }

    public function removePoster(Image $poster)
    {
        if ($this->getPoster()->contains($poster)) {
            $this->getPoster()->removeElement($poster);
        }
    }
	///

    public function getVideo()
    {
        return $this->video ?: $this->video = new ArrayCollection();
    }

    public function getVideoConverted()
    {
        $list = new ArrayCollection();

        foreach($this->getVideo() as $video) {
            if ($video->getConverted()) {
                $list[] = $video;
            }
        }

        return $list;
    }

    public function hasVideo()
    {
        return count($this->getVideo()) > 0;
    }

    public function hasVideoConverted()
    {
        return count($this->getVideoConverted()) > 0;
    }

    public function addVideo(VideoEx $image)
    {
        if (!$this->getVideo()->contains($image)) {
            $this->getVideo()->add($image);
        }
    }

    public function removeVideo(VideoEx $image)
    {
        if ($this->getVideo()->contains($image)) {
            $this->getVideo()->removeElement($image);
        }
    }
	
    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
	
	public function setRefId($id) {
		$this->refid = $id;
	}
	
	public function getRefId()
	{
		return $this->refid;
	}
	
	// Videos:
	// Webm SD:
	public function getWebmSd()
    {
        return $this->webmsd ?: $this->webmsd = new ArrayCollection();
    }

    public function hasWebmSd()
    {
        return count($this->getWebmSd()) > 0;
    }

    public function addWebmSd(File $file)
    {
        if (!$this->getWebmSd()->contains($file)) {
            $this->getWebmSd()->add($file);
        }
    }

    public function removeWebmSd(File $file)
    {
        if ($this->getWebmSd()->contains($file)) {
            $this->getWebmSd()->removeElement($file);
        }
    }
	
	// Webm HD:
	public function getWebmHd()
    {
        return $this->webmhd ?: $this->webmhd = new ArrayCollection();
    }

    public function hasWebmHd()
    {
        return count($this->getWebmHd()) > 0;
    }

    public function addWebmHd(File $file)
    {
        if (!$this->getWebmHd()->contains($file)) {
            $this->getWebmHd()->add($file);
        }
    }

    public function removeWebmHd(File $file)
    {
        if ($this->getWebmHd()->contains($file)) {
            $this->getWebmHd()->removeElement($file);
        }
    }
	
	// MP4 SD:
	public function getMp4Sd()
    {
        return $this->mp4sd ?: $this->mp4sd = new ArrayCollection();
    }

    public function hasMp4Sd()
    {
        return count($this->getMp4Sd()) > 0;
    }

    public function addMp4Sd(File $file)
    {
        if (!$this->getMp4Sd()->contains($file)) {
            $this->getMp4Sd()->add($file);
        }
    }

    public function removeMp4Sd(File $file)
    {
        if ($this->getMp4Sd()->contains($file)) {
            $this->getMp4Sd()->removeElement($file);
        }
    }
	
	// MP4 HD:
	public function getMp4Hd()
    {
        return $this->mp4hd ?: $this->mp4hd = new ArrayCollection();
    }

    public function hasMp4Hd()
    {
        return count($this->getMp4Hd()) > 0;
    }

    public function addMp4Hd(File $file)
    {
        if (!$this->getMp4Hd()->contains($file)) {
            $this->getMp4Hd()->add($file);
        }
    }

    public function removeMp4Hd(File $file)
    {
        if ($this->getMp4Hd()->contains($file)) {
            $this->getMp4Hd()->removeElement($file);
        }
    }

    public function getUrl()
    {
        return $this->getParent()->getUrl(true);
    }
}