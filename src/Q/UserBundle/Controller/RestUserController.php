<?php

namespace Q\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\UserBundle\Entity\User;
use Q\UserBundle\Entity\UserGroup;


/**
 * Контроллер пользователей
 *
 * @Route("/rest/user")
 * @PreAuthorize("hasRole('ROLE_CMS_ADMIN')")
 */
class RestUserController extends Controller
{
    /**
     * Список пользователй
     *
     * @Route("/", name="cms_rest_user", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restListAction(Request $request)
    {
        $adminGroups = $this->getDoctrine()->getManager()
            ->createQuery('SELECT g.id FROM QUserBundle:UserGroup g WHERE g.roles LIKE \'%"ROLE_CMS"%\'')
            ->getResult();

        foreach ($adminGroups as &$group) {
            $group = $group['id'];
        }
        unset($group);

        $adminUsers = $this->getDoctrine()->getmanager()
            ->createQuery('SELECT u.id FROM '.$this->container->get('fos_user.user_manager')->getClass().' u INNER JOIN u.groups g WITH g.id IN (:groups)')
            ->setParameter('groups', $adminGroups)
            ->getResult();

        foreach ($adminUsers as &$user) {
            $user = $user['id'];
        }
        unset($user);

        $qb = $this->getDoctrine()->getManager()
            ->createQueryBuilder()
            ->select('u')
            ->from($this->container->get('fos_user.user_manager')->getClass(), 'u');

        if ($request->get('admin') == 1) {
            $qb->andWhere('u.id IN (:users)')->setParameter('users', $adminUsers);
        } else {
            $qb->andWhere('u.id NOT IN (:users)')->setParameter('users', $adminUsers);
        }

        Paginator::disableQueryCache();

        return Paginator::createFromRequest($request, $qb->getQuery());
    }

    /**
     * Получение пользователя
     *
     * @Route("/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restGetAction($id)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $id));

        if ($user) {
            return $user;
        } else {
            throw $this->createNotFoundException('User not found');
        }
    }

    /**
     * Создание пользователя
     *
     * @Route("/", name="cms_rest_user_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, array('groups'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($user, null, array('csrf_protection' => false))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager->updateUser($user);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'user' => $user);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование пользователя
     *
     * @Route("/{id}", name="cms_rest_user_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditAction(Request $request, $id)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $id));

        if (is_null($user)) {
            throw $this->createNotFoundException('User not found');
        }

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, array('groups'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($user, null, array('csrf_protection' => false))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->get('fos_user.user_manager')->updateUser($user);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'user' => $user);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Активация/деактивация пользователя
     *
     * @Route("/set/{id}", name="cms_rest_user_edit_set", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $id));

        if (is_null($user)) {
            throw $this->createNotFoundException('User not found');
        }

        if ($request->get('enabled') == 'true' || $request->get('enabled') === true) {
            $user->setEnabled(true);
        } else {
            $user->setEnabled(false);
        }

        if ($request->get('locked') == 'true' || $request->get('locked') === true) {
            $user->setLocked(true);
        } else {
            $user->setLocked(false);
        }

        $this->container->get('fos_user.user_manager')->updateUser($user);

        return $user;
    }

    /**
     * Удаление пользователя
     *
     * @Route("/{id}", name="cms_rest_user_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function restPostDeleteAction(Request $request, $id)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $id));

        if (is_null($user)) {
            throw $this->createNotFoundException('User not found');
        }

        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }

    /**
     * Рендер формы добавления
     * @Route("/form/", name="cms_rest_user_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetAddForm()
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();

        $form = $this->get('form_metadata.mapper')->createFormBuilder($user, null, array('csrf_protection' => false))
            ->setAction($this->generateUrl('cms_rest_user_create'))
            ->getForm();

        return array('form' => $form->createView());
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_user_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetEditForm($id)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $id));

        if (is_null($user)) {
            throw $this->createNotFoundException('User not found');
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($user, null, array('csrf_protection' => false))
            ->setAction($this->generateUrl('cms_rest_user_edit', array('id' => $id)))
            ->getForm();

        return array('form' => $form->createView(), 'user' => $user);
    }
}
