<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\Form\Type\UserRegistrationType;


/**
 * Фронтэнд регистрация
 */
class RegistrationController extends Controller implements ActiveSecuredController
{
    /**
     * Страница авторизации
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirect($this->get('qcore.routing.current')->getNode()->getParent()->getUrl(true));
        }

        return array('form' => $this->getForm());
    }

    /**
     * Построение формы
     */
    protected function getForm()
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEnabled(false);
        $em = $this->getDoctrine()->getManager();
        $user->addGroup($em->find('QUserBundle:UserGroup', User::FRONTEND_GROUP));

        return $this->createForm(new UserRegistrationType(), $user, array(
            'action' => $this->get('router')->generate('krd_user_registration_submit'),
            'method' => 'POST',
        ));
    }

    /**
     * Сабмит формы регистрации
     *
     * @Route("/ajax/krd/user/registration/submit/", name="krd_user_registration_submit")
     * @Method({"POST"})
     */
    public function submitAction(Request $request)
    {
        $form = $this->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        //return new JsonResponse(array('success' => false, 'error' => 'Регистрация запрещена'));

        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');
        $user = $form->getData();

        if ($user instanceof User) {
            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

            $response = new JsonResponse(array('success' => true));

            $user->setUsername($user->getEmail());
            $user->setWidgets(array('5', '7'));
            $user->setNewbie(true);

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        } else {
            $this->get('logger')->crit('Ошибка регистрации');

            return new JsonResponse(array('success' => false, 'error' => 'Ошибка сервера'));
        }
    }

    /**
     * Подтверждение регистрации
     * @Route("/user/registration/confirmation/{token}/", name="krd_user_registration_comfirmation")
     * @Template()
     */
    public function confirmationAction(Request $request, $token)
    {
        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (!$user) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        $dispatcher = $this->container->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, new Response()));

        return array('user' => $user);
    }

    /**
     * Сообщение о проверке емаила. Нам не нужно
     * @Route("/user/registration/check_email/", name="fos_user_registration_check_email")
     */
    public function checkEmailAction(Request $request)
    {
        die('check email');
    }

    /**
     * Просто редирект на форму регистрации
     * @Route("/user/registration/redirect/to/form", name="krd_user_registration_path")
     */
    public function targetPathIndex()
    {
        $url = $this->get('qcore.routing.current')->generate(42);

        if ($url == '#') {
            throw $this->createNotFoundException();
        } else {
            return $this->redirect($url);
        }
    }
}
