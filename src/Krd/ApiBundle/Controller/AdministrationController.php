<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;

/**
 * Структура администрации
 *
 * @Route("/administration")
 */
class AdministrationController extends Controller
{
    /**
     * Детальная информация о человеке
     *
     * @Route("/human/{id}.{_format}", requirements={"id"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function humanAction(Request $request, $id)
    {
        $human = $this->getDoctrine()->getManager()->find('KrdAdministrationBundle:Human', $id);

        if (!$human) {
            throw $this->createNotFoundException();
        }

        $shedule = $human->getShedule();

        foreach ($shedule as &$item) {
            $data = explode(',', $item, 2);
            $date_full = trim(@$data[0]);
            $address = trim(@$data[1]);
            $data = explode(' ', $date_full, 2);
            $date = trim(@$data[0]);
            $time = trim(@$data[1]);

            $item = array('date' => $date, 'time' => $time);
        }
        unset($item);

        $human = array(
            'fio' => $human->getFio(),
            'fior' => $human->getFior(),
            'post' => $human->getPost(),
            'post_mobile' => $human->getPostMobile(),
            'content' => $human->getContent(),
            'contacts' => $human->getContacts(),
            'shedule_title' => $human->getSheduleTitle(),
            'shedule_address' => isset($address) ? $address : '',
            'shedule' => $shedule,
            'image_main' => $human->getMainWhiteImage(),
            'image_main_other' => $human->getMainImage(),
            'has_children_human' => $human->hasChildrenHuman(),
            'url' => $human->getUrl(),
        );

        return $human;
    }

    /**
     * Список
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
       /* $underDevelop = array(
            'page_count' => 1,
            'pages' => array(),
            'items_count' => 1,
            'items' => array(
                array(
                    'id' => 0,
                    'fio' => 'Раздел в стадии наполнения',
                    'fior' => 'Раздел в стадии наполнения',
                    'name' => "develop",
                    'post' => '',
                    'post_mobile' => '',
                    'active' => true,
                    'sort' => 0,
                    'showlink' => false,
                    'url' => '#',
                )
            ),
        );
        header('Content-Type: application/json');
        echo json_encode($underDevelop);
        exit;
*/
        $queryBuilder = $this->getDoctrine()->getRepository('KrdAdministrationBundle:Human')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->setParameter('parent', $parent)
            ->orderBy('n.sort', 'ASC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Список подчиненных
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/childs/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function childsAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdAdministrationBundle:Human')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parentHuman = :parentHuman')
            ->setParameter('parentHuman', $parent)
            ->orderBy('n.sort', 'ASC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }
}
