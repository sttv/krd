<?php

namespace Q\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Видео в определенном формате
 *
 * @ORM\Entity
 * @ORM\Table(name="video_format")
 *
 * @FileStore\Uploadable
 *
 * @JMS\ExclusionPolicy("all")
 */
class VideoFormat
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="format")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     */
    private $format = '';

    /**
     * Загруженный файл
     * @ORM\Column(type="array")
     * @FileStore\UploadableField(mapping="video")
     *
     * @JMS\Expose
     */
    private $file;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;
	
	/**
	 * @ORM\Column(type="string")
	 */
	private $type;


    public function getId()
    {
        return $this->id;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(Video $video)
    {
        $this->parent = $video;
    }

    public function clearParent()
    {
        $this->parent = null;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function setFormat($format)
    {
        $this->format = (string)$format;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (empty($this->title)) {
            $this->title = $file->getClientOriginalName();
        }
    }

    public function setFileArray(array $file) {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
	
	public function setType($type) 
	{
		$this->type = $type;
	}
	
	public function getType()
	{
		return $this->type;
	}

    public function getFileType()
    {
        return pathinfo($this->file['path'], PATHINFO_EXTENSION);
    }

    public function getFilePath()
    {
        return isset($this->file['path']) ? $this->file['path'] : '';
    }

    public function getSize()
    {
        return isset($this->file['size']) ? $this->file['size'] : 0;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}
