<?php

namespace Krd\UserBundle\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Exception;
use Q\CoreBundle\Swift\Mailer;
use Q\UserBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\Container;


class UserSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var array
     */
    protected $deferredUpdated = array();

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
            'postUpdate',
        );
    }

    /**
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Уведомление об активации отзыва
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof User) {
            if ($args->hasChangedField('enabled')
                && $args->getOldValue('enabled') == false
                && $args->getNewValue('enabled') == true
            ) {
                $this->deferredUpdated[] = $entity;
                $this->logger->debug(
                    'Пользователь добавлен в очередь на отправку уведомления об активации',
                    array($entity->getId())
                );
            }
        }
    }

    /**
     * Уведомление об активации отзыва
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $list = $this->deferredUpdated;
        $this->deferredUpdated = array();

        foreach ($list as $entity) {
            /** @var User $entity */
            $email = $entity->getEmail();

            if (!$email) {
                $this->logger->error('Некуда отправлять уведомление об активации', array($entity->getId()));
                continue;
            }

            $message = $this->getMailer()->newMessage('Ваша учетная запись в личном кабинете интернет-портала администрации Краснодара активирована');
            $message->setTo($email);

            try {
                $this->getMailer()->sendTemplate(
                    $message,
                    'KrdUserBundle:Email:activated-user.html.twig',
                    array('user' => $entity)
                );
                $this->logger->debug('Уведомление об активации пользователя отправлено', array($entity->getId(), $email));
            } catch (Exception $e) {
                $this->logger->critical(
                    'Ошибка отправки email уведомления об активации пользователя',
                    array($entity->getId(), $e->getMessage())
                );
            }
        }
    }
}