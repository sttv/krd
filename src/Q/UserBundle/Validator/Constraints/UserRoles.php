<?php

namespace Q\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Валидатор списка ролей пользователя/группы
 */
class UserRoles extends Constraint
{
    public $message = 'Значение должно быть вида ROLE_XXX';
    public $notArrayMessage = 'Необходимо предоставить список значений';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
