<?
include_once 'conf.php';
ob_start();
$page = 1;
$auth = new auth();

if (isset($_POST['send'])) {
	if (!$auth->authorization()) {
		$error = $_SESSION['error'];
		unset ($_SESSION['error']);
	}
}
if (isset($_GET['exit'])) $auth->exit_user();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Интерфейс администрирования</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="./media/js/jquery.js"></script>
        <script src="./media/js/bootstrap.min.js"></script>
        <style type="text/css">
            .empty-form {display: none;}
        </style>
    </head>
    <body>
		<div class="container">
			<?
			if ($auth->check()) {
				if (isset($_GET['export'])) {
					$item = new items();
					$object = $item->get_all();
					$json_str = json_encode($object);
					$json_str = str_replace('\/', '/', $json_str);
					$file = fopen("./data/objects.json", "w");
					fwrite($file, $json_str);
					fclose($file);
					$_SESSION['json_link'] = '<label class="text-success"><a href="./data/objects.json">Скачать .json файл</a></label>';
					header("Location: index.php");
					exit();
				}
				include_once 'menu_template.php';
				?>
				<h3>Карта</h3>
				<h5>Интерфейс администрирования</h5>
				<?
				if (isset($_SESSION['json_link'])) {
					echo $_SESSION['json_link'];
					unset($_SESSION['json_link']);
				}
			} else {
				include_once 'menu_template.php';
				?>				
				<form action="" method="post">
					<input type="hidden" name="send" value="1" />
					<div class="control-group">
						<?=(isset($error) ? $error:'');?>
					</div>
					<div class="control-group">
						<label for="login">Логин:</label>
						<input id="login" type="text" name="login" value="<?=@$_POST['login']?>" class="span6">
					</div>
					<div class="control-group">
						<label for="passwd">Пароль:</label>
						<input id="passwd" type="password" name="passwd" class="span6">
					</div>
					<div class="control-group">
						<a href="recovery.php">Восстановить пароль</a>
					</div>
					<div class="control-group">
						<button type="submit" name="send" class="btn btn-primary input-large">Войти</button>
					</div>
				</form>
			<?}?>
		</div>
	</body>
</html>
<?ob_end_flush();?>