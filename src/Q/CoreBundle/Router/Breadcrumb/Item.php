<?php

namespace Q\CoreBundle\Router\Breadcrumb;


/**
 * Элемент хлебных крошек
 */
class Item
{
    /**
     * Заголовок
     */
    protected $title;

    /**
     * Ссылка
     */
    protected $url;

    /**
     * Элемент является последним
     * @var boolean
     */
    protected $last = false;

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getFrontendTitle()
    {
        return $this->title;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setLast($last)
    {
        $this->last = (boolean)$last;
    }

    public function isLast()
    {
        return $this->last;
    }

    /**
     * Сравнение двух элементов
     * @param  Item    $item
     * @return boolean
     */
    public function isEqual(Item $item)
    {
        return ($item->getTitle() == $this->getTitle());
    }
}
