<?php

namespace Krd\SubscribeBundle\Navigation;

use Q\CoreBundle\Navigation\Item;


/**
 * Пункт меню управления рассылками
 */
class Subscribe extends Item
{
    public function getTitle()
    {
        return 'Рассылка';
    }

    public function getLink()
    {
        return $this->router->generate('cms_krd_subscribe_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS_SUBSCRIBE');
    }
}
