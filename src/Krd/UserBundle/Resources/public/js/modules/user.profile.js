/**
 * Профиль пользователя
 */
angular.module('user.profile', ['user.notifications'])
    /**
     * Контроллер формы профиля
     */
    .controller('UserProfileFormCtrl', ['$scope', '$element', 'notifications', function($scope, $element, notifications) {
        $scope.onSuccess = function(response) {
            if (!!response.message) {
                notifications.user.info(response.message);
            }
        };

        $scope.onError = function(response) {
            if (!!response.error) {
                notifications.user.error(response.error);
            }
        };
    }])
;
