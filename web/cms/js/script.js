/**
 * Сериализация формы в объект
 */
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

!function(b){var a=function(d,c,f){if(f){f.stopPropagation();f.preventDefault()}this.$element=b(d);this.$newElement=null;this.button=null;this.options=b.extend({},b.fn.selectpicker.defaults,this.$element.data(),typeof c=="object"&&c);if(this.options.title==null){this.options.title=this.$element.attr("title")}this.val=a.prototype.val;this.render=a.prototype.render;this.refresh=a.prototype.refresh;this.selectAll=a.prototype.selectAll;this.deselectAll=a.prototype.deselectAll;this.init()};a.prototype={constructor:a,init:function(d){if(!this.options.container){this.$element.hide()}else{this.$element.css("visibility","hidden")}this.multiple=this.$element.prop("multiple");var f=this.$element.attr("class")!==undefined?this.$element.attr("class").split(/\s+/):"";var h=this.$element.attr("id");this.$element.after(this.createView());this.$newElement=this.$element.next(".bootstrap-select");if(this.options.container){this.selectPosition()}this.button=this.$newElement.find("> button");if(h!==undefined){var g=this;this.button.attr("data-id",h);b('label[for="'+h+'"]').click(function(){g.$newElement.find("button[data-id="+h+"]").focus()})}for(var c=0;c<f.length;c++){if(f[c]!="selectpicker"){this.$newElement.addClass(f[c])}}if(this.multiple){this.$newElement.addClass("show-tick")}this.button.addClass(this.options.style);this.checkDisabled();this.checkTabIndex();this.clickListener();this.render();this.setSize()},createDropdown:function(){var c="<div class='btn-group bootstrap-select'><button type='button' class='btn dropdown-toggle' data-toggle='dropdown'><span class='filter-option pull-left'></span>&nbsp;<span class='caret'></span></button><ul class='dropdown-menu' role='menu'></ul></div>";return b(c)},createView:function(){var c=this.createDropdown();var d=this.createLi();c.find("ul").append(d);return c},reloadLi:function(){this.destroyLi();var c=this.createLi();this.$newElement.find("ul").append(c)},destroyLi:function(){this.$newElement.find("li").remove()},createLi:function(){var h=this;var e=[];var g=[];var c="";this.$element.find("option").each(function(){e.push(b(this).text())});this.$element.find("option").each(function(k){var l=b(this);var j=l.attr("class")!==undefined?l.attr("class"):"";var p=l.text();var n=l.data("subtext")!==undefined?'<small class="muted">'+l.data("subtext")+"</small>":"";var m=l.data("icon")!==undefined?'<i class="'+l.data("icon")+'"></i> ':"";if(l.is(":disabled")||l.parent().is(":disabled")){m="<span>"+m+"</span>"}p=m+'<span class="text">'+p+n+"</span>";if(h.options.hideDisabled&&(l.is(":disabled")||l.parent().is(":disabled"))){g.push('<a style="min-height: 0; padding: 0"></a>')}else{if(l.parent().is("optgroup")&&l.data("divider")!=true){if(l.index()==0){var o=l.parent().attr("label");var q=l.parent().data("subtext")!==undefined?'<small class="muted">'+l.parent().data("subtext")+"</small>":"";var i=l.parent().data("icon")?'<i class="'+l.parent().data("icon")+'"></i> ':"";o=i+'<span class="text">'+o+q+"</span>";if(l[0].index!=0){g.push('<div class="div-contain"><div class="divider"></div></div><dt>'+o+"</dt>"+h.createA(p,"opt "+j))}else{g.push("<dt>"+o+"</dt>"+h.createA(p,"opt "+j))}}else{g.push(h.createA(p,"opt "+j))}}else{if(l.data("divider")==true){g.push('<div class="div-contain"><div class="divider"></div></div>')}else{if(b(this).data("hidden")==true){g.push("")}else{g.push(h.createA(p,j))}}}}});if(e.length>0){for(var d=0;d<e.length;d++){var f=this.$element.find("option").eq(d);c+="<li rel="+d+">"+g[d]+"</li>"}}if(!this.multiple&&this.$element.find("option:selected").length==0&&!h.options.title){this.$element.find("option").eq(0).prop("selected",true).attr("selected","selected")}return b(c)},createA:function(d,c){return'<a tabindex="0" class="'+c+'">'+d+'<i class="icon-ok check-mark"></i></a>'},render:function(){var h=this;this.$element.find("option").each(function(i){h.setDisabled(i,b(this).is(":disabled")||b(this).parent().is(":disabled"));h.setSelected(i,b(this).is(":selected"))});var g=this.$element.find("option:selected").map(function(i,k){var j;if(h.options.showSubtext&&b(this).attr("data-subtext")&&!h.multiple){j=' <small class="muted">'+b(this).data("subtext")+"</small>"}else{j=""}if(b(this).attr("title")!=undefined){return b(this).attr("title")}else{return b(this).text()+j}}).toArray();var f=!this.multiple?g[0]:g.join(", ");if(h.multiple&&h.options.selectedTextFormat.indexOf("count")>-1){var c=h.options.selectedTextFormat.split(">");var e=this.options.hideDisabled?":not([disabled])":"";if((c.length>1&&g.length>c[1])||(c.length==1&&g.length>=2)){f="выбрано "+g.length+" из "+this.$element.find('option:not([data-divider="true"]):not([data-hidden="true"])'+e).length}}if(!f){f=h.options.title!=undefined?h.options.title:h.options.noneSelectedText}var d;if(this.options.showSubtext&&this.$element.find("option:selected").attr("data-subtext")){d=' <small class="muted">'+this.$element.find("option:selected").data("subtext")+"</small>"}else{d=""}h.$newElement.find(".filter-option").html(f+d)},setSize:function(){if(this.options.container){this.$newElement.toggle(this.$element.parent().is(":visible"))}var j=this;var e=this.$newElement.find(".dropdown-menu");var l=e.find("li > a");var o=this.$newElement.addClass("open").find(".dropdown-menu li > a").outerHeight();this.$newElement.removeClass("open");var h=e.find("li .divider").outerHeight(true);var g=this.$newElement.offset().top;var k=this.$newElement.outerHeight();var c=parseInt(e.css("padding-top"))+parseInt(e.css("padding-bottom"))+parseInt(e.css("border-top-width"))+parseInt(e.css("border-bottom-width"));var d=this.options.hideDisabled?":not(.disabled)":"";var f;if(this.options.size=="auto"){var p=function(){var q=g-b(window).scrollTop();var u=window.innerHeight;var r=c+parseInt(e.css("margin-top"))+parseInt(e.css("margin-bottom"))+2;var t=u-q-k-r;var s;f=t;if(j.$newElement.hasClass("dropup")){f=q-r}if((e.find("li").length+e.find("dt").length)>3){s=o*3+r-2}else{s=0}e.css({"max-height":f+"px","overflow-y":"auto","min-height":s+"px"})};p();b(window).resize(p);b(window).scroll(p)}else{if(this.options.size&&this.options.size!="auto"&&e.find("li"+d).length>this.options.size){var n=e.find("li"+d+" > *").filter(":not(.div-contain)").slice(0,this.options.size).last().parent().index();var m=e.find("li").slice(0,n+1).find(".div-contain").length;f=o*this.options.size+m*h+c;e.css({"max-height":f+"px","overflow-y":"auto"})}}if(this.options.width=="auto"){this.$newElement.find(".dropdown-menu").css("min-width","0");var i=this.$newElement.find(".dropdown-menu").css("width");this.$newElement.css("width",i);if(this.options.container){this.$element.css("width",i)}}else{if(this.options.width){if(this.options.container){this.$element.css("width",this.options.width);this.$newElement.width(this.$element.outerWidth())}else{this.$newElement.css("width",this.options.width)}}else{if(this.options.container){this.$newElement.width(this.$element.outerWidth())}}}},selectPosition:function(){var e=b(this.options.container).offset();var d=this.$element.offset();if(e&&d){var f=d.top-e.top;var c=d.left-e.left;this.$newElement.appendTo(this.options.container);this.$newElement.css({position:"absolute",top:f+"px",left:c+"px"})}},refresh:function(){this.reloadLi();this.render();this.setSize();this.checkDisabled();if(this.options.container){this.selectPosition()}},setSelected:function(c,d){if(d){this.$newElement.find("li").eq(c).addClass("selected")}else{this.$newElement.find("li").eq(c).removeClass("selected")}},setDisabled:function(c,d){if(d){this.$newElement.find("li").eq(c).addClass("disabled").find("a").attr("href","#").attr("tabindex",-1)}else{this.$newElement.find("li").eq(c).removeClass("disabled").find("a").removeAttr("href").attr("tabindex",0)}},isDisabled:function(){return this.$element.is(":disabled")||this.$element.attr("readonly")},checkDisabled:function(){if(this.isDisabled()){this.button.addClass("disabled");this.button.click(function(c){c.preventDefault()});this.button.attr("tabindex","-1")}else{if(!this.isDisabled()&&this.button.hasClass("disabled")){this.button.removeClass("disabled");this.button.click(function(){return true});this.button.removeAttr("tabindex")}}},checkTabIndex:function(){if(this.$element.is("[tabindex]")){var c=this.$element.attr("tabindex");this.button.attr("tabindex",c)}},clickListener:function(){var c=this;b("body").on("touchstart.dropdown",".dropdown-menu",function(d){d.stopPropagation()});this.$newElement.on("click","li a",function(j){var g=b(this).parent().index(),i=b(this).parent(),d=i.parents(".bootstrap-select"),h=c.$element.val();if(c.multiple){j.stopPropagation()}j.preventDefault();if(c.$element.not(":disabled")&&!b(this).parent().hasClass("disabled")){if(!c.multiple){c.$element.find("option").prop("selected",false);c.$element.find("option").eq(g).prop("selected",true)}else{var f=c.$element.find("option").eq(g).prop("selected");if(f){c.$element.find("option").eq(g).prop("selected",false)}else{c.$element.find("option").eq(g).prop("selected",true)}}d.find(".filter-option").html(i.text());d.find("button").focus();if(h!=c.$element.val()){c.$element.trigger("change")}c.render()}});this.$newElement.on("click","li.disabled a, li dt, li .div-contain",function(f){f.preventDefault();f.stopPropagation();var d=b(this).parent().parents(".bootstrap-select");d.find("button").focus()});this.$element.on("change",function(d){c.render()})},val:function(c){if(c!=undefined){this.$element.val(c);this.$element.trigger("change");return this.$element}else{return this.$element.val()}},selectAll:function(){this.$element.find("option").prop("selected",true).attr("selected","selected");this.render()},deselectAll:function(){this.$element.find("option").prop("selected",false).removeAttr("selected");this.render()},keydown:function(n){var o,m,h,l,j,i,p,d,g;o=b(this);h=o.parent();m=b("[role=menu] li:not(.divider):visible a",h);if(!m.length){return}if(/(38|40)/.test(n.keyCode)){l=m.index(m.filter(":focus"));i=m.parent(":not(.disabled)").first().index();p=m.parent(":not(.disabled)").last().index();j=m.eq(l).parent().nextAll(":not(.disabled)").eq(0).index();d=m.eq(l).parent().prevAll(":not(.disabled)").eq(0).index();g=m.eq(j).parent().prevAll(":not(.disabled)").eq(0).index();if(n.keyCode==38){if(l!=g&&l>d){l=d}if(l<i){l=i}}if(n.keyCode==40){if(l!=g&&l<j){l=j}if(l>p){l=p}}m.eq(l).focus()}else{var f={48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};var c=[];m.each(function(){if(b(this).parent().is(":not(.disabled)")){if(b.trim(b(this).text().toLowerCase()).substring(0,1)==f[n.keyCode]){c.push(b(this).parent().index())}}});var k=b(document).data("keycount");k++;b(document).data("keycount",k);var q=b.trim(b(":focus").text().toLowerCase()).substring(0,1);if(q!=f[n.keyCode]){k=1;b(document).data("keycount",k)}else{if(k>=c.length){b(document).data("keycount",0)}}m.eq(c[k-1]).focus()}if(/(13)/.test(n.keyCode)){b(":focus").click();h.addClass("open");b(document).data("keycount",0)}}};b.fn.selectpicker=function(e,f){var c=arguments;var g;var d=this.each(function(){if(b(this).is("select")){var m=b(this),l=m.data("selectpicker"),h=typeof e=="object"&&e;if(!l){m.data("selectpicker",(l=new a(this,h,f)))}else{if(h){for(var j in h){l.options[j]=h[j]}}}if(typeof e=="string"){var k=e;if(l[k] instanceof Function){[].shift.apply(c);g=l[k].apply(l,c)}else{g=l.options[k]}}}});if(g!=undefined){return g}else{return d}};b.fn.selectpicker.defaults={style:null,size:"auto",title:null,selectedTextFormat:"values",noneSelectedText:"Ничего не выбрано",width:null,container:false,hideDisabled:false,showSubtext:false};b(document).data("keycount",0).on("keydown","[data-toggle=dropdown], [role=menu]",a.prototype.keydown)}(window.jQuery);

/*! jQuery UI - v1.10.3 - 2013-09-16
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.sortable.js
* Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */

(function(e,t){function i(t,i){var s,a,o,r=t.nodeName.toLowerCase();return"area"===r?(s=t.parentNode,a=s.name,t.href&&a&&"map"===s.nodeName.toLowerCase()?(o=e("img[usemap=#"+a+"]")[0],!!o&&n(o)):!1):(/input|select|textarea|button|object/.test(r)?!t.disabled:"a"===r?t.href||i:i)&&n(t)}function n(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return"hidden"===e.css(this,"visibility")}).length}var s=0,a=/^ui-id-\d+$/;e.ui=e.ui||{},e.extend(e.ui,{version:"1.10.3",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({focus:function(t){return function(i,n){return"number"==typeof i?this.each(function(){var t=this;setTimeout(function(){e(t).focus(),n&&n.call(t)},i)}):t.apply(this,arguments)}}(e.fn.focus),scrollParent:function(){var t;return t=e.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(e.css(this,"position"))&&/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!t.length?e(document):t},zIndex:function(i){if(i!==t)return this.css("zIndex",i);if(this.length)for(var n,s,a=e(this[0]);a.length&&a[0]!==document;){if(n=a.css("position"),("absolute"===n||"relative"===n||"fixed"===n)&&(s=parseInt(a.css("zIndex"),10),!isNaN(s)&&0!==s))return s;a=a.parent()}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++s)})},removeUniqueId:function(){return this.each(function(){a.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,n){return!!e.data(t,n[3])},focusable:function(t){return i(t,!isNaN(e.attr(t,"tabindex")))},tabbable:function(t){var n=e.attr(t,"tabindex"),s=isNaN(n);return(s||n>=0)&&i(t,!s)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(i,n){function s(t,i,n,s){return e.each(a,function(){i-=parseFloat(e.css(t,"padding"+this))||0,n&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),s&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var a="Width"===n?["Left","Right"]:["Top","Bottom"],o=n.toLowerCase(),r={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+n]=function(i){return i===t?r["inner"+n].call(this):this.each(function(){e(this).css(o,s(this,i)+"px")})},e.fn["outer"+n]=function(t,i){return"number"!=typeof t?r["outer"+n].call(this,t):this.each(function(){e(this).css(o,s(this,t,!0,i)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.support.selectstart="onselectstart"in document.createElement("div"),e.fn.extend({disableSelection:function(){return this.bind((e.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(e){e.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),e.extend(e.ui,{plugin:{add:function(t,i,n){var s,a=e.ui[t].prototype;for(s in n)a.plugins[s]=a.plugins[s]||[],a.plugins[s].push([i,n[s]])},call:function(e,t,i){var n,s=e.plugins[t];if(s&&e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType)for(n=0;s.length>n;n++)e.options[s[n][0]]&&s[n][1].apply(e.element,i)}},hasScroll:function(t,i){if("hidden"===e(t).css("overflow"))return!1;var n=i&&"left"===i?"scrollLeft":"scrollTop",s=!1;return t[n]>0?!0:(t[n]=1,s=t[n]>0,t[n]=0,s)}})})(jQuery);(function(t,e){var i=0,s=Array.prototype.slice,n=t.cleanData;t.cleanData=function(e){for(var i,s=0;null!=(i=e[s]);s++)try{t(i).triggerHandler("remove")}catch(o){}n(e)},t.widget=function(i,s,n){var o,a,r,h,l={},c=i.split(".")[0];i=i.split(".")[1],o=c+"-"+i,n||(n=s,s=t.Widget),t.expr[":"][o.toLowerCase()]=function(e){return!!t.data(e,o)},t[c]=t[c]||{},a=t[c][i],r=t[c][i]=function(t,i){return this._createWidget?(arguments.length&&this._createWidget(t,i),e):new r(t,i)},t.extend(r,a,{version:n.version,_proto:t.extend({},n),_childConstructors:[]}),h=new s,h.options=t.widget.extend({},h.options),t.each(n,function(i,n){return t.isFunction(n)?(l[i]=function(){var t=function(){return s.prototype[i].apply(this,arguments)},e=function(t){return s.prototype[i].apply(this,t)};return function(){var i,s=this._super,o=this._superApply;return this._super=t,this._superApply=e,i=n.apply(this,arguments),this._super=s,this._superApply=o,i}}(),e):(l[i]=n,e)}),r.prototype=t.widget.extend(h,{widgetEventPrefix:a?h.widgetEventPrefix:i},l,{constructor:r,namespace:c,widgetName:i,widgetFullName:o}),a?(t.each(a._childConstructors,function(e,i){var s=i.prototype;t.widget(s.namespace+"."+s.widgetName,r,i._proto)}),delete a._childConstructors):s._childConstructors.push(r),t.widget.bridge(i,r)},t.widget.extend=function(i){for(var n,o,a=s.call(arguments,1),r=0,h=a.length;h>r;r++)for(n in a[r])o=a[r][n],a[r].hasOwnProperty(n)&&o!==e&&(i[n]=t.isPlainObject(o)?t.isPlainObject(i[n])?t.widget.extend({},i[n],o):t.widget.extend({},o):o);return i},t.widget.bridge=function(i,n){var o=n.prototype.widgetFullName||i;t.fn[i]=function(a){var r="string"==typeof a,h=s.call(arguments,1),l=this;return a=!r&&h.length?t.widget.extend.apply(null,[a].concat(h)):a,r?this.each(function(){var s,n=t.data(this,o);return n?t.isFunction(n[a])&&"_"!==a.charAt(0)?(s=n[a].apply(n,h),s!==n&&s!==e?(l=s&&s.jquery?l.pushStack(s.get()):s,!1):e):t.error("no such method '"+a+"' for "+i+" widget instance"):t.error("cannot call methods on "+i+" prior to initialization; "+"attempted to call method '"+a+"'")}):this.each(function(){var e=t.data(this,o);e?e.option(a||{})._init():t.data(this,o,new n(a,this))}),l}},t.Widget=function(){},t.Widget._childConstructors=[],t.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(e,s){s=t(s||this.defaultElement||this)[0],this.element=t(s),this.uuid=i++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=t.widget.extend({},this.options,this._getCreateOptions(),e),this.bindings=t(),this.hoverable=t(),this.focusable=t(),s!==this&&(t.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(t){t.target===s&&this.destroy()}}),this.document=t(s.style?s.ownerDocument:s.document||s),this.window=t(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:t.noop,_getCreateEventData:t.noop,_create:t.noop,_init:t.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:t.noop,widget:function(){return this.element},option:function(i,s){var n,o,a,r=i;if(0===arguments.length)return t.widget.extend({},this.options);if("string"==typeof i)if(r={},n=i.split("."),i=n.shift(),n.length){for(o=r[i]=t.widget.extend({},this.options[i]),a=0;n.length-1>a;a++)o[n[a]]=o[n[a]]||{},o=o[n[a]];if(i=n.pop(),s===e)return o[i]===e?null:o[i];o[i]=s}else{if(s===e)return this.options[i]===e?null:this.options[i];r[i]=s}return this._setOptions(r),this},_setOptions:function(t){var e;for(e in t)this._setOption(e,t[e]);return this},_setOption:function(t,e){return this.options[t]=e,"disabled"===t&&(this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!e).attr("aria-disabled",e),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_on:function(i,s,n){var o,a=this;"boolean"!=typeof i&&(n=s,s=i,i=!1),n?(s=o=t(s),this.bindings=this.bindings.add(s)):(n=s,s=this.element,o=this.widget()),t.each(n,function(n,r){function h(){return i||a.options.disabled!==!0&&!t(this).hasClass("ui-state-disabled")?("string"==typeof r?a[r]:r).apply(a,arguments):e}"string"!=typeof r&&(h.guid=r.guid=r.guid||h.guid||t.guid++);var l=n.match(/^(\w+)\s*(.*)$/),c=l[1]+a.eventNamespace,u=l[2];u?o.delegate(u,c,h):s.bind(c,h)})},_off:function(t,e){e=(e||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,t.unbind(e).undelegate(e)},_delay:function(t,e){function i(){return("string"==typeof t?s[t]:t).apply(s,arguments)}var s=this;return setTimeout(i,e||0)},_hoverable:function(e){this.hoverable=this.hoverable.add(e),this._on(e,{mouseenter:function(e){t(e.currentTarget).addClass("ui-state-hover")},mouseleave:function(e){t(e.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(e){this.focusable=this.focusable.add(e),this._on(e,{focusin:function(e){t(e.currentTarget).addClass("ui-state-focus")},focusout:function(e){t(e.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(e,i,s){var n,o,a=this.options[e];if(s=s||{},i=t.Event(i),i.type=(e===this.widgetEventPrefix?e:this.widgetEventPrefix+e).toLowerCase(),i.target=this.element[0],o=i.originalEvent)for(n in o)n in i||(i[n]=o[n]);return this.element.trigger(i,s),!(t.isFunction(a)&&a.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},t.each({show:"fadeIn",hide:"fadeOut"},function(e,i){t.Widget.prototype["_"+e]=function(s,n,o){"string"==typeof n&&(n={effect:n});var a,r=n?n===!0||"number"==typeof n?i:n.effect||i:e;n=n||{},"number"==typeof n&&(n={duration:n}),a=!t.isEmptyObject(n),n.complete=o,n.delay&&s.delay(n.delay),a&&t.effects&&t.effects.effect[r]?s[e](n):r!==e&&s[r]?s[r](n.duration,n.easing,o):s.queue(function(i){t(this)[e](),o&&o.call(s[0]),i()})}})})(jQuery);(function(t){var e=!1;t(document).mouseup(function(){e=!1}),t.widget("ui.mouse",{version:"1.10.3",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var e=this;this.element.bind("mousedown."+this.widgetName,function(t){return e._mouseDown(t)}).bind("click."+this.widgetName,function(i){return!0===t.data(i.target,e.widgetName+".preventClickEvent")?(t.removeData(i.target,e.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):undefined}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&t(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(i){if(!e){this._mouseStarted&&this._mouseUp(i),this._mouseDownEvent=i;var s=this,n=1===i.which,a="string"==typeof this.options.cancel&&i.target.nodeName?t(i.target).closest(this.options.cancel).length:!1;return n&&!a&&this._mouseCapture(i)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){s.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(i)&&this._mouseDelayMet(i)&&(this._mouseStarted=this._mouseStart(i)!==!1,!this._mouseStarted)?(i.preventDefault(),!0):(!0===t.data(i.target,this.widgetName+".preventClickEvent")&&t.removeData(i.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(t){return s._mouseMove(t)},this._mouseUpDelegate=function(t){return s._mouseUp(t)},t(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),i.preventDefault(),e=!0,!0)):!0}},_mouseMove:function(e){return t.ui.ie&&(!document.documentMode||9>document.documentMode)&&!e.button?this._mouseUp(e):this._mouseStarted?(this._mouseDrag(e),e.preventDefault()):(this._mouseDistanceMet(e)&&this._mouseDelayMet(e)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,e)!==!1,this._mouseStarted?this._mouseDrag(e):this._mouseUp(e)),!this._mouseStarted)},_mouseUp:function(e){return t(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,e.target===this._mouseDownEvent.target&&t.data(e.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(e)),!1},_mouseDistanceMet:function(t){return Math.max(Math.abs(this._mouseDownEvent.pageX-t.pageX),Math.abs(this._mouseDownEvent.pageY-t.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}})})(jQuery);(function(t){function e(t,e,i){return t>e&&e+i>t}function i(t){return/left|right/.test(t.css("float"))||/inline|table-cell/.test(t.css("display"))}t.widget("ui.sortable",t.ui.mouse,{version:"1.10.3",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_create:function(){var t=this.options;this.containerCache={},this.element.addClass("ui-sortable"),this.refresh(),this.floating=this.items.length?"x"===t.axis||i(this.items[0].item):!1,this.offset=this.element.offset(),this._mouseInit(),this.ready=!0},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled"),this._mouseDestroy();for(var t=this.items.length-1;t>=0;t--)this.items[t].item.removeData(this.widgetName+"-item");return this},_setOption:function(e,i){"disabled"===e?(this.options[e]=i,this.widget().toggleClass("ui-sortable-disabled",!!i)):t.Widget.prototype._setOption.apply(this,arguments)},_mouseCapture:function(e,i){var s=null,n=!1,o=this;return this.reverting?!1:this.options.disabled||"static"===this.options.type?!1:(this._refreshItems(e),t(e.target).parents().each(function(){return t.data(this,o.widgetName+"-item")===o?(s=t(this),!1):undefined}),t.data(e.target,o.widgetName+"-item")===o&&(s=t(e.target)),s?!this.options.handle||i||(t(this.options.handle,s).find("*").addBack().each(function(){this===e.target&&(n=!0)}),n)?(this.currentItem=s,this._removeCurrentsFromItems(),!0):!1:!1)},_mouseStart:function(e,i,s){var n,o,a=this.options;if(this.currentContainer=this,this.refreshPositions(),this.helper=this._createHelper(e),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},t.extend(this.offset,{click:{left:e.pageX-this.offset.left,top:e.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(e),this.originalPageX=e.pageX,this.originalPageY=e.pageY,a.cursorAt&&this._adjustOffsetFromHelper(a.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!==this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),a.containment&&this._setContainment(),a.cursor&&"auto"!==a.cursor&&(o=this.document.find("body"),this.storedCursor=o.css("cursor"),o.css("cursor",a.cursor),this.storedStylesheet=t("<style>*{ cursor: "+a.cursor+" !important; }</style>").appendTo(o)),a.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",a.opacity)),a.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",a.zIndex)),this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",e,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions(),!s)for(n=this.containers.length-1;n>=0;n--)this.containers[n]._trigger("activate",e,this._uiHash(this));return t.ui.ddmanager&&(t.ui.ddmanager.current=this),t.ui.ddmanager&&!a.dropBehaviour&&t.ui.ddmanager.prepareOffsets(this,e),this.dragging=!0,this.helper.addClass("ui-sortable-helper"),this._mouseDrag(e),!0},_mouseDrag:function(e){var i,s,n,o,a=this.options,r=!1;for(this.position=this._generatePosition(e),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll&&(this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-e.pageY<a.scrollSensitivity?this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop+a.scrollSpeed:e.pageY-this.overflowOffset.top<a.scrollSensitivity&&(this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop-a.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-e.pageX<a.scrollSensitivity?this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft+a.scrollSpeed:e.pageX-this.overflowOffset.left<a.scrollSensitivity&&(this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft-a.scrollSpeed)):(e.pageY-t(document).scrollTop()<a.scrollSensitivity?r=t(document).scrollTop(t(document).scrollTop()-a.scrollSpeed):t(window).height()-(e.pageY-t(document).scrollTop())<a.scrollSensitivity&&(r=t(document).scrollTop(t(document).scrollTop()+a.scrollSpeed)),e.pageX-t(document).scrollLeft()<a.scrollSensitivity?r=t(document).scrollLeft(t(document).scrollLeft()-a.scrollSpeed):t(window).width()-(e.pageX-t(document).scrollLeft())<a.scrollSensitivity&&(r=t(document).scrollLeft(t(document).scrollLeft()+a.scrollSpeed))),r!==!1&&t.ui.ddmanager&&!a.dropBehaviour&&t.ui.ddmanager.prepareOffsets(this,e)),this.positionAbs=this._convertPositionTo("absolute"),this.options.axis&&"y"===this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"===this.options.axis||(this.helper[0].style.top=this.position.top+"px"),i=this.items.length-1;i>=0;i--)if(s=this.items[i],n=s.item[0],o=this._intersectsWithPointer(s),o&&s.instance===this.currentContainer&&n!==this.currentItem[0]&&this.placeholder[1===o?"next":"prev"]()[0]!==n&&!t.contains(this.placeholder[0],n)&&("semi-dynamic"===this.options.type?!t.contains(this.element[0],n):!0)){if(this.direction=1===o?"down":"up","pointer"!==this.options.tolerance&&!this._intersectsWithSides(s))break;this._rearrange(e,s),this._trigger("change",e,this._uiHash());break}return this._contactContainers(e),t.ui.ddmanager&&t.ui.ddmanager.drag(this,e),this._trigger("sort",e,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(e,i){if(e){if(t.ui.ddmanager&&!this.options.dropBehaviour&&t.ui.ddmanager.drop(this,e),this.options.revert){var s=this,n=this.placeholder.offset(),o=this.options.axis,a={};o&&"x"!==o||(a.left=n.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollLeft)),o&&"y"!==o||(a.top=n.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollTop)),this.reverting=!0,t(this.helper).animate(a,parseInt(this.options.revert,10)||500,function(){s._clear(e)})}else this._clear(e,i);return!1}},cancel:function(){if(this.dragging){this._mouseUp({target:null}),"original"===this.options.helper?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var e=this.containers.length-1;e>=0;e--)this.containers[e]._trigger("deactivate",null,this._uiHash(this)),this.containers[e].containerCache.over&&(this.containers[e]._trigger("out",null,this._uiHash(this)),this.containers[e].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),"original"!==this.options.helper&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),t.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?t(this.domPosition.prev).after(this.currentItem):t(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(e){var i=this._getItemsAsjQuery(e&&e.connected),s=[];return e=e||{},t(i).each(function(){var i=(t(e.item||this).attr(e.attribute||"id")||"").match(e.expression||/(.+)[\-=_](.+)/);i&&s.push((e.key||i[1]+"[]")+"="+(e.key&&e.expression?i[1]:i[2]))}),!s.length&&e.key&&s.push(e.key+"="),s.join("&")},toArray:function(e){var i=this._getItemsAsjQuery(e&&e.connected),s=[];return e=e||{},i.each(function(){s.push(t(e.item||this).attr(e.attribute||"id")||"")}),s},_intersectsWith:function(t){var e=this.positionAbs.left,i=e+this.helperProportions.width,s=this.positionAbs.top,n=s+this.helperProportions.height,o=t.left,a=o+t.width,r=t.top,h=r+t.height,l=this.offset.click.top,c=this.offset.click.left,u="x"===this.options.axis||s+l>r&&h>s+l,d="y"===this.options.axis||e+c>o&&a>e+c,p=u&&d;return"pointer"===this.options.tolerance||this.options.forcePointerForContainers||"pointer"!==this.options.tolerance&&this.helperProportions[this.floating?"width":"height"]>t[this.floating?"width":"height"]?p:e+this.helperProportions.width/2>o&&a>i-this.helperProportions.width/2&&s+this.helperProportions.height/2>r&&h>n-this.helperProportions.height/2},_intersectsWithPointer:function(t){var i="x"===this.options.axis||e(this.positionAbs.top+this.offset.click.top,t.top,t.height),s="y"===this.options.axis||e(this.positionAbs.left+this.offset.click.left,t.left,t.width),n=i&&s,o=this._getDragVerticalDirection(),a=this._getDragHorizontalDirection();return n?this.floating?a&&"right"===a||"down"===o?2:1:o&&("down"===o?2:1):!1},_intersectsWithSides:function(t){var i=e(this.positionAbs.top+this.offset.click.top,t.top+t.height/2,t.height),s=e(this.positionAbs.left+this.offset.click.left,t.left+t.width/2,t.width),n=this._getDragVerticalDirection(),o=this._getDragHorizontalDirection();return this.floating&&o?"right"===o&&s||"left"===o&&!s:n&&("down"===n&&i||"up"===n&&!i)},_getDragVerticalDirection:function(){var t=this.positionAbs.top-this.lastPositionAbs.top;return 0!==t&&(t>0?"down":"up")},_getDragHorizontalDirection:function(){var t=this.positionAbs.left-this.lastPositionAbs.left;return 0!==t&&(t>0?"right":"left")},refresh:function(t){return this._refreshItems(t),this.refreshPositions(),this},_connectWith:function(){var t=this.options;return t.connectWith.constructor===String?[t.connectWith]:t.connectWith},_getItemsAsjQuery:function(e){var i,s,n,o,a=[],r=[],h=this._connectWith();if(h&&e)for(i=h.length-1;i>=0;i--)for(n=t(h[i]),s=n.length-1;s>=0;s--)o=t.data(n[s],this.widgetFullName),o&&o!==this&&!o.options.disabled&&r.push([t.isFunction(o.options.items)?o.options.items.call(o.element):t(o.options.items,o.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),o]);for(r.push([t.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):t(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]),i=r.length-1;i>=0;i--)r[i][0].each(function(){a.push(this)});return t(a)},_removeCurrentsFromItems:function(){var e=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=t.grep(this.items,function(t){for(var i=0;e.length>i;i++)if(e[i]===t.item[0])return!1;return!0})},_refreshItems:function(e){this.items=[],this.containers=[this];var i,s,n,o,a,r,h,l,c=this.items,u=[[t.isFunction(this.options.items)?this.options.items.call(this.element[0],e,{item:this.currentItem}):t(this.options.items,this.element),this]],d=this._connectWith();if(d&&this.ready)for(i=d.length-1;i>=0;i--)for(n=t(d[i]),s=n.length-1;s>=0;s--)o=t.data(n[s],this.widgetFullName),o&&o!==this&&!o.options.disabled&&(u.push([t.isFunction(o.options.items)?o.options.items.call(o.element[0],e,{item:this.currentItem}):t(o.options.items,o.element),o]),this.containers.push(o));for(i=u.length-1;i>=0;i--)for(a=u[i][1],r=u[i][0],s=0,l=r.length;l>s;s++)h=t(r[s]),h.data(this.widgetName+"-item",a),c.push({item:h,instance:a,width:0,height:0,left:0,top:0})},refreshPositions:function(e){this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset());var i,s,n,o;for(i=this.items.length-1;i>=0;i--)s=this.items[i],s.instance!==this.currentContainer&&this.currentContainer&&s.item[0]!==this.currentItem[0]||(n=this.options.toleranceElement?t(this.options.toleranceElement,s.item):s.item,e||(s.width=n.outerWidth(),s.height=n.outerHeight()),o=n.offset(),s.left=o.left,s.top=o.top);if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(i=this.containers.length-1;i>=0;i--)o=this.containers[i].element.offset(),this.containers[i].containerCache.left=o.left,this.containers[i].containerCache.top=o.top,this.containers[i].containerCache.width=this.containers[i].element.outerWidth(),this.containers[i].containerCache.height=this.containers[i].element.outerHeight();return this},_createPlaceholder:function(e){e=e||this;var i,s=e.options;s.placeholder&&s.placeholder.constructor!==String||(i=s.placeholder,s.placeholder={element:function(){var s=e.currentItem[0].nodeName.toLowerCase(),n=t("<"+s+">",e.document[0]).addClass(i||e.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper");return"tr"===s?e.currentItem.children().each(function(){t("<td>&#160;</td>",e.document[0]).attr("colspan",t(this).attr("colspan")||1).appendTo(n)}):"img"===s&&n.attr("src",e.currentItem.attr("src")),i||n.css("visibility","hidden"),n},update:function(t,n){(!i||s.forcePlaceholderSize)&&(n.height()||n.height(e.currentItem.innerHeight()-parseInt(e.currentItem.css("paddingTop")||0,10)-parseInt(e.currentItem.css("paddingBottom")||0,10)),n.width()||n.width(e.currentItem.innerWidth()-parseInt(e.currentItem.css("paddingLeft")||0,10)-parseInt(e.currentItem.css("paddingRight")||0,10)))}}),e.placeholder=t(s.placeholder.element.call(e.element,e.currentItem)),e.currentItem.after(e.placeholder),s.placeholder.update(e,e.placeholder)},_contactContainers:function(s){var n,o,a,r,h,l,c,u,d,p,f=null,g=null;for(n=this.containers.length-1;n>=0;n--)if(!t.contains(this.currentItem[0],this.containers[n].element[0]))if(this._intersectsWith(this.containers[n].containerCache)){if(f&&t.contains(this.containers[n].element[0],f.element[0]))continue;f=this.containers[n],g=n}else this.containers[n].containerCache.over&&(this.containers[n]._trigger("out",s,this._uiHash(this)),this.containers[n].containerCache.over=0);if(f)if(1===this.containers.length)this.containers[g].containerCache.over||(this.containers[g]._trigger("over",s,this._uiHash(this)),this.containers[g].containerCache.over=1);else{for(a=1e4,r=null,p=f.floating||i(this.currentItem),h=p?"left":"top",l=p?"width":"height",c=this.positionAbs[h]+this.offset.click[h],o=this.items.length-1;o>=0;o--)t.contains(this.containers[g].element[0],this.items[o].item[0])&&this.items[o].item[0]!==this.currentItem[0]&&(!p||e(this.positionAbs.top+this.offset.click.top,this.items[o].top,this.items[o].height))&&(u=this.items[o].item.offset()[h],d=!1,Math.abs(u-c)>Math.abs(u+this.items[o][l]-c)&&(d=!0,u+=this.items[o][l]),a>Math.abs(u-c)&&(a=Math.abs(u-c),r=this.items[o],this.direction=d?"up":"down"));if(!r&&!this.options.dropOnEmpty)return;if(this.currentContainer===this.containers[g])return;r?this._rearrange(s,r,null,!0):this._rearrange(s,null,this.containers[g].element,!0),this._trigger("change",s,this._uiHash()),this.containers[g]._trigger("change",s,this._uiHash(this)),this.currentContainer=this.containers[g],this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[g]._trigger("over",s,this._uiHash(this)),this.containers[g].containerCache.over=1}},_createHelper:function(e){var i=this.options,s=t.isFunction(i.helper)?t(i.helper.apply(this.element[0],[e,this.currentItem])):"clone"===i.helper?this.currentItem.clone():this.currentItem;return s.parents("body").length||t("parent"!==i.appendTo?i.appendTo:this.currentItem[0].parentNode)[0].appendChild(s[0]),s[0]===this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),(!s[0].style.width||i.forceHelperSize)&&s.width(this.currentItem.width()),(!s[0].style.height||i.forceHelperSize)&&s.height(this.currentItem.height()),s},_adjustOffsetFromHelper:function(e){"string"==typeof e&&(e=e.split(" ")),t.isArray(e)&&(e={left:+e[0],top:+e[1]||0}),"left"in e&&(this.offset.click.left=e.left+this.margins.left),"right"in e&&(this.offset.click.left=this.helperProportions.width-e.right+this.margins.left),"top"in e&&(this.offset.click.top=e.top+this.margins.top),"bottom"in e&&(this.offset.click.top=this.helperProportions.height-e.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var e=this.offsetParent.offset();return"absolute"===this.cssPosition&&this.scrollParent[0]!==document&&t.contains(this.scrollParent[0],this.offsetParent[0])&&(e.left+=this.scrollParent.scrollLeft(),e.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&"html"===this.offsetParent[0].tagName.toLowerCase()&&t.ui.ie)&&(e={top:0,left:0}),{top:e.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:e.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"===this.cssPosition){var t=this.currentItem.position();return{top:t.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:t.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var e,i,s,n=this.options;"parent"===n.containment&&(n.containment=this.helper[0].parentNode),("document"===n.containment||"window"===n.containment)&&(this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,t("document"===n.containment?document:window).width()-this.helperProportions.width-this.margins.left,(t("document"===n.containment?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),/^(document|window|parent)$/.test(n.containment)||(e=t(n.containment)[0],i=t(n.containment).offset(),s="hidden"!==t(e).css("overflow"),this.containment=[i.left+(parseInt(t(e).css("borderLeftWidth"),10)||0)+(parseInt(t(e).css("paddingLeft"),10)||0)-this.margins.left,i.top+(parseInt(t(e).css("borderTopWidth"),10)||0)+(parseInt(t(e).css("paddingTop"),10)||0)-this.margins.top,i.left+(s?Math.max(e.scrollWidth,e.offsetWidth):e.offsetWidth)-(parseInt(t(e).css("borderLeftWidth"),10)||0)-(parseInt(t(e).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,i.top+(s?Math.max(e.scrollHeight,e.offsetHeight):e.offsetHeight)-(parseInt(t(e).css("borderTopWidth"),10)||0)-(parseInt(t(e).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top])},_convertPositionTo:function(e,i){i||(i=this.position);var s="absolute"===e?1:-1,n="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&t.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,o=/(html|body)/i.test(n[0].tagName);return{top:i.top+this.offset.relative.top*s+this.offset.parent.top*s-("fixed"===this.cssPosition?-this.scrollParent.scrollTop():o?0:n.scrollTop())*s,left:i.left+this.offset.relative.left*s+this.offset.parent.left*s-("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():o?0:n.scrollLeft())*s}},_generatePosition:function(e){var i,s,n=this.options,o=e.pageX,a=e.pageY,r="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&t.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,h=/(html|body)/i.test(r[0].tagName);return"relative"!==this.cssPosition||this.scrollParent[0]!==document&&this.scrollParent[0]!==this.offsetParent[0]||(this.offset.relative=this._getRelativeOffset()),this.originalPosition&&(this.containment&&(e.pageX-this.offset.click.left<this.containment[0]&&(o=this.containment[0]+this.offset.click.left),e.pageY-this.offset.click.top<this.containment[1]&&(a=this.containment[1]+this.offset.click.top),e.pageX-this.offset.click.left>this.containment[2]&&(o=this.containment[2]+this.offset.click.left),e.pageY-this.offset.click.top>this.containment[3]&&(a=this.containment[3]+this.offset.click.top)),n.grid&&(i=this.originalPageY+Math.round((a-this.originalPageY)/n.grid[1])*n.grid[1],a=this.containment?i-this.offset.click.top>=this.containment[1]&&i-this.offset.click.top<=this.containment[3]?i:i-this.offset.click.top>=this.containment[1]?i-n.grid[1]:i+n.grid[1]:i,s=this.originalPageX+Math.round((o-this.originalPageX)/n.grid[0])*n.grid[0],o=this.containment?s-this.offset.click.left>=this.containment[0]&&s-this.offset.click.left<=this.containment[2]?s:s-this.offset.click.left>=this.containment[0]?s-n.grid[0]:s+n.grid[0]:s)),{top:a-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.scrollParent.scrollTop():h?0:r.scrollTop()),left:o-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():h?0:r.scrollLeft())}},_rearrange:function(t,e,i,s){i?i[0].appendChild(this.placeholder[0]):e.item[0].parentNode.insertBefore(this.placeholder[0],"down"===this.direction?e.item[0]:e.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var n=this.counter;this._delay(function(){n===this.counter&&this.refreshPositions(!s)})},_clear:function(t,e){this.reverting=!1;var i,s=[];if(!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null,this.helper[0]===this.currentItem[0]){for(i in this._storedCSS)("auto"===this._storedCSS[i]||"static"===this._storedCSS[i])&&(this._storedCSS[i]="");this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();for(this.fromOutside&&!e&&s.push(function(t){this._trigger("receive",t,this._uiHash(this.fromOutside))}),!this.fromOutside&&this.domPosition.prev===this.currentItem.prev().not(".ui-sortable-helper")[0]&&this.domPosition.parent===this.currentItem.parent()[0]||e||s.push(function(t){this._trigger("update",t,this._uiHash())}),this!==this.currentContainer&&(e||(s.push(function(t){this._trigger("remove",t,this._uiHash())}),s.push(function(t){return function(e){t._trigger("receive",e,this._uiHash(this))}}.call(this,this.currentContainer)),s.push(function(t){return function(e){t._trigger("update",e,this._uiHash(this))}}.call(this,this.currentContainer)))),i=this.containers.length-1;i>=0;i--)e||s.push(function(t){return function(e){t._trigger("deactivate",e,this._uiHash(this))}}.call(this,this.containers[i])),this.containers[i].containerCache.over&&(s.push(function(t){return function(e){t._trigger("out",e,this._uiHash(this))}}.call(this,this.containers[i])),this.containers[i].containerCache.over=0);if(this.storedCursor&&(this.document.find("body").css("cursor",this.storedCursor),this.storedStylesheet.remove()),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex","auto"===this._storedZIndex?"":this._storedZIndex),this.dragging=!1,this.cancelHelperRemoval){if(!e){for(this._trigger("beforeStop",t,this._uiHash()),i=0;s.length>i;i++)s[i].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!1}if(e||this._trigger("beforeStop",t,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.helper[0]!==this.currentItem[0]&&this.helper.remove(),this.helper=null,!e){for(i=0;s.length>i;i++)s[i].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!0},_trigger:function(){t.Widget.prototype._trigger.apply(this,arguments)===!1&&this.cancel()},_uiHash:function(e){var i=e||this;return{helper:i.helper,placeholder:i.placeholder||t([]),position:i.position,originalPosition:i.originalPosition,offset:i.positionAbs,item:i.currentItem,sender:e?e.element:null}}})})(jQuery);

/**
 * @license AngularJS v1.0.1
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {
'use strict';

/**
 * @ngdoc overview
 * @name ngSanitize
 * @description
 */

/*
 * HTML Parser By Misko Hevery (misko@hevery.com)
 * based on:  HTML Parser By John Resig (ejohn.org)
 * Original code by Erik Arvidsson, Mozilla Public License
 * http://erik.eae.net/simplehtmlparser/simplehtmlparser.js
 *
 * // Use like so:
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 */


/**
 * @ngdoc service
 * @name ngSanitize.$sanitize
 * @function
 *
 * @description
 *   The input is sanitized by parsing the html into tokens. All safe tokens (from a whitelist) are
 *   then serialized back to properly escaped html string. This means that no unsafe input can make
 *   it into the returned string, however, since our parser is more strict than a typical browser
 *   parser, it's possible that some obscure input, which would be recognized as valid HTML by a
 *   browser, won't make it through the sanitizer.
 *
 * @param {string} html Html input.
 * @returns {string} Sanitized html.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             '<p style="color:blue">an html\n' +
             '<em onmouseover="this.textContent=\'PWN3D!\'">click here</em>\n' +
             'snippet</p>';
         }
       </script>
       <div ng-controller="Ctrl">
          Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
           <table>
             <tr>
               <td>Filter</td>
               <td>Source</td>
               <td>Rendered</td>
             </tr>
             <tr id="html-filter">
               <td>html filter</td>
               <td>
                 <pre>&lt;div ng-bind-html="snippet"&gt;<br/>&lt;/div&gt;</pre>
               </td>
               <td>
                 <div ng-bind-html="snippet"></div>
               </td>
             </tr>
             <tr id="escaped-html">
               <td>no filter</td>
               <td><pre>&lt;div ng-bind="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind="snippet"></div></td>
             </tr>
             <tr id="html-unsafe-filter">
               <td>unsafe html filter</td>
               <td><pre>&lt;div ng-bind-html-unsafe="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind-html-unsafe="snippet"></div></td>
             </tr>
           </table>
         </div>
     </doc:source>
     <doc:scenario>
       it('should sanitize the html snippet ', function() {
         expect(using('#html-filter').element('div').html()).
           toBe('<p>an html\n<em>click here</em>\nsnippet</p>');
       });

       it('should escape snippet without any filter', function() {
         expect(using('#escaped-html').element('div').html()).
           toBe("&lt;p style=\"color:blue\"&gt;an html\n" +
                "&lt;em onmouseover=\"this.textContent='PWN3D!'\"&gt;click here&lt;/em&gt;\n" +
                "snippet&lt;/p&gt;");
       });

       it('should inline raw snippet if filtered as unsafe', function() {
         expect(using('#html-unsafe-filter').element("div").html()).
           toBe("<p style=\"color:blue\">an html\n" +
                "<em onmouseover=\"this.textContent='PWN3D!'\">click here</em>\n" +
                "snippet</p>");
       });

       it('should update', function() {
         input('snippet').enter('new <b>text</b>');
         expect(using('#html-filter').binding('snippet')).toBe('new <b>text</b>');
         expect(using('#escaped-html').element('div').html()).toBe("new &lt;b&gt;text&lt;/b&gt;");
         expect(using('#html-unsafe-filter').binding("snippet")).toBe('new <b>text</b>');
       });
     </doc:scenario>
   </doc:example>
 */
var $sanitize = function(html) {
  var buf = [];
    htmlParser(html, htmlSanitizeWriter(buf));
    return buf.join('');
};


// Regular Expressions for parsing tags and attributes
var START_TAG_REGEXP = /^<\s*([\w:-]+)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*>/,
  END_TAG_REGEXP = /^<\s*\/\s*([\w:-]+)[^>]*>/,
  ATTR_REGEXP = /([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,
  BEGIN_TAG_REGEXP = /^</,
  BEGING_END_TAGE_REGEXP = /^<\s*\//,
  COMMENT_REGEXP = /<!--(.*?)-->/g,
  CDATA_REGEXP = /<!\[CDATA\[(.*?)]]>/g,
  URI_REGEXP = /^((ftp|https?):\/\/|mailto:|#)/,
  NON_ALPHANUMERIC_REGEXP = /([^\#-~| |!])/g; // Match everything outside of normal chars and " (quote character)


// Good source of info about elements and attributes
// http://dev.w3.org/html5/spec/Overview.html#semantics
// http://simon.html5.org/html-elements

// Safe Void Elements - HTML5
// http://dev.w3.org/html5/spec/Overview.html#void-elements
var voidElements = makeMap("area,br,col,hr,img,wbr");

// Elements that you can, intentionally, leave open (and which close themselves)
// http://dev.w3.org/html5/spec/Overview.html#optional-tags
var optionalEndTagBlockElements = makeMap("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),
    optionalEndTagInlineElements = makeMap("rp,rt"),
    optionalEndTagElements = angular.extend({}, optionalEndTagInlineElements, optionalEndTagBlockElements);

// Safe Block Elements - HTML5
var blockElements = angular.extend({}, optionalEndTagBlockElements, makeMap("address,article,aside," +
        "blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6," +
        "header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul"));

// Inline Elements - HTML5
var inlineElements = angular.extend({}, optionalEndTagInlineElements, makeMap("a,abbr,acronym,b,bdi,bdo," +
        "big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small," +
        "span,strike,strong,sub,sup,time,tt,u,var"));


// Special Elements (can contain anything)
var specialElements = makeMap("script,style");

var validElements = angular.extend({}, voidElements, blockElements, inlineElements, optionalEndTagElements);

//Attributes that have href and hence need to be sanitized
var uriAttrs = makeMap("background,cite,href,longdesc,src,usemap");
var validAttrs = angular.extend({}, uriAttrs, makeMap(
    'abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,'+
    'color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,'+
    'ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,'+
    'scope,scrolling,shape,span,start,summary,target,title,type,'+
    'valign,value,vspace,width'));

function makeMap(str) {
  var obj = {}, items = str.split(','), i;
  for (i = 0; i < items.length; i++) obj[items[i]] = true;
  return obj;
}


/**
 * @example
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 * @param {string} html string
 * @param {object} handler
 */
function htmlParser( html, handler ) {
  var index, chars, match, stack = [], last = html;
  stack.last = function() { return stack[ stack.length - 1 ]; };

  while ( html ) {
    chars = true;

    // Make sure we're not in a script or style element
    if ( !stack.last() || !specialElements[ stack.last() ] ) {

      // Comment
      if ( html.indexOf("<!--") === 0 ) {
        index = html.indexOf("-->");

        if ( index >= 0 ) {
          if (handler.comment) handler.comment( html.substring( 4, index ) );
          html = html.substring( index + 3 );
          chars = false;
        }

      // end tag
      } else if ( BEGING_END_TAGE_REGEXP.test(html) ) {
        match = html.match( END_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( END_TAG_REGEXP, parseEndTag );
          chars = false;
        }

      // start tag
      } else if ( BEGIN_TAG_REGEXP.test(html) ) {
        match = html.match( START_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( START_TAG_REGEXP, parseStartTag );
          chars = false;
        }
      }

      if ( chars ) {
        index = html.indexOf("<");

        var text = index < 0 ? html : html.substring( 0, index );
        html = index < 0 ? "" : html.substring( index );

        if (handler.chars) handler.chars( decodeEntities(text) );
      }

    } else {
      html = html.replace(new RegExp("(.*)<\\s*\\/\\s*" + stack.last() + "[^>]*>", 'i'), function(all, text){
        text = text.
          replace(COMMENT_REGEXP, "$1").
          replace(CDATA_REGEXP, "$1");

        if (handler.chars) handler.chars( decodeEntities(text) );

        return "";
      });

      parseEndTag( "", stack.last() );
    }

    if ( html == last ) {
      throw "Parse Error: " + html;
    }
    last = html;
  }

  // Clean up any remaining tags
  parseEndTag();

  function parseStartTag( tag, tagName, rest, unary ) {
    tagName = angular.lowercase(tagName);
    if ( blockElements[ tagName ] ) {
      while ( stack.last() && inlineElements[ stack.last() ] ) {
        parseEndTag( "", stack.last() );
      }
    }

    if ( optionalEndTagElements[ tagName ] && stack.last() == tagName ) {
      parseEndTag( "", tagName );
    }

    unary = voidElements[ tagName ] || !!unary;

    if ( !unary )
      stack.push( tagName );

    var attrs = {};

    rest.replace(ATTR_REGEXP, function(match, name, doubleQuotedValue, singleQoutedValue, unqoutedValue) {
      var value = doubleQuotedValue
        || singleQoutedValue
        || unqoutedValue
        || '';

      attrs[name] = decodeEntities(value);
    });
    if (handler.start) handler.start( tagName, attrs, unary );
  }

  function parseEndTag( tag, tagName ) {
    var pos = 0, i;
    tagName = angular.lowercase(tagName);
    if ( tagName )
      // Find the closest opened tag of the same type
      for ( pos = stack.length - 1; pos >= 0; pos-- )
        if ( stack[ pos ] == tagName )
          break;

    if ( pos >= 0 ) {
      // Close all the open elements, up the stack
      for ( i = stack.length - 1; i >= pos; i-- )
        if (handler.end) handler.end( stack[ i ] );

      // Remove the open elements from the stack
      stack.length = pos;
    }
  }
}

/**
 * decodes all entities into regular string
 * @param value
 * @returns {string} A string with decoded entities.
 */
var hiddenPre=document.createElement("pre");
function decodeEntities(value) {
  hiddenPre.innerHTML=value.replace(/</g,"&lt;");
  return hiddenPre.innerText || hiddenPre.textContent || '';
}

/**
 * Escapes all potentially dangerous characters, so that the
 * resulting string can be safely inserted into attribute or
 * element text.
 * @param value
 * @returns escaped text
 */
function encodeEntities(value) {
  return value.
    replace(/&/g, '&amp;').
    replace(NON_ALPHANUMERIC_REGEXP, function(value){
      return '&#' + value.charCodeAt(0) + ';';
    }).
    replace(/</g, '&lt;').
    replace(/>/g, '&gt;');
}

/**
 * create an HTML/XML writer which writes to buffer
 * @param {Array} buf use buf.jain('') to get out sanitized html string
 * @returns {object} in the form of {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * }
 */
function htmlSanitizeWriter(buf){
  var ignore = false;
  var out = angular.bind(buf, buf.push);
  return {
    start: function(tag, attrs, unary){
      tag = angular.lowercase(tag);
      if (!ignore && specialElements[tag]) {
        ignore = tag;
      }
      if (!ignore && validElements[tag] == true) {
        out('<');
        out(tag);
        angular.forEach(attrs, function(value, key){
          var lkey=angular.lowercase(key);
          if (validAttrs[lkey]==true && (uriAttrs[lkey]!==true || value.match(URI_REGEXP))) {
            out(' ');
            out(key);
            out('="');
            out(encodeEntities(value));
            out('"');
          }
        });
        out(unary ? '/>' : '>');
      }
    },
    end: function(tag){
        tag = angular.lowercase(tag);
        if (!ignore && validElements[tag] == true) {
          out('</');
          out(tag);
          out('>');
        }
        if (tag == ignore) {
          ignore = false;
        }
      },
    chars: function(chars){
        if (!ignore) {
          out(encodeEntities(chars));
        }
      }
  };
}


// define ngSanitize module and register $sanitize service
angular.module('ngSanitize', []).value('$sanitize', $sanitize);

/**
 * @ngdoc directive
 * @name ngSanitize.directive:ngBindHtml
 *
 * @description
 * Creates a binding that will sanitize the result of evaluating the `expression` with the
 * {@link ngSanitize.$sanitize $sanitize} service and innerHTML the result into the current element.
 *
 * See {@link ngSanitize.$sanitize $sanitize} docs for examples.
 *
 * @element ANY
 * @param {expression} ngBindHtml {@link guide/expression Expression} to evaluate.
 */
angular.module('ngSanitize').directive('ngBindHtml', ['$sanitize', function($sanitize) {
  return function(scope, element, attr) {
    element.addClass('ng-binding').data('$binding', attr.ngBindHtml);
    scope.$watch(attr.ngBindHtml, function(value) {
      value = $sanitize(value);
      element.html(value || '');
    });
  };
}]);
/**
 * @ngdoc filter
 * @name ngSanitize.filter:linky
 * @function
 *
 * @description
 *   Finds links in text input and turns them into html links. Supports http/https/ftp/mailto and
 *   plain email address links.
 *
 * @param {string} text Input text.
 * @returns {string} Html-linkified text.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             'Pretty text with some links:\n'+
             'http://angularjs.org/,\n'+
             'mailto:us@somewhere.org,\n'+
             'another@somewhere.org,\n'+
             'and one more: ftp://127.0.0.1/.';
         }
       </script>
       <div ng-controller="Ctrl">
       Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
       <table>
         <tr>
           <td>Filter</td>
           <td>Source</td>
           <td>Rendered</td>
         </tr>
         <tr id="linky-filter">
           <td>linky filter</td>
           <td>
             <pre>&lt;div ng-bind-html="snippet | linky"&gt;<br>&lt;/div&gt;</pre>
           </td>
           <td>
             <div ng-bind-html="snippet | linky"></div>
           </td>
         </tr>
         <tr id="escaped-html">
           <td>no filter</td>
           <td><pre>&lt;div ng-bind="snippet"&gt;<br>&lt;/div&gt;</pre></td>
           <td><div ng-bind="snippet"></div></td>
         </tr>
       </table>
     </doc:source>
     <doc:scenario>
       it('should linkify the snippet with urls', function() {
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('Pretty text with some links:&#10;' +
                '<a href="http://angularjs.org/">http://angularjs.org/</a>,&#10;' +
                '<a href="mailto:us@somewhere.org">us@somewhere.org</a>,&#10;' +
                '<a href="mailto:another@somewhere.org">another@somewhere.org</a>,&#10;' +
                'and one more: <a href="ftp://127.0.0.1/">ftp://127.0.0.1/</a>.');
       });

       it ('should not linkify snippet without the linky filter', function() {
         expect(using('#escaped-html').binding('snippet')).
           toBe("Pretty text with some links:\n" +
                "http://angularjs.org/,\n" +
                "mailto:us@somewhere.org,\n" +
                "another@somewhere.org,\n" +
                "and one more: ftp://127.0.0.1/.");
       });

       it('should update', function() {
         input('snippet').enter('new http://link.');
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('new <a href="http://link">http://link</a>.');
         expect(using('#escaped-html').binding('snippet')).toBe('new http://link.');
       });
     </doc:scenario>
   </doc:example>
 */
angular.module('ngSanitize').filter('linky', function() {
  var LINKY_URL_REGEXP = /((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s\.\;\,\(\)\{\}\<\>]/,
      MAILTO_REGEXP = /^mailto:/;

  return function(text) {
    if (!text) return text;
    var match;
    var raw = text;
    var html = [];
    // TODO(vojta): use $sanitize instead
    var writer = htmlSanitizeWriter(html);
    var url;
    var i;
    while ((match = raw.match(LINKY_URL_REGEXP))) {
      // We can not end in these as they are sometimes found at the end of the sentence
      url = match[0];
      // if we did not match ftp/http/mailto then assume mailto
      if (match[2] == match[3]) url = 'mailto:' + url;
      i = match.index;
      writer.chars(raw.substr(0, i));
      writer.start('a', {href:url});
      writer.chars(match[0].replace(MAILTO_REGEXP, ''));
      writer.end('a');
      raw = raw.substring(i + match[0].length);
    }
    writer.chars(raw);
    return html.join('');
  };
});

})(window, window.angular);

angular.module("ui.bootstrap",["ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.position","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(t,e,n){function o(t){for(var e in t)if(void 0!==a.style[e])return t[e]}var i=function(o,a,r){r=r||{};var s=t.defer(),l=i[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){o.unbind(l,c),s.resolve(o)})};return l&&o.bind(l,c),e(function(){angular.isString(a)?o.addClass(a):angular.isFunction(a)?a(o):angular.isObject(a)&&o.css(a),l||s.resolve(o)}),s.promise.cancel=function(){l&&o.unbind(l,c),s.reject("Transition cancelled")},s.promise},a=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},s={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return i.transitionEndEventName=o(r),i.animationEndEventName=o(s),i}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(t){var e=function(t,e,n){e.removeClass("collapse"),e.css({height:n}),e[0].offsetWidth,e.addClass("collapse")};return{link:function(n,o,i){var a,r=!0;n.$watch(function(){return o[0].scrollHeight},function(){0!==o[0].scrollHeight&&(a||(r?e(n,o,o[0].scrollHeight+"px"):e(n,o,"auto")))}),n.$watch(i.collapse,function(t){t?u():c()});var s,l=function(e){return s&&s.cancel(),s=t(o,e),s.then(function(){s=void 0},function(){s=void 0}),s},c=function(){r?(r=!1,a||e(n,o,"auto")):l({height:o[0].scrollHeight+"px"}).then(function(){a||e(n,o,"auto")}),a=!1},u=function(){a=!0,r?(r=!1,e(n,o,0)):(e(n,o,o[0].scrollHeight+"px"),l({height:"0"}))}}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(t,e,n){this.groups=[],this.closeOthers=function(o){var i=angular.isDefined(e.closeOthers)?t.$eval(e.closeOthers):n.closeOthers;i&&angular.forEach(this.groups,function(t){t!==o&&(t.isOpen=!1)})},this.addGroup=function(t){var e=this;this.groups.push(t),t.$on("$destroy",function(){e.removeGroup(t)})},this.removeGroup=function(t){var e=this.groups.indexOf(t);-1!==e&&this.groups.splice(this.groups.indexOf(t),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse","$transition","$timeout",function(t){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:["$scope",function(){this.setHeading=function(t){this.heading=t}}],link:function(e,n,o,i){var a,r;i.addGroup(e),e.isOpen=!1,o.isOpen&&(a=t(o.isOpen),r=a.assign,e.$watch(function(){return a(e.$parent)},function(t){e.isOpen=t}),e.isOpen=a?a(e.$parent):!1),e.$watch("isOpen",function(t){t&&i.closeOthers(e),r&&r(e.$parent,t)})}}}]).directive("accordionHeading",function(){return{restrict:"E",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(t,e,n){return function(t,e,o,i){i.setHeading(n(t,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(t,e,n,o){t.$watch(function(){return o[n.accordionTransclude]},function(t){t&&(e.html(""),e.append(t))})}}}),angular.module("ui.bootstrap.alert",[]).directive("alert",function(){return{restrict:"EA",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"},link:function(t,e,n){t.closeable="close"in n}}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).directive("btnRadio",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,i,a){var r=t.$eval(i.btnRadio);t.$watch(function(){return a.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){o.hasClass(e)||t.$apply(function(){a.$setViewValue(r)})})}}}]).directive("btnCheckbox",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,i,a){var r=t.$eval(i.btnCheckboxTrue),s=t.$eval(i.btnCheckboxFalse);r=angular.isDefined(r)?r:!0,s=angular.isDefined(s)?s:!1,t.$watch(function(){return a.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){t.$apply(function(){a.$setViewValue(o.hasClass(e)?s:r)})})}}}]),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$transition","$q",function(t,e,n){function o(){function n(){a?(t.next(),o()):t.pause()}i&&e.cancel(i);var r=+t.interval;!isNaN(r)&&r>=0&&(i=e(n,r))}var i,a,r=this,s=r.slides=[],l=-1;r.currentSlide=null,r.select=function(i,a){function c(){r.currentSlide&&angular.isString(a)&&!t.noTransition&&i.$element?(i.$element.addClass(a),i.$element[0].offsetWidth=i.$element[0].offsetWidth,angular.forEach(s,function(t){angular.extend(t,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(i,{direction:a,active:!0,entering:!0}),angular.extend(r.currentSlide||{},{direction:a,leaving:!0}),t.$currentTransition=n(i.$element,{}),function(e,n){t.$currentTransition.then(function(){u(e,n)},function(){u(e,n)})}(i,r.currentSlide)):u(i,r.currentSlide),r.currentSlide=i,l=p,o()}function u(e,n){angular.extend(e,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(n||{},{direction:"",active:!1,leaving:!1,entering:!1}),t.$currentTransition=null}var p=s.indexOf(i);void 0===a&&(a=p>l?"next":"prev"),i&&i!==r.currentSlide&&(t.$currentTransition?(t.$currentTransition.cancel(),e(c)):c())},r.indexOfSlide=function(t){return s.indexOf(t)},t.next=function(){var t=(l+1)%s.length;return r.select(s[t],"next")},t.prev=function(){var t=0>l-1?s.length-1:l-1;return r.select(s[t],"prev")},t.select=function(t){r.select(t)},t.isActive=function(t){return r.currentSlide===t},t.slides=function(){return s},t.$watch("interval",o),t.play=function(){a||(a=!0,o())},t.pause=function(){a=!1,i&&e.cancel(i)},r.addSlide=function(e,n){e.$element=n,s.push(e),1===s.length||e.active?(r.select(s[s.length-1]),1==s.length&&t.play()):e.active=!1},r.removeSlide=function(t){var e=s.indexOf(t);s.splice(e,1),s.length>0&&t.active&&(e>=s.length?r.select(s[e-1]):r.select(s[e]))}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"="}}}]).directive("slide",[function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"="},link:function(t,e,n,o){o.addSlide(t,e),t.$on("$destroy",function(){o.removeSlide(t)}),t.$watch("active",function(e){e&&o.select(t)})}}}]);var dialogModule=angular.module("ui.bootstrap.dialog",["ui.bootstrap.transition"]);dialogModule.controller("MessageBoxController",["$scope","dialog","model",function(t,e,n){t.title=n.title,t.message=n.message,t.buttons=n.buttons,t.close=function(t){e.close(t)}}]),dialogModule.provider("$dialog",function(){var t={backdrop:!0,dialogClass:"modal",backdropClass:"modal-backdrop",transitionClass:"fade",triggerClass:"in",dialogOpenClass:"modal-open",resolve:{},backdropFade:!1,dialogFade:!1,keyboard:!0,backdropClick:!0},e={},n={value:0};this.options=function(t){e=t},this.$get=["$http","$document","$compile","$rootScope","$controller","$templateCache","$q","$transition","$injector",function(o,i,a,r,s,l,c,u,p){function d(t){var e=angular.element("<div>");return e.addClass(t),e}function f(n){var o=this,i=this.options=angular.extend({},t,e,n);this._open=!1,this.backdropEl=d(i.backdropClass),i.backdropFade&&(this.backdropEl.addClass(i.transitionClass),this.backdropEl.removeClass(i.triggerClass)),this.modalEl=d(i.dialogClass),i.dialogFade&&(this.modalEl.addClass(i.transitionClass),this.modalEl.removeClass(i.triggerClass)),this.handledEscapeKey=function(t){27===t.which&&(o.close(),t.preventDefault(),o.$scope.$apply())},this.handleBackDropClick=function(t){o.close(),t.preventDefault(),o.$scope.$apply()},this.handleLocationChange=function(){o.close()}}var h=i.find("body");return f.prototype.isOpen=function(){return this._open},f.prototype.open=function(t,e){var n=this,o=this.options;if(t&&(o.templateUrl=t),e&&(o.controller=e),!o.template&&!o.templateUrl)throw Error("Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.");return this._loadResolves().then(function(t){var e=t.$scope=n.$scope=t.$scope?t.$scope:r.$new();if(n.modalEl.html(t.$template),n.options.controller){var o=s(n.options.controller,t);n.modalEl.children().data("ngControllerController",o)}a(n.modalEl)(e),n._addElementsToDom(),h.addClass(n.options.dialogOpenClass),setTimeout(function(){n.options.dialogFade&&n.modalEl.addClass(n.options.triggerClass),n.options.backdropFade&&n.backdropEl.addClass(n.options.triggerClass)}),n._bindEvents()}),this.deferred=c.defer(),this.deferred.promise},f.prototype.close=function(t){function e(t){t.removeClass(o.options.triggerClass)}function n(){o._open&&o._onCloseComplete(t)}var o=this,i=this._getFadingElements();if(h.removeClass(o.options.dialogOpenClass),i.length>0)for(var a=i.length-1;a>=0;a--)u(i[a],e).then(n);else this._onCloseComplete(t)},f.prototype._getFadingElements=function(){var t=[];return this.options.dialogFade&&t.push(this.modalEl),this.options.backdropFade&&t.push(this.backdropEl),t},f.prototype._bindEvents=function(){this.options.keyboard&&h.bind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.bind("click",this.handleBackDropClick),this.$scope.$on("$locationChangeSuccess",this.handleLocationChange)},f.prototype._unbindEvents=function(){this.options.keyboard&&h.unbind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.unbind("click",this.handleBackDropClick)},f.prototype._onCloseComplete=function(t){this._removeElementsFromDom(),this._unbindEvents(),this.deferred.resolve(t)},f.prototype._addElementsToDom=function(){h.append(this.modalEl),this.options.backdrop&&(0===n.value&&h.append(this.backdropEl),n.value++),this._open=!0},f.prototype._removeElementsFromDom=function(){this.modalEl.remove(),this.options.backdrop&&(n.value--,0===n.value&&this.backdropEl.remove()),this._open=!1},f.prototype._loadResolves=function(){var t,e=[],n=[],i=this;return this.options.template?t=c.when(this.options.template):this.options.templateUrl&&(t=o.get(this.options.templateUrl,{cache:l}).then(function(t){return t.data})),angular.forEach(this.options.resolve||[],function(t,o){n.push(o),e.push(angular.isString(t)?p.get(t):p.invoke(t))}),n.push("$template"),e.push(t),c.all(e).then(function(t){var e={};return angular.forEach(t,function(t,o){e[n[o]]=t}),e.dialog=i,e})},{dialog:function(t){return new f(t)},messageBox:function(t,e,n){return new f({templateUrl:"template/dialog/message.html",controller:"MessageBoxController",resolve:{model:function(){return{title:t,message:e,buttons:n}}}})}}}]}),angular.module("ui.bootstrap.dropdownToggle",[]).directive("dropdownToggle",["$document","$location","$window",function(t){var e=null,n=angular.noop;return{restrict:"CA",link:function(o,i){o.$watch("$location.path",function(){n()}),i.parent().bind("click",function(){n()}),i.bind("click",function(o){o.preventDefault(),o.stopPropagation();var a=i===e;e&&n(),a||(i.parent().addClass("open"),e=i,n=function(o){o&&(o.preventDefault(),o.stopPropagation()),t.unbind("click",n),i.parent().removeClass("open"),n=angular.noop,e=null},t.bind("click",n))})}}}]),angular.module("ui.bootstrap.modal",["ui.bootstrap.dialog"]).directive("modal",["$parse","$dialog",function(t,e){return{restrict:"EA",terminal:!0,link:function(n,o,i){var a,r=angular.extend({},n.$eval(i.uiOptions||i.bsOptions||i.options)),s=i.modal||i.show;r=angular.extend(r,{template:o.html(),resolve:{$scope:function(){return n}}});var l=e.dialog(r);o.remove(),a=i.close?function(){t(i.close)(n)}:function(){angular.isFunction(t(s).assign)&&t(s).assign(n,!1)},n.$watch(s,function(t){t?l.open().then(function(){a()}):l.isOpen()&&l.close()})}}}]),angular.module("ui.bootstrap.pagination",[]).constant("paginationConfig",{boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last"}).directive("pagination",["paginationConfig",function(t){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",maxSize:"=",onSelectPage:"&"},templateUrl:"template/pagination/pagination.html",replace:!0,link:function(e,n,o){function i(t,e,n,o){return{number:t,text:e,active:n,disabled:o}}var a=angular.isDefined(o.boundaryLinks)?e.$eval(o.boundaryLinks):t.boundaryLinks,r=angular.isDefined(o.directionLinks)?e.$eval(o.directionLinks):t.directionLinks,s=angular.isDefined(o.firstText)?o.firstText:t.firstText,l=angular.isDefined(o.previousText)?o.previousText:t.previousText,c=angular.isDefined(o.nextText)?o.nextText:t.nextText,u=angular.isDefined(o.lastText)?o.lastText:t.lastText;e.$watch("numPages + currentPage + maxSize",function(){e.pages=[];var t=1,n=e.numPages;e.maxSize&&e.maxSize<e.numPages&&(t=Math.max(e.currentPage-Math.floor(e.maxSize/2),1),n=t+e.maxSize-1,n>e.numPages&&(n=e.numPages,t=n-e.maxSize+1));for(var o=t;n>=o;o++){var p=i(o,o,e.isActive(o),!1);e.pages.push(p)}if(r){var d=i(e.currentPage-1,l,!1,e.noPrevious());e.pages.unshift(d);var f=i(e.currentPage+1,c,!1,e.noNext());e.pages.push(f)}if(a){var h=i(1,s,!1,e.noPrevious());e.pages.unshift(h);var g=i(e.numPages,u,!1,e.noNext());e.pages.push(g)}e.currentPage>e.numPages&&e.selectPage(e.numPages)}),e.noPrevious=function(){return 1===e.currentPage},e.noNext=function(){return e.currentPage===e.numPages},e.isActive=function(t){return e.currentPage===t},e.selectPage=function(t){!e.isActive(t)&&t>0&&e.numPages>=t&&(e.currentPage=t,e.onSelectPage({page:t}))}}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(t,e){function n(t,n){return t.currentStyle?t.currentStyle[n]:e.getComputedStyle?e.getComputedStyle(t)[n]:t.style[n]}function o(t){return"static"===(n(t,"position")||"static")}var i=function(e){for(var n=t[0],i=e.offsetParent||n;i&&i!==n&&o(i);)i=i.offsetParent;return i||n};return{position:function(e){var n=this.offset(e),o={top:0,left:0},a=i(e[0]);return a!=t[0]&&(o=this.offset(angular.element(a)),o.top+=a.clientTop,o.left+=a.clientLeft),{width:e.prop("offsetWidth"),height:e.prop("offsetHeight"),top:n.top-o.top,left:n.left-o.left}},offset:function(n){var o=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:o.top+(e.pageYOffset||t[0].body.scrollTop),left:o.left+(e.pageXOffset||t[0].body.scrollLeft)}}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position"]).provider("$tooltip",function(){function t(t){var e=/[A-Z]/g,n="-";return t.replace(e,function(t,e){return(e?n:"")+t.toLowerCase()})}var e={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},o={};this.options=function(t){angular.extend(o,t)},this.$get=["$window","$compile","$timeout","$parse","$document","$position",function(i,a,r,s,l,c){return function(i,u,p){function d(t){var e,o;return e=t||f.trigger||p,o=angular.isDefined(f.trigger)?n[f.trigger]||e:n[e]||e,{show:e,hide:o}}var f=angular.extend({},e,o),h=t(i),g=d(void 0),m="<"+h+"-popup "+'title="{{tt_title}}" '+'content="{{tt_content}}" '+'placement="{{tt_placement}}" '+'animation="tt_animation()" '+'is-open="tt_isOpen"'+">"+"</"+h+"-popup>";return{restrict:"EA",scope:!0,link:function(t,e,n){function o(){t.tt_isOpen?h():p()}function p(){t.tt_popupDelay?y=r(v,t.tt_popupDelay):t.$apply(v)}function h(){t.$apply(function(){$()})}function v(){var n,o,i,a;if(t.tt_content){switch(b&&r.cancel(b),x.css({top:0,left:0,display:"block"}),f.appendToBody?(k=k||l.find("body"),k.append(x)):e.after(x),n=c.position(e),o=x.prop("offsetWidth"),i=x.prop("offsetHeight"),t.tt_placement){case"right":a={top:n.top+n.height/2-i/2+"px",left:n.left+n.width+"px"};break;case"bottom":a={top:n.top+n.height+"px",left:n.left+n.width/2-o/2+"px"};break;case"left":a={top:n.top+n.height/2-i/2+"px",left:n.left-o+"px"};break;default:a={top:n.top-i+"px",left:n.left+n.width/2-o/2+"px"}}x.css(a),t.tt_isOpen=!0}}function $(){t.tt_isOpen=!1,r.cancel(y),angular.isDefined(t.tt_animation)&&t.tt_animation()?b=r(function(){x.remove()},500):x.remove()}var b,y,k,x=a(m)(t);t.tt_isOpen=!1,n.$observe(i,function(e){t.tt_content=e}),n.$observe(u+"Title",function(e){t.tt_title=e}),n.$observe(u+"Placement",function(e){t.tt_placement=angular.isDefined(e)?e:f.placement}),n.$observe(u+"Animation",function(e){t.tt_animation=angular.isDefined(e)?s(e):function(){return f.animation}}),n.$observe(u+"PopupDelay",function(e){var n=parseInt(e,10);t.tt_popupDelay=isNaN(n)?f.popupDelay:n}),n.$observe(u+"Trigger",function(t){e.unbind(g.show),e.unbind(g.hide),g=d(t),g.show===g.hide?e.bind(g.show,o):(e.bind(g.show,p),e.bind(g.hide,h))})}}}}]}).directive("tooltipPopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(t){return t("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(t){return t("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$compile","$timeout","$parse","$window","$tooltip",function(t,e,n,o,i){return i("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",["ui.bootstrap.transition"]).constant("progressConfig",{animate:!0,autoType:!1,stackedTypes:["success","info","warning","danger"]}).controller("ProgressBarController",["$scope","$attrs","progressConfig",function(t,e,n){function o(t){return r[t]}var i=angular.isDefined(e.animate)?t.$eval(e.animate):n.animate,a=angular.isDefined(e.autoType)?t.$eval(e.autoType):n.autoType,r=angular.isDefined(e.stackedTypes)?t.$eval("["+e.stackedTypes+"]"):n.stackedTypes;this.makeBar=function(t,e,n){var r=angular.isObject(t)?t.value:t||0,s=angular.isObject(e)?e.value:e||0,l=angular.isObject(t)&&angular.isDefined(t.type)?t.type:a?o(n||0):null;return{from:s,to:r,type:l,animate:i}},this.addBar=function(e){t.bars.push(e),t.totalPercent+=e.to},this.clearBars=function(){t.bars=[],t.totalPercent=0},this.clearBars()}]).directive("progress",function(){return{restrict:"EA",replace:!0,controller:"ProgressBarController",scope:{value:"=",onFull:"&",onEmpty:"&"},templateUrl:"template/progressbar/progress.html",link:function(t,e,n,o){t.$watch("value",function(t,e){if(o.clearBars(),angular.isArray(t))for(var n=0,i=t.length;i>n;n++)o.addBar(o.makeBar(t[n],e[n],n));else o.addBar(o.makeBar(t,e))},!0),t.$watch("totalPercent",function(e){e>=100?t.onFull():0>=e&&t.onEmpty()},!0)}}}).directive("progressbar",["$transition",function(t){return{restrict:"EA",replace:!0,scope:{width:"=",old:"=",type:"=",animate:"="},templateUrl:"template/progressbar/bar.html",link:function(e,n){e.$watch("width",function(o){e.animate?(n.css("width",e.old+"%"),t(n,{width:o+"%"})):n.css("width",o+"%")})}}}]),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5}).directive("rating",["ratingConfig","$parse",function(t,e){return{restrict:"EA",scope:{value:"="},templateUrl:"template/rating/rating.html",replace:!0,link:function(n,o,i){var a=angular.isDefined(i.max)?n.$eval(i.max):t.max;n.range=[];for(var r=1;a>=r;r++)n.range.push(r);n.rate=function(t){n.readonly||(n.value=t)},n.enter=function(t){n.readonly||(n.val=t)},n.reset=function(){n.val=angular.copy(n.value)},n.reset(),n.$watch("value",function(t){n.val=t}),n.readonly=!1,i.readonly&&n.$parent.$watch(e(i.readonly),function(t){n.readonly=!!t})}}}]),angular.module("ui.bootstrap.tabs",[]).controller("TabsController",["$scope","$element",function(t){var e=t.panes=[];this.select=t.select=function(t){angular.forEach(e,function(t){t.selected=!1}),t.selected=!0},this.addPane=function(n){e.length||t.select(n),e.push(n)},this.removePane=function(n){var o=e.indexOf(n);e.splice(o,1),n.selected&&e.length>0&&t.select(e[e.length>o?o:o-1])}}]).directive("tabs",function(){return{restrict:"EA",transclude:!0,scope:{},controller:"TabsController",templateUrl:"template/tabs/tabs.html",replace:!0}}).directive("pane",["$parse",function(t){return{require:"^tabs",restrict:"EA",transclude:!0,scope:{heading:"@"},link:function(e,n,o,i){var a,r;e.selected=!1,o.active&&(a=t(o.active),r=a.assign,e.$watch(function(){return a(e.$parent)},function(t){e.selected=t}),e.selected=a?a(e.$parent):!1),e.$watch("selected",function(t){t&&i.select(e),r&&r(e.$parent,t)}),i.addPane(e),e.$on("$destroy",function(){i.removePane(e)})},templateUrl:"template/tabs/pane.html",replace:!0}}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position"]).factory("typeaheadParser",["$parse",function(t){var e=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var o=n.match(e);if(!o)throw Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:o[3],source:t(o[4]),viewMapper:t(o[2]||o[1]),modelMapper:t(o[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$document","$position","typeaheadParser",function(t,e,n,o,i,a){var r=[9,13,27,38,40];return{require:"ngModel",link:function(s,l,c,u){var p,d=s.$eval(c.typeaheadMinLength)||1,f=a.parse(c.typeahead),h=s.$eval(c.typeaheadEditable)!==!1,g=e(c.typeaheadLoading).assign||angular.noop,m=angular.element("<typeahead-popup matches='matches' active='activeIdx' select='select(activeIdx)' query='query' position='position'></typeahead-popup>"),v=s.$new();s.$on("$destroy",function(){v.$destroy()});var $=function(){v.matches=[],v.activeIdx=-1},b=function(t){var e={$viewValue:t};g(s,!0),n.when(f.source(v,e)).then(function(n){if(t===u.$viewValue){if(n.length>0){v.activeIdx=0,v.matches.length=0;for(var o=0;n.length>o;o++)e[f.itemName]=n[o],v.matches.push({label:f.viewMapper(v,e),model:n[o]});v.query=t,v.position=i.position(l),v.position.top=v.position.top+l.prop("offsetHeight")}else $();g(s,!1)}},function(){$(),g(s,!1)})};$(),v.query=void 0,u.$parsers.push(function(t){return $(),p?t:(t&&t.length>=d&&b(t),h?t:void 0)}),u.$render=function(){var t={};t[f.itemName]=p||u.$viewValue,l.val(f.viewMapper(v,t)||u.$viewValue),p=void 0},v.select=function(t){var e={};e[f.itemName]=p=v.matches[t].model,u.$setViewValue(f.modelMapper(v,e)),u.$render()},l.bind("keydown",function(t){0!==v.matches.length&&-1!==r.indexOf(t.which)&&(t.preventDefault(),40===t.which?(v.activeIdx=(v.activeIdx+1)%v.matches.length,v.$digest()):38===t.which?(v.activeIdx=(v.activeIdx?v.activeIdx:v.matches.length)-1,v.$digest()):13===t.which||9===t.which?v.$apply(function(){v.select(v.activeIdx)}):27===t.which&&(t.stopPropagation(),$(),v.$digest()))}),o.bind("click",function(){$(),v.$digest()}),l.after(t(m)(v))}}}]).directive("typeaheadPopup",function(){return{restrict:"E",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead.html",link:function(t){t.isOpen=function(){return t.matches.length>0},t.isActive=function(e){return t.active==e},t.selectActive=function(e){t.active=e},t.selectMatch=function(e){t.select({activeIdx:e})}}}}).filter("typeaheadHighlight",function(){function t(t){return t.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(e,n){return n?e.replace(RegExp(t(n),"gi"),"<strong>$&</strong>"):n}});

angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.position","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/dialog/message.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/rating/rating.html","template/tabs/pane.html","template/tabs/tabs.html","template/typeahead/typeahead.html"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(t,e,n){function o(t){for(var e in t)if(void 0!==i.style[e])return t[e]}var a=function(o,i,r){r=r||{};var l=t.defer(),s=a[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){o.unbind(s,c),l.resolve(o)})};return s&&o.bind(s,c),e(function(){angular.isString(i)?o.addClass(i):angular.isFunction(i)?i(o):angular.isObject(i)&&o.css(i),s||l.resolve(o)}),l.promise.cancel=function(){s&&o.unbind(s,c),l.reject("Transition cancelled")},l.promise},i=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},l={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return a.transitionEndEventName=o(r),a.animationEndEventName=o(l),a}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(t){var e=function(t,e,n){e.removeClass("collapse"),e.css({height:n}),e[0].offsetWidth,e.addClass("collapse")};return{link:function(n,o,a){var i,r=!0;n.$watch(function(){return o[0].scrollHeight},function(){0!==o[0].scrollHeight&&(i||(r?e(n,o,o[0].scrollHeight+"px"):e(n,o,"auto")))}),n.$watch(a.collapse,function(t){t?u():c()});var l,s=function(e){return l&&l.cancel(),l=t(o,e),l.then(function(){l=void 0},function(){l=void 0}),l},c=function(){r?(r=!1,i||e(n,o,"auto")):s({height:o[0].scrollHeight+"px"}).then(function(){i||e(n,o,"auto")}),i=!1},u=function(){i=!0,r?(r=!1,e(n,o,0)):(e(n,o,o[0].scrollHeight+"px"),s({height:"0"}))}}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(t,e,n){this.groups=[],this.closeOthers=function(o){var a=angular.isDefined(e.closeOthers)?t.$eval(e.closeOthers):n.closeOthers;a&&angular.forEach(this.groups,function(t){t!==o&&(t.isOpen=!1)})},this.addGroup=function(t){var e=this;this.groups.push(t),t.$on("$destroy",function(){e.removeGroup(t)})},this.removeGroup=function(t){var e=this.groups.indexOf(t);-1!==e&&this.groups.splice(this.groups.indexOf(t),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse","$transition","$timeout",function(t){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:["$scope",function(){this.setHeading=function(t){this.heading=t}}],link:function(e,n,o,a){var i,r;a.addGroup(e),e.isOpen=!1,o.isOpen&&(i=t(o.isOpen),r=i.assign,e.$watch(function(){return i(e.$parent)},function(t){e.isOpen=t}),e.isOpen=i?i(e.$parent):!1),e.$watch("isOpen",function(t){t&&a.closeOthers(e),r&&r(e.$parent,t)})}}}]).directive("accordionHeading",function(){return{restrict:"E",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(t,e,n){return function(t,e,o,a){a.setHeading(n(t,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(t,e,n,o){t.$watch(function(){return o[n.accordionTransclude]},function(t){t&&(e.html(""),e.append(t))})}}}),angular.module("ui.bootstrap.alert",[]).directive("alert",function(){return{restrict:"EA",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"},link:function(t,e,n){t.closeable="close"in n}}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).directive("btnRadio",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,a,i){var r=t.$eval(a.btnRadio);t.$watch(function(){return i.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){o.hasClass(e)||t.$apply(function(){i.$setViewValue(r)})})}}}]).directive("btnCheckbox",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,a,i){var r=t.$eval(a.btnCheckboxTrue),l=t.$eval(a.btnCheckboxFalse);r=angular.isDefined(r)?r:!0,l=angular.isDefined(l)?l:!1,t.$watch(function(){return i.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){t.$apply(function(){i.$setViewValue(o.hasClass(e)?l:r)})})}}}]),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$transition","$q",function(t,e,n){function o(){function n(){i?(t.next(),o()):t.pause()}a&&e.cancel(a);var r=+t.interval;!isNaN(r)&&r>=0&&(a=e(n,r))}var a,i,r=this,l=r.slides=[],s=-1;r.currentSlide=null,r.select=function(a,i){function c(){r.currentSlide&&angular.isString(i)&&!t.noTransition&&a.$element?(a.$element.addClass(i),a.$element[0].offsetWidth=a.$element[0].offsetWidth,angular.forEach(l,function(t){angular.extend(t,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(a,{direction:i,active:!0,entering:!0}),angular.extend(r.currentSlide||{},{direction:i,leaving:!0}),t.$currentTransition=n(a.$element,{}),function(e,n){t.$currentTransition.then(function(){u(e,n)},function(){u(e,n)})}(a,r.currentSlide)):u(a,r.currentSlide),r.currentSlide=a,s=p,o()}function u(e,n){angular.extend(e,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(n||{},{direction:"",active:!1,leaving:!1,entering:!1}),t.$currentTransition=null}var p=l.indexOf(a);void 0===i&&(i=p>s?"next":"prev"),a&&a!==r.currentSlide&&(t.$currentTransition?(t.$currentTransition.cancel(),e(c)):c())},r.indexOfSlide=function(t){return l.indexOf(t)},t.next=function(){var t=(s+1)%l.length;return r.select(l[t],"next")},t.prev=function(){var t=0>s-1?l.length-1:s-1;return r.select(l[t],"prev")},t.select=function(t){r.select(t)},t.isActive=function(t){return r.currentSlide===t},t.slides=function(){return l},t.$watch("interval",o),t.play=function(){i||(i=!0,o())},t.pause=function(){i=!1,a&&e.cancel(a)},r.addSlide=function(e,n){e.$element=n,l.push(e),1===l.length||e.active?(r.select(l[l.length-1]),1==l.length&&t.play()):e.active=!1},r.removeSlide=function(t){var e=l.indexOf(t);l.splice(e,1),l.length>0&&t.active&&(e>=l.length?r.select(l[e-1]):r.select(l[e]))}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"="}}}]).directive("slide",[function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"="},link:function(t,e,n,o){o.addSlide(t,e),t.$on("$destroy",function(){o.removeSlide(t)}),t.$watch("active",function(e){e&&o.select(t)})}}}]);var dialogModule=angular.module("ui.bootstrap.dialog",["ui.bootstrap.transition"]);dialogModule.controller("MessageBoxController",["$scope","dialog","model",function(t,e,n){t.title=n.title,t.message=n.message,t.buttons=n.buttons,t.close=function(t){e.close(t)}}]),dialogModule.provider("$dialog",function(){var t={backdrop:!0,dialogClass:"modal",backdropClass:"modal-backdrop",transitionClass:"fade",triggerClass:"in",dialogOpenClass:"modal-open",resolve:{},backdropFade:!1,dialogFade:!1,keyboard:!0,backdropClick:!0},e={},n={value:0};this.options=function(t){e=t},this.$get=["$http","$document","$compile","$rootScope","$controller","$templateCache","$q","$transition","$injector",function(o,a,i,r,l,s,c,u,p){function d(t){var e=angular.element("<div>");return e.addClass(t),e}function f(n){var o=this,a=this.options=angular.extend({},t,e,n);this._open=!1,this.backdropEl=d(a.backdropClass),a.backdropFade&&(this.backdropEl.addClass(a.transitionClass),this.backdropEl.removeClass(a.triggerClass)),this.modalEl=d(a.dialogClass),a.dialogFade&&(this.modalEl.addClass(a.transitionClass),this.modalEl.removeClass(a.triggerClass)),this.handledEscapeKey=function(t){27===t.which&&(o.close(),t.preventDefault(),o.$scope.$apply())},this.handleBackDropClick=function(t){o.close(),t.preventDefault(),o.$scope.$apply()},this.handleLocationChange=function(){o.close()}}var g=a.find("body");return f.prototype.isOpen=function(){return this._open},f.prototype.open=function(t,e){var n=this,o=this.options;if(t&&(o.templateUrl=t),e&&(o.controller=e),!o.template&&!o.templateUrl)throw Error("Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.");return this._loadResolves().then(function(t){var e=t.$scope=n.$scope=t.$scope?t.$scope:r.$new();if(n.modalEl.html(t.$template),n.options.controller){var o=l(n.options.controller,t);n.modalEl.children().data("ngControllerController",o)}i(n.modalEl)(e),n._addElementsToDom(),g.addClass(n.options.dialogOpenClass),setTimeout(function(){n.options.dialogFade&&n.modalEl.addClass(n.options.triggerClass),n.options.backdropFade&&n.backdropEl.addClass(n.options.triggerClass)}),n._bindEvents()}),this.deferred=c.defer(),this.deferred.promise},f.prototype.close=function(t){function e(t){t.removeClass(o.options.triggerClass)}function n(){o._open&&o._onCloseComplete(t)}var o=this,a=this._getFadingElements();if(g.removeClass(o.options.dialogOpenClass),a.length>0)for(var i=a.length-1;i>=0;i--)u(a[i],e).then(n);else this._onCloseComplete(t)},f.prototype._getFadingElements=function(){var t=[];return this.options.dialogFade&&t.push(this.modalEl),this.options.backdropFade&&t.push(this.backdropEl),t},f.prototype._bindEvents=function(){this.options.keyboard&&g.bind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.bind("click",this.handleBackDropClick),this.$scope.$on("$locationChangeSuccess",this.handleLocationChange)},f.prototype._unbindEvents=function(){this.options.keyboard&&g.unbind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.unbind("click",this.handleBackDropClick)},f.prototype._onCloseComplete=function(t){this._removeElementsFromDom(),this._unbindEvents(),this.deferred.resolve(t)},f.prototype._addElementsToDom=function(){g.append(this.modalEl),this.options.backdrop&&(0===n.value&&g.append(this.backdropEl),n.value++),this._open=!0},f.prototype._removeElementsFromDom=function(){this.modalEl.remove(),this.options.backdrop&&(n.value--,0===n.value&&this.backdropEl.remove()),this._open=!1},f.prototype._loadResolves=function(){var t,e=[],n=[],a=this;return this.options.template?t=c.when(this.options.template):this.options.templateUrl&&(t=o.get(this.options.templateUrl,{cache:s}).then(function(t){return t.data})),angular.forEach(this.options.resolve||[],function(t,o){n.push(o),e.push(angular.isString(t)?p.get(t):p.invoke(t))}),n.push("$template"),e.push(t),c.all(e).then(function(t){var e={};return angular.forEach(t,function(t,o){e[n[o]]=t}),e.dialog=a,e})},{dialog:function(t){return new f(t)},messageBox:function(t,e,n){return new f({templateUrl:"template/dialog/message.html",controller:"MessageBoxController",resolve:{model:function(){return{title:t,message:e,buttons:n}}}})}}}]}),angular.module("ui.bootstrap.dropdownToggle",[]).directive("dropdownToggle",["$document","$location","$window",function(t){var e=null,n=angular.noop;return{restrict:"CA",link:function(o,a){o.$watch("$location.path",function(){n()}),a.parent().bind("click",function(){n()}),a.bind("click",function(o){o.preventDefault(),o.stopPropagation();var i=a===e;e&&n(),i||(a.parent().addClass("open"),e=a,n=function(o){o&&(o.preventDefault(),o.stopPropagation()),t.unbind("click",n),a.parent().removeClass("open"),n=angular.noop,e=null},t.bind("click",n))})}}}]),angular.module("ui.bootstrap.modal",["ui.bootstrap.dialog"]).directive("modal",["$parse","$dialog",function(t,e){return{restrict:"EA",terminal:!0,link:function(n,o,a){var i,r=angular.extend({},n.$eval(a.uiOptions||a.bsOptions||a.options)),l=a.modal||a.show;r=angular.extend(r,{template:o.html(),resolve:{$scope:function(){return n}}});var s=e.dialog(r);o.remove(),i=a.close?function(){t(a.close)(n)}:function(){angular.isFunction(t(l).assign)&&t(l).assign(n,!1)},n.$watch(l,function(t){t?s.open().then(function(){i()}):s.isOpen()&&s.close()})}}}]),angular.module("ui.bootstrap.pagination",[]).constant("paginationConfig",{boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last"}).directive("pagination",["paginationConfig",function(t){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",maxSize:"=",onSelectPage:"&"},templateUrl:"template/pagination/pagination.html",replace:!0,link:function(e,n,o){function a(t,e,n,o){return{number:t,text:e,active:n,disabled:o}}var i=angular.isDefined(o.boundaryLinks)?e.$eval(o.boundaryLinks):t.boundaryLinks,r=angular.isDefined(o.directionLinks)?e.$eval(o.directionLinks):t.directionLinks,l=angular.isDefined(o.firstText)?o.firstText:t.firstText,s=angular.isDefined(o.previousText)?o.previousText:t.previousText,c=angular.isDefined(o.nextText)?o.nextText:t.nextText,u=angular.isDefined(o.lastText)?o.lastText:t.lastText;e.$watch("numPages + currentPage + maxSize",function(){e.pages=[];var t=1,n=e.numPages;e.maxSize&&e.maxSize<e.numPages&&(t=Math.max(e.currentPage-Math.floor(e.maxSize/2),1),n=t+e.maxSize-1,n>e.numPages&&(n=e.numPages,t=n-e.maxSize+1));for(var o=t;n>=o;o++){var p=a(o,o,e.isActive(o),!1);e.pages.push(p)}if(r){var d=a(e.currentPage-1,s,!1,e.noPrevious());e.pages.unshift(d);var f=a(e.currentPage+1,c,!1,e.noNext());e.pages.push(f)}if(i){var g=a(1,l,!1,e.noPrevious());e.pages.unshift(g);var m=a(e.numPages,u,!1,e.noNext());e.pages.push(m)}e.currentPage>e.numPages&&e.selectPage(e.numPages)}),e.noPrevious=function(){return 1===e.currentPage},e.noNext=function(){return e.currentPage===e.numPages},e.isActive=function(t){return e.currentPage===t},e.selectPage=function(t){!e.isActive(t)&&t>0&&e.numPages>=t&&(e.currentPage=t,e.onSelectPage({page:t}))}}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(t,e){function n(t,n){return t.currentStyle?t.currentStyle[n]:e.getComputedStyle?e.getComputedStyle(t)[n]:t.style[n]}function o(t){return"static"===(n(t,"position")||"static")}var a=function(e){for(var n=t[0],a=e.offsetParent||n;a&&a!==n&&o(a);)a=a.offsetParent;return a||n};return{position:function(e){var n=this.offset(e),o={top:0,left:0},i=a(e[0]);return i!=t[0]&&(o=this.offset(angular.element(i)),o.top+=i.clientTop,o.left+=i.clientLeft),{width:e.prop("offsetWidth"),height:e.prop("offsetHeight"),top:n.top-o.top,left:n.left-o.left}},offset:function(n){var o=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:o.top+(e.pageYOffset||t[0].body.scrollTop),left:o.left+(e.pageXOffset||t[0].body.scrollLeft)}}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position"]).provider("$tooltip",function(){function t(t){var e=/[A-Z]/g,n="-";return t.replace(e,function(t,e){return(e?n:"")+t.toLowerCase()})}var e={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},o={};this.options=function(t){angular.extend(o,t)},this.$get=["$window","$compile","$timeout","$parse","$document","$position",function(a,i,r,l,s,c){return function(a,u,p){function d(t){var e,o;return e=t||f.trigger||p,o=angular.isDefined(f.trigger)?n[f.trigger]||e:n[e]||e,{show:e,hide:o}}var f=angular.extend({},e,o),g=t(a),m=d(void 0),h="<"+g+"-popup "+'title="{{tt_title}}" '+'content="{{tt_content}}" '+'placement="{{tt_placement}}" '+'animation="tt_animation()" '+'is-open="tt_isOpen"'+">"+"</"+g+"-popup>";return{restrict:"EA",scope:!0,link:function(t,e,n){function o(){t.tt_isOpen?g():p()}function p(){t.tt_popupDelay?y=r(v,t.tt_popupDelay):t.$apply(v)}function g(){t.$apply(function(){b()})}function v(){var n,o,a,i;if(t.tt_content){switch($&&r.cancel($),C.css({top:0,left:0,display:"block"}),f.appendToBody?(k=k||s.find("body"),k.append(C)):e.after(C),n=c.position(e),o=C.prop("offsetWidth"),a=C.prop("offsetHeight"),t.tt_placement){case"right":i={top:n.top+n.height/2-a/2+"px",left:n.left+n.width+"px"};break;case"bottom":i={top:n.top+n.height+"px",left:n.left+n.width/2-o/2+"px"};break;case"left":i={top:n.top+n.height/2-a/2+"px",left:n.left-o+"px"};break;default:i={top:n.top-a+"px",left:n.left+n.width/2-o/2+"px"}}C.css(i),t.tt_isOpen=!0}}function b(){t.tt_isOpen=!1,r.cancel(y),angular.isDefined(t.tt_animation)&&t.tt_animation()?$=r(function(){C.remove()},500):C.remove()}var $,y,k,C=i(h)(t);t.tt_isOpen=!1,n.$observe(a,function(e){t.tt_content=e}),n.$observe(u+"Title",function(e){t.tt_title=e}),n.$observe(u+"Placement",function(e){t.tt_placement=angular.isDefined(e)?e:f.placement}),n.$observe(u+"Animation",function(e){t.tt_animation=angular.isDefined(e)?l(e):function(){return f.animation}}),n.$observe(u+"PopupDelay",function(e){var n=parseInt(e,10);t.tt_popupDelay=isNaN(n)?f.popupDelay:n}),n.$observe(u+"Trigger",function(t){e.unbind(m.show),e.unbind(m.hide),m=d(t),m.show===m.hide?e.bind(m.show,o):(e.bind(m.show,p),e.bind(m.hide,g))})}}}}]}).directive("tooltipPopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(t){return t("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(t){return t("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$compile","$timeout","$parse","$window","$tooltip",function(t,e,n,o,a){return a("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",["ui.bootstrap.transition"]).constant("progressConfig",{animate:!0,autoType:!1,stackedTypes:["success","info","warning","danger"]}).controller("ProgressBarController",["$scope","$attrs","progressConfig",function(t,e,n){function o(t){return r[t]}var a=angular.isDefined(e.animate)?t.$eval(e.animate):n.animate,i=angular.isDefined(e.autoType)?t.$eval(e.autoType):n.autoType,r=angular.isDefined(e.stackedTypes)?t.$eval("["+e.stackedTypes+"]"):n.stackedTypes;this.makeBar=function(t,e,n){var r=angular.isObject(t)?t.value:t||0,l=angular.isObject(e)?e.value:e||0,s=angular.isObject(t)&&angular.isDefined(t.type)?t.type:i?o(n||0):null;return{from:l,to:r,type:s,animate:a}},this.addBar=function(e){t.bars.push(e),t.totalPercent+=e.to},this.clearBars=function(){t.bars=[],t.totalPercent=0},this.clearBars()}]).directive("progress",function(){return{restrict:"EA",replace:!0,controller:"ProgressBarController",scope:{value:"=",onFull:"&",onEmpty:"&"},templateUrl:"template/progressbar/progress.html",link:function(t,e,n,o){t.$watch("value",function(t,e){if(o.clearBars(),angular.isArray(t))for(var n=0,a=t.length;a>n;n++)o.addBar(o.makeBar(t[n],e[n],n));else o.addBar(o.makeBar(t,e))},!0),t.$watch("totalPercent",function(e){e>=100?t.onFull():0>=e&&t.onEmpty()},!0)}}}).directive("progressbar",["$transition",function(t){return{restrict:"EA",replace:!0,scope:{width:"=",old:"=",type:"=",animate:"="},templateUrl:"template/progressbar/bar.html",link:function(e,n){e.$watch("width",function(o){e.animate?(n.css("width",e.old+"%"),t(n,{width:o+"%"})):n.css("width",o+"%")})}}}]),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5}).directive("rating",["ratingConfig","$parse",function(t,e){return{restrict:"EA",scope:{value:"="},templateUrl:"template/rating/rating.html",replace:!0,link:function(n,o,a){var i=angular.isDefined(a.max)?n.$eval(a.max):t.max;n.range=[];for(var r=1;i>=r;r++)n.range.push(r);n.rate=function(t){n.readonly||(n.value=t)},n.enter=function(t){n.readonly||(n.val=t)},n.reset=function(){n.val=angular.copy(n.value)},n.reset(),n.$watch("value",function(t){n.val=t}),n.readonly=!1,a.readonly&&n.$parent.$watch(e(a.readonly),function(t){n.readonly=!!t})}}}]),angular.module("ui.bootstrap.tabs",[]).controller("TabsController",["$scope","$element",function(t){var e=t.panes=[];this.select=t.select=function(t){angular.forEach(e,function(t){t.selected=!1}),t.selected=!0},this.addPane=function(n){e.length||t.select(n),e.push(n)},this.removePane=function(n){var o=e.indexOf(n);e.splice(o,1),n.selected&&e.length>0&&t.select(e[e.length>o?o:o-1])}}]).directive("tabs",function(){return{restrict:"EA",transclude:!0,scope:{},controller:"TabsController",templateUrl:"template/tabs/tabs.html",replace:!0}}).directive("pane",["$parse",function(t){return{require:"^tabs",restrict:"EA",transclude:!0,scope:{heading:"@"},link:function(e,n,o,a){var i,r;e.selected=!1,o.active&&(i=t(o.active),r=i.assign,e.$watch(function(){return i(e.$parent)},function(t){e.selected=t}),e.selected=i?i(e.$parent):!1),e.$watch("selected",function(t){t&&a.select(e),r&&r(e.$parent,t)}),a.addPane(e),e.$on("$destroy",function(){a.removePane(e)})},templateUrl:"template/tabs/pane.html",replace:!0}}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position"]).factory("typeaheadParser",["$parse",function(t){var e=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var o=n.match(e);if(!o)throw Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:o[3],source:t(o[4]),viewMapper:t(o[2]||o[1]),modelMapper:t(o[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$document","$position","typeaheadParser",function(t,e,n,o,a,i){var r=[9,13,27,38,40];return{require:"ngModel",link:function(l,s,c,u){var p,d=l.$eval(c.typeaheadMinLength)||1,f=i.parse(c.typeahead),g=l.$eval(c.typeaheadEditable)!==!1,m=e(c.typeaheadLoading).assign||angular.noop,h=angular.element("<typeahead-popup matches='matches' active='activeIdx' select='select(activeIdx)' query='query' position='position'></typeahead-popup>"),v=l.$new();l.$on("$destroy",function(){v.$destroy()});var b=function(){v.matches=[],v.activeIdx=-1},$=function(t){var e={$viewValue:t};m(l,!0),n.when(f.source(v,e)).then(function(n){if(t===u.$viewValue){if(n.length>0){v.activeIdx=0,v.matches.length=0;for(var o=0;n.length>o;o++)e[f.itemName]=n[o],v.matches.push({label:f.viewMapper(v,e),model:n[o]});v.query=t,v.position=a.position(s),v.position.top=v.position.top+s.prop("offsetHeight")}else b();m(l,!1)}},function(){b(),m(l,!1)})};b(),v.query=void 0,u.$parsers.push(function(t){return b(),p?t:(t&&t.length>=d&&$(t),g?t:void 0)}),u.$render=function(){var t={};t[f.itemName]=p||u.$viewValue,s.val(f.viewMapper(v,t)||u.$viewValue),p=void 0},v.select=function(t){var e={};e[f.itemName]=p=v.matches[t].model,u.$setViewValue(f.modelMapper(v,e)),u.$render()},s.bind("keydown",function(t){0!==v.matches.length&&-1!==r.indexOf(t.which)&&(t.preventDefault(),40===t.which?(v.activeIdx=(v.activeIdx+1)%v.matches.length,v.$digest()):38===t.which?(v.activeIdx=(v.activeIdx?v.activeIdx:v.matches.length)-1,v.$digest()):13===t.which||9===t.which?v.$apply(function(){v.select(v.activeIdx)}):27===t.which&&(t.stopPropagation(),b(),v.$digest()))}),o.bind("click",function(){b(),v.$digest()}),s.after(t(h)(v))}}}]).directive("typeaheadPopup",function(){return{restrict:"E",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead.html",link:function(t){t.isOpen=function(){return t.matches.length>0},t.isActive=function(e){return t.active==e},t.selectActive=function(e){t.active=e},t.selectMatch=function(e){t.select({activeIdx:e})}}}}).filter("typeaheadHighlight",function(){function t(t){return t.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(e,n){return n?e.replace(RegExp(t(n),"gi"),"<strong>$&</strong>"):n}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(t){t.put("template/accordion/accordion-group.html",'<div class="accordion-group">\n  <div class="accordion-heading" ><a class="accordion-toggle" ng-click="isOpen = !isOpen" accordion-transclude="heading">{{heading}}</a></div>\n  <div class="accordion-body" collapse="!isOpen">\n    <div class="accordion-inner" ng-transclude></div>  </div>\n</div>')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(t){t.put("template/accordion/accordion.html",'<div class="accordion" ng-transclude></div>')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(t){t.put("template/alert/alert.html","<div class='alert' ng-class='type && \"alert-\" + type'>\n    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n    <div ng-transclude></div>\n</div>\n")}]),angular.module("template/carousel/carousel.html",[]).run(["$templateCache",function(t){t.put("template/carousel/carousel.html",'<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel">\n    <ol class="carousel-indicators" ng-show="slides().length > 1">\n        <li ng-repeat="slide in slides()" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a ng-click="prev()" class="carousel-control left" ng-show="slides().length > 1">&lsaquo;</a>\n    <a ng-click="next()" class="carousel-control right" ng-show="slides().length > 1">&rsaquo;</a>\n</div>\n')}]),angular.module("template/carousel/slide.html",[]).run(["$templateCache",function(t){t.put("template/carousel/slide.html","<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item\" ng-transclude></div>\n")}]),angular.module("template/dialog/message.html",[]).run(["$templateCache",function(t){t.put("template/dialog/message.html",'<div class="modal-header">\n  <h1>{{ title }}</h1>\n</div>\n<div class="modal-body">\n    <p>{{ message }}</p>\n</div>\n<div class="modal-footer">\n  <button ng-repeat="btn in buttons" ng-click="close(btn.result)" class=btn ng-class="btn.cssClass">{{ btn.label }}</button>\n</div>\n')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(t){t.put("template/pagination/pagination.html",'<div class="pagination"><ul>\n  <li ng-repeat="page in pages" ng-class="{active: page.active, disabled: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  </ul>\n</div>\n')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(t){t.put("template/tooltip/tooltip-html-unsafe-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind-html-unsafe="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(t){t.put("template/tooltip/tooltip-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(t){t.put("template/popover/popover.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(t){t.put("template/progressbar/bar.html",'<div class="bar" ng-class=\'type && "bar-" + type\'></div>')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(t){t.put("template/progressbar/progress.html",'<div class="progress"><progressbar ng-repeat="bar in bars" width="bar.to" old="bar.from" animate="bar.animate" type="bar.type"></progressbar></div>')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(t){t.put("template/rating/rating.html",'<span ng-mouseleave="reset()">\n    <i ng-repeat="number in range" ng-mouseenter="enter(number)" ng-click="rate(number)" ng-class="{\'icon-star\': number <= val, \'icon-star-empty\': number > val}"></i>\n</span>\n')}]),angular.module("template/tabs/pane.html",[]).run(["$templateCache",function(t){t.put("template/tabs/pane.html",'<div class="tab-pane" ng-class="{active: selected}" ng-show="selected" ng-transclude></div>\n')}]),angular.module("template/tabs/tabs.html",[]).run(["$templateCache",function(t){t.put("template/tabs/tabs.html",'<div class="tabbable">\n  <ul class="nav nav-tabs">\n    <li ng-repeat="pane in panes" ng-class="{active:pane.selected}">\n      <a ng-click="select(pane)">{{pane.heading}}</a>\n    </li>\n  </ul>\n  <div class="tab-content" ng-transclude></div>\n</div>\n')}]),angular.module("template/typeahead/match.html",[]).run(["$templateCache",function(t){t.put("template/typeahead/match.html",'<a tabindex="-1" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')}]),angular.module("template/typeahead/typeahead.html",[]).run(["$templateCache",function(t){t.put("template/typeahead/typeahead.html",'<ul class="typeahead dropdown-menu" ng-style="{display: isOpen()&&\'block\' || \'none\', top: position.top+\'px\', left: position.left+\'px\'}">\n    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)">\n        <a tabindex="-1" ng-click="selectMatch($index)" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>\n    </li>\n</ul>')}]);

/*
 jQuery UI Sortable plugin wrapper

 @param [ui-sortable] {object} Options to pass to $.fn.sortable() merged onto ui.config
*/
angular.module('ui.sortable', [])
  .value('uiSortableConfig',{})
  .directive('uiSortable', [ 'uiSortableConfig',
        function(uiSortableConfig) {
        return {
          require: '?ngModel',
          link: function(scope, element, attrs, ngModel) {

              function combineCallbacks(first,second){
                  if( second && (typeof second === "function") ){
                      return function(e,ui){
                          first(e,ui);
                          second(e,ui);
                      };
                  }
                  return first;
              }

            var opts = {};

            var callbacks = {
                receive: null,
                remove:null,
                start:null,
                stop:null,
                update:null
            };

            var apply = function(e, ui) {
              if (ui.item.sortable.resort || ui.item.sortable.relocate) {
                scope.$apply();
              }
            };

            angular.extend(opts, uiSortableConfig);

            if (ngModel) {

              ngModel.$render = function() {
                element.sortable( "refresh" );
              };

              callbacks.start = function(e, ui) {
                // Save position of dragged item
                ui.item.sortable = { index: ui.item.index() };
              };

              callbacks.update = function(e, ui) {
                // For some reason the reference to ngModel in stop() is wrong
                ui.item.sortable.resort = ngModel;
              };

              callbacks.receive = function(e, ui) {
                ui.item.sortable.relocate = true;
                // added item to array into correct position and set up flag
                ngModel.$modelValue.splice(ui.item.index(), 0, ui.item.sortable.moved);
              };

              callbacks.remove = function(e, ui) {
                // copy data into item
                if (ngModel.$modelValue.length === 1) {
                  ui.item.sortable.moved = ngModel.$modelValue.splice(0, 1)[0];
                } else {
                  ui.item.sortable.moved =  ngModel.$modelValue.splice(ui.item.sortable.index, 1)[0];
                }
              };

              callbacks.stop = function(e, ui) {
                // digest all prepared changes
                if (ui.item.sortable.resort && !ui.item.sortable.relocate) {

                  // Fetch saved and current position of dropped element
                  var end, start;
                  start = ui.item.sortable.index;
                  end = ui.item.index();

                  // Reorder array and apply change to scope
                  ui.item.sortable.resort.$modelValue.splice(end, 0, ui.item.sortable.resort.$modelValue.splice(start, 1)[0]);

                }
              };

            }


              scope.$watch(attrs.uiSortable, function(newVal, oldVal){
                  angular.forEach(newVal, function(value, key){

                      if( callbacks[key] ){
                          // wrap the callback
                          value = combineCallbacks( callbacks[key], value );

                          if ( key === 'stop' ){
                              // call apply after stop
                              value = combineCallbacks( value, apply );
                          }
                      }

                      element.sortable('option', key, value);
                  });
              }, true);

              angular.forEach(callbacks, function(value, key ){

                    opts[key] = combineCallbacks(value, opts[key]);
              });

              // call apply after stop
              opts.stop = combineCallbacks( opts.stop, apply );

              // Create sortable

            element.sortable(opts);
          }
        };
      }
]);

(function($) {

function getPasteEvent() {
    var el = document.createElement('input'),
        name = 'onpaste';
    el.setAttribute(name, '');
    return (typeof el[name] === 'function')?'paste':'input';
}

var pasteEventName = getPasteEvent() + ".mask",
    ua = navigator.userAgent,
    iPhone = /iphone/i.test(ua),
    chrome = /chrome/i.test(ua),
    android=/android/i.test(ua),
    caretTimeoutId;

$.mask = {
    //Predefined character definitions
    definitions: {
        '9': "[0-9]",
        'a': "[A-Za-z]",
        '*': "[A-Za-z0-9]"
    },
    autoclear: true,
    dataName: "rawMaskFn",
    placeholder: '_'
};

$.fn.extend({
    //Helper Function for Caret positioning
    caret: function(begin, end) {
        var range;

        if (this.length === 0 || this.is(":hidden")) {
            return;
        }

        if (typeof begin == 'number') {
            end = (typeof end === 'number') ? end : begin;
            return this.each(function() {
                if (this.setSelectionRange) {
                    this.setSelectionRange(begin, end);
                } else if (this.createTextRange) {
                    range = this.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', end);
                    range.moveStart('character', begin);
                    range.select();
                }
            });
        } else {
            if (this[0].setSelectionRange) {
                begin = this[0].selectionStart;
                end = this[0].selectionEnd;
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                begin = 0 - range.duplicate().moveStart('character', -100000);
                end = begin + range.text.length;
            }
            return { begin: begin, end: end };
        }
    },
    unmask: function() {
        return this.trigger("unmask");
    },
    mask: function(mask, settings) {
        var input,
            defs,
            tests,
            partialPosition,
            firstNonMaskPos,
            len;

        if (!mask && this.length > 0) {
            input = $(this[0]);
            return input.data($.mask.dataName)();
        }
        settings = $.extend({
            autoclear: $.mask.autoclear,
            placeholder: $.mask.placeholder, // Load default placeholder
            completed: null
        }, settings);


        defs = $.mask.definitions;
        tests = [];
        partialPosition = len = mask.length;
        firstNonMaskPos = null;

        $.each(mask.split(""), function(i, c) {
            if (c == '?') {
                len--;
                partialPosition = i;
            } else if (defs[c]) {
                tests.push(new RegExp(defs[c]));
                if (firstNonMaskPos === null) {
                    firstNonMaskPos = tests.length - 1;
                }
            } else {
                tests.push(null);
            }
        });

        return this.trigger("unmask").each(function() {
            var input = $(this),
                buffer = $.map(
                mask.split(""),
                function(c, i) {
                    if (c != '?') {
                        return defs[c] ? settings.placeholder : c;
                    }
                }),
                defaultBuffer = buffer.join(''),
                focusText = input.val();

            function seekNext(pos) {
                while (++pos < len && !tests[pos]);
                return pos;
            }

            function seekPrev(pos) {
                while (--pos >= 0 && !tests[pos]);
                return pos;
            }

            function shiftL(begin,end) {
                var i,
                    j;

                if (begin<0) {
                    return;
                }

                for (i = begin, j = seekNext(end); i < len; i++) {
                    if (tests[i]) {
                        if (j < len && tests[i].test(buffer[j])) {
                            buffer[i] = buffer[j];
                            buffer[j] = settings.placeholder;
                        } else {
                            break;
                        }

                        j = seekNext(j);
                    }
                }
                writeBuffer();
                input.caret(Math.max(firstNonMaskPos, begin));
            }

            function shiftR(pos) {
                var i,
                    c,
                    j,
                    t;

                for (i = pos, c = settings.placeholder; i < len; i++) {
                    if (tests[i]) {
                        j = seekNext(i);
                        t = buffer[i];
                        buffer[i] = c;
                        if (j < len && tests[j].test(t)) {
                            c = t;
                        } else {
                            break;
                        }
                    }
                }
            }

      function blurEvent(e) {
          checkVal();

          if (input.val() != focusText)
            input.change();
      }

            function keydownEvent(e) {
                var k = e.which,
                    pos,
                    begin,
                    end;

                //backspace, delete, and escape get special treatment
                if (k === 8 || k === 46 || (iPhone && k === 127)) {
                    pos = input.caret();
                    begin = pos.begin;
                    end = pos.end;

                    if (end - begin === 0) {
                        begin=k!==46?seekPrev(begin):(end=seekNext(begin-1));
                        end=k===46?seekNext(end):end;
                    }
                    clearBuffer(begin, end);
                    shiftL(begin, end - 1);

                    e.preventDefault();
                } else if( k === 13 ) { // enter
                    blurEvent.call(this, e);
                } else if (k === 27) { // escape
                    input.val(focusText);
                    input.caret(0, checkVal());
                    e.preventDefault();
                }
            }

            function keypressEvent(e) {
                var k = e.which,
                    pos = input.caret(),
                    p,
                    c,
                    next;

                    if (k == 0) {
                        // unable to detect key pressed. Grab it from pos and adjust
                        // this is a failsafe for mobile chrome
                        // which can't detect keypress events
                        // reliably
                        if (pos.begin >= len) {
                            input.val(input.val().substr(0, len));
                            e.preventDefault();
                            return false;
                        }
                        if (pos.begin == pos.end) {
                            k = input.val().charCodeAt(pos.begin - 1);
                            pos.begin--;
                            pos.end--;
                        }
                    }

                if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
                    return;
                } else if ( k && k !== 13 ) {
                    if (pos.end - pos.begin !== 0){
                        clearBuffer(pos.begin, pos.end);
                        shiftL(pos.begin, pos.end-1);
                    }

                    p = seekNext(pos.begin - 1);
                    if (p < len) {
                        c = String.fromCharCode(k);
                        if (tests[p].test(c)) {
                            shiftR(p);

                            buffer[p] = c;
                            writeBuffer();
                            next = seekNext(p);

                            if(android){
                                setTimeout($.proxy($.fn.caret,input,next),0);
                            }else{
                                input.caret(next);
                            }

                            if (settings.completed && next >= len) {
                                settings.completed.call(input);
                            }
                        }
                    }
                    e.preventDefault();
                }
            }

            function clearBuffer(start, end) {
                var i;
                for (i = start; i < end && i < len; i++) {
                    if (tests[i]) {
                        buffer[i] = settings.placeholder;
                    }
                }
            }

            function writeBuffer() { input.val(buffer.join('')); }

            function checkVal(allow) {
                //try to place characters where they belong
                var test = input.val(),
                    lastMatch = -1,
                    i,
                    c,
                    pos;

                for (i = 0, pos = 0; i < len; i++) {
                    if (tests[i]) {
                        buffer[i] = settings.placeholder;
                        while (pos++ < test.length) {
                            c = test.charAt(pos - 1);
                            if (tests[i].test(c)) {
                                buffer[i] = c;
                                lastMatch = i;
                                break;
                            }
                        }
                        if (pos > test.length) {
                            break;
                        }
                    } else if (buffer[i] === test.charAt(pos) && i !== partialPosition) {
                        pos++;
                        lastMatch = i;
                    }
                }
                if (allow) {
                    writeBuffer();
                } else if (lastMatch + 1 < partialPosition) {
                    if (settings.autoclear || buffer.join('') === defaultBuffer) {
                        // Invalid value. Remove it and replace it with the
                        // mask, which is the default behavior.
                        input.val("");
                        clearBuffer(0, len);
                    } else {
                        // Invalid value, but we opt to show the value to the
                        // user and allow them to correct their mistake.
                        writeBuffer();
                    }
                } else {
                    writeBuffer();
                    input.val(input.val().substring(0, lastMatch + 1));
                }
                return (partialPosition ? i : firstNonMaskPos);
            }

            input.data($.mask.dataName,function(){
                return $.map(buffer, function(c, i) {
                    return tests[i]&&c!=settings.placeholder ? c : null;
                }).join('');
            });

            if (!input.attr("readonly"))
                input
                .one("unmask", function() {
                    input
                        .unbind(".mask")
                        .removeData($.mask.dataName);
                })
                .bind("focus.mask", function() {
                    clearTimeout(caretTimeoutId);
                    var pos;

                    focusText = input.val();

                    pos = checkVal();

                    caretTimeoutId = setTimeout(function(){
                        writeBuffer();
                        if (pos == mask.replace("?","").length) {
                            input.caret(0, pos);
                        } else {
                            input.caret(pos);
                        }
                    }, 10);
                })
                .bind("blur.mask", blurEvent)
                .bind("keydown.mask", keydownEvent)
                .bind("keypress.mask", keypressEvent)
                .bind(pasteEventName, function() {
                    setTimeout(function() {
                        var pos=checkVal(true);
                        input.caret(pos);
                        if (settings.completed && pos == input.val().length)
                            settings.completed.call(input);
                    }, 0);
                });
                if (chrome && android) {
                    input.bind("keyup.mask", keypressEvent);
                }
                checkVal(); //Perform initial check for existing values
        });
    }
});
})(jQuery);

"use strict";angular.module("ngTable",[]).directive("ngTable",["$compile","$q","$parse","$http","ngTableParams",function(a,b,c,d,e){return{restrict:"A",priority:1001,scope:!0,controller:["$scope","$timeout",function(a){var b;return a.params=a.params||{page:1,count:10},a.$watch("params.filter",function(c){return a.params.$liveFiltering?b(c):void 0},!0),b=function(b){return b=angular.extend(a.params,b),a.paramsModel.assign(a.$parent,new e(b)),a.params=angular.copy(b)},a.goToPage=function(c){return c>0&&a.params.page!==c&&a.params.count*(c-1)<=a.params.total?b({page:c}):void 0},a.changeCount=function(a){return b({page:1,count:a})},a.doFilter=function(){return b({page:1})},a.sortBy=function(c){var d,e;if(c.sortable)return d=a.params.sorting&&a.params.sorting[c.sortable]&&"desc"===a.params.sorting[c.sortable],e={},e[c.sortable]=d?"asc":"desc",b({sorting:e})}}],compile:function(b){var d,e;return e=0,d=[],angular.forEach(b.find("td"),function(a){var b;return b=$(a),d.push({id:e++,title:b.data("title")||b.text(),sortable:b.attr("sortable")?b.attr("sortable"):!1,filter:b.attr("filter")?c(b.attr("filter"))():!1,filterData:b.attr("filter-data")?b.attr("filter-data"):null})}),function(b,f,g){var h,i,j;return b.columns=d,h=function(a,b,c){var d,f,g,h,i,j;if(d=11,j=[],i=Math.ceil(b/c),i>1){for(j.push({type:"prev",number:Math.max(1,a-1),active:a>1}),j.push({type:"first",number:1,active:a>1}),g=Math.round((d-5)/2),h=Math.max(2,a-g),f=Math.min(i-1,a+2*g-(a-h)),h=Math.max(2,h-(2*g-(f-h))),e=h;f>=e;)e===h&&2!==e||e===f&&e!==i-1?j.push({type:"more"}):j.push({type:"page",number:e,active:a!==e}),e++;j.push({type:"last",number:i,active:a!==i}),j.push({type:"next",number:Math.min(i,a+1),active:i>a})}return j},b.$parent.$watch(g.ngTable,function(a){return angular.isUndefined(a)?void 0:(b.paramsModel=c(g.ngTable),b.pages=h(a.page,a.total,a.count),b.params=angular.copy(a))},!0),g.showFilter&&b.$parent.$watch(g.showFilter,function(a){return b.show_filter=a}),angular.forEach(d,function(a){var d;if(a.filterData){if(d=c(a.filterData)(b,{$column:a}),!angular.isObject(d)||!angular.isFunction(d.then))throw new Error("Function "+a.filterData+" must be promise");return delete a.filterData,d.then(function(b){return angular.isArray(b)||(b=[]),b.unshift({title:"-",id:""}),a.data=b})}}),f.hasClass("ng-table")?void 0:(b.templates={header:g.templateHeader?g.templateHeader:"ng-table/header.html",pagination:g.templatePagination?g.templatePagination:"ng-table/pager.html"},i=a('<thead ng-include="templates.header"></thead>')(b),j=a('<div ng-include="templates.pagination"></div>')(b),f.filter("thead").remove(),f.prepend(i).addClass("ng-table"),f.after(j))}}}}]);var __hasProp={}.hasOwnProperty;angular.module("ngTable").factory("ngTableParams",function(){var a,b;return a=function(a){return!isNaN(parseFloat(a))&&isFinite(a)},b=function(b){var c,d,e,f,g,h,i,j,k,l;c=["total","counts"],this.page=1,this.count=1,this.counts=[10,25,50,100],this.filter={},this.sorting={};for(d in b)if(i=b[d],d.indexOf("[")>=0){for(g=d.split(/\[(.*)\]/),e="",l=g.reverse(),j=0,k=l.length;k>j;j++)f=l[j],""!==f&&(h=i,i={},i[e=f]=a(h)?parseFloat(h):h);this[e]=angular.extend(this[e]||{},i[e])}else this[d]=a(b[d])?parseFloat(b[d]):b[d];return this.orderBy=function(){var a,b,c,d;c=[],d=this.sorting;for(a in d)__hasProp.call(d,a)&&(b=d[a],c.push(("asc"===b?"+":"-")+a));return c},this.url=function(a){var b,e,g,h;a=a||!1,e=a?[]:{};for(d in this)if(this.hasOwnProperty(d)){if(c.indexOf(d)>=0)continue;if(b=this[d],f=encodeURIComponent(d),"object"==typeof b)for(h in b)angular.isUndefined(b[h])||""===b[h]||(g=f+"["+encodeURIComponent(h)+"]",a?e.push(g+"="+encodeURIComponent(b[h])):e[g]=encodeURIComponent(b[h]));else angular.isFunction(b)||angular.isUndefined(b)||""===b||(a?e.push(f+"="+encodeURIComponent(b)):e[f]=encodeURIComponent(b))}return e},this}}),angular.module("ngTable").run(["$templateCache",function(a){a.put("ng-table/filters/button.html",'<button ng-click="doFilter()" ng-show="filter==\'button\'" class="btn btn-primary btn-block">Filter</button>'),a.put("ng-table/filters/select.html",'<select ng-options="data.id as data.title for data in column.data" ng-model="params.filter[name]" ng-show="filter==\'select\'" class="filter filter-select"></select>'),a.put("ng-table/filters/text.html",'<input type="text" ng-model="params.filter[name]" ng-show="filter==\'text\'" class="input-filter"/>'),a.put("ng-table/header.html",'<tr><th ng-class="{sortable: column.sortable,\'sort-asc\': params.sorting[column.sortable]==\'asc\', \'sort-desc\': params.sorting[column.sortable]==\'desc\'}" ng-click="sortBy(column)" ng-repeat="column in columns" class="header"><div>{{column.title}}</div></th></tr><tr ng-show="show_filter"><th ng-repeat="column in columns" class="filter"><form ng-submit="doFilter()"><input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/><div ng-repeat="(name, filter) in column.filter"><div ng-include="\'ng-table/filters/\' + filter + \'.html\'"></div></div></form></th></tr>'),a.put("ng-table/pager.html",'<div><ul class="pagination ng-cloak" ng-show="pages.length>0"><li ng-class="{\'disabled\': !page.active}" ng-repeat="page in pages" ng-switch="page.type"><a ng-switch-when="prev" ng-click="goToPage(page.number)" href="">«</a><a ng-switch-when="first" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="page" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="more" ng-click="goToPage(page.number)" href="">…</a><a ng-switch-when="last" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="next" ng-click="goToPage(page.number)" href="">»</a></li></ul><div ng-show="pages.length>0 && params.counts.length>0" class="btn-group pull-right"><button ng-repeat="count in params.counts" type="button" ng-class="{\'active\':params.count==count}" ng-click="changeCount(count)" class="btn btn-mini">{{count}}</button></div></div>')}]);

angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'cms.table.filters', 'cms.tinymce'])

    .run(['$templateCache', function($templateCache) {
        $templateCache.put('dialog-entity-move.html', '<div class="modal-header">'
            + '<h1>Перемещение элемента</h1>'
        + '</div>'
        + '<div class="modal-body">'
            + '<div entity-move-block loading-container="loading" class="form-horizontal">'
                + '<div class="control-group">'
                    + '<label class="control-label">Куда переносить?<br/><small>Ссылка на раздел</small></label>'
                    + '<div class="controls">'
                        + '<input type="text" required="required" class="input-xxlarge" ng-model="url">'
                    + '</div>'
                + '</div>'

                + '<div class="control-group" ng-show="node">'
                    + '<label class="control-label"></label>'
                    + '<div class="controls">'
                        + '<p>Перенести сущность [{{ item.id }}]{{ item.title }} в раздел <a ng-href="{{ node.url }}" target="_blank">{{ node.title }}</a>?</p>'
                        + '<p>Типы текущего и конечного раздела должны совпадать, иначе вы не увидите переносимый материал.</p>'
                        + '<p>'
                            + '<span class="btn btn-primary btn-small" style="width:130px; text-align:left;" ng-click="submitFirst()"><i class="icon-white icon-chevron-up"></i> Да, в начало списка</span>'
                        + '</p>'
                        + '<p>'
                            + '<span class="btn btn-primary btn-small" style="width:130px; text-align:left;" ng-click="submitLast()"><i class="icon-white icon-chevron-down"></i> Да, в конец списка</span>'
                        + '</p>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>'
        + '<div class="modal-footer">'
            + '<button ng-click="closeDialog()" class="btn">Отмена</button>'
        + '</div>');
    }])

    /**
     * Контроллер для окна переноса сущности
     */
    .controller('entityMoveBlockCtrl', ['$scope', 'item', 'entityResource', 'closeDialog', function($scope, item, entityResource, closeDialog) {
        $scope.item = item;
        $scope.entityResource = entityResource;
        $scope.closeDialog = closeDialog;
    }])

    /**
     * Блок переноса сущности
     */
    .directive('entityMoveBlock', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.url = '';
                $scope.loading = false;
                $scope.httpUrl = Routes.cms_rest_node_byurl;
                $scope.node = false;
                $scope.item = $scope.$parent.item;
                $scope.entityResource = $scope.$parent.entityResource;
                $scope.closeDialog = $scope.$parent.closeDialog;

                $scope.$watch('url', $scope.updateData);
            },

            controller: function($scope) {
                $scope.updateData = function() {
                    $scope.node = false;

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    if (!$scope.url) {
                        return;
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'GET',
                        url: $scope.httpUrl,
                        params: {url: $scope.url},
                        timeout: this.canceler.promise
                    }).success(function(response) {
                        if (response.success && !!response.node) {
                            $scope.node = response.node;
                        }
                    });
                };

                $scope.submit = function(movetype) {
                    $scope.loading = true;

                    $scope.entityResource.move({id:$scope.item.id, node:$scope.node.id, movetype:movetype}, function(response) {
                        $scope.loading = false;

                        if (!!response.id) {
                            $rootScope.$broadcast('ntf-success', 'Элемент успешно перенесен', 5000);
                            $rootScope.$broadcast('needUpdate');
                            $scope.closeDialog();
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела');
                        }
                    }, function(response, code) {
                        $scope.loading = false;

                        if (code > 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела: ' + JSON.stringify(response));
                            console.error(response);
                        }
                    });
                };

                $scope.submitFirst = function() {
                    $scope.submit('first');
                };

                $scope.submitLast = function() {
                    $scope.submit('last');
                };
            }
        }
    }])

    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', '$rootScope', '$timeout', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache, $rootScope, $timeout) {

        function quoteattr(s, preserveCR) {
            preserveCR = preserveCR ? '&#13;' : '\n';

            return ('' + s).replace(/&/g, '&amp;').replace(/'/g, '&apos;').replace(/"/g, '&quot;');
        }

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var template = '';
                var columns = $parse(tAttrs.columns)();

                if (tAttrs.disableHeaderButtons === undefined) {
                    template += '<div class="header-buttons">';
                        template += '<button class="btn btn-info" ng-click="update()"><i class="icon-refresh icon-white"></i> Обновить список</button>';

                        template += '<button class="btn btn-warning" ng-show="itemList.length > 0 && !allSelected()" ng-click="selectAll()"><i class="icon-white icon-ok-circle"></i> Выделить все</button>';

                        template += '<button class="btn btn-warning active" ng-show="itemList.length > 0 && allSelected()" ng-click="resetSelected()"><i class="icon-white icon-remove-circle"></i> Снять выделение</button>';

                        template += '<a class="btn btn-{{ i.type }}" href="#!{{ i.url }}" ng-repeat="i in headerButtons">';
                        template += '<i class="icon-{{ i.icon }} icon-white"></i>&nbsp;';
                        template += '{{ i.title }}</a>';

                        template += '<button class="btn btn-danger" ng-show="hasSelectedItems()" ng-click="removeSelectedEntities()"><i class="icon-trash icon-white"></i>&nbsp;Удалить выбранные ({{ selectedCount() }} шт.)</button>'
                    template += '</div>';
                }

                var tableClass = tAttrs.tableClass || '';

                template += '<div ng-show="itemList.length > 0">';
                template += '<table ng-table="tableParams" class="table table-hover table-striped '+tableClass+'">';
                template += '<tr data-id="{{ item.id }}" ng-repeat="item in itemList" ng-class="{selected:selectedItems[item.id] === true}">';

                if (tAttrs.disableSelection === undefined) {
                    // Выделение нескольких элементов
                    template += '<td data-title=" " width="20">';
                    template += '<label class="checkbox"><input type="checkbox" ng-model="selectedItems[item.id]" /></label>';
                    template += '</td>';
                }

                // Сортировка
                if (tAttrs.sortable !== undefined) {
                    template += '<td data-title=" " width="40" class="sortable-td">';
                    template += '<i class="icon-chevron-up" tooltip="Поднять" ng-click="sortUp(item)" ng-class="{hidden:isFirstItem(item)}"></i>';
                    template += '&nbsp;';
                    template += '<i class="icon-chevron-down" tooltip="Опустить" ng-click="sortDown(item)" ng-class="{hidden:isLastItem(item)}"></i>';
                    template += '</td>';
                }

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        if (columns[i].by instanceof Object) {
                            modifier = '|'+columns[i].modifier+':'+JSON.stringify(columns[i].by)+':item';
                            modifier = modifier.replace(/"/g, '&quot;');
                        } else {
                            modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\':item';
                        }
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html-unsafe="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label>';
                    }

                    template += '</td>';
                }

                if (tAttrs.disableActions === undefined) {
                    // Кнопки действий
                    template += '<td data-title="Действия" width="200" class="actions-td">';
                        if (tAttrs.disableMove === undefined) {
                            template += '<div class="btn btn-info btn-small" tooltip="Перенести элемент" ng-click="moveEntity(item)"><i class="icon-white icon-share-alt"></i></div>';
                        }
                        template += '<a class="btn btn-{{ i.type }} btn-small" tooltip="{{ i.tooltip }}" href="#!{{ i.url|replace:\'{id}\':$parent.item.id }}" ng-repeat="i in actionButtons">';
                            template += '<i class="icon-white icon-{{ i.icon }}"></i>';
                        template += '</a>';
                        template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                    template += '</td>';
                }


                template += '</tr></table></div>';

                template += '<div class="well well-small" ng-show="itemList.length == 0 && !loading">Список пуст</div>';

                template = '<div loading-container="loading">' + template + '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                /**
                 * Поиск элемента в текущем списке по ID
                 */
                $scope.findItemById = function(id) {
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        if ($scope.itemList[i].id == id) {
                            return $scope.itemList[i];
                        }
                    }

                    return false;
                };

                /**
                 * Сортировка элемента вверх
                 */
                $scope.sortUp = function(item) {
                    $scope.sortItem(item, 'up');
                };

                /**
                 * Сортировка элемента вниз
                 */
                $scope.sortDown = function(item) {
                    $scope.sortItem(item, 'down');
                };

                /**
                 * Сортировка
                 */
                $scope.sortItem = function(item, order) {
                    var url = $attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    url = url.replace(':id', 'sort/' + order);

                    $scope.loading = true;
                    $http({
                        method: 'POST',
                        url: url,
                        data: {id: item.id}
                    }).success(function(response) {
                        if (response.success) {
                            if (!response.moved) {
                                $rootScope.$broadcast('ntf-warning', 'При сортировке ничего не изменилось. ', 5000);
                            }
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке.');
                        }

                        $scope.loading = false;
                        $scope.update();
                    }).error(function(response, code) {
                        if (code != 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке: ' + JSON.stringify(response));

                            $scope.loading = false;
                            $scope.update();
                        }
                    });
                };

                /**
                 * Является ли элемент первым в списке
                 * @return {Boolean}
                 */
                $scope.isFirstItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == 1 && angular.equals(item, $scope.itemList[0]);
                };

                /**
                 * Является ли элемент последним в списке
                 * @return {Boolean}
                 */
                $scope.isLastItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == $scope.pagesCount && angular.equals(item, $scope.itemList[$scope.itemList.length - 1]);
                };

                /**
                 * Выполнить каллбек дял каждого выбранного элемента
                 * @param  {Function} callback Каллбек. Первый параметр ID элемента
                 */
                $scope.onEachSelected = function(callback) {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            callback(i);
                        }
                    }
                };

                // Проверяем, если ли выделенные элементы
                $scope.hasSelectedItems = function() {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            return true;
                        }
                    }

                    return false;
                };

                /**
                 * Количество выбранных элементов
                 */
                $scope.selectedCount = function() {
                    var count = 0;

                    $scope.onEachSelected(function() {
                        count++;
                    });

                    return count;
                };

                /**
                 * Сброс выбранных элементов
                 */
                $scope.resetSelected = function() {
                    $scope.selectedItems = {};
                };

                /**
                 * Выделение всех видимых элементов
                 */
                $scope.selectAll = function() {
                    $scope.selectedItems = {};

                    for (var i = 0; i < $scope.itemList.length; i++) {
                        $scope.selectedItems[$scope.itemList[i].id] = true;
                    }
                };

                /**
                 * Выделены все элементы?
                 * @return {boolean}
                 */
                $scope.allSelected = function() {
                    return $scope.selectedCount() == $scope.itemList.length && $scope.itemList.length > 0;
                };

                /**
                 * Обновление списка
                 */
                $scope.update = function() {
                    $scope.loading = true;
                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        if (data.items && data.items.length == 0 && data.items_count > 0 && $scope.tableParams.page > 1) {

                            $scope.tableParams.page--;
                            return;
                        }

                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                        $scope.pagesCount = data.page_count;
                        $scope.totalItems = data.items_count;
                        $scope.resetSelected();

                        if (!!$attrs.onUpdate) {
                            $rootScope.$broadcast($attrs.onUpdate);
                        }

                        $timeout(function() {
                            $scope.loading = false;
                        });
                    }, function(response, errorData){
                        if (response.status == 0) {
                            // Загрузка была отменена
                            return;
                        }

                        $scope.itemList = [];
                        $scope.tableParams.total = 0;

                        var msg = '';

                        try {
                            msg = response.data[0].message;
                        } catch(e) {
                            msg = JSON.stringify(errorData);
                        };

                        $timeout(function() {
                            $scope.loading = false;
                        });

                        $rootScope.$broadcast('ntf-error', response.status + ' - Произошла ошибка при загрузке списка. ' + msg);
                    });
                };

                /**
                 * Есть ли в списке столбцы с логическими значениями?
                 * @return {Boolean}
                 */
                $scope.hasBooleanColumns = function() {
                    for(var i in $scope.columns) {
                        if (!$scope.columns.hasOwnProperty(i)) {
                            continue;
                        }

                        if ($scope.columns[i].isBoolean === true) {
                            return true;
                            break;
                        }
                    }

                    return false;
                };

                /**
                 * Перенос сущности
                 */
                $scope.moveEntity = function(item) {
                    var d = $dialog.dialog({
                        dialogFade: true,
                        resolve: {
                            item: function (){return angular.copy(item);},
                            entityResource: function(){return $scope.entityResource;},
                            closeDialog: function(){return function() {d.close();}}
                        },
                        dialogClass: 'modal w800'
                    });

                    d.open('dialog-entity-move.html', 'entityMoveBlockCtrl');
                };

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.loading = true;

                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function(response) {
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка при удалении: ' + JSON.stringify(response));
                                $scope.$broadcast('needUpdate');
                            });
                        }
                    });
                };

                /**
                 * Удаление выбранных сущностей
                 */
                $scope.removeSelectedEntities = function() {
                    if ($scope.selectedCount() == 0) {
                        return;
                    }

                    var title = 'Удаление элементов';
                    var msg = 'Вы уверены, что хотите удалить выбранные элементы ('+$scope.selectedCount()+' шт.)?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    var removed = 0;

                    var afterDelete = function() {
                        if (++removed == $scope.selectedCount()) {
                            $scope.$broadcast('needUpdate');
                            $scope.resetSelected();
                        }
                    };

                    $dialog.messageBox(title, msg, btns)
                        .open()
                        .then(function(result) {
                            if (result == 'ok') {

                                $scope.loading = true;
                                $scope.onEachSelected(function(id) {
                                    $scope.entityResource.remove({id:id}, afterDelete, function(response) {
                                        $rootScope.$broadcast('ntf-error', 'Произошла ошибка при удалении: ' + JSON.stringify(response));
                                        afterDelete();
                                    });
                                });
                            }
                    });
                };
            },

            compile: function($element, $attrs) {
                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    $scope.pagesCount = 0;
                    $scope.totalItems = 0;

                    // Кнопки в заголовке таблицы
                    $scope.headerButtons = [];

                    if (!!attrs.btnAdd) {
                        $scope.headerButtons.push({
                            type: 'success',
                            url: (attrs.url + '/').replace('//', '/') + 'create/' + (!!attrs.addId ? attrs.addId : ''),
                            icon: 'plus',
                            title: attrs.btnAdd
                        });
                    }

                    // Кнопки действий в конце строки каждой таблицы
                    $scope.actionButtons = [];

                    if (!!attrs.actions) {
                        var actions = $parse(attrs.actions)($scope);
                        for(var i = 0; i < actions.length; i++) {
                            $scope.actionButtons.push(actions[i]);
                        }
                    }

                    $scope.actionButtons.push({
                        type: 'warning',
                        url: (attrs.url + '/').replace('//', '/') + 'edit/{id}',
                        icon: 'edit',
                        tooltip: 'Редактировать'
                    });

                    // Список колонок
                    $scope.columns = $parse(attrs.columns)($scope);

                    // параметры аякс-таблицы
                    $scope.tableParams = new ngTableParams({
                        page: 1,
                        total: 0,
                        count: parseInt(attrs.count, 10)
                    });

                    // Список элементов
                    $scope.itemList = [];

                    // Список выделенных элементов
                    $scope.selectedItems = {};

                    // Создаем ресурс
                    var url = attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    $scope.entityResource = $resource(url, {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'},
                            move: {method:'POST', url: url.replace('/:id', '/move/:id').split('?', 2)[0]}
                        });

                    // Наблюдаем за параметрами таблицы
                    $scope.$watch('tableParams', $scope.update, true);
                    $scope.$on('needUpdate', $scope.update, true);

                    // Событие успешного добавления/редактировния элемента
                    $scope.$on('entityFormClosed', function() {
                        $scope.$broadcast('needUpdate');
                    });

                    // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                    if ($scope.hasBooleanColumns()) {
                        $scope.$watch('itemList', function(itemList, oldList) {
                            if ($scope.loading) {
                                return;
                            }

                            for(var i = 0; i < oldList.length; i++) {
                                if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                                    continue;
                                }

                                if (!angular.equals(itemList[i], oldList[i])) {
                                    var url = attrs.entityList.replace(/:id/, 'set/' + itemList[i].id);

                                    $http.post(url, itemList[i])
                                        .success(function(response) {
                                            $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                        })
                                        .error(function(response, code) {
                                            if (code !== 0) {
                                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка: ' + JSON.stringify(response));
                                            console.error(response);
                                            }
                                        });
                                }
                            }
                        }, true);
                    }

                    // Сортировка перетаскиванием
                    if (attrs.sortable !== undefined) {
                        $element.find('table[ng-table] tbody').sortable({
                            helper: function(e, tr) {
                                var $originals = tr.children();
                                var $helper = tr.clone();

                                $helper.children().each(function(index) {
                                    $(this).width($originals.eq(index).width());
                                });

                                return $helper;
                            },

                            update: function(e, ui) {
                                var $tr = $(ui.item);
                                var item = $scope.findItemById($tr.data('id'));

                                if ($tr.is(':first-child')) {
                                    var anotherItem = $scope.findItemById($tr.next('tr').data('id'));
                                    var direction = 'before';
                                } else {
                                    var anotherItem = $scope.findItemById($tr.prev('tr').data('id'));
                                    var direction = 'after';
                                }

                                if (item && anotherItem) {
                                    $scope.sortItem(item, direction+'-'+anotherItem.id);
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                $scope.$emit('entityFormClosed');
                            } else {
                                var msg = 'Форма содержит ошибки заполнения';

                                if (!!response.errors) {
                                    $scope.errors = response.errors;

                                    var eAr = [];

                                    for (var i in $scope.errors) {
                                        if ($scope.errors.hasOwnProperty(i) && $.isArray($scope.errors[i])) {
                                            eAr.push($scope.errors[i].join(', '));
                                        }
                                    }

                                    if (eAr.length > 0) {
                                        msg += ': ' + eAr.join('; ');
                                    }
                                } else {
                                    msg += ': ' + JSON.stringify(response);
                                }

                                $rootScope.$broadcast('ntf-error', msg);
                            }
                        })
                        .error(function(response, code) {
                            $scope.loading = false;

                            if (code != 0) {
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка: ' + JSON.stringify(response));
                                console.error(response);
                            }
                        });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"], input[type="email"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="range"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"], input[type="email"], input[type="password"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - коллекция объектов
     */
    .directive('formCollection', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)" title="Удалить элемент"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            compile: function($element, attrs) {
                var $body = $element.children('.controls');
                var $itemsList = $body.children('.collection-list');

                if (!attrs.prototype) {
                    $body.html('<span class="label label-important">Empty prototype</span>');
                    return;
                }

                $body.children('.btn-add').attr('ng-click', 'addElement()');

                $itemsList.children('.item').each(function() {
                    $(this).append(removeButtonTpl);
                });

                return function($scope, $element, attrs) {
                    $scope.$body = $body;
                    $scope.$itemsList = $itemsList;
                    $scope.index = $itemsList.children('.item').length;
                    $scope.proto = '<div class="item clearfix">' + attrs.prototype + '</div>';
                }
            },

            controller: function($scope, $element) {
                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var $item = $($scope.proto.replace(/__name__/g, $scope.index));

                    $scope.index++;
                    $item.append(removeButtonTpl);

                    $scope.$itemsList.append($item);
                    $compile($item)($scope);
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.target);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.parent('.item').remove();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var $fInput = $controls.children('input').first();
                    if ($fInput.length == 0) {
                        return;
                    }

                    var name = $fInput.attr('name').replace(/\[\d+\]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', (name+'_'+i).replace(/[\[\]]/g, '_'));

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });

                        $compile($this)($scope);

                        i++;
                    });
                };
            }
        }
    }])

    /**
     * Тип поля - точка на карте
     */
    .directive('yandexMapPoint', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                name: '@yandexMapPoint',
                loadedData: '&data'
            },

            link: function($scope, $element, attrs)  {
                var map;
                var placemark;
                var polygon;

                var allowAddPolygon = (attrs.allowAddPolygon !== undefined);

                $scope.data = {
                    center: [45.034942, 38.976032],
                    point: [45.034942, 38.976032],
                    zoom: 11,
                    polygon: [],
                };

                var loadedData = $scope.loadedData();

                if (!!loadedData && loadedData.center) {
                    $scope.data.center = loadedData.center;
                }

                if (!!loadedData && loadedData.point) {
                    $scope.data.point = loadedData.point;
                }

                if (!!loadedData && loadedData.zoom) {
                    $scope.data.zoom = loadedData.zoom;
                }

                if (!!loadedData && loadedData.polygon) {
                    $.each(loadedData.polygon, function(i, wrongContour) {
                        var contour = [];

                        $.each(wrongContour, function(i, wrongPoint) {
                            var point = [];
                            point.push(parseFloat(wrongPoint[0]));
                            point.push(parseFloat(wrongPoint[1]));

                            contour.push(point);
                        });

                        $scope.data.polygon.push(contour);
                    });
                }


                var $iCenter0 = $('<input type="text" style="display:none;" name="'+$scope.name+'[center][0]" ng-model="data.center.0" />');
                var $iCenter1 = $('<input type="text" style="display:none;" name="'+$scope.name+'[center][1]" ng-model="data.center.1" />');
                var $iPoint0 = $('<input type="text" style="display:none;" name="'+$scope.name+'[point][0]" ng-model="data.point.0" />');
                var $iPoint1 = $('<input type="text" style="display:none;" name="'+$scope.name+'[point][1]" ng-model="data.point.1" />');
                var $iZoom = $('<input type="text" style="display:none;" name="'+$scope.name+'[zoom]" ng-model="data.zoom" />');
                var $iPolygon = $('<input type="text" style="display:none;" ng-repeat="(n, v) in _polygon track by $index" name="{{ n }}" value="{{ v }}" />');

                $element
                    .append($iCenter0)
                    .append($iCenter1)
                    .append($iPoint0)
                    .append($iPoint1)
                    .append($iZoom)
                    .append($iPolygon)
                ;

                $compile($iCenter0)($scope);
                $compile($iCenter1)($scope);
                $compile($iPoint0)($scope);
                $compile($iPoint1)($scope);
                $compile($iZoom)($scope);
                $compile($iPolygon)($scope);

                if (allowAddPolygon) {
                    $scope.$watch('data.polygon', function() {
                        $scope._polygon = {};

                        for (var i = 0; i < $scope.data.polygon.length; i++) {
                            var contour = $scope.data.polygon[i];

                            for (var ii = 0; ii < contour.length; ii++) {
                                var point = contour[ii];

                                $scope._polygon[$scope.name + '[polygon]['+i+']['+ii+'][0]'] = point[0];
                                $scope._polygon[$scope.name + '[polygon]['+i+']['+ii+'][1]'] = point[1];
                            }
                        }
                    }, true);
                }


                // Сохранение данных в инпуты
                var save = function() {
                    $scope.data.center = map.getCenter();
                    $scope.data.point = placemark.geometry.getCoordinates();
                    $scope.data.zoom = map.getZoom();

                    if (allowAddPolygon) {
                        if (polygon) {
                            $scope.data.polygon = polygon.geometry.getCoordinates();
                        } else {
                            $scope.data.polygon = [];
                        }
                    }

                    $scope.$apply();
                };

                // Начало работы виджета
                var start = function() {
                    $scope.id = 'map-' + (Math.random()*1000).toString().replace(/[^0-9]/, '');
                    $element.attr('id', $scope.id);
                    $element.css({width:'100%', height:600});

                    ymaps.ready(function() {
                        map = new ymaps.Map($scope.id, {
                            zoom: $scope.data.zoom,
                            center: $scope.data.center
                        });

                        var searchControl = new ymaps.control.SearchControl({
                            provider: 'yandex#map',
                            noPlacemark:true,
                            resultsPerPage:5,
                            useMapBounds:true,
                            width:350
                        });

                        map.controls
                            .add('zoomControl')
                            .add('mapTools')
                            .add(searchControl)
                        ;

                        placemark = new ymaps.Placemark(
                            $scope.data.point,
                            {
                                hintContent: 'Метку можно перетаскивать',
                            },
                            {
                                draggable: true,
                                preset: 'twirl#redIcon',
                            }
                        );


                        map.geoObjects.add(placemark);

                        map.events.add('click', function(e){
                            var coords = e.get('coordPosition');
                            placemark.geometry.setCoordinates(coords);
                        });

                        placemark.events.add('dragend', save);

                        map.events.add(['actionend', 'actiontickcomplete', 'boundschange', 'click', 'dblclick', 'optionschange', 'sizechange', 'typechange', 'wheel'], save);

                        searchControl.events.add('resultselect', function(e) {
                            var coords = searchControl.getResultsArray()[e.get('resultIndex')].geometry.getCoordinates();

                            placemark.geometry.setCoordinates(coords);
                        });

                        if (allowAddPolygon) {
                            var recreatePolygon = function(points) {
                                if (!!polygon) {
                                    map.geoObjects.remove(polygon);
                                }

                                points = points || [];

                                polygon = new ymaps.Polygon(points, {}, {
                                    editorDrawingCursor: 'crosshair',
                                    fillColor: '#09689f',
                                    fillOpacity: 0.6,
                                    strokeColor: '#09689f',
                                    strokeWidth: 3
                                });

                                polygon.events.add('geometrychange', function() {
                                    save();
                                });

                                map.geoObjects.add(polygon);

                                var stateMonitor = new ymaps.Monitor(polygon.editor.state);
                                stateMonitor.add('drawing', function (state) {
                                    polygon.options.set('strokeColor', state ? '#ff0000' : '#09689f');
                                    if (state) {
                                        btnStartDrawing.select();
                                    } else {
                                        btnStartDrawing.deselect();
                                    }
                                });
                            };
                            recreatePolygon($scope.data.polygon);

                            var btnStartDrawing = new ymaps.control.Button('Редактировать полигон');
                            var btnClearDrawing = new ymaps.control.Button('Удалить полигон', {selectOnClick:false});

                            var btnToolBar = new ymaps.control.ToolBar();
                            btnToolBar.add(btnStartDrawing);
                            btnToolBar.add(btnClearDrawing);

                            btnStartDrawing.events.add('select', function() {
                                polygon.editor.startDrawing();
                            });

                            btnStartDrawing.events.add('deselect', function() {
                                polygon.editor.stopDrawing();
                            });

                            btnClearDrawing.events.add('click', function() {
                                btnStartDrawing.deselect();
                                polygon.editor.stopDrawing();
                                recreatePolygon();
                            });

                            map.controls.add(btnToolBar, {top:5, right:5});
                        }
                    });
                };

                if (!window.yandexMapConnected) {
                    window.yandexMapConnected = true;
                    var fileref=document.createElement('script')
                    fileref.setAttribute("type","text/javascript")
                    fileref.setAttribute("src", 'http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU');
                    fileref.onload = start;
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                } else {
                    start();
                }
            }
        }
    }])

    /**
     * Маска ввода
     */
    .directive('uiMask', [function() {
        return {
            link:function ($scope, $element, attrs) {
                setTimeout(function() {
                    $($element).mask(attrs.uiMask, {placeholder:'#'});
                });
            }
        };
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })
;

/**
 * Перенос разделов
 */
angular.module('cms.nodemove', ['cms.entity'])
    .directive('nodeMoveBlock', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.url = '';
                $scope.loading = false;
                $scope.httpUrl = attrs.nodeMoveBlock;
                $scope.moveUrl = attrs.moveUrl;
                $scope.node = false;

                $scope.$watch('url', $scope.updateData);
            },

            controller: function($scope) {
                $scope.updateData = function() {
                    $scope.node = false;

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    if (!$scope.url) {
                        return;
                    }

                    this.canceler = $q.defer();

                    this.canceler = $q.defer();

                    $http({
                        method: 'GET',
                        url: $scope.httpUrl,
                        params: {url: $scope.url},
                        timeout: this.canceler.promise
                    }).success(function(response) {
                        if (response.success && !!response.node) {
                            $scope.node = response.node;
                        }
                    });
                };

                $scope.submit = function(movetype) {
                    $scope.loading = true;

                    $http({
                        method: 'POST',
                        url: $scope.moveUrl,
                        data: {id2: $scope.node.id, movetype:movetype}
                    }).success(function(response) {
                        $scope.loading = false;

                        if (response.success && !!response.redirect) {
                            $rootScope.$broadcast('ntf-success', 'Раздел успешно перенесен, сейчас вы будете перенаправлены', 5000);
                            window.location.href = response.redirect;
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела', 5000);
                        }
                    }).error(function(response, code) {
                        $scope.loading = false;

                        if (code > 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела', 5000);
                            console.error(response);
                        }
                    });
                };

                $scope.submitFirst = function() {
                    $scope.submit('innerfirst')
                };

                $scope.submitLast = function() {
                    $scope.submit('innerlast')
                };
            }
        }
    }])
;

/**
 * Модуль уведомлений работает через $rootScope.$broadcast
 */
angular.module('cms.notification', [])
    .controller('NotificationCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
        /**
         * Список сообщений
         */
        $scope.alerts = [];

        var counter = 0;

        /**
         * Добавление алерта
         */
        $scope.addAlert = function(msg, type, delay) {
            $scope.$apply(function() {
                $scope.alerts.push({id:counter, msg:msg, type:type});
                counter++;
            });

            if (delay !== undefined && delay > 1000) {
                var toClose = counter - 1;

                setTimeout(function() {
                    for(var i = 0; i < $scope.alerts.length; i++) {
                        if ($scope.alerts[i].id == toClose) {
                            $scope.$apply(function() {
                                $scope.closeAlert(i);
                            });
                            break;
                        }
                    }
                }, delay);
            }
        };

        /**
         * Закрытие алерта
         */
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        /**
         * Событие добавления уведомления
         */
        $rootScope.$on('notification', function(event, notification) {
            setTimeout(function() {
                $scope.addAlert(notification.msg, notification.type, notification.delay);
            }, 13);
        });

        /**
         * Краткие способы доабвления уведомлений
         */
        $rootScope.$on('ntf-success', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'success', delay);
            }, 13);
        });

        $rootScope.$on('ntf-info', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'info', delay);
            }, 13);
        });

        $rootScope.$on('ntf-warning', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'warning', delay);
            }, 13);
        });

        $rootScope.$on('ntf-error', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'error', delay);
            }, 13);
        });
    }])
;

angular.module('cms.selectpicker', []).directive('selectpicker', ['$timeout', function ($timeout) {
    var NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w\d]*)|(?:\(\s*([\$\w][\$\w\d]*)\s*,\s*([\$\w][\$\w\d]*)\s*\)))\s+in\s+(.*)$/;
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function postLink(scope, element, attrs, controller) {
            var options = scope.$eval(attrs.selectpicker) || {};

            $timeout(function () {
                element.selectpicker(options);
                element.next().removeClass('ng-scope');
            });

            if (controller) {
                scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        element.selectpicker('refresh');
                    }
                });
            }
        }
    };
}]);

/**
 * Фильтры для удобного отображения данных в строках таблицы
 */
angular.module('cms.table.filters', [])
    /**
     * Фильтр замены строки
     */
    .filter('replace', [function() {
        return function(value, source, dest) {
            return value.replace(source, dest).replace(source, dest).replace(source, dest);
        }
    }])

    /**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Фильтр отображения объекта
     */
    .filter('el_object', [function() {
        return function(value, name) {
            return !!value && value.hasOwnProperty(name) ? value[name] : '';
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'" target="_self">'+value+'</a>';
        }
    }])

    /**
     * Ссылка на страницу CMS
     */
    .filter('el_cms_link', ['$filter', function($filter) {
        return function(value, prefix, item) {
            if (prefix instanceof Object) {
                var url = ((prefix.prefix || '') + value) || '';
                var title = item[prefix.title] || '';

                if (!value) {
                    return title;
                }

                if (!title) {
                    return '<a href="'+url+'" title="'+url.replace(/"/g, '&quot;')+'" target="_self">' + $filter('characters')(url, 50) + '</a>';
                }

                if (!!title && !!value) {
                    return '<a href="'+url+'" title="'+title.replace(/"/g, '&quot;')+'" target="_self">' + $filter('characters')(title, 50) + '</a>';
                }

                return '---';
            } else {
                var url = prefix + value;

                return '<a href="'+url+'" title="'+url+'" target="_self">' + $filter('characters')(value, 50) + '</a>';
            }
        }
    }])

    /**
     * Одиночное изображение
     */
    .filter('el_image', [function() {
        return function(value) {
            if (!!value && !!value.size && !!value.size.cms) {
                return '<img src="'+value.size.cms+'" title="'+value.title+'" />';
            } else {
                return '---';
            }
        }
    }])
;

/**
 * TinyMCE для админки
 */
angular.module('cms.tinymce', [])
    /**
     * Инициализация tinyMCE
     */
    .directive('tinymce', ['$timeout', function($timeout) {
        var generatedIds = 0;

        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                attrs.$set('id', 'tiny-mce-' + generatedIds++);

                $timeout(function() {
                    var options = angular.extend(window.tinyMCEConfig, {
                    });

                    initTinyMCE(options, $element);
                });
            }
        };
    }])
;

angular.module('cms.tree', ['cms.utils'])
    /**
     * Дерево структуры
     */
    .directive('nodeTree', ['$http', '$parse', '$timeout', '$rootScope', function($http, $parse, $timeout, $rootScope) {
        // Предзагруженные ноды
        var __preload = {};

        return {
            restrict: 'A',
            scope: {
                url: '@',
                root: '@nodeTree',
                prefix: '@'
            },

            template: '<li ng-repeat="item in items" node-tree-item="item" prefix="prefix" url="url" class="node-tree-item" ng-class="{\'current\':item.current, \'disabled\':!item.active, \'denied\':!item.isGranted}"></li>',

            controller: function($scope) {
                /**
                 * Обновление списка пунктов
                 */
                $scope.updateItems = function(items) {
                    $scope.items = items;

                    $timeout(function() {
                        if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                            $scope.$broadcast('autoExpand', $scope.autoExpand);
                        }

                        $scope.$emit('loaded');
                    });
                };

                /**
                 * Загрузка списко элементов
                 */
                $scope.loadItems = function(callback) {
                    $http({
                            method: 'GET',
                            url: $scope.url,
                            params: {parent:$scope.root, count:1000, page:1}
                        })
                        .success(function(response) {
                            $scope.updateItems(response.items);
                            if ($.isFunction(callback)) {
                                callback(response.items);
                            }
                        })
                        .error(function(response) {
                            console.log(response);
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                        });
                };
            },

            compile: function($element) {
                var $link = $element.children('a.link');

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.autoExpand = $parse(attrs.autoExpand)($scope);
                    $scope.loadItems();
                };
            }
        }
    }])

    /**
     * Элемент дерева
     */
    .directive('nodeTreeItem', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                item: '=nodeTreeItem',
                prefix: '=',
                url: '='
            },
            template: '<i class="expand-btn" ng-click="toggle()" ng-class="icon()"></i>'
                    + '<i class="icon-share-alt" title="Редирект на {{ item.redirect }}" ng-show="!!item.redirect"></i>'
                    + '<a href="{{ prefix }}{{ item.url }}" class="link" target="_self" title="{{ title() }}">{{ item.title|characters:gettl(item) }}</a>',
            link: function($scope, $element, attrs) {
                $scope.expanded = false;
                $scope.loading = false;

                if (!$scope.item.title) {
                    $scope.item.title = '<Без названия>';
                }

                $scope.$on('autoExpand', function(event, autoExpand) {
                    if (event.targetScope === $scope) {
                        return;
                    }

                    $scope.autoExpand = autoExpand;
                    var itemPosition = $.inArray($scope.item.id, $scope.autoExpand);

                    if (itemPosition !== -1) {
                        $scope.toggle();
                        $scope.item.current = true;
                        $scope.autoExpand.splice(itemPosition, 1);

                        if ($scope.autoExpand.length == 0) {
                            $scope.item.currentLast = true;
                        }
                    }
                });

                $scope.$on('updateTree', function() {
                    if (!$scope.item.currentLast || $scope.autoExpand.length > 0) {
                        return;
                    }

                    $scope.$tree.scope().loadItems(function(items) {
                        if (!items || items.length == 0) {
                            $scope.item.isParent = false;
                        } else {
                            $scope.item.isParent = true;
                        }
                    });
                });
            },

            controller: function($scope, $element) {
                /**
                 * Получение иконки папки
                 */
                $scope.icon = function() {
                    if ($scope.loading) {
                        return 'icon-refresh infiinite-rotate';
                    }

                    if ($scope.item.isRoot) {
                        return 'icon-home';
                    }

                    if (!$scope.item.isParent) {
                        return 'icon-file';
                    }

                    if ($scope.expanded) {
                        return 'icon-folder-open';
                    } else {
                        return 'icon-folder-close';
                    }
                };

                /**
                 * Генерация дерева
                 */
                $scope.createTree = function() {
                    $scope.expanded = true;
                    $scope.loading = true;
                    $scope.$tree = angular.element('<ul class="node-tree unstyled" node-tree="{{ item.id }}" prefix="{{ prefix }}" url="{{ url }}" ng-show="items.length > 0"></ul>');

                    $element.append($scope.$tree);
                    $compile($scope.$tree)($scope);

                    $scope.$on('loaded', function(event) {
                        event.stopPropagation();
                        $scope.loading = false;
                        if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                            $scope.$broadcast('autoExpand', $scope.autoExpand);
                        }
                    });
                };

                /**
                 * Переключение состояния дерева
                 */
                $scope.toggle = function() {
                    if (!$scope.$tree) {
                        $scope.createTree();
                        return;
                    }

                    if ($scope.expanded) {
                        $scope.$tree.slideUp(300);
                    } else {
                        $scope.$tree.slideDown(300);
                    }

                    $scope.expanded = !$scope.expanded;
                };

                /**
                 * Возвращает длину строки для обрезки названия раздела
                 */
                $scope.gettl = function(item) {
                    var len = 165;

                    len = len - item.level * 15;

                    if (len < 50) {
                        len = 50;
                    }

                    return len;
                };

                /**
                 * Title-подсказка на категориях
                 */
                $scope.title = function() {
                    if (!$scope.item.isGranted) {
                        return 'Доступ запрещен';
                    } else {
                        return '[' + $scope.item.id + '] [' + $scope.item.controller.name + '] ' + $scope.item.title;
                    }
                }
            }
        }
    }])
;

/**
 * Полезные инструменты
 */
angular.module('cms.utils', [])
    /**
     * Фильтр транслитерации
     */
    .filter('translit', [function(){
        var List = {
                'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
                'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
                'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
                'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
                'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
                'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
                'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
                'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
                'Я':'Ya','я':'ya'
            },
            regular = '',
            k;

        for (k in List){
            regular += k;
        }

        regular = new RegExp('[' + regular + ']', 'g');

        k = function(a){
            return a in List ? List[a] : '';
        };

        return function(input){
            if (!!input){
                return input.replace(regular, k);
            }else{
                return '';
            }
        };
    }])

    /**
     * Удобочитаемый размер файла
     */
    .filter('filesize', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '-';
            }

            if (typeof precision === 'undefined') {
                precision = 1;
            }

            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
            var number = Math.floor(Math.log(bytes) / Math.log(1024));

            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })

    /**
     * Обрезание строки по кол-ву символов
     */
    .filter('characters', function () {
        return function (input, chars, breakOnWord) {
            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length >= chars) {
                input = input.substring(0, chars);

                if (!breakOnWord) {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1) {
                        input = input.substr(0, lastspace);
                    }
                }else{
                    while(input.charAt(input.length-1) == ' '){
                        input = input.substr(0, input.length -1);
                    }
                }
                return input + '...';
            }
            return input;
        };
    })

    /**
     * Обрезание строки по кол-ву слов
     */
    .filter('words', function () {
        return function (input, words) {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input) {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words) {
                    input = inputWords.slice(0, words).join(' ') + '...';
                }
            }
            return input;
        };
    })

    /**
     * Директива преобразования в системное имя
     */
    .directive('nodename', ['$filter', function($filter) {
        var getValue = function(value) {
            value = value.toString().toLowerCase();
            value = $filter('translit')(value);
            value = value.replace(/( |\/)/g, '-');
            value = value.replace(/[^a-zA-Z_\-0-9]/g, '');

            return value;
        };

        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                if (!$element.is('input')) {
                    return function() {};
                }

                return function($scope, $element, attrs) {
                    var $source = $element.closest('form').find('*[name="'+attrs.nodename+'"]');

                    if ($source.length == 0) {
                        return;
                    }

                    $source.on('keyup click', function() {
                        var value = $(this).val();
                        $element.val(getValue(value));
                    });

                    $element.on('keyup', function() {
                        $source.off('keyup click');
                    });
                }
            }
        }
    }])
;

/**
 * Переопределение табов для запоминания в адресной строке
 * Так же взаимодействует с директивой entityList инициируя обновление списка
 */
angular.module('ui.bootstrap.tabs', [])
    /**
     * Контроллер переключения табов
     */
    .controller('TabsController', ['$scope', '$element', '$location', '$timeout', function($scope, $element, $location, $timeout) {
        var panes = this.panes = $scope.panes = [];

        /**
         * Делает вкладку активной
         * pane - scope вкладки
         * changeLocation - изменять ли хеш текущей страницы
         * update - инициировать ли обновление entityList
         */
        this.select = $scope.select = function selectPane(pane, changeLocation, update) {
            angular.forEach(panes, function(pane) {
                pane.selected = false;
            });

            pane.selected = true;

            if (changeLocation !== false) {
                $location.path(pane.url);
            }

            if (update === true && $.isFunction(pane.updateEntityList)) {
                pane.updateEntityList();
            }
        };

        this.addPane = function addPane(pane, index) {
            if ($.inArray(pane, panes) === -1) {
                panes.splice(index, 0, pane);
            }

            if (panes.length == 0 || $location.path() == pane.url) {
                $scope.select(pane, $location.path() == pane.url);
            }
        };

        this.hidePane = $scope.hidePane = function hidePane(pane) {
            var index = panes.indexOf(pane);
            pane.hidden = true;
            if (panes.length > 0) {
                var newPane = pane.getParent();
                $scope.select(newPane ? newPane : panes[index < panes.length ? index : index-1], true, true);
            }
        }

        this.removePane = function removePane(pane) {
            var index = panes.indexOf(pane);
            panes.splice(index, 1);
            if (pane.selected && panes.length > 0) {
                $scope.select(panes[index < panes.length ? index : index-1]);
            }
        };
    }])

    /**
     * Контейнер с табами
     */
    .directive('tabs', function() {
        return {
            restrict: 'EA',
            transclude: true,
            scope: {
            },
            controller: 'TabsController',
            template:  "<div class=\"tabbable\">\n" +
                "  <ul class=\"nav nav-tabs\">\n" +
                "    <li ng-repeat=\"pane in panes\" ng-class=\"{active:pane.selected}\" ng-show=\"!pane.hidden\">\n" +
                "      <a ng-click=\"select(pane)\">{{pane.heading}}</a>" +
                "    </li>\n" +
                "  </ul>\n" +
                "  <div class=\"tab-content\" ng-transclude></div>\n" +
                "</div>",
            replace: true
        };
    })

    /**
     * Обычная вкладка.
     * Может содержать entityList
     */
    .directive('pane', ['$parse', '$location', function($parse, $location) {
        return {
            require: '^tabs',
            restrict: 'EA',
            transclude: true,
            scope:{
                heading:'@',
            },
            link: function(scope, element, attrs, tabsCtrl) {
                var getSelected, setSelected;
                scope.selected = false;

                if (!!attrs.url) {
                    scope.url = attrs.url;

                    if (scope.url[scope.url.length - 1] != '/') {
                        scope.url = scope.url + '/';
                    }

                    if (scope.url[0] != '/') {
                        scope.url = '/' + scope.url;
                    }
                }

                if (attrs.active) {
                    getSelected = $parse(attrs.active);
                    setSelected = getSelected.assign;
                    scope.$watch(
                        function watchSelected() {return getSelected(scope.$parent);},
                        function updateSelected(value) {scope.selected = value;}
                    );
                    scope.selected = getSelected ? getSelected(scope.$parent) : false;
                }

                scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select(scope, false);
                    }

                    if(setSelected) {
                        setSelected(scope.$parent, selected);
                    }
                });

                tabsCtrl.addPane(scope, element.index());

                scope.location = $location;
                scope.$watch('location.path()', function(path) {
                    if (path == scope.url) {
                        scope.selected = true;
                    }
                });

                scope.$on('$destroy', function() {
                    tabsCtrl.removePane(scope);
                });

                scope.updateEntityList = function() {
                    element.find('*[entity-list]').each(function() {
                        $(this).scope().$broadcast('needUpdate');
                    });
                };
            },
            templateUrl: 'template/tabs/pane.html',
            replace: true
        };
    }])

    /**
     * Лениво-загружаемая вкладка
     * Обычно содержит формы добавления/редактирования
     *
     * parent - индекс родительской вкладки. На нее будет переключние в случае закрытия формы
     *
     * Пример:
     *     <ghostpane heading="Новая группа" url="/usergroup/create/" parent="3" template="{{ path('cms_rest_usergroup_form_add') }}"></ghostpane>
     */
    .directive('ghostpane', ['$parse', '$location', '$templateCache', '$timeout', function($parse, $location, $templateCache, $timeout) {
        return {
            require: '^tabs',
            restrict: 'EA',

            scope:{
                heading: '@',
                template: '@'
            },

            template: "<div class=\"tab-pane\" ng-class=\"{active: selected}\" ng-show=\"selected && !hidden\" ng-include=\"template\"></div>",
            replace: true,
            transclude: true,

            link: function($scope, element, attrs, tabsCtrl) {
                $scope.selected = false;
                $scope.hidden = true;
                $scope.location = $location;
                $scope.parent = (attrs.parent === undefined ? null : parseInt(attrs.parent, 10));

                $scope.url = attrs.url;

                if ($scope.url[$scope.url.length - 1] != '/' && $scope.url.indexOf('{id}') == -1) {
                    $scope.url = $scope.url + '/';
                }

                if ($scope.url[0] != '/') {
                    $scope.url = '/' + $scope.url;
                }

                $scope.urlRegexp = $scope.url.replace('{id}', '(\\d+)');
                $scope._url = $scope.url;

                $scope._heading = $scope.heading;

                $scope._template = $scope.template;
                $scope.template = '';
                $scope.getTemplateUrl = function(id) {
                    return $scope._template.replace('{id}', id);
                };
                $templateCache.put($scope._template, 'default empty');

                $scope.refresh = function(id) {
                    var tpl = $scope.template;

                    $scope.template = '';
                    $scope.url = $scope._url.replace('{id}', id);
                    $scope.heading = $scope._heading.replace('{id}', id);
                    $timeout(function() {
                        $templateCache.remove($scope.getTemplateUrl(id));
                        $scope.template = $scope.getTemplateUrl(id);
                    });
                };

                $scope.getParent = function() {
                    return tabsCtrl.panes[$scope.parent];
                };

                $scope.$watch('location.path()', function(path) {
                    var matcher = new RegExp($scope.urlRegexp);

                    if (matcher.test(path)) {
                        $scope.selected = true;
                        var id = path.match(matcher)[1];

                        if ($scope.hidden || $scope.lastId != id) {
                            $scope.refresh(id);
                            $scope.lastId = id;
                        }

                        $scope.hidden = false;
                    }
                });

                $scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select($scope, false);
                    }
                });

                $scope.$on('$destroy', function() {
                    tabsCtrl.removePane($scope);
                });

                $scope.$on('entityFormClosed', function(event) {
                    tabsCtrl.hidePane($scope);
                });

                $scope.updateEntityList = function() {
                    element.find('*[entity-list]').each(function() {
                        $(this).scope().$broadcast('needUpdate');
                    });
                };

                tabsCtrl.addPane($scope, element.index());
            }
        };
    }])
;

/**
 * The following features are still outstanding: animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html tooltips, and selector delegation.
 */
angular.module( 'ui.bootstrap.tooltip', [ 'ui.bootstrap.position' ] )

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
.provider( '$tooltip', function () {
  // The default options tooltip and popover.
  var defaultOptions = {
    placement: 'top',
    animation: true,
    popupDelay: 0
  };

  // Default hide triggers for each show trigger
  var triggerMap = {
    'mouseenter': 'mouseleave',
    'click': 'click',
    'focus': 'blur'
  };

  // The options specified to the provider globally.
  var globalOptions = {};

  /**
   * `options({})` allows global configuration of all tooltips in the
   * application.
   *
   *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
   *     // place tooltips left instead of top by default
   *     $tooltipProvider.options( { placement: 'left' } );
   *   });
   */
    this.options = function( value ) {
        angular.extend( globalOptions, value );
    };

  /**
   * This is a helper function for translating camel-case to snake-case.
   */
  function snake_case(name){
    var regexp = /[A-Z]/g;
    var separator = '-';
    return name.replace(regexp, function(letter, pos) {
      return (pos ? separator : '') + letter.toLowerCase();
    });
  }

  /**
   * Returns the actual instance of the $tooltip service.
   * TODO support multiple triggers
   */
  this.$get = [ '$window', '$compile', '$timeout', '$parse', '$document', '$position', function ( $window, $compile, $timeout, $parse, $document, $position ) {
    return function $tooltip ( type, prefix, defaultTriggerShow ) {
      var options = angular.extend( {}, defaultOptions, globalOptions );

      /**
       * Returns an object of show and hide triggers.
       *
       * If a trigger is supplied,
       * it is used to show the tooltip; otherwise, it will use the `trigger`
       * option passed to the `$tooltipProvider.options` method; else it will
       * default to the trigger supplied to this directive factory.
       *
       * The hide trigger is based on the show trigger. If the `trigger` option
       * was passed to the `$tooltipProvider.options` method, it will use the
       * mapped trigger from `triggerMap` or the passed trigger if the map is
       * undefined; otherwise, it uses the `triggerMap` value of the show
       * trigger; else it will just use the show trigger.
       */
      function setTriggers ( trigger ) {
        var show, hide;

        show = trigger || options.trigger || defaultTriggerShow;
        if ( angular.isDefined ( options.trigger ) ) {
          hide = triggerMap[options.trigger] || show;
        } else {
          hide = triggerMap[show] || show;
        }

        return {
          show: show,
          hide: hide
        };
      }

      var directiveName = snake_case( type );
      var triggers = setTriggers( undefined );

      var template =
        '<'+ directiveName +'-popup '+
          'title="{{tt_title}}" '+
          'content="{{tt_content}}" '+
          'placement="{{tt_placement}}" '+
          'animation="tt_animation()" '+
          'is-open="tt_isOpen"'+
          '>'+
        '</'+ directiveName +'-popup>';

      return {
        restrict: 'EA',
        scope: true,
        link: function link ( scope, element, attrs ) {
          var tooltip = $compile( template )( scope );
          var transitionTimeout;
          var popupTimeout;
          var $body;

          // By default, the tooltip is not open.
          // TODO add ability to start tooltip opened
          scope.tt_isOpen = false;

          function toggleTooltipBind () {
            if ( ! scope.tt_isOpen ) {
              showTooltipBind();
            } else {
              hideTooltipBind();
            }
          }

          // Show the tooltip with delay if specified, otherwise show it immediately
          function showTooltipBind() {
            if ( scope.tt_popupDelay ) {
              popupTimeout = $timeout( show, scope.tt_popupDelay );
            } else {
              scope.$apply( show );
            }
          }

          function hideTooltipBind () {
            scope.$apply(function () {
              hide();
            });
          }

          // Show the tooltip popup element.
          function show() {
            var position,
                ttWidth,
                ttHeight,
                ttPosition;

            // Don't show empty tooltips.
            if ( ! scope.tt_content ) {
              return;
            }

            // If there is a pending remove transition, we must cancel it, lest the
            // tooltip be mysteriously removed.
            if ( transitionTimeout ) {
              $timeout.cancel( transitionTimeout );
            }

            // Set the initial positioning.
            tooltip.css({ top: 0, left: 0, display: 'block' });

            // Now we add it to the DOM because need some info about it. But it's not
            // visible yet anyway.
            if ( options.appendToBody ) {
                $body = $body || $document.find( 'body' );
                $body.append( tooltip );
            } else {
              element.after( tooltip );
            }

            // Get the position of the directive element.
            position = options.appendToBody ? $position.offset( element ) : $position.position( element );

            // Get the height and width of the tooltip so we can center it.
            ttWidth = tooltip.prop( 'offsetWidth' );
            ttHeight = tooltip.prop( 'offsetHeight' );

            // Calculate the tooltip's top and left coordinates to center it with
            // this directive.
            switch ( scope.tt_placement ) {
              case 'right':
                ttPosition = {
                  top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                  left: (position.left + position.width) + 'px'
                };
                break;
              case 'bottom':
                ttPosition = {
                  top: (position.top + position.height) + 'px',
                  left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                };
                break;
              case 'left':
                ttPosition = {
                  top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                  left: (position.left - ttWidth) + 'px'
                };
                break;
              default:
                ttPosition = {
                  top: (position.top - ttHeight) + 'px',
                  left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                };
                break;
            }

            // Now set the calculated positioning.
            tooltip.css( ttPosition );

            // And show the tooltip.
            scope.tt_isOpen = true;
          }

          // Hide the tooltip popup element.
          function hide() {
            // First things first: we don't show it anymore.
            scope.tt_isOpen = false;

            //if tooltip is going to be shown after delay, we must cancel this
            $timeout.cancel( popupTimeout );

            // And now we remove it from the DOM. However, if we have animation, we
            // need to wait for it to expire beforehand.
            // FIXME: this is a placeholder for a port of the transitions library.
            if ( angular.isDefined( scope.tt_animation ) && scope.tt_animation() ) {
              transitionTimeout = $timeout( function () { tooltip.remove(); }, 500 );
            } else {
              tooltip.remove();
            }
          }

          /**
           * Observe the relevant attributes.
           */
          attrs.$observe( type, function ( val ) {
            scope.tt_content = val;
          });

          attrs.$observe( prefix+'Title', function ( val ) {
            scope.tt_title = val;
          });

          attrs.$observe( prefix+'Placement', function ( val ) {
            scope.tt_placement = angular.isDefined( val ) ? val : options.placement;
          });

          attrs.$observe( prefix+'Animation', function ( val ) {
            scope.tt_animation = angular.isDefined( val ) ? $parse( val ) : function(){ return options.animation; };
          });

          attrs.$observe( prefix+'PopupDelay', function ( val ) {
            var delay = parseInt( val, 10 );
            scope.tt_popupDelay = ! isNaN(delay) ? delay : options.popupDelay;
          });

          attrs.$observe( prefix+'Trigger', function ( val ) {
            element.unbind( triggers.show );
            element.unbind( triggers.hide );

            triggers = setTriggers( val );

            if ( triggers.show === triggers.hide ) {
              element.bind( triggers.show, toggleTooltipBind );
            } else {
              element.bind( triggers.show, showTooltipBind );
              element.bind( triggers.hide, hideTooltipBind );
            }
          });

          // if a tooltip is attached to <body> we need to remove it on
          // location change as its parent scope will probably not be destroyed
          // by the change.
          if ( options.appendToBody ) {
            scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess () {
            if ( scope.tt_isOpen ) {
              hide();
            }
          });
          }

          // if this trigger element is destroyed while the tooltip is open, we
          // need to close the tooltip.
          scope.$on('$destroy', function closeTooltipOnDestroy () {
            if ( scope.tt_isOpen ) {
              hide();
            }
          });
        }
      };
    };
  }];
})

.directive( 'tooltipPopup', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
    templateUrl: 'template/tooltip/tooltip-popup.html'
  };
})

.directive( 'tooltip', [ '$tooltip', function ( $tooltip ) {
  return $tooltip( 'tooltip', 'tooltip', 'mouseenter' );
}])

.directive( 'tooltipHtmlUnsafePopup', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
    templateUrl: 'template/tooltip/tooltip-html-unsafe-popup.html'
  };
})

.directive( 'tooltipHtmlUnsafe', [ '$tooltip', function ( $tooltip ) {
  return $tooltip( 'tooltipHtmlUnsafe', 'tooltip', 'mouseenter' );
}]);

/**
 * Модуль загрузчика изображений
 */
angular.module('cms.filesuploader.images', ['ui.bootstrap', 'cms.filesuploader.api', 'ui.sortable'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="images-uploader abstract-uploader">'
            + '<div class="files-list clearfix" ui-sortable="{placeholder:\'item placeholder\', forcePlaceholderSize:true, items:\'>.item\', opacity:0.8, tolerance:\'pointer\'}" ng-model="files">'
                + '<images-list-item file="file" ng-repeat="file in files"></images-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Изображения не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить изображения</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-info btn-small" tooltip="Заголовки изображений, опция Главная и сортировка сохраняется сразу, без отправки формы">'
                + '<i class="icon-question-sign icon-white"></i>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.images', template);
    }])

    /**
     * Элемент загрузчика изображений
     */
    .directive('imagesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="img-polaroid">'
                        + '<div class="btn btn-inverse sort-icon"><i class="icon-move icon-white"></i></div>'
                        + '<img ng-src="{{file.size.cms}}">'
                    + '</a>'
                    + '<input type="text" placeholder="Заголовок" ng-model="file.title">'
                    + '<label>'
                        + '<input type="checkbox" ng-model="file.main">'
                        + '&nbsp;Главная'
                    + '</label>'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="{{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i> Скачать</a>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()"><i class="icon-trash icon-white"></i> Удалить</div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление изображения';
                    var msg = 'Вы уверены, что хотите удалить изображение?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                }
            },

            link: function($scope) {
                var fst = true;
                var fst2 = true;

                // Редактирование имени файла
                $scope.$watch('file.title', function(title) {
                    if (fst) {
                        fst = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {title:title},
                        timeout: this.canceler.promise
                    });
                });

                // Редактирование чекбокса "Главная"
                $scope.$watch('file.main', function(main) {
                    if (fst2) {
                        fst2 = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {main:main},
                        timeout: this.canceler.promise
                    });
                });
            }
        }
    }])
;

/**
 * Модуль загрузчика видео
 */
angular.module('cms.filesuploader.video', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="video-uploader abstract-uploader">'
            + '<div class="files-list clearfix" ui-sortable="{placeholder:\'item placeholder\', forcePlaceholderSize:true, items:\'>.item\', opacity:0.8, tolerance:\'pointer\'}" ng-model="files">'
                + '<video-list-item file="file" ng-repeat="file in files"></video-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Видео не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-small btn-info" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить видео</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-small btn-info" ng-controller="videoFtpSelectCtrl" ng-click="ftpShowWindow()">'
                + '<i class="icon-white icon-hdd"></i>&nbsp;'
                + 'Выбрать по FTP'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-info btn-small" tooltip="Заголовки видео, обложки и сортировка сохраняется сразу, без отправки формы">'
                + '<i class="icon-question-sign icon-white"></i>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.video', template);

        var ftpTemplate = '<div style="width:1000px; height:450px;"><iframe src="'+window.elFinderBrowserUrl+'" width="1000" height="450" border="0" style="border:0 none; padding:0; margin:0;"></iframe></div>';
        $templateCache.put('cms.filesuploader.video.ftp', ftpTemplate);
    }])

    /**
     * Контроллер для выбора файла по FTP
     */
    .controller('videoFtpSelectCtrl', ['$scope', '$dialog', '$http', '$rootScope', function($scope, $dialog, $http, $rootScope) {
        /**
         * Диалог выбора файла по FTP
         */
        $scope.ftpShowWindow = function() {
            var d = $dialog.dialog({
                modalFade:true
            });

            var temp = window.tinymce;

            window.tinymce = {
                activeEditor: {
                    windowManager: {
                        getParams: function() {
                            return {
                                setUrl: function(url) {
                                    var self = window.location.protocol + '//' + window.location.host;

                                    if (url.toString().indexOf(self) === 0) {
                                        url = url.toString().substr(self.length);
                                    }

                                    $scope.$apply(function() {
                                        $scope.uploadByFtp(url);
                                    });
                                }
                            }
                        },

                        close: function(){
                            d.close();
                            window.tinymce = temp;
                        }
                    }
                }
            };

            d.open('cms.filesuploader.video.ftp');
        };

        /**
         * Загрузка видео по FTP
         */
        $scope.uploadByFtp = function(url) {
            $scope.$parent.inProgress++;

            $http({
                method:'POST',
                data: {url:url},
                url: $scope.$parent.url + 'byftp/'
            }).success(function(response) {
                $scope.$parent.inProgress--;

                if (response.success&& !!response.entity) {
                    $scope.$parent.files.push(response.entity);
                } else {
                    $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке видео' + response.msg, 10000);
                }
            }).error(function(response) {
                $scope.$parent.inProgress--;
                console.log(response);
                $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке видео' + response.msg, 10000);
            });
        };
    }])

    /**
     * Элемент загрузчика видео
     */
    .directive('videoListItem', ['$dialog', '$http', 'uploaderObject', '$parse', '$q', function($dialog, $http, uploaderObject, $parse, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix" url-image="url">'
                    + '<a ng-href="{{file.image.file.path}}" target="_blank" class="img-polaroid">'
                        + '<div class="btn btn-inverse sort-icon"><i class="icon-move icon-white"></i></div>'
                        + '<img ng-src="{{file.image.size.cms}}">'
                    + '</a>'
                    + '<input type="file" name="image" title="" class="hidden-input" />'
                    + '<input type="text" placeholder="Заголовок" ng-model="file.title">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="Скачать: {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-info btn-mini" tooltip="Изменить обложку" ng-click="changeImage()"><i class="icon-white" ng-class="uploadImageButtonIcon()"></i></div>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()" tooltip="Удалить"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление видео';
                    var msg = 'Вы уверены, что хотите удалить видео?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Клик по кнопке смены обложки
                 */
                $scope.changeImage = function() {
                    if (!$scope.inProgress) {
                        $scope.$imageInput.click();
                    }
                };

                /**
                 * Загрузка новой обложки
                 */
                $scope.uploadImage = function(file) {
                    $scope.inProgress = true;

                    uploaderObject.upload({
                        file: file,
                        url: $scope.urlImage + $scope.file.id,
                        fieldName: 'file',
                        oncomplete: function(uploadSuccess, response) {
                            $scope.$apply(function() {
                                $scope.inProgress = false;

                                if (uploadSuccess && !!response.image) {
                                    $scope.file.image = response.image;
                                } else {
                                    var errMsg = 'Произошла ошибка при загрузке файла: ' + file.name;

                                    if (!!response.errors && !!response.errors.file) {
                                        errMsg += ': ' + response.errors.file.join(', ');
                                    }

                                    $rootScope.$broadcast('ntf-error', errMsg, 10000);
                                }
                            });
                        }
                    });
                };

                /**
                 * Иконка кнопки смены обложки
                 */
                $scope.uploadImageButtonIcon = function() {
                    if ($scope.inProgress) {
                        return 'icon-refresh infiinite-rotate';
                    } else {
                        return 'icon-camera';
                    }
                };
            },

            link: function($scope, $element, attrs) {
                $scope.urlImage = $parse(attrs.urlImage)($scope.$parent) + 'change-image/';

                $scope.$imageInput = $element.find('input[type="file"]');
                $scope.inProgress = false;

                /**
                 * Ловля изменения файловго инпута для обложки
                 */
                $scope.$imageInput.on('change', function() {
                    if (this.files === undefined) return false;

                    for (var i = 0, file; file = this.files[i]; i++){
                        $scope.$apply(function() {
                            $scope.uploadImage(file);
                        });
                    }

                    $scope.$imageInput.val('');

                    return false;
                });

                // Редактирование имени файла
                var fst = true;
                $scope.$watch('file.title', function(title) {
                    if (fst) {
                        fst = false;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {title:title},
                        timeout: this.canceler.promise
                    });
                });
            }
        }
    }])
;

/**
 * Основные функции загрузчика файлов
 */
angular.module('cms.filesuploader.api', [])
    /**
     * Директива загрузчика
     */
    .directive('filesList', ['uploaderObject', '$rootScope', '$http', '$q', function(uploaderObject, $rootScope, $http, $q) {
        return {
            restrict: 'E',
            scope: true,

            templateUrl: function(tElement, tAttrs) {
                return 'cms.filesuploader.' + tAttrs.widget;
            },
            replace: true,

            controller: function($scope, $element) {
                /**
                 * Иконка к кнопке загрузки
                 */
                $scope.uploadButtonIcon = function() {
                    if ($scope.inProgress > 0) {
                        return 'icon-refresh infiinite-rotate icon-white';
                    } else {
                        return 'icon-upload icon-white';
                    }
                };


                /**
                 * Эмуляция клика по файловому инпуту
                 */
                $scope.emitClick = function() {
                    $scope.$fileInput.click();
                };


                /**
                 * Загрузка файла
                 */
                $scope.upload = function(file) {
                    $scope.inProgress++;

                    uploaderObject.upload({
                        file: file,
                        url: $scope.url,
                        fieldName: 'file',
                        oncomplete: function(uploadSuccess, response) {
                            $scope.$apply(function() {
                                $scope.inProgress--;

                                if (uploadSuccess && !!response.entity) {
                                    $scope.files.push(response.entity);
                                } else {
                                    var errMsg = 'Произошла ошибка при загрузке файла: ' + file.name;

                                    if (!!response.errors && !!response.errors.file) {
                                        errMsg += ': ' + response.errors.file.join(', ');
                                    }

                                    $rootScope.$broadcast('ntf-error', errMsg, 10000);
                                }
                            });
                        }
                    });
                };
            },

            link: function($scope, $element, attrs) {
                $scope.url = attrs.url;
                $scope.name = attrs.name;
                $scope.inProgress = 0;
                $scope.files = [];
                $scope.$fileInput = $element.find('.hidden-input');

                /**
                 * Ловля изменения файловго инпута
                 */
                $scope.$fileInput.on('change', function() {
                    if (this.files === undefined) return false;

                    for (var i = 0, file; file = this.files[i]; i++){
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    $scope.$fileInput.val('');

                    return false;
                });

                // Обработка драг-н-дроп
                var _dropCallback = function(e){
                    e.preventDefault();

                    var dt = e.originalEvent.dataTransfer;

                    $element.removeClass('dd-active');

                    if (dt.files.length == 0) return false;

                    for (var i = 0, file; file = dt.files[i]; i++) {
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    return false;
                };

                $element.on({
                    dragenter: function(){
                        $element.addClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                $element.children('.dd-helper').on({
                    dragleave: function(){
                        $element.removeClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                // Сортировка
                $scope.$watch('files', function(files, prevfiles) {
                    if (files.length == 0 || prevfiles === undefined) {
                        return;
                    }

                    var sort = {};

                    for(var i = 0; i < files.length; i++) {
                        sort[files[i].id] = i;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method:'POST',
                        url:$scope.url + 'sort/',
                        data: {sort:sort},
                        timeout: this.canceler.promise
                    });
                }, true);

                /**
                 * Событие удаления файла
                 */
                $scope.$on('remove', function(event, file) {
                    event.stopPropagation();

                    for(var i = 0; i < $scope.files.length; i++) {
                        if ($scope.files[i].id == file.id) {
                            $scope.files.splice(i, 1);
                            break;
                        }
                    }
                });

                if (!!attrs.files) {
                    $scope.files = $.parseJSON(attrs.files);
                }
            }
        }
    }])

    .factory('uploaderObject', [function() {
        /*
         * Загрузчик файла на сервер.
         * file       - объект File (обязателен)
         * url        - строка, указывает куда загружать (обязателен)
         * fieldName  - имя поля, содержащего файл (как если задать атрибут name тегу input)
         * onprogress - функция обратного вызова, вызывается при обновлении данных
         * oncomplete - функция обратного вызова, вызывается при завершении загрузки, принимает два параметра:
         *      uploaded - содержит true, в случае успеха и false, если возникли какие-либо ошибки;
         *      data - в случае успеха в него передается ответ сервера
         *
         *      если в процессе загрузки возникли ошибки, то в свойство lastError объекта помещается объект ошибки, содержащий два поля: code и text
         */
        var uploaderObject = function(params) {

            if(!params.file || !params.url) {
                return false;
            }

            this.xhr = new XMLHttpRequest();
            this.reader = new FileReader();

            this.progress = 0;
            this.uploaded = false;
            this.successful = false;
            this.lastError = false;

            var self = this;
            var uploadCanceled = false;

            self.cancelUpload = function() {
                uploadCanceled = true;
                this.xhr.abort();
            }

            self.reader.onload = function(){
                self.xhr.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable){
                        if(params.onprogress instanceof Function){
                            params.onprogress.call(self, e.loaded, e.total);
                        }
                    }
                }, false);

                self.xhr.upload.addEventListener("load", function(){
                    self.progress = 100;
                    self.uploaded = true;
                }, false);

                self.xhr.upload.addEventListener("error", function(e){
                    self.lastError = {
                        code: 1,
                        text: 'Error uploading on server'
                    };
                }, false);

                self.xhr.onreadystatechange = function(){
                    var callbackDefined = params.oncomplete instanceof Function;

                    if (this.readyState == 4){
                        if(this.status == 200){
                            if(!self.uploaded){
                                if(callbackDefined){
                                    params.oncomplete.call(self, false);
                                }
                            }else{
                                self.successful = true;

                                if(callbackDefined){
                                    params.oncomplete.call(self, true, $.parseJSON(this.responseText), uploadCanceled);
                                }
                            }
                        }else{
                            self.lastError = {
                                code: this.status,
                                text: 'HTTP response code is not OK ('+this.status+')'
                            };

                            if(callbackDefined){
                                params.oncomplete.call(self, false, null, uploadCanceled);
                            }
                        }
                    }
                };

                var boundary = "xxxxxxxxx";

                var body = "--" + boundary + "\r\n";
                body += "Content-Disposition: form-data; name='"+(params.fieldName || 'file')+"'; filename='" + unescape(encodeURIComponent(params.file.name)) + "'\r\n";
                body += "Accept: application/json\r\n"
                body += "Content-Type: application/octet-stream\r\n\r\n";
                body += self.reader.result + "\r\n";
                body += "--" + boundary + "--";

                self.xhr.open("POST", params.url);
                self.xhr.setRequestHeader("Cache-Control", "no-cache");
                self.xhr.setRequestHeader("Accept", "application/json");

                if(self.xhr.sendAsBinary){
                    self.xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary="+boundary);
                    self.xhr.sendAsBinary(body);
                }else{
                    var formData = new FormData();
                    formData.append(params.fieldName || 'file', params.file);
                    self.xhr.send(formData);
                }
            };

            self.reader.readAsBinaryString(params.file);
        };

        return {
            upload: function(params) {
                return new uploaderObject(params);
            }
        }
    }])
;

/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader.files', ['ui.bootstrap', 'cms.filesuploader.api'])
    .run(['$templateCache', function($templateCache) {
        var template = '<div class="files-uploader abstract-uploader">'
            + '<div class="files-list clearfix" ui-sortable="{placeholder:\'item placeholder\', forcePlaceholderSize:true, items:\'>.item\', opacity:0.8, tolerance:\'pointer\', axis:\'y\'}" ng-model="files">'
                + '<files-list-item file="file" ng-repeat="file in files"></files-list-item>'
                + '<div class="alert" ng-show="files.length == 0">Файлы не загружены</div>'
            + '</div>'
            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                + '<span ng-show="inProgress == 0">Загрузить файлы</span>'
                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
            + '</div>'
			+ '<div class="btn btn-small btn-info" ng-controller="fileFtpSelectCtrl" ng-click="ftpShowWindow()">'
                + '<i class="icon-white icon-hdd"></i>&nbsp;'
                + 'Выбрать по FTP'
            + '</div>'
            + '&nbsp;'
            + '<div class="btn btn-info btn-small" tooltip="Заголовки файлов, даты и сортировка сохраняется сразу, без отправки формы">'
                + '<i class="icon-question-sign icon-white"></i>'
            + '</div>'
            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
            + '<div class="dd-helper"></div>'
            + '</div>';
        $templateCache.put('cms.filesuploader.files', template);
    }])
	/**
     * Контроллер для выбора файла по FTP
     */
    .controller('fileFtpSelectCtrl', ['$scope', '$dialog', '$http', '$rootScope', function($scope, $dialog, $http, $rootScope) {
        /**
         * Диалог выбора файла по FTP
         */
        $scope.ftpShowWindow = function() {
            var d = $dialog.dialog({
                modalFade:true
            });

            var temp = window.tinymce;

            window.tinymce = {
                activeEditor: {
                    windowManager: {
                        getParams: function() {
                            return {
                                setUrl: function(url) {
                                    var self = window.location.protocol + '//' + window.location.host;

                                    if (url.toString().indexOf(self) === 0) {
                                        url = url.toString().substr(self.length);
                                    }

                                    $scope.$apply(function() {
                                        $scope.uploadByFtp(url);
                                    });
                                }
                            }
                        },

                        close: function(){
                            d.close();
                            window.tinymce = temp;
                        }
                    }
                }
            };

            d.open('cms.filesuploader.video.ftp');
        };

        /**
         * Загрузка видео по FTP
         */
        $scope.uploadByFtp = function(url) {
            $scope.$parent.inProgress++;

            $http({
                method:'POST',
                data: {url:url},
                url: $scope.$parent.url + 'byftp/'
            }).success(function(response) {
                $scope.$parent.inProgress--;

                if (response.success&& !!response.entity) {
                    $scope.$parent.files.push(response.entity);
                } else {
                    $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке файла' + response.msg, 10000);
                }
            }).error(function(response) {
                $scope.$parent.inProgress--;
                console.log(response);
                $rootScope.$broadcast('ntf-error', 'Возникла ошибка при загрузке файла' + response.msg, 10000);
            });
        };
    }])
    /**
     * Элемент загрузчика файлов
     */
    .directive('filesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item">'
                    + '<input type="text" ng-model="file.title" class="input-title">'
                    + '<input type="text" ng-model="file.date" class="input-date">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-small" tooltip="Скачать {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-small btn-inverse sort-icon" tooltip="Потяните для сортировки"><i class="icon-move icon-white"></i></div>'
                    + '<div class="btn btn-danger btn-small" tooltip="Удалить" ng-click="remove()"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление файла';
                    var msg = 'Вы уверены, что хотите удалить файл?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Обновление полей
                 */
                var fst = 0;
                $scope.updateFields = function() {
                    if (fst < 2) {
                        fst++;
                        return;
                    }

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        data: {title: $scope.file.title, date: $scope.file.date},
                        timeout: this.canceler.promise
                    });
                };
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', $scope.updateFields);

                // Редактирование даты файла
                $scope.$watch('file.date', $scope.updateFields);
            }
        }
    }])
;

/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader', ['cms.filesuploader.files', 'cms.filesuploader.images', 'cms.filesuploader.video']);

angular.module('cms', ['ui.bootstrap', 'cms.entity', 'cms.notification', 'cms.selectpicker', 'cms.tree', 'cms.filesuploader', 'cms.utils', 'cms.nodemove'])
    .config(['$locationProvider', '$httpProvider', '$tooltipProvider', function($locationProvider, $httpProvider, $tooltipProvider){
		$locationProvider.hashPrefix('!');
		$locationProvider.html5Mode(false);

        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        $httpProvider.defaults.transformRequest = [function(data) {
            var param = function(obj) {
                var query = '';
                var name, value, fullSubName, subValue, innerObj, i;

                for(name in obj) {
                    value = obj[name];

                    if(value instanceof Array) {
                        for(i=0; i<value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if(value instanceof Object) {
                        for(subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if(value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

        $tooltipProvider.options({
            appendToBody: true
        });
	}])
;

