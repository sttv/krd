<?php

namespace Q\FilesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Конвертирование видео
 */
class ConvertVideoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('q:files:convert:video')
            ->setDescription('Convert video')
            ->addOption('update', null, InputOption::VALUE_NONE, 'If set, all video will be updated')
            ->setHelp(<<<EOT
                      The <info>q:files:convert:video</info> converts video to different formats.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>------------------------------ Convertation start at '.date('H:i:s').' ------------------------------</info>');

        $this->getContainer()->get('doctrine')->getManager()->getRepository('QFilesBundle:Video')->convertVideos($input->getOption('update'), $output);

        $output->writeln('<info>----------------------------- Convertation complete at '.date('H:i:s').' ----------------------------</info>');
    }
}
