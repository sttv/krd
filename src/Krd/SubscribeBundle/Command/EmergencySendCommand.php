<?php

namespace Krd\SubscribeBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Запуск рассылки экстренных новостей
 */
class EmergencySendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('krd:subscribe:send:emergency');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('qcore.mailer');
        $emerRepo = $this->getManager()->getRepository('KrdNewsBundle:Emergency');
        $subscriberRepo = $this->getManager()->getRepository('KrdSubscribeBundle:ESubscriber');

        $emails = array();

        foreach ($subscriberRepo->findBy(array('active' => true)) as $subscriber) {
            $email = $subscriber->getEmail();
            $email = trim($email);

            if (!empty($email)) {
                $emails[] = $email;
            }
        }

        if (empty($emails)) {
            return;
        }

        $date = new DateTime();
        $date->modify('-1 days');

        $emerRepo->createQuery('UPDATE KrdNewsBundle:Emergency e SET e.active = 0 WHERE e.date < :date')
            ->setParameter('date', $date)
            ->getResult();

        $emerList = $emerRepo->createQuery('SELECT e FROM __CLASS__ e WHERE e.active = 1 AND e.sent = 0')
            ->getResult();

        if (empty($emerList)) {
            return;
        }

        $message = $mailer->newMessage('Рассылка срочных сообщений');

        $message->setTo($emails);

        try {
            $mailer->sendTemplate($message, 'KrdSubscribeBundle:Email:e-letter.html.twig', array('news' => $emerList));

            $emerRepo->createQuery('UPDATE __CLASS__ e SET e.sent = 1 WHERE e.active = 1 AND e.sent = 0')
                ->getResult();
        } catch (\Exception $e) {
            $this->getContainer()->get('logger')->crit('Ошибка рассылки экстренных сообщений', array($e->getMessage()));
        }
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
