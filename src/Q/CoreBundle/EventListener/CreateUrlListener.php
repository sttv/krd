<?php

namespace Q\CoreBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\NodeUrlIntf;


/**
 * Listener для сущностей, который генерирует URL
 * Авто-генерация url-ов для сущностей реализующих интерфейс NodeUrlIntf
 */
class CreateUrlListener
{
    /**
     * Symfony router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof NodeUrlIntf) {
            $parent = $entity->getParent();

            if ($parent instanceof Node) {
                try{
                    $detailUrl = $this->router->generate($entity->getDetailRoute(), array('name' => $entity->getName()));
                    $url = rtrim($parent->getUrl(true), '/').$detailUrl;
                    $entity->setUrl($url);
                } catch (\Exception $e) {
                    // none
                }
            }
        }
    }
}
