<?php

namespace Krd\NewsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Doctrine\ORM\EntityRepository;

/**
 * Ограничение выдачи тегов в админке
 */
class NewsThemeType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'multiple' => true,
            'required' => false,
            'class' => 'KrdNewsBundle:NewsTheme',
            'property' => 'title',
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('e')
                        ->andWhere('e.disabled = 0')
                        ->addOrderBy('e.title');
            },
        ));

        $resolver->addAllowedValues(array(
            'multiple' => array(true),
        ));
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'news_theme';
    }
}
