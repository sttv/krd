<?php

namespace Krd\SupportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

use Q\UserBundle\Entity\User;


/**
 * Запись есть - значит сообщение уже прочитано
 *
 * @ORM\Entity
 * @ORM\Table(name="support_notify")
 * @JMS\ExclusionPolicy("all")
 */
class Notify
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Message")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    public function __construct(\Krd\SupportBundle\Entity\Message $message = null, \Q\UserBundle\Entity\User $user = null)
    {
        if ($message) {
            $this->setMessage($message);
        }

        if ($user) {
            $this->setUser($user);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param \Krd\SupportBundle\Entity\Message $message
     * @return Notify
     */
    public function setMessage(\Krd\SupportBundle\Entity\Message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \Krd\SupportBundle\Entity\Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set user
     *
     * @param \Q\UserBundle\Entity\User $user
     * @return Notify
     */
    public function setUser(\Q\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
