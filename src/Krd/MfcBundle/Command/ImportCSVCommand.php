<?php

namespace Krd\MfcBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ImportCSVCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:mfc:import:csv')
            ->addArgument('file', InputArgument::REQUIRED, 'CSV file name')
            ->setDescription('Import CSV data to the map');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $this->getContainer()->getParameter(
                'kernel.root_dir'
            ) . '/../src/Krd/MfcBundle/Resources/import/' . $input->getArgument('file');
        $filePath = realpath($filePath);

        if (!is_file($filePath)) {
            $output->writeln('<error>File not found</error>');
        }

        if (!is_readable($filePath)) {
            $output->writeln('<error>File is not readable</error>');
        }

        $handle = fopen($filePath, "r");
        $em = $this->getContainer()->get('doctrine.orm.oldsite_entity_manager');

        while ($row = fgetcsv($handle, null, ';')) {
//            $st = $em->getConnection()->prepare('SELECT * FROM `krd__addresses` WHERE ');
//            $st->execute(array('map' => $map));
        }
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
}
