<?php

namespace Q\FilesBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\FilesBundle\Entity\Image;


/**
 * Контроллер изображений
 *
 * @Route("/rest/qfiles/image")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestImageController extends Controller
{
    protected $entityName = 'QFilesBundle:Image';

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_qfiles_image_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddAction(Request $request)
    {
        $entity = new Image();

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();

            if ($entity->hasFile()) {
                return array('success' => true, 'entity' => $entity);
            } else {
                $this->getDoctrine()->getManager()->remove($entity);
                $this->getDoctrine()->getManager()->flush();
                return array('success' => false, 'msg' => 'File not found');
            }
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование полей
     *
     * @Route("/{id}", name="cms_rest_qfiles_image_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()->getManager()->find($this->entityName, $id);

        if ($title = $request->get('title')) {
            $entity->setTitle($title);
        } elseif ($title === '') {
            $entity->setTitle($title);
        }

        if ($main = $request->get('main')) {
            $entity->setMain($main == 'true' || $main === true);
        }

        $this->getDoctrine()->getManager()->flush();

        return array('success' => true, 'entity' => $entity);
    }

    /**
     * Сортировка
     *
     * @Route("/sort/", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostSortAction(Request $request)
    {
        $sort = $request->get('sort', false);

        if ($sort) {
            foreach($sort as $id => $idx) {
                $entity = $this->getDoctrine()->getManager()->find($this->entityName, $id);
                if ($entity) {
                    $entity->setSort($idx);
                }
            }

            $this->getDoctrine()->getManager()->flush();
        }

        return array('success' => true);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_qfiles_image_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function restPostDeleteAction(Request $request, $id)
    {
        try {
            $entity = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM {$this->entityName} g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Not found');
        }

        $this->getDoctrine()->getManager()->remove($entity);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }
}
