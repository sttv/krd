<?php

namespace Krd\GalleryBundle\Modules;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль галереи
 */
class Gallery extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdGalleryBundle:Admin:Module/gallery.html.twig');
    }

    /**
     * Галерея на главной
     */
    public function renderIndexContent()
    {
        if ($gallery = $this->getRepository()->findOneByActive()) {
            return $this->twig->render('KrdGalleryBundle:Module:gallery/index.html.twig', array('gallery' => $gallery));
        }
    }

    /**
     * Получение текущей галареи
     */
    public function getCurrentGallery()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Подробный вид гелереи
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdGalleryBundle:Module:gallery/detail.html.twig', array('item' => $this->getCurrentGallery()));
    }

    /**
     * Список галерей
     */
    public function renderContent()
    {
        $items = $this->getRepository()
            ->createQueryBuilder('g')
            ->andWhere('g.active = 1')
            ->andWhere('g.parent = :parent')
            ->orderBy('g.date', 'DESC')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        $currentItem = $this->getCurrentGallery();

        foreach($items as &$item) {
            if ($item->getId() == $currentItem->getId()) {
                $item->setCurrent(true);
            }
        }

        return $this->twig->render('KrdGalleryBundle:Module:gallery/list.html.twig', array('list' => $items));
    }
	
	protected function getGlavaItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('g')
            ->andWhere('g.active = 1')
            ->andWhere('g.parent = :parent')
            ->orderBy('g.date', 'DESC')
            ->setParameter('parent', $this->node->getNode()->getId());

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }
	
	public function renderGlavaContent()
	{
		$items = $this->getGlavaItemsList(9);

        return $this->twig->render('KrdGlavaBundle:Module:gallery/list.html.twig', array('list' => $items));
	}

    /**
     * Галерея на главной странице департамента
     */
    public function renderDepartamentIndex()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 5)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $newsNode = $temp[0];
        } else {
            return '';
        }

        $queryBuilder->setParameter('parent', $newsNode->getId());

        $result = $queryBuilder->getQuery()
            ->setMaxResults(2)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($result[0])) {
            return $this->twig->render('KrdGalleryBundle:Module:gallery/departament-index.html.twig', array('item' => $result[0], 'hasBtn' => count($result) > 1));
        } else {
            return '';
        }

    }
}
