<?php

namespace Q\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * Коллекция текстовых полей
 */
class TextCollectionType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'type' => 'text',
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
            'cascade_validation' => true,
            'attr' => array('form-collection' => ''),
        ));
    }

    public function getParent()
    {
        return 'collection';
    }

    public function getName()
    {
        return 'text_collection';
    }
}
