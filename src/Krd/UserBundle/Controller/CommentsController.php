<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\KrdUserEvents;
use Krd\UserBundle\Form\Type\UserProfileType;


/**
 * Комментарии
 */
class CommentsController extends Controller implements ActiveSecuredController
{
    /**
     * Главная
     *
     * @Route("/", name="krd_user_comments")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * Список последних комментариев
     *
     * @Route("/list", name="krd_user_comments_list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Последние комментарии', 'url' => '#'));
    }

    /**
     * Список последних комментариев (JSON)
     *
     * @Route("/list/get.json", name="krd_user_comments_list_ajax", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function ajaxListAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        return $this->get('krd.user.modules.comments')->getLastComments(10);
    }

    /**
     * Список моих последних комментариев
     *
     * @Route("/list/my", name="krd_user_comments_list_my")
     * @Template()
     */
    public function listMyAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Ваши комментарии', 'url' => '#'));
    }

    /**
     * Список моих последних комментариев (JSON)
     *
     * @Route("/list/my/get.json", name="krd_user_comments_list_my_ajax", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function ajaxListMyAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        return $this->get('krd.user.modules.comments')->getMyComments(10);
    }

    /**
     * Список последних комментариев определенного раздела
     *
     * @Route("/list/{id}", requirements={"id"="^\d+$"}, name="krd_user_comments_list_node")
     * @Template()
     */
    public function listNodeAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($node = $this->getDoctrine()->getManager()->find('QCoreBundle:Node', $id)) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $node->getTitle(), 'url' => '#'));
            return array('node' => $node);
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список последних комментариев определенного раздела (JSON)
     * @Route("/list/{id}/get.json", requirements={"id"="^\d+$"}, name="krd_user_comments_list_node_ajax", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function ajaxListNodeAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        if ($node = $this->getDoctrine()->getManager()->find('QCoreBundle:Node', $id)) {
            return $this->get('krd.user.modules.comments')->getPageLastComments($node->getUrl(true), 10, true);
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
