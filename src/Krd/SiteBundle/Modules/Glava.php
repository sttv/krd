<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль сйата главы
 */
class Glava extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    /**
     * Получение последних событий
     * @param  integer $offset смещение выборки
     * @param  integer $count  Лимит выборки
     * @return array
     */
    public function getLastNews($offset = 0, $count = 1)
    {
        $offset = (int)$offset;
        $count = (int)$count;

        $statement = $this->em->getConnection()->prepare("SELECT N.`name` as 'title', N.`date`,
                CONCAT('http://glava.krd.ru/news/', N.`id`, '/') as 'url',
                N.`comments` as 'comments_count'
                FROM `evlanov__news` as N
                ORDER BY N.`date` DESC
                LIMIT {$offset}, {$count}");

        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Получение последних записей из прямой речи
     * @param  integer $offset смещение выборки
     * @param  integer $count  Лимит выборки
     * @return array
     */
    public function getLastSpeech($offset = 0, $count = 1)
    {
        $offset = (int)$offset;
        $count = (int)$count;

        $statement = $this->em->getConnection()->prepare("SELECT D.`name` as 'title', D.`date`,
                CONCAT('http://glava.krd.ru/documents/speech/', D.`id`, '/') as 'url',
                D.`comments` as 'comments_count'
                FROM `evlanov__documents` as D
                ORDER BY D.`date` DESC
                LIMIT {$offset}, {$count}");

        $statement->execute();
        return $statement->fetchAll();
    }

    public function onNodeRemove(Node $node)
    {
    }
}
