angular.module('ui.bootstrap.position', [])

/**
 * A set of utility methods that can be use to retrieve position of DOM elements.
 * It is meant to be used where we need to absolute-position DOM elements in
 * relation to other, existing elements (this is the case for tooltips, popovers,
 * typeahead suggestions etc.).
 */
  .factory('$position', ['$document', '$window', function ($document, $window) {

    return {
      /**
       * Provides read-only equivalent of jQuery's position function:
       * http://api.jquery.com/position/
       */
      position: function (element) {
        var position = angular.element(element).position();
        console.log('Position');

        return {
          width: element.prop('offsetWidth'),
          height: element.prop('offsetHeight'),
          top: position.top,
          left: position.left
        };
      },

      /**
       * Provides read-only equivalent of jQuery's offset function:
       * http://api.jquery.com/offset/
       */
      offset: function (element) {
        var offset = angular.element(element).offset();
        console.log('Offset');
        return {
          width: element.prop('offsetWidth'),
          height: element.prop('offsetHeight'),
          top: offset.top,
          left: offset.left
        };
      }
    };
  }]);
