angular.module('user', ['user.registration', 'user.profile', 'user.notifications', 'user.documents', 'user.bookmarks', 'user.appeal'])

    .run(['user', '$dialog', function(user, $dialog) {
        // if (user.isNewbie()) {
        //     setTimeout(function() {
        //         $dialog.create('newbiew-video-help-template');
        //     }, 300);
        // }
    }])

    /**
     * Доступ к пользователю
     */
    .factory('user', [function() {
        var user = function() {
            var user = window.___user || false;

            /**
             * Данные пользователя
             */
            this.me = function() {
                return user;
            };

            /**
             * Авторизован?
             */
            this.isAuthorized = function() {
                return user !== false;
            };

            /**
             * Новичек?
             */
            this.isNewbie = function() {
                return user.newbie === true;
            };

            /**
             * Есть роль?
             */
            this.isGranted = function(role) {
                if (user && user.groups && $.isArray(user.groups)) {
                    for (var i = 0, l = user.groups.length; i < l; i++) {
                        var group = user.groups[i];

                        if ($.isArray(group.roles)) {
                            for (var ii = 0, ll = group.roles.length; ii < ll; ii++) {
                                if (role == group.roles[ii] || group.roles[ii] == 'ROLE_IDDQD') {
                                    return true;
                                }
                            }
                        }
                    }
                }

                return false;
            };
        };

        return new user();
    }])

    /**
     * Сортировка виджетов в личном кабинете
     */
    .directive('lkSortableWidgets', ['$http', '$q', 'router', function($http, $q, router) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                var canceler = $q.defer();

                $element.sortable({
                    handle: '.icon-lk-drag-widget',
                    items: '>.lk-widget-sortable',
                    tolerance: 'pointer',
                    stop: function() {
                        var ids = [];

                        $element.children('.lk-widget').each(function() {
                            var id = $(this).data('id');

                            if (!!id) {
                                ids.push(id);
                            }
                        });

                        canceler.resolve();
                        canceler = $q.defer();

                        $http({
                            method: 'POST',
                            url: router.generate('krd_user_sort_widgets'),
                            data: {ids:ids},
                            timeout: canceler.promise
                        });
                    }
                });
            }
        }
    }])
;
