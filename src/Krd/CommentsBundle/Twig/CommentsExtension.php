<?php

namespace Krd\CommentsBundle\Twig;

use Twig_Environment;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\SessionCsrfProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Q\CoreBundle\Seo\SeoManager;
use Q\CoreBundle\Entity\Node;
use Krd\CommentsBundle\Entity\Comment;


/**
 * Расширение Twig
 */
class CommentsExtension extends \Twig_Extension
{
    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SessionCsrfProvider
     */
    protected $csrfProvider;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var SeoManager
     */
    protected $seoManager;

    public function __construct(Twig_Environment $twig, EntityManager $em, SessionCsrfProvider $csrfProvider, FormFactory $formFactory, Container $container, Router $router)
    {
        $this->twig = $twig;
        $this->em = $em;
        $this->csrfProvider = $csrfProvider;
        $this->formFactory = $formFactory;
        $this->container = $container;
        $this->router = $router;
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        if ($this->container->isScopeActive('request')) {
            return $this->container->get('request');
        }
    }

    /**
     * @return SeoManager
     */
    protected function getSeoManager()
    {
        if ($this->container->isScopeActive('request')) {
            return $this->container->get('qcore.seo.manager');
        }
    }

    public function getFunctions()
    {
        return array(
            'krd_comments' => new \Twig_Function_Method($this, 'krd_comments', array('is_safe' => array('html'))),
        );
    }

    public function getFilters()
    {
        return array(
            'comment_category' => new \Twig_Filter_Function(array($this, 'twig_comment_category_filter')),
        );
    }

    public function getName()
    {
        return 'krd_comments_comments';
    }

    /**
     * Блок комментариев страницы
     */
    public function krd_comments($where = null)
    {
        if (!$where && ($request = $this->getRequest())) {
            $where = $request->getPathInfo();
        }

        $comments = $this->em->getRepository('KrdCommentsBundle:Comment')->getPageList($where);

        $csrfToken = $this->csrfProvider->generateCsrfToken('authenticate');

        $comment = new Comment();
        $comment->setPage($where);

        if (($sm = $this->getSeoManager()) && $sm->hasTitle()) {
            $title = $sm->getTitle();
            $comment->setPageTitle(current($title));
        }

        $formRoot = $this->formFactory
            ->createNamedBuilder('comment_root_form', 'comments_comment_create', $comment)
            ->setAction($this->router->generate('krd_comments_create', array('type' => 'root')))
            ->getForm();

        $formReply = $this->formFactory
            ->createNamedBuilder('comment_form', 'comments_comment_create', $comment)
            ->setAction($this->router->generate('krd_comments_create', array('type' => 'child')))
            ->getForm();

        $formEdit = $this->formFactory
            ->createNamedBuilder('comment_form', 'comments_comment_create', new Comment())
            ->setAction($this->router->generate('krd_comments_edit', array('id' => '__id__')))
            ->getForm();


        return $this->twig->render('KrdCommentsBundle:blocks:comments.html.twig', array(
            'comments' => $comments,
            'csrf_token' => $csrfToken,
            'form_root' => $formRoot->createView(),
            'formReply' => $formReply->createView(),
            'formEdit' => $formEdit->createView(),
            'where' => $comment->getPageHash(),
        ));
    }

    /**
     * Получение наименования родительской категории комментария
     * @param  Comment $comment
     * @return string
     */
    public function twig_comment_category_filter(Comment $comment)
    {
        $page = $comment->getPage();
        preg_match_all('/^(\/[^\/]+)/', $page, $matches);

        if (isset($matches[0]) && isset($matches[0][0])) {
            $url = $matches[0][0].'/';

            $node = $this->em->getRepository('QCoreBundle:Node')->findOneBy(array('urlhash' => md5($url)));

            if ($node instanceof Node) {
                return $node->getTitle();
            }
        }

        return '';
    }
}
