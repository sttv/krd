/**
 * оформление контента
 */
angular.module('content', [])
    .run([function() {
        // Добавляем к контентным нумерованным спискам нужный аттрибут, чтобы не засорять scope всем li на странице
        $('.text-block li').attr('list-item', '');
    }])

    /**
     * Разворачивание вложенных пунктов меню
     */
    .directive('deepContentMenuItem', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.find('.wrap-icon').on('click', function(e) {
                    e.preventDefault();
                    $element.toggleClass('close open');
                });
            }
        }
    }])

    /**
     * Стилизация нумерованных списков
     */
    .directive('listItem', [function() {
        return {
            restrict: 'A',
            scope:{},
            compile: function($element) {
                if ($element.closest('.text-block').length == 0 || $element.parent('ol').length == 0) {
                    return function() {};
                }

                $element.prepend('<span class="number">{{ parent ? parent+\'.\' : \'\' }}{{ index + 1 }})</span>');

                var $parent = $element.parent('ol').parent('li');
                if ($parent.length == 0) {
                    $parent = $element.parent('ol').prev('li');
                }

                return function(scope, $element) {
                    var update = function() {
                        scope.$apply(function() {
                            scope.index = $element.parent('ol').children('li').index($element);

                            if ($parent.length != 0) {
                                scope.parent = $parent.data('index');
                            }
                        });
                    }

                    scope.$watch('index', function() {
                        var str = scope.parent ? scope.parent+'.' : ''
                        str += scope.index + 1;

                        $element.data('index', str);
                    });

                    setTimeout(update, 1);
                    setInterval(update, 1000);
                }
            }
        }
    }])

    /**
     * Удобочитаемый размер файла
     */
    .filter('filesize', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '-';
            }

            if (typeof precision === 'undefined') {
                precision = 1;
            }

            var units = ['bytes', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb'];
            var number = Math.floor(Math.log(bytes) / Math.log(1024));

            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })
;
