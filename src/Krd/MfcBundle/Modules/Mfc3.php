<?php

namespace Krd\MfcBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль MFC 3
 */
class Mfc3 extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    public function renderContent()
    {
        return $this->twig->render('KrdMfcBundle:Module:mfc/map3.html.twig');
    }
}


