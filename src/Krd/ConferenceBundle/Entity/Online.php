<?php

namespace Krd\ConferenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\SecurityContext;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\UserBundle\Entity\User;
use Q\FilesBundle\Entity\Image;
use Krd\AdministrationBundle\Entity\Human;


/**
 * Онлайн конференция
 *
 * @ORM\Entity
 * @ORM\Table(name="conference_online",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="datestart", columns={"datestart"}),
 *          @ORM\Index(name="dateend", columns={"dateend"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     datestart={"datetime", {
 *         "label"="Дата/Время начала конференции",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     dateend={"datetime", {
 *         "label"="Дата/Время окончания конференции",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     dateworkstart={"datetime", {
 *         "label"="Дата/Время начала работы конференции (можно задавать вопросы)",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     dateworkend={"datetime", {
 *         "label"="Дата/Время окончания работы конференции",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     humans={"entity", {
 *         "label"="Участники",
 *         "class"="KrdAdministrationBundle:Human",
 *         "property"="fio",
 *         "multiple"=true,
 *         "required"=false
 *     }},
 *     hPhotos={"files", {
 *         "label"="Фото участников (если не выбраны выше)",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     announce={"textarea", {
 *         "label"="Краткое содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     partners={"entity", {
 *         "label"="Информационные партнеры",
 *         "class"="KrdConferenceBundle:Partner",
 *         "property"="title",
 *         "multiple"=true,
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Online implements NodeUrlIntf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;


    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;


    /**
     * Дата начала
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $datestart;


    /**
     * Дата конца
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $dateend;


    /**
     * Дата начала
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $dateworkstart;


    /**
     * Дата конца
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $dateworkend;


    /**
     * Краткое содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $announce = '';


    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';


    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;


    /**
     * Участники конференции
     *
     * @ORM\ManyToMany(targetEntity="Krd\AdministrationBundle\Entity\Human", cascade={"persist"})
     * @ORM\JoinTable(name="conference_online_human_relation",
     *      joinColumns={@ORM\JoinColumn(name="online_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="human_id", referencedColumnName="id")}
     *      )
     *
     * @ORM\OrderBy({"fio" = "ASC"})
     */
    private $humans;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="conference_online_hphotos_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="online_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $hPhotos;

    /**
     * Информационные партнеры
     *
     * @ORM\ManyToMany(targetEntity="Krd\ConferenceBundle\Entity\Partner", cascade={"persist"})
     * @ORM\JoinTable(name="conference_online_partner_relation",
     *      joinColumns={@ORM\JoinColumn(name="online_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="partner_id", referencedColumnName="id")}
     *      )
     *
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private $partners;

    /**
     * Вопросы конференции
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="parent", cascade={"persist", "remove"})
     * @ORM\OrderBy({"dateask" = "ASC"})
     */
    private $questions;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @JMS\Expose
     * @JMS\SerializedName("humans_photos")
     * @JMS\Accessor(getter="getHumansPhotos")
     */
    private $humansPhotos;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("my_questions_count")
     * @JMS\Accessor(getter="countMyQuestions")
     */
    private $countMyQuestions;


    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var User
     */
    protected $user;

    public function setSecurityContext(SecurityContext $sc)
    {
        $this->securityContext = $sc;
        $this->user = $sc->getToken()->getUser();
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Заголовок из конкатенации ФИО участников через {$glue}
     * @return string
     */
    public function getHumansTitle($glue = ' и ')
    {
        $parts = array();

        foreach ($this->getHumans() as $human) {
            $parts[] = $human->getFio();
        }

        return implode($glue, $parts);
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDateStart()
    {
        return $this->datestart;
    }

    public function setDateStart(\DateTime $datestart)
    {
        $this->datestart = $datestart;
    }

    public function getDateEnd()
    {
        return $this->dateend;
    }

    public function setDateWorkEnd(\DateTime $dateworkend)
    {
        $this->dateworkend = $dateworkend;
    }

    public function getDateWorkStart()
    {
        return $this->dateworkstart;
    }

    public function setDateWorkStart(\DateTime $dateworkstart)
    {
        $this->dateworkstart = $dateworkstart;
    }

    public function getDateWorkEnd()
    {
        return $this->dateworkend;
    }

    public function setDateEnd(\DateTime $dateend)
    {
        $this->dateend = $dateend;
    }

    /**
     * Конференция открыта для вопросов?
     */
    public function isOpen()
    {
        return ($this->getDateWorkStart() <= new \DateTime()) && ($this->getDateWorkEnd() >= new \DateTime());
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getHumans()
    {
        return $this->humans ?: $this->humans = new ArrayCollection();
    }

    /**
     * Список участников конференции у которых требуется выводить ссылку
     * @return array
     */
    public function getHumansWithLink()
    {
        $result = array();

        foreach($this->getHumans() as $item) {
            if ($item->getShowLink()) {
                $result[] = $item;
            }
        }

        return $result;
    }

    public function hasHumans()
    {
        return count($this->getHumans()) > 0;
    }

    public function addHumans(Human $human)
    {
        if (!$this->getHumans()->contains($human)) {
            $this->getHumans()->add($human);
        }
    }

    public function removeHumans(Human $human)
    {
        if ($this->getHumans()->contains($human)) {
            $this->getHumans()->removeElement($human);
        }
    }

    /**
     * Список больших фотографий участников конференции
     * @return array
     */
    public function getHumansBigPhotos()
    {
        $result = array();

        if ($this->hasHumans()) {
            foreach($this->getHumans() as $human) {
                if ($human->hasImages()) {
                    $result[] = $human->getMainImage();
                }
            }
        } elseif ($this->hasHPhotos()) {
            $result = $this->getHPhotos();
        }

        return $result;
    }

    /**
     * Список фотографий участников конференции
     * @return array
     */
    public function getHumansPhotos()
    {
        $result = array();

        if ($this->hasHumans()) {
            foreach($this->getHumans() as $human) {
                if ($human->hasWhiteImages()) {
                    $result[] = $human->getMainWhiteImage();
                }
            }
        } elseif ($this->hasHPhotos()) {
            $result = $this->getHPhotos();
        }

        return $result;
    }

    public function getPartners()
    {
        return $this->partners ?: $this->partners = new ArrayCollection();
    }

    public function hasPartners()
    {
        return count($this->getPartners()) > 0;
    }

    public function addPartners(Partner $partner)
    {
        if (!$this->getPartners()->contains($partner)) {
            $this->getPartners()->add($partner);
        }
    }

    public function removePartners(Partner $partner)
    {
        if ($this->getPartners()->contains($partner)) {
            $this->getPartners()->removeElement($partner);
        }
    }

    public function hasQuestions()
    {
        return count($this->getQuestions()) > 0;
    }

    public function getQuestions()
    {
        return $this->questions ?: $this->questions = new ArrayCollection();
    }

    public function hasActiveQuestions()
    {
        return count($this->getActiveQuestions()) > 0;
    }

    public function getActiveQuestions()
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('active', true))
            ->orderBy(array('dateask' => Criteria::ASC));

        return $this->getQuestions()->matching($criteria);
    }

    public function hasMyQuestions()
    {
        return $this->countMyQuestions() > 0;
    }

    public function countMyQuestions()
    {
        return count($this->getMyQuestions());
    }

    public function getMyQuestions()
    {
        if ($this->user instanceof User) {
            $criteria = Criteria::create()
                ->andWhere(Criteria::expr()->eq('active', true))
                ->andWhere(Criteria::expr()->eq('createdBy', $this->user->getId()))
                ->orderBy(array('dateask' => Criteria::ASC));

            return $this->getQuestions()->matching($criteria);
        } else {
            return new ArrayCollection();
        }
    }

    public function getHPhotos()
    {
        return $this->hPhotos ?: $this->hPhotos = new ArrayCollection();
    }

    public function hasHPhotos()
    {
        return count($this->getHPhotos()) > 0;
    }

    public function addHPhotos(Image $image)
    {
        if (!$this->getHPhotos()->contains($image)) {
            $this->getHPhotos()->add($image);
        }
    }

    public function removeHPhotos(Image $image)
    {
        if ($this->getHPhotos()->contains($image)) {
            $this->getHPhotos()->removeElement($image);
        }
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getDetailRoute()
    {
        return 'krd_conference_online_detail';
    }
}


