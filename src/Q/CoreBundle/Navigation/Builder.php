<?php

namespace Q\CoreBundle\Navigation;

/**
 * Построение навигационного меню в CMS
 * Для добавления элементов меню создайте сервисы с тегом qcore.cms.navigation.item
 */
class Builder
{
    /**
     * Элементы меню
     */
    protected $_items = array();

    /**
     * Добавление пункта меню
     * @param Item $navigationItem [description]
     */
    public function addItem(Item $navigationItem)
    {
        $this->_items[] = $navigationItem;
    }

    /**
     * Возвращает массив с меню
     */
    public function getAll()
    {
        return $this->_items;
    }

    /**
     * Список разрешенных пользователю элементов
     * @return array
     */
    public function getAllGranted()
    {
        $res = array();

        foreach ($this->getAll() as $item) {
            if ($item->isGranted()) {
                $res[] = $item;
            }
        }

        return $res;
    }
}
