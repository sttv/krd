<?php

namespace Krd\DocumentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;
use Krd\DocumentBundle\Entity\Document;


/**
 * Редирект от старых ссылок на документы
 */
class RedirectController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirect('/dokumenty/');
    }

    /**
     * Отображение документа исключительно по хешу
     * Если документ не найден в базе, то подгружаем его с old.krd.ru
     *
     * @Route("/{hash}.{ext}", requirements={"hash"="[a-zA-Z0-9]{32}", "ext"="html|htm"})
     * @Template()
     */
    public function itemAction(Request $request, $hash)
    {
        $item = $this->get('krd.document.modules.document')->getRepository()->findOneBy(array('active' => 1, 'hash' => array(mb_strtoupper($hash), mb_strtolower($hash))));

        if ($item) {
            return $this->redirect($item->getUrl());
        }

        $item = new Document();
        $item->setHash($hash);
        $item->setDate(new \DateTime());

        $title = $this->get('krd_import.old.remote_loader')->getNormaTitle($item->getHash());
        $announce = $this->get('krd_import.old.remote_loader')->getNormaAnnounce($item->getHash());
        $content = $this->get('krd_import.old.remote_loader')->getNormaDetail($item->getHash());

        $item->setTitle($title);
        $item->setContent($announce.'<br />'.$content);

        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));

        return array('item' => $item);
    }
}
