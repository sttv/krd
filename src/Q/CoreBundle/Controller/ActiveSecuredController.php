<?php

namespace Q\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;


/**
 * Контроллеры, наслудующие данный интерфейс перед вызовом будут проверять разрешение на просмотр текущего нода.
 * Если доступ запрещен:
 *     - при наличи у контроллера метода notFoundAction вызывается он
 *     - при отсутствиии метода notFoundAction кидается NotFoundHttpException
 */
interface ActiveSecuredController
{
}
