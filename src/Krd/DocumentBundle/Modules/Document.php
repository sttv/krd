<?php

namespace Krd\DocumentBundle\Modules;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Form;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\DocumentBundle\Doctrine\Tools\VirtualPaginator;
use Krd\ImportBundle\Old\RemoteLoader;
use Krd\DocumentBundle\Entity\Document as DocumentEntity;


/**
 * Модуль документов
 */
class Document extends AbstractModule
{
    protected $remoteLoader;
    protected $formFactory;

    public function setRemoteLoader(RemoteLoader $remoteLoader)
    {
        $this->remoteLoader = $remoteLoader;
    }

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdDocumentBundle:Admin:Module/document.html.twig');
    }

    /**
     * Построение древовидного списка документов
     * @param  integer $root
     * @return array
     */
    public function getItems($root, $maxDepth = 3)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array('list' => array(), 'items' => array());
        }

        $levels = array();

        for ($i = $rootNode->getLevel() + 1; $i <= $rootNode->getLevel() + $maxDepth; $i++) {
            $levels[] = $i;
        }

        $struct = $this->em->createQueryBuilder()
            ->select('n')
            ->from('QCoreBundle:Node', 'n')
            ->andWhere('n.left > :left')
            ->andWhere('n.right < :right')
            ->andWhere('n.active = 1')
            ->andWhere('n.level IN (:levels)')
            ->orderBy('n.left', 'ASC')
            ->setParameters(array(
                'left' => $rootNode->getLeft(),
                'right' => $rootNode->getRight(),
                'levels' => $levels,
            ))
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        $tmp = array();
        foreach($struct as $node) {
            $node->setCustomData('subnodes', array());
            $node->setCustomData('level', 1);
            $tmp[$node->getId()] = $node;
        }
        $struct = $tmp;

        foreach($struct as $i => $node) {
            $items = $this->getRepository()->createQueryBuilder('d')
                ->andWhere('d.active = 1')
                ->andWhere('d.parent = :parent')
                ->orderBy('d.date', 'DESC')
                ->setMaxResults(3)
                ->setParameter('parent', $node->getId())
                ->getQuery()
                    ->useResultCache(true, 15)
                    ->getResult();

            $node->setCustomData('items', $items);

            $parentId = $node->getParent()->getId();
            $parentNode = isset($struct[$parentId]) ? $struct[$parentId] : null;

            if ($parentNode) {
                $lst = $parentNode->getCustomData('subnodes', array());
                $lst[] = $node;
                $node->setCustomData('level', 2);
                $parentNode->setCustomData('subnodes', $lst);

                unset($struct[$i]);
            }
        }

        foreach ($struct as $node) {
            foreach ($struct as $node2) {
                foreach ($node2->getCustomData('subnodes') as $subnode) {
                    if ($node->getParent()->getId() == $subnode->getId()) {
                        $lst = $subnode->getCustomData('subnodes', array());
                        $lst[] = $node;
                        $node->setCustomData('level', 3);
                        $subnode->setCustomData('subnodes', $lst);
                        unset($struct[$node->getId()]);
                    }
                }
            }
        }

        $itemsQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            ->orderBy('d.date', 'DESC')
            ->setParameter('parent', $rootNode->getId())
            ->getQuery();

        $items = Paginator::createFromRequest($this->request, $itemsQuery, true, 10, $rootNode->getUrl(true));

        return array('list' => $struct, 'items' => $items, 'rootNode' => $rootNode);
    }

    /**
     * Поиск по нормам
     * Параметры поиска берутся из запроса
     * @param  integer $root
     * @return array
     */
    public function getItemsNSR($root)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array('list' => array(), 'items' => array(), 'rootNode' => $rootNode);
        }

        $params = array(
            'acttype' => $this->request->query->get('acttype'),
            'owner' => $this->request->query->get('owner'),
            'startdate' => $this->request->query->get('startdate'),
            'enddate' => $this->request->query->get('enddate'),
            'registrationnumber' => $this->request->query->get('registrationnumber'),
            'originator' => $this->request->query->get('originator'),
            'selector1' => $this->request->query->get('selector1'),
            'sword1' => $this->request->query->get('sword1'),
            'sword2' => $this->request->query->get('sword2'),
            'sword3' => $this->request->query->get('sword3'),
            'selector2' => $this->request->query->get('selector2'),
            'bword1' => $this->request->query->get('bword1'),
            'bword2' => $this->request->query->get('bword2'),
            'bword3' => $this->request->query->get('bword3'),
            'searchorder' => $this->request->query->get('searchorder'),
            'searchfuzzy' => $this->request->query->get('searchfuzzy'),
        );

        $hashes = $this->remoteLoader->searchRequest($params);

        if (!empty($hashes)) {
            $itemsQuery = $this->em->createQueryBuilder()
                ->select('d, FIELD(d.hash, :hashes) as HIDDEN ord')
                ->from($this->entityName, 'd')
                ->andWhere('d.active = 1')
                ->andWhere('d.parent = :parent')
                ->andWhere('d.hash IN (:hashes)')
                ->orderBy('ord')
                ->setParameter('parent', $rootNode->getId())
                ->setParameter('hashes', $hashes)
                ->getQuery();

            $url = $this->request->getPathInfo();
            $query = $this->request->query->all();
            $query['page'] = '{page}';

            $items = VirtualPaginator::createFromRequest($this->request, $itemsQuery, true, 10, http_build_url($url, array('query' => http_build_query($query))));

            if ($items->getItemsCount() < count($hashes) && $items->getCurrentPage() == 1) {
                $eHashes = array();

                foreach ($items as $item) {
                    $eHashes[] = $item->getHash();
                }

                $nHashes = array_diff($hashes, $eHashes);
                $nHashes = array_reverse($nHashes);
                $nHashes = array_slice($nHashes, 0, 30);

                foreach ($nHashes as $hash) {
                    $title = $this->remoteLoader->getNormaTitle($hash);
                    $date = $this->remoteLoader->getNormaDate($hash);

                    if (!$date || !($date instanceof \DateTime)) {
                        $date = new \DateTime();
                    }

                    $item = new DocumentEntity();
                    $item->setHash($hash);
                    $item->setDate($date);
                    $item->setTitle($title);
                    $item->setUrl('/norma/'.$item->getHash().'.html');
                    $item->setParent($this->node->getNode());

                    $items->addVirtualItem($item);
                }
            }

            $items->reorderVirtual($hashes);
        } else {
            $items = array();
        }

        return array('list' => array(), 'items' => $items, 'rootNode' => $rootNode);
    }

    /**
     * Список документов для главной страницы
     * @param integer $root ID раздела для вывода
     */
    public function renderIndexPreview($root)
    {
        $result = (array)$this->getItems($root, 2);

        return $this->twig->render('KrdDocumentBundle:Module:document/list-preview.html.twig', $result);
    }

    /**
     * Создание формы поиска норм
     * @return FormBuilder
     */
    public function createNormaSearchFormBuilder()
    {
        return $this->formFactory
            ->createNamedBuilder(null, 'form', null, array('csrf_protection' => false))
            ->add('acttype', 'choice', array(
                'choices' => array(
                    '' => '---',
                    'Постановление' => 'Постановление',
                    'Решение' => 'Решение',
                ),
                'required' => false,
            ))
            ->add('owner', 'choice', array(
                'choices' => array(
                    '' => '---',
                    'Администрации муниципального образования' => 'Администрации муниципального образования',
                    'Главы администрации' => 'Главы администрации',
                    'Главы администрации муниципального образования' => 'Главы администрации муниципального образования',
                    'Главы городского самоуправления-мэра' => 'Главы городского самоуправления-мэра',
                    'Главы муниципального образования' => 'Главы муниципального образования',
                    'Городской Думы' => 'Городской Думы',
                    'Избирательной комиссии' => 'Избирательной комиссии',
                    'Мэрии' => 'Мэрии',
                ),
                'required' => false,
            ))
            ->add('startdate', 'text', array(
                'required' => false,
                'attr' => array(
                    'date-picker' => '',
                    'class' => 'date-mini',
                    'placeholder' => 'дд.мм.гггг',
                ),
            ))
            ->add('enddate', 'text', array(
                'required' => false,
                'attr' => array(
                    'date-picker' => '',
                    'class' => 'date-mini',
                    'placeholder' => 'дд.мм.гггг',
                ),
            ))
            ->add('registrationnumber', 'text', array(
                'required' => false,
            ))
            ->add('originator', 'choice', array(
                'choices' => array(
                    '' => '---',
                ),
                'required' => false,
            ))
            ->add('sword1', 'text', array(
                'required' => false,
            ))
            ->add('sword2', 'text', array(
                'required' => false,
            ))
            ->add('sword3', 'text', array(
                'required' => false,
            ))
            ->add('bword1', 'text', array(
                'required' => false,
            ))
            ->add('bword2', 'text', array(
                'required' => false,
            ))
            ->add('bword3', 'text', array(
                'required' => false,
            ))
            ->add('searchorder', 'choice', array(
                'choices' => array(
                    4 => 'дате подписания',
                    1 => 'количеству совпадений (релевантно)',
                ),
                'required' => false,
                'data' => $this->request->get('searchorder', 4)
            ))
        ;
    }

    /**
     * Список документов
     * Автоматически выводит вложенные категории (макс. 2 вложенности) с документами
     */
    public function renderContent()
    {
        $result = array();

        $pageHasForm = $this->node->getNode()->getController()->getId() == 14;
        $isSearch = count($this->request->query->keys()) > 10;

        if ($pageHasForm) {
            // Показываем форму поиска по нормам
            $formBuilder = $this->createNormaSearchFormBuilder();
            $formBuilder->setAction($this->node->getNode()->getUrl(true));
            $form = $formBuilder->getForm();
            $form->bind($this->request);
            $result['searchform'] = $form->createView();
        }

        if ($isSearch) {
            $result = array_merge($result, (array)$this->getItemsNSR($this->node->getNode()->getId()));
        } else {
            $result = array_merge($result, (array)$this->getItems($this->node->getNode()->getId()));
        }

        return $this->twig->render('KrdDocumentBundle:Module:document/list.html.twig', $result);
    }

    /**
     * Получение подробного элемента
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Детальный вид
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdDocumentBundle:Module:document/detail.html.twig', array('item' => $this->getCurrent()));
    }
}
