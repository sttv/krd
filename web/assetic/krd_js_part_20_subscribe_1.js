/**
 * Подписка на новости
 */
angular.module('subscribe', ['user.notifications'])
    .controller('subscribeFormCtrl', ['$scope', '$element', '$http', '$attrs', 'notifications', function($scope, $element, $http, $attrs, notifications) {

        function disableForm(form) {
            if (form.elements && form.elements.length > 0) {
                for (var i = 0; i < form.elements.length; i++) {
                    form.elements[i].disabled=true;
                }
            }
        }

        $scope.onSuccess = function(response) {
            $scope.$emit('dialog-hide');
            disableForm($element[0]);
            notifications.user.info($attrs.messageSubscribe);
        };

        $scope.unsubscribe = function() {
            var email = $element.find('input[type="text"]').val();

            if (email) {
                $http({
                    method: 'POST',
                    url: $element.attr('action') + '?unsubscribe=1',
                    data: {email: email}
                });

                $scope.$emit('dialog-hide');
                disableForm($element[0]);
                notifications.user.info($attrs.messageUnsubscribe);
            }
        };

        $scope.reload = function() {
            setTimeout(function() {
                window.location.reload();
            }, 1000);
        };
    }])
;
