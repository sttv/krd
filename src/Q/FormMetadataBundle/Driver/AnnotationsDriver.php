<?php
/*
 * This file is part of the Form Metadata library
 *
 * (c) Cameron Manderson <camm@flintinteractive.com.au>
 *
 * For full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Q\FormMetadataBundle\Driver;

use Doctrine\Common\Annotations\AnnotationReader;

use Q\FormMetadataBundle\FormMetadata;


/**
 *
 * @author camm (camm@flintinteractive.com.au)
 */
class AnnotationsDriver implements MetadataDriverInterface
{
    /**
     * Read the entity and create an associated metadata
     * @param $entity
     * @return null|FormMetadata
     */
    public function getMetadata($entity)
    {
        $metadata = new FormMetadata();

        $reader = new AnnotationReader();

        $reflectionClass = new \ReflectionClass(get_class($entity));

        while (is_object($reflectionClass)) {
            $properties = $reflectionClass->getProperties();
            foreach($properties as $property) {
                $field = $reader->getPropertyAnnotation($property, 'Q\FormMetadataBundle\Configuration\Field');
                if(!empty($field)) {
                    if(empty($field->name)) {
                        $field->name = $property->getName();
                    }
                    $metadata->addField($field);
                }
            }

            $fields = $reader->getClassAnnotation($reflectionClass, 'Q\FormMetadataBundle\Configuration\Fields');

            if (is_object($fields)) {
                foreach($fields->fields as $name => $item) {
                    $field = new \Q\FormMetadataBundle\Configuration\Field(array());
                    $field->value = $item[0];
                    $field->name = $name;
                    if (isset($item[1])) {
                        $field->options = $item[1];
                    }
                    $metadata->addField($field);
                }
            }

            $reflectionClass = $reflectionClass->getParentClass();
        }

        return $metadata;
    }
}
