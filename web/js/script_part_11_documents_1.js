angular.module('documents', [])
    /**
     * Меню с документами на главной
     */
    .directive('documentsMenu', [function() {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.find('.hoverable').each(function() {
                    var id = $(this).data('id');

                    if (!id) {
                        return;
                    }

                    $(this).attr('ng-click', 'open('+id+')');
                    $(this).attr('ng-class', '{active: current == '+id+'}');
                });

                return function($scope) {
                    $scope.current = -1;
                    $scope.$documents = $('.documents-preview');

                    $scope.$watch('current', function(current) {
                        var $item = $scope.$documents.filter('[data-id='+current+']');

                        $scope.$documents.stop(true, true);
                        $scope.$documents.not($item).slideUp(200, function() {
                            $item.slideDown(200);
                        });

                        if (!!$.scrollTo) {
                            $.scrollTo($('#documents-menu-anchor'), 200, {offset:{top:-60}});
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                /**
                 * Открытие вкладки
                 */
                $scope.open = function(id) {
                    if ($scope.current != id) {
                        $scope.current = id;
                    } else {
                        $scope.current = -1;
                    }
                };

                $scope.isOpened = function() {
                    return $scope.current != -1;
                };

                $scope.isEnabledFloating = function() {
                    var scroll = $(window).scrollTop() + $(window).height();
                    var $anchor = $('#documents-menu-end-anchor');
                    var top = $anchor.offset().top - 49;

                    return $scope.isOpened() && (scroll < top);
                };
            }
        };
    }])
;
