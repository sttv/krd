/**
 * Виджет городского репортера
 */
angular.module('widget.reporter', [])
    .directive('widgetReporter', ['$http', function($http) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var offset = 0;

                var $updateBtn = $element.find('.btn-group .btn-update');
                var $image = $element.find('.w .icon img');
                var $links = $element.find('.w a[target="_blank"]');
                var $date = $element.find('.w .date');
                var $title = $element.find('.w .title');
                var $wrap = $element.find('.w');

                var setPublication = function(pub) {
                    var image = new Image();

                    image.onload = function() {
                        $image.attr('src', pub.image);
                        $links.attr('href', pub.url);
                        $date.text(pub.date);
                        $title.text(pub.title);

                        $wrap.stop(true, true).animate({opacity:1}, 150);
                    };

                    image.src = pub.image;
                };

                var update = function() {
                    offset++;

                    $updateBtn.addClass('disabled');
                    $wrap.stop(true, true).animate({opacity:0.3}, 150);

                    $scope.$apply(function() {
                        $http({
                            method: 'GET',
                            url: attrs.url,
                            params: {offset:offset}
                        }).success(function(response) {
                            $updateBtn.removeClass('disabled');

                            if (!!response.publications && !!response.publications[0]) {
                                setPublication(response.publications[0]);
                            }
                        }).error(function() {
                            alert('Произошла ошибка, повторите попытку позже');
                            $updateBtn.removeClass('disabled');
                        });
                    });
                };

                $updateBtn.on('click', function(e) {
                    e.preventDefault();

                    if (!$updateBtn.hasClass('disabled')) {
                        update();
                    }
                });
            }
        }
    }])
;
