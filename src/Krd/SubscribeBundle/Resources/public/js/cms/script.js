/**
 * Страница рассылки
 */
angular.module('cms')
    /**
     * Кнопка добавления контентного блока
     */
    .directive('subscribeAddContent', ['$compile', '$rootScope', function($compile, $rootScope) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.$form = $('#'+attrs.subscribeAddContent);
                $scope.index = !!attrs.defaultIndex ? parseInt(attrs.defaultIndex, 10) : 0;

                $element.on('click', function(e) {
                    e.preventDefault();

                    var $block = $('<div subscribe-content-block>---</div>');

                    if ($scope.$form.find('.form-actions').length > 0) {
                        $scope.$form.find('.form-actions').before($block);
                    } else {
                        $scope.$form.append($block);
                    }

                    var $newScope = $rootScope.$new();
                    $newScope.index = $scope.index++;

                    $compile($block)($newScope);
                });
            }
        }
    }])

    /**
     * Кнопка "Сейчас"
     */
    .directive('subscribeNowBtn', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    var date = (new Date()).getFullYear() + '-' + ((new Date()).getMonth() + 1) + '-' + (new Date()).getDate() + ' ' + (new Date()).getHours() + ':' + (new Date()).getMinutes() + ':' + (new Date()).getSeconds();

                    $element.prev('input').val(date);
                });
            }
        }
    }])

    .controller('subscribeContentBlockCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.removeBlock = function() {
            $element.remove();
        };
    }])

    /**
     * Контентный блок
     */
    .directive('subscribeContentBlock', [function() {
        return {
            restrict: 'A',
            replace: true,
            scope:true,
            template: '<div>'
                        + '<div class="control-group">'
                            + '<h4 class="control-label">Блок контента</h4>'

                            + '<div class="controls" style="padding-top:11px;">'
                                + '<div class="btn btn-danger" ng-click="removeBlock()">Удалить</div>'
                            + '</div>'
                        + '</div>'

                        + '<div class="control-group">'
                            + '<label class="control-label required">Заголовок блока</label>'
                            + '<div class="controls">'
                                + '<input type="text" name="block_title[{{ index }}]" class="input-xxlarge" value="Новое на портале">'
                            + '</div>'
                        + '</div>'

                        + '<div class="control-group">'
                            + '<label class="control-label required">Содержание</label>'
                            + '<div class="controls">'
                                + '<textarea name="block_content[{{ index }}]" class="tinymce" data-theme="advanced"></textarea>'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            controller: function($scope, $element) {
                $scope.removeBlock = function() {
                    $element.remove();
                };
            }
        }
    }])
;
