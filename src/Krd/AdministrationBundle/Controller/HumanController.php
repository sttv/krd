<?php

namespace Krd\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Список людей структуры администрации
 */
class HumanController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template()
     */
    public function listAction()
    {

    }

    /**
     * Детальная страница
     *
     * @Route("/{name}.html", name="krd_human_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.administration.modules.human')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getFio(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
