<?php

namespace Q\FilesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Doctrine\ORM\Tools\Pagination\Paginator;


/**
 * Управление ресайзом изображений
 */
class ImageCacheController extends Controller
{
    /**
     * Ресайз изображений
     */
    public function createImageAction(Request $request, $sign0to2, $sign2to4, $paramskey, $id, $sign, $format)
    {
        $imageCacheUrlManager = $this->get('q_files.image_cache.manager.url');
        $imageCacheManager = $this->get('q_files.image_cache.manager');

        $sign = $sign0to2.$sign2to4.$sign;

        if ($sign != $imageCacheUrlManager->getSign(array($paramskey, $id, $format))) {
            throw $this->createNotFoundException('Sign not correct');
        }

        try {
            $imageCache = $imageCacheManager->createImageCache($request->getPathInfo(), $paramskey, $id);
        } catch (\Exception $e) {
//            $this->get('logger')->critical('Не удалось создать превью по ссылке '.$request->getPathInfo().'. Error: '.$e->getMessage());
            throw $this->createNotFoundException();
        }

        if ($imageCache) {
            $imageFileInfo = $imageCache->getFile();
            return new RedirectResponse($imageFileInfo['path']);
        } else {
            throw $this->createNotFoundException('File not found');
        }
    }
}
