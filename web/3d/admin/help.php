<h6 id="closeHelp">Элементы управления просмотром модели:</h6>
<div class="unity_help_list">
	<div class="help_column">
		<div class="help_item">
			<span id="number">1</span><h7>Панель управления просмотром:</h7>
			<ul class="unity_help_list_item">
				<li id="window"> — вызов панели управления;</li>
				<li id="hide"> — свернуть панель управления.</li>
			</ul>
		</div>
		<div class="help_item">
			<span id="number">2</span><h7>Переключение режимов отображения модели:</h7>
			<ul class="unity_help_list_item">
				<li id="maxmize"> — переход в полноэкранный режим;</li>
				<li id="minimize"> — выход из полноэкранного режима, или клавиша <strong>[ESC]</strong> на клавиатуре.</li>
			</ul>
		</div>
		<div class="help_item">
			<span id="number">3</span><h7>Выбор точки обзора модели:</h7>
			<ul class="unity_help_list_item">
				<i>Выбор камеры позволяющим отобразить экран с удобного ракурса:</i>
				<li id="cam_left"> — камера слева;</li>
				<li id="cam_right"> — камера справа;</li>
				<li id="cam_top"> — камера сверху;</li>
				<li id="cam_around"> — круговой облет камеры.</li>
			</ul>
		</div>
		<div class="help_item">
			<span id="number">4</span><h7>Способы управления пермещением камеры:</h7>
			<ul class="unity_help_list_item">
				<li id="cam_control"> — управление виртуальным джойстиком.</li>
				<i>Управление манипулятором-мышью, производится влево-вправо и вверх-вниз путем перемещения манипулятора</i>
			</ul>
		</div>
	</div>
	<div class="help_column">
		<div class="help_item">
			<ul class="unity_help_list_item_empty">
				<i>с зажатой левой или правой клавишей. Вращение скролла манипулятора позволяет перемещать изменять положение камеры, перемещая ее вверх-вниз относительно модели:</i>
				<li>— манипулятом «мышь», трекпадом или джойстиком;</li>
				<li>— клавиатурой, стрелками, либо клавишами:</li>
					<p>[<strong>A</strong> (Ф)]  — влево;</p>
					<p>[<strong>D</strong> (В)]  — вправо;</p>
					<p>[<strong>W</strong> (Ц)]  — вперед;</p>
					<p>[<strong>S</strong> (Ы)]  — назад;</p>
					<p>[<strong>Q</strong> (Й)]  — вниз;</p>
					<p>[<strong>E</strong> (У)]  — вверх.</p>
			</ul>
		</div>
		<div class="help_item">
			<span id="number">5</span><h7>Получение информации об объектах:</h7>
			<ul class="unity_help_list_item">
				<li id="none">— при активации системы на экране отображаются названия улиц микрорайона;</li>
				<li id="none">— для получения подробной информации по любому объекту застройки необходимо навести курсор на объект и кликнуть по нему левой клавишей манипулятора-мыши, появится Всплывающее окно с данными объекта.</li>
				<img src="#" id="cover"/>
				<li id="cam_close">— закрыть всплывающее окно можно, нажав на кнопку в правом верхнем углу.</li>
			</ul>
		</div>
		
	</div>
</div>