<?php

namespace Krd\AppealBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\AppealBundle\Entity\Appeal;


/**
 * Экспорт обращений
 */
class AppealExportTestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:appeal:export:test')
            ->setDescription('Export appeals to remote database')
            ->setHelp(<<<EOT
                      The <info>krd:appeal:export</info> use to export appeals to remote database.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getManager()->getRepository('KrdAppealBundle:Appeal');

        $i = 0;

        foreach($repo->findBy(array('id' => 119)) as $appeal) {
            $output->write('Appeal ['.$appeal->getId().']...');
            $output->writeln($this->export($appeal));
            $this->getManager()->flush($appeal);
            $i++;
        }

        if ($i == 0) {
            $output->writeln('Nothing to export');
        } else {
            $output->writeln('Processed <info>'.$i.'</info> appeals');
        }
    }

    /**
     * Экспорт обращения
     *
     * @param  Appeal $appeal
     * @return string
     */
    protected function export(Appeal $appeal)
    {
        mssql_connect('pgate.krd.ru:3341', 'SQLUS', 'sql.44');
        mssql_select_db('ObrGRWeb');
        var_dump(mssql_get_last_message());
        exit;
    }

    /**
     * Экспорт обращения используя удаленный сервер
     *
     * @param  Appeal $appeal
     * @return string
     */
    protected function exportRemote(Appeal $appeal)
    {
        $options = array();
        $options[CURLOPT_URL] = 'http://178.32.55.193/krd-appeal.php';
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_HEADER] = false;
        $options[CURLOPT_HTTPHEADER] = array('Accept-Language: ru,en-us;q=0.7,en;q=0.3');
        $options[CURLOPT_POST] = true;

        $options[CURLOPT_POSTFIELDS] =
            "fio1=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getName()))
            ."&fio2=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getLastname()))
            ."&fio3=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getMidname()))
            ."&adresat=".urlencode(iconv('utf-8', 'windows-1251', "Адресат: ".$appeal->getDestination()->getTitle().PHP_EOL.$appeal->getMessage()))
            ."&index=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getPostcode()))
            ."&city=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getCity()))
            ."&area=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getDistrict()))
            ."&street=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getStreet()))
            ."&house=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getHouse()))
            ."&flat=".( $appeal->getRoom() != '' ? urlencode(iconv('utf-8', 'windows-1251', $appeal->getRoom())) : 0 )
            ."&phone=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getPhone()))
            ."&email=".urlencode(iconv('utf-8', 'windows-1251', $appeal->getEmail()))
        ;

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result == 'success export') {
            $appeal->setStatus(Appeal::STATUS_EXPORTED);
            return '<info>success</info>';
        } else {
            $this->getContainer()->get('logger')->crit('Ошибка экспорт обращения', array($appeal->getId(), $result));
            return '<error>error</error>';
        }

        return $result;
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
