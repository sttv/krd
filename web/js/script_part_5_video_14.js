angular.module('video', [])
    /**
     * Видео плеер
     */
    .directive('mediaelementVideo', ['$timeout', function($timeout) {
        if (!$.fn.mediaelementplayer) {
            console.error('Mediaelement not found');
            return {};
        }

        return {
            restrict: 'C',
            link: function($scope, $element) {
                $timeout(function() {
                    $element.mediaelementplayer({
                        pluginPath: '/bundles/krdsite/vendor/mediaelement/',
                        alwaysShowControls: true,
                        iPadUseNativeControls: false,
                        iPhoneUseNativeControls: false,
                        AndroidUseNativeControls: false,
                        pauseOtherPlayers: true,
                        enableAutosize: false
                    });

                    $element.on('pause stop ended', function() {
                        $scope.$apply(function() {
                            $scope.$emit('video-stop');
                        });
                    });

                    $element.on('play', function() {
                        $scope.$apply(function() {
                            $scope.$emit('video-play');
                        });
                    });

                    $scope.$on('iconsSlider.next', function() {
                        $element.data('mediaelementplayer').pause();
                    });

                    $scope.$on('iconsSlider.prev', function() {
                        $element.data('mediaelementplayer').pause();
                    });
                });
            }
        }
    }])
;
