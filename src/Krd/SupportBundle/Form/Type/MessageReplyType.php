<?php

namespace Krd\SupportBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Форма ответа на сообщение
 */
class MessageReplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('parent', 'hidden_support_message');

        $content = $builder->create('content', 'textarea', array(
            'label' => false,
            'required' => true,
            'attr' => array('placeholder' => 'Ваше сообщение'),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($content);

        $builder->add('submit', 'submit', array(
            'label' => 'Отправить',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\SupportBundle\Entity\Message',
            'csrf_protection' => true,
        ));
    }

    public function getName()
    {
        return 'support_message_reply';
    }
}
