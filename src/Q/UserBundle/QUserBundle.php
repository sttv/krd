<?php

namespace Q\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class QUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
