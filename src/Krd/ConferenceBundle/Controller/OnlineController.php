<?php

namespace Krd\ConferenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Krd\ConferenceBundle\Entity\Question;


/**
 * Онлайн конференции
 */
class OnlineController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template()
     * @Method({"GET"})
     */
    public function listAction()
    {

    }

    /**
     * Страница
     *
     * @Route("/{name}.html", name="krd_conference_online_detail")
     * @Template()
     * @Method({"GET"})
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.conference.modules.online')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/conference/online/list.json", name="ajax_conference_online_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.conference.modules.online')->getItemsPast($request->get('parent', 1));
    }

    /**
     * Добавление вопроса
     *
     * @Route("/ajax/conference/online/post_question/", name="ajax_conference_online_question_post")
     * @Method({"POST"})
     */
    public function postQuestionAction(Request $request)
    {
        $formBuilder = $this->get('krd.conference.modules.online')->getQuestionFormBuilder();
        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $question = $form->getData();

        if ($question instanceof Question) {
            if (!$question->getParent()->isOpen()) {
                $this->get('logger')->err('Попытка добавить вопрос в закрытую конференцию', array($question->getParent()->getId()));
                return new JsonResponse(array('success' => false, 'errors' => array('question[fioask]' => array('Конференция закрыта для вопросов'))));
            }

            $question->setDateAsk(new \DateTime());
            $question->setActive(false);

            $this->getDoctrine()->getManager()->persist($question);
            $this->getDoctrine()->getManager()->flush();
        }


        return new JsonResponse(array('success' => true));
    }
}
