<?php

namespace Krd\UserBundle\Modules;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Admin\AbstractModule;
use Q\UserBundle\Entity\User;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Krd\AppealBundle\Entity\Appeal;


/**
 * Модуль интернет-приемной личного кабинета
 */
class AppealModule extends AbstractModule
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var User
     */
    protected $user;

    public function renderAdminContent()
    {
        // Nothing
    }

    /**
     * @param SecurityContext $securityContext
     */
    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    /**
     * Список обращений
     * @param integer $parent
     * @param integer[optional] $count
     * @return array
     */
    public function getItemsList($parent, $count = 3)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.createdBy = :user')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        if ($this->user instanceof User) {
            $queryBuilder->setParameters(array('parent' => $parent, 'user' => $this->user->getId()));
        } else {
            $queryBuilder->setParameters(array('parent' => $parent, 'user' => -1));
        }

        Paginator::disableQueryCache();

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count);
    }

    public function renderContent()
    {
        // 6444
        // 10846
        // 10849

        return $this->twig->render('KrdUserBundle:Module:appeal/index.html.twig', array(
            'list1' => $this->getItemsList(6444),
            'list2' => $this->getItemsList(10846),
            'list3' => $this->getItemsList(10849),
        ));
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget()
    {
        $list1 = $this->getItemsList(6444)->getItems();

        if (count($list1) < 3) {
            $list2 = $this->getItemsList(10846)->getItems();
        } else {
            $list2 = new ArrayCollection();
        }

        if (count($list2) + count($list1) < 3) {
            $list3 = $this->getItemsList(10849)->getItems();
        } else {
            $list3 = new ArrayCollection();
        }

        return $this->twig->render('KrdUserBundle:Module:appeal/widget.html.twig', array(
            'list1' => $list1,
            'list2' => $list2,
            'list3' => $list3,
        ));
    }

    /**
     * Удаление черновика пользователя по ID
     * @param  integer $id
     */
    public function removeMyDraft($id)
    {
        if ($this->user instanceof User) {
            $appeal = $this->getRepository()->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId(), 'status' => Appeal::STATUS_DRAFT));

            if ($appeal instanceof Appeal) {
                $this->em->remove($appeal);
                $this->em->flush();
            }
        }
    }
}
