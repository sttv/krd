<?php

namespace Krd\GlavaBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\VideoFormat;
use Q\FilesBundle\Manager\ImageCacheUrl;
use Krd\GlavaBundle\Entity\Video as VideoEntity;


/**
 * Модуль глава-видео
 */
class Video extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdGlavaBundle:Admin:Module/video.html.twig');
    }
	
	public function renderContent()
	{
		$result = $this->getItemsList(9);

        return $this->twig->render('KrdGlavaBundle:Module:video/list.html.twig', array('list' => $result));
	}
	
	/**
     * Список видео для текущей страницы
     * @return array
     */
    protected function getItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }
}
