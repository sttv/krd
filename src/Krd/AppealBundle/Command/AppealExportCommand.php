<?php

namespace Krd\AppealBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\AppealBundle\Entity\Appeal;


/**
 * Экспорт обращений
 */
class AppealExportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:appeal:export')
            ->setDescription('Export appeals to remote database')
            ->setHelp(<<<EOT
                      The <info>krd:appeal:export</info> use to export appeals to remote database.
EOT
                    );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getManager()->getRepository('KrdAppealBundle:Appeal');

        $i = 0;

        foreach($repo->findBy(array('status' => Appeal::STATUS_NEW)) as $appeal) {
            $output->write('Appeal ['.$appeal->getId().']...');
            $output->writeln($this->exportRemote($appeal));
            $this->getManager()->flush($appeal);
            $i++;
        }

        if ($i == 0) {
            $output->writeln('Nothing to export');
        } else {
            $output->writeln('Processed <info>'.$i.'</info> appeals');
        }
    }

    /**
     * Экспорт обращения используя удаленный сервер
     *
     * @param  Appeal $appeal
     * @return string
     */
    protected function exportRemote(Appeal $appeal)
    {
        try {
            if ($appeal->export()) {
                return '<info>success</info>';
            } else {
                $this->getContainer()->get('logger')->crit('Ошибка экспорт обращения', array($appeal->getId(), 'unknown'));
                return '<error>error: unknown</error>';
            }
        } catch (\Exception $e) {
            $this->getContainer()->get('logger')->crit('Ошибка экспорт обращения', array($appeal->getId(), $e->getMessage()));
            return '<error>error: '.$e->getMessage().'</error>';
        }
    }

    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}
