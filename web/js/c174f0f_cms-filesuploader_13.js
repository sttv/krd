/**
 * Модуль загрузчика файлов
 */
angular.module('cms.filesuploader', ['ui.bootstrap'])
    .factory('uploaderObject', [function() {
        /*
         * Загрузчик файла на сервер.
         * file       - объект File (обязателен)
         * url        - строка, указывает куда загружать (обязателен)
         * fieldName  - имя поля, содержащего файл (как если задать атрибут name тегу input)
         * onprogress - функция обратного вызова, вызывается при обновлении данных
         * oncomplete - функция обратного вызова, вызывается при завершении загрузки, принимает два параметра:
         *      uploaded - содержит true, в случае успеха и false, если возникли какие-либо ошибки;
         *      data - в случае успеха в него передается ответ сервера
         *
         *      если в процессе загрузки возникли ошибки, то в свойство lastError объекта помещается объект ошибки, содержащий два поля: code и text
         */
        var uploaderObject = function(params) {

            if(!params.file || !params.url) {
                return false;
            }

            this.xhr = new XMLHttpRequest();
            this.reader = new FileReader();

            this.progress = 0;
            this.uploaded = false;
            this.successful = false;
            this.lastError = false;

            var self = this;
            var uploadCanceled = false;

            self.cancelUpload = function() {
                uploadCanceled = true;
                this.xhr.abort();
            }

            self.reader.onload = function(){
                self.xhr.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable){
                        if(params.onprogress instanceof Function){
                            params.onprogress.call(self, e.loaded, e.total);
                        }
                    }
                }, false);

                self.xhr.upload.addEventListener("load", function(){
                    self.progress = 100;
                    self.uploaded = true;
                }, false);

                self.xhr.upload.addEventListener("error", function(e){
                    self.lastError = {
                        code: 1,
                        text: 'Error uploading on server'
                    };
                }, false);

                self.xhr.onreadystatechange = function(){
                    var callbackDefined = params.oncomplete instanceof Function;

                    if (this.readyState == 4){
                        if(this.status == 200){
                            if(!self.uploaded){
                                if(callbackDefined){
                                    params.oncomplete.call(self, false);
                                }
                            }else{
                                self.successful = true;

                                if(callbackDefined){
                                    params.oncomplete.call(self, true, $.parseJSON(this.responseText), uploadCanceled);
                                }
                            }
                        }else{
                            self.lastError = {
                                code: this.status,
                                text: 'HTTP response code is not OK ('+this.status+')'
                            };

                            if(callbackDefined){
                                params.oncomplete.call(self, false, null, uploadCanceled);
                            }
                        }
                    }
                };

                var boundary = "xxxxxxxxx";

                var body = "--" + boundary + "\r\n";
                body += "Content-Disposition: form-data; name='"+(params.fieldName || 'file')+"'; filename='" + unescape(encodeURIComponent(params.file.name)) + "'\r\n";
                body += "Accept: application/json\r\n"
                body += "Content-Type: application/octet-stream\r\n\r\n";
                body += self.reader.result + "\r\n";
                body += "--" + boundary + "--";

                self.xhr.open("POST", params.url);
                self.xhr.setRequestHeader("Cache-Control", "no-cache");
                self.xhr.setRequestHeader("Accept", "application/json");

                if(self.xhr.sendAsBinary){
                    self.xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary="+boundary);
                    self.xhr.sendAsBinary(body);
                }else{
                    var formData = new FormData();
                    formData.append(params.fieldName || 'file', params.file);
                    self.xhr.send(formData);
                }
            };

            self.reader.readAsBinaryString(params.file);
        };

        return {
            upload: function(params) {
                return new uploaderObject(params);
            }
        }
    }])

    /**
     * Загрузчик файлов
     */
    .directive('filesList', ['uploaderObject', '$rootScope', '$http', function(uploaderObject, $rootScope, $http) {
        return {
            restrict: 'E',
            scope: true,

            // Шаблоны разные для типов винжетов
            template: function(tElement, tAttrs) {
                switch(tAttrs.widget) {
                    case 'images':
                        return '<div class="images-uploader">'
                            + '<div class="files-list clearfix">'
                                + '<images-list-item file="file" ng-repeat="file in files"></images-list-item>'
                                + '<div class="alert" ng-show="files.length == 0">Изображения не загружены</div>'
                            + '</div>'
                            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
                            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                                + '<span ng-show="inProgress == 0">Загрузить изображения</span>'
                                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
                            + '</div>'
                            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
                            + '<div class="dd-helper"></div>'
                            + '</div>';

                    default:
                        return '<div class="files-uploader">'
                            + '<div class="files-list clearfix">'
                                + '<files-list-item file="file" ng-repeat="file in files"></files-list-item>'
                                + '<div class="alert" ng-show="files.length == 0">Файлы не загружены</div>'
                            + '</div>'
                            + '<input type="file" name="file" title="" multiple="true" class="hidden-input" />'
                            + '<div class="btn btn-info btn-small" ng-click="emitClick()">'
                                + '<i ng-class="uploadButtonIcon()"></i>&nbsp;'
                                + '<span ng-show="inProgress == 0">Загрузить файлы</span>'
                                + '<span ng-show="inProgress > 0">Загружается: {{ inProgress }}</span>'
                            + '</div>'
                            + '<input type="hidden" ng-repeat="(i,file) in files" name="{{name}}[{{i}}]" value="{{file.id}}" />'
                            + '<div class="dd-helper"></div>'
                            + '</div>';
                }
            },
            replace: true,

            controller: function($scope, $element) {
                $scope.uploadButtonIcon = function() {
                    if ($scope.inProgress > 0) {
                        return 'icon-refresh infiinite-rotate icon-white';
                    } else {
                        return 'icon-upload icon-white';
                    }
                };


                /**
                 * Эмуляция клика по файловому инпуту
                 */
                $scope.emitClick = function() {
                    $scope.$fileInput.click();
                };


                /**
                 * Загрузка файла
                 */
                $scope.upload = function(file) {
                    $scope.inProgress++;

                    uploaderObject.upload({
                        file: file,
                        url: $scope.url,
                        fieldName: 'file',
                        oncomplete: function(uploadSuccess, response) {
                            $scope.$apply(function() {
                                $scope.inProgress--;

                                if (uploadSuccess && !!response.entity) {
                                    $scope.files.push(response.entity);
                                } else {
                                    $rootScope.$broadcast('ntf-error', 'Произошла ошибка при загрузке файла: ' + file.name, 5000);
                                }
                            });
                        }
                    });
                };
            },

            link: function($scope, $element, attrs) {
                $scope.url = attrs.url;
                $scope.name = attrs.name;
                $scope.inProgress = 0;
                $scope.files = [];
                $scope.$fileInput = $element.find('.hidden-input');

                /**
                 * Ловля изменения файловго инпута
                 */
                $scope.$fileInput.on('change', function() {
                    if (this.files === undefined) return false;

                    for (var i = 0, file; file = this.files[i]; i++){
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    $scope.$fileInput.val('');

                    return false;
                });

                // Обработка драг-н-дроп
                var _dropCallback = function(e){
                    e.preventDefault();

                    var dt = e.originalEvent.dataTransfer;

                    $element.removeClass('dd-active');

                    if (dt.files.length == 0) return false;

                    for (var i = 0, file; file = dt.files[i]; i++) {
                        $scope.$apply(function() {
                            $scope.upload(file);
                        });
                    }

                    return false;
                };

                $element.on({
                    dragenter: function(){
                        $element.addClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                $element.children('.dd-helper').on({
                    dragleave: function(){
                        $element.removeClass('dd-active');
                    },

                    drop: _dropCallback,

                    dragover: function(e){
                        e.preventDefault();
                        return false;
                    }
                });

                /**
                 * Событие удаления файла
                 */
                $scope.$on('remove', function(event, file) {
                    event.stopPropagation();

                    for(var i = 0; i < $scope.files.length; i++) {
                        if ($scope.files[i].id == file.id) {
                            // $http({
                            //     method: 'DELETE',
                            //     url: $scope.url + file.id,
                            // });

                            $scope.files.splice(i, 1);

                            break;
                        }
                    }
                });

                if (!!attrs.files) {
                    $scope.files = $.parseJSON(attrs.files);
                }
            }
        }
    }])

    /**
     * Элемент загрузчика файлов
     */
    .directive('filesListItem', ['$dialog', '$http', '$q', function($dialog, $http, $q) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item">'
                    + '<input type="text" ng-model="file.title" class="input-title">'
                    + '<input type="text" ng-model="file.date" class="input-date">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-small" tooltip="Скачать {{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i></a>'
                    + '<div class="btn btn-danger btn-small" tooltip="Удалить" ng-click="remove()"><i class="icon-trash icon-white"></i></div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление файла';
                    var msg = 'Вы уверены, что хотите удалить файл?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                };

                /**
                 * Обновление полей
                 */
                $scope.updateFields = function() {
                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                   this.canceler = $q.defer();

                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {title: $scope.file.title, date: $scope.file.date},
                        timeout: this.canceler.promise
                    });
                };
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', $scope.updateFields);

                // Редактирование даты файла
                $scope.$watch('file.date', $scope.updateFields);
            }
        }
    }])

    /**
     * Элемент загрузчика изображений
     */
    .directive('imagesListItem', ['$dialog', '$http', function($dialog, $http) {
        return {
            restrict: 'E',
            scope: {
                file: '='
            },

            template: '<div class="item clearfix">'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="img-polaroid">'
                        + '<img ng-src="{{file.size.cms}}">'
                    + '</a>'
                    + '<input type="text" ng-model="file.title">'
                    + '<label>'
                        + '<input type="checkbox" ng-model="file.main">'
                        + '&nbsp;Главная'
                    + '</label>'
                    + '<a ng-href="{{file.file.path}}" target="_blank" class="btn btn-success btn-mini" tooltip="{{file.file.size|filesize}}"><i class="icon-download-alt icon-white"></i> Скачать</a>'
                    + '<div class="btn btn-danger btn-mini" ng-click="remove()"><i class="icon-trash icon-white"></i> Удалить</div>'
                + '</div>',
            replace: true,

            controller: function($scope) {
                /**
                 * Удаление файла
                 */
                $scope.remove = function() {
                    var title = 'Удаление изображения';
                    var msg = 'Вы уверены, что хотите удалить изображение?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.$emit('remove', $scope.file);
                        }
                    });
                }
            },

            link: function($scope) {
                // Редактирование имени файла
                $scope.$watch('file.title', function(title) {
                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {title:title}
                    });
                });

                // Редактирование чекбокса "Главная"
                $scope.$watch('file.main', function(main) {
                    $http({
                        method: 'POST',
                        url: $scope.$parent.url + $scope.file.id,
                        params: {main:main}
                    });
                });
            }
        }
    }])
;
