window.cms = angular.module('cms', ['utils']);

window.cms.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
		window.cms.routeProvider = $routeProvider;

		// $routeProvider.otherwise({redirectTo:'/'});

		$locationProvider.hashPrefix('!');
		$locationProvider.html5Mode(false);
	}]);
