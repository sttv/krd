<?php

namespace Q\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Валидатор системного имени элементов привязанных к ноду
 *
 * @Annotation
 */
class SystemName extends Constraint
{
    public $message = 'Системное имя может состоять из: a-z 0-9 - _';
    public $messageUnique = 'Элемент с таким системным именем существует';

    public function validatedBy()
    {
        return 'system_name';
    }
}
