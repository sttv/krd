/**
 * Интернет-приемная
 */
angular.module('user.appeal', [])
    /**
     * Вкладки с обращениями
     */
    .directive('userAppealTabs', [function() {
        return {
            restrict: 'A',
            compile: function($element, $scope) {
                $element.find('.tabs-head > .tab').each(function() {
                    $(this).attr('ng-class', '{active: isActive("'+$(this).data('name')+'")}');
                });

                $element.find('.tabs-body > .pane').each(function() {
                    $(this).attr('ng-show', 'isActive("'+$(this).data('name')+'")');
                });

                $element.children('.tabs-body').attr('ng-show', 'hasActive()');

                return function($scope) {
                    $scope.active = '';
                };
            },

            controller: ['$scope', function($scope) {
                $scope.setActive = function(name) {
                    $scope.active = name;
                };

                $scope.setNoActive = function() {
                    $scope.setActive('');
                };

                $scope.isActive = function(name) {
                    return name == $scope.active;
                };

                $scope.hasActive = function() {
                    return !!$scope.active;
                };
            }]
        }
    }])
;
