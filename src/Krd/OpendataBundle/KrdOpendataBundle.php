<?php

namespace Krd\OpendataBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Открытые данные
 */
class KrdOpendataBundle extends Bundle
{
}
