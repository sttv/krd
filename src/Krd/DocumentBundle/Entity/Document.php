<?php

namespace Krd\DocumentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;


/**
 * Документ
 *
 * @ORM\Entity
 * @ORM\Table(name="document",
 *      indexes={
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="official", columns={"official"}),
 *          @ORM\Index(name="autoupdate", columns={"autoupdate"}),
 *          @ORM\Index(name="forceupdate", columns={"forceupdate"}),
 *          @ORM\Index(name="hash", columns={"hash"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     announce={"textarea", {
 *         "label"="Краткое содержание",
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     official={"checkbox", {
 *         "label"="Официальное опубликование",
 *         "required"=false
 *     }},
 *     hash={"text", {
 *         "label"="Хэш<br /><em>(часть адреса после /www/norma.nsf/BaseSearch/)</em>",
 *         "required"=false
 *     }},
 *     autoupdate={"checkbox", {
 *         "label"="Автоматически обновлять с old.krd.ru<br /><em>Не чаще раза в час</em>",
 *         "required"=false
 *     }},
 *     forceupdate={"checkbox", {
 *         "label"="Принудительно обновить с old.krd.ru<br /><em>при следующем заходе на фронт-энд</em>",
 *         "required"=false
 *     }},
 *     files={"files", {
 *         "label"="Файлы"
 *     }},
 *     consclusionComplete={"checkbox", {
 *         "label"="Заключение. Экспертиза завершена",
 *         "required"=false
 *     }},
 *     consclusionDeveloper={"text", {
 *         "label"="Заключение. Наименование разработчика",
 *         "required"=false
 *     }},
 *     consclusionDate={"text", {
 *         "label"="Заключение. Дата опубликования",
 *         "required"=false
 *     }},
 *     consclusionExpertDate={"text", {
 *         "label"="Заключение. Срок подготовки экспертного заключения",
 *         "required"=false
 *     }},
 *     consclusionEmail={"text", {
 *         "label"="Заключение. E-mail",
 *         "required"=false
 *     }},
 *     consclusionFiles={"files", {
 *         "label"="Заключение. Файлы"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Document implements NodeUrlIntf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Название
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Хэш (часть адреса после "/www/norma.nsf/BaseSearch/")
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $hash;


    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y H:i'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;


    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;


    /**
     * Краткое содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $announce = '';


    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Versioned
     */
    private $content = '';


    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;


    /**
     * Официальное опубликование
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $official = false;


    /**
     * Автоматически обновлять с old.krd.ru
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $autoupdate = false;

    /**
     * Принудительно обновить с old.krd.ru при следующем заходе
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $forceupdate = false;


    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="document_file_relation",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $files;

    /**
     * Заключение. Экспертиза завершена
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     */
    private $consclusionComplete = false;

    /**
     * Заключение. Наименование разработчика
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $consclusionDeveloper;

    /**
     * Заключение. Дата размещения
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $consclusionDate;

    /**
     * Заключение. Срок подготовки экспертного заключения
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $consclusionExpertDate;

    /**
     * Заключение. E-mail
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $consclusionEmail;

    /**
     * Заключение. Файлы
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="document_confile_relation",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $consclusionFiles;


    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * Размер наименьшего файла
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\SerializedName("minfilesize")
     * @JMS\Accessor(getter="getMinFileSize")
     */
    private $minfilesize = '#';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->consclusionFiles = new ArrayCollection();
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function hasHash()
    {
        return !empty($this->hash);
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Автоматическая генерация системного имени
     */
    public function generateName()
    {
        if (!empty($this->hash)) {
            $this->name = mb_strtolower($this->hash);
        } elseif (!empty($this->date)) {
            $this->name = uniqid($this->date->format('d-m-y-h-i-s'));
        }
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function hasAnnounce()
    {
        return !empty($this->announce);
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function hasContent()
    {
        return !empty($this->content);
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getOfficial()
    {
        return $this->official;
    }

    public function setOfficial($official)
    {
        $this->official = (boolean)$official;
    }

    public function getAutoUpdate()
    {
        return $this->autoupdate;
    }

    public function setAutoUpdate($autoupdate)
    {
        $this->autoupdate = (boolean)$autoupdate;
    }

    public function getForceUpdate()
    {
        return $this->forceupdate;
    }

    public function setForceUpdate($forceupdate)
    {
        $this->forceupdate = (boolean)$forceupdate;
    }

    public function getFiles()
    {
        return $this->files ?: $this->files = new ArrayCollection();
    }

    public function hasFiles()
    {
        return count($this->getFiles()) > 0;
    }

    public function addFiles(File $file)
    {
        if (!$this->getFiles()->contains($file)) {
            $this->getFiles()->add($file);
        }
    }

    public function removeFiles(File $file)
    {
        if ($this->getFiles()->contains($file)) {
            $this->getFiles()->removeElement($file);
        }
    }

    /**
     * Размер минимального из прикрепленных файлов
     * @return string
     */
    public function getMinFileSize()
    {
        if (!$this->hasFiles()) {
            return 0;
        }

        $min = PHP_INT_MAX;
        foreach($this->getFiles() as $file) {
            $min = min($min, $file->getSize());
        }

        return $min;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getDetailRoute()
    {
        return 'krd_document_document_detail';
    }

    /**
     * Set consclusionComplete
     *
     * @param boolean $consclusionComplete
     * @return Document
     */
    public function setConsclusionComplete($consclusionComplete)
    {
        $this->consclusionComplete = $consclusionComplete;
    
        return $this;
    }

    /**
     * Get consclusionComplete
     *
     * @return boolean 
     */
    public function getConsclusionComplete()
    {
        return $this->consclusionComplete;
    }

    /**
     * Set consclusionDeveloper
     *
     * @param string $consclusionDeveloper
     * @return Document
     */
    public function setConsclusionDeveloper($consclusionDeveloper)
    {
        $this->consclusionDeveloper = $consclusionDeveloper;
    
        return $this;
    }

    /**
     * Get consclusionDeveloper
     *
     * @return string 
     */
    public function getConsclusionDeveloper()
    {
        return $this->consclusionDeveloper;
    }

    /**
     * Set consclusionDate
     *
     * @param string $consclusionDate
     * @return Document
     */
    public function setConsclusionDate($consclusionDate)
    {
        $this->consclusionDate = $consclusionDate;
    
        return $this;
    }

    /**
     * Get consclusionDate
     *
     * @return string
     */
    public function getConsclusionDate()
    {
        return $this->consclusionDate;
    }

    /**
     * Set consclusionEmail
     *
     * @param string $consclusionEmail
     * @return Document
     */
    public function setConsclusionEmail($consclusionEmail)
    {
        $this->consclusionEmail = $consclusionEmail;
    
        return $this;
    }

    /**
     * Get consclusionEmail
     *
     * @return string 
     */
    public function getConsclusionEmail()
    {
        return $this->consclusionEmail;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Document
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Document
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Add files
     *
     * @param \Q\FilesBundle\Entity\File $files
     * @return Document
     */
    public function addFile(\Q\FilesBundle\Entity\File $files)
    {
        $this->files[] = $files;
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \Q\FilesBundle\Entity\File $files
     */
    public function removeFile(\Q\FilesBundle\Entity\File $files)
    {
        $this->files->removeElement($files);
    }

    public function hasConsclusionFiles()
    {
        return count($this->getConsclusionFiles()) > 0;
    }

    /**
     * Add consclusionFiles
     *
     * @param \Q\FilesBundle\Entity\File $consclusionFiles
     * @return Document
     */
    public function addConsclusionFile(\Q\FilesBundle\Entity\File $consclusionFiles)
    {
        $this->consclusionFiles[] = $consclusionFiles;
    
        return $this;
    }

    /**
     * Remove consclusionFiles
     *
     * @param \Q\FilesBundle\Entity\File $consclusionFiles
     */
    public function removeConsclusionFile(\Q\FilesBundle\Entity\File $consclusionFiles)
    {
        $this->consclusionFiles->removeElement($consclusionFiles);
    }

    /**
     * Get consclusionFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsclusionFiles()
    {
        return $this->consclusionFiles;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Document
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Document
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Заключение заполнено полностью?
     */
    public function isConsclusionValid()
    {
        $dev = $this->getConsclusionDeveloper();
        $date = $this->getConsclusionDate();

        return !empty($dev) && !empty($date);
    }

    /**
     * Set consclusionExpertDate
     *
     * @param string $consclusionExpertDate
     * @return Document
     */
    public function setConsclusionExpertDate($consclusionExpertDate)
    {
        $this->consclusionExpertDate = $consclusionExpertDate;
    
        return $this;
    }

    /**
     * Get consclusionExpertDate
     *
     * @return string 
     */
    public function getConsclusionExpertDate()
    {
        return $this->consclusionExpertDate;
    }
}