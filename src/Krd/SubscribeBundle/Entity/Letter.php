<?php

namespace Krd\SubscribeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\Content;
use Krd\NewsBundle\Entity\News;


/**
 * Письмо для рассылки
 *
 * @ORM\Entity
 * @ORM\Table(name="subscribe_letter",
 *      indexes={
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="sent", columns={"sent"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Letter
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y (H:i)'>")
     *
     * @Gedmo\Versioned
     */
    private $date;

    /**
     * Заголовок
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $title;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     */
    private $active = false;

    /**
     * Разослано?
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     */
    private $sent = false;

    /**
     * Заголовок блока новостей
     * @ORM\Column(type="text", name="news_title")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $newsTitle;

    /**
     * @ORM\ManyToMany(targetEntity="Krd\NewsBundle\Entity\News")
     * @ORM\JoinTable(name="subscribe_letter_news_relation",
     *     joinColumns={@ORM\JoinColumn(name="letter_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"date" = "DESC"})
     *
     * @JMS\Expose
     */
    private $news;

    /**
     * @ORM\ManyToMany(targetEntity="Q\CoreBundle\Entity\Content", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="subscribe_letter_content_relation",
     *      joinColumns={@ORM\JoinColumn(name="letter_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="content_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $contents;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Состояние рассылки
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("state")
     * @JMS\Accessor(getter="getState")
     */
    private $state = '';

    public function getState()
    {
        if ($this->getSent()) {
            return 'Отправлено';
        } else {
            return 'Ожидает отправки';
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Letter
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Letter
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set sent
     *
     * @param boolean $sent
     * @return Letter
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return boolean
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Letter
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Letter
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add news
     *
     * @param \Krd\NewsBundle\Entity\News $news
     * @return Letter
     */
    public function addNew(\Krd\NewsBundle\Entity\News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \Krd\NewsBundle\Entity\News $news
     */
    public function removeNew(\Krd\NewsBundle\Entity\News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Get news sorted by node
     *
     * @return array
     */
    public function getSortedNews()
    {
        $news = array();

        foreach ($this->getNews() as $item) {
            $news[] = $item;
        }

        usort($news, array($this, '__sortNews'));

        return $news;
    }

    public function __sortNews($item1, $item2)
    {
        $sort = array(15188, 7, 6, 10490, 10489, 10359, 10358);

        $sort = array_reverse($sort);

        $i1 = array_search($item1->getParent()->getId(), $sort);
        $i2 = array_search($item2->getParent()->getId(), $sort);

        if ($i1 === false) {
            $i1 = -10;
        }

        if ($i2 === false) {
            $i2 = -10;
        }

        if ($i1 == $i2) {
            return $item1->getDate() > $item2->getDate() ? -1 : 1;
        }

        return $i1 > $i2 ? -1 : 1;
    }

    public function clearNews()
    {
        $this->news = new ArrayCollection();

        return $this;
    }

    public function hasNews()
    {
        return count($this->getNews()) > 0;
    }

    public function hasNewsItem(\Krd\NewsBundle\Entity\News $news)
    {
        if (!$this->hasNews()) {
            return false;
        }

        return $this->news->contains($news);
    }

    /**
     * Add contents
     *
     * @param \Q\CoreBundle\Entity\Content $contents
     * @return Letter
     */
    public function addContent(\Q\CoreBundle\Entity\Content $contents)
    {
        $this->contents[] = $contents;

        return $this;
    }

    /**
     * Remove contents
     *
     * @param \Q\CoreBundle\Entity\Content $contents
     */
    public function removeContent(\Q\CoreBundle\Entity\Content $contents)
    {
        $this->contents->removeElement($contents);
    }

    /**
     * Get contents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function hasContents()
    {
        return count($this->getContents()) > 0;
    }

    public function clearContents()
    {
        $this->contents = new ArrayCollection();

        return $this;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Letter
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Letter
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set newsTitle
     *
     * @param string $newsTitle
     * @return Letter
     */
    public function setNewsTitle($newsTitle)
    {
        $this->newsTitle = $newsTitle;

        return $this;
    }

    /**
     * Get newsTitle
     *
     * @return string
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    public function hasNewsTitle()
    {
        return !empty($this->newsTitle);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Letter
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}