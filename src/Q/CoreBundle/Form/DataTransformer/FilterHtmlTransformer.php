<?php

namespace Q\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


/**
 * Трансформер для фильтрации нежелательных пользовательских данных
 */
class FilterHtmlTransformer implements DataTransformerInterface
{

    /**
     * Ничего не делаем
     *
     * @param mixed $mixed
     *
     * @return mixed
     */
    public function transform($mixed)
    {
        return $mixed;
    }

    /**
     * Фильтруем входящую строку
     *
     * @param string $string
     *
     * @return string
     */
    public function reverseTransform($string)
    {
        if (!$string) {
            return null;
        }

        if (!is_string($string)) {
            return $string;
        }

        $string = strip_tags($string);
        $string = htmlspecialchars($string);

        return $string;
    }
}
