<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<div class="nav-collapse collapse navbar-responsive-collapse">
				<ul class="nav">
					<li<?if($page==1):?> class="active"<?endif;?>><a href="index.php">Главная</a></li>
					<li class="divider-vertical"></li>
					<?if($_SESSION['admin_user'] == 1):?>
					<li<?if($page==2):?> class="active"<?endif;?>><a href="users.php">Пользователи</a></li>
					<li class="divider-vertical"></li>
					<li<?if($page==3):?> class="active"<?endif;?>><a href="nodes.php">Добавление объекта</a></li>
					<li class="divider-vertical"></li>
					<?endif;?>
					<li<?if($page==4):?> class="active"<?endif;?>><a href="fields.php">Поля объекта</a></li>
					<li class="divider-vertical"></li>
					<li class="dropdown<?if($page==5):?> active<?endif;?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Объекты <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="items.php">Редактировать объекты</a></li>
							<li class="divider"></li>
							<li><a href="index.php?export">Выгрузить все объекты в файл</a></li>
						</ul>
					</li>
					<?if($_SESSION['admin_user'] == 1):?>
					<li class="divider-vertical"></li>
					<li<?if($page==6):?> class="active"<?endif;?>><a href="helpedit.php">Редактировать HELP</a></li>
					<?endif;?>
				</ul>
				<ul class="nav pull-right">
					<?if(isset($_SESSION['admin_user'])):?>
					<li><a href="index.php?exit">Выход</a></li>
					<?endif;?>
				</ul>
			</div><!-- /.nav-collapse -->
		</div>
	</div><!-- /navbar-inner -->
</div>