<?php

namespace Krd\BannersBundle\Navigation;

use Q\CoreBundle\Navigation\Item;


/**
 * Пункт меню управления баннерами
 */
class Banners extends Item
{
    public function getTitle()
    {
        return 'Баннеры';
    }

    public function getLink()
    {
        return $this->router->generate('cms_banners_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS_BANNER');
    }
}
