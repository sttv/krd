/**
 * Контроллер формы вопроса онлайн-конференции
 */
function conferenceQuestionFormCtrl($scope, $element) {
    $scope.onSuccess = function() {
        $element.find('.form-row').slideUp(200);
        $element.find('.page-title').animate({opacity:0}, 150, function() {
            $(this).text($(this).data('success')).animate({opacity:1}, 150);
        })
    };
}
conferenceQuestionFormCtrl.$inject = ['$scope', '$element'];
