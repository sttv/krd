<?php

namespace Krd\RssBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Список RSS лент
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * RSS лента
     *
     * @Route("/{name}.xml", name="krd_rss_feed")
     */
    public function feedAction(Request $request, $name)
    {
        $currentNode = $this->get('qcore.routing.current')->getNode();
        $feed = $this->get('krd.rss.modules.rss')->getRepository()->findOneActiveByNameAndParent($name, $currentNode->getId());

        if (!$feed) {
            throw $this->createNotFoundException();
        }

        $newsNode = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->getNodeByPath($feed->getNodeUrl());

        if (!$newsNode) {
            throw $this->createNotFoundException();
        }

        $newsQuery = $this->getDoctrine()->getManager()->getRepository('KrdNewsBundle:News')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1');
			
		if( $newsNode->getId() == 7 ) {
            $newsQuery->andWhere('e.parent IN(:parent, 15188)');
		} else {
            $newsQuery->andWhere('e.parent = :parent');
        }

        $newsQuery->addOrderBy('e.date', 'DESC')
            ->setParameter('parent', $newsNode)
            ->setMaxResults(30);

        if ($request->get('yandex') == 1) {
            $newsQuery->andWhere('e.rssdisabled = 0');
        }

        $news = $newsQuery->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        if (!$news) {
            $newsQuery = $this->getDoctrine()->getManager()->getRepository('KrdNewsBundle:Emergency')
                ->createQueryBuilder('e')
                ->andWhere('e.active = 1');

                $newsQuery->andWhere('e.parent = :parent');

            $newsQuery->addOrderBy('e.date', 'DESC')
                ->setParameter('parent', $newsNode)
                ->setMaxResults(30);

            $news = $newsQuery->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

            if (!$news) {
                throw $this->createNotFoundException();
            }
        }

        $result = array(
            'date' => new \DateTime(),
            'feed' => $feed,
            'items' => $news,
            'noyandex' => $request->get('noyandex', false) == true,
        );

        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        return $this->render('KrdRssBundle:Index:feed.xml.twig', $result, $response);
    }
}
