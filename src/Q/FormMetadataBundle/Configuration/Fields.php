<?php
namespace Q\FormMetadataBundle\Configuration;

/**
 * Конфигурация нескольких полей. Полезна при наследовании
 * @Annotation
 */
class Fields extends \Doctrine\Common\Annotations\Annotation
{
    /**
     * Fields list
     * @var array
     */
    public $fields = array();

    /**
     * Magic method for passing options through the annotation that are undefined
     * @param $name
     * @param $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }
}
