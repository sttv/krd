angular.module('cms.tree', [])
    /**
     * Дерево структуры
     */
    .directive('nodeTree', ['$http', '$parse', '$timeout', '$rootScope', function($http, $parse, $timeout, $rootScope) {
        // Предзагруженные ноды
        var __preload = {};

        return {
            restrict: 'A',
            scope: {
                url: '@',
                root: '@nodeTree',
                prefix: '@'
            },

            template: '<li ng-repeat="item in items" node-tree-item="item" prefix="prefix" url="url" class="node-tree-item" ng-class="{\'current\':item.current}"></li>',

            compile: function($element) {
                var $link = $element.children('a.link');

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.autoExpand = $parse(attrs.autoExpand)($scope);

                    var preload = $parse(attrs.preload)($scope);
                    if (!!preload) {
                        __preload = angular.extend(__preload, preload);
                    }

                    /**
                     * Обновленеи списка пунктов
                     */
                    $scope.updateItems = function(items) {
                        $scope.items = items;

                        $timeout(function() {
                            if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                                $scope.$broadcast('autoExpand', $scope.autoExpand);
                            }
                            $scope.$emit('loaded');
                        });
                    };

                    if (!!__preload[$scope.root]) {
                        $scope.updateItems(__preload[$scope.root]);
                    } else {
                        $http({
                                method: 'GET',
                                url: $scope.url,
                                params: {parent:$scope.root, count:1000, page:1}
                            })
                            .success(function(response) {
                                $scope.updateItems(response.items);
                            })
                            .error(function(response) {
                                console.log(response);
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                            });
                    }
                };
            }
        }
    }])

    /**
     * Элемент дерева
     */
    .directive('nodeTreeItem', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                item: '=nodeTreeItem',
                prefix: '=',
                url: '='
            },
            template: '<i class="expand-btn" ng-click="toggle()" ng-class="icon()"></i>'
                + '<a href="{{ prefix }}{{ item.url }}" class="link" target="_self">{{ item.title }}</a>',
            compile: function() {
                return function($scope, $element, attrs) {
                    $scope.expanded = false;
                    $scope.loading = false;

                    if (!$scope.item.title) {
                        $scope.item.title = '<Без названия>';
                    }

                    $scope.toggle = function() {
                        if (!$scope.$tree) {
                            $scope.createTree();
                            return;
                        }

                        if ($scope.expanded) {
                            $scope.$tree.slideUp(300);
                        } else {
                            $scope.$tree.slideDown(300);
                        }

                        $scope.expanded = !$scope.expanded;
                    }

                    $scope.createTree = function() {
                        $scope.expanded = true;
                        $scope.loading = true;
                        $scope.$tree = angular.element('<ul class="node-tree unstyled" node-tree="{{ item.id }}" prefix="{{ prefix }}" url="{{ url }}" ng-show="items.length > 0"></ul>');

                        $element.append($scope.$tree);
                        $compile($scope.$tree)($scope);

                        $scope.$on('loaded', function(event) {
                            event.stopPropagation();
                            $scope.loading = false;
                            if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                                $scope.$broadcast('autoExpand', $scope.autoExpand);
                            }
                        });
                    };

                    $scope.icon = function() {
                        if ($scope.loading) {
                            return 'icon-refresh infiinite-rotate';
                        }

                        if ($scope.item.isRoot) {
                            return 'icon-home';
                        }

                        if (!$scope.item.isParent) {
                            return 'icon-file';
                        }

                        if ($scope.expanded) {
                            return 'icon-folder-open';
                        } else {
                            return 'icon-folder-close';
                        }
                    };

                    $scope.$on('autoExpand', function(event, autoExpand) {
                        if (event.targetScope === $scope) {
                            return;
                        }

                        $scope.autoExpand = autoExpand;
                        if ($.inArray($scope.item.id, $scope.autoExpand) != -1) {
                            $scope.toggle();
                            $scope.item.current = true;
                        }
                    });
                };
            }
        }
    }])
;
