<?php

namespace Krd\ImportBundle\Importer;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\Content;
use Q\FilesBundle\Entity\File;
use Q\FilesBundle\Entity\Image;
use Krd\ImportBundle\Exception\ImportStructureException;
use Krd\ImportBundle\Old\Structure as OldStructure;
use Krd\NewsBundle\Entity\News;
use Krd\NewsBundle\Entity\NewsTheme;
use Krd\DocumentBundle\Entity\Document;


/**
 * Импорт разделов старого сайта
 */
class Structure extends AbstractImporter
{
    protected $em;
    protected $currentNode;
    protected $oldStructure;

    protected $uploadDir;

    public function __construct(EntityManager $em, CurrentNode $currentNode, OldStructure $oldStructure)
    {
        $this->em = $em;
        $this->currentNode = $currentNode;
        $this->oldStructure = $oldStructure;

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em->getConfiguration()->setResultCacheImpl(new \Krd\ImportBundle\Doctrine\Cache\NoCache());
    }

    public function setUploadDir($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    public function getUploadDir()
    {
        return $this->uploadDir;
    }

    /**
     * Рекурсивный импорт разделов структуры
     * @param  string $sourceUrlRes      URL старого сайта
     * @param  string $destinationUrlRes URL нового сайта
     * @param  boolean[optional] $menuMode Включает режим обновления положения раздела в меню
     * @param  boolean[optional] $updateMode Включает режим обновления существующего контента
     * @param  boolean[optional] $recursiveMode Рекурсивный импорт категорий
     */
    public function import($sourceUrlRes, $destinationUrlRes, $menuMode = false, $updateMode = false, $recursiveMode = false)
    {
        $sourceUrlRes = rtrim($sourceUrlRes, '/ ').'/';
        $destinationUrlRes = rtrim($destinationUrlRes, '/ ').'/';

        $this->outputWrite('<comment>Importing</comment> ['.$sourceUrlRes.'] to ['.$destinationUrlRes.']...');

        $sourceUrl = parse_url($sourceUrlRes);
        $destinationUrl = parse_url($destinationUrlRes);

        if ($sourceUrl['path'] == '/') {
            $this->outputWriteln('<error>Source url too short</error>');
            usleep(200);
            return;
        }

        if ($destinationUrl['path'] == '/') {
            $this->outputWriteln('<error>Destination url too short</error>');
            usleep(200);
            return;
        }

        $destinationNode = $this->em->getRepository('QCoreBundle:Node')->getNodeByPath($destinationUrl['path']);

        if (!$destinationNode) {
            $this->outputWriteln('<error>Destination url not found on this site</error>');
            usleep(200);
            return;
        }

        // Фиксим обращение к странице по алиасу
        if ($destinationNode->getUrlAlias() == $destinationUrl['path']) {
            $destinationUrl['path'] = $destinationNode->getUrl(false);
            $destinationUrlRes = $destinationUrl['scheme'].'://'.$destinationUrl['host'].$destinationUrl['path'];
        }

        $tmp = explode('/', trim($destinationUrl['path'], '/ '));
        $expectedLevel = count($tmp);

        if ($expectedLevel - $destinationNode->getLevel() > 1) {
            $this->outputWriteln('<error>Please create some structure of '.$destinationUrl['path'].'</error>');
            usleep(200);
            return;
        }

        $sourceNode = $this->oldStructure->getByPath($sourceUrl['path']);

        if (!$sourceNode) {
            $this->outputWriteln('<error>Source url not found on old2.krd.ru</error>');
            usleep(200);
            return;
        }

        $isContentPage = false;
        $isNewsPage = false;

        if ($sourceNode['tpl'] == 2) {
            $isContentPage = true;
        } elseif ($sourceNode['tpl'] == 3) {
            $isNewsPage = true;
        }

        if (!$isContentPage && !$isNewsPage) {
            $this->outputWriteln('<error>Can\'t import this page type</error>');
            usleep(200);
            return;
        }

        if ($isContentPage) {
            $this->outputWrite('content page ['.$sourceNode['id'].']['.$destinationNode->getId().']...');
        } elseif ($isNewsPage) {
            $this->outputWrite('news page ['.$sourceNode['id'].']['.$destinationNode->getId().']...');
        }

        if ($expectedLevel != $destinationNode->getLevel()) {
            // Раздел создается заного
            if ($isContentPage) {
                $destinationNode = $this->createEmptyNode($destinationNode, array_pop($tmp), $sourceNode['title'], 'content');
            } elseif ($isNewsPage) {
                $destinationNode = $this->createEmptyNode($destinationNode, array_pop($tmp), $sourceNode['title'], 'news');
            }

            $this->outputWrite('created...');
        } elseif ($updateMode) {
            // Проверяем соответствие типа раздела
            if ($isContentPage && $destinationNode->getController()->getId() != 2 && $destinationNode->getController()->getId() != 13) {
                $this->outputWriteln('<error>Destination page type error</error>');
                usleep(200);
                return;
            }

            if ($isNewsPage && $destinationNode->getController()->getId() != 3) {
                $this->outputWriteln('<error>Destination page type error</error>');
                usleep(200);
                return;
            }

            // Обновляем заголовок раздела
            $destinationNode->setTitle($sourceNode['title']);
            $this->em->flush();
            $this->outputWrite('updated...');
        }

        // Небольшой фикс контента
        // Из-за ошибки импорта контент иногда дублировался
        if ($isContentPage && ($contents = $this->em->getRepository('QCoreBundle:Content')->findByParent($destinationNode->getId()))) {
            if (count($contents) == 2 && $contents[0]->getName() == 'content' && $contents[1]->getName() == 'content') {
                $fContent = $contents[0]->getContent();
                if (empty($fContent)) {
                    $this->em->remove($contents[0]);
                } else {
                    $this->em->remove($contents[1]);
                }

                $this->em->flush();
            }
        }

        // Импорт новостей
        if ($isNewsPage && !empty($sourceNode['news'])) {
            $this->em->getConnection()->beginTransaction();
            $newsProcessed = 0;

            foreach($sourceNode['news'] as $oldNews) {
                $news = $this->em->getRepository('KrdNewsBundle:News')->findBy(array('parent' => $destinationNode->getId(), 'name' => $oldNews['request']));
                if (!empty($news)) {
                    $news = array_pop($news);
                }

                $needUpdate = false;

                if (!$news) {
                    $news = new News();
                    $news->setName($oldNews['request']);
                    $news->setParent($destinationNode);
                    $this->em->persist($news);
                    $needUpdate = true;
                } elseif ($updateMode) {
                    $needUpdate = true;
                }

                if ($needUpdate) {
                    $news->setTitle($oldNews['title']);
                    $news->setDate($oldNews['date']);
                    $news->setRedirect($oldNews['redirect']);
                    $news->setAnnounce($oldNews['anons']);
                    $news->setContent($oldNews['content']);
                    $news->setSeoTitle($oldNews['seo_title']);
                    $news->setSeoKeywords($oldNews['seo_keywords']);
                    $news->setSeoDescription($oldNews['seo_description']);
                    $news->setActive(true);

                    foreach($news->getThemes() as $theme) {
                        $news->removeThemes($theme);
                    }

                    foreach($news->getFiles() as $file) {
                        $news->removeFiles($file);
                        $this->em->remove($file);
                    }

                    foreach($news->getImages() as $images) {
                        $news->removeImages($images);
                        $this->em->remove($images);
                    }

                    foreach($news->getVideo() as $video) {
                        $news->removeVideo($video);
                        $this->em->remove($video);
                    }

                    $this->em->flush();

                    if (!empty($oldNews['tags'])) {
                        foreach($oldNews['tags'] as $tag) {
                            $theme = $this->em->getRepository('KrdNewsBundle:NewsTheme')->findByTitle($tag);
                            if ($theme) {
                                $theme = array_pop($theme);
                            } else {
                                $theme = new NewsTheme();
                                $theme->setTitle($tag);
                                $this->em->persist($theme);
                            }

                            $news->addThemes($theme);
                        }
                    }

                    if (!empty($oldNews['images'])) {
                        foreach($oldNews['images'] as $i => $imageArr) {
                            $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['path']), PATHINFO_EXTENSION);
                            $filesystem = new Filesystem();
                            $filesystem->dumpFile($tempPath, file_get_contents($imageArr['url']));

                            $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
                            $image->setTitle('');
                            $image->setSort($i);
                            $news->addImages($image);
                        }
                    }

                    if (!empty($oldNews['video']) && is_file($oldNews['video']['src'])) {
                        $tempPath = $this->getUploadDir().'/'.uniqid('video-', true).'.'.pathinfo(basename($oldNews['video']['src']), PATHINFO_EXTENSION);
                        $filesystem = new Filesystem();
                        $filesystem->copy($oldNews['video']['src'], $tempPath);

                        $video = $this->em->getRepository('QFilesBundle:Video')->createVideoFromFile($tempPath);
                        $video->setTitle('');
                        $news->addVideo($video);
                        $this->em->flush();

                        if (empty($oldNews['video']['icon'])) {
                            $this->em->getRepository('QFilesBundle:Video')->cretePreview($video);
                        } else {
                            $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($oldNews['video']['icon']['path']), PATHINFO_EXTENSION);
                            $filesystem = new Filesystem();
                            $filesystem->dumpFile($tempPath, file_get_contents($oldNews['video']['icon']['url']));

                            $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
                            $video->setImage($image);
                            $this->em->flush();
                        }
                    }

                    $this->em->flush();
                    $this->outputWrite('.');
                    $newsProcessed++;

                    if ($newsProcessed % 30 == 0) {
                        $this->em->getConnection()->commit();
                        $this->em->clear('KrdNewsBundle:News');
                        $this->outputWrite($newsProcessed);
                        $this->em->getConnection()->beginTransaction();
                    }
                }

                unset($news);
            }

            $this->em->getConnection()->commit();
            $this->outputWrite('imported '.$newsProcessed.' news...');
        }


        // Добавляем контент на страницу
        if ($isContentPage && isset($sourceNode['content']) && !empty($sourceNode['content']['content'])) {

            $this->em->getConnection()->beginTransaction();
            $content = $this->em->getRepository('QCoreBundle:Content')->findOneByParent($destinationNode->getId());

            // Замена внутренних ссылок
            $sourceNode['content']['content'] = str_replace($sourceUrl['path'], $destinationUrl['path'], $sourceNode['content']['content']);
            $sourceNode['content']['content'] = str_replace($sourceUrl['scheme'].'://'.$sourceUrl['host'].$destinationUrl['path'], $destinationUrl['path'], $sourceNode['content']['content']);
            $sourceNode['content']['content'] = str_replace($sourceUrl['scheme'].'://www.'.$sourceUrl['host'].$destinationUrl['path'], $destinationUrl['path'], $sourceNode['content']['content']);

            if (mb_strpos($sourceNode['content']['content'], 'http://') || mb_strpos($sourceNode['content']['content'], 'https://') || mb_strpos($sourceNode['content']['content'], '"//')) {
                $this->outputWrite('<question>May has an external links, images or files</question>...');
            }

            if (!$content) {
                // Создаем заного
                $content = new Content();
                $content->setParent($destinationNode);
                $content->setTitle('Контент');
                $content->setName('content');
                $content->setContent($sourceNode['content']['content']);

                $this->em->persist($content);
                $this->em->flush();
                $this->outputWrite('content created ['.$content->getId().']...');
            } elseif ($updateMode) {
                // Обновляем существующий контент
                $content->setContent($sourceNode['content']['content']);
                $this->em->flush();
                $this->outputWrite('content updated ['.$content->getId().']...');
            }

            $date = $sourceNode['content']['date']->format('Y-m-d H:i:s');

            $this->em->getConnection()->exec("UPDATE `krdru13__content` SET `created` = '{$date}', `updated` = '{$date}' WHERE `id` = {$content->getId()}");
            $this->em->getConnection()->commit();
        } else {
            if ($isContentPage) {
                $this->outputWrite('<comment>no content</comment>...');
            }
        }

        // Файлы на странице
        if ($isContentPage && !empty($sourceNode['files'])) {
            $content = $this->em->getRepository('QCoreBundle:Content')->findOneByParent($destinationNode->getId());

            $update = $updateMode;

            if (!$content) {
                $content = new Content();
                $content->setParent($destinationNode);
                $content->setTitle('Контент');
                $content->setName('content');

                $this->em->persist($content);
                $update = true;
            }

            $this->em->flush();

            if ($update) {
                $this->em->getConnection()->beginTransaction();

                foreach($content->getFiles() as $file) {
                    $content->removeFiles($file);
                    $this->em->remove($file);
                }

                $this->em->flush();
                $this->em->getConnection()->commit();

                foreach($sourceNode['files'] as $fileArr) {
                    $this->em->getConnection()->beginTransaction();
                    $filesystem = new Filesystem();
                    $savePath = $this->getUploadDir().str_replace('/files/file', '', $fileArr['path']);
                    $filesystem->dumpFile($savePath, file_get_contents($fileArr['url']));

                    $fileInfo = new \Symfony\Component\HttpFoundation\File\File($savePath);

                    $file = new File();
                    $file->setTitle($fileArr['title']);
                    $file->setDate($fileArr['date']);
                    $uploadedArray = array(
                        'fileName' => str_replace('/files/file', '', $fileArr['path']),
                        'originalName' => basename($fileArr['file']),
                        'mimeType' => $fileInfo->getMimeType(),
                        'size' => $fileInfo->getSize(),
                        'path' => str_replace('/files/file', '/'.basename($this->getUploadDir()), $fileArr['path']),
                    );

                    $this->em->persist($file);
                    $content->addFiles($file);
                    $this->em->flush();

                    $file->setFileArray($uploadedArray);
                    $this->em->flush();

                    $this->em->getConnection()->commit();
                }

                $this->outputWrite(count($sourceNode['files']).' files added ['.$content->getId().']...');
            }
        }

        // Изображения на странице
        if ($isContentPage && !empty($sourceNode['images'])) {
            $content = $this->em->getRepository('QCoreBundle:Content')->findOneByParent($destinationNode->getId());

            $update = $updateMode;

            if (!$content) {
                $content = new Content();
                $content->setParent($destinationNode);
                $content->setTitle('Контент');
                $content->setName('content');

                $this->em->persist($content);
                $update = true;
            }

            $this->em->flush();

            if ($update) {
                $this->em->getConnection()->beginTransaction();

                foreach($content->getImages() as $image) {
                    $content->removeImages($image);
                    $this->em->remove($image);
                }

                $this->em->flush();
                $this->em->getConnection()->commit();

                foreach($sourceNode['images'] as $imageArr) {
                    $this->em->getConnection()->beginTransaction();
                    $filesystem = new Filesystem();
                    $savePath = $this->getUploadDir().str_replace('/file', '', $imageArr['path']);
                    $filesystem->dumpFile($savePath, file_get_contents($imageArr['url']));

                    $fileInfo = new \Symfony\Component\HttpFoundation\File\File($savePath);

                    $image = new Image();
                    $image->setTitle($imageArr['title']);
                    $uploadedArray = array(
                        'fileName' => str_replace('/file', '/'.basename($this->getUploadDir()), $imageArr['path']),
                        'originalName' => basename($imageArr['file']),
                        'mimeType' => $fileInfo->getMimeType(),
                        'size' => $fileInfo->getSize(),
                        'path' => str_replace('/file', '/'.basename($this->getUploadDir()), $imageArr['path']),
                        'width' => $imageArr['width'],
                        'height' => $imageArr['height'],
                    );

                    $this->em->persist($image);
                    $content->addImages($image);
                    $this->em->flush();

                    $image->setFileArray($uploadedArray);
                    $this->em->flush();

                    $this->em->getConnection()->commit();
                }

                $this->outputWrite(count($sourceNode['images']).' images added...');
            }
        }

        if ($isContentPage && isset($sourceNode['documents']) && !empty($sourceNode['documents'])) {
            $destinationNode->setController($this->em->find('QCoreBundle:NodeController', 13));

            if ($updateMode) {
                $list = $this->em->getRepository('KrdDocumentBundle:Document')->findBy(array('parent' => $destinationNode->getId()));

                $this->em->getConnection()->beginTransaction();

                foreach($list as $doc) {
                    $this->em->remove($doc);
                }

                $this->em->flush();
                $this->em->getConnection()->commit();
            }

            $i = 0;
            $this->em->getConnection()->beginTransaction();

            foreach ($sourceNode['documents'] as $doc) {
                $existsDoc = $this->em->getRepository('KrdDocumentBundle:Document')->findOneBy(array('parent' => $destinationNode->getId(), 'name' => mb_strtolower($doc['key'])));

                if (!$existsDoc) {
                    $document = new Document();
                    $document->setTitle($doc['title']);
                    $document->setDate($doc['date']);
                    $document->setActive(true);
                    $document->setOfficial($doc['official'] == 1);
                    $document->setHash(mb_strtoupper($doc['key']));
                    $document->setName(mb_strtolower($doc['key']));
                    $document->setAutoupdate(true);
                    $document->setForceupdate(true);
                    $document->setParent($destinationNode);

                    $this->em->persist($document);
                    $this->em->flush();
                    $i++;
                    $this->outputWrite('.');

                    if ($i % 30 == 0) {
                        $this->outputWrite($i);
                        $this->em->getConnection()->commit();
                        $this->em->getConnection()->beginTransaction();
                    }
                }
            }

            $this->em->getConnection()->commit();

            if ($i > 0) {
                $this->outputWrite($i.' documents added...');
            }
        }

        if ($isContentPage && empty($sourceNode['content']['content']) && empty($sourceNode['files']) && empty($sourceNode['images']) && empty($sourceNode['documents'])) {
            $this->outputWrite('<question>Not found content, images or files</question>...');
        }

        // Положение страницы в меню
        if ($menuMode && !empty($sourceNode['links']) && isset($sourceNode['links'][0]) && $sourceNode['links'][0] > 0) {
            $this->em->getConnection()->beginTransaction();
            $destinationNode->clearMenutype();

            if ($sourceNode['links'][0] == 1) {
                // Меню-ссылка
                $destinationNode->addMenutype($this->em->getRepository('QCoreBundle:MenuType')->findOneByName('content-link-top'));
            } else {
                // Обычная ссылка
                $destinationNode->addMenutype($this->em->getRepository('QCoreBundle:MenuType')->findOneByName('content-menu-top'));
            }

            $this->em->flush();
            $this->em->getConnection()->commit();
            $this->outputWrite('menu updated...');
        }

        $this->outputWriteLn('<info>Done</info>');
        $this->outputWriteLn('');

        gc_collect_cycles();
        $this->em->getConnection()->beginTransaction();
        $this->em->flush();
        $this->em->getConnection()->commit();
        $this->em->clear();

        // $this->outputWriteLn(sprintf('Memory usage %dKB/ %dKB', round(memory_get_usage(true) / 1024), memory_get_peak_usage(true) / 1024));

        if ($recursiveMode) {
            // Рекурсивный импорт подкатегорий

            foreach($this->oldStructure->getChildrens($sourceNode['id'], true) as $child) {
                $sourceUrlResC = 'http://'.$sourceUrl['host'].$child['path'];
                $destinationUrlResC = $destinationUrlRes.$child['request'];

                $this->import($sourceUrlResC, $destinationUrlResC, true, $updateMode, $recursiveMode);
            }
        }
    }

    /**
     * Создание пустого раздела внутри указанного
     * @param  Node   $parentNode
     * @param  string $name
     * @param  string $title
     * @return Node
     */
    public function createEmptyNode(Node $parentNode, $name, $title, $type = 'content')
    {
        $this->em->getConnection()->beginTransaction();

        switch ($type) {
            default:
            case 'content':
                $controller = $this->em->find('QCoreBundle:NodeController', 2);
                break;

            case 'news':
                $controller = $this->em->find('QCoreBundle:NodeController', 3);
                break;
        }

        $node = new Node();
        $node->setParent($parentNode);
        $node->setTitle($title);
        $node->setName($name);
        $node->setController($controller);
        $node->setActive($parentNode->isActive());

        $this->em->getRepository('QCoreBundle:Node')->update($node);
        $this->em->getConnection()->commit();

        return $node;
    }
}
