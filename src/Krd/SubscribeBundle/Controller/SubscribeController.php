<?php

namespace Krd\SubscribeBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Krd\SubscribeBundle\Entity\ESubscriber;
use Krd\SubscribeBundle\Entity\RSubscriber;
use Krd\SubscribeBundle\Entity\Subscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Подписка на новости
 */
class SubscribeController extends Controller
{
    /**
     * Страница управления рассылками
     *
     * @Route("/ajax/krd/subscribe/form/", name="krd_subscribe_form")
     */
    public function formAction()
    {
        $formBuilder = $this->getSubscriberForm();
        $formBuilder->setAction($this->get('router')->generate('krd_subscribe_form_submit'));
        $form = $formBuilder->getForm();

        $eformBuilder = $this->getEmerSubscriberForm();
        $eformBuilder->setAction($this->get('router')->generate('krd_e_subscribe_form_submit'));
        $eform = $eformBuilder->getForm();

        return $this->render('KrdSubscribeBundle:Subscribe:form.html.twig', array(
            'form' => $form->createView(),
            'eform' => $eform->createView(),
        ));
    }

    /**
     * Подтверждение подписки
     *
     * @Route("/subscribe/confirm/{id}/{key}", name="krd_subscribe_confirm")
     * @Template()
     */
    public function confirmAction(Request $request, $id, $key)
    {
        $subscriber = $this->getDoctrine()->getManager()->find('KrdSubscribeBundle:Subscriber', $id);

        if (!$subscriber || $subscriber->getActive() || $key != $subscriber->getConfirmKey()) {
            throw $this->createNotFoundException();
        }

        $subscriber->setActive(true);
        $this->getDoctrine()->getManager()->flush();

        return array('subscriber' => $subscriber);
    }

    /**
     * Сабмит формы подписки на новости
     *
     * @Route("/ajax/krd/subscribe/form/submit/", name="krd_subscribe_form_submit")
     */
    public function submitAction(Request $request)
    {
        if ($request->query->get('unsubscribe')) {
            // Отписываем от рассылки
            $email = $request->request->get('email');

            if ($email) {
                $subscriber = $this->getDoctrine()->getManager()->getRepository('KrdSubscribeBundle:Subscriber')->findOneBy(array('email' => $email));

                if ($subscriber) {
                    $this->getDoctrine()->getManager()->remove($subscriber);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse(array('success' => true));
                }
            }

            return new JsonResponse(array('success' => false));
        }

        $formBuilder = $this->getSubscriberForm();
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $subscriber = $form->getData();

        if ($subscriber instanceof Subscriber) {
            $subscriber->setDate(new \DateTime());
            $subscriber->setActive(false);
            $this->getDoctrine()->getManager()->persist($subscriber);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('success' => true));
        } else {
            $this->get('logger')->err('Ошибка при подписке на новости. Результат формы не Subscriber');
            return new JsonResponse(array('success' => false));
        }
    }

    /**
     * Фронтэнд форма для подписки
     *
     * @return FormBuilder
     */
    public function getSubscriberForm()
    {
        return $this->get('form.factory')->createNamedBuilder('subscriber', 'subscriber', new Subscriber());
    }

    /**
     * Подтверждение подписки на репортера
     *
     * @Route("/rsubscribe/confirm/{id}/{key}", name="krd_r_subscribe_confirm")
     * @Template()
     */
    public function rconfirmAction(Request $request, $id, $key)
    {
        $subscriber = $this->getDoctrine()->getManager()->find('KrdSubscribeBundle:RSubscriber', $id);

        if (!$subscriber || $subscriber->getActive() || $key != $subscriber->getConfirmKey()) {
            throw $this->createNotFoundException();
        }

        $subscriber->setActive(true);
        $this->getDoctrine()->getManager()->flush();

        return array('subscriber' => $subscriber);
    }

    /**
     * Сабмит формы подписки на репортера
     *
     * @Route("/ajax/krd/rsubscribe/form/submit/", name="krd_r_subscribe_form_submit")
     */
    public function rsubmitAction(Request $request)
    {
        if ($request->query->get('unsubscribe')) {
            // Отписываем от рассылки
            $email = $request->request->get('email');

            if ($email) {
                $subscriber = $this->getDoctrine()->getManager()->getRepository('KrdSubscribeBundle:RSubscriber')->findOneBy(array('email' => $email));

                if ($subscriber) {
                    $this->getDoctrine()->getManager()->remove($subscriber);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse(array('success' => true));
                }
            }

            return new JsonResponse(array('success' => false));
        }

        $formBuilder = $this->getRerporterSubscriberForm();
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $subscriber = $form->getData();

        if ($subscriber instanceof RSubscriber) {
            $subscriber->setDate(new \DateTime());
            $subscriber->setActive(false);
            $this->getDoctrine()->getManager()->persist($subscriber);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('success' => true));
        } else {
            $this->get('logger')->err('Ошибка при подписке на репортера. Результат формы не RSubscriber');
            return new JsonResponse(array('success' => false));
        }
    }

    /**
     * Фронтэнд форма для подписки на репортера
     *
     * @return FormBuilder
     */
    public function getRerporterSubscriberForm()
    {
        return $this->get('form.factory')->createNamedBuilder('r_subscriber', 'r_subscriber', new RSubscriber());
    }

    /**
     * Сабмит формы подписки на экстренные новости
     *
     * @Route("/ajax/krd/esubscribe/form/submit/", name="krd_e_subscribe_form_submit")
     */
    public function esubmitAction(Request $request)
    {
        if ($request->query->get('unsubscribe')) {
            // Отписываем от рассылки
            $email = $request->request->get('email');

            if ($email) {
                $subscriber = $this->getDoctrine()->getManager()->getRepository('KrdSubscribeBundle:ESubscriber')->findOneBy(array('email' => $email));

                if ($subscriber) {
                    $this->getDoctrine()->getManager()->remove($subscriber);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse(array('success' => true));
                }
            }

            return new JsonResponse(array('success' => false));
        }

        $formBuilder = $this->getEmerSubscriberForm();
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $subscriber = $form->getData();

        if ($subscriber instanceof ESubscriber) {
            $subscriber->setDate(new \DateTime());
            $subscriber->setActive(false);
            $this->getDoctrine()->getManager()->persist($subscriber);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('success' => true));
        } else {
            $this->get('logger')->err('Ошибка при подписке на новости. Результат формы не ESubscriber');
            return new JsonResponse(array('success' => false));
        }
    }

    /**
     * Подтверждение подписки на экстренные новости
     *
     * @Route("/esubscribe/confirm/{id}/{key}", name="krd_e_subscribe_confirm")
     * @Template()
     */
    public function econfirmAction(Request $request, $id, $key)
    {
        $subscriber = $this->getDoctrine()->getManager()->find('KrdSubscribeBundle:ESubscriber', $id);

        if (!$subscriber || $subscriber->getActive() || $key != $subscriber->getConfirmKey()) {
            throw $this->createNotFoundException();
        }

        $subscriber->setActive(true);
        $this->getDoctrine()->getManager()->flush();

        return array('subscriber' => $subscriber);
    }

    /**
     * Фронтэнд форма для подписки на экстренные сообщения
     *
     * @return FormBuilder
     */
    public function getEmerSubscriberForm()
    {
        return $this->get('form.factory')->createNamedBuilder('e_subscriber', 'e_subscriber', new ESubscriber());
    }
}
