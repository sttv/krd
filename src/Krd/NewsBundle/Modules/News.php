<?php

namespace Krd\NewsBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;
use Q\FilesBundle\Entity\Video;
use Q\FilesBundle\Entity\VideoFormat;
use Q\FilesBundle\Manager\ImageCacheUrl;
use Krd\NewsBundle\Entity\News as NewsEntity;
use Krd\NewsBundle\Entity\NewsTheme;


/**
 * Модуль новостей
 */
class News extends AbstractModule
{
    /**
     * @var ImageCacheUrl
     */
    protected $imageCacheUrlManager;

    public function setImageCacheUrlManager(ImageCacheUrl $im)
    {
        $this->imageCacheUrlManager = $im;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdNewsBundle:Admin:Module/news.html.twig');
    }

    /**
     * Получение текущей новости на основе запроса
     */
    public function getCurrentNews()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Блок "Так же по теме" относительно текущей новости
     */
    public function renderCurrentSimilar()
    {
        $similarItems = $this->getRepository()->findSimilar($this->getCurrentNews());

        return $this->twig->render('KrdNewsBundle:Module:news/similar.html.twig', array('items' => $similarItems));
    }

    /**
     * Детальная новость
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdNewsBundle:Module:news/detail.html.twig', array('item' => $this->getCurrentNews()));
    }
	
	/**
     * Детальная новость
     */
    public function renderGlavaDetailContent()
    {
        return $this->twig->render('KrdGlavaBundle:Module:events/detail.html.twig', array('item' => $this->getCurrentNews()));
    }

    /**
     * Список новостей для текущей страницы
     * @return array
     */
    protected function getItemsList($count = 10)
    {
        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1');
		if( $this->node->getNode()->getId() == 7 ) {
			$queryBuilder->andWhere('n.parent IN(:parent, 15188)');
		} else {
			$queryBuilder->andWhere('n.parent = :parent');
		}
        $queryBuilder->orderBy('n.date', 'DESC');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        if ($date = $this->request->get('date')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') = :date');
            $queryBuilder->setParameter('date', $date->format('Y-m-d'));
        }
		
		if ($date = $this->request->get('date_begin')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') >= :date1');
            $queryBuilder->setParameter('date1', $date->format('Y-m-d'));
        }
		
		if ($date = $this->request->get('date_end')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') <= :date2');
            $queryBuilder->setParameter('date2', $date->format('Y-m-d'));
        }
		
		if( count($this->request->get('themes', array())) > 0 ) {
			$queryBuilder->leftJoin('n.themes', 't')
						->andWhere('t.id IN(:themes)')
						->setParameter('themes', $this->request->get('themes'));
		}

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true, $count, $this->node->getNode()->getUrl(true));
    }

    /**
     * Список новостей
     */
    public function renderContent()
    {
        $date = $this->request->get('date');
        $date = preg_replace('/[^0-9\-]/', '', $date);
        $date = \DateTime::createFromFormat('d-m-Y', $date);
		
		$date_begin = $this->request->get('date_begin');
        $date_begin = preg_replace('/[^0-9\-]/', '', $date_begin);
        $date_begin = \DateTime::createFromFormat('d-m-Y', $date_begin);


		$date_end = $this->request->get('date_end');
        $date_end = preg_replace('/[^0-9\-]/', '', $date_end);
        $date_end = \DateTime::createFromFormat('d-m-Y', $date_end);

        $result = $this->getItemsList();
		
        return $this->twig->render('KrdNewsBundle:Module:news/list.html.twig', array(
			'list' => $result, 
			'nowDate' => $date,
			'themes' => $this->request->get('themes', array()),
			'date_begin' => $date_begin,
			'date_end' => $date_end
		));
    }
	
    /**
     * Список новостей на странице главы города
     */
    public function renderGlavaContent()
    {
        $date = $this->request->get('date');
        $date = preg_replace('/[^0-9\-]/', '', $date);
        $date = \DateTime::createFromFormat('d-m-Y', $date);
		
		$date_begin = $this->request->get('date_begin');
        $date_begin = preg_replace('/[^0-9\-]/', '', $date_begin);
        $date_begin = \DateTime::createFromFormat('d-m-Y', $date_begin);


		$date_end = $this->request->get('date_end');
        $date_end = preg_replace('/[^0-9\-]/', '', $date_end);
        $date_end = \DateTime::createFromFormat('d-m-Y', $date_end);


        $result = $this->getItemsList();
		
        return $this->twig->render('KrdGlavaBundle:Module:events/list.html.twig', array( 
			'list' => $result, 
			'nowDate' => $date, 
			'themes' => $this->request->get('themes', array()),
			'date_begin' => $date_begin,
			'date_end' => $date_end
		));
    }
	
	/**
	 * Кол-во комментариев для новости
	 */
	public function getComments( $news ) {
		$url = $this->node->getNode()->getUrl(true) . $news->getName() . '.html';
		return $this->em->getRepository('KrdCommentsBundle:Comment')->getCount($url);
	}

    /**
     * Список новостей в блочном оформлении
     */
    public function renderContentBlock($count = 10)
    {
        $result = $this->getItemsList($count);

        return $this->twig->render('KrdNewsBundle:Module:news/list-block.html.twig', array('list' => $result));
    }

    /**
     * Список новостей на главной странице департамента
     */
    public function renderDepartamentIndex()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 3)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $newsNode = $temp[0];
        } else {
            return '';
        }

        $queryBuilder->setParameter('parent', $newsNode->getId());

        $result = $queryBuilder->getQuery()
            ->setMaxResults(3)
            ->useResultCache(true, 15)
            ->getResult();

        return $this->twig->render('KrdNewsBundle:Module:news/departament-block.html.twig', array('list' => $result, 'newsNode' => $newsNode));
    }

    /**
     * Список новостей для главной страницы
     */
    public function renderIndexContent()
    {
        $categories = array();

        // 4 - для английской версии
        // 116 - для gorod.krd.ru
        $nodes = $this->em->createQuery("SELECT n FROM QCoreBundle:Node n WHERE n.active = 1 AND n.controller = 3 AND n.id IN (116, 4, 7, 10358, 10490, 10489, 10359)")
        ->useResultCache(true, 15)
        ->getResult();

        foreach($nodes as $node) {
            // Необходимо, чтобы небыло переименования нодов
            $this->em->detach($node);

            switch ($node->getId()) {
                case 10358:
                    $node->setTitle('Западный округ');
                    break;

                case 10490:
                    $node->setTitle('Центральный округ');
                    break;

                case 10489:
                    $node->setTitle('Прикубанский округ');
                    break;

                case 10359:
                    $node->setTitle('Карасунский округ');
                    break;
            }
			
			if( $node->getId() == 7 ) {
				$sql = "SELECT n
                              FROM __CLASS__ n
                              WHERE n.active = 1 AND (n.parent = :parent OR (n.parent=15188 AND n.onhome=1))
                              ORDER BY n.date DESC";
			} else {
				$sql = "SELECT n
                              FROM __CLASS__ n
                              WHERE n.active = 1 AND n.parent = :parent
                              ORDER BY n.date DESC";
			}

            $item = array();
            $item['bold'] = false;
            $data['adv_link'] = false;
            $item['node'] = $node;
            $item['news'] = $this->getRepository()
                ->createQuery($sql)
                ->setparameter('parent', $node->getId())
                ->setMaxResults(6)
                ->useResultCache(true, 15)
                ->getResult();

            $categories[] = $item;
        }

        if ($this->request->getHost() == 'krd.ru') {
            $data = array();
            $data['bold'] = true;
            $data['adv_link'] = array('title' => 'Сайт краевого штаба', 'url' => '//gorod.krd.ru');
            $data['node'] = new Node();
            $data['node']->setTitle('Краснодар Обновление');
            $data['node']->setUrl('http://gorod.krd.ru/novosti/');

            $statement = $this->em->getConnection()->prepare("SELECT * FROM `gorkrdru13__news` WHERE `parent_id` = 116 AND `active` = 1 ORDER BY `date` DESC LIMIT 6");
            $statement->execute();

            while ($item = $statement->fetch()) {
                $data['news'][] = $this->parseNewsEntity($item);
            }

            $categories[] = $data;
        }

        return $this->twig->render('KrdNewsBundle:Module:news/index.html.twig', array('categories' => $categories));
    }

    /**
     * Распарсивание массива с новостью для создания сущности
     * @param  array  $item
     * @return NewsEntity
     */
    protected function parseNewsEntity(array $item)
    {
        $n = new NewsEntity();
        $n->setTitle($item['title']);
        $n->setUrl('http://gorod.krd.ru/novosti/'.$item['name'].'.html');
        $n->setDate(new \DateTime($item['date']));

        // Теги
        $statement = $this->em->getConnection()
            ->prepare("SELECT NT.*
                FROM `gorkrdru13__news_theme` as NT
                INNER JOIN `gorkrdru13__news_newstheme_relation` as R
                ON R.`newstheme_id` = NT.`id`
                WHERE R.`news_id` = :id");
        $statement->execute(array('id' => $item['id']));

        while ($themeRaw = $statement->fetch()) {
            $theme = new NewsTheme();
            $theme->setTitle($themeRaw['title']);
            $n->addThemes($theme);
        }

        // Изображения
        $statement = $this->em->getConnection()
            ->prepare("SELECT I.*
                FROM `gorkrdru13__image` as I
                INNER JOIN `gorkrdru13__news_image_relation` as R
                ON R.`image_id` = I.`id`
                WHERE R.`news_id` = :id");
        $statement->execute(array('id' => $item['id']));

        while ($imageRaw = $statement->fetch()) {
            $n->addImages($this->parseImageEntity($imageRaw));
        }

        // Видео
        $statement = $this->em->getConnection()
            ->prepare("SELECT V.*
                FROM `gorkrdru13__video` as V
                INNER JOIN `gorkrdru13__news_video_relation` as R
                ON R.`video_id` = V.`id`
                WHERE R.`news_id` = :id");
        $statement->execute(array('id' => $item['id']));

        $stImage = $this->em->getConnection()->prepare("SELECT * FROM `gorkrdru13__image` WHERE `id` = :id");
        $stFormat = $this->em->getConnection()->prepare("SELECT * FROM `gorkrdru13__video_format` WHERE `parent_id` = :id");

        while ($videoRaw = $statement->fetch()) {
            if ($videoRaw['converted'] != 1) {
                continue;
            }

            $video = new Video();
            $video->setTitle($videoRaw['title']);
            $video->setFileArray(unserialize($videoRaw['file']));
            $video->setConverted(true);

            $stImage->execute(array('id' => $videoRaw['image_id']));

            if ($imageRaw = $stImage->fetch()) {
                $video->setImage($this->parseImageEntity($imageRaw));
            }

            $stFormat->execute(array('id' => $videoRaw['id']));

            while ($formatRaw = $stFormat->fetch()) {
                $format = new VideoFormat();
                $format->setFormat($formatRaw['format']);
                $file = unserialize($formatRaw['file']);
                $file['path'] = '//gorod.krd.ru'.$file['path'];
                $format->setFileArray($file);
                $video->addFormat($format);
            }

            $n->addVideo($video);
        }
        return $n;
    }

    /**
     * распарсивание изображения в сущность
     * @param  array  $imageRaw
     * @return Image
     */
    protected function parseImageEntity(array $imageRaw)
    {
        $image = new Image();
        $image->setId($imageRaw['id']);
        $image->setTitle($imageRaw['title']);
        $image->setFileArray(unserialize($imageRaw['file']));

        $temp = $this->imageCacheUrlManager->getBaseUrl();
        $this->imageCacheUrlManager->setBaseUrl('//gorod.krd.ru');
        $image->setSize($this->imageCacheUrlManager->getAllImageCacheUrl($image));
        $this->imageCacheUrlManager->setBaseUrl($temp);

        return $image;
    }

    /**
     * Календарь новостей в сайдбаре
     */
    public function renderAsideCalendar()
    {
        $dates = $this->getRepository()->getDatesList($this->node->getNode()->getId());

        if ($date = $this->request->get('date')) {
            $date = preg_replace('/[^0-9\-]/', '', $date);
            $date = \DateTime::createFromFormat('d-m-Y', $date);
        }

        return $this->twig->render('KrdNewsBundle:Module:news/aside.calendar.html.twig', array(
                'availDates' => $dates,
                'nowDate' => $date,
            ));
    }

    /**
     * Виджет экстренных сообщений
     */
    public function renderEmergencyWidget()
    {
        $node = $this->em->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.id = 17255')
            ->setMaxResults(1)
            ->getQuery()
            ->useResultCache(true, 15)
            ->getResult();

        if (!$node) {
            return '';
        }

        $node = $node[0];

        $list = $this->getRepository()->createQueryBuilder('e')
            ->andWhere('e.active = 1')
            ->andWhere('e.parent = :parent')
            ->setParameter('parent', $node->getId())
            ->orderBy('e.date', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->useResultCache(true, 15)
            ->getResult();

        if (!$list) {
            return '';
        }

        return $this->twig->render('KrdNewsBundle:Module:info/widget.html.twig', array('items' => $list, 'node' => $node));
    }
}
