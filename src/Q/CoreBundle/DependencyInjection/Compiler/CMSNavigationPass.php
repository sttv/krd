<?php

namespace Q\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Добавление элементов в меню навигации CMS
 * Сервисы должны быть с тегом qcore.cms.navigation.item
 */
class CMSNavigationPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('qcore.cms.navigation')) {
            return;
        }

        $definition = $container->getDefinition('qcore.cms.navigation');

        $taggedServices = $container->findTaggedServiceIds('qcore.cms.navigation.item');

        foreach ($taggedServices as &$attributes) {
            if (!isset($attributes[0]) || !isset($attributes[0]['order']) || !is_numeric($attributes[0]['order'])) {
                $attributes[0]['order'] = 1000;
            }
        }

        uasort($taggedServices, array($this, 'cmpItems'));

        foreach ($taggedServices as $id => $attributes) {
            $definition->addMethodCall('addItem', array(new Reference($id)));
        }
    }

    /**
     * Метод для сортировки сервисов
     */
    protected function cmpItems($a, $b)
    {
        return $a[0]['order'] - $b[0]['order'];
    }
}
