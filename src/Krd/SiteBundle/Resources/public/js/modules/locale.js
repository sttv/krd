/**
 * Простенький моделья для реализации переводов
 */
angular.module('locale', [])
    /**
     * Словарь
     */
    .factory('localeDictionary', [function() {
        return {
            en: {
                "Январь": "January",
                "Февраль": "February",
                "Март": "March",
                "Апрель": "April",
                "Май": "May",
                "Июнь": "June",
                "Июль": "July",
                "Август": "August",
                "Сентябрь": "September",
                "Октябрь": "October",
                "Ноябрь": "November",
                "Декабрь": "December",
                "Пн": "Mo",
                "Вт": "W",
                "Ср": "Wed",
                "Чт": "Th",
                "Пт": "Fr",
                "Сб": "Sat",
                "Вс": "Su"
            }
        }
    }])

    /**
     * Сервис для переводов
     */
    .service('locale', ['localeDictionary', function(localeDictionary) {
        var locale = $('html').attr('lang');

        this.getLocale = function() {
            return locale;
        };

        var trans = this.trans = function(data) {
            var result = angular.copy(data);

            if ($.isArray(result)) {
                for (var i = 0; i < result.length; i++) {
                    result[i] = trans(result[i]);
                }

                return result;
            }

            if ($.isFunction(result)) {
                return trans(result());
            }

            if ($.isPlainObject(result)) {
                $.each(result, function(i, val) {
                    result[i] = trans(val);
                })

                return result;
            }

            if (!!localeDictionary[locale] && !!localeDictionary[locale][result]) {
                return localeDictionary[locale][result];
            }

            return result;
        };
    }])

    .filter('trans', ['locale', function(locale) {
        return function(value) {
            return locale.trans(value);
        };
    }])
;
