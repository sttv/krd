<?php

namespace Krd\UserBundle\Modules;

use Symfony\Component\Security\Core\SecurityContext;

use Q\CoreBundle\Admin\AbstractModule;
use Q\UserBundle\Entity\User;


/**
 * Модуль главной страницы личного кабинета
 */
class IndexModule extends AbstractModule
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var User
     */
    protected $user;

    public function renderAdminContent()
    {
        return $this->twig->render('KrdUserBundle:Admin:Module/index.html.twig');
    }

    /**
     * @param SecurityContext $securityContext
     */
    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    /**
     * Список виджетов пользователя
     */
    public function renderContent()
    {
        if ($this->user instanceof User) {
            $ids = $this->user->getWidgets();
            $ids = array_map('intval', $ids);

            if (count($ids) > 0) {
                $all = $this->getRepository()->createQueryBuilder('w')
                    ->select('w, FIELD(w.id, :ids) as HIDDEN ord')
                    ->andWhere('w.active = 1')
                    ->addOrderBy('ord')
                    ->addOrderBy('w.sort')
                    ->setParameter('ids', $ids)
                    ->getQuery()
                        ->getresult();
            } else {
                $all = $this->getRepository()->findBy(array('active' => 1), array('sort' => 'ASC'));
            }

            $widgetsShow = array();
            $widgetsAvail = array();

            foreach ($all as $widget) {
                if (in_array($widget->getId(), $ids)) {
                    $widgetsShow[] = $widget;
                } else {
                    $widgetsAvail[] = $widget;
                }
            }

            return $this->twig->render('KrdUserBundle:Module:index/index.html.twig', array(
                'widgets_show' => $widgetsShow,
                'widgets_avail' => $widgetsAvail,
            ));
        } else {
            throw new \Exception('User not logged');
        }
    }
}
