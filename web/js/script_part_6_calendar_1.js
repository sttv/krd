angular.module('calendar', [])
    /**
     * Одиночный календарь
     *
     * links - список активных дат
     * url - базовый URl
     * now - подсветка текущей даты
     * select-month - выбранный по умолчанию месяц
     * comments - дни с комментариями
     * disabled-month
     * disabled-year
     * hidden-year
     * hidden-month
     */
    .directive('calendarSingle', ['$parse', function($parse) {
        var monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        var daysList = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

        return {
            restrict: 'C',
            template: '<div class="selectors" ng-class="selectorsClass()">'
                        + '<div calendar-select="year" from="years" disabled="disabledYear" hidden="hiddenYear"></div>'
                        + '<div calendar-select="month" from="months" disabled="disabledMonth" hidden="hiddenMonth"></div>'
                    + '</div>'
                    + '<div class="calendar">'
                        + '<table class="no-design">'
                            + '<tr ng-repeat="week in weeks">'
                                + '<td ng-repeat="day in week.days" ng-class="{inactive:day.inactive, lastinactive:day.lastinactive, now:day.isnow, link:day.isLink, comment:day.isComment}">'
                                    + '<a ng-href="{{ day.url }}" ui-if="day.isLink">{{ day.day }}</a>'
                                    + '<span class="calendar-cell" ui-if="day.isComment">{{ day.day }}'
                                        + '<span class="comment">{{ day.fullDate }} <b>{{ day.comment }}</b></span>'
                                    + '</span>'
                                    + '<span ui-if="!day.isLink && !day.isComment">{{ day.day }}</span>'
                                + '</td>'
                            + '</tr>'
                        + '</table>'
                    + '</div>',
            scope: true,

            link: function($scope, $element, attrs) {
                $scope.weeks = [];

                if (!!attrs.selectYear) {
                    $scope.year = parseInt(attrs.selectYear, 10);
                } else {
                    $scope.year = Date.today().getFullYear();
                }

                if (!!attrs.selectMonth) {
                    $scope.month = parseInt(attrs.selectMonth, 10);
                } else {
                    $scope.month = Date.today().getMonth();
                }

                $scope.links = $parse(attrs.links)($scope);
                if (!$scope.links) {
                    $scope.links = [];
                }

                $scope.comments = $parse(attrs.comments)($scope);
                if (!$scope.comments) {
                    $scope.comments = [];
                }

                $scope.url = attrs.url;
                $scope.now = attrs.now;

                if (!attrs.selectYear && !!$scope.now) {
                    $scope.year = Date.parse($scope.now).getFullYear();
                }

                if (!attrs.selectMonth && !!$scope.now) {
                    $scope.month = Date.parse($scope.now).getMonth();
                }

                $scope.years = [];
                for(var i = 0; i < 3; i++) {
                    $scope.years.push({value:$scope.year - i, title:$scope.year - i});
                }

                $scope.months = [];
                for(var i = 0; i < 12; i++) {
                    $scope.months.push({value:i, title:monthNames[i]});
                }

                $scope.disabledYear = (attrs.disabledYear !== undefined);
                $scope.disabledMonth = (attrs.disabledMonth !== undefined);

                $scope.hiddenYear = (attrs.hiddenYear !== undefined);
                $scope.hiddenMonth = (attrs.hiddenMonth !== undefined);

                $scope.$watch('year', $scope.update);
                $scope.$watch('month', $scope.update);
            },

            controller: function($scope) {
                $scope.selectorsClass = function() {
                    if ($scope.hiddenYear || $scope.hiddenMonth) {
                        return 'text-align-left';
                    }
                };

                /**
                 * Обновление календаря
                 */
                $scope.update = function() {
                    var days = [];

                    var daysCount = Date.getDaysInMonth($scope.year, $scope.month);

                    for(var i = 1; i <= daysCount; i++) {
                        var date = Date.today().set({year:$scope.year, month:$scope.month, day:i});

                        days.push({
                            fullDate: date.toString('dd.MM.yyyy'),
                            day: i,
                            isLink: $.inArray(date.toString('dd/MM/yyyy'), $scope.links) !== -1,
                            isComment: !!$scope.comments[date.toString('dd/MM/yyyy')],
                            comment: !!$scope.comments[date.toString('dd/MM/yyyy')] ? $scope.comments[date.toString('dd/MM/yyyy')] : null,
                            isnow: date.toString('dd/MM/yyyy') == $scope.now,
                            url: $scope.url + '?date='+ date.toString('dd-MM-yyyy'),
                            weekDayNumber: $.inArray(date.toString('ddd'), daysList)
                        });
                    }

                    var prevYear = $scope.year;
                    var prevMonth = $scope.month - 1;

                    if (prevMonth < 0) {
                        prevMonth = 11;
                        prevYear--;
                    }

                    var ii = 0;
                    for(var i = days[0].weekDayNumber; i > 0 ; i--, ii++) {
                        var day = Date.getDaysInMonth(prevYear, prevMonth) - ii;
                        days.unshift({
                            fullDate: date.toString('dd.MM.yyyy'),
                            inactive: true,
                            day: day,
                            weekDayNumber: $.inArray(Date.today().set({year:prevYear, month:prevMonth, day:day}).toString('ddd'), daysList)
                        });
                    }

                    if (ii > 0) {
                        days[ii-1].lastinactive = true;
                    }

                    var nextYear = $scope.year;
                    var nextMonth = $scope.month + 1;

                    if (nextMonth > 11) {
                        nextMonth = 0;
                        nextYear++;
                    }

                    var ii = 1;
                    for(var i = days[days.length - 1].weekDayNumber; i < 6 ; i++, ii++) {
                        var day = ii;

                        days.push({
                            fullDate: date.toString('dd.MM.yyyy'),
                            inactive: true,
                            day: day,
                            weekDayNumber: $.inArray(Date.today().set({year:nextYear, month:nextMonth, day:day}).toString('ddd'), daysList)
                        });
                    }

                    $scope.weeks = [];

                    for(var i = 0; i < days.length; i+=7) {
                        var week = {days:[]};

                        for(var j = 0; j < 7; j++) {
                            week.days.push(days[i+j]);
                        }

                        $scope.weeks.push(week);
                    }
                };
            }
        }
    }])


    /**
     * Селектор для календаря
     */
    .directive('calendarSelect', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            template: '<div class="select" ng-class="className()">'
                        + '<div class="value">{{ selected }}</div>'
                        + '<div class="list">'
                            + '<div class="item" ng-repeat="item in list" ng-click="clkValue(item)">'
                                + '{{ item.title }}'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            replace: true,
            scope:{
                value: '=calendarSelect',
                list: '=from',
                disabled: '=',
                hidden: '='
            },

            link: function($scope, $element, attrs) {
                $scope.name = attrs.calendarSelect;
                $scope.selected = '---';
                $scope.$list = $element.find('.list');

                $scope.$watch('list', function(list) {
                    for(var i = 0; i < list.length; i++) {
                        if (list[i].value == $scope.value) {
                            $scope.selected = list[i].title;
                        }
                    }
                });
            },

            controller: function($scope) {
                $scope.clkValue = function(item) {
                    $scope.selected = item.title;
                    $scope.value = item.value;
                    $scope.$list.hide();
                    $timeout(function() {
                        $scope.$list.removeAttr('style');
                    }, 100);
                };

                $scope.className = function() {
                    var className = $scope.name;

                    if ($scope.disabled) {
                        className += ' disabled';
                    }

                    if ($scope.hidden) {
                        className += ' hidden';
                    }

                    return className;
                };
            }
        }
    }])
;
