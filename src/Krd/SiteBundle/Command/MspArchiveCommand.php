<?php

namespace Krd\SiteBundle\Command;

use DateTime;
use Krd\SiteBundle\Entity\MSP;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Архивирование старых записей MSP
 */
class MspArchiveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:msp:archive')
            ->setDescription('Archive MSP entities');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $query = $em->createQuery('SELECT e FROM KrdSiteBundle:MSP e WHERE e.dateInc <= :date AND e.archive = 0');
        $date = new DateTime();
        $date = $date->modify('-3 years');
        $query->setParameter('date', $date);
        $archive = array();

        $em->getConnection()->beginTransaction();

        foreach ($query->iterate() as $msp) {
            /** @var MSP $msp */
            $msp = $msp[0];

            $archive[] = array(
                'recNum' => $msp->getRecNum(),
                'dateInc' => $msp->getDateInc(),
                'llcTitle' => $msp->getLlcTitle(),
            );

            $msp->setArchive(true);
        }

        $em->flush();
        $em->getConnection()->commit();

        if (count($archive) > 0) {
            $mailer = $this->getContainer()->get('qcore.mailer');
            $message = $mailer->newMessage('Реест МСП. Записи перенесены в архив');

            $message->setTo(array('2672230@mail.ru'));

            try {
                $mailer->sendTemplate($message, 'KrdSiteBundle:Email:msp-archive.html.twig', array('archive' => $archive));
            } catch (\Exception $e) {
                $this->getContainer()->get('logger')->crit('Ошибка отправки уведомления МСП', array($e->getMessage()));
            }
        }
    }
}
