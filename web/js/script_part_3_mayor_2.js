/**
 * Руководители города
 */
angular.module('mayor', ['dialog'])
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    }])

    /**
     * Руководитель города
     */
    .directive('mayorItem', ['$dialog', '$location', function($dialog, $location) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                $scope.location = $location;
                var url = '/' + $element.find('.mayor-link').attr('href').split('/').pop();
                var dialogUrl = attrs.mayorItem;
                $scope.$watch('location.path()', function(path) {
                    if (url == path) {
                        $dialog.create(dialogUrl,
                            {
                                onClose: function() {
                                    $scope.$apply(function() {
                                        $location.path('/');
                                    });
                                }
                            }
                        );
                    }
                });
            }
        }
    }])
;
