/**
 * Объекты города
 */
angular.module('city-object', ['dialog'])
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    }])

    /**
     * Объект города
     */
    .directive('cityObjectItem', ['$dialog', '$location', function($dialog, $location) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                $scope.location = $location;
                var url = '/' + $element.find('.city-object-link').attr('href').split('/').pop();
                var dialogUrl = attrs.cityObjectItem;
                $scope.$watch('location.path()', function(path) {
                    if (url == path) {
                        $dialog.create(dialogUrl,
                            {
                                onClose: function() {
                                    $scope.$apply(function() {
                                        $location.path('/');
                                    });
                                }
                            }
                        );
                    }
                });
            }
        }
    }])
;
