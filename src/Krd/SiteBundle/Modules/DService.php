<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль - предоставляемые услуги (Департамент)
 */
class DService extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/dservice.html.twig');
    }

    /**
     * Текущая услуга
     * @return DService
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Список услуг
     */
    public function renderContent()
    {
        $list = $this->getRepository()
            ->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent AND e.active = 1")
            ->setParameter('parent', $this->node->getNode()->getId())
            ->useResultCache(true, 15)
            ->getResult();

        return $this->twig->render('KrdSiteBundle:Module:dservice/list.html.twig', array('list' => $list));
    }

    /**
     * Подробная страница
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdSiteBundle:Module:dservice/detail.html.twig', array('item' => $this->getCurrent()));
    }

    /**
     * Список услуг на главной департамента
     */
    public function renderDepartamentIndex()
    {
        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 22)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $node = $temp[0];
        } else {
            return '';
        }

        $list = $this->getRepository()
            ->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent AND e.active = 1 ORDER BY e.sort")
            ->setParameter('parent', $node->getId())
            ->setMaxResults(10)
            ->useResultCache(true, 15)
            ->getResult();

        return $this->twig->render('KrdSiteBundle:Module:dservice/departament-list.html.twig', array('list' => $list, 'node' => $node));
    }
}
