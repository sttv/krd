<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Импорт подкатегорий
 */
class ImportGlavaSpeechCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:glava:speech')
            ->setDescription('Import glava speech to krd.ru')
            ->setHelp(<<<EOT
                      The <info>krd:import:glava:speech</info> imports glava speech to krd.ru.
EOT
                    );
        ;
    }
	
	protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$this->getContainer()->get('krd_import.old.glava_speech')->import($output);
	}
}