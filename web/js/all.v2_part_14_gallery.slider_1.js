/**
 * Слайдер подробного вида галереи
 */
angular.module('gallery.slider', ['horizontal.scroll'])
    .directive('gallerySlider', ['$compile', '$timeout', '$http', function($compile, $timeout, $http) {
        return {
            restrict: 'A',
            scrope: true,

            template: function($element) {
                var tpl = '<div class="big-preview no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                        + '<a class="icon fancybox-disabled" ng-click="openFancybox($event)" ng-href="{{ fancyboxUrl }}" ng-class="{busy:loading}" title="{{ previewTitle }}" rel="{{ uid }}-fancybox">'
                            + '<img ng-src="{{ previewUrl }}" />'
                            + '<div class="title" ng-show="!!previewTitle">{{ previewTitle }}</div>'
                            + '<div class="loading"></div>'
                        + '</a>'
                        + '<a ng-repeat="item in items" class="fancybox hidden" ng-href="{{ item.fancybox }}" title="{{ item.title }}" rel="{{ uid }}-fancybox"></a>'
                    + '</div>'

                    + '<div class="preview-list no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="items-list-wrap" horizontal-scroll="gs" horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active">'
                            + '<div class="items-list clearfix">'
                                + $element.html()
                            + '</div>'
                        + '</div>'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                    + '</div>';

                return tpl;
            },

            link: function($scope, $element) {
                var $items = $element.find('.preview-list .items-list > .item');

                $scope.uid = 'gs-' + (Math.random()*100000).toString().replace(/[^0-9]/, '');
                $scope.current = 0;
                $scope.fancyboxUrl = '';
                $scope.previewUrl = '';
                $scope.previewTitle = '';
                $scope.loading = false;
                $scope.items = [];
                $scope.$items = $items;

                $scope.$items.each(function(i) {
                    $scope.items.push({
                        fancybox: $(this).data('fancybox'),
                        title: $(this).attr('title')
                    });

                    $(this).on('click', function() {
                        $scope.setCurrent(i);
                        $scope.$apply();
                    });
                });

                $scope.$watch('current', function(current) {
                    if (current > $scope.$items.length - 1) {
                        $scope.current = $scope.$items.length - 1;
                        return;
                    }

                    if (current < 0) {
                        $scope.current = 0;
                        return;
                    }

                    $scope.$broadcast('setPosition', 'gs', current);
                    $scope.$items.removeClass('active');

                    var $activeItem = $scope.$items.eq(current);

                    $activeItem.addClass('active');

                    $scope.fancyboxUrl = $activeItem.data('fancybox');
                    $scope.previewUrl = $activeItem.data('preview');
                    $scope.previewTitle = $activeItem.attr('title');
                });

                $scope.$watch('previewUrl', function(previewUrl) {
                    if (!previewUrl) {
                        return;
                    }

                    $scope.loading = true;

                    if ($.browser.msie && $.browser.version <= 7) {
                        $timeout(function() {
                            $scope.loading = false;
                        }, 500);
                    } else {
                        $.get(previewUrl, function() {
                            $timeout(function() {
                                $scope.loading = false;
                            }, 200);
                        });
                    }
                });

                if ($.browser.msie && $.browser.version <= 7) {
                    $items.each(function(i) {
                        $(this).on('click', function() {
                            $scope.setCurrent(i);
                            $scope.$apply();
                        });
                    });
                }
            },

            controller: function($scope, $element) {
                $scope.next = function() {
                    $scope.current++;
                };

                $scope.prev = function() {
                    $scope.current--;
                };

                $scope.setCurrent = function(current) {
                    $scope.current = current;
                };

                $scope.openFancybox = function($event) {
                    $event.preventDefault();

                    $element.find('.fancybox.hidden[href="'+$scope.fancyboxUrl+'"]').click();
                };
            }
        }
    }])
;
