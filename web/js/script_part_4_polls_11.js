/**
 * Модуль опросов
 */
angular.module('polls', [])
    /**
     * Виджет отпросов
     */
    .directive('widgetPolls', ['$http', '$compile', function($http, $compile) {
        return {
            restrict: 'A',
            scope: {
                pollId: '=widgetPolls',
                url: '@'
            },

            compile: function($element) {
                $element.find('.item').attr('ng-class', '{loading:loading}');

                return function($scope, $element) {
                    $scope.loading = false;

                    var resultsTpl = '<div class="item" ng-repeat="item in results">{{ item.title }} - <b>{{ item.counter }}</b></div>';

                    $element.find('.item input').on('change', function() {
                        if ($scope.loading) {
                            return;
                        }

                        var id = $(this).data('id');

                        $scope.$apply(function() {
                            $scope.loading = true;

                            $http({
                                method: 'POST',
                                url: $scope.url,
                                data: {poll: $scope.pollId, answer: id}
                            }).success(function(response) {
                                $scope.loading = false;
                                $scope.results = response.poll.answers_by_counter;

                                var $answers = $element.find('.list.answers');

                                $answers.html(resultsTpl).removeClass('answers').addClass('results');
                                $compile($answers)($scope);

                                $element.next('.btn-group').remove();
                            }).error(function() {
                                $scope.loading = false;

                                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз');
                            });
                        });
                    });
                }
            }
        }
    }])
;
