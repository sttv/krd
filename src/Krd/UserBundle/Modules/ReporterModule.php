<?php

namespace Krd\UserBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Krd\SiteBundle\Modules\Reporter;


/**
 * Модуль городского репортера
 */
class ReporterModule extends AbstractModule
{
    /**
     * @var Reporter
     */
    protected $parentModule;

    public function setParentModule(Reporter $m)
    {
        $this->parentModule = $m;
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    public function renderContent()
    {
        // Nothing
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget()
    {
        $publications = $this->parentModule->getLastPublications();

        return $this->twig->render('KrdUserBundle:Module:reporter/widget.html.twig', array('item' => $publications[0]));
    }
}
