angular.module('glava.common', ['calendar'])
.directive('moreBtnEx', ['$compile', '$http', '$rootScope', function($compile, $http, $rootScope) {
    return {
        restrict: 'A',
        scope: {
            url: '@'
        },

        compile: function($element, attrs) {
            return function($scope, $element, attrs) {
                $scope.currentPage = 1;
                $scope.loading = false;
                $scope.originalHtml = $element.html();

                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $scope.nextPage();
                    });
                });

                $scope.$watch('loading', function(loading) {
                    if (loading) {
                        $element.html('Загрузка...');
                    } else {
                        $element.html($scope.originalHtml);
                    }
                });
            };
        },

        controller: function($scope, $element, $attrs) {
            $scope.nextPage = function() {
                if ($scope.loading) {
                    return;
                }

                $scope.loading = true;
                $scope.currentPage++;

                $http({
                    method: 'GET',
                    url: $scope.url,
                    params: {page: $scope.currentPage}
                }).success(function(response) {
                    if (response.page_count <= $scope.currentPage + 2) {
                        $element.hide();
                    }
					
					var $newScope = $rootScope.$new(true);
				 	var $listBody = $('#'+$attrs.moreBtnBody);
		            var $listTemplate = $($('#glava-list-item').html());
					
		            $listBody.append($listTemplate);
					$listTemplate.attr('id', 'row-' + (Math.random().toString().replace('.', '')));
					
					$compile($listTemplate)($newScope);
					
					$newScope.items = [];
                    for(var i = 0; i < response.items.length; i++) {
                        $newScope.items.push(response.items[i]);
                    }
					
                    $scope.loading = false;
                }).error(function() {
                    alert('Произошла ошибка, повторите попытку позже');
                    $scope.loading = false;
                });
            };
        }
    }
}]).directive('glavaSchedule', function($http, $compile) {
	return {
		restrict : 'A',
		scope : true,
		
		link : function(scope, $element, attrs) {
			$http({
				url : attrs.url,
				method : 'GET'
			}).success(function(data) {
				$element.append(data);
				$compile(data)(scope);
			}).error(function() {
				
			})
		}
	};
});
