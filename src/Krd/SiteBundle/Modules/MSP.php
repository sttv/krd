<?php

namespace Krd\SiteBundle\Modules;

use DateTime;
use Krd\SiteBundle\Entity\MSP as MSPent;
use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Модуль - реестр МСП
 */
class MSP extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/msp.html.twig');
    }

    /**
     * Список для текущей страницы
     * @return array
     */
    public function getItemsList($parent = null)
    {
        $query = $this->request->get('query', '');
        $searchPlace = $this->request->get('searchplace', '');

        $queryBuilder = $this->getRepository()
            ->createQueryBuilder('n')
            ->andWhere('n.active = 1 AND n.archive = 0 AND n.parent = :parent')
            ->addOrderBy('n.category', 'ASC')
            ->addOrderBy('n.recNum1', 'DESC')
            ->addOrderBy('n.recNum2', 'DESC');

        if (!empty($query)) {
            if (preg_match('#^\d{2}\.\d{2}\.\d{4}$#', $query)) {
                $query = DateTime::createFromFormat('d.m.Y', $query);
                $query = $query->format('Y-m-d');
                $dateSearch = true;
            } else {
                $dateSearch = false;
            }

            $queryBuilder->setParameter('query', '%' . $query . '%');

            switch ($searchPlace) {
                default:
                    if ($dateSearch) {
                        $queryBuilder->andWhere('n.recNum LIKE :query OR n.dateInc LIKE :query OR n.orgTitle LIKE :query OR n.dateApprove LIKE :query OR n.llcTitle LIKE :query OR n.inn LIKE :query OR n.form LIKE :query OR n.view LIKE :query OR n.amount LIKE :query OR n.yearOf LIKE :query OR n.information LIKE :query');
                    } else {
                        $queryBuilder->andWhere('n.recNum LIKE :query OR n.orgTitle LIKE :query OR n.llcTitle LIKE :query OR n.inn LIKE :query OR n.form LIKE :query OR n.view LIKE :query OR n.amount LIKE :query OR n.yearOf LIKE :query OR n.information LIKE :query');
                    }

                    break;

                case 'orgTitle':
                    $queryBuilder->andWhere('n.orgTitle LIKE :query OR n.llcTitle LIKE :query');
                    break;

                case 'inn':
                    $queryBuilder->andWhere('n.inn LIKE :query');
                    break;

                case 'yearOf':
                    $queryBuilder->andWhere('n.yearOf LIKE :query');
                    break;
            }
        }

        if ($parent === null) {
            $queryBuilder->setParameter('parent', $this->node->getNode()->getId());
        } else {
            $queryBuilder->setParameter('parent', $parent);
        }

        return Paginator::createFromRequest($this->request, $queryBuilder->getQuery(), true);
    }

    /**
     * Отображение списка
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdSiteBundle:Module:msp/list.html.twig', array('list' => $result));
    }

    /**
     * Деактивация всех записей
     */
    public function deactivateAll()
    {
        $this->getRepository()->createQuery('UPDATE __CLASS__ e SET e.active = 0')->execute();
    }

    /**
     * Импорт CSV файла
     * @param $filePath
     * @param $parent
     * @return bool
     */
    public function importCSV($filePath, $parent)
    {
        $handle = fopen($filePath, 'r');
        $parentNode = $this->em->getRepository('QCoreBundle:Node')->findOneById($parent);
        ini_set('display_errors', 1);
        error_reporting(-1);
        
        if ($handle) {
            fgetcsv($handle, null, ';'); // Пропускаем заголовок

            while($row = fgetcsv($handle, null, ';')) {
                foreach($row as &$v) {
                    $v = iconv('windows-1251', 'utf-8', $v);
                }
                unset($v);

                $data= $this->getRepository()
                    ->createQueryBuilder('e')
                    ->andWhere('e.recNum = :rn')
                    ->setParameter('rn', $row[0])
                    ->getQuery()
                    ->getResult();

                if (!empty($data) && $data[0] instanceof MSPent) {
                    $entity = $data[0];
                } else {
                    $entity = new MSPent();
                    $entity->setArchive(false);
                    $this->em->persist($entity);
                }

                $dateInc = DateTime::createFromFormat('d.m.Y', $row[1]);
                $dateApprove = DateTime::createFromFormat('d.m.Y', $row[3]);

                $entity->setRecNum($row[0]);
                $entity->setDateInc($dateInc ? $dateInc : null);
                $entity->setOrgTitle($row[2]);
                $entity->setDateApprove($dateApprove ? $dateApprove : null);
                $entity->setLlcTitle($row[4]);
                $entity->setInn($row[5]);
                $entity->setView($row[6]);
                $entity->setForm($row[7]);
                $entity->setAmount($row[8]);
                $entity->setYearOf($row[9]);
                $entity->setInformation($row[10]);
                $entity->setCategory((int)$row[11]);

                $entity->setActive(true);
                $entity->setParent($parentNode);

                $this->em->flush();
            }

            return true;
        }

        return false;
    }
}
