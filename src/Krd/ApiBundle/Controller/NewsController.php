<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Новости
 *
 * @Route("/news")
 */
class NewsController extends Controller
{
    protected $sectionList = array(7, 10358, 10359, 10489, 10490);

    /**
     * Список новостных разделов сайта
     *
     * @Route("/nodes.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function nodesAction()
    {
        $nodes = $this->getDoctrine()->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('e')
            ->andWhere('e.active = 1 AND e.controller = 3 AND e.id IN (:sectionList)')
            ->addOrderBy('e.left')
            ->setParameter('sectionList', $this->sectionList)
            ->getQuery()
                ->getResult();

        $result = array();

        foreach ($nodes as $node) {
            $result[] = array('id' => $node->getId(), 'title' => $node->getTitle(), 'url' => $node->getUrl(true));
        }

        return $result;
    }

    /**
     * Список новостей из всех разделов для проверки на ново-добавленные
     *
     * Обязательные параметры:
     *  lastId - ID последней новости о которой вам известно
     *
     * @Route("/novelty-{lastId}.{_format}", requirements={"lastId"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function noveltyAction($lastId)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdNewsBundle:News')
            ->createQueryBuilder('n')
            // ->andWhere('n.active = 1')
            ->andWhere('n.parent IN (:sectionList)')
            ->andWhere('n.id > :lastId')
            ->orderBy('n.date', 'DESC')
            ->setParameter('sectionList', $this->sectionList)
            ->setParameter('lastId', $lastId)
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Список новостей
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *  date - фильтрация по дате в формате Y-m-d (2014-06-19)
     *
     * Сортировка всегда происходит по дате по убыванию
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdNewsBundle:News')
            ->createQueryBuilder('n')
//            ->leftJoin('n.images', 'i')
//            ->leftJoin('n.video', 'v')
//            ->leftJoin('n.themes', 't')
            ->andWhere('n.active = 1');
		
		if( $parent == 7 ) {
            $queryBuilder->andWhere('n.parent IN(:parent, 15188)');
		} else
			$queryBuilder->andWhere('n.parent = :parent');
			
        $queryBuilder->setParameter('parent', $parent)
            ->orderBy('n.date', 'DESC');


        if ($date = $request->get('date')) {
            $date = new \DateTime($date);
            $queryBuilder->andWhere('DATE_FORMAT(n.date, \'%Y-%m-%d\') = :date');
            $queryBuilder->setParameter('date', $date->format('Y-m-d'));
        }

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Детальная новость
     *
     * @Route("/item/{id}.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function itemAction($id)
    {
        $item = $this->getDoctrine()->getRepository('KrdNewsBundle:News')->find($id);

        // if (!$item || !$item->getActive()) {
        //     throw $this->createNotFoundException();
        // }

        $response = new Response();
        $response->headers->set('Content-type', 'application/json');

        $json = $this->get('jms_serializer')->serialize($item, 'json');
        $json = json_decode($json, true);
        $json['content'] = $item->getContent();
        $json = json_encode($json);

        $response->setContent($json);

        return $response;
    }
}
