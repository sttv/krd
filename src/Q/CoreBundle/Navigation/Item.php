<?php

namespace Q\CoreBundle\Navigation;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;


/**
 * Пункт верхнего меню в CMS
 */
abstract class Item
{
    /**
     * Получение ссылки элемента меню
     */
    abstract public function getLink();

    /**
     * Получение заголовка элемента меню
     */
    abstract public function getTitle();

    /**
     * Раздел разрешен пользователю?
     * @return boolean
     */
    abstract public function isGranted();

    /**
     * Router
     * @var Router
     */
    protected $router;

    /**
     * SecurityContext
     * @var SecurityContext
     */
    protected $securityContext;

    function __construct(Router $router, SecurityContext $securityContext)
    {
        $this->router = $router;
        $this->securityContext = $securityContext;
    }

    /**
     * Получение подменю
     */
    public function getSubItems()
    {
        return array();
    }

    /**
     * Есть ли дочерние элементы?
     * @return boolean
     */
    public function hasSubItems()
    {
        return count($this->getSubItems()) > 0;
    }
}
