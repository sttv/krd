<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
ini_set('display_errors', 1);
/**
 * Контент
 *
 * @Route("/content")
 */
class ContentController extends Controller
{
    /**
     * Подготовка списка разделов для отдачи
     * @param  array $list
     * @return array
     */
    protected function prepareList($list)
    {
        $result = array();

        foreach ($list as $content) {
            $images = $this->get('jms_serializer')->serialize($content->getImages(), 'json');
            $files = $this->get('jms_serializer')->serialize($content->getFiles(), 'json');
            $cnt = $content->getContentMobile() ?: $content->getContent();

            $result[] = array(
                'title' => $content->getTitle(),
                'node_title' => $content->getParent()->getTitle(),
                'url' => $content->getParent()->getUrl(true),
                'name' => $content->getName(),
                'content' => $cnt,
                'announce' => mb_substr(strip_tags($cnt), 0, 100).'...',
                'created' => $content->getCreated()->format('Y-m-d H:i:s'),
                'hidedate' => $content->getHidedate(),
                'images' => json_decode($images, true),
                'files' => json_decode($files, true),
            );
        }

        return $result;
    }

    /**
     * Список контентных блоков раздела
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * @Route("/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction($parent)
    {
        $list = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Content')
            ->createQueryBuilder('e')
            ->andWhere('e.parent = :parent')
            ->addOrderBy('e.id')
            ->setParameter('parent', $parent)
            ->getQuery()
                ->getResult();

        return $this->prepareList($list);
    }
}
