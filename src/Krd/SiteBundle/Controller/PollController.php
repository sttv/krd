<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Контроллер опросов
 */
class PollController extends Controller implements ActiveSecuredController
{
    /**
     * Голосование в опросе
     *
     * @Route("/ajax/poll/vote.json", name="krd_ajax_site_poll_vote", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function voteAction(Request $request)
    {
        $pm = $this->get('krd.site.modules.poll');

        $poll = $pm->getRepository()->find($request->request->get('poll'));

        if (!$poll || $poll->hasVotedip($request->getClientIp())) {
            return array('success' => false, 'error' => 'Already voted');
        }

        $answer = $pm->getManager()->getRepository('KrdSiteBundle:PollAnswer')->findOneBy(array('id' => $request->request->get('answer'), 'parent' => $poll));

        if (!$answer) {
            return array('success' => false, 'error' => 'Invalid answer');
        }

        $poll->addVotedip($request->getClientIp());
        $answer->incrementCounter();

        $pm->getManager()->flush();

        return array('success' => true, 'poll' => $poll);
    }
}
