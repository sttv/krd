<?php

namespace Krd\MfcBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль MFC 1
 */
class Mfc1 extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    public function renderContent()
    {
        return $this->twig->render('KrdMfcBundle:Module:mfc/map1.html.twig');
    }
}


