/**
 * Интерактивность сайта
 */
angular.module('interactive', ['ngSanitize'])
    /**
     * Контроллер формы OpenKrasnodarConferenceController
     */
    .controller('okConferenceFormCtrl', ['$scope', '$element', function($scope, $element) {
        $('#ok-conference-form-success').hide();

        $scope.onSuccess = function(response) {
            $('#ok-conference-form-success').slideDown();
            $element.slideUp();

            if (!!$.scrollTo) {
                $.scrollTo($('#ok-conference-form-success'), 200, {offset:{top:-150}});
            }
        };
    }])

    /**
     * Кнопка прокрутки вверх
     */
    .directive('scrollUpBtn', [function() {
        return {
            restrict: 'A',
            scope: true,

            link: function($scope, $element) {
                $scope.visible = false;

                $scope.$watch('visible', function(visible) {
                    if (visible) {
                        $element.addClass('visible');
                    } else {
                        $element.removeClass('visible');
                    }
                });

                $(window).on('scroll', function() {
                    $scope.$apply(function() {
                        $scope.visible = ($(window).scrollTop() > 250);
                    });
                });

                $element.on('click', function(e) {
                    e.preventDefault();

                    if ($scope.visible) {
                        $.scrollTo(0, 200);
                    }
                });
            }
        }
    }])


    /**
     * Кликабельная иконка календаря у инпутов
     */
    .directive('uiDateIcon', [function() {
        return {
            restrict: 'AC',
            link: function($scope, $element) {
                var $icon = $('<i class="icon-calendar"></i>');
                $element.after($icon);
                $icon.on('click', function(e) {
                    e.preventDefault();
                    $element.focus();
                });
            }
        }
    }])

    /**
     * Управление классами body
     */
    .directive('themebleBody', [function() {
        return {
            restrict: 'A',
            scope: true,

            link: function($scope, $element) {
                $scope.fontsize = 2;

                if ($.cookie('fontsize')) {
                    $scope.fontsize = parseInt($.cookie('fontsize'), 10);
                }

                if ($scope.fontsize < 1) {
                    $scope.fontsize = 1;
                } else if ($scope.fontsize > 3) {
                    $scope.fontsize = 3;
                }

                $scope.$on('setFontSize', function(e, size) {
                    $scope.fontsize = size;
                });

                $scope.$on('setFontSmaller', function() {
                    if ($scope.fontsize > 1) {
                        $scope.fontsize--;
                    }
                });

                $scope.$on('setFontLarger', function() {
                    if ($scope.fontsize < 3) {
                        $scope.fontsize++;
                    }
                });

                $scope.$watch('fontsize', function() {
                    if (isNaN($scope.fontsize)) {
                        $scope.fontsize = 2;
                    }

                    $.cookie('fontsize', $scope.fontsize, {path: '/'});
                    $scope.$broadcast('krdFontSizeChange', $scope.fontsize);
                });
            },

            controller: function($scope) {
                $scope.getClass = function() {
                    var cl = [];;

                    if ($scope.fontsize) {
                        cl.push('font-size-' + $scope.fontsize);
                    }

                    return cl.join(' ');
                };
            }
        }
    }])

    /**
     * Кнопка расшаривания через Instapaper
     */
    .directive('krdShareInstapaper', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    var d=document;

                    try {
                        if (!d.body)
                            throw(0);

                        window.location='http://www.instapaper.com/text?u='+encodeURIComponent(d.location.href);
                    } catch(e) {
                        alert('Please wait until the page has loaded.');
                    }
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Readability
     */
    .directive('krdShareReadability', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    window.baseUrl='http://www.readability.com';
                    window.readabilityToken='';
                    var s=document.createElement('script');
                    s.setAttribute('type','text/javascript');
                    s.setAttribute('charset','UTF-8');
                    s.setAttribute('src',baseUrl+'/bookmarklet/read.js');
                    document.documentElement.appendChild(s);
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Evernote
     */
    .directive('krdShareEvernote', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    EN_CLIP_HOST='http://www.evernote.com';

                    try {
                        var x=document.createElement('SCRIPT');
                        x.type='text/javascript';
                        x.src=EN_CLIP_HOST+'/public/bookmarkClipper.js?'+(new Date().getTime()/100000);
                        document.getElementsByTagName('head')[0].appendChild(x);
                    } catch (e) {
                        location.href=EN_CLIP_HOST+'/clip.action?url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title);
                    }
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Pocket
     */
    .directive('krdSharePocket', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    ISRIL_H='1c95';
                    PKT_D='getpocket.com';
                    ISRIL_SCRIPT=document.createElement('SCRIPT');
                    ISRIL_SCRIPT.type='text/javascript';
                    ISRIL_SCRIPT.src='http://'+PKT_D+'/b/r.js';
                    document.getElementsByTagName('head')[0].appendChild(ISRIL_SCRIPT);
                });
            }
        }
    }])

    /**
     * Кнопка печати
     */
    .directive('krdPrint', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    window.print();
                });
            }
        }
    }])

    /**
     * Кнопки уменьшения шрифта
     */
    .directive('krdFontSmaller', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $rootScope.$broadcast('setFontSmaller');
                    });
                });

                $scope.$on('krdFontSizeChange', function(e, size) {
                    if (size == 1) {
                        $element.addClass('disabled');
                    } else {
                        $element.removeClass('disabled');
                    }
                });
            }
        }
    }])

    /**
     * Кнопки увеличения шрифта
     */
    .directive('krdFontLarger', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $rootScope.$broadcast('setFontLarger');
                    });
                });

                $scope.$on('krdFontSizeChange', function(e, size) {
                    if (size == 3) {
                        $element.addClass('disabled');
                    } else {
                        $element.removeClass('disabled');
                    }
                });
            }
        }
    }])

    /**
     * Плавающий блок
     */
    .directive('floating', [function() {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                var position = $element.css('position');

                if (position == 'absolute' || position == 'fixed') {
                    return;
                }

                var offset = attrs.floating ? parseInt(attrs.floating, 10) : 0;

                var top;
                var $placeholder;

                var enable = function() {
                    !!$placeholder && $element.after($placeholder);
                    $element.css({
                        position: 'fixed',
                        top: offset
                    });
                    $element.addClass('floating');
                    $scope.stateFloating = true;
                };

                var disable = function() {
                    $element.css('position', 'static');
                    $element.removeClass('floating');
                    !!$placeholder && $placeholder.remove();
                    $scope.stateFloating = false;
                };

                var update = function() {
                    var scroll = $(window).scrollTop();

                    if (attrs.floatingState !== undefined && $scope.$eval(attrs.floatingState) === false) {
                        disable();
                        return;
                    }

                    if ($element.css('position') != 'fixed') {
                        top = $element.offset().top - parseFloat($element.css('marginTop').replace(/auto/, 0)) - offset;
                        $placeholder = $('<div></div>').css({
                            width: $element.width(),
                            height: $element.height(),
                            marginTop: parseFloat($element.css('marginTop').replace(/auto/, 0)),
                            marginBottom: parseFloat($element.css('marginBottom').replace(/auto/, 0))
                        });
                    }

                    if(scroll < top) {
                        disable();
                    } else {
                        enable();
                    }
                };

                $scope.enableFloating = enable;
                $scope.disableFloating = disable;
                $scope.updateFloating = update;
                $scope.stateFloating = false;

                $(window).on('scroll resize', update);

                if (attrs.floatingState !== undefined) {
                    $scope.$watch(attrs.floatingState, update);
                }
            }
        }
    }])

    /**
     * Скрытие элемента и удаление его из DOM
     */
    .directive('uiIf', [function() {
        return {
            transclude: 'element',
            priority: 1000,
            terminal: true,
            restrict: 'A',
            compile: function (element, attr, transclude) {
                return function (scope, element, attr) {
                    var childElement;
                    var childScope;

                    scope.$watch(attr['uiIf'], function (newValue) {
                        if (childElement) {
                            childElement.remove();
                            childElement = undefined;
                        }

                        if (childScope) {
                            childScope.$destroy();
                            childScope = undefined;
                        }

                        if (newValue) {
                            childScope = scope.$new();

                            transclude(childScope, function (clone) {
                                childElement = clone;
                                element.after(clone);
                            });
                        }
                    });
                };
            }
        };
    }])

    /**
     * Список табов
     * Очень простой. Без запоминания таба и т.п.
     */
    .directive('tabsList', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                $element.find('.pane-list > .item').each(function(index) {
                    $(this).attr({
                        'ng-click': 'setActive('+index+')',
                        'ng-class': '{active:active == '+index+'}'
                    });
                });

                var $tabs = $element.children('.tab').hide();

                // Link
                return function($scope, $element, attrs) {
                    $scope.canBeNull = (attrs.noActive !== undefined);

                    var $activePane = $element.find('.pane-list > .item.active');
                    if ($activePane.length > 0) {
                        $scope.active = $activePane.index();
                    } else if ($scope.canBeNull) {
                        $scope.active = -1;
                    } else {
                        $scope.active = 0;
                    }

                    $scope.$watch('active', function(active) {
                        $tabs.hide();
                        $tabs.eq(active).show();

                        $scope.$broadcast('tabChange', active);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.setActive = function(index) {
                    if ($scope.canBeNull && $scope.active == index) {
                        $scope.active = -1;
                    } else {
                        $scope.active = index;
                    }
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей высоте
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiHeight', [function() {
        var heightList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiHeight:'@'
            },

            link: function($scope, $element) {
                if (heightList[$scope.multiHeight] === undefined) {
                    heightList[$scope.multiHeight] = $scope.heightList = {};
                } else {
                    $scope.heightList = heightList[$scope.multiHeight];
                }

                $scope.index = i++;

                $scope.heightList[$scope.index] = $element.height();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('heightList', function(heightList) {
                    var arr = [];

                    for(var ii in heightList) {
                        arr.push(parseInt(heightList[ii], 10));
                    }
                    var height = Math.max.apply(Math, arr);

                    if (height > 0 && !isNaN(height)) {
                        $element.height(height);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.height();
                    $element.css('height', 'auto');
                    $scope.heightList[$scope.index] = $element.height();
                    $element.height(h);
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей ширине
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiWidth', [function() {
        var widthList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiWidth:'@'
            },

            link: function($scope, $element) {
                if (widthList[$scope.multiWidth] === undefined) {
                    widthList[$scope.multiWidth] = $scope.widthList = {};
                } else {
                    $scope.widthList = widthList[$scope.multiWidth];
                }

                $scope.index = i++;

                $scope.widthList[$scope.index] = $element.width();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('widthList', function(widthList) {
                    var arr = [];

                    for(var ii in widthList) {
                        arr.push(parseInt(widthList[ii], 10));
                    }
                    var width = Math.max.apply(Math, arr);

                    if (width > 0 && !isNaN(width)) {
                        $element.width(width);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.width();
                    $element.css('width', 'auto');
                    $scope.widthList[$scope.index] = $element.width();
                    $element.width(h);
                };
            }
        }
    }])

    /**
     * Модификация ng-include для полной замены шаблона
     * Поддерживает onload
     */
    .directive('ngIncludeReplace', ['$parse', function($parse) {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: function(tElement, tAttrs) {
                return $parse(tAttrs.ngIncludeReplace, {})();
            },
            link: function($scope, $element, attrs) {
                if ($.isFunction(attrs.onload)) {
                    attrs.onload();
                }
            }
        }
    }])

    /**
     * Кнопка показа большего количества элементов
     *
     * Аттрибуты:
     *     more-btn="ea-list-item"         ID шаблона для элемента
     *     more-btn-body="ea-list-body"    ID блока списка элементов
     *     url="/simple/path"              URL для загрузки списка
     *     tpl-append                      Использовать обычный $.append() для шаблона. Полезно при использовании <tr> в качестве элементов
     */
    .directive('moreBtn', ['$compile', '$http', function($compile, $http) {
        return {
            restrict: 'A',
            scope: {
                url: '@'
            },

            compile: function($element, attrs) {
                if (!!attrs.moreBtnBody) {
                    var $listBody = $('#'+attrs.moreBtnBody);
                } else {
                    var $listBody = $element.prev('.items-list');
                }

                if (!!attrs.tplAppend) {
                    var $listTemplate = $('<div ng-include-replace="\''+attrs.moreBtn+'\'" ng-repeat="item in items"></div>');

                    $listBody.append($listTemplate);
                } else {
                    var $listTemplate = $($('#'+attrs.moreBtn).html());

                    $listTemplate.attr({
                        'ng-repeat': 'item in items'
                    });

                    $listBody.append($listTemplate);
                }

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.currentPage = 1;
                    $scope.loading = false;
                    $scope.originalHtml = $element.html();

                    $compile($listTemplate)($scope);

                    $element.on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.nextPage();
                        });
                    });

                    $scope.$watch('loading', function(loading) {
                        if (loading) {
                            $element.html('Загрузка...');
                        } else {
                            $element.html($scope.originalHtml);
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if (response.page_count <= $scope.currentPage) {
                            $element.hide();
                        }

                        for(var i = 0; i < response.items.length; i++) {
                            $scope.items.push(response.items[i]);
                        }

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])

    /**
     * Блок FAQ
     */
    .directive('faqBlockInteractive', [function() {
        return {
            restrict: 'C',
            link: function($scope, $element) {
                $element.find('.item > .question').each(function() {
                    var $item = $(this).parent();

                    $(this).on('click', function(e) {
                        e.preventDefault();
                        $item.toggleClass('close open');
                    });
                });
            }
        };
    }])
;
