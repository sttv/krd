<?php

namespace Q\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use FOS\UserBundle\Model\Group as BaseGroup;

use Q\FormMetadataBundle\Configuration as Form;

/**
 * Модель группы пользователей
 *
 * @ORM\Entity
 * @ORM\Table(name="user_group")
 *
 * @Form\Fields(
 *     name={"text", {"label"="Название"}},
 *     roles={"text_collection", {"label"="Привелегии"}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class UserGroup extends BaseGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @param string $role
     *
     * @return Group
     */
    public function addRole($role)
    {
        if (!empty($role) && !$this->hasRole($role) && mb_strtoupper($role) != 'ROLE_IDDQD') {
            $this->roles[] = mb_strtoupper($role);
        }

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
