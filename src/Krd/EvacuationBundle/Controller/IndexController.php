<?php

namespace Krd\EvacuationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Эвакуированные автомобили
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template()
     */
    public function listAction()
    {

    }

    /**
     * Список для виджета на главной
     * @Route("/ajax/evacuated_avto/widget_list.json", name="ajax_evacuated_avto_widget_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function widgetListAction(Request $request)
    {
        return array(
            'items' => $this->get('krd.evacuation.modules.avto')->getRepository()->getWidgetList()
        );
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/evacuated_avto/list.json", name="ajax_evacuated_avto_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        return $this->get('krd.evacuation.modules.avto')->getItems();
    }
}
