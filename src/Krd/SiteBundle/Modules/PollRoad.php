<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use KrdSiteBundle\Entity\Poll as PollEntity;
use KrdSiteBundle\Entity\PollAnswer;


/**
 * Модуль - PollRoad
 */
class PollRoad extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/poll_road.html.twig');
    }

    /**
     * Список улиц
     * @return array
     */
    protected function getItemsList()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.sort');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        return $queryBuilder->getQuery()->getResult();
    }

    public function renderContent()
    {
        return $this->twig->render('KrdSiteBundle:Module:poll_road/list.html.twig', array(
            'list' => $this->getItemsList(),
            'isVoted' => $_COOKIE['poll-road-'.$this->node->getNode()->getId()],
            ));
    }
}
