<?php

namespace Krd\BannersBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль баннеров
 */
class Banners extends AbstractModule
{
    public function renderAdminContent() {}

    /**
     * Список баннеров в футере
     */
    public function renderFooterBanners()
    {
        return $this->twig->render('KrdBannersBundle:Module:banners/footer.html.twig', array('items' => $this->getRepository()->findActiveByType('footer')));
    }

    /**
     * Баннер на главной странице под виджетами
     */
    public function renderIndexBanners()
    {
        return $this->twig->render('KrdBannersBundle:Module:banners/index.html.twig', array('items' => $this->getRepository()->findActiveByType('index')));
    }

    public function onNodeRemove(Node $node)
    {
    }
}
