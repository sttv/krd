<?php

namespace Krd\GlavaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Видео
 */
class FaqController extends Controller implements ActiveSecuredController
{
    /**
     * Список вопросов
     *
     * @Route("/")
     * @Template()
     */
	//@Secure(roles="ROLE_CMS")
    public function listAction()
    {
    }
	
	/**
	 * Конкретный вопрос
	 * 
	 * @Route("/detail-{id}.{_format}", requirements={"id"="^\d+$", "_format"="html"})
	 * @Method({"GET"})
	 */
	public function detailAction(Request $request)
	{
		$item = $this->getDoctrine()->getManager()->find('KrdSiteBundle:Faq', $request->get('id'));
		return $this->render('KrdGlavaBundle:Module:faq/print.html.twig', array('item' => $item));
	}
	
	/**
     * Список альбомов для постраничного вывода
     * @Route("/ajax/glava/faq/list.json", name="ajax_glava_faq_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
    	$queryBuilder = $this->get('krd.site.modules.faq')->getRepository()
            ->createQueryBuilder('v')
            ->andWhere('v.active = 1')
            ->andWhere('v.parent = :parent')
            ->orderBy('v.date', 'DESC')
            ->setParameter('parent', $request->get('parent'));
				
        return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 10);
    }
}