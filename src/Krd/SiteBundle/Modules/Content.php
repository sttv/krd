<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Entity\Node;


/**
 * Модуль контента
 */
class Content extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/content.html.twig');
    }

    public function renderContent()
    {
        $content = $this->getRepository()->findByParent($this->node->getNode()->getId());

        return $this->twig->render('KrdSiteBundle:Module:content/content.html.twig', array('content' => $content));
    }

    /**
     * Возвращает весь контент страницы без обрамления в шаблон
     * @return string
     */
    public function renderRawContent()
    {
        $content = $this->getRepository()->findByParent($this->node->getNode()->getId());
        $result = '';

        foreach($content as $item) {
            $result .= $item->getContent();
        }

        return $result;
    }

    /**
     * Контент на главной департамента
     */
    public function renderDepartamentIndex()
    {
        $content = $this->getRepository()
            ->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent AND e.name != 'contacts'")
            ->setParameter('parent', $this->node->getNode()->getId())
            ->useResultCache(true, 15)
            ->getResult();

        return $this->twig->render('KrdSiteBundle:Module:content/departament-content.html.twig', array('content' => $content));
    }

    /**
     * Контакты на главной департамента
     */
    public function renderDepartamentContacts()
    {
        $content = $this->getRepository()
            ->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent AND e.name = 'contacts'")
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller AND n.name IN (:names)')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 2)
            ->setParameter('names', array('contacts', 'contact', 'kontakty', 'kontakt'))
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $node = $temp[0];
        } else {
            $node = null;
        }

        return trim($this->twig->render('KrdSiteBundle:Module:content/departament-content-contacts.html.twig', array('content' => $content, 'node' => $node)));
    }

    /**
     * Получение контента по системному имени
     * @param  string $name
     * @return Content
     */
    public function getContent($name)
    {
        $content = $this->getRepository()->findBy(array('parent' => $this->node->getNode()->getId(), 'name' => $name));

        if (isset($content[0])) {
            return $content[0];
        } else {
            return null;
        }
    }

    /**
     * Получение контентных блоков текущей страницы
     * @return array
     */
    public function getContents()
    {
        return $this->getRepository()->findBy(array('parent' => $this->node->getNode()->getId()));
    }

    /**
     * Получение списка привязанных файлов к разделу
     *
     * @return  array
     */
    public function getFilesForNode(Node $node)
    {
        $content = $this->getRepository()
            ->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent")
            ->setParameter('parent', $node->getId())
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($content[0])) {
            return $content[0]->getFiles();
        } else {
            return array();
        }
    }

    /**
     * Дополнительное менб
     */
    public function renderAsideMenu()
    {
        $menu = array();
        $node = $this->node->getNode();

        while(empty($menu) && $node instanceof Node) {
            $menu = $this->menuManager->getMenu('aside', $node->getId());
            $node = $node->getParent();
        }

        if (!empty($menu)) {
            return $this->twig->render('KrdSiteBundle:Module:content/aside-menu.html.twig', array('menu' => $menu));
        } else {
            return '';
        }
    }

    /**
     * Внутриконтентное меню
     */
    public function renderIncontentMenu()
    {
        $menu = $this->menuManager->getMenu('content', $this->node->getNode()->getId());
        $menuExtend = $this->menuManager->getMenu('content-extend', $this->node->getNode()->getId());

        $menu = array_merge((array)$menu, (array)$menuExtend);

        return $this->twig->render('KrdSiteBundle:Module:content/incontent-menu.html.twig', array('menu' => $menu));
    }

    /**
     * Внутриконтентное меню (только родительское)
     */
    public function renderIncontentMenuParent()
    {
        $menu = $this->menuManager->getMenu('content-extend', $this->node->getNode()->getParent()->getId());

        return $this->twig->render('KrdSiteBundle:Module:content/incontent-menu.html.twig', array('menu' => $menu));
    }

    /**
     * Меню глубокоуровневых страниц над контентом
     */
    public function renderContentTopMenu()
    {
        $menu = $this->menuManager->getMenu(array('content-link-top', 'content-menu-top'), $this->node->getNode()->getId(), array('content-link-top', 'content-menu-top', 'content-link-bottom', 'content-menu-bottom'));

        return $this->twig->render('KrdSiteBundle:Module:content/deep-content-menu.html.twig', array('menu' => $menu));
    }

    /**
     * Меню глубокоуровневых страниц под контентом
     */
    public function renderContentBottomMenu()
    {
        $menu = $this->menuManager->getMenu(array('content-link-bottom', 'content-menu-bottom'), $this->node->getNode()->getId(), array('content-link-top', 'content-menu-top', 'content-link-bottom', 'content-menu-bottom'));

        return $this->twig->render('KrdSiteBundle:Module:content/deep-content-menu.html.twig', array('menu' => $menu));
    }
}


