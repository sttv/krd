/**
 * Фильтры для удобного отображения данных в строках таблицы
 */
angular.module('cms.table.filters', [])
    /**
     * Фильтр замены строки
     */
    .filter('replace', [function() {
        return function(value, source, dest) {
            return value.replace(source, dest).replace(source, dest).replace(source, dest);
        }
    }])

    /**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Фильтр отображения объекта
     */
    .filter('el_object', [function() {
        return function(value, name) {
            return !!value && value.hasOwnProperty(name) ? value[name] : '';
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'" target="_self">'+value+'</a>';
        }
    }])

    /**
     * Ссылка на страницу CMS
     */
    .filter('el_cms_link', ['$filter', function($filter) {
        return function(value, prefix) {
            var url = prefix + value;

            return '<a href="'+url+'" title="'+url+'" target="_self">' + $filter('characters')(value, 50) + '</a>';
        }
    }])

    /**
     * Одиночное изображение
     */
    .filter('el_image', [function() {
        return function(value) {
            if (!!value && !!value.size && !!value.size.cms) {
                return '<img src="'+value.size.cms+'" title="'+value.title+'" />';
            } else {
                return '---';
            }
        }
    }])
;
