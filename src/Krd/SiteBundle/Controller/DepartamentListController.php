<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Список департаментов
 */
class DepartamentListController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')
            ->createQueryBuilder('n')
            ->andWhere('n.parent = :parent')
            ->andWhere('n.controller = 8')
            ->andWhere('n.active = 1')
            ->addOrderBy('n.left', 'ASC')
            ->setParameter('parent', $this->get('qcore.routing.current')->getNode()->getId())
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

        return array('list' => $list);
    }
}
