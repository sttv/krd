<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Краснодар и цифры
 */
class CitynumController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/ajax/krd/site/citynum/random.json", defaults={"_format"="json"}, name="krd_site_citynum_random")
     * @Method({"GET"})
     */
    public function getRandomAction(Request $request)
    {
        $ids = explode(',', $request->query->get('id', ''));
        $ids = array_map('intval', $ids);

        $item = $this->get('krd.site.modules.citynum')->findRandom($ids);

        if ($item) {
            return array('item' => $item);
        } else {
            throw $this->createNotFoundException();
        }
    }
}
