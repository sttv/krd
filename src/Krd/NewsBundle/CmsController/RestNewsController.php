<?php

namespace Krd\NewsBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер новостей
 *
 * @Route("/rest/krd/news")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestNewsController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'KrdNewsBundle:News';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_krd_news_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_krd_news_edit';
    }

    /**
     * Список
     *
     * @Route("/", name="cms_rest_krd_news", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        return parent::listAction($request);
    }

    protected function updateListQueryBuilder(Request $request, QueryBuilder $qb)
    {
        if ($parent = $request->get('parent')) {
            $qb->andWhere('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        }

        $qb->orderBy('e.date', 'DESC');
    }

    /**
     * Создание
     *
     * @Route("/", name="cms_rest_krd_news_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $entity = $this->getRepository()->createNew();

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, $this->getRepository()->getManyToManyFields(), $this->getFormName('create_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getParent()->getId() == 17255) {
                $errors = array();
                $annonceLength = $this->getTextLength($entity->getAnnounce());
                $contentLength = $this->getTextLength($entity->getContent());

                if ($annonceLength > 250) {
                    $errors['create_krdnewsbundle_news[announce]'][] = 'Длина экстренного сообщения не должна превышать 250 символов, сейчас '.$annonceLength;
                }

                if ($contentLength > 250) {
                    $errors['create_krdnewsbundle_news[content]'][] = 'Длина экстренного сообщения не должна превышать 250 символов, сейчас '.$contentLength;
                }

                if (!empty($errors)) {
                    return array('success' => false, 'errors' => $errors);
                }
            }

            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'entity' => $entity);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_krd_news_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, $this->getRepository()->getManyToManyFields(), $this->getFormName('edit_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($entity, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getParent()->getId() == 17255) {
                $errors = array();
                $annonceLength = $this->getTextLength($entity->getAnnounce());
                $contentLength = $this->getTextLength($entity->getContent());

                if ($annonceLength > 250) {
                    $errors['edit_krdnewsbundle_news[announce]'][] = 'Длина экстренного сообщения не должна превышать 250 символов, сейчас '.$annonceLength;
                }

                if ($contentLength > 250) {
                    $errors['edit_krdnewsbundle_news[content]'][] = 'Длина экстренного сообщения не должна превышать 250 символов, сейчас '.$contentLength;
                }

                if (!empty($errors)) {
                    return array('success' => false, 'errors' => $errors);
                }
            }

            $this->getDoctrine()->getManager()->persist($entity);
            $this->getDoctrine()->getManager()->flush();
            return array('success' => true, 'entity' => $entity);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Длина текста
     *
     * @param $text
     * @return int
     */
    private function getTextLength($text)
    {
        $text = strip_tags($text);
        $text = trim($text);

        return mb_strlen($text);
    }

    /**
     * Удаление
     *
     * @Route("/{id}", name="cms_rest_krd_news_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        return parent::removeAction($request, $id);
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->findOne($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');
        $entity->setRssdisabled($request->get('rssdisabled', false) === true || $request->get('rssdisabled', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Рендер формы добавления
     *
     * @Route("/form/", name="cms_rest_krd_news_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function createFormAction(Request $request)
    {
        return parent::createFormAction($request);
    }

    public function beforeCreateForm(Request $request, $entity)
    {
        $entity->setTitle('Новость');
        $entity->setName('news_'.date('dmY_His'));
        $entity->setDate(new \DateTime());

        if (($parent = $request->get('parent')) && ($parent = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->find($parent))) {
            $entity->setParent($parent);
        }
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_krd_news_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function editFormAction($id)
    {
        return parent::editFormAction($id);
    }

    /**
     * Перемещение
     *
     * @Route("/move/{id}", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function moveAction(Request $request, $id)
    {
        $nodeId = $request->request->get('node');
        $movetype = $request->request->get('movetype', 'last');

        return parent::actionMoveEntity($id, $nodeId, $movetype);
    }
}
