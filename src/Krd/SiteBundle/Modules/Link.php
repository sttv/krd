<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль - ссылки
 */
class Link extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/link.html.twig');
    }

    public function renderPreFooter()
    {
        if (!preg_match('/^\/lichnyy-kabinet/', $this->request->getPathInfo())) {
            $items = $this->getRepository()->findBy(array('parent' => 1, 'active' => 1), array('sort' => 'ASC'));
            return $this->twig->render('KrdSiteBundle:Module:link/pre-footer.html.twig', array('items' => $items));
        }
    }
}
