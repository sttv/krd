<?php

namespace Krd\GlavaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Бандл страницы главы
 */
class KrdGlavaBundle extends Bundle
{
}
