/**
 * Объекты города
 */
angular.module('city-object', ['dialog'])
    /**
     * Объект города
     */
    .directive('cityObjectItem', ['$dialog', function($dialog) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                var dialogUrl = attrs.cityObjectItem;
                var name = attrs.cityObjectName;

                var showMe = function() {
                    window.location.hash = name;

                    $dialog.create(dialogUrl,
                        {
                            onClose: function() {
                                window.location.hash = '_';
                            }
                        }
                    );
                };

                $element.find('a').on('click', function(e) {
                    e.preventDefault();

                    showMe();
                });

                if (window.location.hash) {
                    var hash = window.location.hash.toString().replace('/', '').replace('#', '');
                    if (hash && hash == name) {
                        showMe();
                    }
                }
            }
        }
    }])
;
