<?php

namespace Krd\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Список руководителей города
 */
class MayorController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.line.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * Детальная страница
     *
     * @Route("/{name}.html", name="krd_administration_mayor_detail")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function detailAction()
    {
    }

    /**
     * Детальное описани для подробного вывода
     * @Route("/ajax/administration/mayor/{id}.html", name="ajax_administration_mayor_detail", requirements={"id"="^\d+$"}, defaults={"_format"="html"})
     * @Method({"GET"})
     * @Template()
     */
    public function detailDialogAction(Request $request, $id)
    {
        $result = $this->get('krd.administration.modules.mayor')->getRepository()->find(array('id' => $id, 'active' => true));

        if (!$result) {
            throw $this->createNotFoundException('Not found');
        }

        return array('item' => $result);
    }
}
