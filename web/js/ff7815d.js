/**
 * Директива отображения и управления списком сущностей
 */
angular.module('cms')
    .directive('cmsEntityList', ['$http', function($http) {
        var url = {};

        return {
            restrict: 'E',
            link: function($scope, $element, attrs) {
                url = {
                    list: attrs.url+'list.json',
                };
                 alert(1);
                $http.get(url.list).success(function(list) {
                    console.log(list);
                });
            }
        };
    }]);
