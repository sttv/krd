<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;
use Krd\GlavaBundle\Entity\Video;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Импорт раздела видео с сайта glava.krd.ru на страницу главы на сайт krd.ru
 */
class GlavaVideo
{
    protected $em;
	protected $gem;
	
	protected $uploadDir;

    public function __construct(EntityManager $em, EntityManager $gem)
    {
        $this->em = $em;
		$this->gem = $gem;
    }
	
	public function setUploadDir($dir)
	{
		$this->uploadDir = $dir;
	}
	
	public function getUploadDir()
	{
		return $this->uploadDir;
	}
	
	public function import($output) 
	{
		$videoList = $this->loadVideo();
		$videoNode = $this->em->find('QCoreBundle:Node', 15269);
		
		$videoProcessed = 0;
		$this->em->getConnection()->beginTransaction();
		foreach( $videoList as $oldVideo ) {
			$name = 'video_'.date('dmY', strtotime($oldVideo['date'])).'_'.$oldVideo['id'];
			$exists = false;
			$video = $this->em->getRepository('KrdGlavaBundle:Video')->findBy(array('name' => $name));
			
			if( !empty($video) ) {
				$video = array_pop($video); 
				$video->setRefId((int)$oldVideo['id']);
				//$output->writeln("Exists {$video->getId()}:{$name}");
				
				$exists = true;
			} else {
				$video = new Video();
				$video->setRefId((int)$oldVideo['id']);
				$video->setTitle(htmlspecialchars_decode( $oldVideo['name']) );
				
				$video->setParent($videoNode);
				$video->setName($name);
				$video->setDate(new \DateTime($oldVideo['date']));
				$video->setActive(true);
				$this->em->persist($video);
			}
			
			if( !$exists ) {
				if (empty($oldVideo['video_icon'])) {
	                foreach( $this->getPhotos($oldVideo['video_icon']) as $imageArr ) {
	                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
	                    $filesystem = new Filesystem();
	                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
	
	                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
	                    $image->setTitle('');
	                    $image->setSort($i);
	                    $video->addImages($image);
	                }
	            }
				
				/*if ( !empty($oldVideo['video']) ) {
					$videoUrl = preg_replace('/http:\/\/(www.)?krd.ru\//i', '/', $oldVideo['video']);
					$videoUrl = '/home/krdru/data/www/krd.ru/web' . $videoUrl;
					
					if( is_file($videoUrl) ) {
		                $tempPath = $this->getUploadDir().'/'.uniqid('video-', true).'.'.pathinfo(basename($oldVideo['video']), PATHINFO_EXTENSION);
		                $filesystem = new Filesystem();
		                $filesystem->copy($videoUrl, $tempPath);
		
		                $file = $this->em->getRepository('QFilesBundle:Video')->createVideoFromFile($tempPath);
		                $file->setTitle('');
		                $video->addVideo($file);
		                $this->em->flush();
		
		                if (empty($oldVideo['video_icon'])) {
		                    $this->em->getRepository('QFilesBundle:Video')->cretePreview($file);
		                } else {
		                	foreach( $this->getPhotos($oldVideo['video_icon']) as $imageArr ) {
					
			                    $tempPath = $this->getUploadDir().'/'.uniqid('image-', true).'.'.pathinfo(basename($imageArr['sourceFile']), PATHINFO_EXTENSION);
			                    $filesystem = new Filesystem();
			                    $filesystem->dumpFile($tempPath, file_get_contents($imageArr['sourceFile']));
			
			                    $image = $this->em->getRepository('QFilesBundle:Image')->createImageFromFile($tempPath);
			                    $video->addImages($image);
		                    	$this->em->flush();
							}
		                }
					}
	            }*/
			}
			
			$this->em->flush();
			$videoProcessed++;

			if ($videoProcessed % 30 == 0) {
				$this->em->getConnection()->commit();
                $this->em->clear('KrdGlavaBundle:Video');
                $output->writeln("Processed {$videoProcessed}");
                $this->em->getConnection()->beginTransaction();
			}
		}
		$this->em->getConnection()->commit();
		$output->writeln('imported '.$videoProcessed.' videos...');
	}
	
	function getPhotos($group_id,$main=0){
		$list = $this->gem->getConnection()->prepare("SELECT * FROM `evlanov__photos2` WHERE `group_id`=:group_id ORDER BY `main` DESC, `ord`");
		$list->execute(array('group_id' => $group_id));
		
		$arr = array();
		while($r = $list->fetch()) {
			$photo = unserialize($r['photo']);
			$r['sourceFile'] = '/home/krdru/data/www/glava.krd.ru/files/image/video/' . $photo['size2'];
			$arr[] = $r;
		}

		return $arr;
	}
	
	private function loadVideo()
	{
		$list = $this->gem->getConnection()->prepare('SELECT * FROM evlanov__video');
		$list->execute();
		
		$video = array();
		while( $row = $list->fetch() ) {
			$video[] = $row;
		}
		
		return $video;
	}
}