<?php

namespace Krd\ConferenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Krd\AdministrationBundle\Entity\Human;
use Q\FilesBundle\Entity\Image;


/**
 * Вопрос онлайн конференции
 *
 * @ORM\Entity
 * @ORM\Table(name="conference_online_question",
 *      indexes={
 *          @ORM\Index(name="dateask", columns={"dateask"}),
 *          @ORM\Index(name="datereply", columns={"datereply"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"entity", {
 *         "label"="Относится к конференции",
 *         "class"="KrdConferenceBundle:Online",
 *         "property"="title",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     dateask={"datetime", {
 *         "label"="Дата/Время вопроса",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     fioask={"text", {
 *         "label"="ФИО вопрошающего",
 *     }},
 *     question={"textarea", {
 *         "label"="Текст вопроса",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     datereply={"datetime", {
 *         "label"="Дата/Время ответа",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     humans={"entity", {
 *         "label"="Отвечающие",
 *         "class"="KrdAdministrationBundle:Human",
 *         "property"="fio",
 *         "multiple"=true,
 *         "required"=false
 *     }},
 *     hPhotos={"files", {
 *         "label"="Фото отвечающих (если не выбраны выше)",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     hTitle={"text", {
 *         "label"="ФИО отвечающих (если не выбраны выше)",
 *         "required"=false
 *     }},
 *     reply={"textarea", {
 *         "label"="Текст ответа",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Online", inversedBy="questions")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Дата вопроса
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     */
    private $dateask;

    /**
     * ФИО задающего вопрос
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $fioask = '';

    /**
     * Вопрос
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $question = '';


    /**
     * Дата ответа
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     */
    private $datereply;

    /**
     * Отвечающие
     *
     * @ORM\ManyToMany(targetEntity="Krd\AdministrationBundle\Entity\Human", cascade={"persist"})
     * @ORM\JoinTable(name="conference_question_human_relation",
     *      joinColumns={@ORM\JoinColumn(name="online_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="human_id", referencedColumnName="id")}
     *      )
     *
     * @ORM\OrderBy({"fio" = "ASC"})
     */
    private $humans;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="conference_question_hphotos_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $hPhotos;

    /**
     * Имя отвечающего
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $hTitle = '';

    /**
     * Ответ
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $reply = '';

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function setParent(Online $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDateAsk()
    {
        return $this->dateask;
    }

    public function setDateAsk(\DateTime $dateask)
    {
        $this->dateask = $dateask;
    }

    public function getFioAsk()
    {
        return $this->fioask;
    }

    public function setFioAsk($fioask)
    {
        $this->fioask = $fioask;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function getDateReply()
    {
        return $this->datereply;
    }

    public function setDateReply(\DateTime $datereply)
    {
        $this->datereply = $datereply;
    }

    public function getHumans()
    {
        return $this->humans ?: $this->humans = new ArrayCollection();
    }

    public function hasHumans()
    {
        return count($this->getHumans()) > 0;
    }

    public function addHumans(Human $human)
    {
        if (!$this->getHumans()->contains($human)) {
            $this->getHumans()->add($human);
        }
    }

    public function removeHumans(Human $human)
    {
        if ($this->getHumans()->contains($human)) {
            $this->getHumans()->removeElement($human);
        }
    }

    public function getReply()
    {
        return $this->reply;
    }

    public function hasReply()
    {
        return !empty($this->reply);
    }

    public function setReply($reply)
    {
        $this->reply = $reply;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getHPhotos()
    {
        return $this->hPhotos ?: $this->hPhotos = new ArrayCollection();
    }

    public function hasHPhotos()
    {
        return count($this->getHPhotos()) > 0;
    }

    public function addHPhotos(Image $image)
    {
        if (!$this->getHPhotos()->contains($image)) {
            $this->getHPhotos()->add($image);
        }
    }

    public function removeHPhotos(Image $image)
    {
        if ($this->getHPhotos()->contains($image)) {
            $this->getHPhotos()->removeElement($image);
        }
    }

    public function getHTitle()
    {
        return $this->hTitle;
    }

    public function hasHTitle()
    {
        return !empty($this->hTitle);
    }

    public function setHTitle($hTitle)
    {
        $this->hTitle = $hTitle;
    }

    public function getUrl()
    {
        return $this->getParent()->getUrl().'#conference-online-question-'.$this->getId();
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Список фотографий отвечающих
     * @return array
     */
    public function getHumansPhotos()
    {
        $result = array();

        if ($this->hasHumans()) {
            foreach($this->getHumans() as $human) {
                if ($human->hasWhiteImages()) {
                    $result[] = $human->getMainWhiteImage();
                }
            }
        } elseif ($this->hasHPhotos()) {
            $result = $this->getHPhotos();
        }

        return $result;
    }

    /**
     * ФИО отвечающего
     */
    public function getReplyTitle()
    {
        if ($this->hasHumans()) {
            $res = array();
            foreach ($this->getHumans() as $human) {
                $res[] = $human->getFio();
            }

            return implode(', ', $res);
        } elseif ($this->hasHTitle()) {
            return $this->getHTitle();
        } else {
            return '---';
        }
    }
}


