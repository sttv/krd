/**
 * Группы соц-сетей в футере
 */
angular.module('socialgroups', [])
    /**
     * Табы соц-плагинов
     */
    .directive('socialTabs', [function() {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                $element.find('.st-item').each(function() {
                    var target = $(this).data('target');

                    $(this).attr('ng-click', 'setActive("'+target+'")');
                    $(this).attr('ng-class', '{active:active == "'+target+'"}');
                });

                return function($scope, $element) {
                    $scope.active = '';

                    var fst = true;
                    $scope.$watch('active', function(active) {
                        if (fst) {
                            fst = false;
                            return;
                        }

                        $element.find('.st-item').each(function() {
                            var target = $(this).data('target');
                            if (active == target) {
                                $('#'+target).show();
                            } else {
                                $('#'+target).hide();
                            }
                        });
                    });
                }
            },

            controller: function($scope) {
                $scope.setActive = function(target) {
                    if ($scope.active == target) {
                        $scope.active = '';
                    } else {
                        $scope.active = target;
                    }
                };
            }
        }
    }])

    /**
     * Виджет твиттера
     */
    .directive('socialTwitter', [function() {
        return {
            restrict: 'A',
            scope: {
                widgetId: '@socialTwitter',
                width: '@',
                href: '@',
                height: '@'
            },

            template: '<a class="twitter-timeline" width="{{ width }}" height="{{ height }}" href="https://twitter.com/krdru" data-widget-id="{{ widgetId }}">Твиты пользователя @krdru</a>',

            link: function($scope, $element, attrs) {
                var __interval = setInterval(function() {
                    var $iframe = $element.children('iframe');

                    if ($iframe.length) {
                        $iframe.width($scope.width);
                    }
                }, 100);

                $(window).load(function() {
                    setTimeout(function() {
                        clearInterval(__interval);
                    }, 15000);

                    if (attrs.autoHide !== undefined) {
                        $element.hide().addClass('loaded');
                    }
                })
            }
        }
    }])

    /**
     * Виджет facebook
     */
    .directive('socialFacebook', [function() {
        return {
            restrict: 'A',
            scope: {
                href: '@socialFacebook',
                width: '@',
                colorscheme: '@',
                showFaces: '@',
                header: '@',
                stream: '@',
                showBorder: '@'
            },

            template: '<div class="fb-like-box" data-href="{{ href }}" data-width="{{ width }}" data-colorscheme="{{ colorscheme }}" data-show-faces="{{ showFaces }}" data-header="{{ header }}" data-stream="{{ stream }}" data-show-border="{{ showBorder }}"></div>',

            link: function($scope, $element, attrs) {
                $(window).on('load', function() {
                    if (!!FB && !!FB.init) {
                        FB.init();

                        if (attrs.autoHide !== undefined) {
                            $element.hide().addClass('loaded');
                        }
                    }
                });
            }
        }
    }])

    /**
     * Виджет вконтакте
     */
    .directive('socialVkcom', [function() {
        return {
            restrict: 'A',
            scope: {
                group: '@socialVkcom',
                width: '@',
                color1: '@',
                color2: '@',
                color3: '@'
            },

            link: function($scope, $element, attrs) {
                var id = 'vkgroup-widget-'+(Math.random()*1000).toString().replace(/[^0-9]/, '')+(Math.random()*1000).toString().replace(/[^0-9]/, '');
                var $container = $('<div id="'+id+'"></div>');

                $container.height($scope.height);

                $element.append($container);

                $(window).load(function() {
                    VK.Widgets.Group(id, {
                        mode:0,
                        width: $scope.width,
                        height: $scope.height,
                        color1: $scope.color1,
                        color2: $scope.color2,
                        color3: $scope.color3},
                    $scope.group);

                    setTimeout(function() {
                        $container.children('iframe').on('load', function() {
                            if (attrs.autoHide !== undefined) {
                                $element.hide().addClass('loaded');
                            }
                        });
                    }, 13);
                });
            }
        }
    }])
;
