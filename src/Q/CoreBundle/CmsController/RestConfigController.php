<?php

namespace Q\CoreBundle\CmsController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

use Q\CoreBundle\Controller\RestUniversalController;


/**
 * Контроллер Config
 *
 * @Route("/rest/q/core/config")
 * @PreAuthorize("hasRole('ROLE_CMS_ADMIN')")
 */
class RestConfigController extends RestUniversalController
{
    protected function getEntityName()
    {
        return 'QCoreBundle:Config';
    }

    protected function getCreateFormAction()
    {
        return 'cms_rest_q_core_config_create';
    }

    protected function getEditFormAction()
    {
        return 'cms_rest_q_core_config_edit';
    }

    /**
     * Редактирование
     *
     * @Route("/{id}", name="cms_rest_q_core_config_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }
}
