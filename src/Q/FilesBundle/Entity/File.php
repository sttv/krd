<?php

namespace Q\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Загружаемый файл
 *
 * @ORM\Entity
 * @ORM\Table(name="file",
 *     indexes={
 *          @ORM\Index(name="sort", columns={"sort"})
 *     }
 * )
 *
 * @FileStore\Uploadable
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Название"}},
 *     file={"file", {"label"="Файл"}}
 * )
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $title = '';

    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d/m/Y H:i'>")
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;

    /**
     * Загруженный файл
     * @ORM\Column(type="array")
     * @FileStore\UploadableField(mapping="files")
     *
     * @JMS\Expose
     */
    private $file;

    /**
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = (string)$title;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (empty($this->title)) {
            $this->title = $file->getClientOriginalName();
        }
    }

    public function setFileArray(array $file) {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setSort($sort)
    {
        $this->sort = (int)$sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getFileType()
    {
        return pathinfo($this->file['path'], PATHINFO_EXTENSION);
    }

    public function getSize()
    {
        return isset($this->file['size']) ? $this->file['size'] : 0;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}
