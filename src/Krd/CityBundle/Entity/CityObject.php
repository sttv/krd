<?php

namespace Krd\CityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\Node;
use Q\FormMetadataBundle\Configuration as Form;
use Q\FilesBundle\Entity\Image;


/**
 * Объект города
 *
 * @ORM\Entity
 * @ORM\Table(name="city_object",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     announce={"textarea", {
 *         "label"="Краткое содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     content={"textarea", {
 *         "label"="Содержание",
 *         "attr"={"class"="tinymce", "data-theme"="advanced"},
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class CityObject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;

    /**
     * Краткое содержание
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $announce = '';

    /**
     * Содержание
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $content = '';


    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="city_object_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="city_object_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getMainImage")
     */
    private $mainImage;


    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function hasAnnounce()
    {
        return !empty($this->announce);
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function hasContent()
    {
        return !empty($this->content);
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function getUrl()
    {
        return '#'.$this->getName();
    }
}


