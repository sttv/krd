$(function() {
    $('body').on('click', '.error-report', function(e) {
        e.preventDefault();

        var $overlay = $('<div class="export-overlay"></div>').appendTo('body');
        var $window = $('#error-report-window').appendTo('body');

        $window.find('input:hidden[name="page"]').val(window.location.href);
        $window.find('a.page-address').html(window.location.href);
        $window.find('.row').show();
        $window.find('.success').remove();

        $overlay.css({opacity:0, display:'block'}).animate({opacity:0.5});
        $window.css({opacity:1}).fadeIn(200);

        var __close = function() {
            $overlay.fadeOut(200);
            $window.fadeOut(200, function() {
                $overlay.remove();
            });

            return false;
        };

        $overlay.on('click', __close);
        $window.find('.close').on('click', __close);
    });

    var submitFrom = function(){
        var $window = $('#error-report-window');

        var data = {
            name: $window.find('input[name="name"]').val(),
            page: window.location.href,
            type: $window.find('select[name="type"]').val(),
            description: $window.find('textarea[name="description"]').val()
        }

        var error = false;

        if (data.type == '') {
            $window.find('select[name="type"]').closest('.row').addClass('error');
            error = true;
        } else {
            $window.find('select[name="type"]').closest('.row').removeClass('error');
        }

        if (data.description == '') {
            $window.find('textarea[name="description"]').closest('.row').addClass('error');
            error = true;
        } else {
            $window.find('textarea[name="description"]').closest('.row').removeClass('error');
        }

        if (!error) {
            $.post('/opendata/list/?errorreport=1', data, function(r) {
                if (r.success) {
                    $window.find('.row').slideUp(200);
                    setTimeout(function(){
                        $window.find('.w').append('<div class="success">Спасибо, Ваше сообщение отправлено!</div>');
                    }, 200);
                } else {
                    alert('Произошла ошибка, попробуйте еще раз');
                }
            });
        }
    }

    $('#error-report-window').find('input').on('keyup', function(e) {
        if (e.keyCode == 13) {
            submitFrom();
        }
    });

    $('#error-report-window').find('button').on('click', function() {
        submitFrom();
    });
});
