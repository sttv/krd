/**
 * Слайдер подробного вида галереи
 */
angular.module('gallery.slider', ['horizontal.scroll'])
    .directive('gallerySlider', ['$compile', '$timeout', '$http', function($compile, $timeout, $http) {
        return {
            restrict: 'A',
            scrope: true,

            template: function($element) {
                var tpl = '<div class="big-preview no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                        + '<a class="icon" ng-href="{{ fancyboxUrl }}" ng-class="{busy:loading}">'
                            + '<img ng-src="{{ previewUrl }}" />'
                            + '<div class="title" ng-show="!!previewTitle">{{ previewTitle }}</div>'
                            + '<div class="loading"></div>'
                        + '</a>'
                    + '</div>'
                    + '<div class="preview-list no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="items-list-wrap" horizontal-scroll="gs" horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active">'
                            + '<div class="items-list clearfix">'
                                + $element.html()
                            + '</div>'
                        + '</div>'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                    + '</div>';

                return tpl;
            },

            link: function($scope, $element) {
                $scope.current = 0;
                $scope.fancyboxUrl = '';
                $scope.previewUrl = '';
                $scope.previewTitle = '';
                $scope.loading = false;
                $scope.$items = $element.find('.preview-list .items-list > .item');

                $scope.$items.each(function(i) {
                    $(this).on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.setCurrent(i);
                        });
                    });
                });

                $scope.$watch('current', function(current) {
                    if (current > $scope.$items.length - 1) {
                        $scope.current = $scope.$items.length - 1;
                        return;
                    }

                    if (current < 0) {
                        $scope.current = 0;
                        return;
                    }

                    $scope.$broadcast('setPosition', 'gs', current);
                    $scope.$items.removeClass('active');

                    var $activeItem = $scope.$items.eq(current);

                    $activeItem.addClass('active');

                    $scope.fancyboxUrl = $activeItem.data('fancybox');
                    $scope.previewUrl = $activeItem.data('preview');
                    $scope.previewTitle = $activeItem.attr('title');
                });

                $scope.$watch('previewUrl', function(previewUrl) {
                    if (previewUrl == '') {
                        return;
                    }

                    $scope.loading = true;

                    $http.get(previewUrl).success(function() {
                        $timeout(function() {
                            $scope.loading = false;
                        }, 300);
                    });
                });
            },

            controller: function($scope, $element) {
                $scope.next = function() {
                    $scope.current++;
                };

                $scope.prev = function() {
                    $scope.current--;
                };

                $scope.setCurrent = function(current) {
                    $scope.current = current;
                }
            }
        }
    }])
;
