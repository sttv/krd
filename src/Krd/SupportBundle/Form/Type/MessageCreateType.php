<?php

namespace Krd\SupportBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Форма создания нового сообщения
 */
class MessageCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('parent', 'hidden_support_message');

        $theme = $builder->create('theme', 'choice', array(
            'label' => 'Выберите',
            'required' => true,
            'choices' => array(
                'Проблема в работе личного кабинета (профиля)' => 'Проблема в работе личного кабинета (профиля)',
                'Пожелания по работе личного кабинета (сайта)' => 'Пожелания по работе личного кабинета (сайта)',
                'Техническая консультация' => 'Техническая консультация',
                'Пользовательский интерфейс' => 'Пользовательский интерфейс',
                'Проблемы функционального характера' => 'Проблемы функционального характера',
                'Актуальность информации на сайте' => 'Актуальность информации на сайте',
                'Проблема с поиском информации' => 'Проблема с поиском информации',
                'Вопрос администратору сайта' => 'Вопрос администратору сайта',
                'Смена E-mail адреса' => 'Смена E-mail адреса',
                'Удаление личного кабинета' => 'Удаление личного кабинета',
                'Другая проблема' => 'Другая проблема',
            ),
            'empty_value' => '--- Выберите тему ---',
        ))->addModelTransformer(new FilterHtmlTransformer());

        $title = $builder->create('title', 'text', array(
            'label' => 'Заголовок',
            'required' => true,
        ))->addModelTransformer(new FilterHtmlTransformer());

        $content = $builder->create('content', 'textarea', array(
            'label' => 'Вопрос',
            'required' => true,
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($theme);
        $builder->add($title);
        $builder->add($content);

        $builder->add('submit', 'submit', array(
            'label' => 'Отправить',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\SupportBundle\Entity\Message',
            'csrf_protection' => true,
            'validation_groups' => array('create'),
        ));
    }

    public function getName()
    {
        return 'support_message_create';
    }
}
