<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Контентная страница
 */
class ContentController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $cn = $this->get('qcore.routing.current')->getNode();

        if (isset($_POST['just_password'])) {
            $_SESSION['jp_node_'.$cn->getId()] = $_POST['just_password'];
        }

        $isUnderPassword = ($cn && $cn->isUnderJustPassword() && $_SESSION['jp_node_'.$cn->getId()] != $cn->getJustPassword());

        return array('isUnderPassword' => $isUnderPassword);
    }
}
