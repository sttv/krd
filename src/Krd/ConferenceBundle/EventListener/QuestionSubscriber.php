<?php

namespace Krd\ConferenceBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;
use Krd\ConferenceBundle\Entity\Question;


/**
 * События для Question
 */
class QuestionSubscriber implements EventSubscriber
{
    protected $container;
    protected $mailer;
    protected $deferred = array();

    public function __construct(Container $container)
    {
        $this->container = $container;

    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Сбор ново-добавленных вопросов
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Question) {
            $this->deferred[] = $entity;
        }
    }

    /**
     * Отправка Email уведомлений о новых вопросах
     */
    public function postFlush()
    {
        foreach($this->deferred as $entity) {
            $message = $this->getMailer()->newMessage('Новый вопрос к онлайн-конферении на сайте %title%');

            $this->getMailer()->sendTemplate($message, 'KrdConferenceBundle:Email:new-question.admin.html.twig', array('question' => $entity));
        }

        $this->deferred = array();
    }
}

