<?php

namespace Krd\AppealBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
//use Symfony\Component\HttpFoundation\File\File;

use Q\FilesBundle\Form\Type\FilesType;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;

/**
 * Обращение
 */
class AppealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Поскольку понормальному оно почему-то не работает
        $addressGroups = array('address');

        if (in_array('address', $options['validation_groups'])) {
            $addressGroups[] = 'Default';
        }

        $builder->add('parent', 'hidden_node', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Не указан раздел интернет-приемной')),
            ),
        ));

        $builder->add('destination', 'appeal_destination', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать адресата')),
            ),
        ));

        $name = $builder->create('name', 'text', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Имя')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $lastname = $builder->create('lastname', 'text', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Фамилию')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $midname = $builder->create('midname', 'text', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Отчество')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $phone = $builder->create('phone', 'text', array(
            'required' => false,
            'validation_groups' => array('Default'),
            'attr' => array(
                'ui-phone' => '',
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $email = $builder->create('email', 'text', array(
            'required' => false,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Email')),
                new Assert\Email(array('message' => 'Email имеет не верный формат')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $postcode = $builder->create('postcode', 'text', array(
            'required' => true,
            'validation_groups' => $addressGroups,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Индекс')),
                new Assert\Regex(array('pattern' => "/^\d{6}$/", 'message' => 'Индекс указан не верно')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $city = $builder->create('city', 'text', array(
            'required' => true,
            'validation_groups' => $addressGroups,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Город')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $district = $builder->create('district', 'text', array(
            'required' => false,
            'validation_groups' => $addressGroups,
        ))->addModelTransformer(new FilterHtmlTransformer());

        $street = $builder->create('street', 'text', array(
            'required' => true,
            'validation_groups' => $addressGroups,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Улицу')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $house = $builder->create('house', 'text', array(
            'required' => true,
            'validation_groups' => $addressGroups,
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Необходимо указать Дом')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $room = $builder->create('room', 'text', array(
            'required' => false,
            'validation_groups' => array('Default'),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $message = $builder->create('message', 'textarea', array(
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы забыли написать сообщение')),
            ),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $replyType = $builder->create('replyType', 'choice', array(
            'label' => 'Направить ответ',
            'required' => true,
            'validation_groups' => array('Default'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Вы не выбрали куда направить ответ')),
            ),
            'choices' => array(
                1 => 'По электронной почте',
                2 => 'По почте',
            ),
            'empty_value' => '--- Выберите ---',
        ))->addModelTransformer(new FilterHtmlTransformer());

        $appealFiles = $builder->create('appealFiles', 'text', array(
            'label' => 'Файл',
            'required' => false,
            'validation_groups' => array('Default'),
        ))->addModelTransformer(new FilterHtmlTransformer());

        $builder->add($name);
        $builder->add($lastname);
        $builder->add($midname);
        $builder->add($phone);
        $builder->add($email);
        $builder->add($postcode);
        $builder->add($city);
        $builder->add($district);
        $builder->add($street);
        $builder->add($house);
        $builder->add($room);
        $builder->add($message);
        $builder->add($replyType);

        if(isset($_COOKIE['dddd'])) {

            $builder->add($appealFiles);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Krd\AppealBundle\Entity\Appeal',
            'csrf_protection' => true,
        ));
    }

    public function getName()
    {
        return 'appeal';
    }
}
