<?php

namespace Q\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Валидатор списка ролей пользователя/группы
 */
class UserRolesValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!is_array($value)) {
            $this->context->addViolation($constraint->notArrayMessage);
            return;
        }

        foreach($value as $i => $role) {
            if (!preg_match('/^ROLE_\w+$/', $role)) {
                $this->context->addViolationAt('['.$i.']', $constraint->message);
            }
        }
    }
}
