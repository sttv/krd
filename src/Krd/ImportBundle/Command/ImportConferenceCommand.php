<?php

namespace Krd\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

use Krd\ConferenceBundle\Entity\Online;
use Krd\ConferenceBundle\Entity\Question;


/**
 * Импорт онлайн конференций
 */
class ImportConferenceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('krd:import:conference')
            ->setDescription('Import online conference from old site krd.ru')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $oldEm = $this->getContainer()->get('doctrine.orm.oldsite_entity_manager');

        $stConference = $oldEm->getConnection()->prepare("SELECT * FROM `krd__conference` ORDER BY `id`");
        $stConference->execute();

        $conferenceNode = $em->getRepository('QCoreBundle:Node')->find(2311);

        $partners = $em->getRepository('KrdConferenceBundle:Partner')->findAll();

        while ($conference = $stConference->fetch()) {
            $dateStart = \DateTime::createFromFormat('U', $conference['date_start']);
            $dateStart->setTimezone(new \DateTimeZone(date_default_timezone_get()));

            $dateEnd = \DateTime::createFromFormat('U', $conference['date_end']);
            $dateEnd->setTimezone(new \DateTimeZone(date_default_timezone_get()));

            $confEntity = new Online();
            $confEntity->setParent($conferenceNode);
            $confEntity->setTitle($conference['title']);
            $confEntity->setName(uniqid());
            $confEntity->setDatestart($dateStart);
            $confEntity->setDateend($dateEnd);
            $confEntity->setAnnounce($conference['anons']);
            $confEntity->setContent($conference['content']);
            $confEntity->setActive(true);

            foreach ($this->parseHumans($conference['fio']) as $human) {
                $confEntity->addHumans($human);
            }

            foreach ($partners as $partner) {
                $confEntity->addPartners($partner);
            }

            $em->persist($confEntity);
            $em->flush();

            $stQuestions = $oldEm->getConnection()->prepare("SELECT * FROM `krd__conference_comment` WHERE `show` = 1 AND `parent` = :parent");
            $stQuestions->execute(array('parent' => $conference['id']));

            while ($question = $stQuestions->fetch()) {
                $dateAsk = \DateTime::createFromFormat('U', $question['q_date']);
                $dateAsk->setTimezone(new \DateTimeZone(date_default_timezone_get()));

                $dateReply = \DateTime::createFromFormat('U', $question['a_date']);
                $dateReply->setTimezone(new \DateTimeZone(date_default_timezone_get()));

                $quEntity = new Question();
                $quEntity->setParent($confEntity);
                $quEntity->setDateAsk($dateAsk);
                $quEntity->setFioask($question['name']);
                $quEntity->setQuestion($question['q_text']);
                $quEntity->setDateReply($dateReply);

                if ($human = $this->parseHumans(($question['a_name'] == 0 ? $conference['fio'] : $conference['fio2']), true)) {
                    $quEntity->setHuman($human);
                }

                $quEntity->setReply($question['a_text']);
                $quEntity->setActive(true);

                $em->persist($quEntity);
                $em->flush();
            }
        }
    }

    /**
     * Парсинг строки с именами
     * На выходе массив с элементами Human
     *
     * @param  string $fio
     * @param  boolean $getFirst
     * @return array
     */
    protected function parseHumans($fio, $getFirst = false)
    {
        $result = array();

        foreach (explode(',', $fio) as $item) {
            $item = trim($item);
            $parts = explode(' ', $item);
            $itempart = array_pop($parts);

            if (empty($itempart)) {
                $itempart = 'Sjkqjwexciu'; // Nothing to find
            }

            $humans = $this->getContainer()->get('doctrine.orm.entity_manager')->
                createQuery("SELECT h FROM KrdAdministrationBundle:Human h WHERE h.fio = :fio OR h.fio LIKE :fiolike OR h.fio LIKE :fiolikepart")
                ->setParameter('fio', $item)
                ->setParameter('fiolike', '%'.$item.'%')
                ->setParameter('fiolikepart', '%'.$itempart.'%')
                ->setMaxResults(1)
                ->getResult();

            foreach ($humans as $human) {
                $result[] = $human;
            }
        }

        if ($getFirst && !empty($result)) {
            return $result[0];
        }

        return $result;
    }
}
