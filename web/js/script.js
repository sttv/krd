/*!
* MediaElement.js
* HTML5 <video> and <audio> shim and player
* http://mediaelementjs.com/
*
* Creates a JavaScript object that mimics HTML5 MediaElement API
* for browsers that don't understand HTML5 or can't play the provided codec
* Can play MP4 (H.264), Ogg, WebM, FLV, WMV, WMA, ACC, and MP3
*
* Copyright 2010-2013, John Dyer (http://j.hn)
* License: MIT
*
*/var mejs=mejs||{};mejs.version="2.13.1";mejs.meIndex=0;
mejs.plugins={silverlight:[{version:[3,0],types:["video/mp4","video/m4v","video/mov","video/wmv","audio/wma","audio/m4a","audio/mp3","audio/wav","audio/mpeg"]}],flash:[{version:[9,0,124],types:["video/mp4","video/m4v","video/mov","video/flv","video/rtmp","video/x-flv","audio/flv","audio/x-flv","audio/mp3","audio/m4a","audio/mpeg","video/youtube","video/x-youtube"]}],youtube:[{version:null,types:["video/youtube","video/x-youtube","audio/youtube","audio/x-youtube"]}],vimeo:[{version:null,types:["video/vimeo",
"video/x-vimeo"]}]};
mejs.Utility={encodeUrl:function(a){return encodeURIComponent(a)},escapeHTML:function(a){return a.toString().split("&").join("&amp;").split("<").join("&lt;").split('"').join("&quot;")},absolutizeUrl:function(a){var b=document.createElement("div");b.innerHTML='<a href="'+this.escapeHTML(a)+'">x</a>';return b.firstChild.href},getScriptPath:function(a){for(var b=0,c,d="",e="",f,g,h=document.getElementsByTagName("script"),l=h.length,j=a.length;b<l;b++){f=h[b].src;c=f.lastIndexOf("/");if(c>-1){g=f.substring(c+
1);f=f.substring(0,c+1)}else{g=f;f=""}for(c=0;c<j;c++){e=a[c];e=g.indexOf(e);if(e>-1){d=f;break}}if(d!=="")break}return d},secondsToTimeCode:function(a,b,c,d){if(typeof c=="undefined")c=false;else if(typeof d=="undefined")d=25;var e=Math.floor(a/3600)%24,f=Math.floor(a/60)%60,g=Math.floor(a%60);a=Math.floor((a%1*d).toFixed(3));return(b||e>0?(e<10?"0"+e:e)+":":"")+(f<10?"0"+f:f)+":"+(g<10?"0"+g:g)+(c?":"+(a<10?"0"+a:a):"")},timeCodeToSeconds:function(a,b,c,d){if(typeof c=="undefined")c=false;else if(typeof d==
"undefined")d=25;a=a.split(":");b=parseInt(a[0],10);var e=parseInt(a[1],10),f=parseInt(a[2],10),g=0,h=0;if(c)g=parseInt(a[3])/d;return h=b*3600+e*60+f+g},convertSMPTEtoSeconds:function(a){if(typeof a!="string")return false;a=a.replace(",",".");var b=0,c=a.indexOf(".")!=-1?a.split(".")[1].length:0,d=1;a=a.split(":").reverse();for(var e=0;e<a.length;e++){d=1;if(e>0)d=Math.pow(60,e);b+=Number(a[e])*d}return Number(b.toFixed(c))},removeSwf:function(a){var b=document.getElementById(a);if(b&&/object|embed/i.test(b.nodeName))if(mejs.MediaFeatures.isIE){b.style.display=
"none";(function(){b.readyState==4?mejs.Utility.removeObjectInIE(a):setTimeout(arguments.callee,10)})()}else b.parentNode.removeChild(b)},removeObjectInIE:function(a){if(a=document.getElementById(a)){for(var b in a)if(typeof a[b]=="function")a[b]=null;a.parentNode.removeChild(a)}}};
mejs.PluginDetector={hasPluginVersion:function(a,b){var c=this.plugins[a];b[1]=b[1]||0;b[2]=b[2]||0;return c[0]>b[0]||c[0]==b[0]&&c[1]>b[1]||c[0]==b[0]&&c[1]==b[1]&&c[2]>=b[2]?true:false},nav:window.navigator,ua:window.navigator.userAgent.toLowerCase(),plugins:[],addPlugin:function(a,b,c,d,e){this.plugins[a]=this.detectPlugin(b,c,d,e)},detectPlugin:function(a,b,c,d){var e=[0,0,0],f;if(typeof this.nav.plugins!="undefined"&&typeof this.nav.plugins[a]=="object"){if((c=this.nav.plugins[a].description)&&
!(typeof this.nav.mimeTypes!="undefined"&&this.nav.mimeTypes[b]&&!this.nav.mimeTypes[b].enabledPlugin)){e=c.replace(a,"").replace(/^\s+/,"").replace(/\sr/gi,".").split(".");for(a=0;a<e.length;a++)e[a]=parseInt(e[a].match(/\d+/),10)}}else if(typeof window.ActiveXObject!="undefined")try{if(f=new ActiveXObject(c))e=d(f)}catch(g){}return e}};
mejs.PluginDetector.addPlugin("flash","Shockwave Flash","application/x-shockwave-flash","ShockwaveFlash.ShockwaveFlash",function(a){var b=[];if(a=a.GetVariable("$version")){a=a.split(" ")[1].split(",");b=[parseInt(a[0],10),parseInt(a[1],10),parseInt(a[2],10)]}return b});
mejs.PluginDetector.addPlugin("silverlight","Silverlight Plug-In","application/x-silverlight-2","AgControl.AgControl",function(a){var b=[0,0,0,0],c=function(d,e,f,g){for(;d.isVersionSupported(e[0]+"."+e[1]+"."+e[2]+"."+e[3]);)e[f]+=g;e[f]-=g};c(a,b,0,1);c(a,b,1,1);c(a,b,2,1E4);c(a,b,2,1E3);c(a,b,2,100);c(a,b,2,10);c(a,b,2,1);c(a,b,3,1);return b});
mejs.MediaFeatures={init:function(){var a=this,b=document,c=mejs.PluginDetector.nav,d=mejs.PluginDetector.ua.toLowerCase(),e,f=["source","track","audio","video"];a.isiPad=d.match(/ipad/i)!==null;a.isiPhone=d.match(/iphone/i)!==null;a.isiOS=a.isiPhone||a.isiPad;a.isAndroid=d.match(/android/i)!==null;a.isBustedAndroid=d.match(/android 2\.[12]/)!==null;a.isBustedNativeHTTPS=location.protocol==="https:"&&(d.match(/android [12]\./)!==null||d.match(/macintosh.* version.* safari/)!==null);a.isIE=c.appName.toLowerCase().match(/trident/gi)!==
null;a.isChrome=d.match(/chrome/gi)!==null;a.isFirefox=d.match(/firefox/gi)!==null;a.isWebkit=d.match(/webkit/gi)!==null;a.isGecko=d.match(/gecko/gi)!==null&&!a.isWebkit&&!a.isIE;a.isOpera=d.match(/opera/gi)!==null;a.hasTouch="ontouchstart"in window&&window.ontouchstart!=null;a.svg=!!document.createElementNS&&!!document.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect;for(c=0;c<f.length;c++)e=document.createElement(f[c]);a.supportsMediaTag=typeof e.canPlayType!=="undefined"||a.isBustedAndroid;
try{e.canPlayType("video/mp4")}catch(g){a.supportsMediaTag=false}a.hasSemiNativeFullScreen=typeof e.webkitEnterFullscreen!=="undefined";a.hasNativeFullscreen=typeof e.requestFullscreen!=="undefined";a.hasWebkitNativeFullScreen=typeof e.webkitRequestFullScreen!=="undefined";a.hasMozNativeFullScreen=typeof e.mozRequestFullScreen!=="undefined";a.hasMsNativeFullScreen=typeof e.msRequestFullscreen!=="undefined";a.hasTrueNativeFullScreen=a.hasWebkitNativeFullScreen||a.hasMozNativeFullScreen||a.hasMsNativeFullScreen;
a.nativeFullScreenEnabled=a.hasTrueNativeFullScreen;if(a.hasMozNativeFullScreen)a.nativeFullScreenEnabled=document.mozFullScreenEnabled;else if(a.hasMsNativeFullScreen)a.nativeFullScreenEnabled=document.msFullscreenEnabled;if(a.isChrome)a.hasSemiNativeFullScreen=false;if(a.hasTrueNativeFullScreen){a.fullScreenEventName="";if(a.hasWebkitNativeFullScreen)a.fullScreenEventName="webkitfullscreenchange";else if(a.hasMozNativeFullScreen)a.fullScreenEventName="mozfullscreenchange";else if(a.hasMsNativeFullScreen)a.fullScreenEventName=
"MSFullscreenChange";a.isFullScreen=function(){if(e.mozRequestFullScreen)return b.mozFullScreen;else if(e.webkitRequestFullScreen)return b.webkitIsFullScreen;else if(e.hasMsNativeFullScreen)return b.msFullscreenElement!==null};a.requestFullScreen=function(h){if(a.hasWebkitNativeFullScreen)h.webkitRequestFullScreen();else if(a.hasMozNativeFullScreen)h.mozRequestFullScreen();else a.hasMsNativeFullScreen&&h.msRequestFullscreen()};a.cancelFullScreen=function(){if(a.hasWebkitNativeFullScreen)document.webkitCancelFullScreen();
else if(a.hasMozNativeFullScreen)document.mozCancelFullScreen();else a.hasMsNativeFullScreen&&document.msExitFullscreen()}}if(a.hasSemiNativeFullScreen&&d.match(/mac os x 10_5/i)){a.hasNativeFullScreen=false;a.hasSemiNativeFullScreen=false}}};mejs.MediaFeatures.init();
mejs.HtmlMediaElement={pluginType:"native",isFullScreen:false,setCurrentTime:function(a){this.currentTime=a},setMuted:function(a){this.muted=a},setVolume:function(a){this.volume=a},stop:function(){this.pause()},setSrc:function(a){for(var b=this.getElementsByTagName("source");b.length>0;)this.removeChild(b[0]);if(typeof a=="string")this.src=a;else{var c;for(b=0;b<a.length;b++){c=a[b];if(this.canPlayType(c.type)){this.src=c.src;break}}}},setVideoSize:function(a,b){this.width=a;this.height=b}};
mejs.PluginMediaElement=function(a,b,c){this.id=a;this.pluginType=b;this.src=c;this.events={};this.attributes={}};
mejs.PluginMediaElement.prototype={pluginElement:null,pluginType:"",isFullScreen:false,playbackRate:-1,defaultPlaybackRate:-1,seekable:[],played:[],paused:true,ended:false,seeking:false,duration:0,error:null,tagName:"",muted:false,volume:1,currentTime:0,play:function(){if(this.pluginApi!=null){this.pluginType=="youtube"?this.pluginApi.playVideo():this.pluginApi.playMedia();this.paused=false}},load:function(){if(this.pluginApi!=null){this.pluginType!="youtube"&&this.pluginApi.loadMedia();this.paused=
false}},pause:function(){if(this.pluginApi!=null){this.pluginType=="youtube"?this.pluginApi.pauseVideo():this.pluginApi.pauseMedia();this.paused=true}},stop:function(){if(this.pluginApi!=null){this.pluginType=="youtube"?this.pluginApi.stopVideo():this.pluginApi.stopMedia();this.paused=true}},canPlayType:function(a){var b,c,d,e=mejs.plugins[this.pluginType];for(b=0;b<e.length;b++){d=e[b];if(mejs.PluginDetector.hasPluginVersion(this.pluginType,d.version))for(c=0;c<d.types.length;c++)if(a==d.types[c])return"probably"}return""},
positionFullscreenButton:function(a,b,c){this.pluginApi!=null&&this.pluginApi.positionFullscreenButton&&this.pluginApi.positionFullscreenButton(Math.floor(a),Math.floor(b),c)},hideFullscreenButton:function(){this.pluginApi!=null&&this.pluginApi.hideFullscreenButton&&this.pluginApi.hideFullscreenButton()},setSrc:function(a){if(typeof a=="string"){this.pluginApi.setSrc(mejs.Utility.absolutizeUrl(a));this.src=mejs.Utility.absolutizeUrl(a)}else{var b,c;for(b=0;b<a.length;b++){c=a[b];if(this.canPlayType(c.type)){this.pluginApi.setSrc(mejs.Utility.absolutizeUrl(c.src));
this.src=mejs.Utility.absolutizeUrl(a);break}}}},setCurrentTime:function(a){if(this.pluginApi!=null){this.pluginType=="youtube"?this.pluginApi.seekTo(a):this.pluginApi.setCurrentTime(a);this.currentTime=a}},setVolume:function(a){if(this.pluginApi!=null){this.pluginType=="youtube"?this.pluginApi.setVolume(a*100):this.pluginApi.setVolume(a);this.volume=a}},setMuted:function(a){if(this.pluginApi!=null){if(this.pluginType=="youtube"){a?this.pluginApi.mute():this.pluginApi.unMute();this.muted=a;this.dispatchEvent("volumechange")}else this.pluginApi.setMuted(a);
this.muted=a}},setVideoSize:function(a,b){if(this.pluginElement.style){this.pluginElement.style.width=a+"px";this.pluginElement.style.height=b+"px"}this.pluginApi!=null&&this.pluginApi.setVideoSize&&this.pluginApi.setVideoSize(a,b)},setFullscreen:function(a){this.pluginApi!=null&&this.pluginApi.setFullscreen&&this.pluginApi.setFullscreen(a)},enterFullScreen:function(){this.pluginApi!=null&&this.pluginApi.setFullscreen&&this.setFullscreen(true)},exitFullScreen:function(){this.pluginApi!=null&&this.pluginApi.setFullscreen&&
this.setFullscreen(false)},addEventListener:function(a,b){this.events[a]=this.events[a]||[];this.events[a].push(b)},removeEventListener:function(a,b){if(!a){this.events={};return true}var c=this.events[a];if(!c)return true;if(!b){this.events[a]=[];return true}for(i=0;i<c.length;i++)if(c[i]===b){this.events[a].splice(i,1);return true}return false},dispatchEvent:function(a){var b,c,d=this.events[a];if(d){c=Array.prototype.slice.call(arguments,1);for(b=0;b<d.length;b++)d[b].apply(null,c)}},hasAttribute:function(a){return a in
this.attributes},removeAttribute:function(a){delete this.attributes[a]},getAttribute:function(a){if(this.hasAttribute(a))return this.attributes[a];return""},setAttribute:function(a,b){this.attributes[a]=b},remove:function(){mejs.Utility.removeSwf(this.pluginElement.id);mejs.MediaPluginBridge.unregisterPluginElement(this.pluginElement.id)}};
mejs.MediaPluginBridge={pluginMediaElements:{},htmlMediaElements:{},registerPluginElement:function(a,b,c){this.pluginMediaElements[a]=b;this.htmlMediaElements[a]=c},unregisterPluginElement:function(a){delete this.pluginMediaElements[a];delete this.htmlMediaElements[a]},initPlugin:function(a){var b=this.pluginMediaElements[a],c=this.htmlMediaElements[a];if(b){switch(b.pluginType){case "flash":b.pluginElement=b.pluginApi=document.getElementById(a);break;case "silverlight":b.pluginElement=document.getElementById(b.id);
b.pluginApi=b.pluginElement.Content.MediaElementJS}b.pluginApi!=null&&b.success&&b.success(b,c)}},fireEvent:function(a,b,c){var d,e;if(a=this.pluginMediaElements[a]){b={type:b,target:a};for(d in c){a[d]=c[d];b[d]=c[d]}e=c.bufferedTime||0;b.target.buffered=b.buffered={start:function(){return 0},end:function(){return e},length:1};a.dispatchEvent(b.type,b)}}};
mejs.MediaElementDefaults={mode:"auto",plugins:["flash","silverlight","youtube","vimeo"],enablePluginDebug:false,httpsBasicAuthSite:false,type:"",pluginPath:mejs.Utility.getScriptPath(["mediaelement.js","mediaelement.min.js","mediaelement-and-player.js","mediaelement-and-player.min.js"]),flashName:"flashmediaelement.swf",flashStreamer:"",enablePluginSmoothing:false,enablePseudoStreaming:false,pseudoStreamingStartQueryParam:"start",silverlightName:"silverlightmediaelement.xap",defaultVideoWidth:480,
defaultVideoHeight:270,pluginWidth:-1,pluginHeight:-1,pluginVars:[],timerRate:250,startVolume:0.8,success:function(){},error:function(){}};mejs.MediaElement=function(a,b){return mejs.HtmlMediaElementShim.create(a,b)};
mejs.HtmlMediaElementShim={create:function(a,b){var c=mejs.MediaElementDefaults,d=typeof a=="string"?document.getElementById(a):a,e=d.tagName.toLowerCase(),f=e==="audio"||e==="video",g=f?d.getAttribute("src"):d.getAttribute("href");e=d.getAttribute("poster");var h=d.getAttribute("autoplay"),l=d.getAttribute("preload"),j=d.getAttribute("controls"),k;for(k in b)c[k]=b[k];g=typeof g=="undefined"||g===null||g==""?null:g;e=typeof e=="undefined"||e===null?"":e;l=typeof l=="undefined"||l===null||l==="false"?
"none":l;h=!(typeof h=="undefined"||h===null||h==="false");j=!(typeof j=="undefined"||j===null||j==="false");k=this.determinePlayback(d,c,mejs.MediaFeatures.supportsMediaTag,f,g);k.url=k.url!==null?mejs.Utility.absolutizeUrl(k.url):"";if(k.method=="native"){if(mejs.MediaFeatures.isBustedAndroid){d.src=k.url;d.addEventListener("click",function(){d.play()},false)}return this.updateNative(k,c,h,l)}else if(k.method!=="")return this.createPlugin(k,c,e,h,l,j);else{this.createErrorMessage(k,c,e);return this}},
determinePlayback:function(a,b,c,d,e){var f=[],g,h,l,j={method:"",url:"",htmlMediaElement:a,isVideo:a.tagName.toLowerCase()!="audio"},k;if(typeof b.type!="undefined"&&b.type!=="")if(typeof b.type=="string")f.push({type:b.type,url:e});else for(g=0;g<b.type.length;g++)f.push({type:b.type[g],url:e});else if(e!==null){l=this.formatType(e,a.getAttribute("type"));f.push({type:l,url:e})}else for(g=0;g<a.childNodes.length;g++){h=a.childNodes[g];if(h.nodeType==1&&h.tagName.toLowerCase()=="source"){e=h.getAttribute("src");
l=this.formatType(e,h.getAttribute("type"));h=h.getAttribute("media");if(!h||!window.matchMedia||window.matchMedia&&window.matchMedia(h).matches)f.push({type:l,url:e})}}if(!d&&f.length>0&&f[0].url!==null&&this.getTypeFromFile(f[0].url).indexOf("audio")>-1)j.isVideo=false;if(mejs.MediaFeatures.isBustedAndroid)a.canPlayType=function(m){return m.match(/video\/(mp4|m4v)/gi)!==null?"maybe":""};if(c&&(b.mode==="auto"||b.mode==="auto_plugin"||b.mode==="native")&&!(mejs.MediaFeatures.isBustedNativeHTTPS&&
b.httpsBasicAuthSite===true)){if(!d){g=document.createElement(j.isVideo?"video":"audio");a.parentNode.insertBefore(g,a);a.style.display="none";j.htmlMediaElement=a=g}for(g=0;g<f.length;g++)if(a.canPlayType(f[g].type).replace(/no/,"")!==""||a.canPlayType(f[g].type.replace(/mp3/,"mpeg")).replace(/no/,"")!==""){j.method="native";j.url=f[g].url;break}if(j.method==="native"){if(j.url!==null)a.src=j.url;if(b.mode!=="auto_plugin")return j}}if(b.mode==="auto"||b.mode==="auto_plugin"||b.mode==="shim")for(g=
0;g<f.length;g++){l=f[g].type;for(a=0;a<b.plugins.length;a++){e=b.plugins[a];h=mejs.plugins[e];for(c=0;c<h.length;c++){k=h[c];if(k.version==null||mejs.PluginDetector.hasPluginVersion(e,k.version))for(d=0;d<k.types.length;d++)if(l==k.types[d]){j.method=e;j.url=f[g].url;return j}}}}if(b.mode==="auto_plugin"&&j.method==="native")return j;if(j.method===""&&f.length>0)j.url=f[0].url;return j},formatType:function(a,b){return a&&!b?this.getTypeFromFile(a):b&&~b.indexOf(";")?b.substr(0,b.indexOf(";")):b},
getTypeFromFile:function(a){a=a.split("?")[0];a=a.substring(a.lastIndexOf(".")+1).toLowerCase();return(/(mp4|m4v|ogg|ogv|webm|webmv|flv|wmv|mpeg|mov)/gi.test(a)?"video":"audio")+"/"+this.getTypeFromExtension(a)},getTypeFromExtension:function(a){switch(a){case "mp4":case "m4v":return"mp4";case "webm":case "webma":case "webmv":return"webm";case "ogg":case "oga":case "ogv":return"ogg";default:return a}},createErrorMessage:function(a,b,c){var d=a.htmlMediaElement,e=document.createElement("div");e.className=
"me-cannotplay";try{e.style.width=d.width+"px";e.style.height=d.height+"px"}catch(f){}e.innerHTML=b.customError?b.customError:c!==""?'<a href="'+a.url+'"><img src="'+c+'" width="100%" height="100%" /></a>':'<a href="'+a.url+'"><span>'+mejs.i18n.t("Download File")+"</span></a>";d.parentNode.insertBefore(e,d);d.style.display="none";b.error(d)},createPlugin:function(a,b,c,d,e,f){c=a.htmlMediaElement;var g=1,h=1,l="me_"+a.method+"_"+mejs.meIndex++,j=new mejs.PluginMediaElement(l,a.method,a.url),k=document.createElement("div"),
m;j.tagName=c.tagName;for(m=0;m<c.attributes.length;m++){var n=c.attributes[m];n.specified==true&&j.setAttribute(n.name,n.value)}for(m=c.parentNode;m!==null&&m.tagName.toLowerCase()!="body";){if(m.parentNode.tagName.toLowerCase()=="p"){m.parentNode.parentNode.insertBefore(m,m.parentNode);break}m=m.parentNode}if(a.isVideo){g=b.pluginWidth>0?b.pluginWidth:b.videoWidth>0?b.videoWidth:c.getAttribute("width")!==null?c.getAttribute("width"):b.defaultVideoWidth;h=b.pluginHeight>0?b.pluginHeight:b.videoHeight>
0?b.videoHeight:c.getAttribute("height")!==null?c.getAttribute("height"):b.defaultVideoHeight;g=mejs.Utility.encodeUrl(g);h=mejs.Utility.encodeUrl(h)}else if(b.enablePluginDebug){g=320;h=240}j.success=b.success;mejs.MediaPluginBridge.registerPluginElement(l,j,c);k.className="me-plugin";k.id=l+"_container";a.isVideo?c.parentNode.insertBefore(k,c):document.body.insertBefore(k,document.body.childNodes[0]);d=["id="+l,"isvideo="+(a.isVideo?"true":"false"),"autoplay="+(d?"true":"false"),"preload="+e,"width="+
g,"startvolume="+b.startVolume,"timerrate="+b.timerRate,"flashstreamer="+b.flashStreamer,"height="+h,"pseudostreamstart="+b.pseudoStreamingStartQueryParam];if(a.url!==null)a.method=="flash"?d.push("file="+mejs.Utility.encodeUrl(a.url)):d.push("file="+a.url);b.enablePluginDebug&&d.push("debug=true");b.enablePluginSmoothing&&d.push("smoothing=true");b.enablePseudoStreaming&&d.push("pseudostreaming=true");f&&d.push("controls=true");if(b.pluginVars)d=d.concat(b.pluginVars);switch(a.method){case "silverlight":k.innerHTML=
'<object data="data:application/x-silverlight-2," type="application/x-silverlight-2" id="'+l+'" name="'+l+'" width="'+g+'" height="'+h+'" class="mejs-shim"><param name="initParams" value="'+d.join(",")+'" /><param name="windowless" value="true" /><param name="background" value="black" /><param name="minRuntimeVersion" value="3.0.0.0" /><param name="autoUpgrade" value="true" /><param name="source" value="'+b.pluginPath+b.silverlightName+'" /></object>';break;case "flash":if(mejs.MediaFeatures.isIE){a=
document.createElement("div");k.appendChild(a);a.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="'+l+'" width="'+g+'" height="'+h+'" class="mejs-shim"><param name="movie" value="'+b.pluginPath+b.flashName+"?x="+new Date+'" /><param name="flashvars" value="'+d.join("&amp;")+'" /><param name="quality" value="high" /><param name="bgcolor" value="#000000" /><param name="wmode" value="transparent" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /></object>'}else k.innerHTML=
'<embed id="'+l+'" name="'+l+'" play="true" loop="false" quality="high" bgcolor="#000000" wmode="transparent" allowScriptAccess="always" allowFullScreen="true" type="application/x-shockwave-flash" pluginspage="//www.macromedia.com/go/getflashplayer" src="'+b.pluginPath+b.flashName+'" flashvars="'+d.join("&")+'" width="'+g+'" height="'+h+'" class="mejs-shim"></embed>';break;case "youtube":b=a.url.substr(a.url.lastIndexOf("=")+1);youtubeSettings={container:k,containerId:k.id,pluginMediaElement:j,pluginId:l,
videoId:b,height:h,width:g};mejs.PluginDetector.hasPluginVersion("flash",[10,0,0])?mejs.YouTubeApi.createFlash(youtubeSettings):mejs.YouTubeApi.enqueueIframe(youtubeSettings);break;case "vimeo":j.vimeoid=a.url.substr(a.url.lastIndexOf("/")+1);k.innerHTML='<iframe src="http://player.vimeo.com/video/'+j.vimeoid+'?portrait=0&byline=0&title=0" width="'+g+'" height="'+h+'" frameborder="0" class="mejs-shim"></iframe>'}c.style.display="none";c.removeAttribute("autoplay");return j},updateNative:function(a,
b){var c=a.htmlMediaElement,d;for(d in mejs.HtmlMediaElement)c[d]=mejs.HtmlMediaElement[d];b.success(c,c);return c}};
mejs.YouTubeApi={isIframeStarted:false,isIframeLoaded:false,loadIframeApi:function(){if(!this.isIframeStarted){var a=document.createElement("script");a.src="//www.youtube.com/player_api";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b);this.isIframeStarted=true}},iframeQueue:[],enqueueIframe:function(a){if(this.isLoaded)this.createIframe(a);else{this.loadIframeApi();this.iframeQueue.push(a)}},createIframe:function(a){var b=a.pluginMediaElement,c=new YT.Player(a.containerId,
{height:a.height,width:a.width,videoId:a.videoId,playerVars:{controls:0},events:{onReady:function(){a.pluginMediaElement.pluginApi=c;mejs.MediaPluginBridge.initPlugin(a.pluginId);setInterval(function(){mejs.YouTubeApi.createEvent(c,b,"timeupdate")},250)},onStateChange:function(d){mejs.YouTubeApi.handleStateChange(d.data,c,b)}}})},createEvent:function(a,b,c){c={type:c,target:b};if(a&&a.getDuration){b.currentTime=c.currentTime=a.getCurrentTime();b.duration=c.duration=a.getDuration();c.paused=b.paused;
c.ended=b.ended;c.muted=a.isMuted();c.volume=a.getVolume()/100;c.bytesTotal=a.getVideoBytesTotal();c.bufferedBytes=a.getVideoBytesLoaded();var d=c.bufferedBytes/c.bytesTotal*c.duration;c.target.buffered=c.buffered={start:function(){return 0},end:function(){return d},length:1}}b.dispatchEvent(c.type,c)},iFrameReady:function(){for(this.isIframeLoaded=this.isLoaded=true;this.iframeQueue.length>0;)this.createIframe(this.iframeQueue.pop())},flashPlayers:{},createFlash:function(a){this.flashPlayers[a.pluginId]=
a;var b,c="//www.youtube.com/apiplayer?enablejsapi=1&amp;playerapiid="+a.pluginId+"&amp;version=3&amp;autoplay=0&amp;controls=0&amp;modestbranding=1&loop=0";if(mejs.MediaFeatures.isIE){b=document.createElement("div");a.container.appendChild(b);b.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="'+a.pluginId+'" width="'+a.width+'" height="'+a.height+'" class="mejs-shim"><param name="movie" value="'+
c+'" /><param name="wmode" value="transparent" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /></object>'}else a.container.innerHTML='<object type="application/x-shockwave-flash" id="'+a.pluginId+'" data="'+c+'" width="'+a.width+'" height="'+a.height+'" style="visibility: visible; " class="mejs-shim"><param name="allowScriptAccess" value="always"><param name="wmode" value="transparent"></object>'},flashReady:function(a){var b=this.flashPlayers[a],c=
document.getElementById(a),d=b.pluginMediaElement;d.pluginApi=d.pluginElement=c;mejs.MediaPluginBridge.initPlugin(a);c.cueVideoById(b.videoId);a=b.containerId+"_callback";window[a]=function(e){mejs.YouTubeApi.handleStateChange(e,c,d)};c.addEventListener("onStateChange",a);setInterval(function(){mejs.YouTubeApi.createEvent(c,d,"timeupdate")},250)},handleStateChange:function(a,b,c){switch(a){case -1:c.paused=true;c.ended=true;mejs.YouTubeApi.createEvent(b,c,"loadedmetadata");break;case 0:c.paused=false;
c.ended=true;mejs.YouTubeApi.createEvent(b,c,"ended");break;case 1:c.paused=false;c.ended=false;mejs.YouTubeApi.createEvent(b,c,"play");mejs.YouTubeApi.createEvent(b,c,"playing");break;case 2:c.paused=true;c.ended=false;mejs.YouTubeApi.createEvent(b,c,"pause");break;case 3:mejs.YouTubeApi.createEvent(b,c,"progress")}}};function onYouTubePlayerAPIReady(){mejs.YouTubeApi.iFrameReady()}function onYouTubePlayerReady(a){mejs.YouTubeApi.flashReady(a)}window.mejs=mejs;window.MediaElement=mejs.MediaElement;
(function(a,b){var c={locale:{language:"",strings:{}},methods:{}};c.locale.getLanguage=function(){return c.locale.language||navigator.language};if(typeof mejsL10n!="undefined")c.locale.language=mejsL10n.language;c.locale.INIT_LANGUAGE=c.locale.getLanguage();c.methods.checkPlain=function(d){var e,f,g={"&":"&amp;",'"':"&quot;","<":"&lt;",">":"&gt;"};d=String(d);for(e in g)if(g.hasOwnProperty(e)){f=RegExp(e,"g");d=d.replace(f,g[e])}return d};c.methods.formatString=function(d,e){for(var f in e){switch(f.charAt(0)){case "@":e[f]=
c.methods.checkPlain(e[f]);break;case "!":break;default:e[f]='<em class="placeholder">'+c.methods.checkPlain(e[f])+"</em>"}d=d.replace(f,e[f])}return d};c.methods.t=function(d,e,f){if(c.locale.strings&&c.locale.strings[f.context]&&c.locale.strings[f.context][d])d=c.locale.strings[f.context][d];if(e)d=c.methods.formatString(d,e);return d};c.t=function(d,e,f){if(typeof d==="string"&&d.length>0){var g=c.locale.getLanguage();f=f||{context:g};return c.methods.t(d,e,f)}else throw{name:"InvalidArgumentException",
message:"First argument is either not a string or empty."};};b.i18n=c})(document,mejs);(function(a){if(typeof mejsL10n!="undefined")a[mejsL10n.language]=mejsL10n.strings})(mejs.i18n.locale.strings);(function(a){a.de={Fullscreen:"Vollbild","Go Fullscreen":"Vollbild an","Turn off Fullscreen":"Vollbild aus",Close:"Schlie\u00dfen"}})(mejs.i18n.locale.strings);
(function(a){a.zh={Fullscreen:"\u5168\u87a2\u5e55","Go Fullscreen":"\u5168\u5c4f\u6a21\u5f0f","Turn off Fullscreen":"\u9000\u51fa\u5168\u5c4f\u6a21\u5f0f",Close:"\u95dc\u9589"}})(mejs.i18n.locale.strings);

/*!
 * MediaElementPlayer
 * http://mediaelementjs.com/
 *
 * Creates a controller bar for HTML5 <video> add <audio> tags
 * using jQuery and MediaElement.js (HTML5 Flash/Silverlight wrapper)
 *
 * Copyright 2010-2013, John Dyer (http://j.hn/)
 * License: MIT
 *
 */if(typeof jQuery!="undefined")mejs.$=jQuery;else if(typeof ender!="undefined")mejs.$=ender;
(function(f){mejs.MepDefaults={poster:"",showPosterWhenEnded:false,defaultVideoWidth:480,defaultVideoHeight:270,videoWidth:-1,videoHeight:-1,defaultAudioWidth:400,defaultAudioHeight:30,defaultSeekBackwardInterval:function(a){return a.duration*0.05},defaultSeekForwardInterval:function(a){return a.duration*0.05},audioWidth:-1,audioHeight:-1,startVolume:0.8,loop:false,autoRewind:true,enableAutosize:true,alwaysShowHours:false,showTimecodeFrameCount:false,framesPerSecond:25,autosizeProgress:true,alwaysShowControls:false,
hideVideoControlsOnLoad:false,clickToPlayPause:true,iPadUseNativeControls:false,iPhoneUseNativeControls:false,AndroidUseNativeControls:false,features:["playpause","current","progress","duration","tracks","volume","fullscreen"],isVideo:true,enableKeyboard:true,pauseOtherPlayers:true,keyActions:[{keys:[32,179],action:function(a,b){b.paused||b.ended?b.play():b.pause()}},{keys:[38],action:function(a,b){b.setVolume(Math.min(b.volume+0.1,1))}},{keys:[40],action:function(a,b){b.setVolume(Math.max(b.volume-
0.1,0))}},{keys:[37,227],action:function(a,b){if(!isNaN(b.duration)&&b.duration>0){if(a.isVideo){a.showControls();a.startControlsTimer()}var c=Math.max(b.currentTime-a.options.defaultSeekBackwardInterval(b),0);b.setCurrentTime(c)}}},{keys:[39,228],action:function(a,b){if(!isNaN(b.duration)&&b.duration>0){if(a.isVideo){a.showControls();a.startControlsTimer()}var c=Math.min(b.currentTime+a.options.defaultSeekForwardInterval(b),b.duration);b.setCurrentTime(c)}}},{keys:[70],action:function(a){if(typeof a.enterFullScreen!=
"undefined")a.isFullScreen?a.exitFullScreen():a.enterFullScreen()}}]};mejs.mepIndex=0;mejs.players={};mejs.MediaElementPlayer=function(a,b){if(!(this instanceof mejs.MediaElementPlayer))return new mejs.MediaElementPlayer(a,b);this.$media=this.$node=f(a);this.node=this.media=this.$media[0];if(typeof this.node.player!="undefined")return this.node.player;else this.node.player=this;if(typeof b=="undefined")b=this.$node.data("mejsoptions");this.options=f.extend({},mejs.MepDefaults,b);this.id="mep_"+mejs.mepIndex++;
mejs.players[this.id]=this;this.init();return this};mejs.MediaElementPlayer.prototype={hasFocus:false,controlsAreVisible:true,init:function(){var a=this,b=mejs.MediaFeatures,c=f.extend(true,{},a.options,{success:function(d,g){a.meReady(d,g)},error:function(d){a.handleError(d)}}),e=a.media.tagName.toLowerCase();a.isDynamic=e!=="audio"&&e!=="video";a.isVideo=a.isDynamic?a.options.isVideo:e!=="audio"&&a.options.isVideo;if(b.isiPad&&a.options.iPadUseNativeControls||b.isiPhone&&a.options.iPhoneUseNativeControls){a.$media.attr("controls",
"controls");if(b.isiPad&&a.media.getAttribute("autoplay")!==null){a.media.load();a.media.play()}}else if(!(b.isAndroid&&a.options.AndroidUseNativeControls)){a.$media.removeAttr("controls");a.container=f('<div id="'+a.id+'" class="mejs-container '+(mejs.MediaFeatures.svg?"svg":"no-svg")+'"><div class="mejs-inner"><div class="mejs-mediaelement"></div><div class="mejs-layers"></div><div class="mejs-controls"></div><div class="mejs-clear"></div></div></div>').addClass(a.$media[0].className).insertBefore(a.$media);
a.container.addClass((b.isAndroid?"mejs-android ":"")+(b.isiOS?"mejs-ios ":"")+(b.isiPad?"mejs-ipad ":"")+(b.isiPhone?"mejs-iphone ":"")+(a.isVideo?"mejs-video ":"mejs-audio "));if(b.isiOS){b=a.$media.clone();a.container.find(".mejs-mediaelement").append(b);a.$media.remove();a.$node=a.$media=b;a.node=a.media=b[0]}else a.container.find(".mejs-mediaelement").append(a.$media);a.controls=a.container.find(".mejs-controls");a.layers=a.container.find(".mejs-layers");b=a.isVideo?"video":"audio";e=b.substring(0,
1).toUpperCase()+b.substring(1);a.width=a.options[b+"Width"]>0||a.options[b+"Width"].toString().indexOf("%")>-1?a.options[b+"Width"]:a.media.style.width!==""&&a.media.style.width!==null?a.media.style.width:a.media.getAttribute("width")!==null?a.$media.attr("width"):a.options["default"+e+"Width"];a.height=a.options[b+"Height"]>0||a.options[b+"Height"].toString().indexOf("%")>-1?a.options[b+"Height"]:a.media.style.height!==""&&a.media.style.height!==null?a.media.style.height:a.$media[0].getAttribute("height")!==
null?a.$media.attr("height"):a.options["default"+e+"Height"];a.setPlayerSize(a.width,a.height);c.pluginWidth=a.width;c.pluginHeight=a.height}mejs.MediaElement(a.$media[0],c);typeof a.container!="undefined"&&a.controlsAreVisible&&a.container.trigger("controlsshown")},showControls:function(a){var b=this;a=typeof a=="undefined"||a;if(!b.controlsAreVisible){if(a){b.controls.css("visibility","visible").stop(true,true).fadeIn(200,function(){b.controlsAreVisible=true;b.container.trigger("controlsshown")});
b.container.find(".mejs-control").css("visibility","visible").stop(true,true).fadeIn(200,function(){b.controlsAreVisible=true})}else{b.controls.css("visibility","visible").css("display","block");b.container.find(".mejs-control").css("visibility","visible").css("display","block");b.controlsAreVisible=true;b.container.trigger("controlsshown")}b.setControlsSize()}},hideControls:function(a){var b=this;a=typeof a=="undefined"||a;if(!(!b.controlsAreVisible||b.options.alwaysShowControls))if(a){b.controls.stop(true,
true).fadeOut(200,function(){f(this).css("visibility","hidden").css("display","block");b.controlsAreVisible=false;b.container.trigger("controlshidden")});b.container.find(".mejs-control").stop(true,true).fadeOut(200,function(){f(this).css("visibility","hidden").css("display","block")})}else{b.controls.css("visibility","hidden").css("display","block");b.container.find(".mejs-control").css("visibility","hidden").css("display","block");b.controlsAreVisible=false;b.container.trigger("controlshidden")}},
controlsTimer:null,startControlsTimer:function(a){var b=this;a=typeof a!="undefined"?a:1500;b.killControlsTimer("start");b.controlsTimer=setTimeout(function(){b.hideControls();b.killControlsTimer("hide")},a)},killControlsTimer:function(){if(this.controlsTimer!==null){clearTimeout(this.controlsTimer);delete this.controlsTimer;this.controlsTimer=null}},controlsEnabled:true,disableControls:function(){this.killControlsTimer();this.hideControls(false);this.controlsEnabled=false},enableControls:function(){this.showControls(false);
this.controlsEnabled=true},meReady:function(a,b){var c=this,e=mejs.MediaFeatures,d=b.getAttribute("autoplay");d=!(typeof d=="undefined"||d===null||d==="false");var g;if(!c.created){c.created=true;c.media=a;c.domNode=b;if(!(e.isAndroid&&c.options.AndroidUseNativeControls)&&!(e.isiPad&&c.options.iPadUseNativeControls)&&!(e.isiPhone&&c.options.iPhoneUseNativeControls)){c.buildposter(c,c.controls,c.layers,c.media);c.buildkeyboard(c,c.controls,c.layers,c.media);c.buildoverlays(c,c.controls,c.layers,c.media);
c.findTracks();for(g in c.options.features){e=c.options.features[g];if(c["build"+e])try{c["build"+e](c,c.controls,c.layers,c.media)}catch(k){}}c.container.trigger("controlsready");c.setPlayerSize(c.width,c.height);c.setControlsSize();if(c.isVideo){if(mejs.MediaFeatures.hasTouch)c.$media.bind("touchstart",function(){if(c.controlsAreVisible)c.hideControls(false);else c.controlsEnabled&&c.showControls(false)});else{mejs.MediaElementPlayer.prototype.clickToPlayPauseCallback=function(){if(c.options.clickToPlayPause)c.media.paused?
c.media.play():c.media.pause()};c.media.addEventListener("click",c.clickToPlayPauseCallback,false);c.container.bind("mouseenter mouseover",function(){if(c.controlsEnabled)if(!c.options.alwaysShowControls){c.killControlsTimer("enter");c.showControls();c.startControlsTimer(2500)}}).bind("mousemove",function(){if(c.controlsEnabled){c.controlsAreVisible||c.showControls();c.options.alwaysShowControls||c.startControlsTimer(2500)}}).bind("mouseleave",function(){c.controlsEnabled&&!c.media.paused&&!c.options.alwaysShowControls&&
c.startControlsTimer(1E3)})}c.options.hideVideoControlsOnLoad&&c.hideControls(false);d&&!c.options.alwaysShowControls&&c.hideControls();c.options.enableAutosize&&c.media.addEventListener("loadedmetadata",function(j){if(c.options.videoHeight<=0&&c.domNode.getAttribute("height")===null&&!isNaN(j.target.videoHeight)){c.setPlayerSize(j.target.videoWidth,j.target.videoHeight);c.setControlsSize();c.media.setVideoSize(j.target.videoWidth,j.target.videoHeight)}},false)}a.addEventListener("play",function(){for(var j in mejs.players){var m=
mejs.players[j];m.id!=c.id&&c.options.pauseOtherPlayers&&!m.paused&&!m.ended&&m.pause();m.hasFocus=false}c.hasFocus=true},false);c.media.addEventListener("ended",function(){if(c.options.autoRewind)try{c.media.setCurrentTime(0)}catch(j){}c.media.pause();c.setProgressRail&&c.setProgressRail();c.setCurrentRail&&c.setCurrentRail();if(c.options.loop)c.media.play();else!c.options.alwaysShowControls&&c.controlsEnabled&&c.showControls()},false);c.media.addEventListener("loadedmetadata",function(){c.updateDuration&&
c.updateDuration();c.updateCurrent&&c.updateCurrent();if(!c.isFullScreen){c.setPlayerSize(c.width,c.height);c.setControlsSize()}},false);setTimeout(function(){c.setPlayerSize(c.width,c.height);c.setControlsSize()},50);c.globalBind("resize",function(){c.isFullScreen||mejs.MediaFeatures.hasTrueNativeFullScreen&&document.webkitIsFullScreen||c.setPlayerSize(c.width,c.height);c.setControlsSize()});c.media.pluginType=="youtube"&&c.container.find(".mejs-overlay-play").hide()}if(d&&a.pluginType=="native"){a.load();
a.play()}if(c.options.success)typeof c.options.success=="string"?window[c.options.success](c.media,c.domNode,c):c.options.success(c.media,c.domNode,c)}},handleError:function(a){this.controls.hide();this.options.error&&this.options.error(a)},setPlayerSize:function(a,b){if(typeof a!="undefined")this.width=a;if(typeof b!="undefined")this.height=b;if(this.height.toString().indexOf("%")>0||this.$node.css("max-width")==="100%"||parseInt(this.$node.css("max-width").replace(/px/,""),10)/this.$node.offsetParent().width()===
1||this.$node[0].currentStyle&&this.$node[0].currentStyle.maxWidth==="100%"){var c=this.isVideo?this.media.videoWidth&&this.media.videoWidth>0?this.media.videoWidth:this.options.defaultVideoWidth:this.options.defaultAudioWidth,e=this.isVideo?this.media.videoHeight&&this.media.videoHeight>0?this.media.videoHeight:this.options.defaultVideoHeight:this.options.defaultAudioHeight,d=this.container.parent().closest(":visible").width();c=this.isVideo||!this.options.autosizeProgress?parseInt(d*e/c,10):e;if(this.container.parent()[0].tagName.toLowerCase()===
"body"){d=f(window).width();c=f(window).height()}if(c!=0&&d!=0){this.container.width(d).height(c);this.$media.add(this.container.find(".mejs-shim")).width("100%").height("100%");this.isVideo&&this.media.setVideoSize&&this.media.setVideoSize(d,c);this.layers.children(".mejs-layer").width("100%").height("100%")}}else{this.container.width(this.width).height(this.height);this.layers.children(".mejs-layer").width(this.width).height(this.height)}d=this.layers.find(".mejs-overlay-play");c=d.find(".mejs-overlay-button");
d.height(this.container.height()-this.controls.height());c.css("margin-top","-"+(c.height()/2-this.controls.height()/2).toString()+"px")},setControlsSize:function(){var a=0,b=0,c=this.controls.find(".mejs-time-rail"),e=this.controls.find(".mejs-time-total");this.controls.find(".mejs-time-current");this.controls.find(".mejs-time-loaded");var d=c.siblings();if(this.options&&!this.options.autosizeProgress)b=parseInt(c.css("width"));if(b===0||!b){d.each(function(){var g=f(this);if(g.css("position")!=
"absolute"&&g.is(":visible"))a+=f(this).outerWidth(true)});b=this.controls.width()-a-(c.outerWidth(true)-c.width())}c.width(b);e.width(b-(e.outerWidth(true)-e.width()));this.setProgressRail&&this.setProgressRail();this.setCurrentRail&&this.setCurrentRail()},buildposter:function(a,b,c,e){var d=f('<div class="mejs-poster mejs-layer"></div>').appendTo(c);b=a.$media.attr("poster");if(a.options.poster!=="")b=a.options.poster;b!==""&&b!=null?this.setPoster(b):d.hide();e.addEventListener("play",function(){d.hide()},
false);a.options.showPosterWhenEnded&&a.options.autoRewind&&e.addEventListener("ended",function(){d.show()},false)},setPoster:function(a){var b=this.container.find(".mejs-poster"),c=b.find("img");if(c.length==0)c=f('<img width="100%" height="100%" />').appendTo(b);c.attr("src",a);b.css({"background-image":"url("+a+")"})},buildoverlays:function(a,b,c,e){var d=this;if(a.isVideo){var g=f('<div class="mejs-overlay mejs-layer"><div class="mejs-overlay-loading"><span></span></div></div>').hide().appendTo(c),
k=f('<div class="mejs-overlay mejs-layer"><div class="mejs-overlay-error"></div></div>').hide().appendTo(c),j=f('<div class="mejs-overlay mejs-layer mejs-overlay-play"><div class="mejs-overlay-button"></div></div>').appendTo(c).click(function(){if(d.options.clickToPlayPause)e.paused?e.play():e.pause()});e.addEventListener("play",function(){j.hide();g.hide();b.find(".mejs-time-buffering").hide();k.hide()},false);e.addEventListener("playing",function(){j.hide();g.hide();b.find(".mejs-time-buffering").hide();
k.hide()},false);e.addEventListener("seeking",function(){g.show();b.find(".mejs-time-buffering").show()},false);e.addEventListener("seeked",function(){g.hide();b.find(".mejs-time-buffering").hide()},false);e.addEventListener("pause",function(){mejs.MediaFeatures.isiPhone||j.show()},false);e.addEventListener("waiting",function(){g.show();b.find(".mejs-time-buffering").show()},false);e.addEventListener("loadeddata",function(){g.show();b.find(".mejs-time-buffering").show()},false);e.addEventListener("canplay",
function(){g.hide();b.find(".mejs-time-buffering").hide()},false);e.addEventListener("error",function(){g.hide();b.find(".mejs-time-buffering").hide();k.show();k.find("mejs-overlay-error").html("Error loading this resource")},false)}},buildkeyboard:function(a,b,c,e){this.globalBind("keydown",function(d){if(a.hasFocus&&a.options.enableKeyboard)for(var g=0,k=a.options.keyActions.length;g<k;g++)for(var j=a.options.keyActions[g],m=0,q=j.keys.length;m<q;m++)if(d.keyCode==j.keys[m]){d.preventDefault();
j.action(a,e,d.keyCode);return false}return true});this.globalBind("click",function(d){if(f(d.target).closest(".mejs-container").length==0)a.hasFocus=false})},findTracks:function(){var a=this,b=a.$media.find("track");a.tracks=[];b.each(function(c,e){e=f(e);a.tracks.push({srclang:e.attr("srclang")?e.attr("srclang").toLowerCase():"",src:e.attr("src"),kind:e.attr("kind"),label:e.attr("label")||"",entries:[],isLoaded:false})})},changeSkin:function(a){this.container[0].className="mejs-container "+a;this.setPlayerSize(this.width,
this.height);this.setControlsSize()},play:function(){this.media.play()},pause:function(){try{this.media.pause()}catch(a){}},load:function(){this.media.load()},setMuted:function(a){this.media.setMuted(a)},setCurrentTime:function(a){this.media.setCurrentTime(a)},getCurrentTime:function(){return this.media.currentTime},setVolume:function(a){this.media.setVolume(a)},getVolume:function(){return this.media.volume},setSrc:function(a){this.media.setSrc(a)},remove:function(){var a,b;for(a in this.options.features){b=
this.options.features[a];if(this["clean"+b])try{this["clean"+b](this)}catch(c){}}if(this.isDynamic)this.$node.insertBefore(this.container);else{this.$media.prop("controls",true);this.$node.clone().show().insertBefore(this.container);this.$node.remove()}this.media.pluginType!=="native"&&this.media.remove();delete mejs.players[this.id];this.container.remove();this.globalUnbind();delete this.node.player}};(function(){function a(c,e){var d={d:[],w:[]};f.each((c||"").split(" "),function(g,k){var j=k+"."+
e;if(j.indexOf(".")===0){d.d.push(j);d.w.push(j)}else d[b.test(k)?"w":"d"].push(j)});d.d=d.d.join(" ");d.w=d.w.join(" ");return d}var b=/^((after|before)print|(before)?unload|hashchange|message|o(ff|n)line|page(hide|show)|popstate|resize|storage)\b/;mejs.MediaElementPlayer.prototype.globalBind=function(c,e,d){c=a(c,this.id);c.d&&f(document).bind(c.d,e,d);c.w&&f(window).bind(c.w,e,d)};mejs.MediaElementPlayer.prototype.globalUnbind=function(c,e){c=a(c,this.id);c.d&&f(document).unbind(c.d,e);c.w&&f(window).unbind(c.w,
e)}})();if(typeof jQuery!="undefined")jQuery.fn.mediaelementplayer=function(a){a===false?this.each(function(){var b=jQuery(this).data("mediaelementplayer");b&&b.remove();jQuery(this).removeData("mediaelementplayer")}):this.each(function(){jQuery(this).data("mediaelementplayer",new mejs.MediaElementPlayer(this,a))});return this};f(document).ready(function(){f(".mejs-player").mediaelementplayer()});window.MediaElementPlayer=mejs.MediaElementPlayer})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{playpauseText:mejs.i18n.t("Play/Pause")});f.extend(MediaElementPlayer.prototype,{buildplaypause:function(a,b,c,e){var d=f('<div class="mejs-button mejs-playpause-button mejs-play" ><button type="button" aria-controls="'+this.id+'" title="'+this.options.playpauseText+'" aria-label="'+this.options.playpauseText+'"></button></div>').appendTo(b).click(function(g){g.preventDefault();e.paused?e.play():e.pause();return false});e.addEventListener("play",function(){d.removeClass("mejs-play").addClass("mejs-pause")},
false);e.addEventListener("playing",function(){d.removeClass("mejs-play").addClass("mejs-pause")},false);e.addEventListener("pause",function(){d.removeClass("mejs-pause").addClass("mejs-play")},false);e.addEventListener("paused",function(){d.removeClass("mejs-pause").addClass("mejs-play")},false)}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{stopText:"Stop"});f.extend(MediaElementPlayer.prototype,{buildstop:function(a,b,c,e){f('<div class="mejs-button mejs-stop-button mejs-stop"><button type="button" aria-controls="'+this.id+'" title="'+this.options.stopText+'" aria-label="'+this.options.stopText+'"></button></div>').appendTo(b).click(function(){e.paused||e.pause();if(e.currentTime>0){e.setCurrentTime(0);e.pause();b.find(".mejs-time-current").width("0px");b.find(".mejs-time-handle").css("left",
"0px");b.find(".mejs-time-float-current").html(mejs.Utility.secondsToTimeCode(0));b.find(".mejs-currenttime").html(mejs.Utility.secondsToTimeCode(0));c.find(".mejs-poster").show()}})}})})(mejs.$);
(function(f){f.extend(MediaElementPlayer.prototype,{buildprogress:function(a,b,c,e){f('<div class="mejs-time-rail"><span class="mejs-time-total"><span class="mejs-time-buffering"></span><span class="mejs-time-loaded"></span><span class="mejs-time-current"></span><span class="mejs-time-handle"></span><span class="mejs-time-float"><span class="mejs-time-float-current">00:00</span><span class="mejs-time-float-corner"></span></span></span></div>').appendTo(b);b.find(".mejs-time-buffering").hide();var d=
this,g=b.find(".mejs-time-total");c=b.find(".mejs-time-loaded");var k=b.find(".mejs-time-current"),j=b.find(".mejs-time-handle"),m=b.find(".mejs-time-float"),q=b.find(".mejs-time-float-current"),p=function(h){h=h.pageX;var l=g.offset(),r=g.outerWidth(true),n=0,o=n=0;if(e.duration){if(h<l.left)h=l.left;else if(h>r+l.left)h=r+l.left;o=h-l.left;n=o/r;n=n<=0.02?0:n*e.duration;t&&n!==e.currentTime&&e.setCurrentTime(n);if(!mejs.MediaFeatures.hasTouch){m.css("left",o);q.html(mejs.Utility.secondsToTimeCode(n));
m.show()}}},t=false;g.bind("mousedown",function(h){if(h.which===1){t=true;p(h);d.globalBind("mousemove.dur",function(l){p(l)});d.globalBind("mouseup.dur",function(){t=false;m.hide();d.globalUnbind(".dur")});return false}}).bind("mouseenter",function(){d.globalBind("mousemove.dur",function(h){p(h)});mejs.MediaFeatures.hasTouch||m.show()}).bind("mouseleave",function(){if(!t){d.globalUnbind(".dur");m.hide()}});e.addEventListener("progress",function(h){a.setProgressRail(h);a.setCurrentRail(h)},false);
e.addEventListener("timeupdate",function(h){a.setProgressRail(h);a.setCurrentRail(h)},false);d.loaded=c;d.total=g;d.current=k;d.handle=j},setProgressRail:function(a){var b=a!=undefined?a.target:this.media,c=null;if(b&&b.buffered&&b.buffered.length>0&&b.buffered.end&&b.duration)c=b.buffered.end(0)/b.duration;else if(b&&b.bytesTotal!=undefined&&b.bytesTotal>0&&b.bufferedBytes!=undefined)c=b.bufferedBytes/b.bytesTotal;else if(a&&a.lengthComputable&&a.total!=0)c=a.loaded/a.total;if(c!==null){c=Math.min(1,
Math.max(0,c));this.loaded&&this.total&&this.loaded.width(this.total.width()*c)}},setCurrentRail:function(){if(this.media.currentTime!=undefined&&this.media.duration)if(this.total&&this.handle){var a=Math.round(this.total.width()*this.media.currentTime/this.media.duration),b=a-Math.round(this.handle.outerWidth(true)/2);this.current.width(a);this.handle.css("left",b)}}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{duration:-1,timeAndDurationSeparator:"<span> | </span>"});f.extend(MediaElementPlayer.prototype,{buildcurrent:function(a,b,c,e){f('<div class="mejs-time"><span class="mejs-currenttime">'+(a.options.alwaysShowHours?"00:":"")+(a.options.showTimecodeFrameCount?"00:00:00":"00:00")+"</span></div>").appendTo(b);this.currenttime=this.controls.find(".mejs-currenttime");e.addEventListener("timeupdate",function(){a.updateCurrent()},false)},buildduration:function(a,b,
c,e){if(b.children().last().find(".mejs-currenttime").length>0)f(this.options.timeAndDurationSeparator+'<span class="mejs-duration">'+(this.options.duration>0?mejs.Utility.secondsToTimeCode(this.options.duration,this.options.alwaysShowHours||this.media.duration>3600,this.options.showTimecodeFrameCount,this.options.framesPerSecond||25):(a.options.alwaysShowHours?"00:":"")+(a.options.showTimecodeFrameCount?"00:00:00":"00:00"))+"</span>").appendTo(b.find(".mejs-time"));else{b.find(".mejs-currenttime").parent().addClass("mejs-currenttime-container");
f('<div class="mejs-time mejs-duration-container"><span class="mejs-duration">'+(this.options.duration>0?mejs.Utility.secondsToTimeCode(this.options.duration,this.options.alwaysShowHours||this.media.duration>3600,this.options.showTimecodeFrameCount,this.options.framesPerSecond||25):(a.options.alwaysShowHours?"00:":"")+(a.options.showTimecodeFrameCount?"00:00:00":"00:00"))+"</span></div>").appendTo(b)}this.durationD=this.controls.find(".mejs-duration");e.addEventListener("timeupdate",function(){a.updateDuration()},
false)},updateCurrent:function(){if(this.currenttime)this.currenttime.html(mejs.Utility.secondsToTimeCode(this.media.currentTime,this.options.alwaysShowHours||this.media.duration>3600,this.options.showTimecodeFrameCount,this.options.framesPerSecond||25))},updateDuration:function(){this.container.toggleClass("mejs-long-video",this.media.duration>3600);if(this.durationD&&(this.options.duration>0||this.media.duration))this.durationD.html(mejs.Utility.secondsToTimeCode(this.options.duration>0?this.options.duration:
this.media.duration,this.options.alwaysShowHours,this.options.showTimecodeFrameCount,this.options.framesPerSecond||25))}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{muteText:mejs.i18n.t("Mute Toggle"),hideVolumeOnTouchDevices:true,audioVolume:"horizontal",videoVolume:"vertical"});f.extend(MediaElementPlayer.prototype,{buildvolume:function(a,b,c,e){if(!(mejs.MediaFeatures.hasTouch&&this.options.hideVolumeOnTouchDevices)){var d=this,g=d.isVideo?d.options.videoVolume:d.options.audioVolume,k=g=="horizontal"?f('<div class="mejs-button mejs-volume-button mejs-mute"><button type="button" aria-controls="'+d.id+'" title="'+d.options.muteText+
'" aria-label="'+d.options.muteText+'"></button></div><div class="mejs-horizontal-volume-slider"><div class="mejs-horizontal-volume-total"></div><div class="mejs-horizontal-volume-current"></div><div class="mejs-horizontal-volume-handle"></div></div>').appendTo(b):f('<div class="mejs-button mejs-volume-button mejs-mute"><button type="button" aria-controls="'+d.id+'" title="'+d.options.muteText+'" aria-label="'+d.options.muteText+'"></button><div class="mejs-volume-slider"><div class="mejs-volume-total"></div><div class="mejs-volume-current"></div><div class="mejs-volume-handle"></div></div></div>').appendTo(b),
j=d.container.find(".mejs-volume-slider, .mejs-horizontal-volume-slider"),m=d.container.find(".mejs-volume-total, .mejs-horizontal-volume-total"),q=d.container.find(".mejs-volume-current, .mejs-horizontal-volume-current"),p=d.container.find(".mejs-volume-handle, .mejs-horizontal-volume-handle"),t=function(n,o){if(!j.is(":visible")&&typeof o=="undefined"){j.show();t(n,true);j.hide()}else{n=Math.max(0,n);n=Math.min(n,1);n==0?k.removeClass("mejs-mute").addClass("mejs-unmute"):k.removeClass("mejs-unmute").addClass("mejs-mute");
if(g=="vertical"){var s=m.height(),u=m.position(),v=s-s*n;p.css("top",Math.round(u.top+v-p.height()/2));q.height(s-v);q.css("top",u.top+v)}else{s=m.width();u=m.position();s=s*n;p.css("left",Math.round(u.left+s-p.width()/2));q.width(Math.round(s))}}},h=function(n){var o=null,s=m.offset();if(g=="vertical"){o=m.height();parseInt(m.css("top").replace(/px/,""),10);o=(o-(n.pageY-s.top))/o;if(s.top==0||s.left==0)return}else{o=m.width();o=(n.pageX-s.left)/o}o=Math.max(0,o);o=Math.min(o,1);t(o);o==0?e.setMuted(true):
e.setMuted(false);e.setVolume(o)},l=false,r=false;k.hover(function(){j.show();r=true},function(){r=false;!l&&g=="vertical"&&j.hide()});j.bind("mouseover",function(){r=true}).bind("mousedown",function(n){h(n);d.globalBind("mousemove.vol",function(o){h(o)});d.globalBind("mouseup.vol",function(){l=false;d.globalUnbind(".vol");!r&&g=="vertical"&&j.hide()});l=true;return false});k.find("button").click(function(){e.setMuted(!e.muted)});e.addEventListener("volumechange",function(){if(!l)if(e.muted){t(0);
k.removeClass("mejs-mute").addClass("mejs-unmute")}else{t(e.volume);k.removeClass("mejs-unmute").addClass("mejs-mute")}},false);if(d.container.is(":visible")){t(a.options.startVolume);a.options.startVolume===0&&e.setMuted(true);e.pluginType==="native"&&e.setVolume(a.options.startVolume)}}}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{usePluginFullScreen:true,newWindowCallback:function(){return""},fullscreenText:mejs.i18n.t("Fullscreen")});f.extend(MediaElementPlayer.prototype,{isFullScreen:false,isNativeFullScreen:false,isInIframe:false,buildfullscreen:function(a,b,c,e){if(a.isVideo){a.isInIframe=window.location!=window.parent.location;if(mejs.MediaFeatures.hasTrueNativeFullScreen){c=function(){if(a.isFullScreen)if(mejs.MediaFeatures.isFullScreen()){a.isNativeFullScreen=true;a.setControlsSize()}else{a.isNativeFullScreen=
false;a.exitFullScreen()}};mejs.MediaFeatures.hasMozNativeFullScreen?a.globalBind(mejs.MediaFeatures.fullScreenEventName,c):a.container.bind(mejs.MediaFeatures.fullScreenEventName,c)}var d=this,g=f('<div class="mejs-button mejs-fullscreen-button"><button type="button" aria-controls="'+d.id+'" title="'+d.options.fullscreenText+'" aria-label="'+d.options.fullscreenText+'"></button></div>').appendTo(b);if(d.media.pluginType==="native"||!d.options.usePluginFullScreen&&!mejs.MediaFeatures.isFirefox)g.click(function(){mejs.MediaFeatures.hasTrueNativeFullScreen&&
mejs.MediaFeatures.isFullScreen()||a.isFullScreen?a.exitFullScreen():a.enterFullScreen()});else{var k=null;if(function(){var h=document.createElement("x"),l=document.documentElement,r=window.getComputedStyle;if(!("pointerEvents"in h.style))return false;h.style.pointerEvents="auto";h.style.pointerEvents="x";l.appendChild(h);r=r&&r(h,"").pointerEvents==="auto";l.removeChild(h);return!!r}()&&!mejs.MediaFeatures.isOpera){var j=false,m=function(){if(j){for(var h in q)q[h].hide();g.css("pointer-events",
"");d.controls.css("pointer-events","");d.media.removeEventListener("click",d.clickToPlayPauseCallback);j=false}},q={};b=["top","left","right","bottom"];var p,t=function(){var h=g.offset().left-d.container.offset().left,l=g.offset().top-d.container.offset().top,r=g.outerWidth(true),n=g.outerHeight(true),o=d.container.width(),s=d.container.height();for(p in q)q[p].css({position:"absolute",top:0,left:0});q.top.width(o).height(l);q.left.width(h).height(n).css({top:l});q.right.width(o-h-r).height(n).css({top:l,
left:h+r});q.bottom.width(o).height(s-n-l).css({top:l+n})};d.globalBind("resize",function(){t()});p=0;for(c=b.length;p<c;p++)q[b[p]]=f('<div class="mejs-fullscreen-hover" />').appendTo(d.container).mouseover(m).hide();g.on("mouseover",function(){if(!d.isFullScreen){var h=g.offset(),l=a.container.offset();e.positionFullscreenButton(h.left-l.left,h.top-l.top,false);g.css("pointer-events","none");d.controls.css("pointer-events","none");d.media.addEventListener("click",d.clickToPlayPauseCallback);for(p in q)q[p].show();
t();j=true}});e.addEventListener("fullscreenchange",function(){d.isFullScreen=!d.isFullScreen;d.isFullScreen?d.media.removeEventListener("click",d.clickToPlayPauseCallback):d.media.addEventListener("click",d.clickToPlayPauseCallback);m()});d.globalBind("mousemove",function(h){if(j){var l=g.offset();if(h.pageY<l.top||h.pageY>l.top+g.outerHeight(true)||h.pageX<l.left||h.pageX>l.left+g.outerWidth(true)){g.css("pointer-events","");d.controls.css("pointer-events","");j=false}}})}else g.on("mouseover",
function(){if(k!==null){clearTimeout(k);delete k}var h=g.offset(),l=a.container.offset();e.positionFullscreenButton(h.left-l.left,h.top-l.top,true)}).on("mouseout",function(){if(k!==null){clearTimeout(k);delete k}k=setTimeout(function(){e.hideFullscreenButton()},1500)})}a.fullscreenBtn=g;d.globalBind("keydown",function(h){if((mejs.MediaFeatures.hasTrueNativeFullScreen&&mejs.MediaFeatures.isFullScreen()||d.isFullScreen)&&h.keyCode==27)a.exitFullScreen()})}},cleanfullscreen:function(a){a.exitFullScreen()},
containerSizeTimeout:null,enterFullScreen:function(){var a=this;if(!(a.media.pluginType!=="native"&&(mejs.MediaFeatures.isFirefox||a.options.usePluginFullScreen))){f(document.documentElement).addClass("mejs-fullscreen");normalHeight=a.container.height();normalWidth=a.container.width();if(a.media.pluginType==="native")if(mejs.MediaFeatures.hasTrueNativeFullScreen){mejs.MediaFeatures.requestFullScreen(a.container[0]);a.isInIframe&&setTimeout(function c(){if(a.isNativeFullScreen)f(window).width()!==
screen.width?a.exitFullScreen():setTimeout(c,500)},500)}else if(mejs.MediaFeatures.hasSemiNativeFullScreen){a.media.webkitEnterFullscreen();return}if(a.isInIframe){var b=a.options.newWindowCallback(this);if(b!=="")if(mejs.MediaFeatures.hasTrueNativeFullScreen)setTimeout(function(){if(!a.isNativeFullScreen){a.pause();window.open(b,a.id,"top=0,left=0,width="+screen.availWidth+",height="+screen.availHeight+",resizable=yes,scrollbars=no,status=no,toolbar=no")}},250);else{a.pause();window.open(b,a.id,
"top=0,left=0,width="+screen.availWidth+",height="+screen.availHeight+",resizable=yes,scrollbars=no,status=no,toolbar=no");return}}a.container.addClass("mejs-container-fullscreen").width("100%").height("100%");a.containerSizeTimeout=setTimeout(function(){a.container.css({width:"100%",height:"100%"});a.setControlsSize()},500);if(a.media.pluginType==="native")a.$media.width("100%").height("100%");else{a.container.find(".mejs-shim").width("100%").height("100%");a.media.setVideoSize(f(window).width(),
f(window).height())}a.layers.children("div").width("100%").height("100%");a.fullscreenBtn&&a.fullscreenBtn.removeClass("mejs-fullscreen").addClass("mejs-unfullscreen");a.setControlsSize();a.isFullScreen=true}},exitFullScreen:function(){clearTimeout(this.containerSizeTimeout);if(this.media.pluginType!=="native"&&mejs.MediaFeatures.isFirefox)this.media.setFullscreen(false);else{if(mejs.MediaFeatures.hasTrueNativeFullScreen&&(mejs.MediaFeatures.isFullScreen()||this.isFullScreen))mejs.MediaFeatures.cancelFullScreen();
f(document.documentElement).removeClass("mejs-fullscreen");this.container.removeClass("mejs-container-fullscreen").width(normalWidth).height(normalHeight);if(this.media.pluginType==="native")this.$media.width(normalWidth).height(normalHeight);else{this.container.find(".mejs-shim").width(normalWidth).height(normalHeight);this.media.setVideoSize(normalWidth,normalHeight)}this.layers.children("div").width(normalWidth).height(normalHeight);this.fullscreenBtn.removeClass("mejs-unfullscreen").addClass("mejs-fullscreen");
this.setControlsSize();this.isFullScreen=false}}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{startLanguage:"",tracksText:mejs.i18n.t("Captions/Subtitles"),hideCaptionsButtonWhenEmpty:true,toggleCaptionsButtonWhenOnlyOne:false,slidesSelector:""});f.extend(MediaElementPlayer.prototype,{hasChapters:false,buildtracks:function(a,b,c,e){if(a.tracks.length!=0){var d;if(this.domNode.textTracks)for(d=this.domNode.textTracks.length-1;d>=0;d--)this.domNode.textTracks[d].mode="hidden";a.chapters=f('<div class="mejs-chapters mejs-layer"></div>').prependTo(c).hide();a.captions=
f('<div class="mejs-captions-layer mejs-layer"><div class="mejs-captions-position mejs-captions-position-hover"><span class="mejs-captions-text"></span></div></div>').prependTo(c).hide();a.captionsText=a.captions.find(".mejs-captions-text");a.captionsButton=f('<div class="mejs-button mejs-captions-button"><button type="button" aria-controls="'+this.id+'" title="'+this.options.tracksText+'" aria-label="'+this.options.tracksText+'"></button><div class="mejs-captions-selector"><ul><li><input type="radio" name="'+
a.id+'_captions" id="'+a.id+'_captions_none" value="none" checked="checked" /><label for="'+a.id+'_captions_none">'+mejs.i18n.t("None")+"</label></li></ul></div></div>").appendTo(b);for(d=b=0;d<a.tracks.length;d++)a.tracks[d].kind=="subtitles"&&b++;this.options.toggleCaptionsButtonWhenOnlyOne&&b==1?a.captionsButton.on("click",function(){a.setTrack(a.selectedTrack==null?a.tracks[0].srclang:"none")}):a.captionsButton.hover(function(){f(this).find(".mejs-captions-selector").css("visibility","visible")},
function(){f(this).find(".mejs-captions-selector").css("visibility","hidden")}).on("click","input[type=radio]",function(){lang=this.value;a.setTrack(lang)});a.options.alwaysShowControls?a.container.find(".mejs-captions-position").addClass("mejs-captions-position-hover"):a.container.bind("controlsshown",function(){a.container.find(".mejs-captions-position").addClass("mejs-captions-position-hover")}).bind("controlshidden",function(){e.paused||a.container.find(".mejs-captions-position").removeClass("mejs-captions-position-hover")});
a.trackToLoad=-1;a.selectedTrack=null;a.isLoadingTrack=false;for(d=0;d<a.tracks.length;d++)a.tracks[d].kind=="subtitles"&&a.addTrackButton(a.tracks[d].srclang,a.tracks[d].label);a.loadNextTrack();e.addEventListener("timeupdate",function(){a.displayCaptions()},false);if(a.options.slidesSelector!=""){a.slidesContainer=f(a.options.slidesSelector);e.addEventListener("timeupdate",function(){a.displaySlides()},false)}e.addEventListener("loadedmetadata",function(){a.displayChapters()},false);a.container.hover(function(){if(a.hasChapters){a.chapters.css("visibility",
"visible");a.chapters.fadeIn(200).height(a.chapters.find(".mejs-chapter").outerHeight())}},function(){a.hasChapters&&!e.paused&&a.chapters.fadeOut(200,function(){f(this).css("visibility","hidden");f(this).css("display","block")})});a.node.getAttribute("autoplay")!==null&&a.chapters.css("visibility","hidden")}},setTrack:function(a){var b;if(a=="none"){this.selectedTrack=null;this.captionsButton.removeClass("mejs-captions-enabled")}else for(b=0;b<this.tracks.length;b++)if(this.tracks[b].srclang==a){this.selectedTrack==
null&&this.captionsButton.addClass("mejs-captions-enabled");this.selectedTrack=this.tracks[b];this.captions.attr("lang",this.selectedTrack.srclang);this.displayCaptions();break}},loadNextTrack:function(){this.trackToLoad++;if(this.trackToLoad<this.tracks.length){this.isLoadingTrack=true;this.loadTrack(this.trackToLoad)}else{this.isLoadingTrack=false;this.checkForTracks()}},loadTrack:function(a){var b=this,c=b.tracks[a];f.ajax({url:c.src,dataType:"text",success:function(e){c.entries=typeof e=="string"&&
/<tt\s+xml/ig.exec(e)?mejs.TrackFormatParser.dfxp.parse(e):mejs.TrackFormatParser.webvvt.parse(e);c.isLoaded=true;b.enableTrackButton(c.srclang,c.label);b.loadNextTrack();c.kind=="chapters"&&b.media.addEventListener("play",function(){b.media.duration>0&&b.displayChapters(c)},false);c.kind=="slides"&&b.setupSlides(c)},error:function(){b.loadNextTrack()}})},enableTrackButton:function(a,b){if(b==="")b=mejs.language.codes[a]||a;this.captionsButton.find("input[value="+a+"]").prop("disabled",false).siblings("label").html(b);
this.options.startLanguage==a&&f("#"+this.id+"_captions_"+a).click();this.adjustLanguageBox()},addTrackButton:function(a,b){if(b==="")b=mejs.language.codes[a]||a;this.captionsButton.find("ul").append(f('<li><input type="radio" name="'+this.id+'_captions" id="'+this.id+"_captions_"+a+'" value="'+a+'" disabled="disabled" /><label for="'+this.id+"_captions_"+a+'">'+b+" (loading)</label></li>"));this.adjustLanguageBox();this.container.find(".mejs-captions-translations option[value="+a+"]").remove()},
adjustLanguageBox:function(){this.captionsButton.find(".mejs-captions-selector").height(this.captionsButton.find(".mejs-captions-selector ul").outerHeight(true)+this.captionsButton.find(".mejs-captions-translations").outerHeight(true))},checkForTracks:function(){var a=false;if(this.options.hideCaptionsButtonWhenEmpty){for(i=0;i<this.tracks.length;i++)if(this.tracks[i].kind=="subtitles"){a=true;break}if(!a){this.captionsButton.hide();this.setControlsSize()}}},displayCaptions:function(){if(typeof this.tracks!=
"undefined"){var a,b=this.selectedTrack;if(b!=null&&b.isLoaded)for(a=0;a<b.entries.times.length;a++)if(this.media.currentTime>=b.entries.times[a].start&&this.media.currentTime<=b.entries.times[a].stop){this.captionsText.html(b.entries.text[a]);this.captions.show().height(0);return}this.captions.hide()}},setupSlides:function(a){this.slides=a;this.slides.entries.imgs=[this.slides.entries.text.length];this.showSlide(0)},showSlide:function(a){if(!(typeof this.tracks=="undefined"||typeof this.slidesContainer==
"undefined")){var b=this,c=b.slides.entries.text[a],e=b.slides.entries.imgs[a];if(typeof e=="undefined"||typeof e.fadeIn=="undefined")b.slides.entries.imgs[a]=e=f('<img src="'+c+'">').on("load",function(){e.appendTo(b.slidesContainer).hide().fadeIn().siblings(":visible").fadeOut()});else!e.is(":visible")&&!e.is(":animated")&&e.fadeIn().siblings(":visible").fadeOut()}},displaySlides:function(){if(typeof this.slides!="undefined"){var a=this.slides,b;for(b=0;b<a.entries.times.length;b++)if(this.media.currentTime>=
a.entries.times[b].start&&this.media.currentTime<=a.entries.times[b].stop){this.showSlide(b);break}}},displayChapters:function(){var a;for(a=0;a<this.tracks.length;a++)if(this.tracks[a].kind=="chapters"&&this.tracks[a].isLoaded){this.drawChapters(this.tracks[a]);this.hasChapters=true;break}},drawChapters:function(a){var b=this,c,e,d=e=0;b.chapters.empty();for(c=0;c<a.entries.times.length;c++){e=a.entries.times[c].stop-a.entries.times[c].start;e=Math.floor(e/b.media.duration*100);if(e+d>100||c==a.entries.times.length-
1&&e+d<100)e=100-d;b.chapters.append(f('<div class="mejs-chapter" rel="'+a.entries.times[c].start+'" style="left: '+d.toString()+"%;width: "+e.toString()+'%;"><div class="mejs-chapter-block'+(c==a.entries.times.length-1?" mejs-chapter-block-last":"")+'"><span class="ch-title">'+a.entries.text[c]+'</span><span class="ch-time">'+mejs.Utility.secondsToTimeCode(a.entries.times[c].start)+"&ndash;"+mejs.Utility.secondsToTimeCode(a.entries.times[c].stop)+"</span></div></div>"));d+=e}b.chapters.find("div.mejs-chapter").click(function(){b.media.setCurrentTime(parseFloat(f(this).attr("rel")));
b.media.paused&&b.media.play()});b.chapters.show()}});mejs.language={codes:{af:"Afrikaans",sq:"Albanian",ar:"Arabic",be:"Belarusian",bg:"Bulgarian",ca:"Catalan",zh:"Chinese","zh-cn":"Chinese Simplified","zh-tw":"Chinese Traditional",hr:"Croatian",cs:"Czech",da:"Danish",nl:"Dutch",en:"English",et:"Estonian",tl:"Filipino",fi:"Finnish",fr:"French",gl:"Galician",de:"German",el:"Greek",ht:"Haitian Creole",iw:"Hebrew",hi:"Hindi",hu:"Hungarian",is:"Icelandic",id:"Indonesian",ga:"Irish",it:"Italian",ja:"Japanese",
ko:"Korean",lv:"Latvian",lt:"Lithuanian",mk:"Macedonian",ms:"Malay",mt:"Maltese",no:"Norwegian",fa:"Persian",pl:"Polish",pt:"Portuguese",ro:"Romanian",ru:"Russian",sr:"Serbian",sk:"Slovak",sl:"Slovenian",es:"Spanish",sw:"Swahili",sv:"Swedish",tl:"Tagalog",th:"Thai",tr:"Turkish",uk:"Ukrainian",vi:"Vietnamese",cy:"Welsh",yi:"Yiddish"}};mejs.TrackFormatParser={webvvt:{pattern_identifier:/^([a-zA-z]+-)?[0-9]+$/,pattern_timecode:/^([0-9]{2}:[0-9]{2}:[0-9]{2}([,.][0-9]{1,3})?) --\> ([0-9]{2}:[0-9]{2}:[0-9]{2}([,.][0-9]{3})?)(.*)$/,
parse:function(a){var b=0;a=mejs.TrackFormatParser.split2(a,/\r?\n/);for(var c={text:[],times:[]},e,d;b<a.length;b++)if(this.pattern_identifier.exec(a[b])){b++;if((e=this.pattern_timecode.exec(a[b]))&&b<a.length){b++;d=a[b];for(b++;a[b]!==""&&b<a.length;){d=d+"\n"+a[b];b++}d=f.trim(d).replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig,"<a href='$1' target='_blank'>$1</a>");c.text.push(d);c.times.push({start:mejs.Utility.convertSMPTEtoSeconds(e[1])==0?0.2:mejs.Utility.convertSMPTEtoSeconds(e[1]),
stop:mejs.Utility.convertSMPTEtoSeconds(e[3]),settings:e[5]})}}return c}},dfxp:{parse:function(a){a=f(a).filter("tt");var b=0;b=a.children("div").eq(0);var c=b.find("p");b=a.find("#"+b.attr("style"));var e,d;a={text:[],times:[]};if(b.length){d=b.removeAttr("id").get(0).attributes;if(d.length){e={};for(b=0;b<d.length;b++)e[d[b].name.split(":")[1]]=d[b].value}}for(b=0;b<c.length;b++){var g;d={start:null,stop:null,style:null};if(c.eq(b).attr("begin"))d.start=mejs.Utility.convertSMPTEtoSeconds(c.eq(b).attr("begin"));
if(!d.start&&c.eq(b-1).attr("end"))d.start=mejs.Utility.convertSMPTEtoSeconds(c.eq(b-1).attr("end"));if(c.eq(b).attr("end"))d.stop=mejs.Utility.convertSMPTEtoSeconds(c.eq(b).attr("end"));if(!d.stop&&c.eq(b+1).attr("begin"))d.stop=mejs.Utility.convertSMPTEtoSeconds(c.eq(b+1).attr("begin"));if(e){g="";for(var k in e)g+=k+":"+e[k]+";"}if(g)d.style=g;if(d.start==0)d.start=0.2;a.times.push(d);d=f.trim(c.eq(b).html()).replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig,
"<a href='$1' target='_blank'>$1</a>");a.text.push(d);if(a.times.start==0)a.times.start=2}return a}},split2:function(a,b){return a.split(b)}};if("x\n\ny".split(/\n/gi).length!=3)mejs.TrackFormatParser.split2=function(a,b){var c=[],e="",d;for(d=0;d<a.length;d++){e+=a.substring(d,d+1);if(b.test(e)){c.push(e.replace(b,""));e=""}}c.push(e);return c}})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{contextMenuItems:[{render:function(a){if(typeof a.enterFullScreen=="undefined")return null;return a.isFullScreen?mejs.i18n.t("Turn off Fullscreen"):mejs.i18n.t("Go Fullscreen")},click:function(a){a.isFullScreen?a.exitFullScreen():a.enterFullScreen()}},{render:function(a){return a.media.muted?mejs.i18n.t("Unmute"):mejs.i18n.t("Mute")},click:function(a){a.media.muted?a.setMuted(false):a.setMuted(true)}},{isSeparator:true},{render:function(){return mejs.i18n.t("Download Video")},
click:function(a){window.location.href=a.media.currentSrc}}]});f.extend(MediaElementPlayer.prototype,{buildcontextmenu:function(a){a.contextMenu=f('<div class="mejs-contextmenu"></div>').appendTo(f("body")).hide();a.container.bind("contextmenu",function(b){if(a.isContextMenuEnabled){b.preventDefault();a.renderContextMenu(b.clientX-1,b.clientY-1);return false}});a.container.bind("click",function(){a.contextMenu.hide()});a.contextMenu.bind("mouseleave",function(){a.startContextMenuTimer()})},cleancontextmenu:function(a){a.contextMenu.remove()},
isContextMenuEnabled:true,enableContextMenu:function(){this.isContextMenuEnabled=true},disableContextMenu:function(){this.isContextMenuEnabled=false},contextMenuTimeout:null,startContextMenuTimer:function(){var a=this;a.killContextMenuTimer();a.contextMenuTimer=setTimeout(function(){a.hideContextMenu();a.killContextMenuTimer()},750)},killContextMenuTimer:function(){var a=this.contextMenuTimer;if(a!=null){clearTimeout(a);delete a}},hideContextMenu:function(){this.contextMenu.hide()},renderContextMenu:function(a,
b){for(var c=this,e="",d=c.options.contextMenuItems,g=0,k=d.length;g<k;g++)if(d[g].isSeparator)e+='<div class="mejs-contextmenu-separator"></div>';else{var j=d[g].render(c);if(j!=null)e+='<div class="mejs-contextmenu-item" data-itemindex="'+g+'" id="element-'+Math.random()*1E6+'">'+j+"</div>"}c.contextMenu.empty().append(f(e)).css({top:b,left:a}).show();c.contextMenu.find(".mejs-contextmenu-item").each(function(){var m=f(this),q=parseInt(m.data("itemindex"),10),p=c.options.contextMenuItems[q];typeof p.show!=
"undefined"&&p.show(m,c);m.click(function(){typeof p.click!="undefined"&&p.click(c);c.contextMenu.hide()})});setTimeout(function(){c.killControlsTimer("rev3")},100)}})})(mejs.$);
(function(f){f.extend(mejs.MepDefaults,{postrollCloseText:mejs.i18n.t("Close")});f.extend(MediaElementPlayer.prototype,{buildpostroll:function(a,b,c){var e=this.container.find('link[rel="postroll"]').attr("href");if(typeof e!=="undefined"){a.postroll=f('<div class="mejs-postroll-layer mejs-layer"><a class="mejs-postroll-close" onclick="$(this).parent().hide();return false;">'+this.options.postrollCloseText+'</a><div class="mejs-postroll-layer-content"></div></div>').prependTo(c).hide();this.media.addEventListener("ended",
function(){f.ajax({dataType:"html",url:e,success:function(d){c.find(".mejs-postroll-layer-content").html(d)}});a.postroll.show()},false)}}})})(mejs.$);

/*!
 * fancyBox - jQuery Plugin
 * version: 2.1.5 (Fri, 14 Jun 2013)
 * @requires jQuery v1.6 or later
 *
 * Examples at http://fancyapps.com/fancybox/
 * License: www.fancyapps.com/fancybox/#license
 *
 * Copyright 2012 Janis Skarnelis - janis@fancyapps.com
 *
 */

(function (window, document, $, undefined) {
	"use strict";

	var H = $("html"),
		W = $(window),
		D = $(document),
		F = $.fancybox = function () {
			F.open.apply( this, arguments );
		},
		IE =  navigator.userAgent.match(/msie/i),
		didUpdate	= null,
		isTouch		= document.createTouch !== undefined,

		isQuery	= function(obj) {
			return obj && obj.hasOwnProperty && obj instanceof $;
		},
		isString = function(str) {
			return str && $.type(str) === "string";
		},
		isPercentage = function(str) {
			return isString(str) && str.indexOf('%') > 0;
		},
		isScrollable = function(el) {
			return (el && !(el.style.overflow && el.style.overflow === 'hidden') && ((el.clientWidth && el.scrollWidth > el.clientWidth) || (el.clientHeight && el.scrollHeight > el.clientHeight)));
		},
		getScalar = function(orig, dim) {
			var value = parseInt(orig, 10) || 0;

			if (dim && isPercentage(orig)) {
				value = F.getViewport()[ dim ] / 100 * value;
			}

			return Math.ceil(value);
		},
		getValue = function(value, dim) {
			return getScalar(value, dim) + 'px';
		};

	$.extend(F, {
		// The current version of fancyBox
		version: '2.1.5',

		defaults: {
			padding : 15,
			margin  : 20,

			width     : 800,
			height    : 600,
			minWidth  : 100,
			minHeight : 100,
			maxWidth  : 9999,
			maxHeight : 9999,
			pixelRatio: 1, // Set to 2 for retina display support

			autoSize   : true,
			autoHeight : false,
			autoWidth  : false,

			autoResize  : true,
			autoCenter  : !isTouch,
			fitToView   : true,
			aspectRatio : false,
			topRatio    : 0.5,
			leftRatio   : 0.5,

			scrolling : 'auto', // 'auto', 'yes' or 'no'
			wrapCSS   : '',

			arrows     : true,
			closeBtn   : true,
			closeClick : false,
			nextClick  : false,
			mouseWheel : true,
			autoPlay   : false,
			playSpeed  : 3000,
			preload    : 3,
			modal      : false,
			loop       : true,

			ajax  : {
				dataType : 'html',
				headers  : { 'X-fancyBox': true }
			},
			iframe : {
				scrolling : 'auto',
				preload   : true
			},
			swf : {
				wmode: 'transparent',
				allowfullscreen   : 'true',
				allowscriptaccess : 'always'
			},

			keys  : {
				next : {
					13 : 'left', // enter
					34 : 'up',   // page down
					39 : 'left', // right arrow
					40 : 'up'    // down arrow
				},
				prev : {
					8  : 'right',  // backspace
					33 : 'down',   // page up
					37 : 'right',  // left arrow
					38 : 'down'    // up arrow
				},
				close  : [27], // escape key
				play   : [32], // space - start/stop slideshow
				toggle : [70]  // letter "f" - toggle fullscreen
			},

			direction : {
				next : 'left',
				prev : 'right'
			},

			scrollOutside  : true,

			// Override some properties
			index   : 0,
			type    : null,
			href    : null,
			content : null,
			title   : null,

			// HTML templates
			tpl: {
				wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
				image    : '<img class="fancybox-image" src="{href}" alt="" />',
				iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (IE ? ' allowtransparency="true"' : '') + '></iframe>',
				error    : '<p class="fancybox-error">Ошибка загрузки контента.</p>',
				closeBtn : '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
				next     : '<a title="Следующая" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev     : '<a title="Предыдущая" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			},

			// Properties for each animation type
			// Opening fancyBox
			openEffect  : 'fade', // 'elastic', 'fade' or 'none'
			openSpeed   : 250,
			openEasing  : 'swing',
			openOpacity : true,
			openMethod  : 'zoomIn',

			// Closing fancyBox
			closeEffect  : 'fade', // 'elastic', 'fade' or 'none'
			closeSpeed   : 250,
			closeEasing  : 'swing',
			closeOpacity : true,
			closeMethod  : 'zoomOut',

			// Changing next gallery item
			nextEffect : 'elastic', // 'elastic', 'fade' or 'none'
			nextSpeed  : 250,
			nextEasing : 'swing',
			nextMethod : 'changeIn',

			// Changing previous gallery item
			prevEffect : 'elastic', // 'elastic', 'fade' or 'none'
			prevSpeed  : 250,
			prevEasing : 'swing',
			prevMethod : 'changeOut',

			// Enable default helpers
			helpers : {
				overlay : true,
				title   : true
			},

			// Callbacks
			onCancel     : $.noop, // If canceling
			beforeLoad   : $.noop, // Before loading
			afterLoad    : $.noop, // After loading
			beforeShow   : $.noop, // Before changing in current item
			afterShow    : $.noop, // After opening
			beforeChange : $.noop, // Before changing gallery item
			beforeClose  : $.noop, // Before closing
			afterClose   : $.noop  // After closing
		},

		//Current state
		group    : {}, // Selected group
		opts     : {}, // Group options
		previous : null,  // Previous element
		coming   : null,  // Element being loaded
		current  : null,  // Currently loaded element
		isActive : false, // Is activated
		isOpen   : false, // Is currently open
		isOpened : false, // Have been fully opened at least once

		wrap  : null,
		skin  : null,
		outer : null,
		inner : null,

		player : {
			timer    : null,
			isActive : false
		},

		// Loaders
		ajaxLoad   : null,
		imgPreload : null,

		// Some collections
		transitions : {},
		helpers     : {},

		/*
		 *	Static methods
		 */

		open: function (group, opts) {
			if (!group) {
				return;
			}

			if (!$.isPlainObject(opts)) {
				opts = {};
			}

			// Close if already active
			if (false === F.close(true)) {
				return;
			}

			// Normalize group
			if (!$.isArray(group)) {
				group = isQuery(group) ? $(group).get() : [group];
			}

			// Recheck if the type of each element is `object` and set content type (image, ajax, etc)
			$.each(group, function(i, element) {
				var obj = {},
					href,
					title,
					content,
					type,
					rez,
					hrefParts,
					selector;

				if ($.type(element) === "object") {
					// Check if is DOM element
					if (element.nodeType) {
						element = $(element);
					}

					if (isQuery(element)) {
						obj = {
							href    : element.data('fancybox-href') || element.attr('href'),
							title   : element.data('fancybox-title') || element.attr('title'),
							isDom   : true,
							element : element
						};

						if ($.metadata) {
							$.extend(true, obj, element.metadata());
						}

					} else {
						obj = element;
					}
				}

				href  = opts.href  || obj.href || (isString(element) ? element : null);
				title = opts.title !== undefined ? opts.title : obj.title || '';

				content = opts.content || obj.content;
				type    = content ? 'html' : (opts.type  || obj.type);

				if (!type && obj.isDom) {
					type = element.data('fancybox-type');

					if (!type) {
						rez  = element.prop('class').match(/fancybox\.(\w+)/);
						type = rez ? rez[1] : null;
					}
				}

				if (isString(href)) {
					// Try to guess the content type
					if (!type) {
						if (F.isImage(href)) {
							type = 'image';

						} else if (F.isSWF(href)) {
							type = 'swf';

						} else if (href.charAt(0) === '#') {
							type = 'inline';

						} else if (isString(element)) {
							type    = 'html';
							content = element;
						}
					}

					// Split url into two pieces with source url and content selector, e.g,
					// "/mypage.html #my_id" will load "/mypage.html" and display element having id "my_id"
					if (type === 'ajax') {
						hrefParts = href.split(/\s+/, 2);
						href      = hrefParts.shift();
						selector  = hrefParts.shift();
					}
				}

				if (!content) {
					if (type === 'inline') {
						if (href) {
							content = $( isString(href) ? href.replace(/.*(?=#[^\s]+$)/, '') : href ); //strip for ie7

						} else if (obj.isDom) {
							content = element;
						}

					} else if (type === 'html') {
						content = href;

					} else if (!type && !href && obj.isDom) {
						type    = 'inline';
						content = element;
					}
				}

				$.extend(obj, {
					href     : href,
					type     : type,
					content  : content,
					title    : title,
					selector : selector
				});

				group[ i ] = obj;
			});

			// Extend the defaults
			F.opts = $.extend(true, {}, F.defaults, opts);

			// All options are merged recursive except keys
			if (opts.keys !== undefined) {
				F.opts.keys = opts.keys ? $.extend({}, F.defaults.keys, opts.keys) : false;
			}

			F.group = group;

			return F._start(F.opts.index);
		},

		// Cancel image loading or abort ajax request
		cancel: function () {
			var coming = F.coming;

			if (!coming || false === F.trigger('onCancel')) {
				return;
			}

			F.hideLoading();

			if (F.ajaxLoad) {
				F.ajaxLoad.abort();
			}

			F.ajaxLoad = null;

			if (F.imgPreload) {
				F.imgPreload.onload = F.imgPreload.onerror = null;
			}

			if (coming.wrap) {
				coming.wrap.stop(true, true).trigger('onReset').remove();
			}

			F.coming = null;

			// If the first item has been canceled, then clear everything
			if (!F.current) {
				F._afterZoomOut( coming );
			}
		},

		// Start closing animation if is open; remove immediately if opening/closing
		close: function (event) {
			F.cancel();

			if (false === F.trigger('beforeClose')) {
				return;
			}

			F.unbindEvents();

			if (!F.isActive) {
				return;
			}

			if (!F.isOpen || event === true) {
				$('.fancybox-wrap').stop(true).trigger('onReset').remove();

				F._afterZoomOut();

			} else {
				F.isOpen = F.isOpened = false;
				F.isClosing = true;

				$('.fancybox-item, .fancybox-nav').remove();

				F.wrap.stop(true, true).removeClass('fancybox-opened');

				F.transitions[ F.current.closeMethod ]();
			}
		},

		// Manage slideshow:
		//   $.fancybox.play(); - toggle slideshow
		//   $.fancybox.play( true ); - start
		//   $.fancybox.play( false ); - stop
		play: function ( action ) {
			var clear = function () {
					clearTimeout(F.player.timer);
				},
				set = function () {
					clear();

					if (F.current && F.player.isActive) {
						F.player.timer = setTimeout(F.next, F.current.playSpeed);
					}
				},
				stop = function () {
					clear();

					D.unbind('.player');

					F.player.isActive = false;

					F.trigger('onPlayEnd');
				},
				start = function () {
					if (F.current && (F.current.loop || F.current.index < F.group.length - 1)) {
						F.player.isActive = true;

						D.bind({
							'onCancel.player beforeClose.player' : stop,
							'onUpdate.player'   : set,
							'beforeLoad.player' : clear
						});

						set();

						F.trigger('onPlayStart');
					}
				};

			if (action === true || (!F.player.isActive && action !== false)) {
				start();
			} else {
				stop();
			}
		},

		// Navigate to next gallery item
		next: function ( direction ) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.next;
				}

				F.jumpto(current.index + 1, direction, 'next');
			}
		},

		// Navigate to previous gallery item
		prev: function ( direction ) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.prev;
				}

				F.jumpto(current.index - 1, direction, 'prev');
			}
		},

		// Navigate to gallery item by index
		jumpto: function ( index, direction, router ) {
			var current = F.current;

			if (!current) {
				return;
			}

			index = getScalar(index);

			F.direction = direction || current.direction[ (index >= current.index ? 'next' : 'prev') ];
			F.router    = router || 'jumpto';

			if (current.loop) {
				if (index < 0) {
					index = current.group.length + (index % current.group.length);
				}

				index = index % current.group.length;
			}

			if (current.group[ index ] !== undefined) {
				F.cancel();

				F._start(index);
			}
		},

		// Center inside viewport and toggle position type to fixed or absolute if needed
		reposition: function (e, onlyAbsolute) {
			var current = F.current,
				wrap    = current ? current.wrap : null,
				pos;

			if (wrap) {
				pos = F._getPosition(onlyAbsolute);

				if (e && e.type === 'scroll') {
					delete pos.position;

					wrap.stop(true, true).animate(pos, 200);

				} else {
					wrap.css(pos);

					current.pos = $.extend({}, current.dim, pos);
				}
			}
		},

		update: function (e) {
			var type = (e && e.type),
				anyway = !type || type === 'orientationchange';

			if (anyway) {
				clearTimeout(didUpdate);

				didUpdate = null;
			}

			if (!F.isOpen || didUpdate) {
				return;
			}

			didUpdate = setTimeout(function() {
				var current = F.current;

				if (!current || F.isClosing) {
					return;
				}

				F.wrap.removeClass('fancybox-tmp');

				if (anyway || type === 'load' || (type === 'resize' && current.autoResize)) {
					F._setDimension();
				}

				if (!(type === 'scroll' && current.canShrink)) {
					F.reposition(e);
				}

				F.trigger('onUpdate');

				didUpdate = null;

			}, (anyway && !isTouch ? 0 : 300));
		},

		// Shrink content to fit inside viewport or restore if resized
		toggle: function ( action ) {
			if (F.isOpen) {
				F.current.fitToView = $.type(action) === "boolean" ? action : !F.current.fitToView;

				// Help browser to restore document dimensions
				if (isTouch) {
					F.wrap.removeAttr('style').addClass('fancybox-tmp');

					F.trigger('onUpdate');
				}

				F.update();
			}
		},

		hideLoading: function () {
			D.unbind('.loading');

			$('#fancybox-loading').remove();
		},

		showLoading: function () {
			var el, viewport;

			F.hideLoading();

			el = $('<div id="fancybox-loading"><div></div></div>').click(F.cancel).appendTo('body');

			// If user will press the escape-button, the request will be canceled
			D.bind('keydown.loading', function(e) {
				if ((e.which || e.keyCode) === 27) {
					e.preventDefault();

					F.cancel();
				}
			});

			if (!F.defaults.fixed) {
				viewport = F.getViewport();

				el.css({
					position : 'absolute',
					top  : (viewport.h * 0.5) + viewport.y,
					left : (viewport.w * 0.5) + viewport.x
				});
			}
		},

		getViewport: function () {
			var locked = (F.current && F.current.locked) || false,
				rez    = {
					x: W.scrollLeft(),
					y: W.scrollTop()
				};

			if (locked) {
				rez.w = locked[0].clientWidth;
				rez.h = locked[0].clientHeight;

			} else {
				// See http://bugs.jquery.com/ticket/6724
				rez.w = isTouch && window.innerWidth  ? window.innerWidth  : W.width();
				rez.h = isTouch && window.innerHeight ? window.innerHeight : W.height();
			}

			return rez;
		},

		// Unbind the keyboard / clicking actions
		unbindEvents: function () {
			if (F.wrap && isQuery(F.wrap)) {
				F.wrap.unbind('.fb');
			}

			D.unbind('.fb');
			W.unbind('.fb');
		},

		bindEvents: function () {
			var current = F.current,
				keys;

			if (!current) {
				return;
			}

			// Changing document height on iOS devices triggers a 'resize' event,
			// that can change document height... repeating infinitely
			W.bind('orientationchange.fb' + (isTouch ? '' : ' resize.fb') + (current.autoCenter && !current.locked ? ' scroll.fb' : ''), F.update);

			keys = current.keys;

			if (keys) {
				D.bind('keydown.fb', function (e) {
					var code   = e.which || e.keyCode,
						target = e.target || e.srcElement;

					// Skip esc key if loading, because showLoading will cancel preloading
					if (code === 27 && F.coming) {
						return false;
					}

					// Ignore key combinations and key events within form elements
					if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && !(target && (target.type || $(target).is('[contenteditable]')))) {
						$.each(keys, function(i, val) {
							if (current.group.length > 1 && val[ code ] !== undefined) {
								F[ i ]( val[ code ] );

								e.preventDefault();
								return false;
							}

							if ($.inArray(code, val) > -1) {
								F[ i ] ();

								e.preventDefault();
								return false;
							}
						});
					}
				});
			}

			if ($.fn.mousewheel && current.mouseWheel) {
				F.wrap.bind('mousewheel.fb', function (e, delta, deltaX, deltaY) {
					var target = e.target || null,
						parent = $(target),
						canScroll = false;

					while (parent.length) {
						if (canScroll || parent.is('.fancybox-skin') || parent.is('.fancybox-wrap')) {
							break;
						}

						canScroll = isScrollable( parent[0] );
						parent    = $(parent).parent();
					}

					if (delta !== 0 && !canScroll) {
						if (F.group.length > 1 && !current.canShrink) {
							if (deltaY > 0 || deltaX > 0) {
								F.prev( deltaY > 0 ? 'down' : 'left' );

							} else if (deltaY < 0 || deltaX < 0) {
								F.next( deltaY < 0 ? 'up' : 'right' );
							}

							e.preventDefault();
						}
					}
				});
			}
		},

		trigger: function (event, o) {
			var ret, obj = o || F.coming || F.current;

			if (!obj) {
				return;
			}

			if ($.isFunction( obj[event] )) {
				ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
			}

			if (ret === false) {
				return false;
			}

			if (obj.helpers) {
				$.each(obj.helpers, function (helper, opts) {
					if (opts && F.helpers[helper] && $.isFunction(F.helpers[helper][event])) {
						F.helpers[helper][event]($.extend(true, {}, F.helpers[helper].defaults, opts), obj);
					}
				});
			}

			D.trigger(event);
		},

		isImage: function (str) {
			return isString(str) && str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
		},

		isSWF: function (str) {
			return isString(str) && str.match(/\.(swf)((\?|#).*)?$/i);
		},

		_start: function (index) {
			var coming = {},
				obj,
				href,
				type,
				margin,
				padding;

			index = getScalar( index );
			obj   = F.group[ index ] || null;

			if (!obj) {
				return false;
			}

			coming = $.extend(true, {}, F.opts, obj);

			// Convert margin and padding properties to array - top, right, bottom, left
			margin  = coming.margin;
			padding = coming.padding;

			if ($.type(margin) === 'number') {
				coming.margin = [margin, margin, margin, margin];
			}

			if ($.type(padding) === 'number') {
				coming.padding = [padding, padding, padding, padding];
			}

			// 'modal' propery is just a shortcut
			if (coming.modal) {
				$.extend(true, coming, {
					closeBtn   : false,
					closeClick : false,
					nextClick  : false,
					arrows     : false,
					mouseWheel : false,
					keys       : null,
					helpers: {
						overlay : {
							closeClick : false
						}
					}
				});
			}

			// 'autoSize' property is a shortcut, too
			if (coming.autoSize) {
				coming.autoWidth = coming.autoHeight = true;
			}

			if (coming.width === 'auto') {
				coming.autoWidth = true;
			}

			if (coming.height === 'auto') {
				coming.autoHeight = true;
			}

			/*
			 * Add reference to the group, so it`s possible to access from callbacks, example:
			 * afterLoad : function() {
			 *     this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 * }
			 */

			coming.group  = F.group;
			coming.index  = index;

			// Give a chance for callback or helpers to update coming item (type, title, etc)
			F.coming = coming;

			if (false === F.trigger('beforeLoad')) {
				F.coming = null;

				return;
			}

			type = coming.type;
			href = coming.href;

			if (!type) {
				F.coming = null;

				//If we can not determine content type then drop silently or display next/prev item if looping through gallery
				if (F.current && F.router && F.router !== 'jumpto') {
					F.current.index = index;

					return F[ F.router ]( F.direction );
				}

				return false;
			}

			F.isActive = true;

			if (type === 'image' || type === 'swf') {
				coming.autoHeight = coming.autoWidth = false;
				coming.scrolling  = 'visible';
			}

			if (type === 'image') {
				coming.aspectRatio = true;
			}

			if (type === 'iframe' && isTouch) {
				coming.scrolling = 'scroll';
			}

			// Build the neccessary markup
			coming.wrap = $(coming.tpl.wrap).addClass('fancybox-' + (isTouch ? 'mobile' : 'desktop') + ' fancybox-type-' + type + ' fancybox-tmp ' + coming.wrapCSS).appendTo( coming.parent || 'body' );

			$.extend(coming, {
				skin  : $('.fancybox-skin',  coming.wrap),
				outer : $('.fancybox-outer', coming.wrap),
				inner : $('.fancybox-inner', coming.wrap)
			});

			$.each(["Top", "Right", "Bottom", "Left"], function(i, v) {
				coming.skin.css('padding' + v, getValue(coming.padding[ i ]));
			});

			F.trigger('onReady');

			// Check before try to load; 'inline' and 'html' types need content, others - href
			if (type === 'inline' || type === 'html') {
				if (!coming.content || !coming.content.length) {
					return F._error( 'content' );
				}

			} else if (!href) {
				return F._error( 'href' );
			}

			if (type === 'image') {
				F._loadImage();

			} else if (type === 'ajax') {
				F._loadAjax();

			} else if (type === 'iframe') {
				F._loadIframe();

			} else {
				F._afterLoad();
			}
		},

		_error: function ( type ) {
			$.extend(F.coming, {
				type       : 'html',
				autoWidth  : true,
				autoHeight : true,
				minWidth   : 0,
				minHeight  : 0,
				scrolling  : 'no',
				hasError   : type,
				content    : F.coming.tpl.error
			});

			F._afterLoad();
		},

		_loadImage: function () {
			// Reset preload image so it is later possible to check "complete" property
			var img = F.imgPreload = new Image();

			img.onload = function () {
				this.onload = this.onerror = null;

				F.coming.width  = this.width / F.opts.pixelRatio;
				F.coming.height = this.height / F.opts.pixelRatio;

				F._afterLoad();
			};

			img.onerror = function () {
				this.onload = this.onerror = null;

				F._error( 'image' );
			};

			img.src = F.coming.href;

			if (img.complete !== true) {
				F.showLoading();
			}
		},

		_loadAjax: function () {
			var coming = F.coming;

			F.showLoading();

			F.ajaxLoad = $.ajax($.extend({}, coming.ajax, {
				url: coming.href,
				error: function (jqXHR, textStatus) {
					if (F.coming && textStatus !== 'abort') {
						F._error( 'ajax', jqXHR );

					} else {
						F.hideLoading();
					}
				},
				success: function (data, textStatus) {
					if (textStatus === 'success') {
						coming.content = data;

						F._afterLoad();
					}
				}
			}));
		},

		_loadIframe: function() {
			var coming = F.coming,
				iframe = $(coming.tpl.iframe.replace(/\{rnd\}/g, new Date().getTime()))
					.attr('scrolling', isTouch ? 'auto' : coming.iframe.scrolling)
					.attr('src', coming.href);

			// This helps IE
			$(coming.wrap).bind('onReset', function () {
				try {
					$(this).find('iframe').hide().attr('src', '//about:blank').end().empty();
				} catch (e) {}
			});

			if (coming.iframe.preload) {
				F.showLoading();

				iframe.one('load', function() {
					$(this).data('ready', 1);

					// iOS will lose scrolling if we resize
					if (!isTouch) {
						$(this).bind('load.fb', F.update);
					}

					// Without this trick:
					//   - iframe won't scroll on iOS devices
					//   - IE7 sometimes displays empty iframe
					$(this).parents('.fancybox-wrap').width('100%').removeClass('fancybox-tmp').show();

					F._afterLoad();
				});
			}

			coming.content = iframe.appendTo( coming.inner );

			if (!coming.iframe.preload) {
				F._afterLoad();
			}
		},

		_preloadImages: function() {
			var group   = F.group,
				current = F.current,
				len     = group.length,
				cnt     = current.preload ? Math.min(current.preload, len - 1) : 0,
				item,
				i;

			for (i = 1; i <= cnt; i += 1) {
				item = group[ (current.index + i ) % len ];

				if (item.type === 'image' && item.href) {
					new Image().src = item.href;
				}
			}
		},

		_afterLoad: function () {
			var coming   = F.coming,
				previous = F.current,
				placeholder = 'fancybox-placeholder',
				current,
				content,
				type,
				scrolling,
				href,
				embed;

			F.hideLoading();

			if (!coming || F.isActive === false) {
				return;
			}

			if (false === F.trigger('afterLoad', coming, previous)) {
				coming.wrap.stop(true).trigger('onReset').remove();

				F.coming = null;

				return;
			}

			if (previous) {
				F.trigger('beforeChange', previous);

				previous.wrap.stop(true).removeClass('fancybox-opened')
					.find('.fancybox-item, .fancybox-nav')
					.remove();
			}

			F.unbindEvents();

			current   = coming;
			content   = coming.content;
			type      = coming.type;
			scrolling = coming.scrolling;

			$.extend(F, {
				wrap  : current.wrap,
				skin  : current.skin,
				outer : current.outer,
				inner : current.inner,
				current  : current,
				previous : previous
			});

			href = current.href;

			switch (type) {
				case 'inline':
				case 'ajax':
				case 'html':
					if (current.selector) {
						content = $('<div>').html(content).find(current.selector);

					} else if (isQuery(content)) {
						if (!content.data(placeholder)) {
							content.data(placeholder, $('<div class="' + placeholder + '"></div>').insertAfter( content ).hide() );
						}

						content = content.show().detach();

						current.wrap.bind('onReset', function () {
							if ($(this).find(content).length) {
								content.hide().replaceAll( content.data(placeholder) ).data(placeholder, false);
							}
						});
					}
				break;

				case 'image':
					content = current.tpl.image.replace('{href}', href);
				break;

				case 'swf':
					content = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + href + '"></param>';
					embed   = '';

					$.each(current.swf, function(name, val) {
						content += '<param name="' + name + '" value="' + val + '"></param>';
						embed   += ' ' + name + '="' + val + '"';
					});

					content += '<embed src="' + href + '" type="application/x-shockwave-flash" width="100%" height="100%"' + embed + '></embed></object>';
				break;
			}

			if (!(isQuery(content) && content.parent().is(current.inner))) {
				current.inner.append( content );
			}

			// Give a chance for helpers or callbacks to update elements
			F.trigger('beforeShow');

			// Set scrolling before calculating dimensions
			current.inner.css('overflow', scrolling === 'yes' ? 'scroll' : (scrolling === 'no' ? 'hidden' : scrolling));

			// Set initial dimensions and start position
			F._setDimension();

			F.reposition();

			F.isOpen = false;
			F.coming = null;

			F.bindEvents();

			if (!F.isOpened) {
				$('.fancybox-wrap').not( current.wrap ).stop(true).trigger('onReset').remove();

			} else if (previous.prevMethod) {
				F.transitions[ previous.prevMethod ]();
			}

			F.transitions[ F.isOpened ? current.nextMethod : current.openMethod ]();

			F._preloadImages();
		},

		_setDimension: function () {
			var viewport   = F.getViewport(),
				steps      = 0,
				canShrink  = false,
				canExpand  = false,
				wrap       = F.wrap,
				skin       = F.skin,
				inner      = F.inner,
				current    = F.current,
				width      = current.width,
				height     = current.height,
				minWidth   = current.minWidth,
				minHeight  = current.minHeight,
				maxWidth   = current.maxWidth,
				maxHeight  = current.maxHeight,
				scrolling  = current.scrolling,
				scrollOut  = current.scrollOutside ? current.scrollbarWidth : 0,
				margin     = current.margin,
				wMargin    = getScalar(margin[1] + margin[3]),
				hMargin    = getScalar(margin[0] + margin[2]),
				wPadding,
				hPadding,
				wSpace,
				hSpace,
				origWidth,
				origHeight,
				origMaxWidth,
				origMaxHeight,
				ratio,
				width_,
				height_,
				maxWidth_,
				maxHeight_,
				iframe,
				body;

			// Reset dimensions so we could re-check actual size
			wrap.add(skin).add(inner).width('auto').height('auto').removeClass('fancybox-tmp');

			wPadding = getScalar(skin.outerWidth(true)  - skin.width());
			hPadding = getScalar(skin.outerHeight(true) - skin.height());

			// Any space between content and viewport (margin, padding, border, title)
			wSpace = wMargin + wPadding;
			hSpace = hMargin + hPadding;

			origWidth  = isPercentage(width)  ? (viewport.w - wSpace) * getScalar(width)  / 100 : width;
			origHeight = isPercentage(height) ? (viewport.h - hSpace) * getScalar(height) / 100 : height;

			if (current.type === 'iframe') {
				iframe = current.content;

				if (current.autoHeight && iframe.data('ready') === 1) {
					try {
						if (iframe[0].contentWindow.document.location) {
							inner.width( origWidth ).height(9999);

							body = iframe.contents().find('body');

							if (scrollOut) {
								body.css('overflow-x', 'hidden');
							}

							origHeight = body.outerHeight(true);
						}

					} catch (e) {}
				}

			} else if (current.autoWidth || current.autoHeight) {
				inner.addClass( 'fancybox-tmp' );

				// Set width or height in case we need to calculate only one dimension
				if (!current.autoWidth) {
					inner.width( origWidth );
				}

				if (!current.autoHeight) {
					inner.height( origHeight );
				}

				if (current.autoWidth) {
					origWidth = inner.width();
				}

				if (current.autoHeight) {
					origHeight = inner.height();
				}

				inner.removeClass( 'fancybox-tmp' );
			}

			width  = getScalar( origWidth );
			height = getScalar( origHeight );

			ratio  = origWidth / origHeight;

			// Calculations for the content
			minWidth  = getScalar(isPercentage(minWidth) ? getScalar(minWidth, 'w') - wSpace : minWidth);
			maxWidth  = getScalar(isPercentage(maxWidth) ? getScalar(maxWidth, 'w') - wSpace : maxWidth);

			minHeight = getScalar(isPercentage(minHeight) ? getScalar(minHeight, 'h') - hSpace : minHeight);
			maxHeight = getScalar(isPercentage(maxHeight) ? getScalar(maxHeight, 'h') - hSpace : maxHeight);

			// These will be used to determine if wrap can fit in the viewport
			origMaxWidth  = maxWidth;
			origMaxHeight = maxHeight;

			if (current.fitToView) {
				maxWidth  = Math.min(viewport.w - wSpace, maxWidth);
				maxHeight = Math.min(viewport.h - hSpace, maxHeight);
			}

			maxWidth_  = viewport.w - wMargin;
			maxHeight_ = viewport.h - hMargin;

			if (current.aspectRatio) {
				if (width > maxWidth) {
					width  = maxWidth;
					height = getScalar(width / ratio);
				}

				if (height > maxHeight) {
					height = maxHeight;
					width  = getScalar(height * ratio);
				}

				if (width < minWidth) {
					width  = minWidth;
					height = getScalar(width / ratio);
				}

				if (height < minHeight) {
					height = minHeight;
					width  = getScalar(height * ratio);
				}

			} else {
				width = Math.max(minWidth, Math.min(width, maxWidth));

				if (current.autoHeight && current.type !== 'iframe') {
					inner.width( width );

					height = inner.height();
				}

				height = Math.max(minHeight, Math.min(height, maxHeight));
			}

			// Try to fit inside viewport (including the title)
			if (current.fitToView) {
				inner.width( width ).height( height );

				wrap.width( width + wPadding );

				// Real wrap dimensions
				width_  = wrap.width();
				height_ = wrap.height();

				if (current.aspectRatio) {
					while ((width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight) {
						if (steps++ > 19) {
							break;
						}

						height = Math.max(minHeight, Math.min(maxHeight, height - 10));
						width  = getScalar(height * ratio);

						if (width < minWidth) {
							width  = minWidth;
							height = getScalar(width / ratio);
						}

						if (width > maxWidth) {
							width  = maxWidth;
							height = getScalar(width / ratio);
						}

						inner.width( width ).height( height );

						wrap.width( width + wPadding );

						width_  = wrap.width();
						height_ = wrap.height();
					}

				} else {
					width  = Math.max(minWidth,  Math.min(width,  width  - (width_  - maxWidth_)));
					height = Math.max(minHeight, Math.min(height, height - (height_ - maxHeight_)));
				}
			}

			if (scrollOut && scrolling === 'auto' && height < origHeight && (width + wPadding + scrollOut) < maxWidth_) {
				width += scrollOut;
			}

			inner.width( width ).height( height );

			wrap.width( width + wPadding );

			width_  = wrap.width();
			height_ = wrap.height();

			canShrink = (width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight;
			canExpand = current.aspectRatio ? (width < origMaxWidth && height < origMaxHeight && width < origWidth && height < origHeight) : ((width < origMaxWidth || height < origMaxHeight) && (width < origWidth || height < origHeight));

			$.extend(current, {
				dim : {
					width	: getValue( width_ ),
					height	: getValue( height_ )
				},
				origWidth  : origWidth,
				origHeight : origHeight,
				canShrink  : canShrink,
				canExpand  : canExpand,
				wPadding   : wPadding,
				hPadding   : hPadding,
				wrapSpace  : height_ - skin.outerHeight(true),
				skinSpace  : skin.height() - height
			});

			if (!iframe && current.autoHeight && height > minHeight && height < maxHeight && !canExpand) {
				inner.height('auto');
			}
		},

		_getPosition: function (onlyAbsolute) {
			var current  = F.current,
				viewport = F.getViewport(),
				margin   = current.margin,
				width    = F.wrap.width()  + margin[1] + margin[3],
				height   = F.wrap.height() + margin[0] + margin[2],
				rez      = {
					position: 'absolute',
					top  : margin[0],
					left : margin[3]
				};

			if (current.autoCenter && current.fixed && !onlyAbsolute && height <= viewport.h && width <= viewport.w) {
				rez.position = 'fixed';

			} else if (!current.locked) {
				rez.top  += viewport.y;
				rez.left += viewport.x;
			}

			rez.top  = getValue(Math.max(rez.top,  rez.top  + ((viewport.h - height) * current.topRatio)));
			rez.left = getValue(Math.max(rez.left, rez.left + ((viewport.w - width)  * current.leftRatio)));

			return rez;
		},

		_afterZoomIn: function () {
			var current = F.current;

			if (!current) {
				return;
			}

			F.isOpen = F.isOpened = true;

			F.wrap.css('overflow', 'visible').addClass('fancybox-opened');

			F.update();

			// Assign a click event
			if ( current.closeClick || (current.nextClick && F.group.length > 1) ) {
				F.inner.css('cursor', 'pointer').bind('click.fb', function(e) {
					if (!$(e.target).is('a') && !$(e.target).parent().is('a')) {
						e.preventDefault();

						F[ current.closeClick ? 'close' : 'next' ]();
					}
				});
			}

			// Create a close button
			if (current.closeBtn) {
				$(current.tpl.closeBtn).appendTo(F.skin).bind('click.fb', function(e) {
					e.preventDefault();

					F.close();
				});
			}

			// Create navigation arrows
			if (current.arrows && F.group.length > 1) {
				if (current.loop || current.index > 0) {
					$(current.tpl.prev).appendTo(F.outer).bind('click.fb', F.prev);
				}

				if (current.loop || current.index < F.group.length - 1) {
					$(current.tpl.next).appendTo(F.outer).bind('click.fb', F.next);
				}
			}

			F.trigger('afterShow');

			// Stop the slideshow if this is the last item
			if (!current.loop && current.index === current.group.length - 1) {
				F.play( false );

			} else if (F.opts.autoPlay && !F.player.isActive) {
				F.opts.autoPlay = false;

				F.play();
			}
		},

		_afterZoomOut: function ( obj ) {
			obj = obj || F.current;

			$('.fancybox-wrap').trigger('onReset').remove();

			$.extend(F, {
				group  : {},
				opts   : {},
				router : false,
				current   : null,
				isActive  : false,
				isOpened  : false,
				isOpen    : false,
				isClosing : false,
				wrap   : null,
				skin   : null,
				outer  : null,
				inner  : null
			});

			F.trigger('afterClose', obj);
		}
	});

	/*
	 *	Default transitions
	 */

	F.transitions = {
		getOrigPosition: function () {
			var current  = F.current,
				element  = current.element,
				orig     = current.orig,
				pos      = {},
				width    = 50,
				height   = 50,
				hPadding = current.hPadding,
				wPadding = current.wPadding,
				viewport = F.getViewport();

			if (!orig && current.isDom && element.is(':visible')) {
				orig = element.find('img:first');

				if (!orig.length) {
					orig = element;
				}
			}

			if (isQuery(orig)) {
				pos = orig.offset();

				if (orig.is('img')) {
					width  = orig.outerWidth();
					height = orig.outerHeight();
				}

			} else {
				pos.top  = viewport.y + (viewport.h - height) * current.topRatio;
				pos.left = viewport.x + (viewport.w - width)  * current.leftRatio;
			}

			if (F.wrap.css('position') === 'fixed' || current.locked) {
				pos.top  -= viewport.y;
				pos.left -= viewport.x;
			}

			pos = {
				top     : getValue(pos.top  - hPadding * current.topRatio),
				left    : getValue(pos.left - wPadding * current.leftRatio),
				width   : getValue(width  + wPadding),
				height  : getValue(height + hPadding)
			};

			return pos;
		},

		step: function (now, fx) {
			var ratio,
				padding,
				value,
				prop       = fx.prop,
				current    = F.current,
				wrapSpace  = current.wrapSpace,
				skinSpace  = current.skinSpace;

			if (prop === 'width' || prop === 'height') {
				ratio = fx.end === fx.start ? 1 : (now - fx.start) / (fx.end - fx.start);

				if (F.isClosing) {
					ratio = 1 - ratio;
				}

				padding = prop === 'width' ? current.wPadding : current.hPadding;
				value   = now - padding;

				F.skin[ prop ](  getScalar( prop === 'width' ?  value : value - (wrapSpace * ratio) ) );
				F.inner[ prop ]( getScalar( prop === 'width' ?  value : value - (wrapSpace * ratio) - (skinSpace * ratio) ) );
			}
		},

		zoomIn: function () {
			var current  = F.current,
				startPos = current.pos,
				effect   = current.openEffect,
				elastic  = effect === 'elastic',
				endPos   = $.extend({opacity : 1}, startPos);

			// Remove "position" property that breaks older IE
			delete endPos.position;

			if (elastic) {
				startPos = this.getOrigPosition();

				if (current.openOpacity) {
					startPos.opacity = 0.1;
				}

			} else if (effect === 'fade') {
				startPos.opacity = 0.1;
			}

			F.wrap.css(startPos).animate(endPos, {
				duration : effect === 'none' ? 0 : current.openSpeed,
				easing   : current.openEasing,
				step     : elastic ? this.step : null,
				complete : F._afterZoomIn
			});
		},

		zoomOut: function () {
			var current  = F.current,
				effect   = current.closeEffect,
				elastic  = effect === 'elastic',
				endPos   = {opacity : 0.1};

			if (elastic) {
				endPos = this.getOrigPosition();

				if (current.closeOpacity) {
					endPos.opacity = 0.1;
				}
			}

			F.wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : current.closeSpeed,
				easing   : current.closeEasing,
				step     : elastic ? this.step : null,
				complete : F._afterZoomOut
			});
		},

		changeIn: function () {
			var current   = F.current,
				effect    = current.nextEffect,
				startPos  = current.pos,
				endPos    = { opacity : 1 },
				direction = F.direction,
				distance  = 200,
				field;

			startPos.opacity = 0.1;

			if (effect === 'elastic') {
				field = direction === 'down' || direction === 'up' ? 'top' : 'left';

				if (direction === 'down' || direction === 'right') {
					startPos[ field ] = getValue(getScalar(startPos[ field ]) - distance);
					endPos[ field ]   = '+=' + distance + 'px';

				} else {
					startPos[ field ] = getValue(getScalar(startPos[ field ]) + distance);
					endPos[ field ]   = '-=' + distance + 'px';
				}
			}

			// Workaround for http://bugs.jquery.com/ticket/12273
			if (effect === 'none') {
				F._afterZoomIn();

			} else {
				F.wrap.css(startPos).animate(endPos, {
					duration : current.nextSpeed,
					easing   : current.nextEasing,
					complete : F._afterZoomIn
				});
			}
		},

		changeOut: function () {
			var previous  = F.previous,
				effect    = previous.prevEffect,
				endPos    = { opacity : 0.1 },
				direction = F.direction,
				distance  = 200;

			if (effect === 'elastic') {
				endPos[ direction === 'down' || direction === 'up' ? 'top' : 'left' ] = ( direction === 'up' || direction === 'left' ? '-' : '+' ) + '=' + distance + 'px';
			}

			previous.wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : previous.prevSpeed,
				easing   : previous.prevEasing,
				complete : function () {
					$(this).trigger('onReset').remove();
				}
			});
		}
	};

	/*
	 *	Overlay helper
	 */

	F.helpers.overlay = {
		defaults : {
			closeClick : true,      // if true, fancyBox will be closed when user clicks on the overlay
			speedOut   : 200,       // duration of fadeOut animation
			showEarly  : true,      // indicates if should be opened immediately or wait until the content is ready
			css        : {},        // custom CSS properties
			locked     : !isTouch,  // if true, the content will be locked into overlay
			fixed      : true       // if false, the overlay CSS position property will not be set to "fixed"
		},

		overlay : null,      // current handle
		fixed   : false,     // indicates if the overlay has position "fixed"
		el      : $('html'), // element that contains "the lock"

		// Public methods
		create : function(opts) {
			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.close();
			}

			this.overlay = $('<div class="fancybox-overlay"></div>').appendTo( F.coming ? F.coming.parent : opts.parent );
			this.fixed   = false;

			if (opts.fixed && F.defaults.fixed) {
				this.overlay.addClass('fancybox-overlay-fixed');

				this.fixed = true;
			}
		},

		open : function(opts) {
			var that = this;

			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.overlay.unbind('.overlay').width('auto').height('auto');

			} else {
				this.create(opts);
			}

			if (!this.fixed) {
				W.bind('resize.overlay', $.proxy( this.update, this) );

				this.update();
			}

			if (opts.closeClick) {
				this.overlay.bind('click.overlay', function(e) {
					if ($(e.target).hasClass('fancybox-overlay')) {
						if (F.isActive) {
							F.close();
						} else {
							that.close();
						}

						return false;
					}
				});
			}

			this.overlay.css( opts.css ).show();
		},

		close : function() {
			var scrollV, scrollH;

			W.unbind('resize.overlay');

			if (this.el.hasClass('fancybox-lock')) {
				$('.fancybox-margin').removeClass('fancybox-margin');

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.removeClass('fancybox-lock');

				W.scrollTop( scrollV ).scrollLeft( scrollH );
			}

			$('.fancybox-overlay').remove().hide();

			$.extend(this, {
				overlay : null,
				fixed   : false
			});
		},

		// Private, callbacks

		update : function () {
			var width = '100%', offsetWidth;

			// Reset width/height so it will not mess
			this.overlay.width(width).height('100%');

			// jQuery does not return reliable result for IE
			if (IE) {
				offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);

				if (D.width() > offsetWidth) {
					width = D.width();
				}

			} else if (D.width() > W.width()) {
				width = D.width();
			}

			this.overlay.width(width).height(D.height());
		},

		// This is where we can manipulate DOM, because later it would cause iframes to reload
		onReady : function (opts, obj) {
			var overlay = this.overlay;

			$('.fancybox-overlay').stop(true, true);

			if (!overlay) {
				this.create(opts);
			}

			if (opts.locked && this.fixed && obj.fixed) {
				if (!overlay) {
					this.margin = D.height() > W.height() ? $('html').css('margin-right').replace("px", "") : false;
				}

				obj.locked = this.overlay.append( obj.wrap );
				obj.fixed  = false;
			}

			if (opts.showEarly === true) {
				this.beforeShow.apply(this, arguments);
			}
		},

		beforeShow : function(opts, obj) {
			var scrollV, scrollH;

			if (obj.locked) {
				if (this.margin !== false) {
					$('*').filter(function(){
						return ($(this).css('position') === 'fixed' && !$(this).hasClass("fancybox-overlay") && !$(this).hasClass("fancybox-wrap") );
					}).addClass('fancybox-margin');

					this.el.addClass('fancybox-margin');
				}

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.addClass('fancybox-lock');

				W.scrollTop( scrollV ).scrollLeft( scrollH );
			}

			this.open(opts);
		},

		onUpdate : function() {
			if (!this.fixed) {
				this.update();
			}
		},

		afterClose: function (opts) {
			// Remove overlay if exists and fancyBox is not opening
			// (e.g., it is not being open using afterClose callback)
			//if (this.overlay && !F.isActive) {
			if (this.overlay && !F.coming) {
				this.overlay.fadeOut(opts.speedOut, $.proxy( this.close, this ));
			}
		}
	};

	/*
	 *	Title helper
	 */

	F.helpers.title = {
		defaults : {
			type     : 'float', // 'float', 'inside', 'outside' or 'over',
			position : 'bottom' // 'top' or 'bottom'
		},

		beforeShow: function (opts) {
			var current = F.current,
				text    = current.title,
				type    = opts.type,
				title,
				target;

			if ($.isFunction(text)) {
				text = text.call(current.element, current);
			}

			if (!isString(text) || $.trim(text) === '') {
				return;
			}

			title = $('<div class="fancybox-title fancybox-title-' + type + '-wrap">' + text + '</div>');

			switch (type) {
				case 'inside':
					target = F.skin;
				break;

				case 'outside':
					target = F.wrap;
				break;

				case 'over':
					target = F.inner;
				break;

				default: // 'float'
					target = F.skin;

					title.appendTo('body');

					if (IE) {
						title.width( title.width() );
					}

					title.wrapInner('<span class="child"></span>');

					//Increase bottom margin so this title will also fit into viewport
					F.current.margin[2] += Math.abs( getScalar(title.css('margin-bottom')) );
				break;
			}

			title[ (opts.position === 'top' ? 'prependTo'  : 'appendTo') ](target);
		}
	};

	// jQuery plugin initialization
	$.fn.fancybox = function (options) {
		var index,
			that     = $(this),
			selector = this.selector || '',
			run      = function(e) {
				var what = $(this).blur(), idx = index, relType, relVal;

				if (!(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey) && !what.is('.fancybox-wrap')) {
					relType = options.groupAttr || 'data-fancybox-group';
					relVal  = what.attr(relType);

					if (!relVal) {
						relType = 'rel';
						relVal  = what.get(0)[ relType ];
					}

					if (relVal && relVal !== '' && relVal !== 'nofollow') {
						what = selector.length ? $(selector) : that;
						what = what.filter('[' + relType + '="' + relVal + '"]');
						idx  = what.index(this);
					}

					options.index = idx;

					// Stop an event from bubbling if everything is fine
					if (F.open(what, options) !== false) {
						e.preventDefault();
					}
				}
			};

		options = options || {};
		index   = options.index || 0;

		if (!selector || options.live === false) {
			that.unbind('click.fb-start').bind('click.fb-start', run);

		} else {
			D.undelegate(selector, 'click.fb-start').delegate(selector + ":not('.fancybox-item, .fancybox-nav')", 'click.fb-start', run);
		}

		this.filter('[data-fancybox-start=1]').trigger('click');

		return this;
	};

	// Tests that need a body at doc ready
	D.ready(function() {
		var w1, w2;

		if ( $.scrollbarWidth === undefined ) {
			// http://benalman.com/projects/jquery-misc-plugins/#scrollbarwidth
			$.scrollbarWidth = function() {
				var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body'),
					child  = parent.children(),
					width  = child.innerWidth() - child.height( 99 ).innerWidth();

				parent.remove();

				return width;
			};
		}

		if ( $.support.fixedPosition === undefined ) {
			$.support.fixedPosition = (function() {
				var elem  = $('<div style="position:fixed;top:20px;"></div>').appendTo('body'),
					fixed = ( elem[0].offsetTop === 20 || elem[0].offsetTop === 15 );

				elem.remove();

				return fixed;
			}());
		}

		$.extend(F.defaults, {
			scrollbarWidth : $.scrollbarWidth(),
			fixed  : $.support.fixedPosition,
			parent : $('body')
		});

		//Get real width of page scroll-bar
		w1 = $(window).width();

		H.addClass('fancybox-lock-test');

		w2 = $(window).width();

		H.removeClass('fancybox-lock-test');

		$("<style type='text/css'>.fancybox-margin{margin-right:" + (w2 - w1) + "px;}</style>").appendTo("head");
	});

}(window, document, jQuery));

(function(){function n(a,b,c,d){function e(){if(f)return null;var h=b;b.childNodes&&b.childNodes.length&&!k?b=b[d?"lastChild":"firstChild"]:b[d?"previousSibling":"nextSibling"]?(b=b[d?"previousSibling":"nextSibling"],k=!1):b.parentNode&&(b=b.parentNode,b===a&&(f=!0),k=!0,e());h===c&&(f=!0);return h}var d=!!d,b=b||a[d?"lastChild":"firstChild"],f=!b,k=!1;return e}function q(a){for(var b=1;b<arguments.length;b++)for(key in arguments[b])a[key]=arguments[b][key];return a}function o(a){return(a||"").replace(/^\s+|\s+$/g,
"")}function B(a,b){var c="";document.defaultView&&document.defaultView.getComputedStyle?c=document.defaultView.getComputedStyle(a,"").getPropertyValue(b):a.currentStyle&&(b=b.replace(/\-(\w)/g,function(a,b){return b.toUpperCase()}),c=a.currentStyle[b]);return c}function r(a,b){for(;a&&!l(a,b);)a=a.parentNode;return a||null}function t(a,b){for(var c=n(a),d=null;d=c();)if(1===d.nodeType&&l(d,b))return d;return null}function u(a){for(var a=n(a),b=null;(b=a())&&3!==b.nodeType;);return b}function v(a,
b){if(a.getElementsByClassName)return a.getElementsByClassName(b);for(var c=[],d,e=n(a);d=e();)1==d.nodeType&&l(d,b)&&c.push(d);return c}function p(a){for(var b=[],c=n(a);a=c();)3===a.nodeType&&b.push(a);return b}function w(a){return RegExp("(^|\\s+)"+a+"(?:$|\\s+)","g")}function l(a,b){return w(b).test(a.className)}function x(a,b){w(b).test(a.className)||(a.className=a.className+" "+b)}function s(a,b){var c=w(b);c.test(a.className)&&(a.className=o(a.className.replace(c,"$1")))}function z(a,b){for(var c=
0,d=b.length;c<d;c++)if(b[c]===a)return c;return-1}function g(a,b,c){a.addEventListener?a.addEventListener(b,c,!1):a.attachEvent&&a.attachEvent("on"+b,c)}function y(a){a.preventDefault?a.preventDefault():a.returnValue=!1}function C(a){if(null==a.pageX){var b=document.documentElement,c=document.body;return{x:a.clientX+(b&&b.scrollLeft||c&&c.scrollLeft||0)-(b.clientLeft||0),y:a.clientY+(b&&b.scrollTop||c&&c.scrollTop||0)-(b.clientTop||0)}}return{x:a.pageX,y:a.pageY}}var m=function(){};m.prototype={setHash:function(a){window.location.hash=
a},getHash:function(){return window.location.hash},addHashchange:function(a){g(window,"hashchange",a)}};var j=function(a){a=a||{};"select_message"in a&&(a.selectMessage=a.select_message);"enable_haschange"in a&&(a.enableHaschange=a.enable_haschange);"is_block"in a&&(a.isBlock=a.is_block);this.options=q({},j.defaultOptions,a);q(this,{counter:0,savedSel:[],ranges:{},childs:[],blocks:{}});this.init()};j.version="22.05.2012-12:08:33";j.LocationHandler=m;j.defaultOptions={regexp:"[^\\s,;:\u2013.!?<>\u2026\\n\u00a0\\*]+",
selectable:"selectable-content",marker:"txtselect_marker",ignored:null,selectMessage:null,location:new m,validate:!1,enableHaschange:!0,onMark:null,onUnmark:null,onHashRead:function(){var a=t(this.selectable,"user_selection_true");a&&!this.hashWasRead&&(this.hashWasRead=!0,window.setTimeout(function(){for(var b=0,c=0;a;)b+=a.offsetLeft,c+=a.offsetTop,a=a.offsetParent;window.scrollTo(b,c-150)},1))},isBlock:function(a){return"BR"==a.nodeName||-1==z(B(a,"display"),["inline","none"])}};j.prototype={init:function(){function a(a){y(a);
a.stopPropagation?a.stopPropagation():a.cancelBubble=!0;a=a.target||a.srcElement;if(!l(this,"masha-marker-bar")||l(a,"masha-social")||l(a,"masha-marker"))if(s(b.marker,"show"),b.rangeIsSelectable()&&(b.addSelection(),b.updateHash(),b.options.onMark&&b.options.onMark.call(b),b.options.selectMessage&&b._showMessage(),l(a,"masha-social")&&(a=a.getAttribute("data-pattern"))))a=a.replace("{url}",encodeURIComponent(window.location.toString())),b.openShareWindow(a)}this.selectable="string"==typeof this.options.selectable?
document.getElementById(this.options.selectable):this.options.selectable;"string"==typeof this.options.marker?(this.marker=document.getElementById(this.options.marker),null===this.marker&&(this.marker=document.createElement("a"),this.marker.setAttribute("id",this.options.marker),this.marker.setAttribute("href","#"),document.body.appendChild(this.marker))):this.marker=this.options.marker;if("string"!=typeof this.options.regexp)throw"regexp is set as string";this.regexp=RegExp(this.options.regexp,"ig");
var b=this;this.selectable&&(this.isIgnored=this.constructIgnored(this.options.ignored),this.options.selectMessage&&this.initMessage(),this.enumerateElements(),"ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch?g(this.selectable,"touchend",function(){window.setTimeout(function(){var a=window.getSelection();if(a.rangeCount){a=a.getRangeAt(0).getClientRects();if(a=a[a.length-1])var d={x:a.left+a.width+document.body.scrollLeft,y:a.top+a.height/2+document.body.scrollTop};
b.showMarker(d)}},1)}):g(this.selectable,"mouseup",function(a){var d=C(a);window.setTimeout(function(){b.showMarker(d)},1)}),g(this.marker,"click",a),g(this.marker,"touchend",a),g(document,"click",function(a){(a.target||a.srcElement)!=b.marker&&s(b.marker,"show")}),this.options.enableHaschange&&this.options.location.addHashchange(function(){if(b.lastHash!=b.options.location.getHash()){var a=[],d;for(d in b.ranges)a.push(d);b.deleteSelections(a);b.readHash()}}),this.readHash())},openShareWindow:function(a){window.open(a,
"","status=no,toolbar=no,menubar=no,width=800,height=400")},getMarkerCoords:function(a,b){return{x:b.x+5,y:b.y-33}},getPositionChecksum:function(a){for(var b="",c=0;3>c;c++){var d=(a()||"").charAt(0);d&&(d="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".charAt(d.charCodeAt(0)%62));b+=d}return b},showMarker:function(a){var b=RegExp(this.options.regexp,"g"),c=window.getSelection().toString();""!=c&&b.test(c)&&this.rangeIsSelectable()&&(a=this.getMarkerCoords(this.marker,a),this.marker.style.top=
a.y+"px",this.marker.style.left=a.x+"px",x(this.marker,"show"))},deleteSelections:function(a){for(var b=a.length;b--;){var c=a[b],d=v(this.selectable,c),e=t(d[d.length-1],"closewrap");e.parentNode.removeChild(e);this.removeTextSelection(d);delete this.ranges[c]}},removeTextSelection:function(a){for(var b=a.length;b--;){for(var c=a[b],d=0;d<c.childNodes.length;d++)c.parentNode.insertBefore(c.childNodes[d],c);c.parentNode.removeChild(c)}},isInternal:function(a){for(;a.parentNode;){if(a==this.selectable)return!0;
a=a.parentNode}return!1},_siblingNode:function(a,b,c,d,e){for(e=e||this.regexp;a.parentNode&&this.isInternal(a);){for(;a[b+"Sibling"];){for(a=a[b+"Sibling"];1==a.nodeType&&a.childNodes.length;)a=a[c+"Child"];if(3==a.nodeType&&null!=a.data.match(e))return{_container:a,_offset:d*a.data.length}}a=a.parentNode}return null},prevNode:function(a,b){return this._siblingNode(a,"previous","last",1,b)},nextNode:function(a,b){return this._siblingNode(a,"next","first",0,b)},wordCount:function(a){var b=0;if(3==
a.nodeType)(a=a.nodeValue.match(this.regexp))&&(b+=a.length);else if(a.childNodes&&a.childNodes.length){a=p(a);for(i=a.length;i--;)b+=a[i].nodeValue.match(this.regexp).length}return b},words:function(a,b,c){1==a.nodeType&&(a=u(a));b=a.data.substring(0,b).match(this.regexp);null!=b?("start"==c&&(b=b.length+1),"end"==c&&(b=b.length)):b=1;for(var c=a,a=this.getNum(a),d=this.getFirstTextNode(a);c&&c!=d;)c=this.prevNode(c,/.*/)._container,b+=this.wordCount(c);return a+":"+b},symbols:function(a){var b=
0;if(3==a.nodeType)b=a.nodeValue.length;else if(a.childNodes&&a.childNodes.length)for(var a=p(a),c=a.length;c--;)b+=a[c].nodeValue.length;return b},updateHash:function(){var a=[];for(key in this.ranges)a.push(this.ranges[key]);this.lastHash=a="#sel="+a.join(";");this.options.location.setHash(a)},readHash:function(){var a=this.splittedHash();if(a){for(var b=0;b<a.length;b++)this.deserializeSelection(a[b]);this.updateHash();this.options.onHashRead&&this.options.onHashRead.call(this)}},splittedHash:function(){var a=
this.options.location.getHash();if(!a)return null;a=a.replace(/^#/,"").replace(/;+$/,"");if(!/^sel\=(?:\d+\:\d+(?:\:[^:;]*)?\,|%2C\d+\:\d+(?:\:[^:;]*)?;)*\d+\:\d+(?:\:[^:;]*)?\,|%2C\d+\:\d+(?:\:[^:;]*)?$/.test(a))return null;a=a.substring(4,a.length);return a.split(";")},deserializeSelection:function(a){var b=window.getSelection();0<b.rangeCount&&b.removeAllRanges();(a=this.deserializeRange(a))&&this.addSelection(a)},deserializeRange:function(a){var b=/^([0-9A-Za-z:]+)(?:,|%2C)([0-9A-Za-z:]+)$/.exec(a),
c=b[1].split(":"),b=b[2].split(":");if(parseInt(c[0],10)<parseInt(b[0],10)||c[0]==b[0]&&parseInt(c[1],10)<=parseInt(b[1],10)){var d=this.deserializePosition(c,"start"),e=this.deserializePosition(b,"end");if(d.node&&e.node){var f=document.createRange();f.setStart(d.node,d.offset);f.setEnd(e.node,e.offset);if(!this.options.validate||this.validateRange(f,c[2],b[2]))return f}}window.console&&"function"==typeof window.console.warn&&window.console.warn("Cannot deserialize range: "+a);return null},validateRange:function(a,
b,c){var d=!0,e;b&&(e=this.getPositionChecksum(a.getWordIterator(this.regexp)),d=d&&b==e);c&&(e=this.getPositionChecksum(a.getWordIterator(this.regexp,!0)),d=d&&c==e);return d},getRangeChecksum:function(a){sum1=this.getPositionChecksum(a.getWordIterator(this.regexp));sum2=this.getPositionChecksum(a.getWordIterator(this.regexp,!0));return[sum1,sum2]},deserializePosition:function(a,b){for(var c=this.blocks[parseInt(a[0],10)],d,e=0;c;){for(var f=RegExp(this.options.regexp,"ig");null!=(myArray=f.exec(c.data));)if(e++,
e==a[1])return"start"==b&&(d=myArray.index),"end"==b&&(d=f.lastIndex),{node:c,offset:parseInt(d,10)};(c=(c=this.nextNode(c,/.*/))?c._container:null)&&this.isFirstTextNode(c)&&(c=null)}return{node:null,offset:0}},serializeRange:function(a){var b=this.words(a.startContainer,a.startOffset,"start"),c=this.words(a.endContainer,a.endOffset,"end");this.options.validate&&(a=this.getRangeChecksum(a),b+=":"+a[0],c+=":"+a[1]);return b+","+c},checkSelection:function(a){this.checkPosition(a,a.startOffset,a.startContainer,
"start");this.checkPosition(a,a.endOffset,a.endContainer,"end");this.checkBrackets(a);this.checkSentence(a);return a},checkPosition:function(a,b,c,d){function e(a){return null!=a.match(j.regexp)}function f(a){return null==a.match(j.regexp)}function k(a,b,c){for(;0<b&&c(a.data.charAt(b-1));)b--;return b}function h(a,b,c){for(;b<a.data.length&&c(a.data.charAt(b));)b++;return b}var j=this;if(1==c.nodeType&&0<b)if(b<c.childNodes.length)c=c.childNodes[b],b=0;else{var g=p(c);g.length&&(c=g[g.length-1],
b=c.data.length)}if("start"==d){if(1==c.nodeType&&""!=o(c.textContent||c.innerText))c=u(c),b=0;if(3!=c.nodeType||null==c.data.substring(b).match(this.regexp))b=this.nextNode(c),c=b._container,b=b._offset;b=h(c,b,f);b=k(c,b,e);a.setStart(c,b)}if("end"==d){if(1==c.nodeType&&""!=o(c.textContent||c.innerText)&&0!=b)c=c.childNodes[a.endOffset-1],g=p(c),c=g[g.length-1],b=c.data.length;if(3!=c.nodeType||null==c.data.substring(0,b).match(this.regexp))b=this.prevNode(c),c=b._container,b=b._offset;b=k(c,b,
f);b=h(c,b,e);a.setEnd(c,b)}},checkBrackets:function(a){this._checkBrackets(a,"(",")",/\(|\)/g,/\(x*\)/g);this._checkBrackets(a,"\u00ab","\u00bb",/\\u00ab|\\u00bb/g,/\u00abx*\u00bb/g)},_checkBrackets:function(a,b,c,d,e){var f=a.toString();if(d=f.match(d)){for(var d=d.join(""),k=d.length+1;d.length<k;)k=d.length,d=d.replace(e,"x");d.charAt(d.length-1)==c&&f.charAt(f.length-1)==c&&(1==a.endOffset?(c=this.prevNode(a.endContainer),a.setEnd(c.container,c.offset)):a.setEnd(a.endContainer,a.endOffset-1));
d.charAt(0)==b&&f.charAt(0)==b&&(a.startOffset==a.startContainer.data.length?(c=this.nextNode(a.endContainer),a.setStart(c.container,c.offset)):a.setStart(a.startContainer,a.startOffset+1))}},checkSentence:function(a){function b(){a.setEnd(c._container,c._offset+1)}var c,d;if(a.endOffset==a.endContainer.data.length){c=this.nextNode(a.endContainer,/.*/);if(!c)return null;d=c._container.data.charAt(0)}else c={_container:a.endContainer,_offset:a.endOffset},d=a.endContainer.data.charAt(a.endOffset);if(d.match(/\.|\?|\!/)){d=
a.toString();if(d.match(/(\.|\?|\!)\s+[A-Z\u0410-\u042f\u0401]/)||0==a.startOffset&&a.startContainer.previousSibling&&1==a.startContainer.previousSibling.nodeType&&"selection_index"==a.startContainer.previousSibling.className)return b();for(var e,f=a.getElementIterator();e=f();)if(1==e.nodeType&&"selection_index"==e.className)return b();return d.charAt(0).match(/[A-Z\u0410-\u042f\u0401]/)&&(d=a.startContainer.data.substring(0,a.startOffset),d.match(/\S/)||(d=this.prevNode(a.startContainer,/\W*/)._container.data),
d=o(d),d.charAt(d.length-1).match(/(\.|\?|\!)/))?b():null}},mergeSelections:function(a){var b=[],c=a.getElementIterator(),d=c(),e=d,f=r(d,"user_selection_true");f&&(f=/(num\d+)(?:$| )/.exec(f.className)[1],a.setStart(u(t(this.selectable,f)),0),b.push(f));for(;d;)1==d.nodeType&&l(d,"user_selection_true")&&(e=/(num\d+)(?:$|)/.exec(d.className)[0],-1==z(e,b)&&b.push(e)),e=d,d=c();if(e=r(e,"user_selection_true"))e=/(num\d+)(?:$| )/.exec(e.className)[1],c=(c=v(this.selectable,e))?c[c.length-1]:null,c=
p(c),c=c[c.length-1],a.setEnd(c,c.length);b.length&&(c=a.startContainer,d=a.startOffset,e=a.endContainer,f=a.endOffset,this.deleteSelections(b),a.setStart(c,d),a.setEnd(e,f));return a},addSelection:function(a){var a=a||this.getFirstRange(),a=this.checkSelection(a),a=this.mergeSelections(a),b="num"+this.counter;this.ranges[b]=this.serializeRange(a);a.wrapSelection(b+" user_selection_true");this.addSelectionEvents(b)},addSelectionEvents:function(a){for(var b=!1,c=this,d=v(this.selectable,a),e=d.length;e--;)g(d[e],
"mouseover",function(){for(var a=d.length;a--;)x(d[a],"hover");window.clearTimeout(b)}),g(d[e],"mouseout",function(a){for(a=a.relatedTarget;a&&a.parentNode&&a.className!=this.className;)a=a.parentNode;if(!a||a.className!=this.className)b=window.setTimeout(function(){for(var a=d.length;a--;)s(d[a],"hover")},2E3)});e=document.createElement("a");e.className="txtsel_close";e.href="#";var f=document.createElement("span");f.className="closewrap";f.appendChild(e);g(e,"click",function(b){y(b);c.deleteSelections([a]);
c.updateHash();c.options.onUnmark&&c.options.onUnmark.call(c)});d[d.length-1].appendChild(f);this.counter++;window.getSelection().removeAllRanges()},getFirstRange:function(){var a=window.getSelection();return a.rangeCount?a.getRangeAt(0):null},enumerateElements:function(){function a(b){for(var b=b.childNodes,e=!1,f=!1,k=0;k<b.length;++k){var h=b.item(k),g=h.nodeType;if(3!=g||h.nodeValue.match(c.regexp))3==g?f||(j.captureCount++,e=document.createElement("span"),e.id="selection_index"+j.captureCount,
e.className="selection_index",h.parentNode.insertBefore(e,h),k++,c.blocks[j.captureCount]=h,e=f=!0):1==g&&!c.isIgnored(h)&&(c.options.isBlock(h)?(h=a(h),e=e||h,f=!1):f||(f=a(h),e=e||f))}return e}var b=this.selectable;j.captureCount=j.captureCount||0;var c=this;a(b)},isFirstTextNode:function(a){for(var a=[a.previousSibling,a.parentNode.previousSibling],b=a.length;b--;)if(a[b]&&1==a[b].nodeType&&"selection_index"==a[b].className)return!0;return!1},getFirstTextNode:function(a){return!a?null:(a=document.getElementById("selection_index"+
a))?1==a.nextSibling.nodeType?a.nextSibling.childNodes[0]:a.nextSibling:null},getNum:function(a){for(;a.parentNode;){for(;a.previousSibling;){for(a=a.previousSibling;1==a.nodeType&&a.childNodes.length;)a=a.lastChild;if(1==a.nodeType&&"selection_index"==a.className)return a.id.replace("selection_index","")}a=a.parentNode}return null},constructIgnored:function(a){if("function"==typeof a)return a;if("string"==typeof a){for(var b=[],c=[],d=[],a=a.split(","),e=0;e<a.length;e++){var f=o(a[e]);"#"==f.charAt(0)?
b.push(f.substr(1)):"."==f.charAt(0)?c.push(f.substr(1)):d.push(f)}return function(a){var e;for(e=b.length;e--;)if(a.id==b[e])return!0;for(e=c.length;e--;)if(l(a,c[e]))return!0;for(e=d.length;e--;)if(a.tagName==d[e].toUpperCase())return!0;return!1}}return function(){return!1}},rangeIsSelectable:function(){var a,b,c,d=!0,e=this.getFirstRange();if(!e)return!1;for(e=e.getElementIterator();a=e();)if(3==a.nodeType&&null!=a.data.match(this.regexp)&&(b=b||a,c=a),a=d&&3==a.nodeType?a.parentNode:a,d=!1,1==
a.nodeType){for(;a!=this.selectable&&a.parentNode;){if(this.isIgnored(a))return!1;a=a.parentNode}if(a!=this.selectable)return!1}b=r(b,"user_selection_true");c=r(c,"user_selection_true");return b&&c?(d=/(?:^| )(num\d+)(?:$| )/,d.exec(b.className)[1]!=d.exec(c.className)[1]):!0},initMessage:function(){var a=this;this.msg="string"==typeof this.options.selectMessage?document.getElementById(this.options.selectMessage):this.options.selectMessage;this.close_button=this.getCloseButton();this.msg_autoclose=
null;g(this.close_button,"click",function(b){y(b);a.hideMessage();a.saveMessageClosed();clearTimeout(a.msg_autoclose)})},showMessage:function(){x(this.msg,"show")},hideMessage:function(){s(this.msg,"show")},getCloseButton:function(){return this.msg.getElementsByTagName("a")[0]},getMessageClosed:function(){return window.localStorage?!!localStorage.masha_warning:!!document.cookie.match(/(?:^|;)\s*masha-warning=/)},saveMessageClosed:function(){window.localStorage?localStorage.masha_warning="true":this.getMessageClosed()||
(document.cookie+="; masha-warning=true")},_showMessage:function(){var a=this;this.getMessageClosed()||(this.showMessage(),clearTimeout(this.msg_autoclose),this.msg_autoclose=setTimeout(function(){a.hideMessage()},1E4))}};m=window.Range||document.createRange().constructor;m.prototype.splitBoundaries=function(){var a=this.startContainer,b=this.startOffset,c=this.endContainer,d=this.endOffset,e=a===c;3==c.nodeType&&d<c.length&&c.splitText(d);3==a.nodeType&&0<b&&(a=a.splitText(b),e&&(d-=b,c=a),b=0);
this.setStart(a,b);this.setEnd(c,d)};m.prototype.getTextNodes=function(){for(var a=this.getElementIterator(),b=[],c;c=a();)3==c.nodeType&&b.push(c);return b};m.prototype.getElementIterator=function(a){return a?n(null,this.endContainer,this.startContainer,!0):n(null,this.startContainer,this.endContainer)};m.prototype.getWordIterator=function(a,b){var c=this.getElementIterator(b),d,e=0,f=0,g=!1,h,j=this;return function(){if(e==f&&!g){do{do d=c();while(d&&3!=d.nodeType);g=!d;g||(value=d.nodeValue,d==
j.endContainer&&(value=value.substr(0,j.endOffset)),d==j.startContainer&&(value=value.substr(j.startOffset)),h=value.match(a))}while(d&&!h);h&&(e=b?0:h.length-1,f=b?h.length-1:0)}else b?f--:f++;return g?null:h[f]}};m.prototype.wrapSelection=function(a){this.splitBoundaries();for(var b=this.getTextNodes(),c=b.length;c--;){var d=document.createElement("span");d.className=a;b[c].parentNode.insertBefore(d,b[c]);d.appendChild(b[c])}};var A=function(a){this.prefix=a};A.prototype={setHash:function(a){a=
a.replace("sel",this.prefix).replace(/^#/,"");a.length==this.prefix.length+1&&(a="");var b=this.getHashPart();window.location.hash.replace(/^#\|?/,"");a=b?window.location.hash.replace(b,a):window.location.hash+"|"+a;a="#"+a.replace("||","").replace(/^#?\|?|\|$/g,"");window.location.hash=a},addHashchange:j.LocationHandler.prototype.addHashchange,getHashPart:function(){for(var a=window.location.hash.replace(/^#\|?/,"").split(/\||%7C/),b=0;b<a.length;b++)if(a[b].substr(0,this.prefix.length+1)==this.prefix+
"=")return a[b];return""},getHash:function(){return this.getHashPart().replace(this.prefix,"sel")}};window.MaSha=j;window.jQuery&&(window.jQuery.fn.masha=function(a){a=a||{};a=q({selectable:this[0]},a);return new j(a)});window.MultiMaSha=function(a,b,c){for(var b=b||function(a){return a.id},d=0;d<a.length;d++){var e=a[d],f=b(e);if(f){e=q({},c||{},{selectable:e,location:new A(f)});new j(e)}}}})();

/**
 * @license AngularJS v1.0.1
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {
'use strict';

/**
 * @ngdoc overview
 * @name ngSanitize
 * @description
 */

/*
 * HTML Parser By Misko Hevery (misko@hevery.com)
 * based on:  HTML Parser By John Resig (ejohn.org)
 * Original code by Erik Arvidsson, Mozilla Public License
 * http://erik.eae.net/simplehtmlparser/simplehtmlparser.js
 *
 * // Use like so:
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 */


/**
 * @ngdoc service
 * @name ngSanitize.$sanitize
 * @function
 *
 * @description
 *   The input is sanitized by parsing the html into tokens. All safe tokens (from a whitelist) are
 *   then serialized back to properly escaped html string. This means that no unsafe input can make
 *   it into the returned string, however, since our parser is more strict than a typical browser
 *   parser, it's possible that some obscure input, which would be recognized as valid HTML by a
 *   browser, won't make it through the sanitizer.
 *
 * @param {string} html Html input.
 * @returns {string} Sanitized html.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             '<p style="color:blue">an html\n' +
             '<em onmouseover="this.textContent=\'PWN3D!\'">click here</em>\n' +
             'snippet</p>';
         }
       </script>
       <div ng-controller="Ctrl">
          Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
           <table>
             <tr>
               <td>Filter</td>
               <td>Source</td>
               <td>Rendered</td>
             </tr>
             <tr id="html-filter">
               <td>html filter</td>
               <td>
                 <pre>&lt;div ng-bind-html="snippet"&gt;<br/>&lt;/div&gt;</pre>
               </td>
               <td>
                 <div ng-bind-html="snippet"></div>
               </td>
             </tr>
             <tr id="escaped-html">
               <td>no filter</td>
               <td><pre>&lt;div ng-bind="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind="snippet"></div></td>
             </tr>
             <tr id="html-unsafe-filter">
               <td>unsafe html filter</td>
               <td><pre>&lt;div ng-bind-html-unsafe="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind-html-unsafe="snippet"></div></td>
             </tr>
           </table>
         </div>
     </doc:source>
     <doc:scenario>
       it('should sanitize the html snippet ', function() {
         expect(using('#html-filter').element('div').html()).
           toBe('<p>an html\n<em>click here</em>\nsnippet</p>');
       });

       it('should escape snippet without any filter', function() {
         expect(using('#escaped-html').element('div').html()).
           toBe("&lt;p style=\"color:blue\"&gt;an html\n" +
                "&lt;em onmouseover=\"this.textContent='PWN3D!'\"&gt;click here&lt;/em&gt;\n" +
                "snippet&lt;/p&gt;");
       });

       it('should inline raw snippet if filtered as unsafe', function() {
         expect(using('#html-unsafe-filter').element("div").html()).
           toBe("<p style=\"color:blue\">an html\n" +
                "<em onmouseover=\"this.textContent='PWN3D!'\">click here</em>\n" +
                "snippet</p>");
       });

       it('should update', function() {
         input('snippet').enter('new <b>text</b>');
         expect(using('#html-filter').binding('snippet')).toBe('new <b>text</b>');
         expect(using('#escaped-html').element('div').html()).toBe("new &lt;b&gt;text&lt;/b&gt;");
         expect(using('#html-unsafe-filter').binding("snippet")).toBe('new <b>text</b>');
       });
     </doc:scenario>
   </doc:example>
 */
var $sanitize = function(html) {
  var buf = [];
    htmlParser(html, htmlSanitizeWriter(buf));
    return buf.join('');
};


// Regular Expressions for parsing tags and attributes
var START_TAG_REGEXP = /^<\s*([\w:-]+)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*>/,
  END_TAG_REGEXP = /^<\s*\/\s*([\w:-]+)[^>]*>/,
  ATTR_REGEXP = /([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,
  BEGIN_TAG_REGEXP = /^</,
  BEGING_END_TAGE_REGEXP = /^<\s*\//,
  COMMENT_REGEXP = /<!--(.*?)-->/g,
  CDATA_REGEXP = /<!\[CDATA\[(.*?)]]>/g,
  URI_REGEXP = /^((ftp|https?):\/\/|mailto:|#)/,
  NON_ALPHANUMERIC_REGEXP = /([^\#-~| |!])/g; // Match everything outside of normal chars and " (quote character)


// Good source of info about elements and attributes
// http://dev.w3.org/html5/spec/Overview.html#semantics
// http://simon.html5.org/html-elements

// Safe Void Elements - HTML5
// http://dev.w3.org/html5/spec/Overview.html#void-elements
var voidElements = makeMap("area,br,col,hr,img,wbr");

// Elements that you can, intentionally, leave open (and which close themselves)
// http://dev.w3.org/html5/spec/Overview.html#optional-tags
var optionalEndTagBlockElements = makeMap("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),
    optionalEndTagInlineElements = makeMap("rp,rt"),
    optionalEndTagElements = angular.extend({}, optionalEndTagInlineElements, optionalEndTagBlockElements);

// Safe Block Elements - HTML5
var blockElements = angular.extend({}, optionalEndTagBlockElements, makeMap("address,article,aside," +
        "blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6," +
        "header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul"));

// Inline Elements - HTML5
var inlineElements = angular.extend({}, optionalEndTagInlineElements, makeMap("a,abbr,acronym,b,bdi,bdo," +
        "big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small," +
        "span,strike,strong,sub,sup,time,tt,u,var"));


// Special Elements (can contain anything)
var specialElements = makeMap("script,style");

var validElements = angular.extend({}, voidElements, blockElements, inlineElements, optionalEndTagElements);

//Attributes that have href and hence need to be sanitized
var uriAttrs = makeMap("background,cite,href,longdesc,src,usemap");
var validAttrs = angular.extend({}, uriAttrs, makeMap(
    'abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,'+
    'color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,'+
    'ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,'+
    'scope,scrolling,shape,span,start,summary,target,title,type,'+
    'valign,value,vspace,width'));

function makeMap(str) {
  var obj = {}, items = str.split(','), i;
  for (i = 0; i < items.length; i++) obj[items[i]] = true;
  return obj;
}


/**
 * @example
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 * @param {string} html string
 * @param {object} handler
 */
function htmlParser( html, handler ) {
  var index, chars, match, stack = [], last = html;
  stack.last = function() { return stack[ stack.length - 1 ]; };

  while ( html ) {
    chars = true;

    // Make sure we're not in a script or style element
    if ( !stack.last() || !specialElements[ stack.last() ] ) {

      // Comment
      if ( html.indexOf("<!--") === 0 ) {
        index = html.indexOf("-->");

        if ( index >= 0 ) {
          if (handler.comment) handler.comment( html.substring( 4, index ) );
          html = html.substring( index + 3 );
          chars = false;
        }

      // end tag
      } else if ( BEGING_END_TAGE_REGEXP.test(html) ) {
        match = html.match( END_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( END_TAG_REGEXP, parseEndTag );
          chars = false;
        }

      // start tag
      } else if ( BEGIN_TAG_REGEXP.test(html) ) {
        match = html.match( START_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( START_TAG_REGEXP, parseStartTag );
          chars = false;
        }
      }

      if ( chars ) {
        index = html.indexOf("<");

        var text = index < 0 ? html : html.substring( 0, index );
        html = index < 0 ? "" : html.substring( index );

        if (handler.chars) handler.chars( decodeEntities(text) );
      }

    } else {
      html = html.replace(new RegExp("(.*)<\\s*\\/\\s*" + stack.last() + "[^>]*>", 'i'), function(all, text){
        text = text.
          replace(COMMENT_REGEXP, "$1").
          replace(CDATA_REGEXP, "$1");

        if (handler.chars) handler.chars( decodeEntities(text) );

        return "";
      });

      parseEndTag( "", stack.last() );
    }

    if ( html == last ) {
      throw "Parse Error: " + html;
    }
    last = html;
  }

  // Clean up any remaining tags
  parseEndTag();

  function parseStartTag( tag, tagName, rest, unary ) {
    tagName = angular.lowercase(tagName);
    if ( blockElements[ tagName ] ) {
      while ( stack.last() && inlineElements[ stack.last() ] ) {
        parseEndTag( "", stack.last() );
      }
    }

    if ( optionalEndTagElements[ tagName ] && stack.last() == tagName ) {
      parseEndTag( "", tagName );
    }

    unary = voidElements[ tagName ] || !!unary;

    if ( !unary )
      stack.push( tagName );

    var attrs = {};

    rest.replace(ATTR_REGEXP, function(match, name, doubleQuotedValue, singleQoutedValue, unqoutedValue) {
      var value = doubleQuotedValue
        || singleQoutedValue
        || unqoutedValue
        || '';

      attrs[name] = decodeEntities(value);
    });
    if (handler.start) handler.start( tagName, attrs, unary );
  }

  function parseEndTag( tag, tagName ) {
    var pos = 0, i;
    tagName = angular.lowercase(tagName);
    if ( tagName )
      // Find the closest opened tag of the same type
      for ( pos = stack.length - 1; pos >= 0; pos-- )
        if ( stack[ pos ] == tagName )
          break;

    if ( pos >= 0 ) {
      // Close all the open elements, up the stack
      for ( i = stack.length - 1; i >= pos; i-- )
        if (handler.end) handler.end( stack[ i ] );

      // Remove the open elements from the stack
      stack.length = pos;
    }
  }
}

/**
 * decodes all entities into regular string
 * @param value
 * @returns {string} A string with decoded entities.
 */
var hiddenPre=document.createElement("pre");
function decodeEntities(value) {
  hiddenPre.innerHTML=value.replace(/</g,"&lt;");
  return hiddenPre.innerText || hiddenPre.textContent || '';
}

/**
 * Escapes all potentially dangerous characters, so that the
 * resulting string can be safely inserted into attribute or
 * element text.
 * @param value
 * @returns escaped text
 */
function encodeEntities(value) {
  return value.
    replace(/&/g, '&amp;').
    replace(NON_ALPHANUMERIC_REGEXP, function(value){
      return '&#' + value.charCodeAt(0) + ';';
    }).
    replace(/</g, '&lt;').
    replace(/>/g, '&gt;');
}

/**
 * create an HTML/XML writer which writes to buffer
 * @param {Array} buf use buf.jain('') to get out sanitized html string
 * @returns {object} in the form of {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * }
 */
function htmlSanitizeWriter(buf){
  var ignore = false;
  var out = angular.bind(buf, buf.push);
  return {
    start: function(tag, attrs, unary){
      tag = angular.lowercase(tag);
      if (!ignore && specialElements[tag]) {
        ignore = tag;
      }
      if (!ignore && validElements[tag] == true) {
        out('<');
        out(tag);
        angular.forEach(attrs, function(value, key){
          var lkey=angular.lowercase(key);
          if (validAttrs[lkey]==true && (uriAttrs[lkey]!==true || value.match(URI_REGEXP))) {
            out(' ');
            out(key);
            out('="');
            out(encodeEntities(value));
            out('"');
          }
        });
        out(unary ? '/>' : '>');
      }
    },
    end: function(tag){
        tag = angular.lowercase(tag);
        if (!ignore && validElements[tag] == true) {
          out('</');
          out(tag);
          out('>');
        }
        if (tag == ignore) {
          ignore = false;
        }
      },
    chars: function(chars){
        if (!ignore) {
          out(encodeEntities(chars));
        }
      }
  };
}


// define ngSanitize module and register $sanitize service
angular.module('ngSanitize', []).value('$sanitize', $sanitize);

/**
 * @ngdoc directive
 * @name ngSanitize.directive:ngBindHtml
 *
 * @description
 * Creates a binding that will sanitize the result of evaluating the `expression` with the
 * {@link ngSanitize.$sanitize $sanitize} service and innerHTML the result into the current element.
 *
 * See {@link ngSanitize.$sanitize $sanitize} docs for examples.
 *
 * @element ANY
 * @param {expression} ngBindHtml {@link guide/expression Expression} to evaluate.
 */
angular.module('ngSanitize').directive('ngBindHtml', ['$sanitize', function($sanitize) {
  return function(scope, element, attr) {
    element.addClass('ng-binding').data('$binding', attr.ngBindHtml);
    scope.$watch(attr.ngBindHtml, function(value) {
      value = $sanitize(value);
      element.html(value || '');
    });
  };
}]);
/**
 * @ngdoc filter
 * @name ngSanitize.filter:linky
 * @function
 *
 * @description
 *   Finds links in text input and turns them into html links. Supports http/https/ftp/mailto and
 *   plain email address links.
 *
 * @param {string} text Input text.
 * @returns {string} Html-linkified text.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             'Pretty text with some links:\n'+
             'http://angularjs.org/,\n'+
             'mailto:us@somewhere.org,\n'+
             'another@somewhere.org,\n'+
             'and one more: ftp://127.0.0.1/.';
         }
       </script>
       <div ng-controller="Ctrl">
       Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
       <table>
         <tr>
           <td>Filter</td>
           <td>Source</td>
           <td>Rendered</td>
         </tr>
         <tr id="linky-filter">
           <td>linky filter</td>
           <td>
             <pre>&lt;div ng-bind-html="snippet | linky"&gt;<br>&lt;/div&gt;</pre>
           </td>
           <td>
             <div ng-bind-html="snippet | linky"></div>
           </td>
         </tr>
         <tr id="escaped-html">
           <td>no filter</td>
           <td><pre>&lt;div ng-bind="snippet"&gt;<br>&lt;/div&gt;</pre></td>
           <td><div ng-bind="snippet"></div></td>
         </tr>
       </table>
     </doc:source>
     <doc:scenario>
       it('should linkify the snippet with urls', function() {
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('Pretty text with some links:&#10;' +
                '<a href="http://angularjs.org/">http://angularjs.org/</a>,&#10;' +
                '<a href="mailto:us@somewhere.org">us@somewhere.org</a>,&#10;' +
                '<a href="mailto:another@somewhere.org">another@somewhere.org</a>,&#10;' +
                'and one more: <a href="ftp://127.0.0.1/">ftp://127.0.0.1/</a>.');
       });

       it ('should not linkify snippet without the linky filter', function() {
         expect(using('#escaped-html').binding('snippet')).
           toBe("Pretty text with some links:\n" +
                "http://angularjs.org/,\n" +
                "mailto:us@somewhere.org,\n" +
                "another@somewhere.org,\n" +
                "and one more: ftp://127.0.0.1/.");
       });

       it('should update', function() {
         input('snippet').enter('new http://link.');
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('new <a href="http://link">http://link</a>.');
         expect(using('#escaped-html').binding('snippet')).toBe('new http://link.');
       });
     </doc:scenario>
   </doc:example>
 */
angular.module('ngSanitize').filter('linky', function() {
  var LINKY_URL_REGEXP = /((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s\.\;\,\(\)\{\}\<\>]/,
      MAILTO_REGEXP = /^mailto:/;

  return function(text) {
    if (!text) return text;
    var match;
    var raw = text;
    var html = [];
    // TODO(vojta): use $sanitize instead
    var writer = htmlSanitizeWriter(html);
    var url;
    var i;
    while ((match = raw.match(LINKY_URL_REGEXP))) {
      // We can not end in these as they are sometimes found at the end of the sentence
      url = match[0];
      // if we did not match ftp/http/mailto then assume mailto
      if (match[2] == match[3]) url = 'mailto:' + url;
      i = match.index;
      writer.chars(raw.substr(0, i));
      writer.start('a', {href:url});
      writer.chars(match[0].replace(MAILTO_REGEXP, ''));
      writer.end('a');
      raw = raw.substring(i + match[0].length);
    }
    writer.chars(raw);
    return html.join('');
  };
});

})(window, window.angular);

/**
 * Version: 1.0 Alpha-1
 * Build Date: 13-Nov-2007
 * Copyright (c) 2006-2007, Coolite Inc. (http://www.coolite.com/). All rights reserved.
 * License: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/.
 * Website: http://www.datejs.com/ or http://www.coolite.com/datejs/
 */
/**
 * Version: 1.0 Alpha-1
 * Build Date: 13-Nov-2007
 * Copyright (c) 2006-2007, Coolite Inc. (http://www.coolite.com/). All rights reserved.
 * License: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/.
 * Website: http://www.datejs.com/ or http://www.coolite.com/datejs/
 */
Date.CultureInfo = {
        /* Culture Name */
    name: "ru-RU",
    englishName: "Russian (Russia)",
    nativeName: "русский (Россия)",

    /* Day Name Strings */
    dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
    abbreviatedDayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    shortestDayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    firstLetterDayNames: ["В", "П", "В", "С", "Ч", "П", "С"],

    /* Month Name Strings */
    monthNames: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноябрь", "декабря"],
    abbreviatedMonthNames: ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"],

        /* AM/PM Designators */
    amDesignator: "",
    pmDesignator: "",

    firstDayOfWeek: 1,
    twoDigitYearMax: 2029,

    /**
     * The dateElementOrder is based on the order of the
     * format specifiers in the formatPatterns.DatePattern.
     *
     * Example:
     <pre>
     shortDatePattern    dateElementOrder
     ------------------  ----------------
     "M/d/yyyy"          "mdy"
     "dd/MM/yyyy"        "dmy"
     "yyyy-MM-dd"        "ymd"
     </pre>
     * The correct dateElementOrder is required by the parser to
     * determine the expected order of the date elements in the
     * string being parsed.
     *
     * NOTE: It is VERY important this value be correct for each Culture.
     */
    dateElementOrder: "dmy",

    /* Standard date and time format patterns */
    formatPatterns: {
        shortDate: "dd.MM.yyyy",
        longDate: "d MMMM yyyy г.",
        shortTime: "H:mm",
        longTime: "H:mm:ss",

                // new
        shortDateTime: "dd.MM.yyyy, H:mm",
                // /new

        fullDateTime: "d MMMM yyyy г., H:mm:ss",
        sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
        universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
        rfc1123: "ddd, dd MMM yyyy, HH:mm:ss GMT",
        monthDay: "d/M",
        yearMonth: "MMMM yyyy 'г.'"
    },

    /**
     * NOTE: If a string format is not parsing correctly, but
     * you would expect it parse, the problem likely lies below.
     *
     * The following regex patterns control most of the string matching
     * within the parser.
     *
     * The Month name and Day name patterns were automatically generated
     * and in general should be (mostly) correct.
     *
     * Beyond the month and day name patterns are natural language strings.
     * Example: "next", "today", "months"
     *
     * These natural language string may NOT be correct for this culture.
     * If they are not correct, please translate and edit this file
     * providing the correct regular expression pattern.
     *
     * If you modify this file, please post your revised CultureInfo file
     * to the Datejs Discussions located at
     *     http://groups.google.com/group/date-js
     *
     * Please mark the subject with [CultureInfo]. Example:
     *    Subject: [CultureInfo] Translated "da-DK" Danish(Denmark)
     *
     * We will add the modified patterns to the master source files.
     *
     * As well, please review the list of "Future Strings" section below.
     */
    regexPatterns: {
        jan: /^янв(арь)?/i,
        feb: /^фев(раль)?/i,
        mar: /^мар(т)?/i,
        apr: /^апр(ель)?/i,
        may: /^май/i,
        jun: /^июн(ь)?/i,
        jul: /^июл(ь)?/i,
        aug: /^авг(уст)?/i,
        sep: /^сен(тябрь)?/i,
        oct: /^окт(ябрь)?/i,
        nov: /^ноя(брь)?/i,
        dec: /^дек(абрь)?/i,

        sun: /^воскресенье/i,
        mon: /^понедельник/i,
        tue: /^вторник/i,
        wed: /^среда/i,
        thu: /^четверг/i,
        fri: /^пятница/i,
        sat: /^суббота/i,

        future: /^след|завтра/i,
        past: /^пред|вчера/i,
        add: /^(\+|через|после|вперед|и|следую?щ(ая|ий|ее)?)/i,
        subtract: /^(\-|за|до|поза|пе?ред((ыдущ|шев?ствующ)(ая|ий|ее))|назад)/i,

        yesterday: /^вчера/i,
        today: /^сегодня/i,
        tomorrow: /^завтра/i,
        now: /^сейчас|сечас|щас/i,

        millisecond: /^мс|мили(секунд)?s?/i,
        second: /^с(ек(унд)?)?/i,
        minute: /^м(ин(ут)?)?/i,
        hour: /^ч((ас)?ов)?/i,
        week: /^н(ед(ель)?)?/i,
        month: /^мес(яцев)?/i,
        day: /^д(ень|ней|ня)?/i,
        year: /^г(ода?)?|л(ет)?/i,

        shortMeridian: /^(a|p)/i,
        longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i,
        timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i,
        ordinalSuffix: /^\s*(st|nd|rd|th)/i,
        timeContext: /^\s*(\:|д|а)/i
    },

    abbreviatedTimeZoneStandard: { GMT: "-000", EST: "-0400", CST: "-0500", MST: "-0600", PST: "-0700" },
    abbreviatedTimeZoneDST: { GMT: "-000", EDT: "-0500", CDT: "-0600", MDT: "-0700", PDT: "-0800" }

};

Date.getMonthNumberFromName=function(name){var n=Date.CultureInfo.monthNames,m=Date.CultureInfo.abbreviatedMonthNames,s=name.toLowerCase();for(var i=0;i<n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}
return-1;};Date.getDayNumberFromName=function(name){var n=Date.CultureInfo.dayNames,m=Date.CultureInfo.abbreviatedDayNames,o=Date.CultureInfo.shortestDayNames,s=name.toLowerCase();for(var i=0;i<n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}
return-1;};Date.isLeapYear=function(year){return(((year%4===0)&&(year%100!==0))||(year%400===0));};Date.getDaysInMonth=function(year,month){return[31,(Date.isLeapYear(year)?29:28),31,30,31,30,31,31,30,31,30,31][month];};Date.getTimezoneOffset=function(s,dst){return(dst||false)?Date.CultureInfo.abbreviatedTimeZoneDST[s.toUpperCase()]:Date.CultureInfo.abbreviatedTimeZoneStandard[s.toUpperCase()];};Date.getTimezoneAbbreviation=function(offset,dst){var n=(dst||false)?Date.CultureInfo.abbreviatedTimeZoneDST:Date.CultureInfo.abbreviatedTimeZoneStandard,p;for(p in n){if(n[p]===offset){return p;}}
return null;};Date.prototype.clone=function(){return new Date(this.getTime());};Date.prototype.compareTo=function(date){if(isNaN(this)){throw new Error(this);}
if(date instanceof Date&&!isNaN(date)){return(this>date)?1:(this<date)?-1:0;}else{throw new TypeError(date);}};Date.prototype.equals=function(date){return(this.compareTo(date)===0);};Date.prototype.between=function(start,end){var t=this.getTime();return t>=start.getTime()&&t<=end.getTime();};Date.prototype.addMilliseconds=function(value){this.setMilliseconds(this.getMilliseconds()+value);return this;};Date.prototype.addSeconds=function(value){return this.addMilliseconds(value*1000);};Date.prototype.addMinutes=function(value){return this.addMilliseconds(value*60000);};Date.prototype.addHours=function(value){return this.addMilliseconds(value*3600000);};Date.prototype.addDays=function(value){return this.addMilliseconds(value*86400000);};Date.prototype.addWeeks=function(value){return this.addMilliseconds(value*604800000);};Date.prototype.addMonths=function(value){var n=this.getDate();this.setDate(1);this.setMonth(this.getMonth()+value);this.setDate(Math.min(n,this.getDaysInMonth()));return this;};Date.prototype.addYears=function(value){return this.addMonths(value*12);};Date.prototype.add=function(config){if(typeof config=="number"){this._orient=config;return this;}
var x=config;if(x.millisecond||x.milliseconds){this.addMilliseconds(x.millisecond||x.milliseconds);}
if(x.second||x.seconds){this.addSeconds(x.second||x.seconds);}
if(x.minute||x.minutes){this.addMinutes(x.minute||x.minutes);}
if(x.hour||x.hours){this.addHours(x.hour||x.hours);}
if(x.month||x.months){this.addMonths(x.month||x.months);}
if(x.year||x.years){this.addYears(x.year||x.years);}
if(x.day||x.days){this.addDays(x.day||x.days);}
return this;};Date._validate=function(value,min,max,name){if(typeof value!="number"){throw new TypeError(value+" is not a Number.");}else if(value<min||value>max){throw new RangeError(value+" is not a valid value for "+name+".");}
return true;};Date.validateMillisecond=function(n){return Date._validate(n,0,999,"milliseconds");};Date.validateSecond=function(n){return Date._validate(n,0,59,"seconds");};Date.validateMinute=function(n){return Date._validate(n,0,59,"minutes");};Date.validateHour=function(n){return Date._validate(n,0,23,"hours");};Date.validateDay=function(n,year,month){return Date._validate(n,1,Date.getDaysInMonth(year,month),"days");};Date.validateMonth=function(n){return Date._validate(n,0,11,"months");};Date.validateYear=function(n){return Date._validate(n,1,9999,"seconds");};Date.prototype.set=function(config){var x=config;if(!x.millisecond&&x.millisecond!==0){x.millisecond=-1;}
if(!x.second&&x.second!==0){x.second=-1;}
if(!x.minute&&x.minute!==0){x.minute=-1;}
if(!x.hour&&x.hour!==0){x.hour=-1;}
if(!x.day&&x.day!==0){x.day=-1;}
if(!x.month&&x.month!==0){x.month=-1;}
if(!x.year&&x.year!==0){x.year=-1;}
if(x.millisecond!=-1&&Date.validateMillisecond(x.millisecond)){this.addMilliseconds(x.millisecond-this.getMilliseconds());}
if(x.second!=-1&&Date.validateSecond(x.second)){this.addSeconds(x.second-this.getSeconds());}
if(x.minute!=-1&&Date.validateMinute(x.minute)){this.addMinutes(x.minute-this.getMinutes());}
if(x.hour!=-1&&Date.validateHour(x.hour)){this.addHours(x.hour-this.getHours());}
if(x.month!==-1&&Date.validateMonth(x.month)){this.addMonths(x.month-this.getMonth());}
if(x.year!=-1&&Date.validateYear(x.year)){this.addYears(x.year-this.getFullYear());}
if(x.day!=-1&&Date.validateDay(x.day,this.getFullYear(),this.getMonth())){this.addDays(x.day-this.getDate());}
if(x.timezone){this.setTimezone(x.timezone);}
if(x.timezoneOffset){this.setTimezoneOffset(x.timezoneOffset);}
return this;};Date.prototype.clearTime=function(){this.setHours(0);this.setMinutes(0);this.setSeconds(0);this.setMilliseconds(0);return this;};Date.prototype.isLeapYear=function(){var y=this.getFullYear();return(((y%4===0)&&(y%100!==0))||(y%400===0));};Date.prototype.isWeekday=function(){return!(this.is().sat()||this.is().sun());};Date.prototype.getDaysInMonth=function(){return Date.getDaysInMonth(this.getFullYear(),this.getMonth());};Date.prototype.moveToFirstDayOfMonth=function(){return this.set({day:1});};Date.prototype.moveToLastDayOfMonth=function(){return this.set({day:this.getDaysInMonth()});};Date.prototype.moveToDayOfWeek=function(day,orient){var diff=(day-this.getDay()+7*(orient||+1))%7;return this.addDays((diff===0)?diff+=7*(orient||+1):diff);};Date.prototype.moveToMonth=function(month,orient){var diff=(month-this.getMonth()+12*(orient||+1))%12;return this.addMonths((diff===0)?diff+=12*(orient||+1):diff);};Date.prototype.getDayOfYear=function(){return Math.floor((this-new Date(this.getFullYear(),0,1))/86400000);};Date.prototype.getWeekOfYear=function(firstDayOfWeek){var y=this.getFullYear(),m=this.getMonth(),d=this.getDate();var dow=firstDayOfWeek||Date.CultureInfo.firstDayOfWeek;var offset=7+1-new Date(y,0,1).getDay();if(offset==8){offset=1;}
var daynum=((Date.UTC(y,m,d,0,0,0)-Date.UTC(y,0,1,0,0,0))/86400000)+1;var w=Math.floor((daynum-offset+7)/7);if(w===dow){y--;var prevOffset=7+1-new Date(y,0,1).getDay();if(prevOffset==2||prevOffset==8){w=53;}else{w=52;}}
return w;};Date.prototype.isDST=function(){console.log('isDST');return this.toString().match(/(E|C|M|P)(S|D)T/)[2]=="D";};Date.prototype.getTimezone=function(){return Date.getTimezoneAbbreviation(this.getUTCOffset,this.isDST());};Date.prototype.setTimezoneOffset=function(s){var here=this.getTimezoneOffset(),there=Number(s)*-6/10;this.addMinutes(there-here);return this;};Date.prototype.setTimezone=function(s){return this.setTimezoneOffset(Date.getTimezoneOffset(s));};Date.prototype.getUTCOffset=function(){var n=this.getTimezoneOffset()*-10/6,r;if(n<0){r=(n-10000).toString();return r[0]+r.substr(2);}else{r=(n+10000).toString();return"+"+r.substr(1);}};Date.prototype.getDayName=function(abbrev){return abbrev?Date.CultureInfo.abbreviatedDayNames[this.getDay()]:Date.CultureInfo.dayNames[this.getDay()];};Date.prototype.getMonthName=function(abbrev){return abbrev?Date.CultureInfo.abbreviatedMonthNames[this.getMonth()]:Date.CultureInfo.monthNames[this.getMonth()];};Date.prototype._toString=Date.prototype.toString;Date.prototype.toString=function(format){var self=this;var p=function p(s){return(s.toString().length==1)?"0"+s:s;};return format?format.replace(/dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?/g,function(format){switch(format){case"hh":return p(self.getHours()<13?self.getHours():(self.getHours()-12));case"h":return self.getHours()<13?self.getHours():(self.getHours()-12);case"HH":return p(self.getHours());case"H":return self.getHours();case"mm":return p(self.getMinutes());case"m":return self.getMinutes();case"ss":return p(self.getSeconds());case"s":return self.getSeconds();case"yyyy":return self.getFullYear();case"yy":return self.getFullYear().toString().substring(2,4);case"dddd":return self.getDayName();case"ddd":return self.getDayName(true);case"dd":return p(self.getDate());case"d":return self.getDate().toString();case"MMMM":return self.getMonthName();case"MMM":return self.getMonthName(true);case"MM":return p((self.getMonth()+1));case"M":return self.getMonth()+1;case"t":return self.getHours()<12?Date.CultureInfo.amDesignator.substring(0,1):Date.CultureInfo.pmDesignator.substring(0,1);case"tt":return self.getHours()<12?Date.CultureInfo.amDesignator:Date.CultureInfo.pmDesignator;case"zzz":case"zz":case"z":return"";}}):this._toString();};
Date.now=function(){return new Date();};Date.today=function(){return Date.now().clearTime();};Date.prototype._orient=+1;Date.prototype.next=function(){this._orient=+1;return this;};Date.prototype.last=Date.prototype.prev=Date.prototype.previous=function(){this._orient=-1;return this;};Date.prototype._is=false;Date.prototype.is=function(){this._is=true;return this;};Number.prototype._dateElement="day";Number.prototype.fromNow=function(){var c={};c[this._dateElement]=this;return Date.now().add(c);};Number.prototype.ago=function(){var c={};c[this._dateElement]=this*-1;return Date.now().add(c);};(function(){var $D=Date.prototype,$N=Number.prototype;var dx=("sunday monday tuesday wednesday thursday friday saturday").split(/\s/),mx=("january february march april may june july august september october november december").split(/\s/),px=("Millisecond Second Minute Hour Day Week Month Year").split(/\s/),de;var df=function(n){return function(){if(this._is){this._is=false;return this.getDay()==n;}
return this.moveToDayOfWeek(n,this._orient);};};for(var i=0;i<dx.length;i++){$D[dx[i]]=$D[dx[i].substring(0,3)]=df(i);}
var mf=function(n){return function(){if(this._is){this._is=false;return this.getMonth()===n;}
return this.moveToMonth(n,this._orient);};};for(var j=0;j<mx.length;j++){$D[mx[j]]=$D[mx[j].substring(0,3)]=mf(j);}
var ef=function(j){return function(){if(j.substring(j.length-1)!="s"){j+="s";}
return this["add"+j](this._orient);};};var nf=function(n){return function(){this._dateElement=n;return this;};};for(var k=0;k<px.length;k++){de=px[k].toLowerCase();$D[de]=$D[de+"s"]=ef(px[k]);$N[de]=$N[de+"s"]=nf(de);}}());Date.prototype.toJSONString=function(){return this.toString("yyyy-MM-ddThh:mm:ssZ");};Date.prototype.toShortDateString=function(){return this.toString(Date.CultureInfo.formatPatterns.shortDatePattern);};Date.prototype.toLongDateString=function(){return this.toString(Date.CultureInfo.formatPatterns.longDatePattern);};Date.prototype.toShortTimeString=function(){return this.toString(Date.CultureInfo.formatPatterns.shortTimePattern);};Date.prototype.toLongTimeString=function(){return this.toString(Date.CultureInfo.formatPatterns.longTimePattern);};Date.prototype.getOrdinal=function(){switch(this.getDate()){case 1:case 21:case 31:return"st";case 2:case 22:return"nd";case 3:case 23:return"rd";default:return"th";}};
(function(){Date.Parsing={Exception:function(s){this.message="Parse error at '"+s.substring(0,10)+" ...'";}};var $P=Date.Parsing;var _=$P.Operators={rtoken:function(r){return function(s){var mx=s.match(r);if(mx){return([mx[0],s.substring(mx[0].length)]);}else{throw new $P.Exception(s);}};},token:function(s){return function(s){return _.rtoken(new RegExp("^\s*"+s+"\s*"))(s);};},stoken:function(s){return _.rtoken(new RegExp("^"+s));},until:function(p){return function(s){var qx=[],rx=null;while(s.length){try{rx=p.call(this,s);}catch(e){qx.push(rx[0]);s=rx[1];continue;}
break;}
return[qx,s];};},many:function(p){return function(s){var rx=[],r=null;while(s.length){try{r=p.call(this,s);}catch(e){return[rx,s];}
rx.push(r[0]);s=r[1];}
return[rx,s];};},optional:function(p){return function(s){var r=null;try{r=p.call(this,s);}catch(e){return[null,s];}
return[r[0],r[1]];};},not:function(p){return function(s){try{p.call(this,s);}catch(e){return[null,s];}
throw new $P.Exception(s);};},ignore:function(p){return p?function(s){var r=null;r=p.call(this,s);return[null,r[1]];}:null;},product:function(){var px=arguments[0],qx=Array.prototype.slice.call(arguments,1),rx=[];for(var i=0;i<px.length;i++){rx.push(_.each(px[i],qx));}
return rx;},cache:function(rule){var cache={},r=null;return function(s){try{r=cache[s]=(cache[s]||rule.call(this,s));}catch(e){r=cache[s]=e;}
if(r instanceof $P.Exception){throw r;}else{return r;}};},any:function(){var px=arguments;return function(s){var r=null;for(var i=0;i<px.length;i++){if(px[i]==null){continue;}
try{r=(px[i].call(this,s));}catch(e){r=null;}
if(r){return r;}}
throw new $P.Exception(s);};},each:function(){var px=arguments;return function(s){var rx=[],r=null;for(var i=0;i<px.length;i++){if(px[i]==null){continue;}
try{r=(px[i].call(this,s));}catch(e){throw new $P.Exception(s);}
rx.push(r[0]);s=r[1];}
return[rx,s];};},all:function(){var px=arguments,_=_;return _.each(_.optional(px));},sequence:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;if(px.length==1){return px[0];}
return function(s){var r=null,q=null;var rx=[];for(var i=0;i<px.length;i++){try{r=px[i].call(this,s);}catch(e){break;}
rx.push(r[0]);try{q=d.call(this,r[1]);}catch(ex){q=null;break;}
s=q[1];}
if(!r){throw new $P.Exception(s);}
if(q){throw new $P.Exception(q[1]);}
if(c){try{r=c.call(this,r[1]);}catch(ey){throw new $P.Exception(r[1]);}}
return[rx,(r?r[1]:s)];};},between:function(d1,p,d2){d2=d2||d1;var _fn=_.each(_.ignore(d1),p,_.ignore(d2));return function(s){var rx=_fn.call(this,s);return[[rx[0][0],r[0][2]],rx[1]];};},list:function(p,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return(p instanceof Array?_.each(_.product(p.slice(0,-1),_.ignore(d)),p.slice(-1),_.ignore(c)):_.each(_.many(_.each(p,_.ignore(d))),px,_.ignore(c)));},set:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return function(s){var r=null,p=null,q=null,rx=null,best=[[],s],last=false;for(var i=0;i<px.length;i++){q=null;p=null;r=null;last=(px.length==1);try{r=px[i].call(this,s);}catch(e){continue;}
rx=[[r[0]],r[1]];if(r[1].length>0&&!last){try{q=d.call(this,r[1]);}catch(ex){last=true;}}else{last=true;}
if(!last&&q[1].length===0){last=true;}
if(!last){var qx=[];for(var j=0;j<px.length;j++){if(i!=j){qx.push(px[j]);}}
p=_.set(qx,d).call(this,q[1]);if(p[0].length>0){rx[0]=rx[0].concat(p[0]);rx[1]=p[1];}}
if(rx[1].length<best[1].length){best=rx;}
if(best[1].length===0){break;}}
if(best[0].length===0){return best;}
if(c){try{q=c.call(this,best[1]);}catch(ey){throw new $P.Exception(best[1]);}
best[1]=q[1];}
return best;};},forward:function(gr,fname){return function(s){return gr[fname].call(this,s);};},replace:function(rule,repl){return function(s){var r=rule.call(this,s);return[repl,r[1]];};},process:function(rule,fn){return function(s){var r=rule.call(this,s);return[fn.call(this,r[0]),r[1]];};},min:function(min,rule){return function(s){var rx=rule.call(this,s);if(rx[0].length<min){throw new $P.Exception(s);}
return rx;};}};var _generator=function(op){return function(){var args=null,rx=[];if(arguments.length>1){args=Array.prototype.slice.call(arguments);}else if(arguments[0]instanceof Array){args=arguments[0];}
if(args){for(var i=0,px=args.shift();i<px.length;i++){args.unshift(px[i]);rx.push(op.apply(null,args));args.shift();return rx;}}else{return op.apply(null,arguments);}};};var gx="optional not ignore cache".split(/\s/);for(var i=0;i<gx.length;i++){_[gx[i]]=_generator(_[gx[i]]);}
var _vector=function(op){return function(){if(arguments[0]instanceof Array){return op.apply(null,arguments[0]);}else{return op.apply(null,arguments);}};};var vx="each any all".split(/\s/);for(var j=0;j<vx.length;j++){_[vx[j]]=_vector(_[vx[j]]);}}());(function(){var flattenAndCompact=function(ax){var rx=[];for(var i=0;i<ax.length;i++){if(ax[i]instanceof Array){rx=rx.concat(flattenAndCompact(ax[i]));}else{if(ax[i]){rx.push(ax[i]);}}}
return rx;};Date.Grammar={};Date.Translator={hour:function(s){return function(){this.hour=Number(s);};},minute:function(s){return function(){this.minute=Number(s);};},second:function(s){return function(){this.second=Number(s);};},meridian:function(s){return function(){this.meridian=s.slice(0,1).toLowerCase();};},timezone:function(s){return function(){var n=s.replace(/[^\d\+\-]/g,"");if(n.length){this.timezoneOffset=Number(n);}else{this.timezone=s.toLowerCase();}};},day:function(x){var s=x[0];return function(){this.day=Number(s.match(/\d+/)[0]);};},month:function(s){return function(){this.month=((s.length==3)?Date.getMonthNumberFromName(s):(Number(s)-1));};},year:function(s){return function(){var n=Number(s);this.year=((s.length>2)?n:(n+(((n+2000)<Date.CultureInfo.twoDigitYearMax)?2000:1900)));};},rday:function(s){return function(){switch(s){case"yesterday":this.days=-1;break;case"tomorrow":this.days=1;break;case"today":this.days=0;break;case"now":this.days=0;this.now=true;break;}};},finishExact:function(x){x=(x instanceof Array)?x:[x];var now=new Date();this.year=now.getFullYear();this.month=now.getMonth();this.day=1;this.hour=0;this.minute=0;this.second=0;for(var i=0;i<x.length;i++){if(x[i]){x[i].call(this);}}
this.hour=(this.meridian=="p"&&this.hour<13)?this.hour+12:this.hour;if(this.day>Date.getDaysInMonth(this.year,this.month)){throw new RangeError(this.day+" is not a valid value for days.");}
var r=new Date(this.year,this.month,this.day,this.hour,this.minute,this.second);if(this.timezone){r.set({timezone:this.timezone});}else if(this.timezoneOffset){r.set({timezoneOffset:this.timezoneOffset});}
return r;},finish:function(x){x=(x instanceof Array)?flattenAndCompact(x):[x];if(x.length===0){return null;}
for(var i=0;i<x.length;i++){if(typeof x[i]=="function"){x[i].call(this);}}
if(this.now){return new Date();}
var today=Date.today();var method=null;var expression=!!(this.days!=null||this.orient||this.operator);if(expression){var gap,mod,orient;orient=((this.orient=="past"||this.operator=="subtract")?-1:1);if(this.weekday){this.unit="day";gap=(Date.getDayNumberFromName(this.weekday)-today.getDay());mod=7;this.days=gap?((gap+(orient*mod))%mod):(orient*mod);}
if(this.month){this.unit="month";gap=(this.month-today.getMonth());mod=12;this.months=gap?((gap+(orient*mod))%mod):(orient*mod);this.month=null;}
if(!this.unit){this.unit="day";}
if(this[this.unit+"s"]==null||this.operator!=null){if(!this.value){this.value=1;}
if(this.unit=="week"){this.unit="day";this.value=this.value*7;}
this[this.unit+"s"]=this.value*orient;}
return today.add(this);}else{if(this.meridian&&this.hour){this.hour=(this.hour<13&&this.meridian=="p")?this.hour+12:this.hour;}
if(this.weekday&&!this.day){this.day=(today.addDays((Date.getDayNumberFromName(this.weekday)-today.getDay()))).getDate();}
if(this.month&&!this.day){this.day=1;}
return today.set(this);}}};var _=Date.Parsing.Operators,g=Date.Grammar,t=Date.Translator,_fn;g.datePartDelimiter=_.rtoken(/^([\s\-\.\,\/\x27]+)/);g.timePartDelimiter=_.stoken(":");g.whiteSpace=_.rtoken(/^\s*/);g.generalDelimiter=_.rtoken(/^(([\s\,]|at|on)+)/);var _C={};g.ctoken=function(keys){var fn=_C[keys];if(!fn){var c=Date.CultureInfo.regexPatterns;var kx=keys.split(/\s+/),px=[];for(var i=0;i<kx.length;i++){px.push(_.replace(_.rtoken(c[kx[i]]),kx[i]));}
fn=_C[keys]=_.any.apply(null,px);}
return fn;};g.ctoken2=function(key){return _.rtoken(Date.CultureInfo.regexPatterns[key]);};g.h=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/),t.hour));g.hh=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/),t.hour));g.H=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/),t.hour));g.HH=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/),t.hour));g.m=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.minute));g.mm=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.minute));g.s=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.second));g.ss=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.second));g.hms=_.cache(_.sequence([g.H,g.mm,g.ss],g.timePartDelimiter));g.t=_.cache(_.process(g.ctoken2("shortMeridian"),t.meridian));g.tt=_.cache(_.process(g.ctoken2("longMeridian"),t.meridian));g.z=_.cache(_.process(_.rtoken(/^(\+|\-)?\s*\d\d\d\d?/),t.timezone));g.zz=_.cache(_.process(_.rtoken(/^(\+|\-)\s*\d\d\d\d/),t.timezone));g.zzz=_.cache(_.process(g.ctoken2("timezone"),t.timezone));g.timeSuffix=_.each(_.ignore(g.whiteSpace),_.set([g.tt,g.zzz]));g.time=_.each(_.optional(_.ignore(_.stoken("T"))),g.hms,g.timeSuffix);g.d=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/),_.optional(g.ctoken2("ordinalSuffix"))),t.day));g.dd=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/),_.optional(g.ctoken2("ordinalSuffix"))),t.day));g.ddd=g.dddd=_.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"),function(s){return function(){this.weekday=s;};}));g.M=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/),t.month));g.MM=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/),t.month));g.MMM=g.MMMM=_.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"),t.month));g.y=_.cache(_.process(_.rtoken(/^(\d\d?)/),t.year));g.yy=_.cache(_.process(_.rtoken(/^(\d\d)/),t.year));g.yyy=_.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/),t.year));g.yyyy=_.cache(_.process(_.rtoken(/^(\d\d\d\d)/),t.year));_fn=function(){return _.each(_.any.apply(null,arguments),_.not(g.ctoken2("timeContext")));};g.day=_fn(g.d,g.dd);g.month=_fn(g.M,g.MMM);g.year=_fn(g.yyyy,g.yy);g.orientation=_.process(g.ctoken("past future"),function(s){return function(){this.orient=s;};});g.operator=_.process(g.ctoken("add subtract"),function(s){return function(){this.operator=s;};});g.rday=_.process(g.ctoken("yesterday tomorrow today now"),t.rday);g.unit=_.process(g.ctoken("minute hour day week month year"),function(s){return function(){this.unit=s;};});g.value=_.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/),function(s){return function(){this.value=s.replace(/\D/g,"");};});g.expression=_.set([g.rday,g.operator,g.value,g.unit,g.orientation,g.ddd,g.MMM]);_fn=function(){return _.set(arguments,g.datePartDelimiter);};g.mdy=_fn(g.ddd,g.month,g.day,g.year);g.ymd=_fn(g.ddd,g.year,g.month,g.day);g.dmy=_fn(g.ddd,g.day,g.month,g.year);g.date=function(s){return((g[Date.CultureInfo.dateElementOrder]||g.mdy).call(this,s));};g.format=_.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/),function(fmt){if(g[fmt]){return g[fmt];}else{throw Date.Parsing.Exception(fmt);}}),_.process(_.rtoken(/^[^dMyhHmstz]+/),function(s){return _.ignore(_.stoken(s));}))),function(rules){return _.process(_.each.apply(null,rules),t.finishExact);});var _F={};var _get=function(f){return _F[f]=(_F[f]||g.format(f)[0]);};g.formats=function(fx){if(fx instanceof Array){var rx=[];for(var i=0;i<fx.length;i++){rx.push(_get(fx[i]));}
return _.any.apply(null,rx);}else{return _get(fx);}};g._formats=g.formats(["yyyy-MM-ddTHH:mm:ss","ddd, MMM dd, yyyy H:mm:ss tt","ddd MMM d yyyy HH:mm:ss zzz","d"]);g._start=_.process(_.set([g.date,g.time,g.expression],g.generalDelimiter,g.whiteSpace),t.finish);g.start=function(s){try{var r=g._formats.call({},s);if(r[1].length===0){return r;}}catch(e){}
return g._start.call({},s);};}());Date._parse=Date.parse;Date.parse=function(s){var r=null;if(!s){return null;}
try{r=Date.Grammar.start.call({},s);}catch(e){return null;}
return((r[1].length===0)?r[0]:null);};Date.getParseFunction=function(fx){var fn=Date.Grammar.formats(fx);return function(s){var r=null;try{r=fn.call({},s);}catch(e){return null;}
return((r[1].length===0)?r[0]:null);};};Date.parseExact=function(s,fx){return Date.getParseFunction(fx)(s);};

/**
 * Copyright (c) 2007-2013 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.6
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,targ,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);

/*global angular */
/*
 jQuery UI Datepicker plugin wrapper

 @note If ≤ IE8 make sure you have a polyfill for Date.toISOString()
 @param [ui-date] {object} Options to pass to $.fn.datepicker() merged onto uiDateConfig
 */

angular.module('ui.date', [])

.constant('uiDateConfig', {})

.directive('uiDate', ['uiDateConfig', '$timeout', function (uiDateConfig, $timeout) {
  'use strict';
  var options;
  options = {};
  angular.extend(options, uiDateConfig);
  return {
    require:'?ngModel',
    link:function (scope, element, attrs, controller) {
      var getOptions = function () {
        return angular.extend({}, uiDateConfig, scope.$eval(attrs.uiDate));
      };
      var initDateWidget = function () {
        var showing = false;
        var opts = getOptions();

        // If we have a controller (i.e. ngModelController) then wire it up
        if (controller) {

          // Set the view value in a $apply block when users selects
          // (calling directive user's function too if provided)
          var _onSelect = opts.onSelect || angular.noop;
          opts.onSelect = function (value, picker) {
            scope.$apply(function() {
              showing = true;
              controller.$setViewValue(element.datepicker("getDate"));
              _onSelect(value, picker);
              element.blur();
            });
          };
          opts.beforeShow = function() {
            showing = true;
          };
          opts.onClose = function(value, picker) {
            showing = false;
          };
          element.on('blur', function() {
            if ( !showing ) {
              scope.$apply(function() {
                element.datepicker("setDate", element.datepicker("getDate"));
                controller.$setViewValue(element.datepicker("getDate"));
              });
            }
          });

          // Update the date picker when the model changes
          controller.$render = function () {
            var date = controller.$viewValue;
            if ( angular.isDefined(date) && date !== null && !angular.isDate(date) ) {
              throw new Error('ng-Model value must be a Date object - currently it is a ' + typeof date + ' - use ui-date-format to convert it from a string');
            }
            element.datepicker("setDate", date);
          };
        }
        // If we don't destroy the old one it doesn't update properly when the config changes
        element.datepicker('destroy');
        // Create the new datepicker widget
        element.datepicker(opts);
        if ( controller ) {
          // Force a render to override whatever is in the input text box
          controller.$render();
        }
      };
      // Watch for changes to the directives options
      scope.$watch(getOptions, initDateWidget, true);
    }
  };
}
])

.constant('uiDateFormatConfig', '')

.directive('uiDateFormat', ['uiDateFormatConfig', function(uiDateFormatConfig) {
  var directive = {
    require:'ngModel',
    link: function(scope, element, attrs, modelCtrl) {
      var dateFormat = attrs.uiDateFormat || uiDateFormatConfig;
      if ( dateFormat ) {
        // Use the datepicker with the attribute value as the dateFormat string to convert to and from a string
        modelCtrl.$formatters.push(function(value) {
          if (angular.isString(value) ) {
            return jQuery.datepicker.parseDate(dateFormat, value);
          }
          return null;
        });
        modelCtrl.$parsers.push(function(value){
          if (value) {
            return jQuery.datepicker.formatDate(dateFormat, value);
          }
          return null;
        });
      } else {
        // Default to ISO formatting
        modelCtrl.$formatters.push(function(value) {
          if (angular.isString(value) ) {
            return new Date(value);
          }
          return null;
        });
        modelCtrl.$parsers.push(function(value){
          if (value) {
            return value.toISOString();
          }
          return null;
        });
      }
    }
  };
  return directive;
}]);

angular.module('calendar', [])
    /**
     * Одиночный календарь
     *
     * links - список активных дат
     * url - базовый URl
     * now - подсветка текущей даты
     * select-month - выбранный по умолчанию месяц
     * comments - дни с комментариями
     * disabled-month
     * disabled-year
     * hidden-year
     * hidden-month
     */
    .directive('calendarSingle', ['$parse', function($parse) {
        var monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        var daysList = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

        return {
            restrict: 'C',
            template: '<div class="selectors" ng-class="selectorsClass()">'
                        + '<div calendar-select="year" from="years" disabled="disabledYear" hidden="hiddenYear"></div>'
                        + '<div calendar-select="month" from="months" disabled="disabledMonth" hidden="hiddenMonth"></div>'
                    + '</div>'
                    + '<div class="calendar">'
                        + '<table class="no-design">'
                            + '<tr ng-repeat="week in weeks">'
                                + '<td ng-repeat="day in week.days" ng-class="{inactive:day.inactive, lastinactive:day.lastinactive, now:day.isnow, link:day.isLink, comment:day.isComment}">'
                                    + '<a ng-href="{{ day.url }}" ui-if="day.isLink">{{ day.day }}</a>'
                                    + '<span class="calendar-cell" ui-if="day.isComment">{{ day.day }}'
                                        + '<span class="comment">{{ day.fullDate }} <b>{{ day.comment }}</b></span>'
                                    + '</span>'
                                    + '<span ui-if="!day.isLink && !day.isComment">{{ day.day }}</span>'
                                + '</td>'
                            + '</tr>'
                        + '</table>'
                    + '</div>',
            scope: true,

            link: function($scope, $element, attrs) {
                $scope.weeks = [];

                if (!!attrs.selectYear) {
                    $scope.year = parseInt(attrs.selectYear, 10);
                } else {
                    $scope.year = Date.today().getFullYear();
                }

                if (!!attrs.selectMonth) {
                    $scope.month = parseInt(attrs.selectMonth, 10);
                } else {
                    $scope.month = Date.today().getMonth();
                }

                $scope.links = $parse(attrs.links)($scope);
                if (!$scope.links) {
                    $scope.links = [];
                }

                $scope.comments = $parse(attrs.comments)($scope);
                if (!$scope.comments) {
                    $scope.comments = [];
                }

                $scope.url = attrs.url;
                $scope.now = attrs.now;

                if (!attrs.selectYear && !!$scope.now) {
                    $scope.year = Date.parse($scope.now).getFullYear();
                }

                if (!attrs.selectMonth && !!$scope.now) {
                    $scope.month = Date.parse($scope.now).getMonth();
                }

                $scope.years = [];
                for(var i = 0; i < 3; i++) {
                    $scope.years.push({value:$scope.year - i, title:$scope.year - i});
                }

                $scope.months = [];
                for(var i = 0; i < 12; i++) {
                    $scope.months.push({value:i, title:monthNames[i]});
                }

                $scope.disabledYear = (attrs.disabledYear !== undefined);
                $scope.disabledMonth = (attrs.disabledMonth !== undefined);

                $scope.hiddenYear = (attrs.hiddenYear !== undefined);
                $scope.hiddenMonth = (attrs.hiddenMonth !== undefined);

                $scope.$watch('year', $scope.update);
                $scope.$watch('month', $scope.update);
            },

            controller: function($scope) {
                $scope.selectorsClass = function() {
                    if ($scope.hiddenYear || $scope.hiddenMonth) {
                        return 'text-align-left';
                    }
                };

                /**
                 * Обновление календаря
                 */
                $scope.update = function() {
                    var days = [];

                    var daysCount = Date.getDaysInMonth($scope.year, $scope.month);

                    for(var i = 1; i <= daysCount; i++) {
                        var date = Date.today().set({year:$scope.year, month:$scope.month, day:i});

                        days.push({
                            fullDate: date.toString('dd.MM.yyyy'),
                            day: i,
                            isLink: $.inArray(date.toString('dd/MM/yyyy'), $scope.links) !== -1,
                            isComment: !!$scope.comments[date.toString('dd/MM/yyyy')],
                            comment: !!$scope.comments[date.toString('dd/MM/yyyy')] ? $scope.comments[date.toString('dd/MM/yyyy')] : null,
                            isnow: date.toString('dd/MM/yyyy') == $scope.now,
                            url: $scope.url + '?date='+ date.toString('dd-MM-yyyy'),
                            weekDayNumber: $.inArray(date.toString('ddd'), daysList)
                        });
                    }

                    var prevYear = $scope.year;
                    var prevMonth = $scope.month - 1;

                    if (prevMonth < 0) {
                        prevMonth = 11;
                        prevYear--;
                    }

                    var ii = 0;
                    for(var i = days[0].weekDayNumber; i > 0 ; i--, ii++) {
                        var day = Date.getDaysInMonth(prevYear, prevMonth) - ii;
                        days.unshift({
                            fullDate: date.toString('dd.MM.yyyy'),
                            inactive: true,
                            day: day,
                            weekDayNumber: $.inArray(Date.today().set({year:prevYear, month:prevMonth, day:day}).toString('ddd'), daysList)
                        });
                    }

                    if (ii > 0) {
                        days[ii-1].lastinactive = true;
                    }

                    var nextYear = $scope.year;
                    var nextMonth = $scope.month + 1;

                    if (nextMonth > 11) {
                        nextMonth = 0;
                        nextYear++;
                    }

                    var ii = 1;
                    for(var i = days[days.length - 1].weekDayNumber; i < 6 ; i++, ii++) {
                        var day = ii;

                        days.push({
                            fullDate: date.toString('dd.MM.yyyy'),
                            inactive: true,
                            day: day,
                            weekDayNumber: $.inArray(Date.today().set({year:nextYear, month:nextMonth, day:day}).toString('ddd'), daysList)
                        });
                    }

                    $scope.weeks = [];

                    for(var i = 0; i < days.length; i+=7) {
                        var week = {days:[]};

                        for(var j = 0; j < 7; j++) {
                            week.days.push(days[i+j]);
                        }

                        $scope.weeks.push(week);
                    }
                };
            }
        }
    }])


    /**
     * Селектор для календаря
     */
    .directive('calendarSelect', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            template: '<div class="select" ng-class="className()">'
                        + '<div class="value">{{ selected }}</div>'
                        + '<div class="list">'
                            + '<div class="item" ng-repeat="item in list" ng-click="clkValue(item)">'
                                + '{{ item.title }}'
                            + '</div>'
                        + '</div>'
                    + '</div>',
            replace: true,
            scope:{
                value: '=calendarSelect',
                list: '=from',
                disabled: '=',
                hidden: '='
            },

            link: function($scope, $element, attrs) {
                $scope.name = attrs.calendarSelect;
                $scope.selected = '---';
                $scope.$list = $element.find('.list');

                $scope.$watch('list', function(list) {
                    for(var i = 0; i < list.length; i++) {
                        if (list[i].value == $scope.value) {
                            $scope.selected = list[i].title;
                        }
                    }
                });
            },

            controller: function($scope) {
                $scope.clkValue = function(item) {
                    $scope.selected = item.title;
                    $scope.value = item.value;
                    $scope.$list.hide();
                    $timeout(function() {
                        $scope.$list.removeAttr('style');
                    }, 100);
                };

                $scope.className = function() {
                    var className = $scope.name;

                    if ($scope.disabled) {
                        className += ' disabled';
                    }

                    if ($scope.hidden) {
                        className += ' hidden';
                    }

                    return className;
                };
            }
        }
    }])
;

/**
 * Виджет Краснодар и цифры
 */
angular.module('citynum', [])
    .directive('cityAndNumbers', ['$http', function($http) {
        return {
            restrict: 'C',
            scope: {
                url: '@',
                count: '@'
            },
            link: function($scope, $element, attrs) {
                $scope.loading = false;
                $scope.ids = [attrs.id];

                $element.find('.btn-update').on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $element.stop().animate({opacity:0.8}, 200);

                        $http({
                            method:'GET',
                            url: $scope.url,
                            params: {id: $scope.ids.join(',')}
                        }).success(function(response) {
                            if (response.item) {
                                $scope.ids.push(response.item.id);

                                $element.find('.title').text(response.item.title);
                                $element.find('.count').text(response.item.subtitle);
                                $element.find('.text-block').text(response.item.content);
                            }

                            $element.stop().animate({opacity:1}, 200);
                        }).error(function(response) {
                            $element.stop().animate({opacity:1}, 200);
                            alert('Произошла ошибка');
                            console.error(response);
                        });
                    });
                });

                $scope.$watch('ids', function(ids) {
                    if (ids.length == $scope.count) {
                        $scope.ids = [$scope.ids.pop()];
                    }
                }, true);
            }
        }
    }])
;

/**
 * Оформление контента
 */
angular.module('content', ['dialog'])
    .run([function() {
        // Добавляем к контентным нумерованным спискам нужный аттрибут, чтобы не засорять scope всем li на странице
        $('.text-block li').attr('list-item', '');
    }])

    /**
     * Всплываюшие в отдельном окне таблицы
     */
    .directive('dialogTable', ['$templateCache', function($templateCache) {
        var tplI = 0;

        return {
            restrict: 'A',
            template: '<div class="btn btn-blue" dialog-show="dt-tpl-'+(tplI++)+'" dialog-class="no-max-width">'
                            + '{{ title }}'
                            + '<div ng-transclude style="display:none;"></div>'
                        + '</div>'
                ,
            replace: true,
            transclude: 'element',
            scope: {
                title: '@dialogTable',
                template: '@dialogShow'
            },
            link: function($scope, $element) {
                $element.children('div').children().removeAttr('dialog-table');
                $element.children('div').children().removeAttr('data-dialog-table');
                $element.children('div').children().removeAttr('ng:dialog-table');
                var template = $element.children('div').html();

                $element.children('div').remove();
                $templateCache.put($scope.template, '<div class="page-title d-line">'+$scope.title+'</div><div class="text-block">' + template + '</div>');
            }
        }
    }])

    /**
     * Разворачивание вложенных пунктов меню
     */
    .directive('deepContentMenuItem', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.find('.wrap-icon').on('click', function(e) {
                    e.preventDefault();
                    $element.toggleClass('close open');
                });
            }
        }
    }])

    /**
     * Стилизация нумерованных списков
     */
    .directive('listItem', [function() {
        return {
            restrict: 'A',
            scope:{},
            compile: function($element) {
                if ($element.closest('.text-block').length == 0 || $element.parent('ol').length == 0) {
                    return function() {};
                }

                $element.prepend('<span class="number">{{ parent ? parent+\'.\' : \'\' }}{{ index + 1 }})</span>');

                var $parent = $element.parent('ol').parent('li');
                if ($parent.length == 0) {
                    $parent = $element.parent('ol').prev('li');
                }

                return function(scope, $element) {
                    var update = function() {
                        scope.$apply(function() {
                            scope.index = $element.parent('ol').children('li').index($element);

                            if ($parent.length != 0) {
                                scope.parent = $parent.data('index');
                            }
                        });
                    }

                    scope.$watch('index', function() {
                        var str = scope.parent ? scope.parent+'.' : ''
                        str += scope.index + 1;

                        $element.data('index', str);
                    });

                    setTimeout(update, 1);
                    setInterval(update, 1000);
                }
            }
        }
    }])

    /**
     * Удобочитаемый размер файла
     */
    .filter('filesize', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '-';
            }

            if (typeof precision === 'undefined') {
                precision = 1;
            }

            var units = ['bytes', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb'];
            var number = Math.floor(Math.log(bytes) / Math.log(1024));

            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })
;

/**
 * Всплывающие окна
 */
angular.module('dialog', [])
    .config(['$anchorScrollProvider', function($anchorScrollProvider) {
        $anchorScrollProvider.disableAutoScrolling();
    }])

    /**
     * Сервис диалогов
     * onClose - событие по закрытию диалога
     */
    .service('$dialog', ['$compile', '$timeout', '$rootScope', function($compile, $timeout, $rootScope) {
        return {
            create: function(template, options) {
                options = $.extend({
                    onClose: function() {},
                    'class': ''
                }, options);

                var $dialog = $('<div dialog-window="'+template+'" ng-class="\''+options['class']+'\'"></div>');
                $dialog.appendTo('body');

                $timeout(function() {
                    var newScope = $rootScope.$new(true);
                    newScope.onClose = options.onClose;
                    $compile($dialog)(newScope);
                });
            }
        }
    }])

    /**
     * Кнопки показа всплывающиего окна
     */
    .directive('dialogShow', ['$dialog', function($dialog) {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                $element.on('click', function(e) {
                    e.preventDefault();
                    $dialog.create(attrs.dialogShow, {
                        'class': attrs.dialogClass
                    });
                });
            }
        }
    }])

    /**
     * Окно диалога
     */
    .directive('dialogWindow', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: true,
            template: '<div class="dialog">'
                        + '<div class="icon-close close-btn" ng-click="hide()"></div>'
                        + '<div class="w" ng-include="template" onload="show()"></div>'
                    + '</div>',
            replace: true,

            link: function($scope, $element, attrs) {
                $element.hide();
                $scope.state = 'hidden';
                $scope.template = attrs.dialogWindow;

                $(document).ready($scope.relocate);
                $(window).load($scope.relocate);
                $(window).resize($scope.relocate);
                $timeout($scope.relocate, 100);

                $scope.$overlay = $('<div class="dialog-overlay"></div>');
                $scope.$overlay.appendTo('body');
                $scope.$overlay.on('click', function() {
                    $scope.hide();
                });

                $timeout(function() {
                    if ($scope.state == 'hidden') {
                        $scope.$overlay.after('<s class="l"></s>');
                    }
                }, 300);
            },

            controller: function($scope, $element) {
                /**
                 * Обновляем позиционирование на экране
                 */
                $scope.relocate = function() {
                    if (document.body.clientHeight < $element.height()) {
                        $element.css({
                            position: 'absolute',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: 0,
                            top: $(window).scrollTop() + 15
                        });
                    } else {
                        $element.css({
                            position: 'fixed',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: -$element.height() / 2
                        });
                    }
                };

                $scope.show = function() {
                    $scope.relocate();
                    $scope.state = 'visible';

                    $element.stop(true, true).css({opacity:1}).fadeIn(300);
                    $scope.$overlay.stop(true, true).css({display:'block'}).animate({opacity:0.5}, 300, function() {
                        $timeout($scope.relocate);
                        $timeout($scope.relocate, 100);
                        $scope.$overlay.next('.l').remove();
                    });
                };

                $scope.hide = function() {
                    $scope.state = 'hidden';
                    $element.stop(true, true).fadeOut(200);
                    $scope.$overlay.stop(true, true).fadeOut(200, function() {
                        $scope.$overlay.remove();
                        $element.remove();
                        !!$scope.onClose && $scope.onClose();
                    });
                };
            }
        }
    }])
;

/**
 * Словарь сокращений
 */
angular.module('dictionary', [])
    /**
     * Алфавитный указатель
     */
    .directive('dictionaryAlphabet', [function() {
        return {
            restrict: 'C',

            link: function($scope, $element) {
                $element.find('.item').on('click', function(e) {
                    e.preventDefault();

                    var $letter = $('.letter[data-letter="'+$(this).text()+'"]');

                    $.scrollTo($letter, 200, {offset:{top:-55}});
                });
            }
        }
    }])

    /**
     * Список сокращений
     */
    .directive('dictionaryList', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.children('.item').children('a').attr('ng-click', 'toggle($event)')
            },

            controller: function($scope) {
                /**
                 * Переключение видимости элемента
                 */
                $scope.toggle = function($event) {
                    $event.preventDefault();

                    var $item = $($event.srcElement).parent('.item');
                    $item.children('.text-block').slideToggle(200);
                }
            }
        }
    }])
;

/**
 * Различные фильтры
 */
angular.module('filters', [])
    /**
     * Удаление времени из даты
     */
    .filter('dateRemoveTime', [function() {
        return function(value) {
            return value.toString().replace(/ \d{1,2}:\d{1,2}/, '');
        }
    }])
;

/**
 * Модуль работы с формами
 */
angular.module('forms', [])
    .run([function() {
        if (!$.fn.serializeObject) {
            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });

                return o;
            };
        }
    }])

    /**
     * Всплывающие сообщения
     */
    .service('$bubble', [function() {
        /**
         * Класс сообщения в зависимости от типа
         */
        var types = {
            'default': '',
            'error': 'error'
        };

        var bubble = {
            /**
             * Удаление всплывающего сообщения у элемента
             */
            remove: function($element) {
                var $message;

                if ($element.length > 1) {
                    $element.each(function() {
                        bubble.remove($(this));
                    });

                    return;
                }

                if ($message = $element.data('forms.bubble.message')) {
                    $message.fadeOut(150, function() {
                        $message.remove();
                    });

                    $element.data('forms.bubble.message', null);
                }
            },

            /**
             * Показ сообщения об ошибка
             * @param  {string} message Сообщение
             * @param  {object} $element  Элемент jQuery (не обязательно)
             */
            error: function(message, $element) {
                if ($element) {
                    $element.each(function() {
                        bubble.showOnElement(message, $(this), 'error');
                    });
                }
            },

            /**
             * Показывает сообщение над элементом
             * Если элемент не виден, то ищет первого видимого родителя
             *
             * @param  {string} message Сообщение
             * @param  {objecy} $element  Элемент
             * @param  {string} type Тип сообщения
             */
            showOnElement: function(message, $element, type) {
                var $exists;

                if ($exists = $element.data('forms.bubble.message')) {
                    bubble.remove($element);
                    setTimeout(function() {
                        bubble.showOnElement(message, $element, type);
                    }, 150);
                    return;
                }

                var $message = $('<div class="bubble"></div>');

                if (type && types[type]) {
                    $message.addClass(types[type]);
                }

                $message.html(message);

                $message.remove().appendTo('body').css({visibility:'hidden', top:0, left:0});

                $message.on('click', function() {
                    bubble.remove($element);
                });

                $('body').on('click', '.dialog-overlay, .dialog .close-btn', function() {
                    bubble.remove($element);
                });

                $element.on('remove', function() {
                    bubble.remove($element);
                });

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                $message.addClass('gravity-'+gravity).fadeIn(150);
                $message.append('<div class="arrow"></div>');

                $element.data('forms.bubble.message', $message);
                bubble.updatePosition($element);

                var interval = setInterval(function() {
                    if ($element.data('forms.bubble.message')) {
                        bubble.updatePosition($element);
                    } else {
                        clearInterval(interval);
                    }
                }, 100);
            },

            /**
             * Обновляет позицию всплывающего сообщения у элемента
             */
            updatePosition: function($element) {
                var $message = $element.data('forms.bubble.message');

                if (!$message) {
                    return;
                }

                var $el = $element;

                if (!$element.is(':visible')) {
                    $el = $element.closest(':visible').first();
                }

                var pos = $.extend({}, $el.offset(), {
                    width: $el[0].offsetWidth,
                    height: $el[0].offsetHeight
                });

                var options = {
                    offset: 0
                };

                var actualWidth = $message[0].offsetWidth,
                    actualHeight = $message[0].offsetHeight;

                var gravity = 'w';

                if ($element.data('gravity')) {
                    gravity = $element.data('gravity');
                }

                var tp = {opacity:1, visibility:'visible'};

                switch (gravity.charAt(0)) {
                    case 'n':
                        tp.top = pos.top + pos.height + options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 's':
                        tp.top = pos.top - actualHeight - options.offset;
                        tp.left = pos.left + pos.width / 2 - actualWidth / 2;
                        break;

                    case 'e':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left - actualWidth - options.offset;
                        break;

                    case 'w':
                        tp.top = pos.top + pos.height / 2 - actualHeight / 2;
                        tp.left = pos.left + pos.width + options.offset;
                        break;
                }

                if (gravity.length > 1) {
                    if (gravity.charAt(1) == 'w') {
                        tp.left = pos.left + pos.width / 2 - 15;
                    } else {
                        tp.left = pos.left + pos.width / 2 - actualWidth + 15;
                    }
                }

                $message.css(tp);
            }
        };

        return bubble;
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })

    /**
     * Аяксовая форма
     * Можно добавить свой контроллер с методами onSuccess, onError, onFatalError
     */
    .directive('ajaxForm', ['$bubble', '$http', function($bubble, $http) {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.find('input, textarea').each(function() {
                    $(this).attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.action;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input, textarea').on('change keyup', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors = {};
                        });
                    });

                    $element.on('submit', function(e) {
                        e.preventDefault();

                        return false;
                    });

                    $scope.$watch('errors', $scope.updateBubbles, true);
                }
            },

            controller: function($scope, $element) {
                $scope.submit = function() {
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                if (!!$scope.onSuccess) {
                                    $scope.onSuccess(response);
                                }
                            } else {
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }

                                if (!!$scope.onError) {
                                    $scope.onError();
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            if (!!$scope.onFatalError) {
                                $scope.onFatalError(response);
                            }

                            console.error(response);
                        });
                };

                /**
                 * Обновление всплывающих ошибок
                 */
                $scope.updateBubbles = function() {
                    $bubble.remove($element.find('input, select, textarea'));

                    var i = 0;
                    for(var name in $scope.errors) {
                        if (!$scope.errors.hasOwnProperty(name)) {
                            continue;
                        }

                        var errors = $scope.errors[name];

                        if (!errors || errors.length == 0) {
                            continue;
                        }

                        var $field = $element.find('*[name="'+name+'"]').first();

                        $bubble.error(errors.join(', '), $field);

                        i++;

                        if (i == 1 && !!$.scrollTo && $field.closest('.dialog').length==0) {
                            var $showEl = $field;

                            if (!$showEl.is(':visible')) {
                                $showEl = $showEl.closest(':visible').first();
                            }

                            if ($showEl.length > 0) {
                                $.scrollTo($showEl, 200, {offset:{top:-150}});
                            }
                        }
                    }
                };

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                };
            }
        }
    }])
;

angular.module('header', [])
    /**
     * Контроллер шапки сайта
     */
    .controller('headerPanelCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.showSearch = function() {
            $element.addClass('opened');
        };

        $scope.hideSearch = function() {
            $element.removeClass('opened');
        };

        $scope.toggleSearch = function() {
            $element.toggleClass('opened');
        };

        $(window).on('resize load scroll', function() {
            if ($(this).scrollTop() > 0) {
                $element.addClass('shadow');
            } else {
                $element.removeClass('shadow');
            }
        });
    }])

    /**
     * Выпадающее верхнее меню
     */
    .directive('dropdownTopmenu', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                var $subLevel = $element.children('.sub-level');

                if ($subLevel.length == 0) {
                    return;
                }

                $element.hover(function() {
                    if (!$subLevel.is(':animated')) {
                        $subLevel.stop(true, true).addClass('is-visible').slideDown(150);
                    }
                }, function() {
                    $subLevel.stop(true, true).removeClass('is-visible').slideUp(150);
                });
            }
        };
    }])

/**
 * Горизонтальный скролл, который прокручивает по элементам
 *     horizontal-scroll - имя скролла (необязательное, используется для $scope.$broadcast('setPosition', 'gs', 1);)
 *     horizontal-scroll-items - селектор для жлементов
 *     horizontal-scroll-wrap - селектор для враппера
 *     step - числовое значение шага скрола, либо "item" для пров\мотки по элементам
 *     item-active = селектор активного элементв
 *     btn-all - Текст для кнопки Свернуть/Развернуть
 *
 * Пример вызова:
 *     <div class="items-wrap" horizontal-scroll horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active" btn-all="Смотреть все альбомы">
 *     </div>
 */
angular.module('horizontal.scroll', [])
    /**
     * Скролл
     */
    .directive('horizontalScroll', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element, attrs) {
                var itemsSelector = attrs.horizontalScrollItems;
                var wrapSelector = attrs.horizontalScrollWrap;

                var $wrap = $element.find(wrapSelector);
                var $items = $element.find(itemsSelector);

                if ($wrap.length == 0 || $items.length < 2) {
                    return;
                }

                var itemsWidth = 0;
                var itemsWidthArr = [];

                var recalcItemsWidth = function() {
                    itemsWidth = 0;
                    itemsWidthArr = [];

                    $items.each(function() {
                        itemsWidth += $(this).outerWidth(true);
                        itemsWidthArr.push(itemsWidth);
                    });
                };

                recalcItemsWidth();

                var scrollTmpl = '<div class="horizontal-scroll" ng-show="viewMode">';
                    scrollTmpl += '<div class="arrow left"></div>';
                    scrollTmpl += '<div class="horizontal-slider"></div>';
                    scrollTmpl += '<div class="arrow right"></div>';
                scrollTmpl += '</div>';

                // Кнопка "Смотреть все"
                if (!!attrs.btnAll) {
                    scrollTmpl += '<div class="hs-btn-all">';
                        scrollTmpl += '<div class="btn btn-deep-blue" ng-click="toggleView()">{{ viewMode ? btnAllText : \'Свернуть\' }}</div>';
                    scrollTmpl += '</div>';
                }

                var $scroll = $(scrollTmpl);
                var $slider = $scroll.find('.horizontal-slider');

                $element.append($scroll);
                $wrap.attr('without-wrap', 'viewMode');

                return function($scope, $element, attrs) {
                    $scope.position = 0;
                    $scope.viewMode = true;
                    $scope.btnAllText = attrs.btnAll;
                    $scope.leftOffset = 0;
                    $scope.id = attrs.horizontalScroll;

                    var updateSlider = function() {};
                    var onPositionUpdate = function() {};
                    var onOffsetUpdate = function() {};

                    switch(attrs.step) {
                        // Перемотка по элементам
                        case 'item':
                            updateSlider = function() {
                                var max = $items.length-1;

                                for(var i = 0; i <= itemsWidthArr.length-1; i++) {
                                    var left = itemsWidthArr[i];
                                    var diff = (itemsWidth - $element.width()) - left;

                                    if (diff <= 0) {
                                        max = i + 1;
                                        break;
                                    }
                                }

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (!$slider.data('ui-slider')) {
                                    return;
                                }

                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $slider.slider('value', position);

                                var left = $items.eq(position).position().left;

                                if (left > (itemsWidth - $element.width())) {
                                    left = (itemsWidth - $element.width());
                                }

                                $scope.leftOffset = -left;
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 200);
                            };

                            break;

                        // Перемотка фиксированным шагом
                        default:
                            updateSlider = function() {
                                var step = !!attrs.step ? parseInt(attrs.step, 10) : 1;
                                var max = (itemsWidth - $element.width());

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    step: step,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (!$slider.data('ui-slider')) {
                                    return;
                                }

                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $scope.leftOffset = -position;
                                $slider.slider('value', position);
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 13);
                            };

                            break;
                    }

                    var isLoaded = false;

                    var interval = setInterval(function() {
                        recalcItemsWidth();

                        $scope.$apply(function() {
                            updateSlider();
                        });
                    }, 300);

                    $(window).on('load', function() {
                        isLoaded = true;
                        recalcItemsWidth();

                        $scope.$apply(function() {
                            updateSlider();
                            onPositionUpdate($scope.position);
                            onOffsetUpdate($scope.leftOffset);
                        });

                        if (($wrap.width() <= $element.width()) || ($scope.maxPosition == 0)) {
                            $wrap.stop().css({marginLeft:0});
                            $scope.$apply(function() {
                                $scope.viewMode = false;
                            });
                        } else {
                            $wrap.stop().css({marginLeft:0});
                            $scope.$apply(function() {
                                $scope.viewMode = true;
                            });
                        }

                        if (!!attrs.itemActive && attrs.step == 'item') {
                            var $activeItem = $items.filter(attrs.itemActive).eq(0);

                            if ($activeItem.length == 0) {
                                return;
                            }

                            $scope.$apply(function() {
                                $scope.position = Math.floor((($activeItem.index() + 1) / $items.length) * $scope.maxPosition);
                            });
                        }
                    });

                    $scope.$watch('viewMode', function(viewMode) {
                        if (viewMode) {
                            $wrap.stop().css('marginLeft', $scope.leftOffset);
                        } else {
                            $wrap.stop().css('marginLeft', 0);
                        }
                    });

                    $scope.$on('setPosition', function(event, id, position) {
                        if (id == $scope.id) {
                            $scope.position = position;
                        }
                    });

                    $scope.$watch('position', onPositionUpdate);
                    $scope.$watch('position', function(position) {
                        $scope.$emit('positionUpdated', position);
                    });

                    $scope.$watch('leftOffset', onOffsetUpdate);
                };
            },

            controller: function($scope, $element) {
                $scope.toggleView = function() {
                    $element.stop(true, true).animate({opacity:0.2}, 200, function() {
                        $scope.$apply(function() {
                            $scope.viewMode = !$scope.viewMode;
                        });
                        $element.stop(true, true).animate({opacity:1}, 200);
                    });
                }
            }
        }
    }])

    /**
     * Автоматическое подстраивание ширины родителя под дочерние элементы
     */
    .directive('withoutWrap', [function() {
        return {
            restrict: 'A',
            scope:{
                viewMode:'=withoutWrap'
            },
            link: function($scope, $element, attrs) {
                var $childrens = $element.children();
                var defPosition = $element.css('position');
                var defLeft = $element.css('left');
                var defRight = $element.css('right');

                $scope.update = function() {
                    if (!$scope.viewMode) {
                        return;
                    }

                    var width = 1;

                    $element.css({width:'auto'});

                    $childrens.each(function() {
                        width += $(this).outerWidth(true);
                    });

                    $element.width(width);
                };

                $(window).on('load resize', $scope.update);
                $scope.$on('updateWithoutWrap', $scope.update);

                $scope.$watch('viewMode', function(viewMode) {
                    $element.removeClass('ww-on ww-off');

                    if (viewMode) {
                        $element.addClass('ww-on');
                        $scope.update();
                    } else {
                        var height = $element.height();
                        $element.addClass('ww-off');
                        $element.css({width:'auto', height:'auto'});
                        var heightTo = $element.height();
                        $element.css({height:height});
                        $element.stop(true, true).animate({height:heightTo}, 200, function() {
                            $element.css({height:'auto'});
                        });
                    }
                });
            }
        }
    }])
;

angular.module('icons.slider', [])
    /**
     * Слайдер изображений/видео новости
     */
    .directive('iconsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowPrev = $('<div class="arrow prev" ng-click="prev()" ng-class="{hidden: $items.length == 1 || hideControls}"><i class="icon-arrow-prev"></i><i class="icon-arrow-prev-mini"></i></div>');
                var $arrowNext = $('<div class="arrow next" ng-click="next()" ng-class="{hidden: $items.length == 1 || hideControls}"><i class="icon-arrow-next"></i><i class="icon-arrow-next-mini"></i></div>');

                $element.append($arrowPrev).append($arrowNext);

                return function($scope, $element) {
                    $scope.hideControls = false;
                    $scope.$items = $element.children('.item');

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$items.eq(active);
                        $scope.$items.stop().css({opacity:1}).not($active).fadeOut(200);

                        $active.fadeIn(150);
                    });

                    if ($element.hasClass('img-left')) {
                        $(window).load(function() {
                            var w = -1;
                            var h = -1;

                            $element.find('img').each(function() {
                                w = Math.max(w, this.width);
                                h = Math.max(h, this.height);
                            });

                            if (w > 0) {
                                $element.stop().css({
                                    width: w
                                });
                            }

                            if (h > 0) {
                                $element.stop().css({
                                    height: h
                                });
                            }
                        });
                    }

                    // Реакция на видео-плеер
                    // $scope.$on('video-play', function(event) {
                    //     event.stopPropagation();
                    //     $scope.hideControls = true;
                    // });

                    // $scope.$on('video-stop', function(event) {
                    //     event.stopPropagation();
                    //     $scope.hideControls = false;
                    // });
                }
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.next = function() {
                    if ($scope.active < $scope.$items.length - 1) {
                        $scope.active++;
                    } else {
                        $scope.active = 0;
                    }

                    $scope.$broadcast('iconsSlider.next');
                };

                $scope.prev = function() {
                    if ($scope.active > 0) {
                        $scope.active--;
                    } else {
                        $scope.active = $scope.$items.length - 1;
                    }

                    $scope.$broadcast('iconsSlider.prev');
                };
            }
        }
    }])
;

/**
 * Карта города на главной странице
 */
angular.module('indexmap', [])
    .directive('indexDistrictMap', [function() {
        /**
         * Координаты центра полигона
         */
        var getPolygonCenter = function(polygon) {
            if (!!polygon.pmCoordinates) {
                return polygon.pmCoordinates;
            }

            var bounds = polygon.geometry.getBounds();

            return [(bounds[0][0] + bounds[1][0])/2, (bounds[0][1] + bounds[1][1])/2];
        };

        /**
         * Генерация контента балуна метки
         */
        var getDescription = function(obj) {
            var descr = '<div style="font-weight:bold; font-size:13px;">'+obj.name+'</div>';

            if (!!obj.description) {
                descr += '<div style="margin-top:7px; font-size:13px;">'+obj.description+'</div>';
            }

            if (!!obj.link) {
                descr += '<a href="'+obj.link+'" class="btn btn-blue" style="margin-top:7px;">Подробнее</a>';
            }

            return descr;
        };

        var regionIndex = [
            'zapad',
            'center',
            'kuban',
            'karas'
        ];

        return {
            restrict: 'A',
            scope: true,

            link: function($scope, $element) {
                $scope.isLoaded = false;
                $scope.fullscreen = false;

                $scope.visibility = {
                    regions: true,
                    districts: false,
                    onlyRegion: ''
                };

                $scope.$watch('visibility', $scope.update, true);

                $scope.$watch('fullscreen', function(fullscreen) {
                    if (fullscreen) {
                        $element.addClass('fullscreen');
                    } else {
                        $element.removeClass('fullscreen');
                    }
                });

                ymaps.ready(function() {
                    $scope.$apply(function() {
                        $scope.isLoaded = true;

                        $scope.map = new ymaps.Map ($element.attr('id'), {
                            center: [45.035407, 38.975277],
                            zoom: 10
                        }, {
                            autoFitToViewport: 'always',
                            adjustZoomOnTypeChange: true,
                            maxAnimationZoomDifference: 15
                        });

                        $scope.map.controls.add('zoomControl');

                        var districtButton = new ymaps.control.Button('Выключить округа');

                        var fullscreenButton = new ymaps.control.Button({
                            data: {
                                image: '/bundles/krdsite/i/ymap-fullscreen.png'
                            }
                        });

                        var zapadDistrictButton = new ymaps.control.Button('Западный округ');
                        var centerDistrictButton = new ymaps.control.Button('Центральный округ');
                        var kubanDistrictButton = new ymaps.control.Button('Прикубанский округ');
                        var karasDistrictButton = new ymaps.control.Button('Карасунский округ');

                        var districtButtonGroup = new ymaps.control.RadioGroup({
                            items: [zapadDistrictButton, centerDistrictButton, kubanDistrictButton, karasDistrictButton]
                        });

                        var toolBar = new ymaps.control.ToolBar();
                        toolBar.add(districtButtonGroup);
                        toolBar.add(districtButton);
                        toolBar.add(fullscreenButton);

                        $scope.map.controls.add(toolBar, {top:5, left:5});


                        districtButton.events.add('select', function() {
                            $scope.$apply(function(){
                                $scope.visibility.regions = false;
                            });
                        });

                        districtButton.events.add('deselect', function() {
                            $scope.$apply(function(){
                                $scope.visibility.regions = true;
                            });
                        });


                        fullscreenButton.events.add('select', function() {
                            $scope.$apply(function(){
                                $scope.fullscreen = true;
                            });
                        });

                        fullscreenButton.events.add('deselect', function() {
                            $scope.$apply(function(){
                                $scope.fullscreen = false;
                            });
                        });

                        $scope.map.events.add('boundschange', function(event) {
                            $scope.$apply(function() {
                                $scope.visibility.districts = (event.get('newZoom') >= 11);
                            });
                        });

                        zapadDistrictButton.events.add('select', function() {
                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = 'zapad';
                            });
                        });

                        zapadDistrictButton.events.add('click', function() {
                            if (!zapadDistrictButton.isSelected()) {
                                return;
                            }

                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = false;
                            });
                        });

                        centerDistrictButton.events.add('select', function() {
                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = 'center';
                            });
                        });

                        centerDistrictButton.events.add('click', function() {
                            if (!centerDistrictButton.isSelected()) {
                                return;
                            }

                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = false;
                            });
                        });

                        kubanDistrictButton.events.add('select', function() {
                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = 'kuban';
                            });
                        });

                        kubanDistrictButton.events.add('click', function() {
                            if (!kubanDistrictButton.isSelected()) {
                                return;
                            }

                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = false;
                            });
                        });

                        karasDistrictButton.events.add('select', function() {
                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = 'karas';
                            });
                        });

                        karasDistrictButton.events.add('click', function() {
                            if (!karasDistrictButton.isSelected()) {
                                return;
                            }

                            $scope.$apply(function() {
                                $scope.visibility.onlyRegion = false;
                            });
                        });

                        $scope.$watch('fullscreen', function(fullscreen) {
                            districtButtonGroup.options.set('visible', fullscreen);
                        });

                        $scope.$watch('visibility.onlyRegion', function() {
                            if ($scope.visibility.onlyRegion) {
                                $scope.collection.each(function(obj) {
                                    if (obj.type == 'region' && obj.isPolygon) {
                                        var r = false;

                                        if ($.isArray(obj.district)) {
                                            r = $.inArray($scope.visibility.onlyRegion, obj.district) !== -1;
                                        } else {
                                            r = ($scope.visibility.onlyRegion == obj.district);
                                        }

                                        if (r) {
                                            setTimeout(function() {
                                                $scope.map.setBounds(obj.geometry.getBounds(), {
                                                    checkZoomRange:true,
                                                    duration:200,
                                                    callback: function() {
                                                        $scope.$apply(function() {
                                                            $scope.visibility.districts = true;
                                                        });
                                                    }
                                                });

                                                $element.parent().scope().active = $.inArray($scope.visibility.onlyRegion, regionIndex);
                                                $element.parent().scope().$apply();
                                            });
                                        }
                                    }
                                });

                                switch ($scope.visibility.onlyRegion) {
                                    case 'zapad':
                                        setTimeout(function(){zapadDistrictButton.select()});
                                        break;

                                    case 'center':
                                        setTimeout(function(){centerDistrictButton.select()});
                                        break;

                                    case 'kuban':
                                        setTimeout(function(){kubanDistrictButton.select()});
                                        break;

                                    case 'karas':
                                        setTimeout(function(){karasDistrictButton.select()});
                                        break;
                                }
                            } else {
                                $scope.visibility.districts = false;

                                setTimeout(function() {
                                    $scope.map.setCenter([45.035407, 38.975277], 10, {
                                        checkZoomRange:true,
                                        duration:200
                                    });

                                    zapadDistrictButton.deselect();
                                    centerDistrictButton.deselect();
                                    kubanDistrictButton.deselect();
                                    karasDistrictButton.deselect();

                                    $element.parent().scope().active = -1;
                                    $element.parent().scope().$apply();
                                });
                            }
                        });

                        $scope.$on('tabChange', function(event, active) {
                            $scope.visibility.onlyRegion = regionIndex[active];
                        });

                        $scope.initMap();
                        $scope.update();
                    });
                });
            },

            controller: function($scope) {
                $scope.initMap = function() {
                    if (!$scope.isLoaded) {
                        return;
                    }

                    if (!!$scope.collection) {
                        $scope.map.geoObjects.remove($scope.collection);
                    }

                    $scope.collection = new ymaps.GeoObjectArray();
                    $scope.map.geoObjects.add($scope.collection);

                    $.each(index_module_objects, function(i, obj) {
                        var polygonOptions = {
                            strokeWidth: 3,
                            cursor: 'default'
                        };

                        var placemarkOptions = {
                            iconImageSize: [27, 43],
                            iconImageOffset: [-14, -36]
                        };

                        switch (obj.type) {
                            case 'district':
                                polygonOptions.strokeColor = '#09689f';
                                polygonOptions.fillColor = '#09689f';
                                polygonOptions.fillOpacity = 0.2;

                                placemarkOptions.iconImageHref = '/bundles/krdsite/i/placemark.png';
                                placemarkOptions.iconImageHrefHover = '/bundles/krdsite/i/placemark-hover.png';

                                break;

                            case 'region':
                                polygonOptions.strokeColor = '#244e64';
                                polygonOptions.fillColor = '#244e64';
                                polygonOptions.fillOpacity = 0;

                                placemarkOptions.iconImageHref = '/bundles/krdsite/i/placemark-deep.png';
                                placemarkOptions.iconImageHrefHover = '/bundles/krdsite/i/placemark-hover.png';

                                break;
                        }

                        var polygon = new ymaps.Polygon(obj.geometry, {}, polygonOptions);

                        polygon.pmCoordinates = obj.pmCoordinates;
                        polygon.type = obj.type;
                        polygon.district = obj.district;
                        polygon.isPolygon = true;

                        $scope.collection.add(polygon);

                        var placemark = new ymaps.Placemark(getPolygonCenter(polygon), {
                            balloonContent: getDescription(obj), hintContent: obj.name
                        }, placemarkOptions);

                        placemark.events
                            .add('mouseenter', function(e) {
                                e.get('target').options.set('iconImageHref', placemarkOptions.iconImageHrefHover);
                            })
                            .add(['mouseleave', 'click'], function(e) {
                                e.get('target').options.set('iconImageHref', placemarkOptions.iconImageHref);
                            });

                        placemark.type = obj.type;
                        placemark.district = obj.district;

                        $scope.collection.add(placemark);
                    });
                };

                // Обновление состояния карты
                $scope.update = function() {
                    if (!$scope.isLoaded || !$scope.collection) {
                        return;
                    }

                    $scope.collection.each(function(obj) {
                        var visible = false;

                        if ($scope.visibility.onlyRegion) {
                            if ($.isArray(obj.district)) {
                                visible = $.inArray($scope.visibility.onlyRegion, obj.district) !== -1;
                            } else {
                                visible = ($scope.visibility.onlyRegion == obj.district);
                            }

                            if (!visible) {
                                obj.options.set('visible', visible);
                                return;
                            }
                        }

                        switch (obj.type) {
                            case 'district':
                                visible = $scope.visibility.districts;
                                break;

                            case 'region':
                                visible = $scope.visibility.regions;
                                break;
                        }

                        obj.options.set('visible', visible);
                    });
                };
            }
        }
    }])
;


var index_module_objects = [
    {
        name:"9 км ростовского шоссе",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.097304,38.979417],[45.097228,38.980426],[45.09735,38.981048],[45.096818,38.986884],[45.09662,38.987056],[45.096164,38.991519000000004],[45.080584,38.988064],[45.075841000000004,38.986862],[45.07571900000001,38.986712000000004],[45.07605300000001,38.982592000000004],[45.07574900000001,38.982463],[45.07581000000001,38.977077],[45.07667700000001,38.974116],[45.09180100000001,38.977954000000004],[45.09517200000001,38.978753000000005],[45.09730000000001,38.979375000000005],[45.097304,38.979417]]]
    },
    {
        name:"Авиагородок",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.097366,38.979251],[45.074565,38.973544],[45.075994,38.96084],[45.094722000000004,38.966919999999995],[45.095299000000004,38.962928999999995],[45.09931100000001,38.963615],[45.09754800000001,38.978722],[45.097366,38.979251]]]
    },
    {
        name:"Аврора",
        description: 'Район кинотеатра Аврора',
        type: 'district',
        district: 'zapad',
        geometry:[[[45.068646,38.985091],[45.068676,38.980864],[45.068782000000006,38.976507999999995],[45.06876700000001,38.973257],[45.069041000000006,38.972935],[45.068912000000005,38.972731],[45.068532000000005,38.97252700000001],[45.06844100000001,38.97145400000001],[45.06518700000001,38.971272000000006],[45.06532400000001,38.96722700000001],[45.06437400000001,38.96747400000001],[45.058192000000005,38.97060700000001],[45.056557000000005,38.97134700000001],[45.05381200000001,38.97131500000001],[45.051736000000005,38.97135800000001],[45.05305200000001,38.97573500000001],[45.05375200000001,38.97791300000001],[45.05388100000001,38.98178600000001],[45.05366800000001,38.98299800000001],[45.05466400000001,38.98760100000001],[45.05911600000001,38.98560300000001],[45.06159500000001,38.984530000000014],[45.06377000000001,38.98440100000001],[45.068621000000014,38.985109000000016],[45.068646,38.985091]]]
    },
    {
        name:"Витаминкомбинат",
        description: '',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.15538451174878, 38.98608893920825],
        geometry:[[[45.167477,38.984034],[45.167719999999996,38.999226],[45.146184,38.999312],[45.145964,38.992821000000006],[45.147338,38.99319700000001],[45.149069,38.95810300000001],[45.167398999999996,38.98398100000001],[45.167477,38.984034]]]
    },
    {
        name:"ГМР",
        description: 'Микрорайон Гидростроителей, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        geometry:[[[45.012723,39.055755],[45.014367,39.062235],[45.01356,39.063319],[45.012898,39.064037],[45.012487,39.064864],[45.012395,39.066398],[45.012609,39.070271],[45.012525,39.072942],[45.012570999999994,39.075945999999995],[45.013513999999994,39.08112799999999],[45.01086599999999,39.08244799999999],[45.00923699999999,39.08249099999999],[45.007425999999995,39.08216899999999],[45.006406,39.081675999999995],[45.003757,39.081846999999996],[45.001326,39.081075],[45.00055,39.08133],[44.998250999999996,39.081222000000004],[44.992678999999995,39.081340000000004],[44.991963,39.06747300000001],[44.99529,39.070367000000005],[44.996587999999996,39.070398000000004],[44.995470999999995,39.054685000000006],[44.997353,39.05524200000001],[44.998931999999996,39.05682800000001],[45.001326999999996,39.05931600000001],[45.003721999999996,39.06129000000001],[45.007456,39.062233000000006],[45.009296,39.06221200000001],[45.011226,39.06156800000001],[45.012365,39.05886400000001],[45.012773,39.055795],[45.012723,39.055755]]]
    },
    {
        name:"ЗИП",
        description: 'Завод измерительных приборов, Прикубанский внутригородской округ',
        type: 'district',
        district: ['kuban', 'center'],
        geometry:[[[45.072424,38.986234],[45.071397999999995,38.986035],[45.069345,38.985483],[45.066806,38.984973000000004],[45.065406,38.98474],[45.063962000000004,38.984581000000006],[45.062861000000005,38.98459100000001],[45.0617,38.984750000000005],[45.057061000000004,38.986602000000005],[45.05455500000001,38.987832000000004],[45.054778000000006,38.989302],[45.05469300000001,38.98957],[45.05451800000001,38.989796],[45.05481400000001,38.994346],[45.055065000000006,38.999176],[45.055369000000006,39.007826],[45.05579600000001,39.016573],[45.057666000000005,39.016821],[45.06821300000001,39.015995],[45.07203700000001,39.015747999999995],[45.07215100000001,39.01590899999999],[45.071984000000015,39.00825699999999],[45.072060000000015,39.00068999999999],[45.072288000000015,38.986135999999995],[45.072424,38.986234]]]
    },
    {
        name:"ККБ",
        description: 'Район краевой клинической больницы',
        type: 'district',
        district: ['kuban', 'center'],
        pmCoordinates: [45.06050985349566, 39.025080410155326],
        geometry:[[[45.054415,38.987851],[45.054628,38.989267],[45.054354000000004,38.989729],[45.05481,38.998183],[45.055578000000004,39.016614999999994],[45.058240000000005,39.01705499999999],[45.064804,39.01646999999999],[45.072465,39.01600399999999],[45.072634,39.02108399999999],[45.071981,39.02114599999999],[45.073457,39.024039999999985],[45.07336,39.034376999999985],[45.061402,39.034843999999985],[45.051538,39.03481199999999],[45.051158,39.02624999999999],[45.050991,39.02328899999999],[45.05082,39.01777499999999],[45.048677000000005,39.01793099999999],[45.04890700000001,39.01551199999999],[45.05059500000001,39.00301299999999],[45.04990200000001,38.999805999999985],[45.05013800000001,38.99884999999998],[45.05003500000001,38.99434999999998],[45.04921800000001,38.991500999999985],[45.04885800000001,38.989713999999985],[45.04853600000001,38.98878599999998],[45.05036900000001,38.98856099999998],[45.05440000000001,38.98785299999998],[45.054415,38.987851]]]
    },
    {
        name:"КМР",
        description: 'Комсомольский микрорайон, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        pmCoordinates: [45.037325794287646, 39.09691817187414],
        geometry:[[[45.041313,39.08678],[45.044195,39.114008999999996],[45.044515000000004,39.117431999999994],[45.044872000000005,39.129051],[45.042492,39.128600999999996],[45.041396000000006,39.12851499999999],[45.041396000000006,39.12439499999999],[45.04191300000001,39.124115999999994],[45.042370000000005,39.12353699999999],[45.043009000000005,39.12220599999999],[45.04334300000001,39.11975999999999],[45.04322200000001,39.11821499999999],[45.04279600000001,39.11615499999999],[45.04206600000001,39.11495399999999],[45.04124400000001,39.11498599999999],[45.04042300000001,39.11546899999999],[45.03912200000001,39.11584399999999],[45.03842200000001,39.11579599999999],[45.03786700000001,39.11487899999999],[45.037680000000016,39.11365499999999],[45.037692000000014,39.112056999999986],[45.03798800000001,39.109481999999986],[45.03810400000001,39.10812499999999],[45.03790100000001,39.10711099999999],[45.037500000000016,39.10625799999999],[45.03604100000002,39.10405299999999],[45.03493000000002,39.10340899999999],[45.03366800000002,39.10319499999999],[45.03277000000002,39.102475999999996],[45.03226800000002,39.10164999999999],[45.032022000000026,39.10097699999999],[45.03057100000002,39.09953599999999],[45.03042300000002,39.09873699999999],[45.03006900000002,39.09809899999999],[45.02957100000002,39.09784099999999],[45.02905000000002,39.09763699999999],[45.02860800000002,39.097711999999994],[45.02808300000002,39.09765899999999],[45.02767200000002,39.09639299999999],[45.02702600000001,39.095437999999994],[45.02703300000001,39.09495499999999],[45.02656200000001,39.09354999999999],[45.02601400000001,39.09227299999999],[45.02599700000001,39.09163699999999],[45.025858000000014,39.091387999999995],[45.02491100000002,39.09056699999999],[45.02680500000002,39.09100699999999],[45.027501000000015,39.09086199999999],[45.027543000000016,39.090158999999986],[45.02811900000002,39.089989999999986],[45.028376000000016,39.08967099999999],[45.03070800000002,39.08925799999999],[45.03625000000002,39.08807799999999],[45.037371000000014,39.08766799999999],[45.03875100000001,39.08736399999999],[45.040275000000015,39.087082999999986],[45.04131300000002,39.08675799999999],[45.041313,39.08678]]]
    },
    {
        name:"Кожзавод",
        description: 'Район кожзавода',
        type: 'district',
        district: 'zapad',
        geometry:[[[45.043122,38.956376],[45.041973,38.956054],[45.040224,38.955453],[45.039091,38.954917],[45.037767,38.954509],[45.036717,38.954144],[45.036534,38.953887],[45.036473,38.953630000000004],[45.036275,38.953394],[45.036534,38.952858000000006],[45.036762,38.95212800000001],[45.036868000000005,38.95103400000001],[45.03708100000001,38.950326000000004],[45.037096000000005,38.949017000000005],[45.03697400000001,38.94856600000001],[45.03708000000001,38.94813700000001],[45.03712600000001,38.94730000000001],[45.03694300000001,38.946849000000014],[45.03697300000001,38.945433000000016],[45.03726200000001,38.944231000000016],[45.037064000000015,38.94339400000002],[45.03712500000002,38.94221400000002],[45.037019000000015,38.93957500000002],[45.03689700000002,38.938566000000016],[45.03689700000002,38.93751500000002],[45.03666900000002,38.93727900000002],[45.03639500000002,38.93582000000002],[45.03627300000002,38.934468000000024],[45.03619700000002,38.93423200000002],[45.03612100000002,38.933781000000025],[45.03545200000002,38.93129200000003],[45.03774900000002,38.92972600000003],[45.03884400000002,38.93264400000003],[45.03954400000002,38.93433900000003],[45.04001600000002,38.93532600000003],[45.040229000000025,38.93558300000003],[45.04111100000002,38.93605500000003],[45.04166600000002,38.93611900000003],[45.04611600000002,38.937514000000036],[45.04311900000002,38.95639700000004],[45.043122,38.956376]]]
    },
    {
        name:"КСК",
        description: 'Микрорайон Камвольно-суконного комбината, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        geometry:[[[45.038262,39.056835],[45.03714,39.057602],[45.035805,39.058748],[45.035896,39.05904],[45.035501000000004,39.059689000000006],[45.035413000000005,39.061277000000004],[45.035417,39.063208],[45.033987,39.063401000000006],[45.031781,39.063627000000004],[45.029635,39.063895],[45.02833,39.064276],[45.02421399999999,39.064378],[45.024452999999994,39.067257999999995],[45.021516999999996,39.068802999999996],[45.01748499999999,39.070004999999995],[45.01900799999999,39.071425999999995],[45.019662999999994,39.072317],[45.020255999999996,39.072756999999996],[45.02124499999999,39.072874999999996],[45.02153499999999,39.072671],[45.021975999999995,39.072499],[45.022417,39.072499],[45.022850999999996,39.072596],[45.023109999999996,39.073229],[45.023422,39.07413],[45.02397,39.075246],[45.024395999999996,39.076566],[45.02463899999999,39.077665],[45.024927999999996,39.078754],[45.025118,39.081211],[45.024803,39.081994],[45.025301,39.082477000000004],[45.025228999999996,39.083604],[45.025408,39.084301],[45.025414999999995,39.085588],[45.02512599999999,39.086576],[45.025045999999996,39.087192],[45.024708,39.088539000000004],[45.024654,39.08970300000001],[45.024943,39.09047000000001],[45.026739,39.09091000000001],[45.027447,39.09076000000001],[45.027492,39.09003000000001],[45.028105000000004,39.08985900000001],[45.028306,39.08960100000001],[45.031464,39.08903200000001],[45.034568,39.08830300000001],[45.036253,39.08800800000001],[45.037481,39.08752000000001],[45.039862,39.08704800000001],[45.040718,39.08684400000001],[45.041224,39.086447000000014],[45.038257,39.056835000000014],[45.038262,39.056835]]]
    },
    {
        name:"МХГ",
        description: 'Район микрохирургии глаза',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.06279025035758, 38.90276467456029],
        geometry:[[[45.058026,38.927747],[45.055783,38.925279999999994],[45.059463,38.918842999999995],[45.059254,38.91864999999999],[45.058893000000005,38.91867099999999],[45.057959000000004,38.917630999999986],[45.056843,38.916696999999985],[45.052991,38.91226599999999],[45.053318,38.91135399999999],[45.053987,38.90974499999999],[45.055006,38.90708399999998],[45.055538,38.90390799999998],[45.056238,38.89884299999998],[45.056458,38.89584999999998],[45.056595,38.88911199999998],[45.064105000000005,38.89017399999998],[45.067485000000005,38.89075099999998],[45.070803000000005,38.89164999999998],[45.077516,38.89355399999998],[45.077539,38.89407999999998],[45.077699,38.89446599999998],[45.068804,38.90771499999998],[45.063809,38.917425999999985],[45.058014,38.92774699999998],[45.058026,38.927747]]]
    },
    {
        name:"ПМР",
        description: 'Пашковский микрорайон, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        pmCoordinates: [45.0230228473401, 39.11073755566305],
        geometry:[[[45.037674,39.115786],[45.038417,39.11674],[45.040042,39.116363],[45.041133,39.116168],[45.041519,39.116679000000005],[45.042008,39.117558],[45.042081,39.118129],[45.042442,39.119229000000004],[45.042723,39.119710000000005],[45.042675,39.120116],[45.042640000000006,39.121012],[45.04179200000001,39.122401],[45.04124000000001,39.122751],[45.04077500000001,39.123108],[45.04096700000001,39.124127],[45.04129800000001,39.12439],[45.04131800000001,39.128530999999995],[45.03950000000001,39.128446],[45.034357000000014,39.129132],[45.031573000000016,39.130055],[45.03017300000002,39.130311999999996],[45.02918400000002,39.130311999999996],[45.027967000000025,39.129861999999996],[45.025775000000024,39.128316999999996],[45.02069300000002,39.124582999999994],[45.012626000000026,39.11887599999999],[45.00792300000003,39.11321099999999],[45.006629000000025,39.11180499999999],[45.00627900000003,39.11078599999999],[45.006001000000026,39.10943599999999],[45.00618000000003,39.108001999999985],[45.00678100000003,39.10669799999999],[45.00797700000003,39.104916999999986],[45.007596000000035,39.103709999999985],[45.01062500000003,39.100872999999986],[45.013478000000035,39.09836199999999],[45.014190000000035,39.09769099999999],[45.01445900000004,39.09738899999999],[45.014682000000036,39.09697799999999],[45.015344000000034,39.094981999999995],[45.015929000000035,39.093723999999995],[45.015876000000034,39.092884999999995],[45.015031000000036,39.089178],[45.014483000000034,39.086571],[45.01390500000004,39.083363],[45.01327700000004,39.079501],[45.01261800000004,39.075896],[45.01260300000004,39.073122],[45.012672000000045,39.070370999999994],[45.012466000000046,39.06634699999999],[45.012573000000046,39.064930999999994],[45.012793000000045,39.064350999999995],[45.01389400000004,39.063005],[45.014361000000044,39.062324],[45.01525200000005,39.065059999999995],[45.015472000000045,39.067054999999996],[45.01545700000005,39.068706999999996],[45.01579200000005,39.069179999999996],[45.015678000000044,39.070145],[45.01664400000004,39.070553],[45.01727200000004,39.070927999999995],[45.01767200000004,39.070682],[45.01792000000004,39.071256999999996],[45.01865500000004,39.072154999999995],[45.01910400000004,39.072903],[45.01978900000004,39.07380199999999],[45.02118100000004,39.073480999999994],[45.02156700000004,39.07303399999999],[45.02161100000004,39.073691999999994],[45.021731000000045,39.07419899999999],[45.02196100000005,39.074239999999996],[45.02219200000005,39.074408999999996],[45.02259100000005,39.075241],[45.02309500000005,39.076125],[45.02362300000005,39.077666],[45.02434100000005,39.080612],[45.02439800000005,39.081398],[45.02479100000005,39.081968],[45.02440400000005,39.082529],[45.024711000000046,39.086159],[45.02394500000005,39.087718],[45.02398600000005,39.089563000000005],[45.02475700000005,39.091365],[45.02619800000005,39.095041],[45.02634600000005,39.096185],[45.02679700000005,39.096986],[45.02818700000005,39.098845000000004],[45.02948800000005,39.099492000000005],[45.03042300000005,39.101169000000006],[45.03205000000005,39.102549],[45.032946000000045,39.104065000000006],[45.033976000000045,39.10443600000001],[45.035319000000044,39.104614000000005],[45.03622100000005,39.105436000000005],[45.03732000000005,39.10677200000001],[45.03753700000005,39.107637000000004],[45.03731100000005,39.110537],[45.037175000000055,39.113351],[45.037245000000055,39.114650000000005],[45.037674,39.115786]]]
    },
    {
        name:"РИП",
        description: 'Завод радиоизмерительных приборов, Прикубанский внутригородской округ',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.072473,38.986609],[45.072230000000005,39.000932],[45.07218400000001,39.00833],[45.07229000000001,39.015534],[45.07232300000001,39.015813],[45.07255400000001,39.015835],[45.07273600000001,39.021274000000005],[45.07212800000001,39.02124200000001],[45.07354200000001,39.02394600000001],[45.07352700000001,39.03437400000001],[45.07767800000001,39.034181000000004],[45.078088000000015,39.034546000000006],[45.088729000000015,39.03450300000001],[45.10605500000001,39.03388100000001],[45.12374000000001,39.03293600000001],[45.135770000000015,39.03216400000001],[45.13860500000001,39.02315200000001],[45.13957700000001,39.01903200000001],[45.13994100000001,39.01070600000001],[45.140237000000006,39.00818500000001],[45.14055200000001,39.00697800000001],[45.14159600000001,39.00499800000001],[45.142371000000004,39.004011000000006],[45.144193,39.003051000000006],[45.145104,39.00278600000001],[45.145955,39.00273500000001],[45.146623,39.00278800000001],[45.128246,38.99887200000001],[45.096900999999995,38.992274000000016],[45.085373,38.989699000000016],[45.072473,38.986609]]]
    },
    {
        name:"РМЗ",
        description: 'Микрорайон Ремонтно-механического завода, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        geometry:[[[45.028928,39.015554],[45.031568,39.021637],[45.034033,39.027474],[45.035189,39.02992],[45.035919,39.033997],[45.038224,39.056667],[45.035797,39.058566],[45.035569,39.058335],[45.035204,39.058169],[45.034474,39.057096],[45.033599,39.055379],[45.033127,39.05406],[45.03175,39.040895],[45.031012000000004,39.041088],[45.028776,39.041389],[45.023609,39.042011],[45.024545,39.039029],[45.026493,39.032226],[45.028133000000004,39.02057],[45.028897,39.015554],[45.028928,39.015554]]]
    },
    {
        name:"Рубероидный",
        description: 'Район рубероидного завода',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.0474883841493, 38.869674124267206],
        geometry:[[[45.056616,38.883306],[45.05692,38.867256],[45.053816999999995,38.852320999999996],[45.05266099999999,38.845884],[45.051444,38.847342999999995],[45.049619,38.85171999999999],[45.047246,38.85403699999999],[45.044812,38.85472399999999],[45.037023,38.85506699999999],[45.034406,38.85652599999999],[45.035075,38.857984999999985],[45.035075,38.86064599999999],[45.035379,38.86279199999999],[45.035987999999996,38.86416499999999],[45.035804999999996,38.86639699999999],[45.036778999999996,38.87334899999999],[45.037448,38.87545199999999],[45.038239,38.876696999999986],[45.039091,38.87768399999999],[45.040338,38.87935799999999],[45.042255,38.88055999999999],[45.044110999999994,38.881503999999985],[45.04502399999999,38.88176099999998],[45.04569299999999,38.881374999999984],[45.04663599999999,38.88167499999999],[45.049586999999995,38.886223999999984],[45.051137999999995,38.88957099999998],[45.05284199999999,38.89343299999998],[45.05454499999999,38.89532099999998],[45.05490999999999,38.89823899999998],[45.05460599999999,38.90107099999998],[45.05396699999999,38.90514799999998],[45.05335899999999,38.90763699999998],[45.05144299999999,38.90982599999998],[45.051503999999994,38.90998699999998],[45.051883999999994,38.91065199999998],[45.05292599999999,38.912346999999976],[45.053115999999996,38.91177799999998],[45.053968,38.90969699999998],[45.054972,38.90702599999998],[45.055322,38.90482699999998],[45.056203999999994,38.898367999999984],[45.056386999999994,38.895105999999984],[45.05663299999999,38.883315999999986],[45.056616,38.883306]]]
    },
    {
        name:"СМР",
        description: 'Славянский микрорайон, Прикубанский внутригородской округ',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.053829,38.939799],[45.054726,38.933995],[45.055076,38.933625000000006],[45.054969,38.933105000000005],[45.06295,38.919291],[45.068861,38.907785000000004],[45.077748,38.894556],[45.078311,38.894878],[45.078873,38.894341],[45.088846000000004,38.903524999999995],[45.081731000000005,38.913653],[45.080029,38.914854999999996],[45.08015,38.928416],[45.079177,38.930389999999996],[45.071682,38.92899499999999],[45.072896,38.931183999999995],[45.073258,38.932599999999994],[45.073243000000005,38.93854399999999],[45.059557000000005,38.93820099999999],[45.059481000000005,38.941697999999995],[45.053829,38.939799]]]
    },
    {
        name:"СХИ",
        description: 'Район сельскохозяйственного института',
        type: 'district',
        district: ['kuban', 'zapad'],
        geometry:[[[45.059366,38.918789],[45.058842999999996,38.918791],[45.05681499999999,38.916863],[45.052916999999994,38.912448],[45.051365999999994,38.909893999999994],[45.049783999999995,38.910548999999996],[45.048201999999996,38.911503999999994],[45.046498,38.911697],[45.045677,38.911654],[45.044855,38.911482],[45.044095,38.911225],[45.043334,38.91085],[45.042903,38.911547000000006],[45.042660000000005,38.912040000000005],[45.041898,38.913349000000004],[45.042219,38.917341],[45.043451000000005,38.919186],[45.04314600000001,38.920838],[45.041762000000006,38.923027000000005],[45.040006000000005,38.92339200000001],[45.03818700000001,38.91432600000001],[45.03678700000001,38.91908900000001],[45.03554000000001,38.92001200000001],[45.03593500000001,38.92105300000001],[45.03531900000001,38.92147200000001],[45.03470300000001,38.92180400000001],[45.03411700000001,38.92198700000001],[45.033775000000006,38.92144000000001],[45.032588000000004,38.92216800000001],[45.032071,38.92118100000001],[45.030801000000004,38.922103000000014],[45.031546000000006,38.92429300000001],[45.03075400000001,38.92525900000001],[45.03147700000001,38.92575200000001],[45.03181200000001,38.92626700000001],[45.03232900000001,38.92643900000001],[45.03312100000001,38.927383000000006],[45.034003000000006,38.92819800000001],[45.035585000000005,38.93111600000001],[45.037769000000004,38.92960300000001],[45.039426000000006,38.933949000000005],[45.04011500000001,38.93533300000001],[45.041030000000006,38.93587500000001],[45.041732,38.93597700000001],[45.053674,38.93971100000001],[45.054548000000004,38.93407700000001],[45.054452000000005,38.933777000000006],[45.054418000000005,38.93339100000001],[45.05464200000001,38.93311800000001],[45.05485200000001,38.93295100000001],[45.05782500000001,38.92781200000001],[45.05553600000001,38.92525900000001],[45.05752800000001,38.921954000000014],[45.05936800000001,38.91884300000002],[45.059366,38.918789]]]
    },
    {
        name:"Табачка",
        description: 'Район табачной фабрики',
        type: 'district',
        district: ['kuban', 'center'],
        geometry:[[[45.048469,38.988745],[45.048795999999996,38.9897],[45.04919099999999,38.99162],[45.050000999999995,38.994355999999996],[45.05009199999999,38.998915999999994],[45.04987099999999,38.999773999999995],[45.04987899999999,38.99997799999999],[45.05056299999999,39.00304599999999],[45.04928499999999,39.01225099999999],[45.04862599999999,39.01788799999999],[45.04864899999999,39.01803799999999],[45.05077799999999,39.01784499999999],[45.05081599999999,39.01951899999999],[45.051112999999994,39.02608499999999],[45.04689199999999,39.02655699999999],[45.044340999999996,39.026760999999986],[45.041281999999995,39.02713699999999],[45.04117599999999,39.02657399999999],[45.041129999999995,39.02610699999999],[45.041495,39.02525899999999],[45.041267,39.02467999999999],[45.038506,39.01917599999999],[45.038377,39.01885399999999],[45.03716,39.01862899999999],[45.039419,39.01664399999999],[45.040157,39.01536699999999],[45.039297,39.01316799999999],[45.038255,39.012339999999995],[45.037335,39.010451999999994],[45.036209,39.01136399999999],[45.036452,39.009282999999996],[45.035706,39.008843],[45.035851,39.008296],[45.036718,39.00821],[45.037342,39.008103],[45.038655000000006,39.007804],[45.039621000000004,39.007021],[45.040313000000005,39.006238],[45.04105800000001,39.005229],[45.04159000000001,39.003834],[45.042092000000004,39.001259],[45.042624,38.997932999999996],[45.04343,38.993447999999994],[45.043826,38.99246099999999],[45.044739,38.99106599999999],[45.045591,38.99014299999999],[45.046108000000004,38.98969199999999],[45.04669200000001,38.98928799999999],[45.04733100000001,38.98900899999999],[45.048426000000006,38.98875199999999],[45.048469,38.988745]]]
    },
    {
        name:"ТЭЦ",
        description: 'Микрорайон ТЭЦ, Карасунский внутригородской округ',
        type: 'district',
        district: 'karas',
        geometry:[[[45.024412,39.067165],[45.024054,39.064247],[45.023476,39.060556],[45.022981,39.057058],[45.022175000000004,39.050290999999994],[45.021174,39.050152],[45.020877,39.050753],[45.020481,39.051225],[45.019133999999994,39.052158000000006],[45.01691999999999,39.05342400000001],[45.01304499999999,39.055735000000006],[45.01283999999999,39.05582100000001],[45.01401999999999,39.06056300000001],[45.01492799999999,39.063843000000006],[45.01531599999999,39.06497],[45.015559999999994,39.066998000000005],[45.01552199999999,39.068693],[45.01581099999999,39.069101],[45.016442999999995,39.069337000000004],[45.01744899999999,39.069832000000005],[45.020408999999994,39.06906000000001],[45.02152099999999,39.068720000000006],[45.02438999999999,39.067164000000005],[45.024412,39.067165]]]
    },
    {
        name:"ФМР",
        description: 'Фестивальный микрорайон, Прикубанский внутригородской округ',
        type: 'district',
        district: ['kuban', 'zapad'],
        pmCoordinates: [45.0618997486525, 38.953500769774756],
        geometry:[[[45.043178,38.95642],[45.052772,38.959559],[45.051722,38.965995],[45.050498,38.965589],[45.049904,38.968174000000005],[45.049565,38.967991000000005],[45.049409000000004,38.96908500000001],[45.04972600000001,38.96950400000001],[45.049645000000005,38.96992300000001],[45.050738,38.97022400000001],[45.051357,38.96997700000001],[45.051817,38.971317000000006],[45.053737,38.971284000000004],[45.056498,38.971306000000006],[45.057824,38.970740000000006],[45.064381999999995,38.96743000000001],[45.06538199999999,38.96717100000001],[45.06522799999999,38.97118900000001],[45.06848599999999,38.97138900000001],[45.06859399999999,38.97245500000001],[45.06873299999999,38.97257700000001],[45.06896899999999,38.972699000000006],[45.06910599999999,38.972942],[45.06883299999999,38.973343],[45.06886499999999,38.974574],[45.06877399999999,38.979504],[45.06874399999999,38.985040999999995],[45.075493999999985,38.98694],[45.07581299999998,38.982915999999996],[45.075561999999984,38.982707],[45.07555799999999,38.977238],[45.076484999999984,38.974176],[45.071755999999986,38.972916],[45.07184499999999,38.968336],[45.07268899999999,38.963583],[45.07296599999999,38.946583],[45.07315999999999,38.938680999999995],[45.06199499999999,38.938433999999994],[45.05964199999999,38.93831599999999],[45.05955799999999,38.94188799999999],[45.05726099999999,38.94107399999999],[45.04992199999999,38.938703],[45.04617899999999,38.937571],[45.04319599999999,38.956373],[45.043178,38.95642]]]
    },
    {
        name:"ХБК",
        description: 'Район хлопчато-бумажного комбината',
        type: 'district',
        district: 'karas',
        geometry:[[[45.02361,39.042147],[45.031673999999995,39.041063],[45.032799999999995,39.051889],[45.033162,39.054609],[45.034284,39.056985],[45.03493,39.057972],[45.035676,39.058745],[45.035824000000005,39.059034000000004],[45.035425000000004,39.059646],[45.035341,39.061234],[45.035372,39.063122],[45.030335,39.063669],[45.02924,39.063838],[45.028144000000005,39.064178999999996],[45.024066000000005,39.064217],[45.02307700000001,39.0575],[45.02221000000001,39.050278999999996],[45.02121300000001,39.050092],[45.02264400000001,39.045232],[45.02361100000001,39.042147],[45.02361,39.042147]]]
    },
    {
        name:"Центр",
        description: 'Центральный округ',
        type: 'district',
        district: ['center', 'zapad'],
        geometry:[[[45.036576,38.954249],[45.047571,38.958088999999994],[45.052696,38.959661999999994],[45.051654,38.96586299999999],[45.050407,38.965433999999995],[45.049844,38.967988],[45.049509,38.967859],[45.049296,38.969103],[45.049631,38.969533],[45.049509,38.970005],[45.050099,38.970219],[45.050753,38.970369],[45.051301,38.970133],[45.053689000000006,38.977965999999995],[45.053811,38.981764],[45.053613000000006,38.982943999999996],[45.05458600000001,38.987547],[45.05066200000001,38.988298],[45.04772700000001,38.988727],[45.04676100000001,38.989129],[45.04574900000001,38.989778],[45.04444800000001,38.991215000000004],[45.04383200000001,38.992192],[45.043566000000006,38.992675000000006],[45.043330000000005,38.99342600000001],[45.042333000000006,38.99897300000001],[45.04195300000001,39.00149400000001],[45.04135300000001,39.004208000000006],[45.04082700000001,39.005356000000006],[45.04037100000001,39.00601],[45.03976300000001,39.006686],[45.03917700000001,39.007212],[45.038500000000006,39.007673000000004],[45.037549000000006,39.00794200000001],[45.03663600000001,39.00815600000001],[45.03589100000001,39.008145000000006],[45.034940000000006,39.00789900000001],[45.0309,39.00672900000001],[45.029656,39.006311000000004],[45.027142000000005,39.005559000000005],[45.025916,39.00480900000001],[45.024714,39.00372500000001],[45.023041000000006,39.00014200000001],[45.020998000000006,38.99529700000001],[45.017951000000004,38.98853400000001],[45.016763000000005,38.98604500000001],[45.016009000000004,38.98405900000001],[45.014754,38.98042200000001],[45.013262000000005,38.976346000000014],[45.012996,38.97700000000001],[45.012901,38.977767000000014],[45.012909,38.979371000000015],[45.012989,38.98034600000001],[45.011238,38.98002600000001],[45.011101,38.97983200000001],[45.01117,38.97840600000001],[45.010758,38.976957000000006],[45.009906,38.974897000000006],[45.008589,38.972795000000005],[45.007889,38.972011],[45.007045,38.971957],[45.006215,38.971764],[45.004813999999996,38.971507],[45.002683,38.970477],[45.000795,38.96979],[44.998968999999995,38.969876000000006],[44.99701999999999,38.970391000000006],[44.99476599999999,38.970133000000004],[44.99342599999999,38.969447],[44.992512999999995,38.967516],[44.992329999999995,38.964726000000006],[44.99281799999999,38.962495000000004],[44.994156999999994,38.960177],[44.99561899999999,38.958890000000004],[44.99714199999999,38.957774],[44.998237999999986,38.956744],[44.99860299999999,38.954684],[45.000185999999985,38.953482],[45.002195999999984,38.953053000000004],[45.00414499999999,38.952881000000005],[45.005422999999986,38.952538000000004],[45.01181599999999,38.949963000000004],[45.01571299999999,38.948762],[45.01942599999999,38.949362],[45.02253099999999,38.951422],[45.025573999999985,38.956916],[45.026912999999986,38.95846],[45.030138999999984,38.959061000000005],[45.03287799999998,38.95801000000001],[45.034703999999984,38.95605700000001],[45.036650999999985,38.95425500000001],[45.036576,38.954249]]]
    },
    {
        name:"ЧМР",
        description: 'Микрорайон Черемушки, Карасунский внутригородской округ',
        type: 'district',
        district: ['karas', 'center'],
        geometry:[[[45.013273,38.976685],[45.013041,38.977374000000005],[45.012977,38.978106000000004],[45.013091,38.980493],[45.011066,38.980086],[45.010941,38.981282],[45.011104,38.982521],[45.011287,38.98441],[45.010678000000006,38.986469],[45.008608,38.98913],[45.007634,38.991555000000005],[45.00717,38.99318600000001],[45.00712,38.99586300000001],[45.007337,38.997488000000004],[45.007626,38.99911900000001],[45.008121,39.00071700000001],[45.007626,39.00124300000001],[45.006842,39.00123200000001],[45.006294,39.00100700000001],[45.005624999999995,39.00003100000001],[45.00527399999999,38.99935500000001],[45.00539599999999,38.99601800000001],[45.00544799999999,38.99542300000001],[45.005726999999986,38.99478400000001],[45.00627199999999,38.99382900000001],[45.00644699999999,38.992488000000016],[45.006354999999985,38.99239200000002],[45.00615399999999,38.992381000000016],[45.00557099999999,38.99348600000002],[45.004969999999986,38.99511700000002],[45.004550999999985,38.99650100000002],[45.004345999999984,38.99814200000002],[45.00422399999999,39.00007900000002],[45.00428499999999,39.00188700000002],[45.00477199999999,39.00475100000002],[45.004893999999986,39.007927000000024],[45.004604999999984,39.008914000000026],[45.00355399999999,39.010760000000026],[45.00209299999999,39.01264800000003],[45.00067699999999,39.013839000000026],[44.99904799999999,39.01479400000002],[44.99747199999999,39.01556600000002],[44.99588099999999,39.016682000000024],[44.99502099999999,39.01706800000002],[44.994053999999984,39.01728300000002],[44.992980999999986,39.017701000000024],[44.99198399999999,39.01831300000002],[44.99131399999999,39.01857000000002],[44.992226999999986,39.03402000000002],[44.992744999999985,39.03353700000002],[44.993262999999985,39.03324700000002],[44.99497499999998,39.033183000000015],[44.99660499999998,39.033397000000015],[44.99880499999998,39.03487800000001],[44.99993499999999,39.036214000000015],[45.00111099999999,39.037013000000016],[45.00159999999999,39.037233000000015],[45.00236299999999,39.03719500000001],[45.003500999999986,39.03824700000001],[45.00544199999999,39.03932000000001],[45.006720999999985,39.04045700000001],[45.00730999999998,39.04183000000001],[45.00802199999998,39.04273100000001],[45.00977299999998,39.04558500000001],[45.01020599999998,39.04698000000001],[45.01058299999998,39.04753300000001],[45.01137799999998,39.05148600000001],[45.012062999999976,39.05376100000001],[45.01200799999997,39.05427300000001],[45.01225399999997,39.055853000000006],[45.01296099999997,39.055665000000005],[45.016248999999966,39.053670000000004],[45.02011799999997,39.051427000000004],[45.02054599999997,39.051065],[45.02089799999997,39.050596],[45.02146099999997,39.049008],[45.02276399999997,39.044496],[45.024793999999964,39.037925],[45.02609499999996,39.033472],[45.026467999999966,39.031901000000005],[45.02881899999996,39.01539400000001],[45.028086999999964,39.01306900000001],[45.023878999999965,39.002604000000005],[45.02085299999997,38.99528300000001],[45.01646199999997,38.985665000000004],[45.013273,38.976685]]]
    },
    {
        name:"ШМР",
        description: 'Школьный микрорайон, Прикубанский внутригородской округ',
        type: 'district',
        district: ['kuban', 'center'],
        geometry:[[[45.035666,39.008852],[45.035788,39.008316],[45.033581,39.007715],[45.029078,39.006299],[45.027654999999996,39.005902],[45.02702299999999,39.005655],[45.02566899999999,39.004839999999994],[45.025333999999994,39.004495999999996],[45.02482499999999,39.004056999999996],[45.02440599999999,39.003445],[45.02417799999999,39.002973],[45.025409999999994,39.00617],[45.030553999999995,39.018829999999994],[45.034372999999995,39.028057],[45.035605,39.030288999999996],[45.039058999999995,39.035641999999996],[45.04255799999999,39.03504099999999],[45.04242099999999,39.027069999999995],[45.04123399999999,39.02721999999999],[45.04105899999999,39.02607199999999],[45.04144699999999,39.02519799999999],[45.03832799999999,39.01893699999999],[45.037057999999995,39.01866899999999],[45.039401,39.01657699999999],[45.040085,39.01535399999999],[45.039255999999995,39.013229999999986],[45.038236999999995,39.01239299999999],[45.037324,39.01052599999999],[45.036122,39.011512999999994],[45.036395999999996,39.009280999999994],[45.035658,39.00887399999999],[45.035666,39.008852]]]
    },
    {
        name:"ЖМР",
        description: 'Микрорайон им. Г.К. Жукова , Прикубанский внутригородской округ',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.103538,38.982456],[45.099929,38.980406],[45.097785,38.979246],[45.099723000000004,38.96338900000001],[45.101949000000005,38.94408800000001],[45.106485000000006,38.946416000000006],[45.106364000000006,38.95551400000001],[45.10514800000001,38.963668000000006],[45.103218000000005,38.973625000000006],[45.10372,38.98237900000001],[45.103538,38.982456]]]
    },
    {
        name:"ЮМР",
        description: 'Юбилейный микрорайон, Западный внутригородской округ',
        type: 'district',
        district: 'zapad',
        geometry:[[[45.042131,38.917455],[45.043302999999995,38.919230999999996],[45.042953999999995,38.920835],[45.041647999999995,38.922841999999996],[45.040068999999995,38.923249999999996],[45.03820399999999,38.913951],[45.03665699999999,38.918903],[45.03530799999999,38.919957000000004],[45.03578699999999,38.921001000000004],[45.034493999999995,38.921714],[45.034144999999995,38.921716],[45.03379699999999,38.921117],[45.03261299999999,38.921894],[45.03213099999999,38.920872],[45.030559999999994,38.921919],[45.03131199999999,38.924355000000006],[45.03080799999999,38.924936],[45.03056099999999,38.924812],[45.029031999999994,38.920872],[45.025738999999994,38.910752],[45.023056999999994,38.903718000000005],[45.02311399999999,38.900718000000005],[45.023531999999996,38.90007800000001],[45.024558999999996,38.899868000000005],[45.027342,38.900305],[45.029517,38.900828000000004],[45.031631,38.901952],[45.034003,38.903727],[45.036190999999995,38.905588],[45.039666999999994,38.908407000000004],[45.04312699999999,38.910861000000004],[45.04172499999999,38.913388000000005],[45.04208599999999,38.917411],[45.042131,38.917455]]]
    },
    {
        name:"п. Белозерный",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.088974,38.679249],[45.08715,38.680451],[45.08338,38.686116],[45.082651,38.690751],[45.076691,38.685429],[45.070974,38.672211],[45.07511,38.662254999999995],[45.089035,38.67920599999999],[45.088974,38.679249]]]
    },
    {
        name:"п. Знаменский",
        description: '',
        type: 'district',
        district: 'karas',
        pmCoordinates: [45.051431641921646, 39.145559968261104],
        geometry:[[[45.045188,39.136119],[45.055683,39.136054],[45.055638,39.131784],[45.060383,39.133522000000006],[45.064154,39.13609700000001],[45.065675,39.13635500000001],[45.065614,39.13944500000001],[45.065735999999994,39.140904000000006],[45.06409299999999,39.141161000000004],[45.059104999999995,39.141054000000004],[45.05911999999999,39.149487],[45.054770999999995,39.149616],[45.05527299999999,39.156525],[45.054207999999996,39.157094],[45.04787999999999,39.157276],[45.046586999999995,39.149744000000005],[45.045354999999994,39.141676000000004],[45.045157999999994,39.136140000000005],[45.045188,39.136119]]]
    },
    {
        name:"п. Индустриальный",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.101889,39.098272],[45.100764,39.114709000000005],[45.093271,39.11397900000001],[45.090171,39.11552400000001],[45.093256,39.12065200000001],[45.092025,39.122305000000004],[45.085428,39.122648000000005],[45.09049,39.113722],[45.086872,39.1081],[45.090034,39.098959],[45.092192000000004,39.098487],[45.09225300000001,39.093423],[45.09672200000001,39.095912],[45.09687400000001,39.093036],[45.10052100000001,39.092693],[45.100643000000005,39.094581],[45.101889,39.098272]]]
    },
    {
        name:"п. Колосистый",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.136218,38.898961],[45.140471,38.906686],[45.135853999999995,38.913209],[45.126011999999996,38.90703],[45.128806,38.899305],[45.1299,38.891752],[45.135489,38.888318],[45.138405,38.884198999999995],[45.140349,38.888662],[45.136583,38.896043],[45.135839000000004,38.89864],[45.136218,38.898961]]]
    },
    {
        name:"п. Новознаменский",
        description: '',
        type: 'district',
        district: 'karas',
        pmCoordinates: [45.0522397324389, 39.12055112963854],
        geometry:[[[45.057534,39.103396],[45.059115,39.125024999999994],[45.066172,39.12493899999999],[45.066232,39.13523899999999],[45.065685,39.135324999999995],[45.060636,39.13300699999999],[45.054492,39.130517999999995],[45.049564000000004,39.12983199999999],[45.045244000000004,39.12905899999999],[45.045122000000006,39.11957499999999],[45.04481800000001,39.11069099999999],[45.046035,39.10820199999999],[45.057519000000006,39.10337399999999],[45.057534,39.103396]]]
    },
    {
        name:"п. Плодородный",
        description: '',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.057881345297744, 39.05115066137674],
        geometry:[[[45.060187,39.048147],[45.060248,39.052438],[45.059487000000004,39.052266],[45.059487000000004,39.053726000000005],[45.05811800000001,39.05364],[45.056506000000006,39.053811],[45.05322100000001,39.05364],[45.053008000000005,39.051751],[45.054255000000005,39.051666000000004],[45.05608,39.051623000000006],[45.056202,39.048232000000006],[45.060187,39.048147]]]
    },
    {
        name:"п. Северный",
        description: '',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.126584812193535, 38.982248521239455],
        geometry:[[[45.141622,38.999436],[45.141622,38.984588],[45.129335,38.984523],[45.130959999999995,38.971069],[45.12938,38.970382],[45.132113999999994,38.946607],[45.127556999999996,38.944805],[45.12290899999999,38.986475000000006],[45.12378999999999,38.987613],[45.12439799999999,38.989222000000005],[45.125156999999994,38.99244100000001],[45.12646399999999,38.99553100000001],[45.12688899999999,38.99634600000001],[45.12893999999999,38.99758700000001],[45.14168299999999,38.99942900000001],[45.141622,38.999436]]]
    },{
        name:"п. Южный",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.150229,39.003084],[45.150290000000005,39.026602000000004],[45.153266,39.026774],[45.154602000000004,39.026602000000004],[45.155574,39.025744],[45.1594,39.025401],[45.157334999999996,39.007892000000005],[45.156181,39.00403000000001],[45.153084,39.003429000000004],[45.150108,39.002914000000004],[45.150229,39.003084]]]
    },
    {
        name:"п. Яблоновский",
        description: '',
        type: 'district',
        district: 'zapad',
        geometry:[[[44.991008,38.964879],[44.986318000000004,38.963034],[44.986471,38.961918],[44.977912,38.957884],[44.979252,38.951189],[44.977547,38.950331],[44.9784,38.946554],[44.96329,38.93368],[44.973526,38.914969],[44.975841,38.913939],[44.978156000000006,38.913939],[44.988755000000005,38.92235],[44.989790000000006,38.925354],[44.99356600000001,38.928959],[45.00014300000001,38.931191],[45.00270100000001,38.930676],[45.00489300000001,38.930161],[45.00696300000001,38.935139],[45.007694000000015,38.940804],[45.00842500000002,38.94458],[45.011834000000015,38.944065],[45.011713000000015,38.947842],[45.00805900000002,38.950074],[45.005746000000016,38.950589],[45.00026500000001,38.951619],[44.996977000000015,38.954193000000004],[44.991008,38.964879]]]
    },
    {
        name:"ст. Елизаветинская",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.052246,38.82717],[45.0475,38.828630000000004],[45.034173,38.823136000000005],[45.032165000000006,38.82245],[45.032652000000006,38.811292],[45.04123200000001,38.794984],[45.041050000000006,38.787173],[45.03928500000001,38.780049000000005],[45.04348400000001,38.769149000000006],[45.046648000000005,38.77206700000001],[45.048473,38.77112300000001],[45.053706,38.77163800000001],[45.05693,38.76811900000001],[45.059303,38.76983600000001],[45.05474,38.80845900000001],[45.058268000000005,38.81172100000001],[45.058451000000005,38.815669000000014],[45.053706000000005,38.81429600000001],[45.052124000000006,38.827342000000016],[45.052246,38.82717]]]
    },
    {
        name:"х. им.Ленина",
        description: '',
        type: 'district',
        district: 'karas',
        geometry:[[[45.02958,39.225756],[45.021606000000006,39.230563],[45.020998000000006,39.227129999999995],[45.01953700000001,39.224211],[45.016493000000004,39.219576999999994],[45.013510000000004,39.215456999999994],[45.01570100000001,39.21365399999999],[45.021119000000006,39.20876199999999],[45.02398000000001,39.208332999999996],[45.02958,39.225756]]]
    },
    {
        name:"х. Копанской",
        description: '',
        type: 'district',
        district: 'kuban',
        geometry:[[[45.179544,38.805112],[45.176023,38.816442],[45.174323,38.816785],[45.165944,38.808546],[45.167766,38.799276],[45.169952,38.795328],[45.179544,38.805112]]]
    },
    {
        name:"Катюша",
        description: '',
        type: 'district',
        district: 'kuban',
        pmCoordinates: [45.12220729593242, 39.011798082030985],
        geometry:[[[45.126824,39.011283],[45.12567,39.017033999999995],[45.11929,39.01746299999999],[45.119412,39.01248499999999],[45.120141,39.01128299999999],[45.121295999999994,39.00063999999999],[45.124942,39.00759199999999],[45.126825999999994,39.01136899999999],[45.126824,39.011283]]]
    },
    {
        name:"Западный",
        description: 'Западный внутригородской округ',
        link: '/podrazdeleniya/administratsii-vnutrigorodskikh-okrugov/zvo/',
        type: 'region',
        district: 'zapad',
        pmCoordinates: [45.04016250656474, 38.96162707421864],
        geometry:[[[45.012823,38.981801],[45.010616,38.981522],[45.010481999999996,38.980235],[45.010451999999994,38.978089],[45.009356,38.9756],[45.007377,38.972853],[45.004089,38.971952],[45.001105,38.971008000000005],[44.995137,38.971652000000006],[44.991879,38.969807],[44.991147999999995,38.966460000000005],[44.992031,38.96259800000001],[44.995684999999995,38.95667600000001],[44.997097,38.95559700000001],[44.998284999999996,38.95323700000001],[45.000873,38.95199200000001],[45.00407,38.95199200000001],[45.006506,38.95134800000001],[45.009581000000004,38.95010300000001],[45.013417000000004,38.94877300000001],[45.016163000000006,38.94798300000001],[45.019572000000004,38.948541000000006],[45.02212900000001,38.950301],[45.02380300000001,38.952576],[45.02541600000001,38.955451000000004],[45.02642000000001,38.957168],[45.02803300000001,38.957726],[45.029776000000005,38.958048],[45.031632,38.957833],[45.033427,38.956374000000004],[45.035009,38.954486],[45.035952,38.951868000000005],[45.036226,38.948091000000005],[45.036409,38.94319900000001],[45.036105,38.93985200000001],[45.035131,38.93547500000001],[45.03367,38.93054000000001],[45.031814000000004,38.92826500000001],[45.029939000000006,38.92544900000001],[45.02817400000001,38.92210200000001],[45.025861000000006,38.91566500000001],[45.021783000000006,38.90382000000001],[45.022361000000004,38.90043000000001],[45.023213000000005,38.899572000000006],[45.02488700000001,38.899057000000006],[45.02771700000001,38.89935700000001],[45.03155100000001,38.900602000000006],[45.03547600000001,38.903563000000005],[45.03769000000001,38.904540000000004],[45.03732500000001,38.907458000000005],[45.03775100000001,38.908188],[45.03793400000001,38.908875],[45.038132000000004,38.911707],[45.03832200000001,38.913632],[45.03886200000001,38.917038],[45.03980500000001,38.922332999999995],[45.04138000000001,38.929531999999995],[45.04160800000001,38.929531999999995],[45.04207200000001,38.92945699999999],[45.04359300000001,38.92999299999999],[45.04794000000001,38.931423999999986],[45.05097500000001,38.93237399999999],[45.05441900000001,38.93334699999999],[45.05470800000001,38.93304699999999],[45.054929000000016,38.933089999999986],[45.053289000000014,38.942832999999986],[45.050860000000014,38.958542999999985],[45.05108100000002,38.95877499999998],[45.05111900000002,38.959085999999985],[45.05650300000002,38.96083499999998],[45.05861700000002,38.96086699999998],[45.06335400000002,38.961070999999976],[45.063704000000016,38.96119999999998],[45.064563000000014,38.961757999999975],[45.069561000000014,38.96770299999997],[45.069941000000014,38.968281999999974],[45.07036700000001,38.96950499999998],[45.070580000000014,38.969761999999974],[45.071491000000016,38.96977799999998],[45.07162000000002,38.97002499999998],[45.07165000000002,38.97032499999998],[45.07155900000002,38.97752399999998],[45.07158100000002,38.98197599999998],[45.07147500000002,38.98213699999998],[45.070061000000024,38.98187999999998],[45.068974000000026,38.98181599999998],[45.06761700000003,38.98248699999998],[45.06636600000002,38.983264999999975],[45.060892000000024,38.982245999999975],[45.06085400000002,38.98352299999998],[45.06109700000002,38.98479999999998],[45.06143200000002,38.985647999999976],[45.05960700000002,38.98574499999997],[45.05908200000002,38.983984999999976],[45.05430600000002,38.981860999999974],[45.049295000000015,38.97956799999997],[45.03924700000002,38.97591299999997],[45.03564900000002,38.97437899999997],[45.02594100000002,38.970870999999974],[45.02160400000002,38.969389999999976],[45.01787500000002,38.968069999999976],[45.01825500000002,38.965902999999976],[45.01520300000002,38.96494799999998],[45.01401600000002,38.97203999999998],[45.01322100000002,38.976380999999975],[45.013023000000025,38.97782899999998],[45.01292400000003,38.97915899999998],[45.01306100000003,38.98012499999998],[45.012947000000025,38.98042499999998],[45.01282500000003,38.981819999999985],[45.012823,38.981801]]]
    },
    {
        name:"Карасунский",
        description: 'Карасунский внутригородской округ',
        link: '/podrazdeleniya/administratsii-vnutrigorodskikh-okrugov/kvo/',
        type: 'region',
        district: 'karas',
        pmCoordinates: [45.05146501341858, 39.22898477539046],
        geometry:[[[45.101998,39.184872],[45.101998,39.261089999999996],[45.125335,39.263149999999996],[45.127279,39.312588],[45.135542,39.311215],[45.134084,39.37576],[45.118529,39.375073],[45.106374,39.35928],[45.078164,39.381938999999996],[45.065514,39.375758999999995],[45.059674,39.377818999999995],[45.041665,39.34074],[45.046533000000004,39.327693999999994],[45.031441,39.32220099999999],[45.030467,39.30022799999999],[45.014397,39.28786799999999],[45.015858,39.26452199999999],[45.004168,39.25834199999999],[44.99345,39.22332299999999],[45.002707,39.199977],[44.992963,39.184183999999995],[44.992476,39.16289799999999],[44.996374,39.14710499999999],[44.996374,39.12238599999999],[44.990771,39.11483299999999],[44.988091000000004,39.09818099999999],[44.98736,39.08513499999999],[44.988822,39.076894999999986],[44.980537999999996,39.066594999999985],[44.975663999999995,39.067281999999985],[44.974202,39.062131999999984],[44.978100999999995,39.05492199999998],[44.983948999999996,39.04736899999998],[44.984193,39.04084599999998],[44.990528,39.03500999999998],[44.998323,39.03638299999998],[45.004169,39.03844299999998],[45.007701,39.04406499999998],[45.011719,39.05440799999998],[45.012389,39.05406499999998],[45.012724,39.05247699999998],[45.015677,39.04539599999998],[45.02128999999999,39.018779999999985],[45.02128999999999,39.016955999999986],[45.020635999999996,39.010046999999986],[45.023002,39.00959799999998],[45.025909,39.00736599999998],[45.029997,39.017409999999984],[45.033197,39.025407999999985],[45.034417000000005,39.028289999999984],[45.036060000000006,39.030982999999985],[45.04255500000001,39.04133599999999],[45.06772300000001,39.08105699999999],[45.06796100000001,39.15618499999999],[45.08425300000001,39.18143899999998],[45.10261300000001,39.18135299999998],[45.10200500000001,39.18495799999998],[45.101998,39.184872]]]
    },
    {
        name:"Прикубанский",
        description: 'Прикубанский внутригородской округ',
        link: '/podrazdeleniya/administratsii-vnutrigorodskikh-okrugov/pvo/',
        type: 'region',
        district: 'kuban',
        pmCoordinates: [45.099378980204975, 38.93635838085928],
        geometry:[[[45.151145,39.195059],[45.133772,39.180296],[45.132314,39.180811],[45.131585,39.179609],[45.130127,39.179952],[45.095092,39.180717],[45.084270000000004,39.18106],[45.06815400000001,39.156212000000004],[45.06775900000001,39.081024000000006],[45.06060800000001,39.069900000000004],[45.04151200000001,39.039611],[45.035761000000015,39.030406],[45.034376000000016,39.028131],[45.033037000000014,39.024891000000004],[45.03353900000001,39.02394700000001],[45.03387400000001,39.02289600000001],[45.03428500000001,39.02096500000001],[45.03458900000001,39.01905500000001],[45.03501500000001,39.01671600000001],[45.03625400000001,39.01114300000001],[45.03812500000001,39.00964100000001],[45.04031600000001,39.00768800000001],[45.04021000000001,39.00691600000001],[45.04027100000001,39.00674400000001],[45.04062900000001,39.00621800000001],[45.04101700000001,39.00575700000001],[45.04268300000001,39.00424400000001],[45.043025000000014,39.006443000000004],[45.04350400000001,39.00745200000001],[45.043892000000014,39.00795600000001],[45.04524600000001,39.00886800000001],[45.045915000000015,39.009265000000006],[45.04781600000001,39.00898600000001],[45.04851600000001,39.009158000000006],[45.04863800000001,39.00832100000001],[45.04891200000001,39.00742000000001],[45.049977000000005,39.00772000000001],[45.050555,39.00319200000001],[45.050874,39.002913000000014],[45.051376,39.00293400000001],[45.055239,39.00264400000001],[45.056015,39.00250500000001],[45.062275,39.001773000000014],[45.062106,38.99881000000001],[45.065543,38.99833800000001],[45.0653,38.99426100000001],[45.063004,38.99415400000001],[45.062715,38.98855400000001],[45.061544,38.98872600000001],[45.061468,38.98561500000001],[45.061118,38.98473500000001],[45.060875,38.98353300000001],[45.060936000000005,38.98228800000001],[45.066371000000004,38.98332000000001],[45.067717,38.98251000000001],[45.069032,38.98187200000001],[45.07005,38.98191100000001],[45.071478,38.98216500000001],[45.071592,38.98199300000001],[45.071576,38.97549800000001],[45.071675,38.97027200000001],[45.071645,38.969972000000006],[45.07147,38.969736000000005],[45.070588,38.96971500000001],[45.070504,38.969640000000005],[45.070389999999996,38.969468000000006],[45.070100999999994,38.96857800000001],[45.069956999999995,38.96823500000001],[45.069576999999995,38.967656000000005],[45.065023,38.962206],[45.06457399999999,38.961723],[45.06406499999999,38.961358],[45.063362999999995,38.961006],[45.056549,38.960704],[45.051171999999994,38.95907],[45.05114199999999,38.958704999999995],[45.050943999999994,38.95851199999999],[45.054959,38.93308499999999],[45.054703999999994,38.93299899999999],[45.05441499999999,38.93323499999999],[45.05284799999999,38.93286999999999],[45.04801099999999,38.931367999999985],[45.042070999999986,38.929414999999985],[45.04141699999999,38.929478999999986],[45.04003099999999,38.92327299999999],[45.03902699999999,38.917822999999984],[45.03849399999999,38.91471999999998],[45.03834199999999,38.91356099999998],[45.038067999999996,38.91068599999998],[45.03796199999999,38.908797999999976],[45.03777899999999,38.90808999999997],[45.03738299999999,38.907553999999976],[45.03777899999999,38.904570999999976],[45.039877999999995,38.90695299999997],[45.041380999999994,38.90817799999997],[45.042916999999996,38.90903599999997],[45.044650999999995,38.909636999999975],[45.04586799999999,38.90989399999997],[45.04839299999999,38.909507999999974],[45.04962499999999,38.90914299999997],[45.05093299999999,38.90862799999997],[45.05187599999999,38.90791999999997],[45.05243899999999,38.90716899999997],[45.05292599999999,38.90590299999997],[45.05348899999999,38.90369299999997],[45.05402099999999,38.89976599999997],[45.05376199999999,38.89725499999997],[45.051556999999995,38.89279199999997],[45.049777,38.88933699999997],[45.04818,38.886976999999966],[45.046963000000005,38.88564699999996],[45.04187400000001,38.88200799999996],[45.038481000000004,38.878016999999964],[45.035940000000004,38.871793999999966],[45.034766000000005,38.86315299999997],[45.032009,38.857257999999966],[45.026674,38.85345199999997],[45.021146,38.85403799999997],[45.01409,38.864372999999965],[45.011049,38.86722299999997],[45.009285,38.86701699999997],[45.004323,38.86528299999997],[45.002024999999996,38.86287099999997],[45.00131,38.86080299999997],[45.002584,38.855380999999966],[45.006569999999996,38.851038999999965],[45.007999,38.84549599999996],[45.007236,38.84132599999996],[45.005376999999996,38.83355099999996],[45.005841,38.83038399999996],[45.013135999999996,38.81953099999996],[45.012271999999996,38.81657399999996],[45.00847399999999,38.81392199999996],[45.00042499999999,38.813368999999966],[44.997246999999994,38.810411999999964],[44.99749799999999,38.80170899999997],[45.002863999999995,38.78613999999997],[45.013099999999994,38.77889599999997],[45.015539,38.77501699999997],[45.023090999999994,38.77388399999997],[45.02746299999999,38.77030299999997],[45.03194499999999,38.75781999999997],[45.03603999999999,38.75585699999996],[45.04073399999999,38.75779399999996],[45.044453999999995,38.76161899999996],[45.05067699999999,38.768582999999964],[45.054807,38.77073399999996],[45.059301999999995,38.764816999999965],[45.060998999999995,38.75726999999996],[45.059166999999995,38.74972299999996],[45.055977,38.74545399999996],[45.049419,38.73716099999996],[45.040791,38.728867999999956],[45.039951,38.72443699999996],[45.041059000000004,38.71966299999996],[45.05127,38.71627399999996],[45.056844000000005,38.71704899999996],[45.06147500000001,38.71200899999996],[45.06440200000001,38.704737999999956],[45.066607000000005,38.70195399999996],[45.07455900000001,38.712102999999956],[45.07764500000001,38.70748999999996],[45.08390800000001,38.71538599999996],[45.08738700000001,38.71126299999996],[45.10241700000001,38.70679699999996],[45.10511800000001,38.71142599999996],[45.10164600000001,38.72454699999996],[45.08581200000001,38.74408899999996],[45.14516000000001,38.80439999999996],[45.17534700000001,38.75198799999996],[45.18131000000001,38.76251999999996],[45.18837900000001,38.75405799999996],[45.219017000000015,38.80099199999996],[45.18660600000001,38.84816799999996],[45.18100400000001,38.84432899999996],[45.17514100000001,38.86136999999996],[45.18089800000001,38.86523899999996],[45.181243000000016,38.87297699999996],[45.171014000000014,38.87937099999996],[45.17098400000001,38.89752399999996],[45.171039000000015,38.92168199999996],[45.17097600000002,38.94311399999996],[45.17103700000002,38.96572999999996],[45.17094600000002,38.99924699999996],[45.14763200000002,38.99928699999996],[45.14762400000002,39.00292399999996],[45.14574900000002,39.00266699999996],[45.14496700000002,39.00278499999996],[45.144026000000025,39.00306399999996],[45.14329000000002,39.00339699999996],[45.14270500000002,39.00376199999996],[45.14232500000002,39.004083999999956],[45.14167200000002,39.004855999999954],[45.14119400000002,39.005595999999954],[45.14084500000002,39.00624999999995],[45.14040500000002,39.007236999999954],[45.140109000000024,39.00877099999995],[45.139577000000024,39.017063999999955],[45.139287000000024,39.02006799999995],[45.13912800000003,39.020829999999954],[45.14383600000003,39.02046499999995],[45.14383600000003,39.02173099999995],[45.14410900000003,39.02215999999995],[45.14409100000003,39.02956799999995],[45.149952000000035,39.029267999999945],[45.15120200000003,39.02952499999994],[45.15703200000004,39.02918199999994],[45.159309000000036,39.028409999999944],[45.15952200000004,39.02536299999994],[45.15921800000004,39.02257399999994],[45.16406600000004,39.02508999999994],[45.16778500000004,39.02747199999994],[45.168366000000034,39.02836499999994],[45.16842700000004,39.02450299999994],[45.16826000000004,39.01606999999994],[45.16786900000004,39.00517999999994],[45.183695000000036,39.00682599999994],[45.204572000000034,39.00991599999994],[45.20517900000004,39.06433299999994],[45.19328500000004,39.10982299999994],[45.18758000000004,39.10570299999994],[45.18660900000004,39.10656099999994],[45.17629000000004,39.09900799999994],[45.17459000000004,39.10038099999994],[45.172404000000036,39.09900799999994],[45.15904600000004,39.16252299999994],[45.15115100000004,39.19548199999994],[45.151145,39.195059]]]
    },
    {
        name:"Центральный",
        description: 'Центральный внутригородской округ',
        link: '/podrazdeleniya/administratsii-vnutrigorodskikh-okrugov/cvo/',
        type: 'region',
        district: 'center',
        pmCoordinates: [45.014760868868855, 38.99855367187472],
        geometry:[[[45.011927,39.053576],[45.010527,39.049456],[45.008396000000005,39.044306],[45.006037000000006,39.040723],[45.004347,39.037998],[44.998830000000005,39.036087],[44.990654000000006,39.034538000000005],[44.984319000000006,39.040203000000005],[44.983137000000006,39.038572],[44.982558000000004,39.036898],[44.980365000000006,39.033894000000004],[44.97672500000001,39.031834],[44.97313100000001,39.030461],[44.970816000000006,39.029345],[44.96892700000001,39.028143],[44.96789100000001,39.02471],[44.96856100000001,39.020418],[44.97105900000001,39.018358],[44.97422700000001,39.018358],[44.979466000000016,39.019474],[44.982329000000014,39.019903],[44.986471000000016,39.019131],[44.99250100000002,39.016299000000004],[44.99963600000002,39.013279000000004],[45.00249800000002,39.011133],[45.003838000000016,39.007099000000004],[45.00341200000002,39.00203500000001],[45.00341200000002,38.997400000000006],[45.00444700000002,38.994310000000006],[45.00523900000002,38.99182100000001],[45.00724800000002,38.98847400000001],[45.00974400000002,38.98529800000001],[45.010657000000016,38.98272300000001],[45.010658000000014,38.98165500000001],[45.01286500000001,38.98195500000001],[45.01298700000001,38.98038900000002],[45.01309400000001,38.98013200000002],[45.01297200000001,38.97916600000002],[45.01323100000001,38.97650500000002],[45.01411400000001,38.971677000000014],[45.015233000000016,38.96500800000001],[45.01820100000002,38.96597400000001],[45.01783500000002,38.96808600000001],[45.02121400000002,38.96930900000001],[45.02447100000002,38.97038200000001],[45.03575100000002,38.97445700000001],[45.03928100000002,38.97595900000001],[45.049002000000016,38.97947300000001],[45.05839000000002,38.983809000000015],[45.059044000000014,38.98402400000001],[45.059576000000014,38.98578400000001],[45.061416000000015,38.985698000000006],[45.06151400000002,38.98875600000001],[45.062700000000014,38.98859500000001],[45.06298000000001,38.99416500000001],[45.06529100000001,38.99429700000001],[45.06552700000001,38.99831000000001],[45.06206800000001,38.99876600000001],[45.06222800000001,39.00173800000001],[45.05517900000001,39.00252100000001],[45.05090500000001,39.00282100000001],[45.050571000000005,39.00292000000001],[45.049902,39.00759800000001],[45.048944000000006,39.00727600000001],[45.048609000000006,39.00808100000001],[45.04848700000001,39.00897100000001],[45.04787900000001,39.00884200000001],[45.04593200000001,39.00914200000001],[45.04412200000001,39.00798300000001],[45.04384800000001,39.00770400000001],[45.04355900000001,39.00731800000001],[45.04334600000001,39.00680300000001],[45.04314800000001,39.00646000000001],[45.04302600000001,39.00596600000001],[45.042980000000014,39.005151000000005],[45.042752000000014,39.004035],[45.04156200000001,39.005049],[45.040497000000016,39.006208],[45.040132000000014,39.00683],[45.040269000000016,39.007645000000004],[45.036161000000014,39.011100000000006],[45.03459400000001,39.018567000000004],[45.03436600000001,39.020197],[45.03395500000001,39.022321000000005],[45.03349900000001,39.023909],[45.03302700000001,39.024746],[45.02593900000001,39.007218],[45.02306300000001,39.00945],[45.02055200000001,39.010008],[45.02122200000001,39.016985999999996],[45.02122200000001,39.01885299999999],[45.01554900000001,39.04546899999999],[45.01389000000001,39.04937399999999],[45.01259600000001,39.052527999999995],[45.01232200000001,39.053965],[45.011972000000014,39.054072],[45.011927,39.053576]]]
    }
];

/**
 * Интерактивность сайта
 */
angular.module('interactive', ['ngSanitize'])
    /**
     * Контроллер формы OpenKrasnodarConferenceController
     */
    .controller('okConferenceFormCtrl', ['$scope', '$element', function($scope, $element) {
        $('#ok-conference-form-success').hide();

        $scope.onSuccess = function(response) {
            $('#ok-conference-form-success').slideDown();
            $element.slideUp();

            if (!!$.scrollTo) {
                $.scrollTo($('#ok-conference-form-success'), 200, {offset:{top:-150}});
            }
        };
    }])

    /**
     * Кнопка прокрутки вверх
     */
    .directive('scrollUpBtn', [function() {
        return {
            restrict: 'A',
            scope: true,

            link: function($scope, $element) {
                $scope.visible = false;

                $scope.$watch('visible', function(visible) {
                    if (visible) {
                        $element.addClass('visible');
                    } else {
                        $element.removeClass('visible');
                    }
                });

                $(window).on('scroll', function() {
                    $scope.$apply(function() {
                        $scope.visible = ($(window).scrollTop() > 250);
                    });
                });

                $element.on('click', function(e) {
                    e.preventDefault();

                    if ($scope.visible) {
                        $.scrollTo(0, 200);
                    }
                });
            }
        }
    }])


    /**
     * Кликабельная иконка календаря у инпутов
     */
    .directive('uiDateIcon', [function() {
        return {
            restrict: 'AC',
            link: function($scope, $element) {
                var $icon = $('<i class="icon-calendar"></i>');
                $element.after($icon);
                $icon.on('click', function(e) {
                    e.preventDefault();
                    $element.focus();
                });
            }
        }
    }])

    /**
     * Управление классами body
     */
    .directive('themebleBody', [function() {
        return {
            restrict: 'A',
            scope: true,

            link: function($scope, $element) {
                $scope.fontsize = 2;

                if ($.cookie('fontsize')) {
                    $scope.fontsize = parseInt($.cookie('fontsize'), 10);
                }

                if ($scope.fontsize < 1) {
                    $scope.fontsize = 1;
                } else if ($scope.fontsize > 3) {
                    $scope.fontsize = 3;
                }

                $scope.$on('setFontSize', function(e, size) {
                    $scope.fontsize = size;
                });

                $scope.$on('setFontSmaller', function() {
                    if ($scope.fontsize > 1) {
                        $scope.fontsize--;
                    }
                });

                $scope.$on('setFontLarger', function() {
                    if ($scope.fontsize < 3) {
                        $scope.fontsize++;
                    }
                });

                $scope.$watch('fontsize', function() {
                    if (isNaN($scope.fontsize)) {
                        $scope.fontsize = 2;
                    }

                    $.cookie('fontsize', $scope.fontsize, {path: '/'});
                    $scope.$broadcast('krdFontSizeChange', $scope.fontsize);
                });
            },

            controller: function($scope) {
                $scope.getClass = function() {
                    var cl = [];;

                    if ($scope.fontsize) {
                        cl.push('font-size-' + $scope.fontsize);
                    }

                    return cl.join(' ');
                };
            }
        }
    }])

    /**
     * Кнопка расшаривания через Instapaper
     */
    .directive('krdShareInstapaper', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    var d=document;

                    try {
                        if (!d.body)
                            throw(0);

                        window.location='http://www.instapaper.com/text?u='+encodeURIComponent(d.location.href);
                    } catch(e) {
                        alert('Please wait until the page has loaded.');
                    }
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Readability
     */
    .directive('krdShareReadability', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    window.baseUrl='http://www.readability.com';
                    window.readabilityToken='';
                    var s=document.createElement('script');
                    s.setAttribute('type','text/javascript');
                    s.setAttribute('charset','UTF-8');
                    s.setAttribute('src',baseUrl+'/bookmarklet/read.js');
                    document.documentElement.appendChild(s);
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Evernote
     */
    .directive('krdShareEvernote', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    EN_CLIP_HOST='http://www.evernote.com';

                    try {
                        var x=document.createElement('SCRIPT');
                        x.type='text/javascript';
                        x.src=EN_CLIP_HOST+'/public/bookmarkClipper.js?'+(new Date().getTime()/100000);
                        document.getElementsByTagName('head')[0].appendChild(x);
                    } catch (e) {
                        location.href=EN_CLIP_HOST+'/clip.action?url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title);
                    }
                });
            }
        }
    }])

    /**
     * Кнопка расшаривания через Pocket
     */
    .directive('krdSharePocket', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    ISRIL_H='1c95';
                    PKT_D='getpocket.com';
                    ISRIL_SCRIPT=document.createElement('SCRIPT');
                    ISRIL_SCRIPT.type='text/javascript';
                    ISRIL_SCRIPT.src='http://'+PKT_D+'/b/r.js';
                    document.getElementsByTagName('head')[0].appendChild(ISRIL_SCRIPT);
                });
            }
        }
    }])

    /**
     * Кнопка печати
     */
    .directive('krdPrint', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    window.print();
                });
            }
        }
    }])

    /**
     * Кнопки уменьшения шрифта
     */
    .directive('krdFontSmaller', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $rootScope.$broadcast('setFontSmaller');
                    });
                });

                $scope.$on('krdFontSizeChange', function(e, size) {
                    if (size == 1) {
                        $element.addClass('disabled');
                    } else {
                        $element.removeClass('disabled');
                    }
                });
            }
        }
    }])

    /**
     * Кнопки увеличения шрифта
     */
    .directive('krdFontLarger', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $rootScope.$broadcast('setFontLarger');
                    });
                });

                $scope.$on('krdFontSizeChange', function(e, size) {
                    if (size == 3) {
                        $element.addClass('disabled');
                    } else {
                        $element.removeClass('disabled');
                    }
                });
            }
        }
    }])

    /**
     * Плавающий блок
     */
    .directive('floating', [function() {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                var position = $element.css('position');

                if (position == 'absolute' || position == 'fixed') {
                    return;
                }

                var offset = attrs.floating ? parseInt(attrs.floating, 10) : 0;

                var top;
                var $placeholder;

                var enable = function() {
                    !!$placeholder && $element.after($placeholder);
                    $element.css({
                        position: 'fixed',
                        top: offset
                    });
                    $element.addClass('floating');
                    $scope.stateFloating = true;
                };

                var disable = function() {
                    $element.css('position', 'static');
                    $element.removeClass('floating');
                    !!$placeholder && $placeholder.remove();
                    $scope.stateFloating = false;
                };

                var update = function() {
                    var scroll = $(window).scrollTop();

                    if (attrs.floatingState !== undefined && $scope.$eval(attrs.floatingState) === false) {
                        disable();
                        return;
                    }

                    if ($element.css('position') != 'fixed') {
                        top = $element.offset().top - parseFloat($element.css('marginTop').replace(/auto/, 0)) - offset;
                        $placeholder = $('<div></div>').css({
                            width: $element.width(),
                            height: $element.height(),
                            marginTop: parseFloat($element.css('marginTop').replace(/auto/, 0)),
                            marginBottom: parseFloat($element.css('marginBottom').replace(/auto/, 0))
                        });
                    }

                    if(scroll < top) {
                        disable();
                    } else {
                        enable();
                    }
                };

                $scope.enableFloating = enable;
                $scope.disableFloating = disable;
                $scope.updateFloating = update;
                $scope.stateFloating = false;

                $(window).on('scroll resize', update);

                if (attrs.floatingState !== undefined) {
                    $scope.$watch(attrs.floatingState, update);
                }
            }
        }
    }])

    /**
     * Скрытие элемента и удаление его из DOM
     */
    .directive('uiIf', [function() {
        return {
            transclude: 'element',
            priority: 1000,
            terminal: true,
            restrict: 'A',
            compile: function (element, attr, transclude) {
                return function (scope, element, attr) {
                    var childElement;
                    var childScope;

                    scope.$watch(attr['uiIf'], function (newValue) {
                        if (childElement) {
                            childElement.remove();
                            childElement = undefined;
                        }

                        if (childScope) {
                            childScope.$destroy();
                            childScope = undefined;
                        }

                        if (newValue) {
                            childScope = scope.$new();

                            transclude(childScope, function (clone) {
                                childElement = clone;
                                element.after(clone);
                            });
                        }
                    });
                };
            }
        };
    }])

    /**
     * Список табов
     * Очень простой. Без запоминания таба и т.п.
     */
    .directive('tabsList', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                $element.find('.pane-list > .item').each(function(index) {
                    $(this).attr({
                        'ng-click': 'setActive('+index+')',
                        'ng-class': '{active:active == '+index+'}'
                    });
                });

                var $tabs = $element.children('.tab').hide();

                // Link
                return function($scope, $element, attrs) {
                    $scope.canBeNull = (attrs.noActive !== undefined);

                    var $activePane = $element.find('.pane-list > .item.active');
                    if ($activePane.length > 0) {
                        $scope.active = $activePane.index();
                    } else if ($scope.canBeNull) {
                        $scope.active = -1;
                    } else {
                        $scope.active = 0;
                    }

                    $scope.$watch('active', function(active) {
                        $tabs.hide();
                        $tabs.eq(active).show();

                        $scope.$broadcast('tabChange', active);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.setActive = function(index) {
                    if ($scope.canBeNull && $scope.active == index) {
                        $scope.active = -1;
                    } else {
                        $scope.active = index;
                    }
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей высоте
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiHeight', [function() {
        var heightList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiHeight:'@'
            },

            link: function($scope, $element) {
                if (heightList[$scope.multiHeight] === undefined) {
                    heightList[$scope.multiHeight] = $scope.heightList = {};
                } else {
                    $scope.heightList = heightList[$scope.multiHeight];
                }

                $scope.index = i++;

                $scope.heightList[$scope.index] = $element.height();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('heightList', function(heightList) {
                    var arr = [];

                    for(var ii in heightList) {
                        arr.push(parseInt(heightList[ii], 10));
                    }
                    var height = Math.max.apply(Math, arr);

                    if (height > 0 && !isNaN(height)) {
                        $element.height(height);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.height();
                    $element.css('height', 'auto');
                    $scope.heightList[$scope.index] = $element.height();
                    $element.height(h);
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей ширине
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiWidth', [function() {
        var widthList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiWidth:'@'
            },

            link: function($scope, $element) {
                if (widthList[$scope.multiWidth] === undefined) {
                    widthList[$scope.multiWidth] = $scope.widthList = {};
                } else {
                    $scope.widthList = widthList[$scope.multiWidth];
                }

                $scope.index = i++;

                $scope.widthList[$scope.index] = $element.width();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('widthList', function(widthList) {
                    var arr = [];

                    for(var ii in widthList) {
                        arr.push(parseInt(widthList[ii], 10));
                    }
                    var width = Math.max.apply(Math, arr);

                    if (width > 0 && !isNaN(width)) {
                        $element.width(width);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.width();
                    $element.css('width', 'auto');
                    $scope.widthList[$scope.index] = $element.width();
                    $element.width(h);
                };
            }
        }
    }])

    /**
     * Модификация ng-include для полной замены шаблона
     * Поддерживает onload
     */
    .directive('ngIncludeReplace', ['$parse', function($parse) {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: function(tElement, tAttrs) {
                return $parse(tAttrs.ngIncludeReplace, {})();
            },
            link: function($scope, $element, attrs) {
                if ($.isFunction(attrs.onload)) {
                    attrs.onload();
                }
            }
        }
    }])

    /**
     * Кнопка показа большего количества элементов
     *
     * Аттрибуты:
     *     more-btn="ea-list-item"         ID шаблона для элемента
     *     more-btn-body="ea-list-body"    ID блока списка элементов
     *     url="/simple/path"              URL для загрузки списка
     *     tpl-append                      Использовать обычный $.append() для шаблона. Полезно при использовании <tr> в качестве элементов
     */
    .directive('moreBtn', ['$compile', '$http', function($compile, $http) {
        return {
            restrict: 'A',
            scope: {
                url: '@'
            },

            compile: function($element, attrs) {
                if (!!attrs.moreBtnBody) {
                    var $listBody = $('#'+attrs.moreBtnBody);
                } else {
                    var $listBody = $element.prev('.items-list');
                }

                if (!!attrs.tplAppend) {
                    var $listTemplate = $('<div ng-include-replace="\''+attrs.moreBtn+'\'" ng-repeat="item in items"></div>');

                    $listBody.append($listTemplate);
                } else {
                    var $listTemplate = $($('#'+attrs.moreBtn).html());

                    $listTemplate.attr({
                        'ng-repeat': 'item in items'
                    });

                    $listBody.append($listTemplate);
                }

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.currentPage = 1;
                    $scope.loading = false;
                    $scope.originalHtml = $element.html();

                    $compile($listTemplate)($scope);

                    $element.on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.nextPage();
                        });
                    });

                    $scope.$watch('loading', function(loading) {
                        if (loading) {
                            $element.html('Загрузка...');
                        } else {
                            $element.html($scope.originalHtml);
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if (response.page_count <= $scope.currentPage) {
                            $element.hide();
                        }

                        for(var i = 0; i < response.items.length; i++) {
                            $scope.items.push(response.items[i]);
                        }

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])

    /**
     * Блок FAQ
     */
    .directive('faqBlockInteractive', [function() {
        return {
            restrict: 'C',
            link: function($scope, $element) {
                $element.find('.item > .question').each(function() {
                    var $item = $(this).parent();

                    $(this).on('click', function(e) {
                        e.preventDefault();
                        $item.toggleClass('close open');
                    });
                });
            }
        };
    }])
;

angular.module('lazy', [])
    /**
     * Загрузка изображения только если оно видимо на странице
     * Без учета скрола, очень проставя реализация
     */
    .directive('lazyImage', [function() {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var src = attrs.original;

                if (!$element.is('img')) {
                    return;
                }

                if (!!$.fn.appear) {
                    $element.appear();

                    $element.one('appear', function() {
                        $element.attr('src', src);
                    });
                } else {
                    var interval = setInterval(function() {
                        if ($element.eq(0).is(':visible')) {
                            $element.attr('src', src);
                            clearInterval(interval);
                        }
                    }, 100);
                }

            }
        }
    }])
;

/**
 * Модуль опросов
 */
angular.module('polls', [])
    /**
     * Виджет отпросов
     */
    .directive('widgetPolls', ['$http', '$compile', function($http, $compile) {
        return {
            restrict: 'A',
            scope: {
                pollId: '=widgetPolls',
                url: '@'
            },

            compile: function($element) {
                $element.find('.item').attr('ng-class', '{loading:loading}');

                return function($scope, $element) {
                    $scope.loading = false;

                    var resultsTpl = '<div class="item" ng-repeat="item in results">{{ item.title }} - <b>{{ item.counter }}</b></div>';

                    $element.find('.item input').on('change', function() {
                        if ($scope.loading) {
                            return;
                        }

                        var id = $(this).data('id');

                        $scope.$apply(function() {
                            $scope.loading = true;

                            $http({
                                method: 'POST',
                                url: $scope.url,
                                data: {poll: $scope.pollId, answer: id}
                            }).success(function(response) {
                                $scope.loading = false;
                                $scope.results = response.poll.answers_by_counter;

                                var $answers = $element.find('.list.answers');

                                $answers.html(resultsTpl).removeClass('answers').addClass('results');
                                $compile($answers)($scope);

                                $element.next('.btn-group').remove();
                            }).error(function() {
                                $scope.loading = false;

                                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз');
                            });
                        });
                    });
                }
            }
        }
    }])
;

/**
 * Группы соц-сетей в футере
 */
angular.module('socialgroups', [])
    /**
     * Табы соц-плагинов
     */
    .directive('socialTabs', [function() {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                $element.find('.st-item').each(function() {
                    var target = $(this).data('target');

                    $(this).attr('ng-click', 'setActive("'+target+'")');
                    $(this).attr('ng-class', '{active:active == "'+target+'"}');
                });

                return function($scope, $element) {
                    $scope.active = '';

                    var fst = true;
                    $scope.$watch('active', function(active) {
                        if (fst) {
                            fst = false;
                            return;
                        }

                        $element.find('.st-item').each(function() {
                            var target = $(this).data('target');
                            if (active == target) {
                                $('#'+target).show();
                            } else {
                                $('#'+target).hide();
                            }
                        });
                    });
                }
            },

            controller: function($scope) {
                $scope.setActive = function(target) {
                    if ($scope.active == target) {
                        $scope.active = '';
                    } else {
                        $scope.active = target;
                    }
                };
            }
        }
    }])

    /**
     * Виджет твиттера
     */
    .directive('socialTwitter', [function() {
        return {
            restrict: 'A',
            scope: {
                widgetId: '@socialTwitter',
                width: '@',
                href: '@',
                height: '@'
            },

            template: '<a class="twitter-timeline" width="{{ width }}" height="{{ height }}" href="https://twitter.com/krdru" data-widget-id="{{ widgetId }}">Твиты пользователя @krdru</a>',

            link: function($scope, $element, attrs) {
                var __interval = setInterval(function() {
                    var $iframe = $element.children('iframe');

                    if ($iframe.length) {
                        $iframe.width($scope.width);
                    }
                }, 100);

                $(window).load(function() {
                    setTimeout(function() {
                        clearInterval(__interval);
                    }, 15000);

                    if (attrs.autoHide !== undefined) {
                        $element.hide().addClass('loaded');
                    }
                })
            }
        }
    }])

    /**
     * Виджет facebook
     */
    .directive('socialFacebook', [function() {
        return {
            restrict: 'A',
            scope: {
                href: '@socialFacebook',
                width: '@',
                colorscheme: '@',
                showFaces: '@',
                header: '@',
                stream: '@',
                showBorder: '@'
            },

            template: '<div class="fb-like-box" data-href="{{ href }}" data-width="{{ width }}" data-colorscheme="{{ colorscheme }}" data-show-faces="{{ showFaces }}" data-header="{{ header }}" data-stream="{{ stream }}" data-show-border="{{ showBorder }}"></div>',

            link: function($scope, $element, attrs) {
                $(window).on('load', function() {
                    if (!!FB && !!FB.init) {
                        FB.init();

                        if (attrs.autoHide !== undefined) {
                            $element.hide().addClass('loaded');
                        }
                    }
                });
            }
        }
    }])

    /**
     * Виджет вконтакте
     */
    .directive('socialVkcom', [function() {
        return {
            restrict: 'A',
            scope: {
                group: '@socialVkcom',
                width: '@',
                color1: '@',
                color2: '@',
                color3: '@'
            },

            link: function($scope, $element, attrs) {
                var id = 'vkgroup-widget-'+(Math.random()*1000).toString().replace(/[^0-9]/, '')+(Math.random()*1000).toString().replace(/[^0-9]/, '');
                var $container = $('<div id="'+id+'"></div>');

                $container.height($scope.height);

                $element.append($container);

                $(window).load(function() {
                    VK.Widgets.Group(id, {
                        mode:0,
                        width: $scope.width,
                        height: $scope.height,
                        color1: $scope.color1,
                        color2: $scope.color2,
                        color3: $scope.color3},
                    $scope.group);

                    setTimeout(function() {
                        $container.children('iframe').on('load', function() {
                            if (attrs.autoHide !== undefined) {
                                $element.hide().addClass('loaded');
                            }
                        });
                    }, 13);
                });
            }
        }
    }])
;

angular.module('video', [])
    /**
     * Видео плеер
     */
    .directive('mediaelementVideo', ['$timeout', function($timeout) {
        if (!$.fn.mediaelementplayer) {
            console.error('Mediaelement not found');
            return {};
        }

        return {
            restrict: 'C',
            link: function($scope, $element) {
                $timeout(function() {
                    $element.mediaelementplayer({
                        pluginPath: '/bundles/krdsite/vendor/mediaelement/',
                        alwaysShowControls: true,
                        iPadUseNativeControls: false,
                        iPhoneUseNativeControls: false,
                        AndroidUseNativeControls: false,
                        pauseOtherPlayers: true,
                        enableAutosize: false
                    });

                    $element.on('pause stop ended', function() {
                        $scope.$apply(function() {
                            $scope.$emit('video-stop');
                        });
                    });

                    $element.on('play', function() {
                        $scope.$apply(function() {
                            $scope.$emit('video-play');
                        });
                    });

                    $scope.$on('iconsSlider.next', function() {
                        $element.data('mediaelementplayer').pause();
                    });

                    $scope.$on('iconsSlider.prev', function() {
                        $element.data('mediaelementplayer').pause();
                    });
                });
            }
        }
    }])
;

/**
 * Виджет городского репортера
 */
angular.module('widget.reporter', [])
    .directive('widgetReporter', ['$http', function($http) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var offset = 0;

                var $updateBtn = $element.find('.btn-group .btn-update');
                var $image = $element.find('.w .icon img');
                var $links = $element.find('.w a[target="_blank"]');
                var $date = $element.find('.w .date');
                var $title = $element.find('.w .title');
                var $wrap = $element.find('.w');

                var setPublication = function(pub) {
                    var image = new Image();

                    image.onload = function() {
                        $image.attr('src', pub.image);
                        $links.attr('href', pub.url);
                        $date.text(pub.date);
                        $title.text(pub.title);

                        $wrap.stop(true, true).animate({opacity:1}, 150);
                    };

                    image.src = pub.image;
                };

                var update = function() {
                    offset++;

                    $updateBtn.addClass('disabled');
                    $wrap.stop(true, true).animate({opacity:0.3}, 150);

                    $scope.$apply(function() {
                        $http({
                            method: 'GET',
                            url: attrs.url,
                            params: {offset:offset}
                        }).success(function(response) {
                            $updateBtn.removeClass('disabled');

                            if (!!response.publications && !!response.publications[0]) {
                                setPublication(response.publications[0]);
                            }
                        }).error(function() {
                            alert('Произошла ошибка, повторите попытку позже');
                            $updateBtn.removeClass('disabled');
                        });
                    });
                };

                $updateBtn.on('click', function(e) {
                    e.preventDefault();

                    if (!$updateBtn.hasClass('disabled')) {
                        update();
                    }
                });
            }
        }
    }])
;

/**
 * Структура администрации
 */
angular.module('administration', ['dialog'])
    /**
     * Показываем курируемые подразделения
     */
    .directive('admShowSubordinates', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element) {
                var prevText = $element.text();
                $element.html('{{ title() }}');

                return function($scope, $element, attrs) {
                    $scope.uid = (Math.random() * 1000000).toString().replace(/[^0-9]/, '');
                    $scope.$body = $('.human-children[data-parent='+attrs.admShowSubordinates+']');

                    $scope.state = false;
                    $scope.text = {
                        visible: attrs.inactiveText,
                        hidden: prevText
                    };

                    $element.add($scope.$body.find('.btn.close')).on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.toggle();
                        });
                    });

                    $scope.$watch('state', function(state) {
                        if (state) {
                            $scope.show();
                        } else {
                            $scope.hide();
                        }
                    });

                    $scope.$on('admShowSubordinates-hide', function(e, o) {
                        if (o.from != $scope.uid) {
                            $scope.hide();
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                // Переключение
                $scope.toggle = function() {
                    $scope.state = !$scope.state;
                };

                // Текст кнопки
                $scope.title = function() {
                    return $scope.state ? $scope.text.visible : $scope.text.hidden;
                };

                // Показываем подразделения
                $scope.show = function() {
                    $scope.state = true;
                    $scope.$body.stop(true, true).animate({opacity:1}, {queue:false}, 170).slideDown(200);

                    $rootScope.$broadcast('admShowSubordinates-hide', {from:$scope.uid});
                };

                // Скрываем подразделения
                $scope.hide = function() {
                    $scope.state = false;
                    $scope.$body.stop(true, true).animate({opacity:0}, {queue:false}, 170).slideUp(200);
                };
            }
        }
    }])
;

/**
 * Руководители города
 */
angular.module('mayor', ['dialog'])
    /**
     * Руководитель города
     */
    .directive('mayorItem', ['$dialog', function($dialog) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                var dialogUrl = attrs.mayorItem;
                var name = attrs.mayorName;

                var showMe = function() {
                    window.location.hash = name;

                    $dialog.create(dialogUrl,
                        {
                            onClose: function() {
                                window.location.hash = '_';
                            }
                        }
                    );
                };

                $element.find('a').on('click', function(e) {
                    e.preventDefault();

                    showMe();
                });

                if (window.location.hash) {
                    var hash = window.location.hash.toString().replace('/', '').replace('#', '');
                    if (hash && hash == name) {
                        showMe();
                    }
                }
            }
        }
    }])
;

/**
 * Контроллер формы вопроса онлайн-конференции
 */
function conferenceQuestionFormCtrl($scope, $element) {
    $scope.onSuccess = function() {
        $element.find('.form-row').slideUp(200);
        $element.find('.page-title').animate({opacity:0}, 150, function() {
            $(this).text($(this).data('success')).animate({opacity:1}, 150);
        })
    };
}
conferenceQuestionFormCtrl.$inject = ['$scope', '$element'];

angular.module('news.slider', [])
    /**
     * Слайдер новостей
     */
    .directive('newsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element, attrs) {
                var $arrowTop = $('<div class="arrow top" ng-click="scrollUp()" ng-class="{hidden: position <= 0 || $items.length == 1}"><i></i></div>');

                var $arrowBottom = $('<div class="arrow bottom" ng-click="scrollDown()" ng-class="{hidden: position >= ($items.length - onPage + visibleDiff) || $items.length == 1}"><i></i></div>');

                var $iconsSliders = $element.find('.icon > .icons-slider');

                $element.find('.items')
                    .append($arrowTop)
                    .append($arrowBottom)
                    .find('.item').each(function(i) {
                        var attrsToSet = {};
                        attrsToSet['ng-click'] = 'setActive('+i+')';
                        if (attrs.noActive === undefined) {
                            attrsToSet['ng-class'] = '{active: active == '+i+'}';
                        }
                        $(this).attr(attrsToSet);
                    });

                $element.find('.items .item a').on('click', function(e) {
                    if (attrs.noActive === undefined && !$(this).closest('.item').hasClass('active')) {
                        e.preventDefault();
                    }
                });

                $element.find('.items > .btn-block').attr({
                    'ng-class': '{hidden: position < $items.length - onPage + 1}'
                });

                return function($scope, $element, attrs) {
                    $scope.$itemsWrap = $element.find('.items > .wrap');
                    $scope.$itemsIcons = $element.find('.icon > .icons-slider');
                    $scope.$items = $element.find('.items > .wrap > .item');
                    $scope.$btnLink = $element.find('.items > .btn-block');
                    $scope.onPage = !!attrs.onPage ? parseInt(attrs.onPage, 10) : 3;
                    $scope.visibleDiff = ($element.find('.items > .btn-block').length > 0) ? 1 : 0;
                    $scope.position = 0;

                    $scope.$watch('position', function(position) {
                        var $item = $scope.$items.eq(position);

                        if ($item.length != 1) {
                            return;
                        }

                        var mt = $item.position().top;

                        $scope.$itemsWrap.stop().animate({marginTop:-mt}, 300);
                    });

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$itemsIcons.eq(active);
                        $scope.$itemsIcons.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;
                $scope.position = 0;

                $scope.setActive = function(active) {
                    $scope.active = active;
                    $scope.setPosition(active - 1);
                };

                $scope.setPosition = function(position) {
                    if (position < 0) {
                        position = 0;
                    } else if (position >= ($scope.$items.length - $scope.visibleDiff)) {
                        position = $scope.$items.length - $scope.onPage;
                    }

                    $scope.position = position;
                };

                $scope.scrollUp = function() {
                    if ($scope.position > 0) {
                        $scope.position--;
                    }
                };

                $scope.scrollDown = function() {
                    if ($scope.position < ($scope.$items.length - $scope.onPage + $scope.visibleDiff)) {
                        $scope.position++;
                    }
                };
            }
        }
    }])
;

/**
 * Объекты города
 */
angular.module('city-object', ['dialog'])
    /**
     * Объект города
     */
    .directive('cityObjectItem', ['$dialog', function($dialog) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                var dialogUrl = attrs.cityObjectItem;
                var name = attrs.cityObjectName;

                var showMe = function() {
                    window.location.hash = name;

                    $dialog.create(dialogUrl,
                        {
                            onClose: function() {
                                window.location.hash = '_';
                            }
                        }
                    );
                };

                $element.find('a').on('click', function(e) {
                    e.preventDefault();

                    showMe();
                });

                if (window.location.hash) {
                    var hash = window.location.hash.toString().replace('/', '').replace('#', '');
                    if (hash && hash == name) {
                        showMe();
                    }
                }
            }
        }
    }])
;

/**
 * Слайдер подробного вида галереи
 */
angular.module('gallery.slider', ['horizontal.scroll'])
    .directive('gallerySlider', ['$compile', '$timeout', '$http', function($compile, $timeout, $http) {
        return {
            restrict: 'A',
            scrope: true,

            template: function($element) {
                var tpl = '<div class="big-preview no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                        + '<a class="icon fancybox-disabled" ng-click="openFancybox($event)" ng-href="{{ fancyboxUrl }}" ng-class="{busy:loading}" title="{{ previewTitle }}" rel="{{ uid }}-fancybox">'
                            + '<img ng-src="{{ previewUrl }}" />'
                            + '<div class="title" ng-show="!!previewTitle">{{ previewTitle }}</div>'
                            + '<div class="loading"></div>'
                        + '</a>'
                        + '<a ng-repeat="item in items" class="fancybox hidden" ng-href="{{ item.fancybox }}" title="{{ item.title }}" rel="{{ uid }}-fancybox"></a>'
                    + '</div>'

                    + '<div class="preview-list no-selection" ng-class="{noprev:(current == 0), nonext:(current == $items.length-1)}">'
                        + '<div class="items-list-wrap" horizontal-scroll="gs" horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active">'
                            + '<div class="items-list clearfix">'
                                + $element.html()
                            + '</div>'
                        + '</div>'
                        + '<div class="arrow left" ng-click="prev()"><i></i></div>'
                        + '<div class="arrow right" ng-click="next()"><i></i></div>'
                    + '</div>';

                return tpl;
            },

            compile: function($element) {
                var $items = $element.find('.preview-list .items-list > .item');

                $items.each(function(i) {
                    $(this).attr('ng-click', 'setCurrent('+i+')');
                });

                return function($scope, $element) {
                    $scope.uid = 'gs-' + (Math.random()*100000).toString().replace(/[^0-9]/, '');
                    $scope.current = 0;
                    $scope.fancyboxUrl = '';
                    $scope.previewUrl = '';
                    $scope.previewTitle = '';
                    $scope.loading = false;
                    $scope.items = [];
                    $scope.$items = $items;

                    $scope.$items.each(function() {
                        $scope.items.push({
                            fancybox: $(this).data('fancybox'),
                            title: $(this).attr('title')
                        });
                    });

                    $scope.$watch('current', function(current) {
                        if (current > $scope.$items.length - 1) {
                            $scope.current = $scope.$items.length - 1;
                            return;
                        }

                        if (current < 0) {
                            $scope.current = 0;
                            return;
                        }

                        $scope.$broadcast('setPosition', 'gs', current);
                        $scope.$items.removeClass('active');

                        var $activeItem = $scope.$items.eq(current);

                        $activeItem.addClass('active');

                        $scope.fancyboxUrl = $activeItem.data('fancybox');
                        $scope.previewUrl = $activeItem.data('preview');
                        $scope.previewTitle = $activeItem.attr('title');
                    });

                    $scope.$watch('previewUrl', function(previewUrl) {
                        if (!previewUrl) {
                            return;
                        }

                        $scope.loading = true;

                        $.get(previewUrl, function() {
                            $timeout(function() {
                                $scope.loading = false;
                            }, 200);
                        });
                    });
                }
            },

            controller: function($scope, $element) {
                $scope.next = function() {
                    $scope.current++;
                };

                $scope.prev = function() {
                    $scope.current--;
                };

                $scope.setCurrent = function(current) {
                    $scope.current = current;
                };

                $scope.openFancybox = function($event) {
                    $event.preventDefault();

                    $element.find('.fancybox.hidden[href="'+$scope.fancyboxUrl+'"]').click();
                };
            }
        }
    }])
;

angular.module('documents', [])
    /**
     * Меню с документами на главной
     */
    .directive('documentsMenu', [function() {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.find('.hoverable').each(function() {
                    var id = $(this).data('id');

                    if (!id) {
                        return;
                    }

                    $(this).attr('ng-click', 'open('+id+')');
                    $(this).attr('ng-class', '{active: current == '+id+'}');
                });

                return function($scope) {
                    $scope.current = -1;
                    $scope.$documents = $('.documents-preview');

                    var fst = true;
                    $scope.$watch('current', function(current) {
                        var $item = $scope.$documents.filter('[data-id='+current+']');

                        $scope.$documents.stop(true, true);
                        $scope.$documents.not($item).slideUp(200, function() {
                            $item.slideDown(200);
                        });

                        if (!fst && !!$.scrollTo) {
                            $.scrollTo($('#documents-menu-anchor'), 200, {offset:{top:-60}});
                        }

                        fst = false;
                    });
                };
            },

            controller: function($scope, $element) {
                /**
                 * Открытие вкладки
                 */
                $scope.open = function(id) {
                    if ($scope.current != id) {
                        $scope.current = id;
                    } else {
                        $scope.current = -1;
                    }
                };

                $scope.isOpened = function() {
                    return $scope.current != -1;
                };

                $scope.isEnabledFloating = function() {
                    var scroll = $(window).scrollTop() + $(window).height();
                    var $anchor = $('#documents-menu-end-anchor');
                    var top = $anchor.offset().top - 49;

                    return $scope.isOpened() && (scroll < top);
                };
            }
        };
    }])
;

/**
 * Поиск на сайте
 */
angular.module('search', [])
    .directive('searchMoreBtn', ['$http', function($http) {
        return {
            restrict: 'A',
            scope: {
                url: '@searchMoreBtn'
            },

            link: function($scope, $element, attrs) {
                if (!!attrs.moreBtnBody) {
                    $scope.$listBody = $('#'+attrs.moreBtnBody);
                } else {
                    $scope.$listBody = $element.prev('.items-list');
                }

                $element.on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $scope.nextPage();
                    });
                });

                $scope.loading = false;
                $scope.currentPage = 1;
                $scope.originalHtml = $element.html();

                $scope.$watch('loading', function(loading) {
                    if (loading) {
                        $element.html('Загрузка...');
                    } else {
                        $element.html($scope.originalHtml);
                    }
                });
            },

            controller: function($scope, $element) {
                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    // В ответ не смотрян и на что приходит html
                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if ($(response).find('#search-results').data('has-more') == undefined) {
                            $element.hide();
                        }

                        $scope.$listBody.append($(response).find('#search-results').html());

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])
;

/**
 * Эвакуация автотранспорта
 */
angular.module('evacuation', [])
    /**
     * Виджет на главной
     */
    .directive('widgetEvacuation', ['$http', function($http) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var $btn = $element.find('.btn-update');

                $scope.widgetItems = [];
                $scope.widgetLoading = false;

                $btn.on('click', function(e) {
                    e.preventDefault();
                    $scope.$apply(function() {
                        $scope.widgetLoading = true;

                        $http({
                            method:'GET',
                            url: attrs.url
                        }).success(function(response) {
                            $scope.widgetLoading = false;
                            $scope.widgetItems = response.items;
                        }).error(function() {
                            $scope.widgetLoading = false;
                            alert('Произошла ошибка');
                        });
                    });
                });
            }
        }
    }])
;

/**
 * Интернет приемная
 */
angular.module('appeal', [])
    /**
     * Контроллер отправки формы
     */
    .controller('AppealFormCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.onSuccess = function(response) {
            $('#appeal-submit-form-success').appendTo($element.parent()).slideUp(0).slideDown();
            $element.add($element.prev('.text-block')).slideUp(250);

            setTimeout(function() {
                if (!!response.successText) {
                    $('#appeal-submit-form-success').html(response.successText);

                    if (!!$.scrollTo) {
                        $.scrollTo($('#appeal-submit-form-success'), 200, {offset:{top:-150}});
                    }
                }
            }, 300);
        };
    }])

    /**
     * Поле выбора адресата обращения
     */
    .directive('appealDestinationField', ['$timeout', function($timeout) {
        return {
            restrict: 'C',
            scope: true,
            link: function($scope, $element, attrs) {
                var $item = $element.children('.item');
                var $handler = $item.children('.wrap-icon');
                var $title = $item.children('.wrap-icon').children('.title');

                $handler.on('click', function(e) {
                    e.preventDefault();
                    $item.toggleClass('close open');
                });

                $scope.original = $title.html();
                $scope.value = '';

                $element.find('input:radio').on('change', function() {
                    var $this = $(this);
                    $scope.$apply(function() {
                        $scope.value = $this.parent().find('.name').text();
                    });
                });

                $scope.$watch('value', function(value) {
                    if (value == '') {
                        $title.html($scope.original);
                    } else {
                        $title.html(attrs.selectedTitle.replace('%value%', '<b>' + value + '</b>'));
                    }
                });

                $timeout(function() {
                    if ($element.find('input:radio').length == 1) {
                        $element.find('input:radio').first().click();
                        $element.hide();
                    }
                });
            }
        }
    }])
;

/**
 * Подписка на новости
 */
angular.module('subscribe', [])
    .controller('subscribeFormCtrl', ['$scope', '$element', '$http', function($scope, $element, $http) {
        $scope.onSuccess = function(response) {
            $('#subscriber-form-success').appendTo($element.parent()).slideUp(0).slideDown();
            $element.slideUp();
        };

        $scope.unsubscribe = function() {
            var email = $element.find('input[type="text"]').val();

            if (email) {
                $http({
                    method: 'POST',
                    url: $element.attr('action') + '?unsubscribe=1',
                    data: {email: email}
                });

                $('#subscriber-un-form-success').appendTo($element.parent()).slideUp(0).slideDown();
                $element.slideUp();
            }
        }
    }])
;

/**
 * Модуль баннеров
 */
angular.module('banners', [])
    /**
     * Слайдер баннеров
     */
    .directive('bannersSlider', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                var $controll = $('<div class="controll"></div>');

                $element.find('.item').each(function(i) {
                    var $cItem = $('<div class="item" ng-class="{active:'+i+'==active}" ng-click="setActive('+i+')" title="'+$(this).attr('title')+'"></div>');
                    $controll.append($cItem);
                });

                $element.append($controll);

                return function($scope, $element) {
                    $scope.$items = $element.children('.item');
                    $scope.setActive(0);

                    $scope.$watch('active', $scope.update);

                    setInterval(function() {
                        $scope.$apply(function() {
                            $scope.autoNext();
                        });
                    }, 7000);
                }
            },

            controller: function($scope, $element) {
                $scope.autoNext = function() {
                    if ($element.find(':hover').length == 0) {
                        var active = $scope.active + 1;

                        if (active >= $scope.$items.length) {
                            active = 0;
                        }

                        $scope.active = active;
                    }
                };

                $scope.setActive = function(i) {
                    $scope.active = i;
                };

                $scope.update = function() {
                    var $active = $scope.$items.eq($scope.active);

                    $scope.$items.stop(true, true).css({opacity:1}).not($active).fadeOut(200);
                    $active.fadeIn(200);
                };
            }
        }
    }])
;

(function() {
    angular.extend( angular, {
        toParam: toParam
    });

    /**
    * Преобразует объект, массив или массив объектов в строку,
    * которая соответствует формату передачи данных через url
    * Почти эквивалент [url]http://api.jquery.com/jQuery.param/[/url]
    * Источник [url]http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object/1714899#1714899[/url]
    *
    * @param object
    * @param [prefix]
    * @returns {string}
    */
    function toParam( object, prefix ) {
        var stack = [];
        var value;
        var key;

        for( key in object ) {
            value = object[ key ];
            key = prefix ? prefix + '[' + key + ']' : key;

            if ( value === null ) {
                value = encodeURIComponent( key ) + '=';
            } else if ( typeof( value ) !== 'object' ) {
                value = encodeURIComponent( key ) + '=' + encodeURIComponent( value );
            } else {
                value = toParam( value, key );
            }

            stack.push( value );
        }

        return stack.join( '&' );
    }
}());


angular.module('krd', ['ui.date', 'interactive', 'header', 'widget.reporter', 'horizontal.scroll', 'gallery.slider', 'icons.slider', 'news.slider', 'calendar', 'content', 'administration', 'dialog', 'forms', 'mayor', 'video', 'city-object', 'documents', 'search', 'evacuation', 'appeal', 'citynum', 'dictionary', 'polls', 'subscribe', 'socialgroups', 'indexmap', 'lazy', 'banners', 'filters'])
    .config(['$httpProvider', '$anchorScrollProvider', function($httpProvider, $anchorScrollProvider) {
        $httpProvider.defaults.headers.post[ 'Content-Type' ] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.transformRequest = function( data ) {
            return angular.isObject( data ) && String( data ) !== '[object File]' ? angular.toParam( data ) : data;
        };

        $anchorScrollProvider.disableAutoScrolling();
    }])

    .run([function() {
        var $imgLinks = $('.text-block a[href$=".bmp"]')
            .add('.text-block a[href$=".gif"]')
            .add('.text-block a[href$=".jpg"]')
            .add('.text-block a[href$=".jpeg"]')
            .not('.disable-fancybox')
            .not('.fancybox')
            .not('.lightbox')
            .addClass('.fancybox-auto-init')
        ;


        $('.fancybox-auto-init').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $('.fancybox, .lightbox').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            },
            live: true
        });

        $(window).on('load', function() {
            if (window.MaSha !== undefined) {
                var $item = $('.page-content');

                if ($item.length > 0) {
                    new MaSha({
                        selectable: $item[0],
                        select_message: 'upmsg-selectable'
                    });
                }
            }

            if (window.location.hash) {
                setTimeout(function() {
                    if (window.location.hash.toString().indexOf('/') === -1) {
                        var $scrollItem = $(window.location.hash);

                        if ($scrollItem.length > 0) {
                            $.scrollTo($scrollItem, 200, {offset:{top:-75}});
                        }
                    }
                }, 13);
            }
        });

        // Плавный якорный скролл
        $('a[href^="#"]').on('click', function(e) {
            e.preventDefault();

            var $link = $(this);
            var $scrollItem = $($link.attr('href'));

            if ($scrollItem.length == 0) {
                $scrollItem = $('*[name="'+$link.attr('href').toString().replace('#', '')+'"]');

                if ($scrollItem.length == 0) {
                    return;
                }
            }

            $.scrollTo($scrollItem, 200, {offset:{top:-75}});
            window.location.hash = $scrollItem.attr('id');
        });

        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }])
;
