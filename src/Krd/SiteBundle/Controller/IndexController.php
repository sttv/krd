<?php

namespace Krd\SiteBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Главная страница
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * Главная страница
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
    }

    /**
     * Голосование за ремонт дорог
     * @Route("/pollroad/submit")
     * @Template()
     */
    public function pollroadSubmitAction(Request $request)
    {
        $poll = $request->get('poll');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        foreach ($poll as $id => $vote) {
            $id = (int)str_replace('id', '', $id);
            $vote = (int)$vote;

            if ($vote >= 1 && $vote <= 5) {
                $pollRoad = $em->find('KrdSiteBundle:PollRoad', $id);

                if ($pollRoad) {
                    $methodNameGet = 'getVote' . $vote;
                    $methodNameset = 'setVote' . $vote;

                    $pollRoad->{$methodNameset}($pollRoad->{$methodNameGet}() + 1);
                }
            }
        }

        $em->flush();

        exit;
    }

    /**
     * Всплывающее окно с формой сообщения об ошибке
     * @Route("/senderror/show")
     * @Template()
     */
    public function senderrorShowAction()
    {
        $form = $this->createForm('site_senderror', null, array('action' => '/senderror/send/'));

        return array('form' => $form->createView());
    }

    /**
     * Валидация и отправка сообщений об ошибках
     * @Route("/senderror/send/", defaults={"_format"="json"})
     * @Template()
     */
    public function senderrorSendAction(Request $request)
    {
        $form = $this->createForm('site_senderror');
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }

        $feedback = $form->getData();

        $this->getDoctrine()->getManager()->persist($feedback);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }
}
