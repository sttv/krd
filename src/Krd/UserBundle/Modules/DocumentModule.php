<?php

namespace Krd\UserBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use KrdUserBundle\Repository\DocumentDirectoryRepository;
use Krd\DocumentBundle\Modules\Document as ParentDocModule;
use Krd\UserBundle\Entity\Widget;


/**
 * Модуль документов личного кабинета
 */
class DocumentModule extends AbstractModule
{
    /**
     * @var ParentDocModule
     */
    protected $parentModule;

    public function setParentModule(ParentDocModule $m)
    {
        $this->parentModule = $m;
    }

    public function renderAdminContent()
    {
        // Nothing
    }

    /**
     * Получение текущего документа на основе запроса
     */
    public function getCurrent()
    {
        $id = $this->request->attributes->get('name');
        return $this->getRepository()->getMyDocument($id);
    }

    /**
     * Репозиторий папок документов
     * @return DocumentDirectoryRepository
     */
    public function getDirectoryRepository()
    {
        return $this->getManager()->getRepository('KrdUserBundle:DocumentDirectory');
    }

    /**
     * Список документов пользователя
     */
    public function renderContent()
    {
        $formBuilder = $this->parentModule->createNormaSearchFormBuilder();
        $formBuilder->setAction('/dokumenty/dokumenty-administratsii/normativnye-pravovye-akty/ofitsialnoe-opublikovanie-normativnykh-pravovykh-aktov/');

        return $this->twig->render('KrdUserBundle:Module:document/index.html.twig', array(
            'docDirectories' => $this->getDirectoryRepository()->getMyDirectories(),
            'searchform' => $formBuilder->getForm()->createView(),
        ));
    }

    /**
     * Подробная страница
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdUserBundle:Module:document/detail.html.twig', array('document' => $this->getCurrent()));
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget(Widget $widget)
    {
        return $this->twig->render('KrdUserBundle:Module:document/widget.html.twig', array('list' => $this->getRepository()->getMyDocuments(3), 'widget' => $widget));
    }
}
