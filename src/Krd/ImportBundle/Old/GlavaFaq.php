<?php

namespace Krd\ImportBundle\Old;

use Doctrine\ORM\EntityManager;
use Krd\SiteBundle\Entity\Faq;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Импорт раздела faq с сайта glava.krd.ru на страницу главы на сайт krd.ru
 */
class GlavaFaq
{
    protected $em;
	protected $gem;
	
	protected $uploadDir;

    public function __construct(EntityManager $em, EntityManager $gem)
    {
        $this->em = $em;
		$this->gem = $gem;
    }
	
	public function setUploadDir($dir)
	{
		$this->uploadDir = $dir;
	}
	
	public function getUploadDir()
	{
		return $this->uploadDir;
	}
	
	public function import($output) 
	{
		$faqList = $this->loadFaq();
		$faqNode = $this->em->find('QCoreBundle:Node', 15267);
		
		$faqProcessed = 0;
		$this->em->getConnection()->beginTransaction();
		foreach( $faqList as $oldFaq ) {
			$exists = false;
			$title = htmlspecialchars_decode($oldFaq['q']);
			$name = !empty($oldFaq['name']) ? htmlspecialchars_decode($oldFaq['name']) : '';
			$faq = $this->em->getRepository('KrdSiteBundle:Faq')->findBy(array('title' => $title));
			
			if( !empty($faq) ) {
				$faq = array_pop($faq);
				//$faq->setTitle(htmlspecialchars_decode($faq->getTitle()));
				$faq->setName($name);
				//$faq->setActive($oldFaq['visible']);
				//$output->writeln("Exists {$faq->getId()}:{$name}");
				
				$exists = true; 
			} else {
				$faq = new Faq();
				
				$faq->setParent($faqNode);
				$faq->setTitle($title); 
				$faq->setName($name);
				$faq->setDate(new \DateTime($oldFaq['date']));
				$faq->setReply($oldFaq['a']);
				$faq->setActive($oldFaq['visible']);
				$this->em->persist($faq);
			}
			
			$this->em->flush();
			$faqProcessed++;

			if ($faqProcessed % 30 == 0) {
				$this->em->getConnection()->commit();
                $this->em->clear('KrdSiteBundle:Faq');
                $output->writeln("Processed {$faqProcessed}");
                $this->em->getConnection()->beginTransaction();
			}
		}
		$this->em->getConnection()->commit();
		$output->writeln('imported '.$faqProcessed.' faq...');
	}
	
	private function loadFaq()
	{
		$list = $this->gem->getConnection()->prepare('SELECT * FROM evlanov__qa ORDER BY date DESC');
		$list->execute();
		
		$faq = array();
		while( $row = $list->fetch() ) {
			$faq[] = $row;
		}
		
		return $faq;
	}
}