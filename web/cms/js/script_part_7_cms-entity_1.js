angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'cms.table.filters', 'cms.tinymce'])

    .run(['$templateCache', function($templateCache) {
        $templateCache.put('dialog-entity-move.html', '<div class="modal-header">'
            + '<h1>Перемещение элемента</h1>'
        + '</div>'
        + '<div class="modal-body">'
            + '<div entity-move-block loading-container="loading" class="form-horizontal">'
                + '<div class="control-group">'
                    + '<label class="control-label">Куда переносить?<br/><small>Ссылка на раздел</small></label>'
                    + '<div class="controls">'
                        + '<input type="text" required="required" class="input-xxlarge" ng-model="url">'
                    + '</div>'
                + '</div>'

                + '<div class="control-group" ng-show="node">'
                    + '<label class="control-label"></label>'
                    + '<div class="controls">'
                        + '<p>Перенести сущность [{{ item.id }}]{{ item.title }} в раздел <a ng-href="{{ node.url }}" target="_blank">{{ node.title }}</a>?</p>'
                        + '<p>Типы текущего и конечного раздела должны совпадать, иначе вы не увидите переносимый материал.</p>'
                        + '<p>'
                            + '<span class="btn btn-primary btn-small" style="width:130px; text-align:left;" ng-click="submitFirst()"><i class="icon-white icon-chevron-up"></i> Да, в начало списка</span>'
                        + '</p>'
                        + '<p>'
                            + '<span class="btn btn-primary btn-small" style="width:130px; text-align:left;" ng-click="submitLast()"><i class="icon-white icon-chevron-down"></i> Да, в конец списка</span>'
                        + '</p>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>'
        + '<div class="modal-footer">'
            + '<button ng-click="closeDialog()" class="btn">Отмена</button>'
        + '</div>');
    }])

    /**
     * Контроллер для окна переноса сущности
     */
    .controller('entityMoveBlockCtrl', ['$scope', 'item', 'entityResource', 'closeDialog', function($scope, item, entityResource, closeDialog) {
        $scope.item = item;
        $scope.entityResource = entityResource;
        $scope.closeDialog = closeDialog;
    }])

    /**
     * Блок переноса сущности
     */
    .directive('entityMoveBlock', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.url = '';
                $scope.loading = false;
                $scope.httpUrl = Routes.cms_rest_node_byurl;
                $scope.node = false;
                $scope.item = $scope.$parent.item;
                $scope.entityResource = $scope.$parent.entityResource;
                $scope.closeDialog = $scope.$parent.closeDialog;

                $scope.$watch('url', $scope.updateData);
            },

            controller: function($scope) {
                $scope.updateData = function() {
                    $scope.node = false;

                    if (this.canceler !== undefined) {
                        this.canceler.resolve();
                    }

                    if (!$scope.url) {
                        return;
                    }

                    this.canceler = $q.defer();

                    $http({
                        method: 'GET',
                        url: $scope.httpUrl,
                        params: {url: $scope.url},
                        timeout: this.canceler.promise
                    }).success(function(response) {
                        if (response.success && !!response.node) {
                            $scope.node = response.node;
                        }
                    });
                };

                $scope.submit = function(movetype) {
                    $scope.loading = true;

                    $scope.entityResource.move({id:$scope.item.id, node:$scope.node.id, movetype:movetype}, function(response) {
                        $scope.loading = false;

                        if (!!response.id) {
                            $rootScope.$broadcast('ntf-success', 'Элемент успешно перенесен', 5000);
                            $rootScope.$broadcast('needUpdate');
                            $scope.closeDialog();
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела');
                        }
                    }, function(response, code) {
                        $scope.loading = false;

                        if (code > 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при переносе раздела: ' + JSON.stringify(response));
                            console.error(response);
                        }
                    });
                };

                $scope.submitFirst = function() {
                    $scope.submit('first');
                };

                $scope.submitLast = function() {
                    $scope.submit('last');
                };
            }
        }
    }])

    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', '$rootScope', '$timeout', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache, $rootScope, $timeout) {

        function quoteattr(s, preserveCR) {
            preserveCR = preserveCR ? '&#13;' : '\n';

            return ('' + s).replace(/&/g, '&amp;').replace(/'/g, '&apos;').replace(/"/g, '&quot;');
        }

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var template = '';
                var columns = $parse(tAttrs.columns)();

                if (tAttrs.disableHeaderButtons === undefined) {
                    template += '<div class="header-buttons">';
                        template += '<button class="btn btn-info" ng-click="update()"><i class="icon-refresh icon-white"></i> Обновить список</button>';

                        template += '<button class="btn btn-warning" ng-show="itemList.length > 0 && !allSelected()" ng-click="selectAll()"><i class="icon-white icon-ok-circle"></i> Выделить все</button>';

                        template += '<button class="btn btn-warning active" ng-show="itemList.length > 0 && allSelected()" ng-click="resetSelected()"><i class="icon-white icon-remove-circle"></i> Снять выделение</button>';

                        template += '<a class="btn btn-{{ i.type }}" href="#!{{ i.url }}" ng-repeat="i in headerButtons">';
                        template += '<i class="icon-{{ i.icon }} icon-white"></i>&nbsp;';
                        template += '{{ i.title }}</a>';

                        template += '<button class="btn btn-danger" ng-show="hasSelectedItems()" ng-click="removeSelectedEntities()"><i class="icon-trash icon-white"></i>&nbsp;Удалить выбранные ({{ selectedCount() }} шт.)</button>'
                    template += '</div>';
                }

                var tableClass = tAttrs.tableClass || '';

                template += '<div ng-show="itemList.length > 0">';
                template += '<table ng-table="tableParams" class="table table-hover table-striped '+tableClass+'">';
                template += '<tr data-id="{{ item.id }}" ng-repeat="item in itemList" ng-class="{selected:selectedItems[item.id] === true}">';

                if (tAttrs.disableSelection === undefined) {
                    // Выделение нескольких элементов
                    template += '<td data-title=" " width="20">';
                    template += '<label class="checkbox"><input type="checkbox" ng-model="selectedItems[item.id]" /></label>';
                    template += '</td>';
                }

                // Сортировка
                if (tAttrs.sortable !== undefined) {
                    template += '<td data-title=" " width="40" class="sortable-td">';
                    template += '<i class="icon-chevron-up" tooltip="Поднять" ng-click="sortUp(item)" ng-class="{hidden:isFirstItem(item)}"></i>';
                    template += '&nbsp;';
                    template += '<i class="icon-chevron-down" tooltip="Опустить" ng-click="sortDown(item)" ng-class="{hidden:isLastItem(item)}"></i>';
                    template += '</td>';
                }

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        if (columns[i].by instanceof Object) {
                            modifier = '|'+columns[i].modifier+':'+JSON.stringify(columns[i].by)+':item';
                            modifier = modifier.replace(/"/g, '&quot;');
                        } else {
                            modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\':item';
                        }
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html-unsafe="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label>';
                    }

                    template += '</td>';
                }

                if (tAttrs.disableActions === undefined) {
                    // Кнопки действий
                    template += '<td data-title="Действия" width="200" class="actions-td">';
                        if (tAttrs.disableMove === undefined) {
                            template += '<div class="btn btn-info btn-small" tooltip="Перенести элемент" ng-click="moveEntity(item)"><i class="icon-white icon-share-alt"></i></div>';
                        }
                        template += '<a class="btn btn-{{ i.type }} btn-small" tooltip="{{ i.tooltip }}" href="#!{{ i.url|replace:\'{id}\':$parent.item.id }}" ng-repeat="i in actionButtons">';
                            template += '<i class="icon-white icon-{{ i.icon }}"></i>';
                        template += '</a>';
                        template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                    template += '</td>';
                }


                template += '</tr></table></div>';

                template += '<div class="well well-small" ng-show="itemList.length == 0 && !loading">Список пуст</div>';

                template = '<div loading-container="loading">' + template + '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                /**
                 * Поиск элемента в текущем списке по ID
                 */
                $scope.findItemById = function(id) {
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        if ($scope.itemList[i].id == id) {
                            return $scope.itemList[i];
                        }
                    }

                    return false;
                };

                /**
                 * Сортировка элемента вверх
                 */
                $scope.sortUp = function(item) {
                    $scope.sortItem(item, 'up');
                };

                /**
                 * Сортировка элемента вниз
                 */
                $scope.sortDown = function(item) {
                    $scope.sortItem(item, 'down');
                };

                /**
                 * Сортировка
                 */
                $scope.sortItem = function(item, order) {
                    var url = $attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    url = url.replace(':id', 'sort/' + order);

                    $scope.loading = true;
                    $http({
                        method: 'POST',
                        url: url,
                        data: {id: item.id}
                    }).success(function(response) {
                        if (response.success) {
                            if (!response.moved) {
                                $rootScope.$broadcast('ntf-warning', 'При сортировке ничего не изменилось. ', 5000);
                            }
                        } else {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке.');
                        }

                        $scope.loading = false;
                        $scope.update();
                    }).error(function(response, code) {
                        if (code != 0) {
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка при сортировке: ' + JSON.stringify(response));

                            $scope.loading = false;
                            $scope.update();
                        }
                    });
                };

                /**
                 * Является ли элемент первым в списке
                 * @return {Boolean}
                 */
                $scope.isFirstItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == 1 && angular.equals(item, $scope.itemList[0]);
                };

                /**
                 * Является ли элемент последним в списке
                 * @return {Boolean}
                 */
                $scope.isLastItem = function(item) {
                    if (!$scope.itemList || $scope.itemList.length == 0) {
                        return false;
                    }

                    return $scope.tableParams.page == $scope.pagesCount && angular.equals(item, $scope.itemList[$scope.itemList.length - 1]);
                };

                /**
                 * Выполнить каллбек дял каждого выбранного элемента
                 * @param  {Function} callback Каллбек. Первый параметр ID элемента
                 */
                $scope.onEachSelected = function(callback) {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            callback(i);
                        }
                    }
                };

                // Проверяем, если ли выделенные элементы
                $scope.hasSelectedItems = function() {
                    for(var i in $scope.selectedItems) {
                        if ($scope.selectedItems.hasOwnProperty(i) && $scope.selectedItems[i] === true) {
                            return true;
                        }
                    }

                    return false;
                };

                /**
                 * Количество выбранных элементов
                 */
                $scope.selectedCount = function() {
                    var count = 0;

                    $scope.onEachSelected(function() {
                        count++;
                    });

                    return count;
                };

                /**
                 * Сброс выбранных элементов
                 */
                $scope.resetSelected = function() {
                    $scope.selectedItems = {};
                };

                /**
                 * Выделение всех видимых элементов
                 */
                $scope.selectAll = function() {
                    $scope.selectedItems = {};

                    for (var i = 0; i < $scope.itemList.length; i++) {
                        $scope.selectedItems[$scope.itemList[i].id] = true;
                    }
                };

                /**
                 * Выделены все элементы?
                 * @return {boolean}
                 */
                $scope.allSelected = function() {
                    return $scope.selectedCount() == $scope.itemList.length && $scope.itemList.length > 0;
                };

                /**
                 * Обновление списка
                 */
                $scope.update = function() {
                    $scope.loading = true;
                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        if (data.items && data.items.length == 0 && data.items_count > 0 && $scope.tableParams.page > 1) {

                            $scope.tableParams.page--;
                            return;
                        }

                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                        $scope.pagesCount = data.page_count;
                        $scope.totalItems = data.items_count;
                        $scope.resetSelected();

                        if (!!$attrs.onUpdate) {
                            $rootScope.$broadcast($attrs.onUpdate);
                        }

                        $timeout(function() {
                            $scope.loading = false;
                        });
                    }, function(response, errorData){
                        if (response.status == 0) {
                            // Загрузка была отменена
                            return;
                        }

                        $scope.itemList = [];
                        $scope.tableParams.total = 0;

                        var msg = '';

                        try {
                            msg = response.data[0].message;
                        } catch(e) {
                            msg = JSON.stringify(errorData);
                        };

                        $timeout(function() {
                            $scope.loading = false;
                        });

                        $rootScope.$broadcast('ntf-error', response.status + ' - Произошла ошибка при загрузке списка. ' + msg);
                    });
                };

                /**
                 * Есть ли в списке столбцы с логическими значениями?
                 * @return {Boolean}
                 */
                $scope.hasBooleanColumns = function() {
                    for(var i in $scope.columns) {
                        if (!$scope.columns.hasOwnProperty(i)) {
                            continue;
                        }

                        if ($scope.columns[i].isBoolean === true) {
                            return true;
                            break;
                        }
                    }

                    return false;
                };

                /**
                 * Перенос сущности
                 */
                $scope.moveEntity = function(item) {
                    var d = $dialog.dialog({
                        dialogFade: true,
                        resolve: {
                            item: function (){return angular.copy(item);},
                            entityResource: function(){return $scope.entityResource;},
                            closeDialog: function(){return function() {d.close();}}
                        },
                        dialogClass: 'modal w800'
                    });

                    d.open('dialog-entity-move.html', 'entityMoveBlockCtrl');
                };

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.loading = true;

                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function(response) {
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка при удалении: ' + JSON.stringify(response));
                                $scope.$broadcast('needUpdate');
                            });
                        }
                    });
                };

                /**
                 * Удаление выбранных сущностей
                 */
                $scope.removeSelectedEntities = function() {
                    if ($scope.selectedCount() == 0) {
                        return;
                    }

                    var title = 'Удаление элементов';
                    var msg = 'Вы уверены, что хотите удалить выбранные элементы ('+$scope.selectedCount()+' шт.)?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    var removed = 0;

                    var afterDelete = function() {
                        if (++removed == $scope.selectedCount()) {
                            $scope.$broadcast('needUpdate');
                            $scope.resetSelected();
                        }
                    };

                    $dialog.messageBox(title, msg, btns)
                        .open()
                        .then(function(result) {
                            if (result == 'ok') {

                                $scope.loading = true;
                                $scope.onEachSelected(function(id) {
                                    $scope.entityResource.remove({id:id}, afterDelete, function(response) {
                                        $rootScope.$broadcast('ntf-error', 'Произошла ошибка при удалении: ' + JSON.stringify(response));
                                        afterDelete();
                                    });
                                });
                            }
                    });
                };
            },

            compile: function($element, $attrs) {
                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    $scope.pagesCount = 0;
                    $scope.totalItems = 0;

                    // Кнопки в заголовке таблицы
                    $scope.headerButtons = [];

                    if (!!attrs.btnAdd) {
                        $scope.headerButtons.push({
                            type: 'success',
                            url: (attrs.url + '/').replace('//', '/') + 'create/' + (!!attrs.addId ? attrs.addId : ''),
                            icon: 'plus',
                            title: attrs.btnAdd
                        });
                    }

                    // Кнопки действий в конце строки каждой таблицы
                    $scope.actionButtons = [];

                    if (!!attrs.actions) {
                        var actions = $parse(attrs.actions)($scope);
                        for(var i = 0; i < actions.length; i++) {
                            $scope.actionButtons.push(actions[i]);
                        }
                    }

                    $scope.actionButtons.push({
                        type: 'warning',
                        url: (attrs.url + '/').replace('//', '/') + 'edit/{id}',
                        icon: 'edit',
                        tooltip: 'Редактировать'
                    });

                    // Список колонок
                    $scope.columns = $parse(attrs.columns)($scope);

                    // параметры аякс-таблицы
                    $scope.tableParams = new ngTableParams({
                        page: 1,
                        total: 0,
                        count: parseInt(attrs.count, 10)
                    });

                    // Список элементов
                    $scope.itemList = [];

                    // Список выделенных элементов
                    $scope.selectedItems = {};

                    // Создаем ресурс
                    var url = attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    $scope.entityResource = $resource(url, {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'},
                            move: {method:'POST', url: url.replace('/:id', '/move/:id').split('?', 2)[0]}
                        });

                    // Наблюдаем за параметрами таблицы
                    $scope.$watch('tableParams', $scope.update, true);
                    $scope.$on('needUpdate', $scope.update, true);

                    // Событие успешного добавления/редактировния элемента
                    $scope.$on('entityFormClosed', function() {
                        $scope.$broadcast('needUpdate');
                    });

                    // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                    if ($scope.hasBooleanColumns()) {
                        $scope.$watch('itemList', function(itemList, oldList) {
                            if ($scope.loading) {
                                return;
                            }

                            for(var i = 0; i < oldList.length; i++) {
                                if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                                    continue;
                                }

                                if (!angular.equals(itemList[i], oldList[i])) {
                                    var url = attrs.entityList.replace(/:id/, 'set/' + itemList[i].id);

                                    $http.post(url, itemList[i])
                                        .success(function(response) {
                                            $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                        })
                                        .error(function(response, code) {
                                            if (code !== 0) {
                                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка: ' + JSON.stringify(response));
                                            console.error(response);
                                            }
                                        });
                                }
                            }
                        }, true);
                    }

                    // Сортировка перетаскиванием
                    if (attrs.sortable !== undefined) {
                        $element.find('table[ng-table] tbody').sortable({
                            helper: function(e, tr) {
                                var $originals = tr.children();
                                var $helper = tr.clone();

                                $helper.children().each(function(index) {
                                    $(this).width($originals.eq(index).width());
                                });

                                return $helper;
                            },

                            update: function(e, ui) {
                                var $tr = $(ui.item);
                                var item = $scope.findItemById($tr.data('id'));

                                if ($tr.is(':first-child')) {
                                    var anotherItem = $scope.findItemById($tr.next('tr').data('id'));
                                    var direction = 'before';
                                } else {
                                    var anotherItem = $scope.findItemById($tr.prev('tr').data('id'));
                                    var direction = 'after';
                                }

                                if (item && anotherItem) {
                                    $scope.sortItem(item, direction+'-'+anotherItem.id);
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                $scope.$emit('entityFormClosed');
                            } else {
                                var msg = 'Форма содержит ошибки заполнения';

                                if (!!response.errors) {
                                    $scope.errors = response.errors;

                                    var eAr = [];

                                    for (var i in $scope.errors) {
                                        if ($scope.errors.hasOwnProperty(i) && $.isArray($scope.errors[i])) {
                                            eAr.push($scope.errors[i].join(', '));
                                        }
                                    }

                                    if (eAr.length > 0) {
                                        msg += ': ' + eAr.join('; ');
                                    }
                                } else {
                                    msg += ': ' + JSON.stringify(response);
                                }

                                $rootScope.$broadcast('ntf-error', msg);
                            }
                        })
                        .error(function(response, code) {
                            $scope.loading = false;

                            if (code != 0) {
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка: ' + JSON.stringify(response));
                                console.error(response);
                            }
                        });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"], input[type="email"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="range"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"], input[type="email"], input[type="password"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - коллекция объектов
     */
    .directive('formCollection', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)" title="Удалить элемент"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            compile: function($element, attrs) {
                var $body = $element.children('.controls');
                var $itemsList = $body.children('.collection-list');

                if (!attrs.prototype) {
                    $body.html('<span class="label label-important">Empty prototype</span>');
                    return;
                }

                $body.children('.btn-add').attr('ng-click', 'addElement()');

                $itemsList.children('.item').each(function() {
                    $(this).append(removeButtonTpl);
                });

                return function($scope, $element, attrs) {
                    $scope.$body = $body;
                    $scope.$itemsList = $itemsList;
                    $scope.index = $itemsList.children('.item').length;
                    $scope.proto = '<div class="item clearfix">' + attrs.prototype + '</div>';
                }
            },

            controller: function($scope, $element) {
                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var $item = $($scope.proto.replace(/__name__/g, $scope.index));

                    $scope.index++;
                    $item.append(removeButtonTpl);

                    $scope.$itemsList.append($item);
                    $compile($item)($scope);
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.target);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.parent('.item').remove();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var $fInput = $controls.children('input').first();
                    if ($fInput.length == 0) {
                        return;
                    }

                    var name = $fInput.attr('name').replace(/\[\d+\]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', (name+'_'+i).replace(/[\[\]]/g, '_'));

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });

                        $compile($this)($scope);

                        i++;
                    });
                };
            }
        }
    }])

    /**
     * Тип поля - точка на карте
     */
    .directive('yandexMapPoint', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                name: '@yandexMapPoint',
                loadedData: '&data'
            },

            link: function($scope, $element, attrs)  {
                var map, placemark;

                $scope.data = {
                    center: [45.034942, 38.976032],
                    point: [45.034942, 38.976032],
                    zoom: 11
                };

                var loadedData = $scope.loadedData();

                if (!!loadedData && loadedData.center) {
                    $scope.data.center = loadedData.center;
                }

                if (!!loadedData && loadedData.point) {
                    $scope.data.point = loadedData.point;
                }

                if (!!loadedData && loadedData.zoom) {
                    $scope.data.zoom = loadedData.zoom;
                }

                var $iCenter0 = $('<input type="text" style="display:none;" name="'+$scope.name+'[center][0]" ng-model="data.center.0" />');
                var $iCenter1 = $('<input type="text" style="display:none;" name="'+$scope.name+'[center][1]" ng-model="data.center.1" />');
                var $iPoint0 = $('<input type="text" style="display:none;" name="'+$scope.name+'[point][0]" ng-model="data.point.0" />');
                var $iPoint1 = $('<input type="text" style="display:none;" name="'+$scope.name+'[point][1]" ng-model="data.point.1" />');
                var $iZoom = $('<input type="text" style="display:none;" name="'+$scope.name+'[zoom]" ng-model="data.zoom" />');

                $element
                    .append($iCenter0)
                    .append($iCenter1)
                    .append($iPoint0)
                    .append($iPoint1)
                    .append($iZoom)
                ;

                $compile($iCenter0)($scope);
                $compile($iCenter1)($scope);
                $compile($iPoint0)($scope);
                $compile($iPoint1)($scope);
                $compile($iZoom)($scope);


                // Сохранение данных в инпуты
                var save = function() {
                    $scope.data.center = map.getCenter();
                    $scope.data.point = placemark.geometry.getCoordinates();
                    $scope.data.zoom = map.getZoom();
                    $scope.$apply();
                };

                // Начало работы виджета
                var start = function() {
                    $scope.id = 'map-' + (Math.random()*1000).toString().replace(/[^0-9]/, '');
                    $element.attr('id', $scope.id);
                    $element.css({width:'100%', height:400});

                    ymaps.ready(function() {
                        map = new ymaps.Map($scope.id, {
                            zoom: $scope.data.zoom,
                            center: $scope.data.center
                        });

                        map.controls
                            .add('zoomControl')
                            .add('mapTools')
                            .add(new ymaps.control.SearchControl({
                                provider: 'yandex#map',
                                noPlacemark:true,
                                resultsPerPage:5,
                                useMapBounds:true,
                                width:350
                            }))
                        ;

                        placemark = new ymaps.Placemark(
                            $scope.data.point,
                            {
                                hintContent: 'Метку можно перетаскивать',
                            },
                            {
                                draggable: true,
                                preset: 'twirl#redIcon',
                            }
                        );


                        map.geoObjects.add(placemark);

                        map.events.add('click', function(e){
                            var coords = e.get('coordPosition');
                            placemark.geometry.setCoordinates(coords);
                        });

                        placemark.events.add('dragend', save);

                        map.events.add(['actionend', 'actiontickcomplete', 'boundschange', 'click', 'dblclick', 'optionschange', 'sizechange', 'typechange', 'wheel'], save);
                    });
                };

                if (!window.yandexMapConnected) {
                    window.yandexMapConnected = true;
                    var fileref=document.createElement('script')
                    fileref.setAttribute("type","text/javascript")
                    fileref.setAttribute("src", 'http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU');
                    fileref.onload = start;
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                } else {
                    start();
                }
            }
        }
    }])

    /**
     * Маска ввода
     */
    .directive('uiMask', [function() {
        return {
            link:function ($scope, $element, attrs) {
                setTimeout(function() {
                    $($element).mask(attrs.uiMask, {placeholder:'#'});
                });
            }
        };
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })
;
