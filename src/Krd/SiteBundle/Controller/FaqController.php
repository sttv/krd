<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * FAQ
 */
class FaqController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {
    }

    /**
     * Список для постраничного вывода
     * @Route("/ajax/faq/list.json", name="ajax_faq_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
        $queryBuilder = $this->get('krd.site.modules.faq')->getRepository()
            ->createQueryBuilder('n')
            ->addOrderBy('n.date', 'DESC')
            ->addOrderBy('n.id', 'DESC');

        if ($parent = $request->get('parent')) {
            $queryBuilder->andWhere('n.parent = :parent');
            $queryBuilder->setParameter('parent', $parent);
        }

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }
}
