<?php

namespace Krd\NewsBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Krd\NewsBundle\Entity\News;


class NewsRepository extends EntityRepository
{
    /**
     * Список дат за которые существуют новости
     * @param  integer[optional] $parentId
     * @return array
     */
    public function getDatesList($parentId = null) {
        if (is_null($parentId)) {
            $dates = $this->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM __CLASS__ n WHERE n.active = 1 GROUP BY formatted")
            ->useResultCache(true, 15)
            ->getResult();
        } else {
            $parents = array($parentId);
            if ($parentId == 7) {
                $parents[] = 15188;
            }
            $dates = $this->createQuery("SELECT n.date, DATE_FORMAT(n.date, '%Y-%m-%d') as formatted FROM __CLASS__ n WHERE n.active = 1 AND n.parent IN (:parents) GROUP BY formatted")
            ->setParameter('parents', $parents)
            ->useResultCache(true, 15)
            ->getResult();
        }

        foreach($dates as &$date) {
            $date = $date['date'];
        }
        unset($date);

        return $dates;
    }

    /**
     * Поиск новостей по темам
     * @param  array $themes Массив вида [1,2,3]
     * @return array
     */
    public function findActiveByThemes($themes)
    {
        if (empty($themes)) {
            return array();
        }

        return $this->createQueryBuilder('n')
            ->innerJoin('n.themes', 'thm', 'WITH', 'thm.id IN (:themes)')
            ->andWhere('n.active = 1')
            ->addOrderBy('n.date', 'DESC')
            ->setParameter('themes', $themes)
            ->setMaxResults(10)
            ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();
    }

    /**
     * Поиск новостей со схожими темами
     * @param  News $item
     * @return array
     */
    public function findSimilar(News $item)
    {
        $themes = array();

        foreach($item->getThemes() as $theme) {
            $themes[] = $theme->getId();
        }

        return $this->findActiveByThemes($themes);
    }
}
