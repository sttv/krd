/**
 * Виджет Краснодар и цифры
 */
angular.module('citynum', [])
    .directive('cityAndNumbers', ['$http', function($http) {
        return {
            restrict: 'C',
            scope: {
                url: '@',
                count: '@'
            },
            link: function($scope, $element, attrs) {
                $scope.loading = false;
                $scope.ids = [attrs.id];

                $element.find('.btn-update').on('click', function(e) {
                    e.preventDefault();

                    $scope.$apply(function() {
                        $element.stop().animate({opacity:0.8}, 200);

                        $http({
                            method:'GET',
                            url: $scope.url,
                            params: {id: $scope.ids.join(',')}
                        }).success(function(response) {
                            if (response.item) {
                                $scope.ids.push(response.item.id);

                                $element.find('.title').text(response.item.title);
                                $element.find('.count').text(response.item.subtitle);
                                $element.find('.text-block').text(response.item.content);
                            }

                            $element.stop().animate({opacity:1}, 200);
                        }).error(function(response) {
                            $element.stop().animate({opacity:1}, 200);
                            alert('Произошла ошибка');
                            console.error(response);
                        });
                    });
                });

                $scope.$watch('ids', function(ids) {
                    if (ids.length == $scope.count) {
                        $scope.ids = [$scope.ids.pop()];
                    }
                }, true);
            }
        }
    }])
;
