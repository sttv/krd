<?php

namespace Q\CoreBundle\Navigation;

use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Router\CurrentNode;


/**
 * Построение меню фронтэнда
 */
class FrontMenu
{
    protected $em;
    protected $currentNode;
    protected $activeIdList = array();

    public function __construct(EntityManager $entityManager, CurrentNode $currentNode)
    {
        $this->em = $entityManager;
        $this->currentNode = $currentNode;

        if ($this->currentNode->hasNode()) {
            $node = $this->currentNode->getNode();
            $this->activeIdList[] = $node->getId();

            while($node = $node->getParent()) {
                $this->activeIdList[] = $node->getId();
            }
        }
    }

    /**
     * Получение структуры указанного меню
     * Можно указывать родительский нод для "отсчета"
     * @param  string|array $menuType
     * @param  integer[optional] $parentNodeId
     * @param  array[optional] $childMenuType Возможные типы дочерних меню
     * @return array
     */
    public function getMenu($menutype, $parentNodeId = null, $childMenuType = null)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('n');
        $queryBuilder->from('QCoreBundle:Node', 'n');
        $queryBuilder->andWhere('n.active = 1');

        if (!is_null($parentNodeId)) {
            $queryBuilder->andWhere('n.parent = :parent')
                ->setParameter('parent', $parentNodeId);
        }

        if (is_array($menutype)) {
            $queryBuilder->innerJoin('n.menutype', 'm', 'WITH', 'm.name IN (:menutype)');
        } else {
            $queryBuilder->innerJoin('n.menutype', 'm', 'WITH', 'm.name = :menutype');
        }

        $queryBuilder->setParameter('menutype', $menutype);
        $queryBuilder->orderBy('n.left', 'ASC');

        $resultRaw = $queryBuilder
            ->getQuery()
            ->useResultCache(true, 15)
            ->getResult();

        if (empty($resultRaw)) {
            return array();
        }

        if (!is_null($childMenuType)) {
            $ids = array();
            foreach($resultRaw as $node) {
                $node->setMenuActive(in_array($node->getId(), $this->activeIdList));
                $ids[] = $node->getId();
            }

            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('n');
            $queryBuilder->from('QCoreBundle:Node', 'n');
            $queryBuilder->andWhere('n.active = 1');
            $queryBuilder->andWhere('n.parent IN (:ids)');
            $queryBuilder->innerJoin('n.menutype', 'm', 'WITH', 'm.name IN (:menutype)');
            $queryBuilder->setParameter('ids', $ids);
            $queryBuilder->setParameter('menutype', $childMenuType);

            $queryBuilder->orderBy('n.left', 'ASC');

            $newResults = $queryBuilder
                ->getQuery()
                ->useResultCache(true, 15)
                ->getResult();

            $resultRaw = array_merge($resultRaw, (array)$newResults);
        }

        $resultAccos = array();
        foreach($resultRaw as $node) {
            $node->setMenuActive(in_array($node->getId(), $this->activeIdList));
            $resultAccos[$node->getId()] = $node;
        }
        unset($resultRaw);

        foreach($resultAccos as $i => &$node) {
            $parentId = $node->getParent()->getId();
            if (isset($resultAccos[$parentId])) {
                $resultAccos[$parentId]->addMenuChildren($node);
                unset($resultAccos[$i]);
            }
        }

        return $resultAccos;
    }
}
