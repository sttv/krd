<?php

namespace Q\CoreBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;


class ContentRepository extends EntityRepository
{
    /**
     * Поиск элементов у указанного родителя
     * @param  integer $parentId
     * @return array
     */
    public function findByParent($parentId)
    {
        return $this->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent")
            ->setParameter('parent', $parentId)
            ->useResultCache(true, 15)
            ->getResult();
    }

    /**
     * Поиск первого контента у указанного родителя
     * @param  integer $parentId
     * @return array
     */
    public function findOneByParent($parentId)
    {
        $result = $this->createQuery("SELECT e FROM __CLASS__ e WHERE e.parent = :parent")
            ->setParameter('parent', $parentId)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (!empty($result) && isset($result[0])) {
            return $result[0];
        }
    }
}
