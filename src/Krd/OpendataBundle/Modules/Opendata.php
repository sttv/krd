<?php

namespace Krd\OpendataBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль - Открытые данные
 */
class Opendata extends AbstractModule
{
    public function renderAdminContent()
    {
        return '';
    }

    /**
     * Главная страница открытых данных
     */
    public function renderIndexContent()
    {
        return $this->twig->render('KrdOpendataBundle:Module:opendata/index.html.twig', array('categories' => $this->getCategories()));
    }

    /**
     * Инфографика
     */
    public function renderInfographic()
    {
        $type = $this->request->query->get('type');

        return $this->twig->render('KrdOpendataBundle:Module:opendata/infographic/'.$type.'.html.twig');
    }

    /**
     * Фильтр для поиска
     * Панель с категориями, если быть точнее
     */
    public function renderCategoryFilter()
    {
        return "<div class=\"sm-filter-list\">
                    <div class=\"lst\"></div>
                    <div class=\"clear\"></div>
                </div>";
    }

    /**
     * Получение текущего учреждения
     */
    public function getCurrent()
    {
        $stPlace = $this->em->getConnection()
            ->prepare("SELECT A.`id`, A.`title`, A.`content`, A.`request`,
                        P.`address`, P.`phone`, P.`region`, P.`mappoint`, P.`qrtitle`,
                        CR.`cat`, CR.`subcat`, SC.`title` as 'subcategory_txt'
                FROM `krd__article` as A

                LEFT JOIN `krd__sm_params` as P
                ON A.`id` = P.`article`

                LEFT JOIN `krd__sm_catrelation` as CR
                ON A.`id` = CR.`article`

                LEFT JOIN `krd__sm_subcategories` as SC
                ON CR.`subcat` = SC.`id`

                WHERE A.`request` = :request
                LIMIT 1");

        $stPlace->execute(array('request' => $this->request->attributes->get('name')));
        $place = $stPlace->fetch();

        // Фотографии
        $stFoto = $this->em->getConnection()
            ->prepare("SELECT * FROM `krd__foto`
                      WHERE `module` = 47 AND `parent` = :parent
                      ORDER BY `sort`");
        $stFoto->execute(array('parent' => $place['id']));

        $place['photos'] = array();

        while ($v = $stFoto->fetch()) {
            $place['photos'][] = array(
                'title' => $v['title'],
                'small'=>'/file/socialmap/'.$v['section1'].'/'.$v['section2'].'/'.$v['name'].'/agmini.'.$v['type'],
                'big'=>'/file/socialmap/'.$v['section1'].'/'.$v['section2'].'/'.$v['name'].'/agbig.'.$v['type'],
                'big2'=>'/file/socialmap/'.$v['section1'].'/'.$v['section2'].'/'.$v['name'].'/agfull.'.$v['type'],
                'full'=>'/file/socialmap/'.$v['section1'].'/'.$v['section2'].'/'.$v['name'].'/full.'.$v['type']
            );
        }

        // Составление QR кода
        $date = date('YmdTHisZ', time());
        $qrcodedata = <<<QR
BEGIN:VCARD
VERSION:4.0
FN: {$place['qrtitle']}
TEL;TYPE=work,voice;VALUE=uri:{$place['phone']}
ADR;TYPE=work;LABEL="{$place['address']}"
 :;;{$place['address']}
REV:{$date}
END:VCARD
QR;
        $place['qrcodedata'] = urlencode($qrcodedata);

        // Рейтинг учреждения
        $stRating = $this->em->getConnection()->prepare("SELECT AVG(`rate`) as 'rate' FROM `krd__sm_rating` WHERE `parent_id` = :parent");
        $stRating->execute(array('parent' => $place['id']));
        $rating = $stRating->fetch();

        $place['rating'] = round($rating['rate']);
        $place['ratingDisabled'] = $this->request->cookies->has('voted-'.$place['id']);

        // Ссылка на учреждение
        $place['href'] = '/opendata/list/'.$place['request'].'.htm';

        return $place;
    }

    /**
     * Список категорий открытых данных
     * @return array
     */
    public function getCategories()
    {
        $catsSt = $this->em->getConnection()->prepare("SELECT * FROM `krd__sm_categories` ORDER BY `sort`");
        $catsSt->execute();
        $cats = $catsSt->fetchAll();

        $catsTmp = array();

        foreach ($cats as $item) {
            $catsTmp[$item['id']] = $item;
        }
        $cats = $catsTmp;

        $subCatsSt = $this->em->getConnection()->prepare("SELECT * FROM `krd__sm_subcategories`");
        $subCatsSt->execute();
        $subCats = $subCatsSt->fetchAll();

        foreach ($subCats as &$sc){
            if (isset($cats[$sc['parent_id']])){
                $cats[$sc['parent_id']]['subcategories'][] = $sc;
            }
        }

        return array_values($cats);
    }

    /**
     * Список регионов
     * @return array
     */
    public function getRegions()
    {
        $regionsSt = $this->em->getConnection()->prepare("SELECT * FROM `krd__sm_regions`");
        $regionsSt->execute();
        $regions = $regionsSt->fetchAll();

        foreach ($regions as &$r) {
            if (empty($r['polygon'])) continue;

            $r['polygon'] = explode('+', $r['polygon']);

            foreach ($r['polygon'] as &$p) {
                $p = explode('|', $p);
            }
        }

        return $regions;
    }

    /**
     * Список учреждений
     * @return array
     */
    public function getPlaces()
    {
        $placesSt = $this->em->getConnection()->prepare("SELECT A.`id`, A.`title`, A.`request`, A.`content`,
                        P.`address`, P.`phone`, P.`region`, P.`mappoint`,
                        CR.`cat`, CR.`subcat`,
                        C.`icon` as 'yIcon', C.`color` as 'yColor'
                    FROM `krd__article` as A

                    LEFT JOIN `krd__sm_params` as P
                    ON A.`id` = P.`article`

                    LEFT JOIN `krd__sm_catrelation` as CR
                    ON A.`id` = CR.`article`

                    LEFT JOIN `krd__sm_categories` as C
                    ON CR.`cat` = C.`id`

                    WHERE A.`parent` = 12634");
        $placesSt->execute();
        $places = $placesSt->fetchAll();

        foreach ($places as &$place) {
            $place['content'] = strip_tags($place['content']);
            $place['yCoords'] = explode('|', $place['mappoint']);
            $place['categories'] = array($place['cat']);
            $place['subcategories'] = array($place['subcat']);
            $place['href'] = '/opendata/list/'.$place['request'].'.htm';
        }

        return $places;
    }

    /**
     * Информация о категории для окна экспорта
     * @param  integer $index
     * @return array
     */
    public function getExportWindow($id)
    {
        $st = $this->em->getConnection()->prepare("SELECT `title` FROM `krd__sm_categories` WHERE `id` = :id");
        $st->execute(array('id' => $id));

        $title = $st->fetch();
        if ($title && isset($title['title'])) {
            $title = $title['title'];
        } else {
            $title = 'Unknow';
        }

        return array('title' => $title, 'category' => $id);
    }

    /**
     * Данные для экспорта
     * @return array
     */
    public function getExport($category)
    {
        $st = $this->em->getConnection()
            ->prepare("SELECT A.`id`, A.`title`, A.`content`,
                        P.`address`, P.`phone`, R.`title` as 'region', P.`mappoint`,
                        C.`title` as 'category',
                        SC.`title` as 'subcategory',
                        (SELECT AVG(`rate`) FROM `krd__sm_rating` as R WHERE R.`parent_id` = A.`id`) as 'rating'
                    FROM `krd__article` as A

                    LEFT JOIN `krd__sm_params` as P
                    ON A.`id` = P.`article`

                    LEFT JOIN `krd__sm_catrelation` as CR
                    ON A.`id` = CR.`article`

                    LEFT JOIN `krd__sm_categories` as C
                    ON CR.`cat` = C.`id`

                    LEFT JOIN `krd__sm_regions` as R
                    ON R.`id` = P.`region`

                    LEFT JOIN `krd__sm_subcategories` as SC
                    ON CR.`subcat` = SC.`id`

                    WHERE A.`parent` = 12634 AND C.`id` = :category");
        $st->execute(array('category' => $category));
        $placesRaw = $st->fetchAll();
        $places = array();

        foreach($placesRaw as $p) {
            if (empty($p['rating'])) {
                $p['rating'] = '0';
            }

            $p['content'] = str_replace(PHP_EOL, '', $p['content']);
            $p['content'] = str_replace("\r", '', $p['content']);
            $p['content'] = str_replace("\n", '', $p['content']);

            if (empty($p['content'])) {
                $p['content'] = '&nbsp;';
            }

            $places[$p['id']] = $p;
        }

        return $places;
    }

    /**
     * Создание отчет об ошибке
     * @param  string $name
     * @param  string $page
     * @param  string $description
     * @param  integer $type
     */
    public function errorReport($name, $page, $description, $type)
    {
        $st = $this->em->getConnection()->prepare("INSERT INTO `krd__sm_errorreport` (`name`, `type`, `link`, `description`, `date`) VALUE (:name, :type, :link, :description, NOW())");
        $st->execute(array('name' =>$name, 'type' => $type, 'link' => $page, 'description' => $description));
    }

    /**
     * Голосование за учреждение
     * @param  integer $placeID
     * @param  integer $placeRate
     *
     * @return integer
     */
    public function voteForPlace($placeID, $placeRate)
    {
        if ($placeRate < 1 || $placeRate > 10){
            return false;
        }

        $st = $this->em->getConnection()->prepare("INSERT INTO `krd__sm_rating` (`parent_id`, `rate`, `date`) VALUES (:parent, :rate, NOW())");
        $st->execute(array('parent' => $placeID, 'rate' => $placeRate));

        $st = $this->em->getConnection()->prepare("SELECT AVG(`rate`) as 'rate' FROM `krd__sm_rating` WHERE `parent_id` = :parent");
        $st->execute(array('parent' => $placeID));

        $rate = $st->fetch();

        if (isset($rate['rate'])) {
            return round($rate['rate']);
        } else {
            return 0;
        }
    }
}
