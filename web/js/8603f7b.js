/**
 * Сериализация формы в объект
 */
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

!function(b){var a=function(d,c,f){if(f){f.stopPropagation();f.preventDefault()}this.$element=b(d);this.$newElement=null;this.button=null;this.options=b.extend({},b.fn.selectpicker.defaults,this.$element.data(),typeof c=="object"&&c);if(this.options.title==null){this.options.title=this.$element.attr("title")}this.val=a.prototype.val;this.render=a.prototype.render;this.refresh=a.prototype.refresh;this.selectAll=a.prototype.selectAll;this.deselectAll=a.prototype.deselectAll;this.init()};a.prototype={constructor:a,init:function(d){if(!this.options.container){this.$element.hide()}else{this.$element.css("visibility","hidden")}this.multiple=this.$element.prop("multiple");var f=this.$element.attr("class")!==undefined?this.$element.attr("class").split(/\s+/):"";var h=this.$element.attr("id");this.$element.after(this.createView());this.$newElement=this.$element.next(".bootstrap-select");if(this.options.container){this.selectPosition()}this.button=this.$newElement.find("> button");if(h!==undefined){var g=this;this.button.attr("data-id",h);b('label[for="'+h+'"]').click(function(){g.$newElement.find("button[data-id="+h+"]").focus()})}for(var c=0;c<f.length;c++){if(f[c]!="selectpicker"){this.$newElement.addClass(f[c])}}if(this.multiple){this.$newElement.addClass("show-tick")}this.button.addClass(this.options.style);this.checkDisabled();this.checkTabIndex();this.clickListener();this.render();this.setSize()},createDropdown:function(){var c="<div class='btn-group bootstrap-select'><button type='button' class='btn dropdown-toggle' data-toggle='dropdown'><span class='filter-option pull-left'></span>&nbsp;<span class='caret'></span></button><ul class='dropdown-menu' role='menu'></ul></div>";return b(c)},createView:function(){var c=this.createDropdown();var d=this.createLi();c.find("ul").append(d);return c},reloadLi:function(){this.destroyLi();var c=this.createLi();this.$newElement.find("ul").append(c)},destroyLi:function(){this.$newElement.find("li").remove()},createLi:function(){var h=this;var e=[];var g=[];var c="";this.$element.find("option").each(function(){e.push(b(this).text())});this.$element.find("option").each(function(k){var l=b(this);var j=l.attr("class")!==undefined?l.attr("class"):"";var p=l.text();var n=l.data("subtext")!==undefined?'<small class="muted">'+l.data("subtext")+"</small>":"";var m=l.data("icon")!==undefined?'<i class="'+l.data("icon")+'"></i> ':"";if(l.is(":disabled")||l.parent().is(":disabled")){m="<span>"+m+"</span>"}p=m+'<span class="text">'+p+n+"</span>";if(h.options.hideDisabled&&(l.is(":disabled")||l.parent().is(":disabled"))){g.push('<a style="min-height: 0; padding: 0"></a>')}else{if(l.parent().is("optgroup")&&l.data("divider")!=true){if(l.index()==0){var o=l.parent().attr("label");var q=l.parent().data("subtext")!==undefined?'<small class="muted">'+l.parent().data("subtext")+"</small>":"";var i=l.parent().data("icon")?'<i class="'+l.parent().data("icon")+'"></i> ':"";o=i+'<span class="text">'+o+q+"</span>";if(l[0].index!=0){g.push('<div class="div-contain"><div class="divider"></div></div><dt>'+o+"</dt>"+h.createA(p,"opt "+j))}else{g.push("<dt>"+o+"</dt>"+h.createA(p,"opt "+j))}}else{g.push(h.createA(p,"opt "+j))}}else{if(l.data("divider")==true){g.push('<div class="div-contain"><div class="divider"></div></div>')}else{if(b(this).data("hidden")==true){g.push("")}else{g.push(h.createA(p,j))}}}}});if(e.length>0){for(var d=0;d<e.length;d++){var f=this.$element.find("option").eq(d);c+="<li rel="+d+">"+g[d]+"</li>"}}if(!this.multiple&&this.$element.find("option:selected").length==0&&!h.options.title){this.$element.find("option").eq(0).prop("selected",true).attr("selected","selected")}return b(c)},createA:function(d,c){return'<a tabindex="0" class="'+c+'">'+d+'<i class="icon-ok check-mark"></i></a>'},render:function(){var h=this;this.$element.find("option").each(function(i){h.setDisabled(i,b(this).is(":disabled")||b(this).parent().is(":disabled"));h.setSelected(i,b(this).is(":selected"))});var g=this.$element.find("option:selected").map(function(i,k){var j;if(h.options.showSubtext&&b(this).attr("data-subtext")&&!h.multiple){j=' <small class="muted">'+b(this).data("subtext")+"</small>"}else{j=""}if(b(this).attr("title")!=undefined){return b(this).attr("title")}else{return b(this).text()+j}}).toArray();var f=!this.multiple?g[0]:g.join(", ");if(h.multiple&&h.options.selectedTextFormat.indexOf("count")>-1){var c=h.options.selectedTextFormat.split(">");var e=this.options.hideDisabled?":not([disabled])":"";if((c.length>1&&g.length>c[1])||(c.length==1&&g.length>=2)){f="выбрано "+g.length+" из "+this.$element.find('option:not([data-divider="true"]):not([data-hidden="true"])'+e).length}}if(!f){f=h.options.title!=undefined?h.options.title:h.options.noneSelectedText}var d;if(this.options.showSubtext&&this.$element.find("option:selected").attr("data-subtext")){d=' <small class="muted">'+this.$element.find("option:selected").data("subtext")+"</small>"}else{d=""}h.$newElement.find(".filter-option").html(f+d)},setSize:function(){if(this.options.container){this.$newElement.toggle(this.$element.parent().is(":visible"))}var j=this;var e=this.$newElement.find(".dropdown-menu");var l=e.find("li > a");var o=this.$newElement.addClass("open").find(".dropdown-menu li > a").outerHeight();this.$newElement.removeClass("open");var h=e.find("li .divider").outerHeight(true);var g=this.$newElement.offset().top;var k=this.$newElement.outerHeight();var c=parseInt(e.css("padding-top"))+parseInt(e.css("padding-bottom"))+parseInt(e.css("border-top-width"))+parseInt(e.css("border-bottom-width"));var d=this.options.hideDisabled?":not(.disabled)":"";var f;if(this.options.size=="auto"){var p=function(){var q=g-b(window).scrollTop();var u=window.innerHeight;var r=c+parseInt(e.css("margin-top"))+parseInt(e.css("margin-bottom"))+2;var t=u-q-k-r;var s;f=t;if(j.$newElement.hasClass("dropup")){f=q-r}if((e.find("li").length+e.find("dt").length)>3){s=o*3+r-2}else{s=0}e.css({"max-height":f+"px","overflow-y":"auto","min-height":s+"px"})};p();b(window).resize(p);b(window).scroll(p)}else{if(this.options.size&&this.options.size!="auto"&&e.find("li"+d).length>this.options.size){var n=e.find("li"+d+" > *").filter(":not(.div-contain)").slice(0,this.options.size).last().parent().index();var m=e.find("li").slice(0,n+1).find(".div-contain").length;f=o*this.options.size+m*h+c;e.css({"max-height":f+"px","overflow-y":"auto"})}}if(this.options.width=="auto"){this.$newElement.find(".dropdown-menu").css("min-width","0");var i=this.$newElement.find(".dropdown-menu").css("width");this.$newElement.css("width",i);if(this.options.container){this.$element.css("width",i)}}else{if(this.options.width){if(this.options.container){this.$element.css("width",this.options.width);this.$newElement.width(this.$element.outerWidth())}else{this.$newElement.css("width",this.options.width)}}else{if(this.options.container){this.$newElement.width(this.$element.outerWidth())}}}},selectPosition:function(){var e=b(this.options.container).offset();var d=this.$element.offset();if(e&&d){var f=d.top-e.top;var c=d.left-e.left;this.$newElement.appendTo(this.options.container);this.$newElement.css({position:"absolute",top:f+"px",left:c+"px"})}},refresh:function(){this.reloadLi();this.render();this.setSize();this.checkDisabled();if(this.options.container){this.selectPosition()}},setSelected:function(c,d){if(d){this.$newElement.find("li").eq(c).addClass("selected")}else{this.$newElement.find("li").eq(c).removeClass("selected")}},setDisabled:function(c,d){if(d){this.$newElement.find("li").eq(c).addClass("disabled").find("a").attr("href","#").attr("tabindex",-1)}else{this.$newElement.find("li").eq(c).removeClass("disabled").find("a").removeAttr("href").attr("tabindex",0)}},isDisabled:function(){return this.$element.is(":disabled")||this.$element.attr("readonly")},checkDisabled:function(){if(this.isDisabled()){this.button.addClass("disabled");this.button.click(function(c){c.preventDefault()});this.button.attr("tabindex","-1")}else{if(!this.isDisabled()&&this.button.hasClass("disabled")){this.button.removeClass("disabled");this.button.click(function(){return true});this.button.removeAttr("tabindex")}}},checkTabIndex:function(){if(this.$element.is("[tabindex]")){var c=this.$element.attr("tabindex");this.button.attr("tabindex",c)}},clickListener:function(){var c=this;b("body").on("touchstart.dropdown",".dropdown-menu",function(d){d.stopPropagation()});this.$newElement.on("click","li a",function(j){var g=b(this).parent().index(),i=b(this).parent(),d=i.parents(".bootstrap-select"),h=c.$element.val();if(c.multiple){j.stopPropagation()}j.preventDefault();if(c.$element.not(":disabled")&&!b(this).parent().hasClass("disabled")){if(!c.multiple){c.$element.find("option").prop("selected",false);c.$element.find("option").eq(g).prop("selected",true)}else{var f=c.$element.find("option").eq(g).prop("selected");if(f){c.$element.find("option").eq(g).prop("selected",false)}else{c.$element.find("option").eq(g).prop("selected",true)}}d.find(".filter-option").html(i.text());d.find("button").focus();if(h!=c.$element.val()){c.$element.trigger("change")}c.render()}});this.$newElement.on("click","li.disabled a, li dt, li .div-contain",function(f){f.preventDefault();f.stopPropagation();var d=b(this).parent().parents(".bootstrap-select");d.find("button").focus()});this.$element.on("change",function(d){c.render()})},val:function(c){if(c!=undefined){this.$element.val(c);this.$element.trigger("change");return this.$element}else{return this.$element.val()}},selectAll:function(){this.$element.find("option").prop("selected",true).attr("selected","selected");this.render()},deselectAll:function(){this.$element.find("option").prop("selected",false).removeAttr("selected");this.render()},keydown:function(n){var o,m,h,l,j,i,p,d,g;o=b(this);h=o.parent();m=b("[role=menu] li:not(.divider):visible a",h);if(!m.length){return}if(/(38|40)/.test(n.keyCode)){l=m.index(m.filter(":focus"));i=m.parent(":not(.disabled)").first().index();p=m.parent(":not(.disabled)").last().index();j=m.eq(l).parent().nextAll(":not(.disabled)").eq(0).index();d=m.eq(l).parent().prevAll(":not(.disabled)").eq(0).index();g=m.eq(j).parent().prevAll(":not(.disabled)").eq(0).index();if(n.keyCode==38){if(l!=g&&l>d){l=d}if(l<i){l=i}}if(n.keyCode==40){if(l!=g&&l<j){l=j}if(l>p){l=p}}m.eq(l).focus()}else{var f={48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};var c=[];m.each(function(){if(b(this).parent().is(":not(.disabled)")){if(b.trim(b(this).text().toLowerCase()).substring(0,1)==f[n.keyCode]){c.push(b(this).parent().index())}}});var k=b(document).data("keycount");k++;b(document).data("keycount",k);var q=b.trim(b(":focus").text().toLowerCase()).substring(0,1);if(q!=f[n.keyCode]){k=1;b(document).data("keycount",k)}else{if(k>=c.length){b(document).data("keycount",0)}}m.eq(c[k-1]).focus()}if(/(13)/.test(n.keyCode)){b(":focus").click();h.addClass("open");b(document).data("keycount",0)}}};b.fn.selectpicker=function(e,f){var c=arguments;var g;var d=this.each(function(){if(b(this).is("select")){var m=b(this),l=m.data("selectpicker"),h=typeof e=="object"&&e;if(!l){m.data("selectpicker",(l=new a(this,h,f)))}else{if(h){for(var j in h){l.options[j]=h[j]}}}if(typeof e=="string"){var k=e;if(l[k] instanceof Function){[].shift.apply(c);g=l[k].apply(l,c)}else{g=l.options[k]}}}});if(g!=undefined){return g}else{return d}};b.fn.selectpicker.defaults={style:null,size:"auto",title:null,selectedTextFormat:"values",noneSelectedText:"Ничего не выбрано",width:null,container:false,hideDisabled:false,showSubtext:false};b(document).data("keycount",0).on("keydown","[data-toggle=dropdown], [role=menu]",a.prototype.keydown)}(window.jQuery);

/**
 * @license AngularJS v1.0.1
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {
'use strict';

/**
 * @ngdoc overview
 * @name ngSanitize
 * @description
 */

/*
 * HTML Parser By Misko Hevery (misko@hevery.com)
 * based on:  HTML Parser By John Resig (ejohn.org)
 * Original code by Erik Arvidsson, Mozilla Public License
 * http://erik.eae.net/simplehtmlparser/simplehtmlparser.js
 *
 * // Use like so:
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 */


/**
 * @ngdoc service
 * @name ngSanitize.$sanitize
 * @function
 *
 * @description
 *   The input is sanitized by parsing the html into tokens. All safe tokens (from a whitelist) are
 *   then serialized back to properly escaped html string. This means that no unsafe input can make
 *   it into the returned string, however, since our parser is more strict than a typical browser
 *   parser, it's possible that some obscure input, which would be recognized as valid HTML by a
 *   browser, won't make it through the sanitizer.
 *
 * @param {string} html Html input.
 * @returns {string} Sanitized html.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             '<p style="color:blue">an html\n' +
             '<em onmouseover="this.textContent=\'PWN3D!\'">click here</em>\n' +
             'snippet</p>';
         }
       </script>
       <div ng-controller="Ctrl">
          Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
           <table>
             <tr>
               <td>Filter</td>
               <td>Source</td>
               <td>Rendered</td>
             </tr>
             <tr id="html-filter">
               <td>html filter</td>
               <td>
                 <pre>&lt;div ng-bind-html="snippet"&gt;<br/>&lt;/div&gt;</pre>
               </td>
               <td>
                 <div ng-bind-html="snippet"></div>
               </td>
             </tr>
             <tr id="escaped-html">
               <td>no filter</td>
               <td><pre>&lt;div ng-bind="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind="snippet"></div></td>
             </tr>
             <tr id="html-unsafe-filter">
               <td>unsafe html filter</td>
               <td><pre>&lt;div ng-bind-html-unsafe="snippet"&gt;<br/>&lt;/div&gt;</pre></td>
               <td><div ng-bind-html-unsafe="snippet"></div></td>
             </tr>
           </table>
         </div>
     </doc:source>
     <doc:scenario>
       it('should sanitize the html snippet ', function() {
         expect(using('#html-filter').element('div').html()).
           toBe('<p>an html\n<em>click here</em>\nsnippet</p>');
       });

       it('should escape snippet without any filter', function() {
         expect(using('#escaped-html').element('div').html()).
           toBe("&lt;p style=\"color:blue\"&gt;an html\n" +
                "&lt;em onmouseover=\"this.textContent='PWN3D!'\"&gt;click here&lt;/em&gt;\n" +
                "snippet&lt;/p&gt;");
       });

       it('should inline raw snippet if filtered as unsafe', function() {
         expect(using('#html-unsafe-filter').element("div").html()).
           toBe("<p style=\"color:blue\">an html\n" +
                "<em onmouseover=\"this.textContent='PWN3D!'\">click here</em>\n" +
                "snippet</p>");
       });

       it('should update', function() {
         input('snippet').enter('new <b>text</b>');
         expect(using('#html-filter').binding('snippet')).toBe('new <b>text</b>');
         expect(using('#escaped-html').element('div').html()).toBe("new &lt;b&gt;text&lt;/b&gt;");
         expect(using('#html-unsafe-filter').binding("snippet")).toBe('new <b>text</b>');
       });
     </doc:scenario>
   </doc:example>
 */
var $sanitize = function(html) {
  var buf = [];
    htmlParser(html, htmlSanitizeWriter(buf));
    return buf.join('');
};


// Regular Expressions for parsing tags and attributes
var START_TAG_REGEXP = /^<\s*([\w:-]+)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*>/,
  END_TAG_REGEXP = /^<\s*\/\s*([\w:-]+)[^>]*>/,
  ATTR_REGEXP = /([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,
  BEGIN_TAG_REGEXP = /^</,
  BEGING_END_TAGE_REGEXP = /^<\s*\//,
  COMMENT_REGEXP = /<!--(.*?)-->/g,
  CDATA_REGEXP = /<!\[CDATA\[(.*?)]]>/g,
  URI_REGEXP = /^((ftp|https?):\/\/|mailto:|#)/,
  NON_ALPHANUMERIC_REGEXP = /([^\#-~| |!])/g; // Match everything outside of normal chars and " (quote character)


// Good source of info about elements and attributes
// http://dev.w3.org/html5/spec/Overview.html#semantics
// http://simon.html5.org/html-elements

// Safe Void Elements - HTML5
// http://dev.w3.org/html5/spec/Overview.html#void-elements
var voidElements = makeMap("area,br,col,hr,img,wbr");

// Elements that you can, intentionally, leave open (and which close themselves)
// http://dev.w3.org/html5/spec/Overview.html#optional-tags
var optionalEndTagBlockElements = makeMap("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),
    optionalEndTagInlineElements = makeMap("rp,rt"),
    optionalEndTagElements = angular.extend({}, optionalEndTagInlineElements, optionalEndTagBlockElements);

// Safe Block Elements - HTML5
var blockElements = angular.extend({}, optionalEndTagBlockElements, makeMap("address,article,aside," +
        "blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6," +
        "header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul"));

// Inline Elements - HTML5
var inlineElements = angular.extend({}, optionalEndTagInlineElements, makeMap("a,abbr,acronym,b,bdi,bdo," +
        "big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small," +
        "span,strike,strong,sub,sup,time,tt,u,var"));


// Special Elements (can contain anything)
var specialElements = makeMap("script,style");

var validElements = angular.extend({}, voidElements, blockElements, inlineElements, optionalEndTagElements);

//Attributes that have href and hence need to be sanitized
var uriAttrs = makeMap("background,cite,href,longdesc,src,usemap");
var validAttrs = angular.extend({}, uriAttrs, makeMap(
    'abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,'+
    'color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,'+
    'ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,'+
    'scope,scrolling,shape,span,start,summary,target,title,type,'+
    'valign,value,vspace,width'));

function makeMap(str) {
  var obj = {}, items = str.split(','), i;
  for (i = 0; i < items.length; i++) obj[items[i]] = true;
  return obj;
}


/**
 * @example
 * htmlParser(htmlString, {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * });
 *
 * @param {string} html string
 * @param {object} handler
 */
function htmlParser( html, handler ) {
  var index, chars, match, stack = [], last = html;
  stack.last = function() { return stack[ stack.length - 1 ]; };

  while ( html ) {
    chars = true;

    // Make sure we're not in a script or style element
    if ( !stack.last() || !specialElements[ stack.last() ] ) {

      // Comment
      if ( html.indexOf("<!--") === 0 ) {
        index = html.indexOf("-->");

        if ( index >= 0 ) {
          if (handler.comment) handler.comment( html.substring( 4, index ) );
          html = html.substring( index + 3 );
          chars = false;
        }

      // end tag
      } else if ( BEGING_END_TAGE_REGEXP.test(html) ) {
        match = html.match( END_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( END_TAG_REGEXP, parseEndTag );
          chars = false;
        }

      // start tag
      } else if ( BEGIN_TAG_REGEXP.test(html) ) {
        match = html.match( START_TAG_REGEXP );

        if ( match ) {
          html = html.substring( match[0].length );
          match[0].replace( START_TAG_REGEXP, parseStartTag );
          chars = false;
        }
      }

      if ( chars ) {
        index = html.indexOf("<");

        var text = index < 0 ? html : html.substring( 0, index );
        html = index < 0 ? "" : html.substring( index );

        if (handler.chars) handler.chars( decodeEntities(text) );
      }

    } else {
      html = html.replace(new RegExp("(.*)<\\s*\\/\\s*" + stack.last() + "[^>]*>", 'i'), function(all, text){
        text = text.
          replace(COMMENT_REGEXP, "$1").
          replace(CDATA_REGEXP, "$1");

        if (handler.chars) handler.chars( decodeEntities(text) );

        return "";
      });

      parseEndTag( "", stack.last() );
    }

    if ( html == last ) {
      throw "Parse Error: " + html;
    }
    last = html;
  }

  // Clean up any remaining tags
  parseEndTag();

  function parseStartTag( tag, tagName, rest, unary ) {
    tagName = angular.lowercase(tagName);
    if ( blockElements[ tagName ] ) {
      while ( stack.last() && inlineElements[ stack.last() ] ) {
        parseEndTag( "", stack.last() );
      }
    }

    if ( optionalEndTagElements[ tagName ] && stack.last() == tagName ) {
      parseEndTag( "", tagName );
    }

    unary = voidElements[ tagName ] || !!unary;

    if ( !unary )
      stack.push( tagName );

    var attrs = {};

    rest.replace(ATTR_REGEXP, function(match, name, doubleQuotedValue, singleQoutedValue, unqoutedValue) {
      var value = doubleQuotedValue
        || singleQoutedValue
        || unqoutedValue
        || '';

      attrs[name] = decodeEntities(value);
    });
    if (handler.start) handler.start( tagName, attrs, unary );
  }

  function parseEndTag( tag, tagName ) {
    var pos = 0, i;
    tagName = angular.lowercase(tagName);
    if ( tagName )
      // Find the closest opened tag of the same type
      for ( pos = stack.length - 1; pos >= 0; pos-- )
        if ( stack[ pos ] == tagName )
          break;

    if ( pos >= 0 ) {
      // Close all the open elements, up the stack
      for ( i = stack.length - 1; i >= pos; i-- )
        if (handler.end) handler.end( stack[ i ] );

      // Remove the open elements from the stack
      stack.length = pos;
    }
  }
}

/**
 * decodes all entities into regular string
 * @param value
 * @returns {string} A string with decoded entities.
 */
var hiddenPre=document.createElement("pre");
function decodeEntities(value) {
  hiddenPre.innerHTML=value.replace(/</g,"&lt;");
  return hiddenPre.innerText || hiddenPre.textContent || '';
}

/**
 * Escapes all potentially dangerous characters, so that the
 * resulting string can be safely inserted into attribute or
 * element text.
 * @param value
 * @returns escaped text
 */
function encodeEntities(value) {
  return value.
    replace(/&/g, '&amp;').
    replace(NON_ALPHANUMERIC_REGEXP, function(value){
      return '&#' + value.charCodeAt(0) + ';';
    }).
    replace(/</g, '&lt;').
    replace(/>/g, '&gt;');
}

/**
 * create an HTML/XML writer which writes to buffer
 * @param {Array} buf use buf.jain('') to get out sanitized html string
 * @returns {object} in the form of {
 *     start: function(tag, attrs, unary) {},
 *     end: function(tag) {},
 *     chars: function(text) {},
 *     comment: function(text) {}
 * }
 */
function htmlSanitizeWriter(buf){
  var ignore = false;
  var out = angular.bind(buf, buf.push);
  return {
    start: function(tag, attrs, unary){
      tag = angular.lowercase(tag);
      if (!ignore && specialElements[tag]) {
        ignore = tag;
      }
      if (!ignore && validElements[tag] == true) {
        out('<');
        out(tag);
        angular.forEach(attrs, function(value, key){
          var lkey=angular.lowercase(key);
          if (validAttrs[lkey]==true && (uriAttrs[lkey]!==true || value.match(URI_REGEXP))) {
            out(' ');
            out(key);
            out('="');
            out(encodeEntities(value));
            out('"');
          }
        });
        out(unary ? '/>' : '>');
      }
    },
    end: function(tag){
        tag = angular.lowercase(tag);
        if (!ignore && validElements[tag] == true) {
          out('</');
          out(tag);
          out('>');
        }
        if (tag == ignore) {
          ignore = false;
        }
      },
    chars: function(chars){
        if (!ignore) {
          out(encodeEntities(chars));
        }
      }
  };
}


// define ngSanitize module and register $sanitize service
angular.module('ngSanitize', []).value('$sanitize', $sanitize);

/**
 * @ngdoc directive
 * @name ngSanitize.directive:ngBindHtml
 *
 * @description
 * Creates a binding that will sanitize the result of evaluating the `expression` with the
 * {@link ngSanitize.$sanitize $sanitize} service and innerHTML the result into the current element.
 *
 * See {@link ngSanitize.$sanitize $sanitize} docs for examples.
 *
 * @element ANY
 * @param {expression} ngBindHtml {@link guide/expression Expression} to evaluate.
 */
angular.module('ngSanitize').directive('ngBindHtml', ['$sanitize', function($sanitize) {
  return function(scope, element, attr) {
    element.addClass('ng-binding').data('$binding', attr.ngBindHtml);
    scope.$watch(attr.ngBindHtml, function(value) {
      value = $sanitize(value);
      element.html(value || '');
    });
  };
}]);
/**
 * @ngdoc filter
 * @name ngSanitize.filter:linky
 * @function
 *
 * @description
 *   Finds links in text input and turns them into html links. Supports http/https/ftp/mailto and
 *   plain email address links.
 *
 * @param {string} text Input text.
 * @returns {string} Html-linkified text.
 *
 * @example
   <doc:example module="ngSanitize">
     <doc:source>
       <script>
         function Ctrl($scope) {
           $scope.snippet =
             'Pretty text with some links:\n'+
             'http://angularjs.org/,\n'+
             'mailto:us@somewhere.org,\n'+
             'another@somewhere.org,\n'+
             'and one more: ftp://127.0.0.1/.';
         }
       </script>
       <div ng-controller="Ctrl">
       Snippet: <textarea ng-model="snippet" cols="60" rows="3"></textarea>
       <table>
         <tr>
           <td>Filter</td>
           <td>Source</td>
           <td>Rendered</td>
         </tr>
         <tr id="linky-filter">
           <td>linky filter</td>
           <td>
             <pre>&lt;div ng-bind-html="snippet | linky"&gt;<br>&lt;/div&gt;</pre>
           </td>
           <td>
             <div ng-bind-html="snippet | linky"></div>
           </td>
         </tr>
         <tr id="escaped-html">
           <td>no filter</td>
           <td><pre>&lt;div ng-bind="snippet"&gt;<br>&lt;/div&gt;</pre></td>
           <td><div ng-bind="snippet"></div></td>
         </tr>
       </table>
     </doc:source>
     <doc:scenario>
       it('should linkify the snippet with urls', function() {
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('Pretty text with some links:&#10;' +
                '<a href="http://angularjs.org/">http://angularjs.org/</a>,&#10;' +
                '<a href="mailto:us@somewhere.org">us@somewhere.org</a>,&#10;' +
                '<a href="mailto:another@somewhere.org">another@somewhere.org</a>,&#10;' +
                'and one more: <a href="ftp://127.0.0.1/">ftp://127.0.0.1/</a>.');
       });

       it ('should not linkify snippet without the linky filter', function() {
         expect(using('#escaped-html').binding('snippet')).
           toBe("Pretty text with some links:\n" +
                "http://angularjs.org/,\n" +
                "mailto:us@somewhere.org,\n" +
                "another@somewhere.org,\n" +
                "and one more: ftp://127.0.0.1/.");
       });

       it('should update', function() {
         input('snippet').enter('new http://link.');
         expect(using('#linky-filter').binding('snippet | linky')).
           toBe('new <a href="http://link">http://link</a>.');
         expect(using('#escaped-html').binding('snippet')).toBe('new http://link.');
       });
     </doc:scenario>
   </doc:example>
 */
angular.module('ngSanitize').filter('linky', function() {
  var LINKY_URL_REGEXP = /((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s\.\;\,\(\)\{\}\<\>]/,
      MAILTO_REGEXP = /^mailto:/;

  return function(text) {
    if (!text) return text;
    var match;
    var raw = text;
    var html = [];
    // TODO(vojta): use $sanitize instead
    var writer = htmlSanitizeWriter(html);
    var url;
    var i;
    while ((match = raw.match(LINKY_URL_REGEXP))) {
      // We can not end in these as they are sometimes found at the end of the sentence
      url = match[0];
      // if we did not match ftp/http/mailto then assume mailto
      if (match[2] == match[3]) url = 'mailto:' + url;
      i = match.index;
      writer.chars(raw.substr(0, i));
      writer.start('a', {href:url});
      writer.chars(match[0].replace(MAILTO_REGEXP, ''));
      writer.end('a');
      raw = raw.substring(i + match[0].length);
    }
    writer.chars(raw);
    return html.join('');
  };
});

})(window, window.angular);

angular.module("ui.bootstrap",["ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.position","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(t,e,n){function o(t){for(var e in t)if(void 0!==a.style[e])return t[e]}var i=function(o,a,r){r=r||{};var s=t.defer(),l=i[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){o.unbind(l,c),s.resolve(o)})};return l&&o.bind(l,c),e(function(){angular.isString(a)?o.addClass(a):angular.isFunction(a)?a(o):angular.isObject(a)&&o.css(a),l||s.resolve(o)}),s.promise.cancel=function(){l&&o.unbind(l,c),s.reject("Transition cancelled")},s.promise},a=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},s={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return i.transitionEndEventName=o(r),i.animationEndEventName=o(s),i}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(t){var e=function(t,e,n){e.removeClass("collapse"),e.css({height:n}),e[0].offsetWidth,e.addClass("collapse")};return{link:function(n,o,i){var a,r=!0;n.$watch(function(){return o[0].scrollHeight},function(){0!==o[0].scrollHeight&&(a||(r?e(n,o,o[0].scrollHeight+"px"):e(n,o,"auto")))}),n.$watch(i.collapse,function(t){t?u():c()});var s,l=function(e){return s&&s.cancel(),s=t(o,e),s.then(function(){s=void 0},function(){s=void 0}),s},c=function(){r?(r=!1,a||e(n,o,"auto")):l({height:o[0].scrollHeight+"px"}).then(function(){a||e(n,o,"auto")}),a=!1},u=function(){a=!0,r?(r=!1,e(n,o,0)):(e(n,o,o[0].scrollHeight+"px"),l({height:"0"}))}}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(t,e,n){this.groups=[],this.closeOthers=function(o){var i=angular.isDefined(e.closeOthers)?t.$eval(e.closeOthers):n.closeOthers;i&&angular.forEach(this.groups,function(t){t!==o&&(t.isOpen=!1)})},this.addGroup=function(t){var e=this;this.groups.push(t),t.$on("$destroy",function(){e.removeGroup(t)})},this.removeGroup=function(t){var e=this.groups.indexOf(t);-1!==e&&this.groups.splice(this.groups.indexOf(t),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse","$transition","$timeout",function(t){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:["$scope",function(){this.setHeading=function(t){this.heading=t}}],link:function(e,n,o,i){var a,r;i.addGroup(e),e.isOpen=!1,o.isOpen&&(a=t(o.isOpen),r=a.assign,e.$watch(function(){return a(e.$parent)},function(t){e.isOpen=t}),e.isOpen=a?a(e.$parent):!1),e.$watch("isOpen",function(t){t&&i.closeOthers(e),r&&r(e.$parent,t)})}}}]).directive("accordionHeading",function(){return{restrict:"E",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(t,e,n){return function(t,e,o,i){i.setHeading(n(t,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(t,e,n,o){t.$watch(function(){return o[n.accordionTransclude]},function(t){t&&(e.html(""),e.append(t))})}}}),angular.module("ui.bootstrap.alert",[]).directive("alert",function(){return{restrict:"EA",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"},link:function(t,e,n){t.closeable="close"in n}}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).directive("btnRadio",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,i,a){var r=t.$eval(i.btnRadio);t.$watch(function(){return a.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){o.hasClass(e)||t.$apply(function(){a.$setViewValue(r)})})}}}]).directive("btnCheckbox",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,i,a){var r=t.$eval(i.btnCheckboxTrue),s=t.$eval(i.btnCheckboxFalse);r=angular.isDefined(r)?r:!0,s=angular.isDefined(s)?s:!1,t.$watch(function(){return a.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){t.$apply(function(){a.$setViewValue(o.hasClass(e)?s:r)})})}}}]),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$transition","$q",function(t,e,n){function o(){function n(){a?(t.next(),o()):t.pause()}i&&e.cancel(i);var r=+t.interval;!isNaN(r)&&r>=0&&(i=e(n,r))}var i,a,r=this,s=r.slides=[],l=-1;r.currentSlide=null,r.select=function(i,a){function c(){r.currentSlide&&angular.isString(a)&&!t.noTransition&&i.$element?(i.$element.addClass(a),i.$element[0].offsetWidth=i.$element[0].offsetWidth,angular.forEach(s,function(t){angular.extend(t,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(i,{direction:a,active:!0,entering:!0}),angular.extend(r.currentSlide||{},{direction:a,leaving:!0}),t.$currentTransition=n(i.$element,{}),function(e,n){t.$currentTransition.then(function(){u(e,n)},function(){u(e,n)})}(i,r.currentSlide)):u(i,r.currentSlide),r.currentSlide=i,l=p,o()}function u(e,n){angular.extend(e,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(n||{},{direction:"",active:!1,leaving:!1,entering:!1}),t.$currentTransition=null}var p=s.indexOf(i);void 0===a&&(a=p>l?"next":"prev"),i&&i!==r.currentSlide&&(t.$currentTransition?(t.$currentTransition.cancel(),e(c)):c())},r.indexOfSlide=function(t){return s.indexOf(t)},t.next=function(){var t=(l+1)%s.length;return r.select(s[t],"next")},t.prev=function(){var t=0>l-1?s.length-1:l-1;return r.select(s[t],"prev")},t.select=function(t){r.select(t)},t.isActive=function(t){return r.currentSlide===t},t.slides=function(){return s},t.$watch("interval",o),t.play=function(){a||(a=!0,o())},t.pause=function(){a=!1,i&&e.cancel(i)},r.addSlide=function(e,n){e.$element=n,s.push(e),1===s.length||e.active?(r.select(s[s.length-1]),1==s.length&&t.play()):e.active=!1},r.removeSlide=function(t){var e=s.indexOf(t);s.splice(e,1),s.length>0&&t.active&&(e>=s.length?r.select(s[e-1]):r.select(s[e]))}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"="}}}]).directive("slide",[function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"="},link:function(t,e,n,o){o.addSlide(t,e),t.$on("$destroy",function(){o.removeSlide(t)}),t.$watch("active",function(e){e&&o.select(t)})}}}]);var dialogModule=angular.module("ui.bootstrap.dialog",["ui.bootstrap.transition"]);dialogModule.controller("MessageBoxController",["$scope","dialog","model",function(t,e,n){t.title=n.title,t.message=n.message,t.buttons=n.buttons,t.close=function(t){e.close(t)}}]),dialogModule.provider("$dialog",function(){var t={backdrop:!0,dialogClass:"modal",backdropClass:"modal-backdrop",transitionClass:"fade",triggerClass:"in",dialogOpenClass:"modal-open",resolve:{},backdropFade:!1,dialogFade:!1,keyboard:!0,backdropClick:!0},e={},n={value:0};this.options=function(t){e=t},this.$get=["$http","$document","$compile","$rootScope","$controller","$templateCache","$q","$transition","$injector",function(o,i,a,r,s,l,c,u,p){function d(t){var e=angular.element("<div>");return e.addClass(t),e}function f(n){var o=this,i=this.options=angular.extend({},t,e,n);this._open=!1,this.backdropEl=d(i.backdropClass),i.backdropFade&&(this.backdropEl.addClass(i.transitionClass),this.backdropEl.removeClass(i.triggerClass)),this.modalEl=d(i.dialogClass),i.dialogFade&&(this.modalEl.addClass(i.transitionClass),this.modalEl.removeClass(i.triggerClass)),this.handledEscapeKey=function(t){27===t.which&&(o.close(),t.preventDefault(),o.$scope.$apply())},this.handleBackDropClick=function(t){o.close(),t.preventDefault(),o.$scope.$apply()},this.handleLocationChange=function(){o.close()}}var h=i.find("body");return f.prototype.isOpen=function(){return this._open},f.prototype.open=function(t,e){var n=this,o=this.options;if(t&&(o.templateUrl=t),e&&(o.controller=e),!o.template&&!o.templateUrl)throw Error("Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.");return this._loadResolves().then(function(t){var e=t.$scope=n.$scope=t.$scope?t.$scope:r.$new();if(n.modalEl.html(t.$template),n.options.controller){var o=s(n.options.controller,t);n.modalEl.children().data("ngControllerController",o)}a(n.modalEl)(e),n._addElementsToDom(),h.addClass(n.options.dialogOpenClass),setTimeout(function(){n.options.dialogFade&&n.modalEl.addClass(n.options.triggerClass),n.options.backdropFade&&n.backdropEl.addClass(n.options.triggerClass)}),n._bindEvents()}),this.deferred=c.defer(),this.deferred.promise},f.prototype.close=function(t){function e(t){t.removeClass(o.options.triggerClass)}function n(){o._open&&o._onCloseComplete(t)}var o=this,i=this._getFadingElements();if(h.removeClass(o.options.dialogOpenClass),i.length>0)for(var a=i.length-1;a>=0;a--)u(i[a],e).then(n);else this._onCloseComplete(t)},f.prototype._getFadingElements=function(){var t=[];return this.options.dialogFade&&t.push(this.modalEl),this.options.backdropFade&&t.push(this.backdropEl),t},f.prototype._bindEvents=function(){this.options.keyboard&&h.bind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.bind("click",this.handleBackDropClick),this.$scope.$on("$locationChangeSuccess",this.handleLocationChange)},f.prototype._unbindEvents=function(){this.options.keyboard&&h.unbind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.unbind("click",this.handleBackDropClick)},f.prototype._onCloseComplete=function(t){this._removeElementsFromDom(),this._unbindEvents(),this.deferred.resolve(t)},f.prototype._addElementsToDom=function(){h.append(this.modalEl),this.options.backdrop&&(0===n.value&&h.append(this.backdropEl),n.value++),this._open=!0},f.prototype._removeElementsFromDom=function(){this.modalEl.remove(),this.options.backdrop&&(n.value--,0===n.value&&this.backdropEl.remove()),this._open=!1},f.prototype._loadResolves=function(){var t,e=[],n=[],i=this;return this.options.template?t=c.when(this.options.template):this.options.templateUrl&&(t=o.get(this.options.templateUrl,{cache:l}).then(function(t){return t.data})),angular.forEach(this.options.resolve||[],function(t,o){n.push(o),e.push(angular.isString(t)?p.get(t):p.invoke(t))}),n.push("$template"),e.push(t),c.all(e).then(function(t){var e={};return angular.forEach(t,function(t,o){e[n[o]]=t}),e.dialog=i,e})},{dialog:function(t){return new f(t)},messageBox:function(t,e,n){return new f({templateUrl:"template/dialog/message.html",controller:"MessageBoxController",resolve:{model:function(){return{title:t,message:e,buttons:n}}}})}}}]}),angular.module("ui.bootstrap.dropdownToggle",[]).directive("dropdownToggle",["$document","$location","$window",function(t){var e=null,n=angular.noop;return{restrict:"CA",link:function(o,i){o.$watch("$location.path",function(){n()}),i.parent().bind("click",function(){n()}),i.bind("click",function(o){o.preventDefault(),o.stopPropagation();var a=i===e;e&&n(),a||(i.parent().addClass("open"),e=i,n=function(o){o&&(o.preventDefault(),o.stopPropagation()),t.unbind("click",n),i.parent().removeClass("open"),n=angular.noop,e=null},t.bind("click",n))})}}}]),angular.module("ui.bootstrap.modal",["ui.bootstrap.dialog"]).directive("modal",["$parse","$dialog",function(t,e){return{restrict:"EA",terminal:!0,link:function(n,o,i){var a,r=angular.extend({},n.$eval(i.uiOptions||i.bsOptions||i.options)),s=i.modal||i.show;r=angular.extend(r,{template:o.html(),resolve:{$scope:function(){return n}}});var l=e.dialog(r);o.remove(),a=i.close?function(){t(i.close)(n)}:function(){angular.isFunction(t(s).assign)&&t(s).assign(n,!1)},n.$watch(s,function(t){t?l.open().then(function(){a()}):l.isOpen()&&l.close()})}}}]),angular.module("ui.bootstrap.pagination",[]).constant("paginationConfig",{boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last"}).directive("pagination",["paginationConfig",function(t){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",maxSize:"=",onSelectPage:"&"},templateUrl:"template/pagination/pagination.html",replace:!0,link:function(e,n,o){function i(t,e,n,o){return{number:t,text:e,active:n,disabled:o}}var a=angular.isDefined(o.boundaryLinks)?e.$eval(o.boundaryLinks):t.boundaryLinks,r=angular.isDefined(o.directionLinks)?e.$eval(o.directionLinks):t.directionLinks,s=angular.isDefined(o.firstText)?o.firstText:t.firstText,l=angular.isDefined(o.previousText)?o.previousText:t.previousText,c=angular.isDefined(o.nextText)?o.nextText:t.nextText,u=angular.isDefined(o.lastText)?o.lastText:t.lastText;e.$watch("numPages + currentPage + maxSize",function(){e.pages=[];var t=1,n=e.numPages;e.maxSize&&e.maxSize<e.numPages&&(t=Math.max(e.currentPage-Math.floor(e.maxSize/2),1),n=t+e.maxSize-1,n>e.numPages&&(n=e.numPages,t=n-e.maxSize+1));for(var o=t;n>=o;o++){var p=i(o,o,e.isActive(o),!1);e.pages.push(p)}if(r){var d=i(e.currentPage-1,l,!1,e.noPrevious());e.pages.unshift(d);var f=i(e.currentPage+1,c,!1,e.noNext());e.pages.push(f)}if(a){var h=i(1,s,!1,e.noPrevious());e.pages.unshift(h);var g=i(e.numPages,u,!1,e.noNext());e.pages.push(g)}e.currentPage>e.numPages&&e.selectPage(e.numPages)}),e.noPrevious=function(){return 1===e.currentPage},e.noNext=function(){return e.currentPage===e.numPages},e.isActive=function(t){return e.currentPage===t},e.selectPage=function(t){!e.isActive(t)&&t>0&&e.numPages>=t&&(e.currentPage=t,e.onSelectPage({page:t}))}}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(t,e){function n(t,n){return t.currentStyle?t.currentStyle[n]:e.getComputedStyle?e.getComputedStyle(t)[n]:t.style[n]}function o(t){return"static"===(n(t,"position")||"static")}var i=function(e){for(var n=t[0],i=e.offsetParent||n;i&&i!==n&&o(i);)i=i.offsetParent;return i||n};return{position:function(e){var n=this.offset(e),o={top:0,left:0},a=i(e[0]);return a!=t[0]&&(o=this.offset(angular.element(a)),o.top+=a.clientTop,o.left+=a.clientLeft),{width:e.prop("offsetWidth"),height:e.prop("offsetHeight"),top:n.top-o.top,left:n.left-o.left}},offset:function(n){var o=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:o.top+(e.pageYOffset||t[0].body.scrollTop),left:o.left+(e.pageXOffset||t[0].body.scrollLeft)}}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position"]).provider("$tooltip",function(){function t(t){var e=/[A-Z]/g,n="-";return t.replace(e,function(t,e){return(e?n:"")+t.toLowerCase()})}var e={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},o={};this.options=function(t){angular.extend(o,t)},this.$get=["$window","$compile","$timeout","$parse","$document","$position",function(i,a,r,s,l,c){return function(i,u,p){function d(t){var e,o;return e=t||f.trigger||p,o=angular.isDefined(f.trigger)?n[f.trigger]||e:n[e]||e,{show:e,hide:o}}var f=angular.extend({},e,o),h=t(i),g=d(void 0),m="<"+h+"-popup "+'title="{{tt_title}}" '+'content="{{tt_content}}" '+'placement="{{tt_placement}}" '+'animation="tt_animation()" '+'is-open="tt_isOpen"'+">"+"</"+h+"-popup>";return{restrict:"EA",scope:!0,link:function(t,e,n){function o(){t.tt_isOpen?h():p()}function p(){t.tt_popupDelay?y=r(v,t.tt_popupDelay):t.$apply(v)}function h(){t.$apply(function(){$()})}function v(){var n,o,i,a;if(t.tt_content){switch(b&&r.cancel(b),x.css({top:0,left:0,display:"block"}),f.appendToBody?(k=k||l.find("body"),k.append(x)):e.after(x),n=c.position(e),o=x.prop("offsetWidth"),i=x.prop("offsetHeight"),t.tt_placement){case"right":a={top:n.top+n.height/2-i/2+"px",left:n.left+n.width+"px"};break;case"bottom":a={top:n.top+n.height+"px",left:n.left+n.width/2-o/2+"px"};break;case"left":a={top:n.top+n.height/2-i/2+"px",left:n.left-o+"px"};break;default:a={top:n.top-i+"px",left:n.left+n.width/2-o/2+"px"}}x.css(a),t.tt_isOpen=!0}}function $(){t.tt_isOpen=!1,r.cancel(y),angular.isDefined(t.tt_animation)&&t.tt_animation()?b=r(function(){x.remove()},500):x.remove()}var b,y,k,x=a(m)(t);t.tt_isOpen=!1,n.$observe(i,function(e){t.tt_content=e}),n.$observe(u+"Title",function(e){t.tt_title=e}),n.$observe(u+"Placement",function(e){t.tt_placement=angular.isDefined(e)?e:f.placement}),n.$observe(u+"Animation",function(e){t.tt_animation=angular.isDefined(e)?s(e):function(){return f.animation}}),n.$observe(u+"PopupDelay",function(e){var n=parseInt(e,10);t.tt_popupDelay=isNaN(n)?f.popupDelay:n}),n.$observe(u+"Trigger",function(t){e.unbind(g.show),e.unbind(g.hide),g=d(t),g.show===g.hide?e.bind(g.show,o):(e.bind(g.show,p),e.bind(g.hide,h))})}}}}]}).directive("tooltipPopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(t){return t("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(t){return t("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$compile","$timeout","$parse","$window","$tooltip",function(t,e,n,o,i){return i("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",["ui.bootstrap.transition"]).constant("progressConfig",{animate:!0,autoType:!1,stackedTypes:["success","info","warning","danger"]}).controller("ProgressBarController",["$scope","$attrs","progressConfig",function(t,e,n){function o(t){return r[t]}var i=angular.isDefined(e.animate)?t.$eval(e.animate):n.animate,a=angular.isDefined(e.autoType)?t.$eval(e.autoType):n.autoType,r=angular.isDefined(e.stackedTypes)?t.$eval("["+e.stackedTypes+"]"):n.stackedTypes;this.makeBar=function(t,e,n){var r=angular.isObject(t)?t.value:t||0,s=angular.isObject(e)?e.value:e||0,l=angular.isObject(t)&&angular.isDefined(t.type)?t.type:a?o(n||0):null;return{from:s,to:r,type:l,animate:i}},this.addBar=function(e){t.bars.push(e),t.totalPercent+=e.to},this.clearBars=function(){t.bars=[],t.totalPercent=0},this.clearBars()}]).directive("progress",function(){return{restrict:"EA",replace:!0,controller:"ProgressBarController",scope:{value:"=",onFull:"&",onEmpty:"&"},templateUrl:"template/progressbar/progress.html",link:function(t,e,n,o){t.$watch("value",function(t,e){if(o.clearBars(),angular.isArray(t))for(var n=0,i=t.length;i>n;n++)o.addBar(o.makeBar(t[n],e[n],n));else o.addBar(o.makeBar(t,e))},!0),t.$watch("totalPercent",function(e){e>=100?t.onFull():0>=e&&t.onEmpty()},!0)}}}).directive("progressbar",["$transition",function(t){return{restrict:"EA",replace:!0,scope:{width:"=",old:"=",type:"=",animate:"="},templateUrl:"template/progressbar/bar.html",link:function(e,n){e.$watch("width",function(o){e.animate?(n.css("width",e.old+"%"),t(n,{width:o+"%"})):n.css("width",o+"%")})}}}]),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5}).directive("rating",["ratingConfig","$parse",function(t,e){return{restrict:"EA",scope:{value:"="},templateUrl:"template/rating/rating.html",replace:!0,link:function(n,o,i){var a=angular.isDefined(i.max)?n.$eval(i.max):t.max;n.range=[];for(var r=1;a>=r;r++)n.range.push(r);n.rate=function(t){n.readonly||(n.value=t)},n.enter=function(t){n.readonly||(n.val=t)},n.reset=function(){n.val=angular.copy(n.value)},n.reset(),n.$watch("value",function(t){n.val=t}),n.readonly=!1,i.readonly&&n.$parent.$watch(e(i.readonly),function(t){n.readonly=!!t})}}}]),angular.module("ui.bootstrap.tabs",[]).controller("TabsController",["$scope","$element",function(t){var e=t.panes=[];this.select=t.select=function(t){angular.forEach(e,function(t){t.selected=!1}),t.selected=!0},this.addPane=function(n){e.length||t.select(n),e.push(n)},this.removePane=function(n){var o=e.indexOf(n);e.splice(o,1),n.selected&&e.length>0&&t.select(e[e.length>o?o:o-1])}}]).directive("tabs",function(){return{restrict:"EA",transclude:!0,scope:{},controller:"TabsController",templateUrl:"template/tabs/tabs.html",replace:!0}}).directive("pane",["$parse",function(t){return{require:"^tabs",restrict:"EA",transclude:!0,scope:{heading:"@"},link:function(e,n,o,i){var a,r;e.selected=!1,o.active&&(a=t(o.active),r=a.assign,e.$watch(function(){return a(e.$parent)},function(t){e.selected=t}),e.selected=a?a(e.$parent):!1),e.$watch("selected",function(t){t&&i.select(e),r&&r(e.$parent,t)}),i.addPane(e),e.$on("$destroy",function(){i.removePane(e)})},templateUrl:"template/tabs/pane.html",replace:!0}}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position"]).factory("typeaheadParser",["$parse",function(t){var e=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var o=n.match(e);if(!o)throw Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:o[3],source:t(o[4]),viewMapper:t(o[2]||o[1]),modelMapper:t(o[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$document","$position","typeaheadParser",function(t,e,n,o,i,a){var r=[9,13,27,38,40];return{require:"ngModel",link:function(s,l,c,u){var p,d=s.$eval(c.typeaheadMinLength)||1,f=a.parse(c.typeahead),h=s.$eval(c.typeaheadEditable)!==!1,g=e(c.typeaheadLoading).assign||angular.noop,m=angular.element("<typeahead-popup matches='matches' active='activeIdx' select='select(activeIdx)' query='query' position='position'></typeahead-popup>"),v=s.$new();s.$on("$destroy",function(){v.$destroy()});var $=function(){v.matches=[],v.activeIdx=-1},b=function(t){var e={$viewValue:t};g(s,!0),n.when(f.source(v,e)).then(function(n){if(t===u.$viewValue){if(n.length>0){v.activeIdx=0,v.matches.length=0;for(var o=0;n.length>o;o++)e[f.itemName]=n[o],v.matches.push({label:f.viewMapper(v,e),model:n[o]});v.query=t,v.position=i.position(l),v.position.top=v.position.top+l.prop("offsetHeight")}else $();g(s,!1)}},function(){$(),g(s,!1)})};$(),v.query=void 0,u.$parsers.push(function(t){return $(),p?t:(t&&t.length>=d&&b(t),h?t:void 0)}),u.$render=function(){var t={};t[f.itemName]=p||u.$viewValue,l.val(f.viewMapper(v,t)||u.$viewValue),p=void 0},v.select=function(t){var e={};e[f.itemName]=p=v.matches[t].model,u.$setViewValue(f.modelMapper(v,e)),u.$render()},l.bind("keydown",function(t){0!==v.matches.length&&-1!==r.indexOf(t.which)&&(t.preventDefault(),40===t.which?(v.activeIdx=(v.activeIdx+1)%v.matches.length,v.$digest()):38===t.which?(v.activeIdx=(v.activeIdx?v.activeIdx:v.matches.length)-1,v.$digest()):13===t.which||9===t.which?v.$apply(function(){v.select(v.activeIdx)}):27===t.which&&(t.stopPropagation(),$(),v.$digest()))}),o.bind("click",function(){$(),v.$digest()}),l.after(t(m)(v))}}}]).directive("typeaheadPopup",function(){return{restrict:"E",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead.html",link:function(t){t.isOpen=function(){return t.matches.length>0},t.isActive=function(e){return t.active==e},t.selectActive=function(e){t.active=e},t.selectMatch=function(e){t.select({activeIdx:e})}}}}).filter("typeaheadHighlight",function(){function t(t){return t.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(e,n){return n?e.replace(RegExp(t(n),"gi"),"<strong>$&</strong>"):n}});

angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.position","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/dialog/message.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/rating/rating.html","template/tabs/pane.html","template/tabs/tabs.html","template/typeahead/typeahead.html"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(t,e,n){function o(t){for(var e in t)if(void 0!==i.style[e])return t[e]}var a=function(o,i,r){r=r||{};var l=t.defer(),s=a[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){o.unbind(s,c),l.resolve(o)})};return s&&o.bind(s,c),e(function(){angular.isString(i)?o.addClass(i):angular.isFunction(i)?i(o):angular.isObject(i)&&o.css(i),s||l.resolve(o)}),l.promise.cancel=function(){s&&o.unbind(s,c),l.reject("Transition cancelled")},l.promise},i=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},l={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return a.transitionEndEventName=o(r),a.animationEndEventName=o(l),a}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(t){var e=function(t,e,n){e.removeClass("collapse"),e.css({height:n}),e[0].offsetWidth,e.addClass("collapse")};return{link:function(n,o,a){var i,r=!0;n.$watch(function(){return o[0].scrollHeight},function(){0!==o[0].scrollHeight&&(i||(r?e(n,o,o[0].scrollHeight+"px"):e(n,o,"auto")))}),n.$watch(a.collapse,function(t){t?u():c()});var l,s=function(e){return l&&l.cancel(),l=t(o,e),l.then(function(){l=void 0},function(){l=void 0}),l},c=function(){r?(r=!1,i||e(n,o,"auto")):s({height:o[0].scrollHeight+"px"}).then(function(){i||e(n,o,"auto")}),i=!1},u=function(){i=!0,r?(r=!1,e(n,o,0)):(e(n,o,o[0].scrollHeight+"px"),s({height:"0"}))}}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(t,e,n){this.groups=[],this.closeOthers=function(o){var a=angular.isDefined(e.closeOthers)?t.$eval(e.closeOthers):n.closeOthers;a&&angular.forEach(this.groups,function(t){t!==o&&(t.isOpen=!1)})},this.addGroup=function(t){var e=this;this.groups.push(t),t.$on("$destroy",function(){e.removeGroup(t)})},this.removeGroup=function(t){var e=this.groups.indexOf(t);-1!==e&&this.groups.splice(this.groups.indexOf(t),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse","$transition","$timeout",function(t){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:["$scope",function(){this.setHeading=function(t){this.heading=t}}],link:function(e,n,o,a){var i,r;a.addGroup(e),e.isOpen=!1,o.isOpen&&(i=t(o.isOpen),r=i.assign,e.$watch(function(){return i(e.$parent)},function(t){e.isOpen=t}),e.isOpen=i?i(e.$parent):!1),e.$watch("isOpen",function(t){t&&a.closeOthers(e),r&&r(e.$parent,t)})}}}]).directive("accordionHeading",function(){return{restrict:"E",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(t,e,n){return function(t,e,o,a){a.setHeading(n(t,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(t,e,n,o){t.$watch(function(){return o[n.accordionTransclude]},function(t){t&&(e.html(""),e.append(t))})}}}),angular.module("ui.bootstrap.alert",[]).directive("alert",function(){return{restrict:"EA",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"},link:function(t,e,n){t.closeable="close"in n}}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).directive("btnRadio",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,a,i){var r=t.$eval(a.btnRadio);t.$watch(function(){return i.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){o.hasClass(e)||t.$apply(function(){i.$setViewValue(r)})})}}}]).directive("btnCheckbox",["buttonConfig",function(t){var e=t.activeClass||"active",n=t.toggleEvent||"click";return{require:"ngModel",link:function(t,o,a,i){var r=t.$eval(a.btnCheckboxTrue),l=t.$eval(a.btnCheckboxFalse);r=angular.isDefined(r)?r:!0,l=angular.isDefined(l)?l:!1,t.$watch(function(){return i.$modelValue},function(t){angular.equals(t,r)?o.addClass(e):o.removeClass(e)}),o.bind(n,function(){t.$apply(function(){i.$setViewValue(o.hasClass(e)?l:r)})})}}}]),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$transition","$q",function(t,e,n){function o(){function n(){i?(t.next(),o()):t.pause()}a&&e.cancel(a);var r=+t.interval;!isNaN(r)&&r>=0&&(a=e(n,r))}var a,i,r=this,l=r.slides=[],s=-1;r.currentSlide=null,r.select=function(a,i){function c(){r.currentSlide&&angular.isString(i)&&!t.noTransition&&a.$element?(a.$element.addClass(i),a.$element[0].offsetWidth=a.$element[0].offsetWidth,angular.forEach(l,function(t){angular.extend(t,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(a,{direction:i,active:!0,entering:!0}),angular.extend(r.currentSlide||{},{direction:i,leaving:!0}),t.$currentTransition=n(a.$element,{}),function(e,n){t.$currentTransition.then(function(){u(e,n)},function(){u(e,n)})}(a,r.currentSlide)):u(a,r.currentSlide),r.currentSlide=a,s=p,o()}function u(e,n){angular.extend(e,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(n||{},{direction:"",active:!1,leaving:!1,entering:!1}),t.$currentTransition=null}var p=l.indexOf(a);void 0===i&&(i=p>s?"next":"prev"),a&&a!==r.currentSlide&&(t.$currentTransition?(t.$currentTransition.cancel(),e(c)):c())},r.indexOfSlide=function(t){return l.indexOf(t)},t.next=function(){var t=(s+1)%l.length;return r.select(l[t],"next")},t.prev=function(){var t=0>s-1?l.length-1:s-1;return r.select(l[t],"prev")},t.select=function(t){r.select(t)},t.isActive=function(t){return r.currentSlide===t},t.slides=function(){return l},t.$watch("interval",o),t.play=function(){i||(i=!0,o())},t.pause=function(){i=!1,a&&e.cancel(a)},r.addSlide=function(e,n){e.$element=n,l.push(e),1===l.length||e.active?(r.select(l[l.length-1]),1==l.length&&t.play()):e.active=!1},r.removeSlide=function(t){var e=l.indexOf(t);l.splice(e,1),l.length>0&&t.active&&(e>=l.length?r.select(l[e-1]):r.select(l[e]))}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"="}}}]).directive("slide",[function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"="},link:function(t,e,n,o){o.addSlide(t,e),t.$on("$destroy",function(){o.removeSlide(t)}),t.$watch("active",function(e){e&&o.select(t)})}}}]);var dialogModule=angular.module("ui.bootstrap.dialog",["ui.bootstrap.transition"]);dialogModule.controller("MessageBoxController",["$scope","dialog","model",function(t,e,n){t.title=n.title,t.message=n.message,t.buttons=n.buttons,t.close=function(t){e.close(t)}}]),dialogModule.provider("$dialog",function(){var t={backdrop:!0,dialogClass:"modal",backdropClass:"modal-backdrop",transitionClass:"fade",triggerClass:"in",dialogOpenClass:"modal-open",resolve:{},backdropFade:!1,dialogFade:!1,keyboard:!0,backdropClick:!0},e={},n={value:0};this.options=function(t){e=t},this.$get=["$http","$document","$compile","$rootScope","$controller","$templateCache","$q","$transition","$injector",function(o,a,i,r,l,s,c,u,p){function d(t){var e=angular.element("<div>");return e.addClass(t),e}function f(n){var o=this,a=this.options=angular.extend({},t,e,n);this._open=!1,this.backdropEl=d(a.backdropClass),a.backdropFade&&(this.backdropEl.addClass(a.transitionClass),this.backdropEl.removeClass(a.triggerClass)),this.modalEl=d(a.dialogClass),a.dialogFade&&(this.modalEl.addClass(a.transitionClass),this.modalEl.removeClass(a.triggerClass)),this.handledEscapeKey=function(t){27===t.which&&(o.close(),t.preventDefault(),o.$scope.$apply())},this.handleBackDropClick=function(t){o.close(),t.preventDefault(),o.$scope.$apply()},this.handleLocationChange=function(){o.close()}}var g=a.find("body");return f.prototype.isOpen=function(){return this._open},f.prototype.open=function(t,e){var n=this,o=this.options;if(t&&(o.templateUrl=t),e&&(o.controller=e),!o.template&&!o.templateUrl)throw Error("Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.");return this._loadResolves().then(function(t){var e=t.$scope=n.$scope=t.$scope?t.$scope:r.$new();if(n.modalEl.html(t.$template),n.options.controller){var o=l(n.options.controller,t);n.modalEl.children().data("ngControllerController",o)}i(n.modalEl)(e),n._addElementsToDom(),g.addClass(n.options.dialogOpenClass),setTimeout(function(){n.options.dialogFade&&n.modalEl.addClass(n.options.triggerClass),n.options.backdropFade&&n.backdropEl.addClass(n.options.triggerClass)}),n._bindEvents()}),this.deferred=c.defer(),this.deferred.promise},f.prototype.close=function(t){function e(t){t.removeClass(o.options.triggerClass)}function n(){o._open&&o._onCloseComplete(t)}var o=this,a=this._getFadingElements();if(g.removeClass(o.options.dialogOpenClass),a.length>0)for(var i=a.length-1;i>=0;i--)u(a[i],e).then(n);else this._onCloseComplete(t)},f.prototype._getFadingElements=function(){var t=[];return this.options.dialogFade&&t.push(this.modalEl),this.options.backdropFade&&t.push(this.backdropEl),t},f.prototype._bindEvents=function(){this.options.keyboard&&g.bind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.bind("click",this.handleBackDropClick),this.$scope.$on("$locationChangeSuccess",this.handleLocationChange)},f.prototype._unbindEvents=function(){this.options.keyboard&&g.unbind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.unbind("click",this.handleBackDropClick)},f.prototype._onCloseComplete=function(t){this._removeElementsFromDom(),this._unbindEvents(),this.deferred.resolve(t)},f.prototype._addElementsToDom=function(){g.append(this.modalEl),this.options.backdrop&&(0===n.value&&g.append(this.backdropEl),n.value++),this._open=!0},f.prototype._removeElementsFromDom=function(){this.modalEl.remove(),this.options.backdrop&&(n.value--,0===n.value&&this.backdropEl.remove()),this._open=!1},f.prototype._loadResolves=function(){var t,e=[],n=[],a=this;return this.options.template?t=c.when(this.options.template):this.options.templateUrl&&(t=o.get(this.options.templateUrl,{cache:s}).then(function(t){return t.data})),angular.forEach(this.options.resolve||[],function(t,o){n.push(o),e.push(angular.isString(t)?p.get(t):p.invoke(t))}),n.push("$template"),e.push(t),c.all(e).then(function(t){var e={};return angular.forEach(t,function(t,o){e[n[o]]=t}),e.dialog=a,e})},{dialog:function(t){return new f(t)},messageBox:function(t,e,n){return new f({templateUrl:"template/dialog/message.html",controller:"MessageBoxController",resolve:{model:function(){return{title:t,message:e,buttons:n}}}})}}}]}),angular.module("ui.bootstrap.dropdownToggle",[]).directive("dropdownToggle",["$document","$location","$window",function(t){var e=null,n=angular.noop;return{restrict:"CA",link:function(o,a){o.$watch("$location.path",function(){n()}),a.parent().bind("click",function(){n()}),a.bind("click",function(o){o.preventDefault(),o.stopPropagation();var i=a===e;e&&n(),i||(a.parent().addClass("open"),e=a,n=function(o){o&&(o.preventDefault(),o.stopPropagation()),t.unbind("click",n),a.parent().removeClass("open"),n=angular.noop,e=null},t.bind("click",n))})}}}]),angular.module("ui.bootstrap.modal",["ui.bootstrap.dialog"]).directive("modal",["$parse","$dialog",function(t,e){return{restrict:"EA",terminal:!0,link:function(n,o,a){var i,r=angular.extend({},n.$eval(a.uiOptions||a.bsOptions||a.options)),l=a.modal||a.show;r=angular.extend(r,{template:o.html(),resolve:{$scope:function(){return n}}});var s=e.dialog(r);o.remove(),i=a.close?function(){t(a.close)(n)}:function(){angular.isFunction(t(l).assign)&&t(l).assign(n,!1)},n.$watch(l,function(t){t?s.open().then(function(){i()}):s.isOpen()&&s.close()})}}}]),angular.module("ui.bootstrap.pagination",[]).constant("paginationConfig",{boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last"}).directive("pagination",["paginationConfig",function(t){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",maxSize:"=",onSelectPage:"&"},templateUrl:"template/pagination/pagination.html",replace:!0,link:function(e,n,o){function a(t,e,n,o){return{number:t,text:e,active:n,disabled:o}}var i=angular.isDefined(o.boundaryLinks)?e.$eval(o.boundaryLinks):t.boundaryLinks,r=angular.isDefined(o.directionLinks)?e.$eval(o.directionLinks):t.directionLinks,l=angular.isDefined(o.firstText)?o.firstText:t.firstText,s=angular.isDefined(o.previousText)?o.previousText:t.previousText,c=angular.isDefined(o.nextText)?o.nextText:t.nextText,u=angular.isDefined(o.lastText)?o.lastText:t.lastText;e.$watch("numPages + currentPage + maxSize",function(){e.pages=[];var t=1,n=e.numPages;e.maxSize&&e.maxSize<e.numPages&&(t=Math.max(e.currentPage-Math.floor(e.maxSize/2),1),n=t+e.maxSize-1,n>e.numPages&&(n=e.numPages,t=n-e.maxSize+1));for(var o=t;n>=o;o++){var p=a(o,o,e.isActive(o),!1);e.pages.push(p)}if(r){var d=a(e.currentPage-1,s,!1,e.noPrevious());e.pages.unshift(d);var f=a(e.currentPage+1,c,!1,e.noNext());e.pages.push(f)}if(i){var g=a(1,l,!1,e.noPrevious());e.pages.unshift(g);var m=a(e.numPages,u,!1,e.noNext());e.pages.push(m)}e.currentPage>e.numPages&&e.selectPage(e.numPages)}),e.noPrevious=function(){return 1===e.currentPage},e.noNext=function(){return e.currentPage===e.numPages},e.isActive=function(t){return e.currentPage===t},e.selectPage=function(t){!e.isActive(t)&&t>0&&e.numPages>=t&&(e.currentPage=t,e.onSelectPage({page:t}))}}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(t,e){function n(t,n){return t.currentStyle?t.currentStyle[n]:e.getComputedStyle?e.getComputedStyle(t)[n]:t.style[n]}function o(t){return"static"===(n(t,"position")||"static")}var a=function(e){for(var n=t[0],a=e.offsetParent||n;a&&a!==n&&o(a);)a=a.offsetParent;return a||n};return{position:function(e){var n=this.offset(e),o={top:0,left:0},i=a(e[0]);return i!=t[0]&&(o=this.offset(angular.element(i)),o.top+=i.clientTop,o.left+=i.clientLeft),{width:e.prop("offsetWidth"),height:e.prop("offsetHeight"),top:n.top-o.top,left:n.left-o.left}},offset:function(n){var o=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:o.top+(e.pageYOffset||t[0].body.scrollTop),left:o.left+(e.pageXOffset||t[0].body.scrollLeft)}}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position"]).provider("$tooltip",function(){function t(t){var e=/[A-Z]/g,n="-";return t.replace(e,function(t,e){return(e?n:"")+t.toLowerCase()})}var e={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},o={};this.options=function(t){angular.extend(o,t)},this.$get=["$window","$compile","$timeout","$parse","$document","$position",function(a,i,r,l,s,c){return function(a,u,p){function d(t){var e,o;return e=t||f.trigger||p,o=angular.isDefined(f.trigger)?n[f.trigger]||e:n[e]||e,{show:e,hide:o}}var f=angular.extend({},e,o),g=t(a),m=d(void 0),h="<"+g+"-popup "+'title="{{tt_title}}" '+'content="{{tt_content}}" '+'placement="{{tt_placement}}" '+'animation="tt_animation()" '+'is-open="tt_isOpen"'+">"+"</"+g+"-popup>";return{restrict:"EA",scope:!0,link:function(t,e,n){function o(){t.tt_isOpen?g():p()}function p(){t.tt_popupDelay?y=r(v,t.tt_popupDelay):t.$apply(v)}function g(){t.$apply(function(){b()})}function v(){var n,o,a,i;if(t.tt_content){switch($&&r.cancel($),C.css({top:0,left:0,display:"block"}),f.appendToBody?(k=k||s.find("body"),k.append(C)):e.after(C),n=c.position(e),o=C.prop("offsetWidth"),a=C.prop("offsetHeight"),t.tt_placement){case"right":i={top:n.top+n.height/2-a/2+"px",left:n.left+n.width+"px"};break;case"bottom":i={top:n.top+n.height+"px",left:n.left+n.width/2-o/2+"px"};break;case"left":i={top:n.top+n.height/2-a/2+"px",left:n.left-o+"px"};break;default:i={top:n.top-a+"px",left:n.left+n.width/2-o/2+"px"}}C.css(i),t.tt_isOpen=!0}}function b(){t.tt_isOpen=!1,r.cancel(y),angular.isDefined(t.tt_animation)&&t.tt_animation()?$=r(function(){C.remove()},500):C.remove()}var $,y,k,C=i(h)(t);t.tt_isOpen=!1,n.$observe(a,function(e){t.tt_content=e}),n.$observe(u+"Title",function(e){t.tt_title=e}),n.$observe(u+"Placement",function(e){t.tt_placement=angular.isDefined(e)?e:f.placement}),n.$observe(u+"Animation",function(e){t.tt_animation=angular.isDefined(e)?l(e):function(){return f.animation}}),n.$observe(u+"PopupDelay",function(e){var n=parseInt(e,10);t.tt_popupDelay=isNaN(n)?f.popupDelay:n}),n.$observe(u+"Trigger",function(t){e.unbind(m.show),e.unbind(m.hide),m=d(t),m.show===m.hide?e.bind(m.show,o):(e.bind(m.show,p),e.bind(m.hide,g))})}}}}]}).directive("tooltipPopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(t){return t("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(t){return t("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$compile","$timeout","$parse","$window","$tooltip",function(t,e,n,o,a){return a("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",["ui.bootstrap.transition"]).constant("progressConfig",{animate:!0,autoType:!1,stackedTypes:["success","info","warning","danger"]}).controller("ProgressBarController",["$scope","$attrs","progressConfig",function(t,e,n){function o(t){return r[t]}var a=angular.isDefined(e.animate)?t.$eval(e.animate):n.animate,i=angular.isDefined(e.autoType)?t.$eval(e.autoType):n.autoType,r=angular.isDefined(e.stackedTypes)?t.$eval("["+e.stackedTypes+"]"):n.stackedTypes;this.makeBar=function(t,e,n){var r=angular.isObject(t)?t.value:t||0,l=angular.isObject(e)?e.value:e||0,s=angular.isObject(t)&&angular.isDefined(t.type)?t.type:i?o(n||0):null;return{from:l,to:r,type:s,animate:a}},this.addBar=function(e){t.bars.push(e),t.totalPercent+=e.to},this.clearBars=function(){t.bars=[],t.totalPercent=0},this.clearBars()}]).directive("progress",function(){return{restrict:"EA",replace:!0,controller:"ProgressBarController",scope:{value:"=",onFull:"&",onEmpty:"&"},templateUrl:"template/progressbar/progress.html",link:function(t,e,n,o){t.$watch("value",function(t,e){if(o.clearBars(),angular.isArray(t))for(var n=0,a=t.length;a>n;n++)o.addBar(o.makeBar(t[n],e[n],n));else o.addBar(o.makeBar(t,e))},!0),t.$watch("totalPercent",function(e){e>=100?t.onFull():0>=e&&t.onEmpty()},!0)}}}).directive("progressbar",["$transition",function(t){return{restrict:"EA",replace:!0,scope:{width:"=",old:"=",type:"=",animate:"="},templateUrl:"template/progressbar/bar.html",link:function(e,n){e.$watch("width",function(o){e.animate?(n.css("width",e.old+"%"),t(n,{width:o+"%"})):n.css("width",o+"%")})}}}]),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5}).directive("rating",["ratingConfig","$parse",function(t,e){return{restrict:"EA",scope:{value:"="},templateUrl:"template/rating/rating.html",replace:!0,link:function(n,o,a){var i=angular.isDefined(a.max)?n.$eval(a.max):t.max;n.range=[];for(var r=1;i>=r;r++)n.range.push(r);n.rate=function(t){n.readonly||(n.value=t)},n.enter=function(t){n.readonly||(n.val=t)},n.reset=function(){n.val=angular.copy(n.value)},n.reset(),n.$watch("value",function(t){n.val=t}),n.readonly=!1,a.readonly&&n.$parent.$watch(e(a.readonly),function(t){n.readonly=!!t})}}}]),angular.module("ui.bootstrap.tabs",[]).controller("TabsController",["$scope","$element",function(t){var e=t.panes=[];this.select=t.select=function(t){angular.forEach(e,function(t){t.selected=!1}),t.selected=!0},this.addPane=function(n){e.length||t.select(n),e.push(n)},this.removePane=function(n){var o=e.indexOf(n);e.splice(o,1),n.selected&&e.length>0&&t.select(e[e.length>o?o:o-1])}}]).directive("tabs",function(){return{restrict:"EA",transclude:!0,scope:{},controller:"TabsController",templateUrl:"template/tabs/tabs.html",replace:!0}}).directive("pane",["$parse",function(t){return{require:"^tabs",restrict:"EA",transclude:!0,scope:{heading:"@"},link:function(e,n,o,a){var i,r;e.selected=!1,o.active&&(i=t(o.active),r=i.assign,e.$watch(function(){return i(e.$parent)},function(t){e.selected=t}),e.selected=i?i(e.$parent):!1),e.$watch("selected",function(t){t&&a.select(e),r&&r(e.$parent,t)}),a.addPane(e),e.$on("$destroy",function(){a.removePane(e)})},templateUrl:"template/tabs/pane.html",replace:!0}}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position"]).factory("typeaheadParser",["$parse",function(t){var e=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var o=n.match(e);if(!o)throw Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:o[3],source:t(o[4]),viewMapper:t(o[2]||o[1]),modelMapper:t(o[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$document","$position","typeaheadParser",function(t,e,n,o,a,i){var r=[9,13,27,38,40];return{require:"ngModel",link:function(l,s,c,u){var p,d=l.$eval(c.typeaheadMinLength)||1,f=i.parse(c.typeahead),g=l.$eval(c.typeaheadEditable)!==!1,m=e(c.typeaheadLoading).assign||angular.noop,h=angular.element("<typeahead-popup matches='matches' active='activeIdx' select='select(activeIdx)' query='query' position='position'></typeahead-popup>"),v=l.$new();l.$on("$destroy",function(){v.$destroy()});var b=function(){v.matches=[],v.activeIdx=-1},$=function(t){var e={$viewValue:t};m(l,!0),n.when(f.source(v,e)).then(function(n){if(t===u.$viewValue){if(n.length>0){v.activeIdx=0,v.matches.length=0;for(var o=0;n.length>o;o++)e[f.itemName]=n[o],v.matches.push({label:f.viewMapper(v,e),model:n[o]});v.query=t,v.position=a.position(s),v.position.top=v.position.top+s.prop("offsetHeight")}else b();m(l,!1)}},function(){b(),m(l,!1)})};b(),v.query=void 0,u.$parsers.push(function(t){return b(),p?t:(t&&t.length>=d&&$(t),g?t:void 0)}),u.$render=function(){var t={};t[f.itemName]=p||u.$viewValue,s.val(f.viewMapper(v,t)||u.$viewValue),p=void 0},v.select=function(t){var e={};e[f.itemName]=p=v.matches[t].model,u.$setViewValue(f.modelMapper(v,e)),u.$render()},s.bind("keydown",function(t){0!==v.matches.length&&-1!==r.indexOf(t.which)&&(t.preventDefault(),40===t.which?(v.activeIdx=(v.activeIdx+1)%v.matches.length,v.$digest()):38===t.which?(v.activeIdx=(v.activeIdx?v.activeIdx:v.matches.length)-1,v.$digest()):13===t.which||9===t.which?v.$apply(function(){v.select(v.activeIdx)}):27===t.which&&(t.stopPropagation(),b(),v.$digest()))}),o.bind("click",function(){b(),v.$digest()}),s.after(t(h)(v))}}}]).directive("typeaheadPopup",function(){return{restrict:"E",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead.html",link:function(t){t.isOpen=function(){return t.matches.length>0},t.isActive=function(e){return t.active==e},t.selectActive=function(e){t.active=e},t.selectMatch=function(e){t.select({activeIdx:e})}}}}).filter("typeaheadHighlight",function(){function t(t){return t.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(e,n){return n?e.replace(RegExp(t(n),"gi"),"<strong>$&</strong>"):n}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(t){t.put("template/accordion/accordion-group.html",'<div class="accordion-group">\n  <div class="accordion-heading" ><a class="accordion-toggle" ng-click="isOpen = !isOpen" accordion-transclude="heading">{{heading}}</a></div>\n  <div class="accordion-body" collapse="!isOpen">\n    <div class="accordion-inner" ng-transclude></div>  </div>\n</div>')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(t){t.put("template/accordion/accordion.html",'<div class="accordion" ng-transclude></div>')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(t){t.put("template/alert/alert.html","<div class='alert' ng-class='type && \"alert-\" + type'>\n    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n    <div ng-transclude></div>\n</div>\n")}]),angular.module("template/carousel/carousel.html",[]).run(["$templateCache",function(t){t.put("template/carousel/carousel.html",'<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel">\n    <ol class="carousel-indicators" ng-show="slides().length > 1">\n        <li ng-repeat="slide in slides()" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a ng-click="prev()" class="carousel-control left" ng-show="slides().length > 1">&lsaquo;</a>\n    <a ng-click="next()" class="carousel-control right" ng-show="slides().length > 1">&rsaquo;</a>\n</div>\n')}]),angular.module("template/carousel/slide.html",[]).run(["$templateCache",function(t){t.put("template/carousel/slide.html","<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item\" ng-transclude></div>\n")}]),angular.module("template/dialog/message.html",[]).run(["$templateCache",function(t){t.put("template/dialog/message.html",'<div class="modal-header">\n  <h1>{{ title }}</h1>\n</div>\n<div class="modal-body">\n    <p>{{ message }}</p>\n</div>\n<div class="modal-footer">\n  <button ng-repeat="btn in buttons" ng-click="close(btn.result)" class=btn ng-class="btn.cssClass">{{ btn.label }}</button>\n</div>\n')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(t){t.put("template/pagination/pagination.html",'<div class="pagination"><ul>\n  <li ng-repeat="page in pages" ng-class="{active: page.active, disabled: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  </ul>\n</div>\n')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(t){t.put("template/tooltip/tooltip-html-unsafe-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind-html-unsafe="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(t){t.put("template/tooltip/tooltip-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(t){t.put("template/popover/popover.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(t){t.put("template/progressbar/bar.html",'<div class="bar" ng-class=\'type && "bar-" + type\'></div>')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(t){t.put("template/progressbar/progress.html",'<div class="progress"><progressbar ng-repeat="bar in bars" width="bar.to" old="bar.from" animate="bar.animate" type="bar.type"></progressbar></div>')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(t){t.put("template/rating/rating.html",'<span ng-mouseleave="reset()">\n    <i ng-repeat="number in range" ng-mouseenter="enter(number)" ng-click="rate(number)" ng-class="{\'icon-star\': number <= val, \'icon-star-empty\': number > val}"></i>\n</span>\n')}]),angular.module("template/tabs/pane.html",[]).run(["$templateCache",function(t){t.put("template/tabs/pane.html",'<div class="tab-pane" ng-class="{active: selected}" ng-show="selected" ng-transclude></div>\n')}]),angular.module("template/tabs/tabs.html",[]).run(["$templateCache",function(t){t.put("template/tabs/tabs.html",'<div class="tabbable">\n  <ul class="nav nav-tabs">\n    <li ng-repeat="pane in panes" ng-class="{active:pane.selected}">\n      <a ng-click="select(pane)">{{pane.heading}}</a>\n    </li>\n  </ul>\n  <div class="tab-content" ng-transclude></div>\n</div>\n')}]),angular.module("template/typeahead/match.html",[]).run(["$templateCache",function(t){t.put("template/typeahead/match.html",'<a tabindex="-1" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')}]),angular.module("template/typeahead/typeahead.html",[]).run(["$templateCache",function(t){t.put("template/typeahead/typeahead.html",'<ul class="typeahead dropdown-menu" ng-style="{display: isOpen()&&\'block\' || \'none\', top: position.top+\'px\', left: position.left+\'px\'}">\n    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)">\n        <a tabindex="-1" ng-click="selectMatch($index)" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>\n    </li>\n</ul>')}]);

"use strict";angular.module("ngTable",[]).directive("ngTable",["$compile","$q","$parse","$http","ngTableParams",function(a,b,c,d,e){return{restrict:"A",priority:1001,scope:!0,controller:["$scope","$timeout",function(a){var b;return a.params=a.params||{page:1,count:10},a.$watch("params.filter",function(c){return a.params.$liveFiltering?b(c):void 0},!0),b=function(b){return b=angular.extend(a.params,b),a.paramsModel.assign(a.$parent,new e(b)),a.params=angular.copy(b)},a.goToPage=function(c){return c>0&&a.params.page!==c&&a.params.count*(c-1)<=a.params.total?b({page:c}):void 0},a.changeCount=function(a){return b({page:1,count:a})},a.doFilter=function(){return b({page:1})},a.sortBy=function(c){var d,e;if(c.sortable)return d=a.params.sorting&&a.params.sorting[c.sortable]&&"desc"===a.params.sorting[c.sortable],e={},e[c.sortable]=d?"asc":"desc",b({sorting:e})}}],compile:function(b){var d,e;return e=0,d=[],angular.forEach(b.find("td"),function(a){var b;return b=$(a),d.push({id:e++,title:b.data("title")||b.text(),sortable:b.attr("sortable")?b.attr("sortable"):!1,filter:b.attr("filter")?c(b.attr("filter"))():!1,filterData:b.attr("filter-data")?b.attr("filter-data"):null})}),function(b,f,g){var h,i,j;return b.columns=d,h=function(a,b,c){var d,f,g,h,i,j;if(d=11,j=[],i=Math.ceil(b/c),i>1){for(j.push({type:"prev",number:Math.max(1,a-1),active:a>1}),j.push({type:"first",number:1,active:a>1}),g=Math.round((d-5)/2),h=Math.max(2,a-g),f=Math.min(i-1,a+2*g-(a-h)),h=Math.max(2,h-(2*g-(f-h))),e=h;f>=e;)e===h&&2!==e||e===f&&e!==i-1?j.push({type:"more"}):j.push({type:"page",number:e,active:a!==e}),e++;j.push({type:"last",number:i,active:a!==i}),j.push({type:"next",number:Math.min(i,a+1),active:i>a})}return j},b.$parent.$watch(g.ngTable,function(a){return angular.isUndefined(a)?void 0:(b.paramsModel=c(g.ngTable),b.pages=h(a.page,a.total,a.count),b.params=angular.copy(a))},!0),g.showFilter&&b.$parent.$watch(g.showFilter,function(a){return b.show_filter=a}),angular.forEach(d,function(a){var d;if(a.filterData){if(d=c(a.filterData)(b,{$column:a}),!angular.isObject(d)||!angular.isFunction(d.then))throw new Error("Function "+a.filterData+" must be promise");return delete a.filterData,d.then(function(b){return angular.isArray(b)||(b=[]),b.unshift({title:"-",id:""}),a.data=b})}}),f.hasClass("ng-table")?void 0:(b.templates={header:g.templateHeader?g.templateHeader:"ng-table/header.html",pagination:g.templatePagination?g.templatePagination:"ng-table/pager.html"},i=a('<thead ng-include="templates.header"></thead>')(b),j=a('<div ng-include="templates.pagination"></div>')(b),f.filter("thead").remove(),f.prepend(i).addClass("ng-table"),f.after(j))}}}}]);var __hasProp={}.hasOwnProperty;angular.module("ngTable").factory("ngTableParams",function(){var a,b;return a=function(a){return!isNaN(parseFloat(a))&&isFinite(a)},b=function(b){var c,d,e,f,g,h,i,j,k,l;c=["total","counts"],this.page=1,this.count=1,this.counts=[10,25,50,100],this.filter={},this.sorting={};for(d in b)if(i=b[d],d.indexOf("[")>=0){for(g=d.split(/\[(.*)\]/),e="",l=g.reverse(),j=0,k=l.length;k>j;j++)f=l[j],""!==f&&(h=i,i={},i[e=f]=a(h)?parseFloat(h):h);this[e]=angular.extend(this[e]||{},i[e])}else this[d]=a(b[d])?parseFloat(b[d]):b[d];return this.orderBy=function(){var a,b,c,d;c=[],d=this.sorting;for(a in d)__hasProp.call(d,a)&&(b=d[a],c.push(("asc"===b?"+":"-")+a));return c},this.url=function(a){var b,e,g,h;a=a||!1,e=a?[]:{};for(d in this)if(this.hasOwnProperty(d)){if(c.indexOf(d)>=0)continue;if(b=this[d],f=encodeURIComponent(d),"object"==typeof b)for(h in b)angular.isUndefined(b[h])||""===b[h]||(g=f+"["+encodeURIComponent(h)+"]",a?e.push(g+"="+encodeURIComponent(b[h])):e[g]=encodeURIComponent(b[h]));else angular.isFunction(b)||angular.isUndefined(b)||""===b||(a?e.push(f+"="+encodeURIComponent(b)):e[f]=encodeURIComponent(b))}return e},this}}),angular.module("ngTable").run(["$templateCache",function(a){a.put("ng-table/filters/button.html",'<button ng-click="doFilter()" ng-show="filter==\'button\'" class="btn btn-primary btn-block">Filter</button>'),a.put("ng-table/filters/select.html",'<select ng-options="data.id as data.title for data in column.data" ng-model="params.filter[name]" ng-show="filter==\'select\'" class="filter filter-select"></select>'),a.put("ng-table/filters/text.html",'<input type="text" ng-model="params.filter[name]" ng-show="filter==\'text\'" class="input-filter"/>'),a.put("ng-table/header.html",'<tr><th ng-class="{sortable: column.sortable,\'sort-asc\': params.sorting[column.sortable]==\'asc\', \'sort-desc\': params.sorting[column.sortable]==\'desc\'}" ng-click="sortBy(column)" ng-repeat="column in columns" class="header"><div>{{column.title}}</div></th></tr><tr ng-show="show_filter"><th ng-repeat="column in columns" class="filter"><form ng-submit="doFilter()"><input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/><div ng-repeat="(name, filter) in column.filter"><div ng-include="\'ng-table/filters/\' + filter + \'.html\'"></div></div></form></th></tr>'),a.put("ng-table/pager.html",'<div><ul class="pagination ng-cloak"><li ng-class="{\'disabled\': !page.active}" ng-repeat="page in pages" ng-switch="page.type"><a ng-switch-when="prev" ng-click="goToPage(page.number)" href="">«</a><a ng-switch-when="first" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="page" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="more" ng-click="goToPage(page.number)" href="">…</a><a ng-switch-when="last" ng-click="goToPage(page.number)" href="">{{page.number}}</a><a ng-switch-when="next" ng-click="goToPage(page.number)" href="">»</a></li></ul><div ng-show="params.counts.length" class="btn-group pull-right"><button ng-repeat="count in params.counts" type="button" ng-class="{\'active\':params.count==count}" ng-click="changeCount(count)" class="btn btn-mini">{{count}}</button></div></div>')}]);

angular.module('cms.entity', ['ngTable', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'cms.table.filters', 'cms.tinemce'])
    /**
     * Директива отображения и управления списком сущностей
     * @requires  1.1+ version
     */
    .directive('entityList', ['$http', '$resource', 'ngTableParams', '$parse', '$dialog', '$templateCache', '$rootScope', function($http, $resource, ngTableParams, $parse, $dialog, $templateCache, $rootScope) {

        return {
            restrict: 'A',
            scope: true,

            template: function(tElement, tAttrs) {
                var columnsGetter = $parse(tAttrs.columns);
                var template = '';
                columns = columnsGetter({});

                template += '<div loading-container="loading">';

                template += '<div class="btn-group">';
                    if (!!tAttrs.btnAdd) {
                        template += '<a class="btn btn-success" href="#!'+(tAttrs.url + '/').replace('//', '/')+'create/"><i class="icon-plus icon-white"></i> '+tAttrs.btnAdd+'</a>';
                    }
                template += '</div>';

                template += '<div ng-show="itemList.length > 0"><table ng-table="tableParams" class="table table-hover table-striped">'+
                    '<tr ng-repeat="item in itemList">';

                var booleanColumns = [];

                for(var i in columns) {
                    if (!columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if (columns[i].isBoolean === true) {
                        columns[i].__name = i;
                        booleanColumns.push(columns[i]);
                        continue;
                    }

                    var modifier = '';

                    // Подключаем модификатор отображения
                    if (!!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier;
                    }

                    if (!!columns[i].by && !!columns[i].modifier) {
                        modifier = '|'+columns[i].modifier+':\''+columns[i].by+'\'';
                    }

                    var item = '';

                    if (modifier != '') {
                        item = '<span ng-bind-html-unsafe="item.'+i+modifier+'"></span>';
                    } else {
                        item = '{{ item.'+i+' }}';
                    }

                    template += '<td data-title="'+columns[i].title+'">'+item+'</td>';
                }

                // Добавляем колонку с логичесиким переключателями
                if (booleanColumns.length > 0) {
                    template += '<td data-title=" ">';

                    for(var i = 0; i < booleanColumns.length; i++) {
                        var column = booleanColumns[i];

                        template += '<label class="checkbox"><input type="checkbox" ng-model="item.'+column.__name+'" /> '+column.title+'</label';
                    }

                    template += '</td>';
                }

                template += '<td data-title="Действия" width="200">';
                    var buttons = $parse(tAttrs.buttons)();
                    if (!!buttons && buttons.length > 0) {
                        // Дополнительные кнопки
                        for(var i = 0; i < buttons.length; i++) {
                            var btn = buttons[i];
                            template += '<a class="btn btn-info btn-small" tooltip="'+btn.tooltip+'" ng-href="#!'+btn.url.replace(/\{id\}/, '{{item.id}}')+'"><i class="icon-white icon-'+btn.icon+'"></i></a>&nbsp;&nbsp;';
                        }
                    }

                    template += '<a class="btn btn-warning btn-small" tooltip="Редактировать" ng-href="#!'+((tAttrs.url + '/').replace('//', '/') + 'edit/')+'{{item.id}}"><i class="icon-white icon-edit"></i></a>&nbsp;&nbsp;';
                    template += '<button class="btn btn-danger btn-small" tooltip="Удалить" ng-click="removeEntity(item)"><i class="icon-white icon-trash"></i></button>';
                template += '</td>';

                template += '</tr></table></div>';

                template += '<div class="well well-small" ng-show="itemList.length == 0 && !loading">Список пуст</div>';

                template += '</div>';

                return template;
            },

            controller: function($scope, $element, $attrs) {
                var columnsGetter = $parse($attrs.columns);
                $scope.columns = columnsGetter({});

                $scope.tableParams = new ngTableParams({
                    page: 1,
                    total: 0,
                    count: parseInt($attrs.count, 10)
                });

                $scope.itemList = [];

                // Обновление списка
                $scope.update = function() {
                    $scope.loading = true;

                    $scope.entityResource.query($scope.tableParams.url(), function(data) {
                        $scope.loading = false;
                        $scope.itemList = data.items;
                        $scope.tableParams.total = data.items_count;
                    }, function(response, errorData){
                        if (response.status == 0) {
                            // Загрузка была отменена
                            return;
                        }

                        console.log(response);
                        $scope.loading = false;
                        $scope.itemList = [];
                        $scope.tableParams.total = 0;

                        var msg = '';

                        try {
                            msg = response.data[0].message;
                        } catch(e) {};

                        $rootScope.$broadcast('ntf-error', response.status + ' - Произошла ошибка при загрузке списка. ' + msg, 5000);
                    });
                }

                // Наблюдаем за параметрами таблицы
                $scope.$watch('tableParams', $scope.update, true);
                $scope.$on('needUpdate', $scope.update, true);

                // Наблюдаем за изменением ресурсов (нужно для чекбоксов в таблице)
                var hasBoolean = false;
                for(var i in $scope.columns) {
                    if (!$scope.columns.hasOwnProperty(i)) {
                        continue;
                    }

                    if ($scope.columns[i].isBoolean === true) {
                        hasBoolean = true;
                        break;
                    }
                }

                if (hasBoolean) {
                    $scope.$watch('itemList', function(itemList, oldList) {
                        for(var i = 0; i < oldList.length; i++) {
                            if (itemList[i] === undefined || itemList[i].id != oldList[i].id) {
                                continue;
                            }

                            if (!angular.equals(itemList[i], oldList[i])) {
                                $http.post(($attrs.entityList + '/').replace('//', '/') + 'set/' + itemList[i].id, itemList[i])
                                    .success(function(response) {
                                        $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                    })
                                    .error(function(response) {
                                        console.log(response);
                                        $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                                    });
                            }
                        }
                    }, true);
                }

                /**
                 * Удаление сущности
                 */
                $scope.removeEntity = function(item) {
                    var title = 'Удаление элемента';
                    var msg = 'Вы уверены, что хотите удалить элемент?';
                    var btns = [{result:'cancel', label: 'Отмена'}, {result:'ok', label: 'Удалить', cssClass: 'btn-danger'}];

                    $dialog.messageBox(title, msg, btns).open().then(function(result) {
                        if (result == 'ok') {
                            $scope.entityResource.remove({id:item.id}, function() {
                                $scope.$broadcast('needUpdate');
                            }, function() {
                                alert('Произошла ошибка при удалении');
                            });
                        }
                    });
                }

                /**
                 * Событие успешного добавления/редактировния элемента
                 */
                $scope.$on('entityFormClosed', function() {
                    $scope.$broadcast('needUpdate');
                });
            },

            compile: function($element, $attrs) {
                $element.addClass('entity-list');

                if (!$element.attr('id')) {
                    $element.attr('id', 'entity-list-'+parseInt(Math.random()*Math.random().toString().replace(/[^0-9]|/g, ''), 10));
                }

                return function($scope, $element, attrs) {
                    var url = $attrs.entityList;

                    if (url.indexOf('?') === -1) {
                        url = (url + '/').replace('//', '/');
                        if (url.indexOf(':id') === -1) {
                            url += ':id';
                        }
                    } else {
                        if (url.indexOf(':id') === -1) {
                            url = url.replace('?', '/:id?').replace('//', '/');
                        }
                    }

                    $scope.entityResource = $resource(url, {
                            id: '@id'
                        }, {
                            get:    {method:'GET'},
                            save:   {method:'POST'},
                            query:  {method:'GET', isArray:false},
                            remove: {method:'DELETE'}
                        });
                }
            }
        };
    }])

    /**
     * Форма добавления/редактирования сущности
     */
    .directive('entityForm', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            restrict: 'A',
            scope: true,

            controller: function($scope, $element, $attrs) {
                $scope.submit = function(){
                    $scope.errors = {};
                    $scope.loading = true;

                    $http[$scope.method]($scope.actionUrl, $element.serializeObject())
                        .success(function(response) {
                            $scope.loading = false;

                            if (response.success) {
                                $rootScope.$broadcast('ntf-success', 'Элемент успешно сохранен', 5000);
                                $scope.$emit('entityFormClosed');
                            } else {
                                $rootScope.$broadcast('ntf-error', 'Форма содержит ошибки заполнения', 5000);
                                if (!!response.errors) {
                                    $scope.errors = response.errors;
                                }
                            }
                        })
                        .error(function(response) {
                            $scope.loading = false;

                            console.log(response);
                            $rootScope.$broadcast('ntf-error', 'Произошла ошибка');
                        });
                }

                /**
                 * Проверяет наличие ошибки в поле
                 */
                $scope.hasError = function(name) {
                    var errors = $scope.errors;

                    if (!!errors[name] && errors[name].length > 0) {
                        return true;
                    }

                    if (name.indexOf('[') !== -1) {
                        name = name.replace(/[0-9\[\]]/g, '');

                        for(var key in errors) {
                            if (!errors.hasOwnProperty(key)) {
                                continue;
                            }

                            if (key.indexOf(name+'[') === 0) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            },

            compile: function($element) {
                $element.find('input[type="text"], input[type="email"], input[type="password"]').each(function() {
                    $(this).attr({
                        popover: '{{errors[\'' + $(this).attr('name') + '\'] |el_list }}',
                        'popover-placement': 'right',
                        'popover-trigger': 'focus'
                    });

                    var $controlGroup = $(this).closest('.control-group');
                    $controlGroup.attr('ng-class', '{error: hasError(\'' + $(this).attr('name') + '\')}');
                });

                $element.wrapInner('<div loading-container="loading"></div>');

                return function($scope, $element, attrs) {
                    $scope.actionUrl = attrs.entityForm;
                    $scope.method = attrs.method.toString().toLowerCase();
                    $scope.errors = {};
                    $scope.loading = false;

                    $element.find('input[type="text"], input[type="email"], input[type="password"]').on('change', function() {
                        var name = $(this).attr('name');

                        $scope.$apply(function() {
                            $scope.errors[name] = [];
                        });
                    });
                }
            }
        }
    }])

    /**
     * Тип полей - список текстовых полей
     */
    .directive('formCollectionText', ['$compile', function($compile) {
        var removeButtonTpl = '<div class="btn btn-mini btn-link btn-remove" ng-click="removeElement($event)"><i class="icon-remove"></i></div>';

        return {
            restrict: 'A',
            scope: true,
            priority: -1000,

            controller: function($scope, $element, $attrs) {
                var $controls = $element.find('.controls');
                var prototype = $attrs.prototype + removeButtonTpl;

                $scope.nextIndex = $controls.find('input').length + 1;

                /**
                 * Добавление элемента
                 */
                $scope.addElement = function() {
                    var input = prototype.replace(/__name__/g, $scope.nextIndex);
                    $controls.children().last().before($compile(input)($scope));

                    $scope.nextIndex = $controls.children('input').length;
                    $scope.updateNames();
                };

                /**
                 * Удаление элемента
                 */
                $scope.removeElement = function($event) {
                    var $removeButton = angular.element($event.target);

                    if (!$removeButton.is('.btn')) {
                        $removeButton = $removeButton.closest('.btn');
                    }

                    $removeButton.add($removeButton.prev('input')).remove();
                    $scope.updateNames();
                };

                /**
                 * Обновление имен элементов
                 */
                $scope.updateNames = function() {
                    var i = 0;
                    var $fInput = $controls.children('input').first();
                    if ($fInput.length == 0) {
                        return;
                    }

                    var name = $fInput.attr('name').replace(/[0-9\[\]]/g, '');

                    $controls.children('input').each(function() {
                        var $this = $(this);
                        $this.attr('name', name+'['+i+']');
                        $this.attr('id', name+'_'+i);

                        $this.attr({
                            popover: '{{errors[\'' + $this.attr('name') + '\'] |el_list }}',
                            'popover-placement': 'right',
                            'popover-trigger': 'focus'
                        });

                        $compile($this)($scope);

                        i++;
                    });
                };
            },

            compile: function($element, $attrs) {
                if (!$attrs.prototype) {
                    $element.find('.controls').html('<span class="label label-important">Empty prototype</span>');
                }

                $element.find('.controls input').after(removeButtonTpl);
                $element.find('.btn-add').attr('ng-click', 'addElement()');

                return function($scope) {
                    $scope.updateNames();
                }
            }
        }
    }])

    /**
     * Контейнер отображающий статус загрузки
     */
    .directive('loadingContainer', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $('<div class="preloader"></div>').appendTo(element);
                $(element).addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggle(value);
                });
            }
        };
    })
;

/**
 * Модуль уведомлений работает через $rootScope.$broadcast
 */
angular.module('cms.notification', [])
    .controller('NotificationCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
        /**
         * Список сообщений
         */
        $scope.alerts = [];

        var counter = 0;

        /**
         * Добавление алерта
         */
        $scope.addAlert = function(msg, type, delay) {
            $scope.$apply(function() {
                $scope.alerts.push({id:counter, msg:msg, type:type});
                counter++;
            });

            if (delay !== undefined && delay > 1000) {
                var toClose = counter - 1;

                setTimeout(function() {
                    for(var i = 0; i < $scope.alerts.length; i++) {
                        if ($scope.alerts[i].id == toClose) {
                            $scope.$apply(function() {
                                $scope.closeAlert(i);
                            });
                            break;
                        }
                    }
                }, delay);
            }
        };

        /**
         * Закрытие алерта
         */
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        /**
         * Событие добавления уведомления
         */
        $rootScope.$on('notification', function(event, notification) {
            setTimeout(function() {
                $scope.addAlert(notification.msg, notification.type, notification.delay);
            }, 13);
        });

        /**
         * Краткие способы доабвления уведомлений
         */
        $rootScope.$on('ntf-success', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'success', delay);
            }, 13);
        });

        $rootScope.$on('ntf-info', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'info', delay);
            }, 13);
        });

        $rootScope.$on('ntf-warning', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'warning', delay);
            }, 13);
        });

        $rootScope.$on('ntf-error', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'error', delay);
            }, 13);
        });
    }])
;

angular.module('cms.selectpicker', []).directive('selectpicker', ['$timeout', function ($timeout) {
    var NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w\d]*)|(?:\(\s*([\$\w][\$\w\d]*)\s*,\s*([\$\w][\$\w\d]*)\s*\)))\s+in\s+(.*)$/;
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function postLink(scope, element, attrs, controller) {
            var options = scope.$eval(attrs.selectpicker) || {};

            $timeout(function () {
                element.selectpicker(options);
                element.next().removeClass('ng-scope');
            });

            if (controller) {
                scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        element.selectpicker('refresh');
                    }
                });
            }
        }
    };
}]);

/**
 * Фильтры для удобного отображения данных в строках таблицы
 */
angular.module('cms.table.filters', [])
/**
     * Фильтр отображения массива в табличном представлении
     */
    .filter('el_list', [function() {
        return function(value, name) {
            if (!$.isArray(value)) {
                return value;
            }

            var arr = [];

            for(var i = 0; i < value.length; i++) {
                if (!name) {
                    arr.push(value[i]);
                } else {
                    arr.push(value[i][name]);
                }
            }

            return arr.join(', ');
        }
    }])

    /**
     * Фильтр отображения объекта
     */
    .filter('el_object', [function() {
        return function(value, name) {
            return !!value && value.hasOwnProperty(name) ? value[name] : '';
        }
    }])

    /**
     * Отображение e-mail адреса
     */
    .filter('el_email', [function() {
        return function(value) {
            return '<a href="mailto:'+value+'" target="_self">'+value+'</a>';
        }
    }])

    /**
     * Ссылка на страницу CMS
     */
    .filter('el_cms_link', ['$filter', function($filter) {
        return function(value, prefix) {
            var url = prefix + value;

            return '<a href="'+url+'" title="'+url+'" target="_self">' + $filter('characters')(value, 50) + '</a>';
        }
    }])

    /**
     * Одиночное изображение
     */
    .filter('el_image', [function() {
        return function(value) {
            if (!!value && !!value.size && !!value.size.cms) {
                return '<img src="'+value.size.cms+'" title="'+value.title+'" />';
            } else {
                return '---';
            }
        }
    }])
;

/**
 * TinyMCE для админки
 */
angular.module('cms.tinemce', [])
    /**
     * Инициализация tinyMCE
     */
    .directive('tinymce', ['$timeout', function($timeout) {
        var generatedIds = 0;

        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                attrs.$set('id', 'tiny-mce-' + generatedIds++);

                $timeout(function() {
                    var options = angular.extend(window.tinyMCEConfig, {
                    });

                    initTinyMCE(options, $element);
                });
            }
        };
    }])
;

angular.module('cms.tree', ['cms.utils'])
    /**
     * Дерево структуры
     */
    .directive('nodeTree', ['$http', '$parse', '$timeout', '$rootScope', function($http, $parse, $timeout, $rootScope) {
        // Предзагруженные ноды
        var __preload = {};

        return {
            restrict: 'A',
            scope: {
                url: '@',
                root: '@nodeTree',
                prefix: '@'
            },

            template: '<li ng-repeat="item in items" node-tree-item="item" prefix="prefix" url="url" class="node-tree-item" ng-class="{\'current\':item.current}"></li>',

            compile: function($element) {
                var $link = $element.children('a.link');

                return function($scope, $element, attrs) {
                    $scope.items = [];
                    $scope.autoExpand = $parse(attrs.autoExpand)($scope);

                    var preload = $parse(attrs.preload)($scope);
                    if (!!preload) {
                        __preload = angular.extend(__preload, preload);
                    }

                    /**
                     * Обновленеи списка пунктов
                     */
                    $scope.updateItems = function(items) {
                        $scope.items = items;

                        $timeout(function() {
                            if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                                $scope.$broadcast('autoExpand', $scope.autoExpand);
                            }
                            $scope.$emit('loaded');
                        });
                    };

                    if (!!__preload[$scope.root]) {
                        $scope.updateItems(__preload[$scope.root]);
                    } else {
                        $http({
                                method: 'GET',
                                url: $scope.url,
                                params: {parent:$scope.root, count:1000, page:1}
                            })
                            .success(function(response) {
                                $scope.updateItems(response.items);
                            })
                            .error(function(response) {
                                console.log(response);
                                $rootScope.$broadcast('ntf-error', 'Произошла ошибка', 5000);
                            });
                    }
                };
            }
        }
    }])

    /**
     * Элемент дерева
     */
    .directive('nodeTreeItem', ['$compile', function($compile) {
        return {
            restrict: 'A',
            scope: {
                item: '=nodeTreeItem',
                prefix: '=',
                url: '='
            },
            template: '<i class="expand-btn" ng-click="toggle()" ng-class="icon()"></i>'
                + '<a href="{{ prefix }}{{ item.url }}" class="link" target="_self" title="[{{ item.id }}] {{ item.title }}">{{ item.title|characters:gettl(item) }}</a>',
            compile: function() {
                return function($scope, $element, attrs) {
                    $scope.expanded = false;
                    $scope.loading = false;

                    if (!$scope.item.title) {
                        $scope.item.title = '<Без названия>';
                    }

                    $scope.$on('autoExpand', function(event, autoExpand) {
                        if (event.targetScope === $scope) {
                            return;
                        }

                        $scope.autoExpand = autoExpand;
                        if ($.inArray($scope.item.id, $scope.autoExpand) != -1) {
                            $scope.toggle();
                            $scope.item.current = true;
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                /**
                 * Получение иконки папки
                 */
                $scope.icon = function() {
                    if ($scope.loading) {
                        return 'icon-refresh infiinite-rotate';
                    }

                    if ($scope.item.isRoot) {
                        return 'icon-home';
                    }

                    if (!$scope.item.isParent) {
                        return 'icon-file';
                    }

                    if ($scope.expanded) {
                        return 'icon-folder-open';
                    } else {
                        return 'icon-folder-close';
                    }
                };

                /**
                 * Генерация дерева
                 */
                $scope.createTree = function() {
                    $scope.expanded = true;
                    $scope.loading = true;
                    $scope.$tree = angular.element('<ul class="node-tree unstyled" node-tree="{{ item.id }}" prefix="{{ prefix }}" url="{{ url }}" ng-show="items.length > 0"></ul>');

                    $element.append($scope.$tree);
                    $compile($scope.$tree)($scope);

                    $scope.$on('loaded', function(event) {
                        event.stopPropagation();
                        $scope.loading = false;
                        if (!!$scope.autoExpand && $scope.autoExpand.length > 0) {
                            $scope.$broadcast('autoExpand', $scope.autoExpand);
                        }
                    });
                };

                /**
                 * Переключение состояния дерева
                 */
                $scope.toggle = function() {
                    if (!$scope.$tree) {
                        $scope.createTree();
                        return;
                    }

                    if ($scope.expanded) {
                        $scope.$tree.slideUp(300);
                    } else {
                        $scope.$tree.slideDown(300);
                    }

                    $scope.expanded = !$scope.expanded;
                };

                /**
                 * Возвращает длину строки для обрезки названия раздела
                 */
                $scope.gettl = function(item) {
                    var len = 165;

                    len = len - item.level * 15;

                    if (len < 50) {
                        len = 50;
                    }

                    return len;
                };
            }
        }
    }])
;

/**
 * Полезные инструменты
 */
angular.module('cms.utils', [])
    /**
     * Фильтр транслитерации
     */
    .filter('translit', [function(){
        var List = {
                'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
                'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
                'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
                'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
                'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
                'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
                'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
                'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
                'Я':'Ya','я':'ya'
            },
            regular = '',
            k;

        for (k in List){
            regular += k;
        }

        regular = new RegExp('[' + regular + ']', 'g');

        k = function(a){
            return a in List ? List[a] : '';
        };

        return function(input){
            if (!!input){
                return input.replace(regular, k);
            }else{
                return '';
            }
        };
    }])

    /**
     * Удобочитаемый размер файла
     */
    .filter('filesize', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '-';
            }

            if (typeof precision === 'undefined') {
                precision = 1;
            }

            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
            var number = Math.floor(Math.log(bytes) / Math.log(1024));

            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })

    /**
     * Обрезание строки по кол-ву символов
     */
    .filter('characters', function () {
        return function (input, chars, breakOnWord) {
            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length >= chars) {
                input = input.substring(0, chars);

                if (!breakOnWord) {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1) {
                        input = input.substr(0, lastspace);
                    }
                }else{
                    while(input.charAt(input.length-1) == ' '){
                        input = input.substr(0, input.length -1);
                    }
                }
                return input + '...';
            }
            return input;
        };
    })

    /**
     * Обрезание строки по кол-ву слов
     */
    .filter('words', function () {
        return function (input, words) {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input) {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words) {
                    input = inputWords.slice(0, words).join(' ') + '...';
                }
            }
            return input;
        };
    })

    /**
     * Директива преобразования в системное имя
     */
    .directive('nodename', ['$filter', function($filter) {
        var getValue = function(value) {
            value = value.toString().toLowerCase();
            value = $filter('translit')(value);
            value = value.replace(/( |\/)/g, '-');
            value = value.replace(/[^a-zA-Z_\-0-9]/g, '');

            return value;
        };

        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                if (!$element.is('input')) {
                    return function() {};
                }

                return function($scope, $element, attrs) {
                    var $source = $element.closest('form').find('*[name="'+attrs.nodename+'"]');

                    if ($source.length == 0) {
                        return;
                    }

                    $source.on('keyup click', function() {
                        var value = $(this).val();
                        $element.val(getValue(value));
                    });

                    $element.on('keyup', function() {
                        $source.off('keyup click');
                    });
                }
            }
        }
    }])
;

/**
 * Переопределение табов для запоминания в адресной строке
 * Так же взаимодействует с директивой entityList инициируя обновление списка
 */
angular.module('ui.bootstrap.tabs', [])
    /**
     * Контроллер переключения табов
     */
    .controller('TabsController', ['$scope', '$element', '$location', '$timeout', function($scope, $element, $location, $timeout) {
        var panes = this.panes = $scope.panes = [];

        /**
         * Делает вкладку активной
         * pane - scope вкладки
         * changeLocation - изменять ли хеш текущей страницы
         * update - инициировать ли обновление entityList
         */
        this.select = $scope.select = function selectPane(pane, changeLocation, update) {
            angular.forEach(panes, function(pane) {
                pane.selected = false;
            });

            pane.selected = true;

            if (changeLocation !== false) {
                $location.path(pane.url);
            }

            if (update === true && $.isFunction(pane.updateEntityList)) {
                pane.updateEntityList();
            }
        };

        this.addPane = function addPane(pane, index) {
            if ($.inArray(pane, panes) === -1) {
                panes.splice(index, 0, pane);
            }

            if (panes.length == 0 || $location.path() == pane.url) {
                $scope.select(pane, $location.path() == pane.url);
            }
        };

        this.hidePane = $scope.hidePane = function hidePane(pane) {
            var index = panes.indexOf(pane);
            pane.hidden = true;
            if (panes.length > 0) {
                var newPane = pane.getParent();
                $scope.select(newPane ? newPane : panes[index < panes.length ? index : index-1], true, true);
            }
        }

        this.removePane = function removePane(pane) {
            var index = panes.indexOf(pane);
            panes.splice(index, 1);
            if (pane.selected && panes.length > 0) {
                $scope.select(panes[index < panes.length ? index : index-1]);
            }
        };
    }])

    /**
     * Контейнер с табами
     */
    .directive('tabs', function() {
        return {
            restrict: 'EA',
            transclude: true,
            scope: {
            },
            controller: 'TabsController',
            template:  "<div class=\"tabbable\">\n" +
                "  <ul class=\"nav nav-tabs\">\n" +
                "    <li ng-repeat=\"pane in panes\" ng-class=\"{active:pane.selected}\" ng-show=\"!pane.hidden\">\n" +
                "      <a ng-click=\"select(pane)\">{{pane.heading}}</a>" +
                "    </li>\n" +
                "  </ul>\n" +
                "  <div class=\"tab-content\" ng-transclude></div>\n" +
                "</div>",
            replace: true
        };
    })

    /**
     * Обычная вкладка.
     * Может содержать entityList
     */
    .directive('pane', ['$parse', '$location', function($parse, $location) {
        return {
            require: '^tabs',
            restrict: 'EA',
            transclude: true,
            scope:{
                heading:'@',
            },
            link: function(scope, element, attrs, tabsCtrl) {
                var getSelected, setSelected;
                scope.selected = false;

                if (!!attrs.url) {
                    scope.url = attrs.url;

                    if (scope.url[scope.url.length - 1] != '/') {
                        scope.url = scope.url + '/';
                    }

                    if (scope.url[0] != '/') {
                        scope.url = '/' + scope.url;
                    }
                }

                if (attrs.active) {
                    getSelected = $parse(attrs.active);
                    setSelected = getSelected.assign;
                    scope.$watch(
                        function watchSelected() {return getSelected(scope.$parent);},
                        function updateSelected(value) {scope.selected = value;}
                    );
                    scope.selected = getSelected ? getSelected(scope.$parent) : false;
                }

                scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select(scope, false);
                    }

                    if(setSelected) {
                        setSelected(scope.$parent, selected);
                    }
                });

                tabsCtrl.addPane(scope, element.index());

                scope.location = $location;
                scope.$watch('location.path()', function(path) {
                    if (path == scope.url) {
                        scope.selected = true;
                    }
                });

                scope.$on('$destroy', function() {
                    tabsCtrl.removePane(scope);
                });

                scope.updateEntityList = function() {
                    element.find('*[entity-list]').each(function() {
                        $(this).scope().$broadcast('needUpdate');
                    });
                };
            },
            templateUrl: 'template/tabs/pane.html',
            replace: true
        };
    }])

    /**
     * Лениво-загружаемая вкладка
     * Обычно содержит формы добавления/редактирования
     *
     * parent - индекс родительской вкладки. На нее будет переключние в случае закрытия формы
     *
     * Пример:
     *     <ghostpane heading="Новая группа" url="/usergroup/create/" parent="3" template="{{ path('cms_rest_usergroup_form_add') }}"></ghostpane>
     */
    .directive('ghostpane', ['$parse', '$location', '$templateCache', '$timeout', function($parse, $location, $templateCache, $timeout) {
        return {
            require: '^tabs',
            restrict: 'EA',
            transclude: true,

            scope:{
                heading: '@',
                template: '@'
            },

            link: function($scope, element, attrs, tabsCtrl) {
                $scope.selected = false;
                $scope.hidden = true;
                $scope.location = $location;
                $scope.parent = (attrs.parent === undefined ? null : parseInt(attrs.parent, 10));

                $scope.url = attrs.url;

                if ($scope.url[$scope.url.length - 1] != '/' && $scope.url.indexOf('{id}') == -1) {
                    $scope.url = $scope.url + '/';
                }

                if ($scope.url[0] != '/') {
                    $scope.url = '/' + $scope.url;
                }

                $scope.urlRegexp = $scope.url.replace('{id}', '(\\d+)');
                $scope._url = $scope.url;

                $scope._heading = $scope.heading;

                $scope._template = $scope.template;
                $scope.template = '';
                $scope.getTemplateUrl = function(id) {
                    return $scope._template.replace('{id}', id);
                };
                $templateCache.put($scope._template, 'default empty');

                $scope.refresh = function(id) {
                    var tpl = $scope.template;

                    $scope.template = '';
                    $scope.url = $scope._url.replace('{id}', id);
                    $scope.heading = $scope._heading.replace('{id}', id);
                    $timeout(function() {
                        $templateCache.remove($scope.getTemplateUrl(id));
                        $scope.template = $scope.getTemplateUrl(id);
                    });
                };

                $scope.getParent = function() {
                    return tabsCtrl.panes[$scope.parent];
                };

                $scope.$watch('location.path()', function(path) {
                    var matcher = new RegExp($scope.urlRegexp);

                    if (matcher.test(path)) {
                        $scope.selected = true;
                        var id = path.match(matcher)[1];

                        if ($scope.hidden || $scope.lastId != id) {
                            $scope.refresh(id);
                            $scope.lastId = id;
                        }

                        $scope.hidden = false;
                    }
                });

                $scope.$watch('selected', function(selected) {
                    if(selected) {
                        tabsCtrl.select($scope, false);
                    }
                });

                $scope.$on('$destroy', function() {
                    tabsCtrl.removePane($scope);
                });

                $scope.$on('entityFormClosed', function(event) {
                    tabsCtrl.hidePane($scope);
                });

                tabsCtrl.addPane($scope, element.index());
            },

            template: "<div class=\"tab-pane\" ng-class=\"{active: selected}\" ng-show=\"selected && !hidden\" ng-include=\"template\"></div>",
            replace: true
        };
    }])
;

/**
 * The following features are still outstanding: animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html tooltips, and selector delegation.
 */
angular.module( 'ui.bootstrap.tooltip', [ 'ui.bootstrap.position' ] )

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
.provider( '$tooltip', function () {
  // The default options tooltip and popover.
  var defaultOptions = {
    placement: 'top',
    animation: true,
    popupDelay: 0
  };

  // Default hide triggers for each show trigger
  var triggerMap = {
    'mouseenter': 'mouseleave',
    'click': 'click',
    'focus': 'blur'
  };

  // The options specified to the provider globally.
  var globalOptions = {};

  /**
   * `options({})` allows global configuration of all tooltips in the
   * application.
   *
   *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
   *     // place tooltips left instead of top by default
   *     $tooltipProvider.options( { placement: 'left' } );
   *   });
   */
    this.options = function( value ) {
        angular.extend( globalOptions, value );
    };

  /**
   * This is a helper function for translating camel-case to snake-case.
   */
  function snake_case(name){
    var regexp = /[A-Z]/g;
    var separator = '-';
    return name.replace(regexp, function(letter, pos) {
      return (pos ? separator : '') + letter.toLowerCase();
    });
  }

  /**
   * Returns the actual instance of the $tooltip service.
   * TODO support multiple triggers
   */
  this.$get = [ '$window', '$compile', '$timeout', '$parse', '$document', '$position', function ( $window, $compile, $timeout, $parse, $document, $position ) {
    return function $tooltip ( type, prefix, defaultTriggerShow ) {
      var options = angular.extend( {}, defaultOptions, globalOptions );

      /**
       * Returns an object of show and hide triggers.
       *
       * If a trigger is supplied,
       * it is used to show the tooltip; otherwise, it will use the `trigger`
       * option passed to the `$tooltipProvider.options` method; else it will
       * default to the trigger supplied to this directive factory.
       *
       * The hide trigger is based on the show trigger. If the `trigger` option
       * was passed to the `$tooltipProvider.options` method, it will use the
       * mapped trigger from `triggerMap` or the passed trigger if the map is
       * undefined; otherwise, it uses the `triggerMap` value of the show
       * trigger; else it will just use the show trigger.
       */
      function setTriggers ( trigger ) {
        var show, hide;

        show = trigger || options.trigger || defaultTriggerShow;
        if ( angular.isDefined ( options.trigger ) ) {
          hide = triggerMap[options.trigger] || show;
        } else {
          hide = triggerMap[show] || show;
        }

        return {
          show: show,
          hide: hide
        };
      }

      var directiveName = snake_case( type );
      var triggers = setTriggers( undefined );

      var template =
        '<'+ directiveName +'-popup '+
          'title="{{tt_title}}" '+
          'content="{{tt_content}}" '+
          'placement="{{tt_placement}}" '+
          'animation="tt_animation()" '+
          'is-open="tt_isOpen"'+
          '>'+
        '</'+ directiveName +'-popup>';

      return {
        restrict: 'EA',
        scope: true,
        link: function link ( scope, element, attrs ) {
          var tooltip = $compile( template )( scope );
          var transitionTimeout;
          var popupTimeout;
          var $body;

          // By default, the tooltip is not open.
          // TODO add ability to start tooltip opened
          scope.tt_isOpen = false;

          function toggleTooltipBind () {
            if ( ! scope.tt_isOpen ) {
              showTooltipBind();
            } else {
              hideTooltipBind();
            }
          }

          // Show the tooltip with delay if specified, otherwise show it immediately
          function showTooltipBind() {
            if ( scope.tt_popupDelay ) {
              popupTimeout = $timeout( show, scope.tt_popupDelay );
            } else {
              scope.$apply( show );
            }
          }

          function hideTooltipBind () {
            scope.$apply(function () {
              hide();
            });
          }

          // Show the tooltip popup element.
          function show() {
            var position,
                ttWidth,
                ttHeight,
                ttPosition;

            // Don't show empty tooltips.
            if ( ! scope.tt_content ) {
              return;
            }

            // If there is a pending remove transition, we must cancel it, lest the
            // tooltip be mysteriously removed.
            if ( transitionTimeout ) {
              $timeout.cancel( transitionTimeout );
            }

            // Set the initial positioning.
            tooltip.css({ top: 0, left: 0, display: 'block' });

            // Now we add it to the DOM because need some info about it. But it's not
            // visible yet anyway.
            if ( options.appendToBody ) {
                $body = $body || $document.find( 'body' );
                $body.append( tooltip );
            } else {
              element.after( tooltip );
            }

            // Get the position of the directive element.
            position = options.appendToBody ? $position.offset( element ) : $position.position( element );

            // Get the height and width of the tooltip so we can center it.
            ttWidth = tooltip.prop( 'offsetWidth' );
            ttHeight = tooltip.prop( 'offsetHeight' );

            // Calculate the tooltip's top and left coordinates to center it with
            // this directive.
            switch ( scope.tt_placement ) {
              case 'right':
                ttPosition = {
                  top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                  left: (position.left + position.width) + 'px'
                };
                break;
              case 'bottom':
                ttPosition = {
                  top: (position.top + position.height) + 'px',
                  left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                };
                break;
              case 'left':
                ttPosition = {
                  top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                  left: (position.left - ttWidth) + 'px'
                };
                break;
              default:
                ttPosition = {
                  top: (position.top - ttHeight) + 'px',
                  left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                };
                break;
            }

            // Now set the calculated positioning.
            tooltip.css( ttPosition );

            // And show the tooltip.
            scope.tt_isOpen = true;
          }

          // Hide the tooltip popup element.
          function hide() {
            // First things first: we don't show it anymore.
            scope.tt_isOpen = false;

            //if tooltip is going to be shown after delay, we must cancel this
            $timeout.cancel( popupTimeout );

            // And now we remove it from the DOM. However, if we have animation, we
            // need to wait for it to expire beforehand.
            // FIXME: this is a placeholder for a port of the transitions library.
            if ( angular.isDefined( scope.tt_animation ) && scope.tt_animation() ) {
              transitionTimeout = $timeout( function () { tooltip.remove(); }, 500 );
            } else {
              tooltip.remove();
            }
          }

          /**
           * Observe the relevant attributes.
           */
          attrs.$observe( type, function ( val ) {
            scope.tt_content = val;
          });

          attrs.$observe( prefix+'Title', function ( val ) {
            scope.tt_title = val;
          });

          attrs.$observe( prefix+'Placement', function ( val ) {
            scope.tt_placement = angular.isDefined( val ) ? val : options.placement;
          });

          attrs.$observe( prefix+'Animation', function ( val ) {
            scope.tt_animation = angular.isDefined( val ) ? $parse( val ) : function(){ return options.animation; };
          });

          attrs.$observe( prefix+'PopupDelay', function ( val ) {
            var delay = parseInt( val, 10 );
            scope.tt_popupDelay = ! isNaN(delay) ? delay : options.popupDelay;
          });

          attrs.$observe( prefix+'Trigger', function ( val ) {
            element.unbind( triggers.show );
            element.unbind( triggers.hide );

            triggers = setTriggers( val );

            if ( triggers.show === triggers.hide ) {
              element.bind( triggers.show, toggleTooltipBind );
            } else {
              element.bind( triggers.show, showTooltipBind );
              element.bind( triggers.hide, hideTooltipBind );
            }
          });

          // if a tooltip is attached to <body> we need to remove it on
          // location change as its parent scope will probably not be destroyed
          // by the change.
          if ( options.appendToBody ) {
            scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess () {
            if ( scope.tt_isOpen ) {
              hide();
            }
          });
          }

          // if this trigger element is destroyed while the tooltip is open, we
          // need to close the tooltip.
          scope.$on('$destroy', function closeTooltipOnDestroy () {
            if ( scope.tt_isOpen ) {
              hide();
            }
          });
        }
      };
    };
  }];
})

.directive( 'tooltipPopup', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
    templateUrl: 'template/tooltip/tooltip-popup.html'
  };
})

.directive( 'tooltip', [ '$tooltip', function ( $tooltip ) {
  return $tooltip( 'tooltip', 'tooltip', 'mouseenter' );
}])

.directive( 'tooltipHtmlUnsafePopup', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
    templateUrl: 'template/tooltip/tooltip-html-unsafe-popup.html'
  };
})

.directive( 'tooltipHtmlUnsafe', [ '$tooltip', function ( $tooltip ) {
  return $tooltip( 'tooltipHtmlUnsafe', 'tooltip', 'mouseenter' );
}]);

angular.module('cms', ['ui.bootstrap', 'cms.entity', 'cms.notification', 'cms.selectpicker', 'cms.tree', 'cms.filesuploader', 'cms.utils'])
    .config(['$locationProvider', '$httpProvider', '$tooltipProvider', function($locationProvider, $httpProvider, $tooltipProvider){
		$locationProvider.hashPrefix('!');
		$locationProvider.html5Mode(false);

        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        $httpProvider.defaults.transformRequest = [function(data) {
            var param = function(obj) {
                var query = '';
                var name, value, fullSubName, subValue, innerObj, i;

                for(name in obj) {
                    value = obj[name];

                    if(value instanceof Array) {
                        for(i=0; i<value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if(value instanceof Object) {
                        for(subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if(value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

        $tooltipProvider.options({
            appendToBody: true
        });
	}])
;

