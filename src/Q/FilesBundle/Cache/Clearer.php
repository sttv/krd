<?php

namespace Q\FilesBundle\Cache;

use Doctrine\ORM\EntityManager;


/**
 * Очистка кеша изображений
 */
class Clearer
{
    /**
     * Doctrine Entity Manager
     */
    protected $em;

    /**
     * Папки с кешем изображений
     */
    protected $uploadDir;

    public function __construct(EntityManager $em, $uploadDir)
    {
        $this->em = $em;
        $this->uploadDir = $uploadDir;
    }

    public function clear($unused)
    {
        $i = 0;
        return;
        $query = $this->em->createQuery('SELECT i FROM QFilesBundle:ImageCache i');

        while ($imageCache = $query->iterate()) {
            $this->em->remove($imageCache[0]);

            if ($i > 100) {
                $this->em->flush();
                $i = 0;
            }
        }

        $this->em->flush();
        $this->removeEmptyFolders($this->uploadDir, false);
    }

    /**
     * очистка пустых директорий
     * @param  string $path
     * @param  boolean[optional] $deleteRoot
     * @return boolean
     */
    protected function removeEmptyFolders($path, $deleteRoot = true)
    {
        $empty = true;

        foreach(glob($path.DIRECTORY_SEPARATOR.'*') as $file) {
            if (is_dir($file)) {
                if (!$this->removeEmptyFolders($file)) {
                    $empty = false;
                }
            } else {
                $empty = false;
            }
        }

        if ($empty && $deleteRoot) {
            rmdir($path);
        }

        return $empty;
    }
}
