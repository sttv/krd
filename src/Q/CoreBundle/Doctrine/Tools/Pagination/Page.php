<?php

namespace Q\CoreBundle\Doctrine\Tools\Pagination;

use JMS\Serializer\Annotation as JMS;


/**
 * Страница
 *
 * @JMS\ExclusionPolicy("all")
 */
class Page
{
    /**
     * Номер страницы
     * @var integer
     *
     * @JMS\Expose
     * @JMS\Groups({"user"})
     */
    protected $number = 1;

    /**
     * Страница является фейковой(...)?
     * @var boolean
     *
     * @JMS\Expose
     * @JMS\Groups({"user"})
     */
    protected $isFake = false;

    /**
     * Родительский пагинатор
     * @var [type]
     */
    protected $paginator;

    /**
     * [__construct description]
     * @param integer $number      Номер страницы
     * @param Paginator $paginator Пагинатор
     * @param boolean $isFake Страница является фейковой(...)?
     */
    public function __construct($number, Paginator $paginator, $isFake = false)
    {
        $this->number = $number;
        $this->paginator = $paginator;
        $this->isFake = $isFake;
    }

    /**
     * Формирование URL страниц
     * @param  integer $page Номер страницы
     * @return string
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function getUrl() {
        if ($this->isFirst()) {
            return str_replace(array('{page}', '?page=', '&page='), array('', ''), $this->paginator->getUrlTemplate());
        } else {
            return str_replace('{page}', $this->getNumber(), $this->paginator->getUrlTemplate());
        }
    }

    /**
     * Проверка активности страницы
     * @return boolean
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function isActive()
    {
        return $this->getNumber() == $this->paginator->getCurrentPage();
    }

    /**
     * Проверка на первую страницу
     * @return boolean
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function isFirst()
    {
        return $this->getNumber() == 1;
    }

    /**
     * Проверка на последнюю страницу
     * @return boolean
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function isLast()
    {
        return $this->getNumber() == $this->paginator->getPageCount();
    }

    /**
     * Номер страницы
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Преобразование в строку
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getNumber();
    }
}
