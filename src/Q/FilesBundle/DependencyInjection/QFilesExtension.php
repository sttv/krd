<?php

namespace Q\FilesBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class QFilesExtension extends Extension
{
    /**
     * Loads the configuration.
     *
     * @param array            $configs   An array of configuration settings
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('q_files.upload_dir', $config['upload_dir']);
        $container->setParameter('q_files.upload_path', $config['upload_path']);

        $container->setParameter('q_files.images.upload_dir', $config['images']['upload_dir']);
        $container->setParameter('q_files.images.upload_path', $config['images']['upload_path']);

        $container->setParameter('q_files.image_cache.secret', $config['image_cache']['secret']);
        $container->setParameter('q_files.image_cache.upload_dir', $config['image_cache']['upload_dir']);
        $container->setParameter('q_files.image_cache.upload_path', $config['image_cache']['upload_path']);
        $container->setParameter('q_files.image_cache.sizes', $config['image_cache']['sizes']);

        $container->setParameter('q_files.video.cache_dir', $config['video']['cache_dir']);
        $container->setParameter('q_files.video.cache_path', $config['video']['cache_path']);
        $container->setParameter('q_files.video.upload_dir', $config['video']['upload_dir']);
        $container->setParameter('q_files.video.upload_path', $config['video']['upload_path']);
        $container->setParameter('q_files.video.ffmpeg_bin', $config['video']['ffmpeg_bin']);
        $container->setParameter('q_files.video.ffprobe_bin', $config['video']['ffprobe_bin']);
        $container->setParameter('q_files.video.qtfaststart_bin', $config['video']['qtfaststart_bin']);

        $container->setParameter('q_files.image_cache.sizes', $config['image_cache']['sizes']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    public function getAlias()
    {
        return 'q_files';
    }

    public function getNamespace()
    {
        return 'http://symfony.com/schema/dic/q_files';
    }
}
