/**
 * Интерактивность сайта
 */
angular.module('interactive', [])
    /**
     * Контроллер шапки сайта
     */
    .controller('headerPanelCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.showSearch = function() {
            $element.addClass('opened');
        };

        $scope.hideSearch = function() {
            $element.removeClass('opened');
        };

        $scope.toggleSearch = function() {
            $element.toggleClass('opened');
        };

        $(window).on('resize load scroll', function() {
            if ($(this).scrollTop() > 0) {
                $element.addClass('shadow');
            } else {
                $element.removeClass('shadow');
            }
        });
    }])

    /**
     * Выпадающее верхнее меню
     */
    .directive('dropdownTopmenu', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                var $subLevel = $element.children('.sub-level');

                if ($subLevel.length == 0) {
                    return;
                }

                $element.hover(function() {
                    $subLevel.stop(true, true).addClass('is-visible').slideDown(200);
                }, function() {
                    $subLevel.stop(true, true).removeClass('is-visible').slideUp(200);
                });
            }
        };
    }])

    /**
     * Список табов
     * Очень простой. Без запоминания таба и т.п.
     */
    .directive('tabsList', [function() {
        return {
            restrict: 'C',
            scope: true,
            compile: function($element) {
                $element.find('.pane-list > .item').each(function(index) {
                    $(this).attr({
                        'ng-click': 'setActive('+index+')',
                        'ng-class': '{active:active == '+index+'}'
                    });
                });

                var $tabs = $element.children('.tab').hide();

                // Link
                return function($scope, $element) {
                    var $activePane = $element.find('.pane-list > .item.active');
                    if ($activePane.length > 0) {
                        $scope.active = $activePane.index();
                    } else {
                        $scope.active = 0;
                    }

                    $scope.$watch('active', function(active) {
                        $tabs.hide();
                        $tabs.eq(active).show();
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.setActive = function(index) {
                    $scope.active = index;
                };
            }
        }
    }])


    /**
     * Слайдер новостей
     */
    .directive('newsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowTop = $('<div class="arrow top" ng-click="scrollUp()" ng-class="{hidden: position <= 0 || $items.length == 1}"><i></i></div>');
                var $arrowBottom = $('<div class="arrow bottom" ng-click="scrollDown()" ng-class="{hidden: position >= ($items.length - onPage + 1) || $items.length == 1}"><i></i></div>');

                var $iconsSliders = $element.find('.icon > .icons-slider');

                $element.find('.items')
                    .append($arrowTop)
                    .append($arrowBottom)
                    .find('.item').each(function(i) {
                        $(this).attr({
                            'ng-class': '{active: active == '+i+'}',
                            'ng-click': 'setActive('+i+')'
                        });
                    });

                $element.find('.items > .btn-block').attr({
                    'ng-class': '{hidden: position < $items.length - onPage + 1}'
                });

                return function($scope, $element) {
                    $scope.$itemsWrap = $element.find('.items > .wrap');
                    $scope.$itemsIcons = $element.find('.icon > .icons-slider');
                    $scope.$items = $element.find('.items > .wrap > .item');
                    $scope.$btnLink = $element.find('.items > .btn-block');
                    $scope.onPage = 3;
                    $scope.position = 0;

                    $scope.$watch('position', function(position) {
                        var $item = $scope.$items.eq(position);

                        if ($item.length != 1) {
                            return;
                        }

                        var mt = $item.position().top;

                        $scope.$itemsWrap.stop().animate({marginTop:-mt}, 300);
                    });

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$itemsIcons.eq(active);
                        $scope.$itemsIcons.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                };
            },

            controller: function($scope) {
                $scope.active = 0;
                $scope.position = 0;

                $scope.setActive = function(active) {
                    $scope.active = active;
                    $scope.setPosition(active - 1);
                };

                $scope.setPosition = function(position) {
                    if (position < 0) {
                        position = 0;
                    } else if (position >= ($scope.$items.length - 1)) {
                        position = $scope.$items.length - $scope.onPage;
                    }

                    $scope.position = position;
                };

                $scope.scrollUp = function() {
                    if ($scope.position > 0) {
                        $scope.position--;
                    }
                };

                $scope.scrollDown = function() {
                    if ($scope.position < ($scope.$items.length - $scope.onPage + 1)) {
                        $scope.position++;
                    }
                };
            }
        }
    }])


    /**
     * Слайдер изображений/видео новости
     */
    .directive('iconsSlider', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                var $arrowPrev = $('<div class="arrow prev" ng-click="prev()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-prev"></i><i class="icon-arrow-prev-mini"></i></div>');
                var $arrowNext = $('<div class="arrow next" ng-click="next()" ng-class="{hidden: $items.length == 1}"><i class="icon-arrow-next"></i><i class="icon-arrow-next-mini"></i></div>');

                $element.append($arrowPrev).append($arrowNext);

                return function($scope, $element) {
                    $scope.$items = $element.children('.item');

                    $scope.$watch('active', function(active) {
                        var $active = $scope.$items.eq(active);
                        $scope.$items.stop().css({opacity:1}).not($active).fadeOut(200);
                        $active.fadeIn(150);
                    });
                }
            },

            controller: function($scope) {
                $scope.active = 0;

                $scope.next = function() {
                    if ($scope.active < $scope.$items.length - 1) {
                        $scope.active++;
                    } else {
                        $scope.active = 0;
                    }
                };

                $scope.prev = function() {
                    if ($scope.active > 0) {
                        $scope.active--;
                    } else {
                        $scope.active = $scope.$items.length - 1;
                    }
                };
            }
        }
    }])

    /**
     * Выравнивание нескольких элементов по наибольшей высоте
     * Автоматически обновляется при ресайзе окна
     */
    .directive('multiHeight', [function() {
        var heightList = {};
        var i = 0;

        return {
            restrict: 'A',
            scope: {
                multiHeight:'@'
            },

            link: function($scope, $element) {
                if (heightList[$scope.multiHeight] === undefined) {
                    heightList[$scope.multiHeight] = $scope.heightList = {};
                } else {
                    $scope.heightList = heightList[$scope.multiHeight];
                }

                $scope.index = i++;

                $scope.heightList[$scope.index] = $element.height();

                $(window).on('load resize', function() {
                    $scope.$apply(function() {
                        $scope.update();
                    });
                });

                $scope.update();

                $scope.$watch('heightList', function(heightList) {
                    var arr = [];

                    for(var ii in heightList) {
                        arr.push(parseInt(heightList[ii], 10));
                    }
                    var height = Math.max.apply(Math, arr);

                    if (height > 0 && !isNaN(height)) {
                        $element.height(height);
                    }
                }, true);
            },

            controller: function($scope, $element) {
                $scope.update = function() {
                    var h = $element.height();
                    $element.css('height', 'auto');
                    $scope.heightList[$scope.index] = $element.height();
                    $element.height(h);
                };
            }
        }
    }])

    /**
     * Кнопка показа большего количества элементов
     */
    .directive('moreBtn', ['$compile', '$http', function($compile, $http) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element, attrs) {
                var $listBody = $element.prev('.items-list');

                var $listTemplate = $($.trim($('#'+attrs.moreBtn).html()));

                $listTemplate.attr('ng-repeat', 'item in items');

                $listBody.append($listTemplate);

                return function($scope, $element, attrs) {
                    $compile($listTemplate)($scope);

                    $scope.url = attrs.url;

                    $element.on('click', function(e) {
                        e.preventDefault();

                        $scope.$apply(function() {
                            $scope.nextPage();
                        });
                    });

                    $scope.$watch('loading', function(loading) {
                        if (loading) {
                            $element.html('Загрузка...');
                        } else {
                            $element.html($scope.originalHtml);
                        }
                    });
                };
            },

            controller: function($scope, $element) {
                $scope.items = [];
                $scope.currentPage = 1;
                $scope.loading = false;
                $scope.originalHtml = $element.html();

                $scope.nextPage = function() {
                    if ($scope.loading) {
                        return;
                    }

                    $scope.loading = true;
                    $scope.currentPage++;

                    $http({
                        method: 'GET',
                        url: $scope.url,
                        params: {page: $scope.currentPage}
                    }).success(function(response) {
                        if (response.page_count <= $scope.currentPage) {
                            $element.hide();
                        }

                        for(var i = 0; i < response.items.length; i++) {
                            $scope.items.push(response.items[i]);
                        }

                        $scope.loading = false;
                    }).error(function() {
                        alert('Произошла ошибка, повторите попытку позже');
                        $scope.loading = false;
                    });
                };
            }
        }
    }])
;

/**
 * Горизонтальный скролл, который прокручивает по элементам
 *     horizontal-scroll - имя скролла (необязательное, используется для $scope.$broadcast('setPosition', 'gs', 1);)
 *     horizontal-scroll-items - селектор для жлементов
 *     horizontal-scroll-wrap - селектор для враппера
 *     step - числовое значение шага скрола, либо "item" для пров\мотки по элементам
 *     item-active = селектор активного элементв
 *     btn-all - Текст для кнопки Свернуть/Развернуть
 *
 * Пример вызова:
 *     <div class="items-wrap" horizontal-scroll horizontal-scroll-items=".items-list > .item" horizontal-scroll-wrap=".items-list" step="item" item-active=".active" btn-all="Смотреть все альбомы">
 *     </div>
 */
angular.module('horizontal.scroll', [])
    /**
     * Скролл
     */
    .directive('horizontalScroll', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: true,

            compile: function($element, attrs) {
                var itemsSelector = attrs.horizontalScrollItems;
                var wrapSelector = attrs.horizontalScrollWrap;

                var $wrap = $element.find(wrapSelector);
                var $items = $element.find(itemsSelector);

                if ($wrap.length == 0 || $items.length < 2) {
                    return;
                }

                var itemsWidth = 0;
                var itemsWidthArr = [];
                $items.each(function() {
                    itemsWidth += $(this).outerWidth(true);
                    itemsWidthArr.push(itemsWidth);
                });

                var scrollTmpl = '<div class="horizontal-scroll" ng-show="viewMode">';
                    scrollTmpl += '<div class="arrow left"></div>';
                    scrollTmpl += '<div class="horizontal-slider"></div>';
                    scrollTmpl += '<div class="arrow right"></div>';
                scrollTmpl += '</div>';

                // Кнопка "Смотреть все"
                if (!!attrs.btnAll) {
                    scrollTmpl += '<div class="hs-btn-all">';
                        scrollTmpl += '<div class="btn btn-deep-blue" ng-click="toggleView()">{{ viewMode ? btnAllText : \'Свернуть\' }}</div>';
                    scrollTmpl += '</div>';
                }

                var $scroll = $(scrollTmpl);
                var $slider = $scroll.find('.horizontal-slider');

                $element.append($scroll);
                $wrap.attr('without-wrap', 'viewMode');

                return function($scope, $element, attrs) {
                    $scope.position = 0;
                    $scope.viewMode = true;
                    $scope.btnAllText = attrs.btnAll;
                    $scope.leftOffset = 0;
                    $scope.id = attrs.horizontalScroll;

                    var updateSlider = function() {};
                    var onPositionUpdate = function() {};
                    var onOffsetUpdate = function() {};

                    switch(attrs.step) {
                        // Перемотка по элементам
                        case 'item':
                            updateSlider = function() {
                                var max = $items.length-1;

                                for(var i = 0; i <= itemsWidthArr.length-1; i++) {
                                    var left = itemsWidthArr[i];
                                    var diff = (itemsWidth - $element.width()) - left;

                                    if (diff <= 0) {
                                        max = i + 1;
                                        break;
                                    }
                                }

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $slider.slider('value', position);

                                var left = $items.eq(position).position().left;

                                if (left > (itemsWidth - $element.width())) {
                                    left = (itemsWidth - $element.width());
                                }

                                $scope.leftOffset = -left;
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 200);
                            };

                            break;

                        // Перемотка фиксированным шагом
                        default:
                            updateSlider = function() {
                                var step = !!attrs.step ? parseInt(attrs.step, 10) : 1;
                                var max = (itemsWidth - $element.width());

                                $slider.slider({
                                    min: 0,
                                    max: max,
                                    step: step,
                                    slide: function(event, ui) {
                                        $scope.$apply(function() {
                                            $scope.position = ui.value;
                                        });
                                    }
                                });

                                $scope.maxPosition = max;
                            };

                            onPositionUpdate = function(position) {
                                if (position < 0) {
                                    $scope.position = 0;
                                    return;
                                }

                                if (position > $scope.maxPosition) {
                                    position = $scope.maxPosition;
                                    return;
                                }

                                $scope.leftOffset = -position;
                                $slider.slider('value', position);
                            };

                            onOffsetUpdate = function(leftOffset) {
                                $wrap.stop().animate({marginLeft: leftOffset}, 13);
                            };

                            break;
                    }

                    updateSlider();

                    $(window).on('load', function() {
                        itemsWidth = 0;
                        itemsWidthArr = [];

                        $items.each(function() {
                            itemsWidth += $(this).outerWidth(true);
                            itemsWidthArr.push(itemsWidth);
                        });

                        $scope.$apply(function() {
                            updateSlider();
                        });

                        onPositionUpdate($scope.position);
                        onOffsetUpdate($scope.leftOffset);
                    });

                    $scope.$watch('viewMode', function(viewMode) {
                        if (viewMode) {
                            $wrap.stop().css('marginLeft', $scope.leftOffset);
                        } else {
                            $wrap.stop().css('marginLeft', 0);
                        }
                    });

                    $scope.$on('setPosition', function(event, id, position) {
                        if (id == $scope.id) {
                            $scope.position = position;
                        }
                    });

                    $scope.$watch('position', onPositionUpdate);
                    $scope.$watch('position', function(position) {
                        $scope.$emit('positionUpdated', position);
                    });

                    $scope.$watch('leftOffset', onOffsetUpdate);

                    if (!!attrs.itemActive && attrs.step == 'item') {
                        var $activeItem = $items.filter(attrs.itemActive).eq(0);
                        $scope.position = $activeItem.index();
                        if ($scope.position < 0) {
                            $scope.position = 0;
                        }
                    }

                };
            },

            controller: function($scope, $element) {
                $scope.toggleView = function() {
                    $element.stop(true, true).animate({opacity:0.2}, 200, function() {
                        $scope.$apply(function() {
                            $scope.viewMode = !$scope.viewMode;
                        });
                        $element.stop(true, true).animate({opacity:1}, 200);
                    });
                }
            }
        }
    }])

    /**
     * Автоматическое подстраивание ширины родителя под дочерние элементы
     */
    .directive('withoutWrap', [function() {
        return {
            restrict: 'A',
            scope:{
                viewMode:'=withoutWrap'
            },
            link: function($scope, $element, attrs) {
                var $childrens = $element.children();
                var defPosition = $element.css('position');
                var defLeft = $element.css('left');
                var defRight = $element.css('right');

                $scope.update = function() {
                    if (!$scope.viewMode) {
                        return;
                    }

                    var width = 1;

                    $element.css({width:'auto'});

                    $childrens.each(function() {
                        width += $(this).outerWidth(true);
                    });

                    $element.width(width);
                };

                $(window).on('load resize', $scope.update);
                $scope.$on('updateWithoutWrap', $scope.update);

                $scope.$watch('viewMode', function(viewMode) {
                    $element.removeClass('ww-on ww-off');

                    if (viewMode) {
                        $element.addClass('ww-on');
                        $scope.update();
                    } else {
                        var height = $element.height();
                        $element.addClass('ww-off');
                        $element.css({width:'auto', height:'auto'});
                        var heightTo = $element.height();
                        $element.css({height:height});
                        $element.stop(true, true).animate({height:heightTo}, 200, function() {
                            $element.css({height:'auto'});
                        });
                    }
                });
            }
        }
    }])
;

/**
 * Виджет городского репортера
 */
angular.module('widget.reporter', [])
    .directive('widgetReporter', ['$http', function($http) {
        return {
            restrict: 'C',
            link: function($scope, $element, attrs) {
                var offset = 0;

                var $updateBtn = $element.find('.btn-group .btn-update');
                var $image = $element.find('.w .icon img');
                var $links = $element.find('.w a[target="_blank"]');
                var $date = $element.find('.w .date');
                var $title = $element.find('.w .title');
                var $wrap = $element.find('.w');

                var setPublication = function(pub) {
                    var image = new Image();

                    image.onload = function() {
                        $image.attr('src', pub.image);
                        $links.attr('href', pub.url);
                        $date.text(pub.date);
                        $title.text(pub.title);

                        $wrap.stop(true, true).animate({opacity:1}, 150);
                    };

                    image.src = pub.image;
                };

                var update = function() {
                    offset++;

                    $updateBtn.addClass('disabled');
                    $wrap.stop(true, true).animate({opacity:0.3}, 150);

                    $scope.$apply(function() {
                        $http({
                            method: 'GET',
                            url: attrs.url,
                            params: {offset:offset}
                        }).success(function(response) {
                            $updateBtn.removeClass('disabled');

                            if (!!response.publications && !!response.publications[0]) {
                                setPublication(response.publications[0]);
                            }
                        }).error(function() {
                            alert('Произошла ошибка, повторите попытку позже');
                            $updateBtn.removeClass('disabled');
                        });
                    });
                };

                $updateBtn.on('click', function(e) {
                    e.preventDefault();

                    if (!$updateBtn.hasClass('disabled')) {
                        update();
                    }
                });
            }
        }
    }])
;

angular.module('krd', ['interactive', 'widget.reporter', 'horizontal.scroll', 'gallery.slider']);
