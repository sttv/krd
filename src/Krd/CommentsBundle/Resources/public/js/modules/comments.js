/**
 * Комментарии
 */
angular.module('comments', ['user', 'user.notifications'])
    /**
     * Расшаренные данные блока комментариев
     */
    .service('commentsData', [function() {
        var container = {};

        this.registerList = function(id) {
            container[id] = {};

            return container[id];
        };

        this.get = function(id) {
            return !!container[id] ? container[id] : false;
        };
    }])

    /**
     * Список комментариев
     */
    .directive('commentsList', ['$compile', 'commentsData', function($compile, commentsData) {
        return {
            restrict: 'A',
            scope: true,
            link: function($scope, $element, attrs) {
                $scope.listId = attrs.commentsList;

                commentsData.registerList($scope.listId);

                $scope.commentsCount = function() {
                    return $element.children('.item').length;
                };

                $scope.$on('commentsListAddItem', function(e, where, parentId, html) {
                    if ($scope.listId == where && parentId == 0) {
                        var $item = $(html);
                        $item.appendTo($element);
                        $compile($item)($scope);
                    }
                });
            }
        }
    }])

    /**
     * Элемент комментариев
     */
    .directive('commentItem', ['$compile', 'user', 'commentsData', '$timeout', function($compile, user, commentsData, $timeout) {
        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                $element.children('.links')
                    .after('<div class="well well-reply" ng-include="replyFormTemplate" ng-if="isReplyFormOpen()"></div><div class="well well-edit" ng-include="editFormTemplate" ng-if="isEditFormOpen()"></div>');

                $element.children('.links')
                    .children('.action-reply')
                        .attr('ng-if', 'active')
                        .attr('ng-click', 'toggleReplyForm()');

                $element.children('.links')
                    .children('.action-edit')
                        .attr('ng-click', 'toggleEditForm()');

                $element.children('.links')
                    .children('.action-activate')
                        .attr('ng-if', 'isAdmin && !active')
                        .attr('ng-click', 'activate()');

                $element.children('.links')
                    .children('.action-deactivate')
                        .attr('ng-if', 'isAdmin && active')
                        .attr('ng-click', 'deactivate()');

                $element.children('.links')
                    .children('.action-delete')
                        .attr('ng-if', 'isAdmin')
                        .attr('ng-click', 'remove()')
                ;

                return function($scope, $element, attrs) {
                    $scope.id = parseInt($element.data('id'), 10);
                    $scope.replyFormTemplate = user.isAuthorized() ? 'comments-create-well' : 'comments-auth-well';
                    $scope.editFormTemplate = 'comments-edit-well';
                    $scope.shared = {};
                    $scope.isAdmin = user.isGranted('ROLE_COMMENT_ADMIN');
                    $scope.active = (attrs.active == 1);
                    $scope.commentContent = attrs.content;
                    $timeout(function() {
                        $scope.shared = commentsData.get(attrs.commentItem);
                    });

                    $scope.$watch('isReplyFormOpen()', function(open) {
                        if (open) {
                            $element.addClass('open-reply');
                            $element.removeClass('open-edit');
                        } else {
                            $element.removeClass('open-reply');
                        }
                    });

                    $scope.$watch('isEditFormOpen()', function(open) {
                        if (open) {
                            $element.addClass('open-edit');
                            $element.removeClass('open-reply');
                        } else {
                            $element.removeClass('open-edit');
                        }
                    });

                    $scope.$on('commentsListAddItem', function(e, where, parentId, html) {
                        if ($scope.listId == where && parentId == $scope.id) {
                            var $item = $(html);
                            $item.appendTo($element);
                            $compile($item)($scope);
                        }
                    });
                }
            },

            controller: ['$scope', '$element', '$http', 'router', 'notifications', function($scope, $element, $http, router, notifications) {
                // Проверка открыта ли форма добавления комментария
                $scope.isReplyFormOpen = function() {
                    return $scope.shared.openedReplyForm == $scope.id;
                };

                // Проверка открыта ли форма редактирования комментария
                $scope.isEditFormOpen = function() {
                    return $scope.shared.openedEditForm == $scope.id;
                };

                // Переключение формы ответа на комментарий
                $scope.toggleReplyForm = function(id) {
                    if (id === false || $scope.isReplyFormOpen()) {
                        $scope.shared.openedReplyForm = -1;
                    } else {
                        $scope.shared.openedReplyForm = $scope.id;
                    }
                };

                // Переключение формы редактирования комментария
                $scope.toggleEditForm = function(id) {
                    if (id === false || $scope.isEditFormOpen()) {
                        $scope.shared.openedEditForm = -1;
                    } else {
                        $scope.shared.openedEditForm = $scope.id;
                        $scope.shared.editCommentContent = $scope.commentContent;
                    }
                };

                // Обновление контента в отредактированном комментарии
                $scope.updateEditedComment = function() {
                    $element.children('.text-block').html($scope.shared.editCommentContent.replace(/([^>])\n/g, '$1<br/>'));
                    $scope.commentContent = $scope.shared.editCommentContent;
                    $scope.active = false;
                };

                // Активация комментария
                $scope.activate = function() {
                    $http.post(router.generate('krd_comments_activate', {':id':$scope.id}))
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий активирован');
                                $scope.active = true;
                            } else {
                                notifications.user.error('Не удалось активировать комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось активировать комментарий');
                        });
                };

                // Деактивация комментария
                $scope.deactivate = function() {
                    $http.post(router.generate('krd_comments_deactivate', {':id':$scope.id}))
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий деактивирован');
                                $scope.active = false;
                            } else {
                                notifications.user.error('Не удалось деактивировать комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось деактивировать комментарий');
                        });
                };

                // Удаление комментария
                $scope.remove = function() {
                    if (!confirm('Удалить этот и все вложенные комментарии?')) {
                        return;
                    }

                    $http({
                            method: 'DELETE',
                            url: router.generate('krd_comments_delete', {':id':$scope.id})
                        })
                        .success(function(response) {
                            if (response.success) {
                                notifications.user.info('Комментарий удален');
                                $element.remove();
                            } else {
                                notifications.user.error('Не удалось удалить комментарий');
                            }
                        })
                        .error(function(response) {
                            notifications.user.error(response.error || 'Не удалось удалить комментарий');
                        });
                };
            }]
        }
    }])

    /**
     * Контроллер формы добавления комментариев
     */
    .controller('CommentsCreateFormCtrl', ['$scope', '$rootScope', 'notifications', function($scope, $rootScope, notifications) {
        $scope.onSuccess = function(response) {
            notifications.user.info(response.message || 'Комментарий успешно отправлен и появится на сайте после проверки модератором.');

            if (!!$scope.$parent.toggleReplyForm) {
                $scope.$parent.toggleReplyForm(false);
            }

            if (!!response.newElement && !!response.where) {
                $rootScope.$broadcast('commentsListAddItem', response.where, response.parentId, response.newElement);
            }

            $scope.content = '';
        };

        $scope.onError = function(response) {
            if (response.message) {
                notifications.user.error(response.message);
            }
        };

        $scope.onFatalError = function() {
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Контроллер формы редактирования комментариев
     */
    .controller('CommentsEditFormCtrl', ['$scope', '$element', '$timeout', 'notifications', function($scope, $element, $timeout, notifications) {
        var action = $element.attr('action');

        $timeout(function() {
            $element.attr('action', action.replace('__id__', $scope.$parent.shared.openedEditForm));
        }, 300);

        $scope.onSuccess = function(response) {
            notifications.user.info(response.message || 'Комментарий успешно изменен и появится на сайте после проверки модератором.');

            if (!!$scope.$parent.toggleEditForm) {
                $scope.$parent.toggleEditForm(false);
            }

            if (!!$scope.$parent.updateEditedComment) {
                $scope.$parent.updateEditedComment();
            }
        };

        $scope.onError = function(response) {
            if (response.message) {
                notifications.user.error(response.message);
            }
        };

        $scope.onFatalError = function() {
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Контроллер формы авторизации
     */
    .controller('CommentsAuthFormCtrl', ['$scope', 'notifications', function($scope) {
        $scope.onResponse = function(response) {
            // Костыли костыли :(
            var $r = $(response);

            if ($r.filter('.container-fluid').find('form > .alert-error').length > 0) {
                $scope.errors['_username'] = ['Неверный логин или пароль'];
            } else {
                window.location.reload();
            }
        };

        $scope.onFatalError = function(){
            notifications.user.error('Произошла ошибка, попробуйте позже.');
        };
    }])

    /**
     * Отправка сообщения по Ctrl+Enter
     */
    .directive('ctrlEnter', [function() {
        return function($scope, $element, attrs) {
            if (!attrs.ctrlEnter) {
                return;
            }

            $element.on('keydown', function(e) {
                if (e.ctrlKey && e.keyCode == 13) {
                    $scope.$eval(attrs.ctrlEnter);
                }
            });
        }
    }])
;
