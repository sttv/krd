<?php

namespace Krd\EvacuationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * Эвакуированный автомобиль
 *
 * @ORM\Entity(repositoryClass="Krd\EvacuationBundle\Repository\AvtoRepository")
 * @ORM\Table(name="evacuation_avto",
 *      indexes={
 *          @ORM\Index(name="number", columns={"number"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="toindex", columns={"toindex"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *          @ORM\Index(name="externalId", columns={"externalId"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     date={"datetime", {
 *         "label"="Дата/Время помещения на стоянку",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     evacuator={"entity", {
 *         "label"="Эвакуатор",
 *         "class"="KrdEvacuationBundle:Evacuator",
 *         "property"="number",
 *         "multiple"=false,
 *         "required"=false
 *     }},
 *     fromplace={"text", {"label"="Место эвакуации"}},
 *     mark={"entity", {
 *         "label"="Марка автомобиля",
 *         "class"="KrdEvacuationBundle:Mark",
 *         "property"="title",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     number={"text", {"label"="Гос. номер<br/><small>Российские номера необходимо писать кириллицей</small>"}},
 *     organization={"entity", {
 *         "label"="Организация",
 *         "class"="KrdEvacuationBundle:Organization",
 *         "property"="title",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     parking={"entity", {
 *         "label"="Место стоянки автомобиля",
 *         "class"="KrdEvacuationBundle:Parking",
 *         "property"="title",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     toindex={"checkbox", {
 *         "label"="На главную",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Avto
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Дата помещения на стоянку
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y H:i'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;


    /**
     * Место эвакуации
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $fromplace;

    /**
     * Эвакуатор
     * @ORM\ManyToOne(targetEntity="Evacuator")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $evacuator;

    /**
     * Марка автомобиля
     * @ORM\ManyToOne(targetEntity="Mark")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $mark;

    /**
     * Государственный регистрационный номер
     * @ORM\Column(type="string")
     * ---Assert\Regex(pattern="/[А-Я] \d{3} [А-Я]{2} \d{2,3}/u", match=true, message="Номер должен быть в формате А 123 ОР 123")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $number;

    /**
     * Организация
     * @ORM\ManyToOne(targetEntity="Organization")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $organization;

    /**
     * Место стоянки
     * @ORM\ManyToOne(targetEntity="Parking")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $parking;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Показывать на главной
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $toindex = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * Внешний ID
     * @ORM\Column(type="integer", nullable=true)
     */
    private $externalId;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getFromplace()
    {
        return $this->fromplace;
    }

    public function setFromplace($fromplace)
    {
        $this->fromplace = $fromplace;
    }

    public function getEvacuator()
    {
        return $this->evacuator;
    }

    public function setEvacuator($evacuator)
    {
        $this->evacuator = $evacuator;
    }

    public function hasEvacuator()
    {
        return $this->getEvacuator() instanceof Evacuator;
    }

    public function getMark()
    {
        return $this->mark;
    }

    public function setMark(Mark $mark)
    {
        $this->mark = $mark;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getOrganization()
    {
        return $this->organization;
    }

    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
    }

    public function getParking()
    {
        return $this->parking;
    }

    public function setParking(Parking $parking)
    {
        $this->parking = $parking;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getToindex()
    {
        return $this->toindex;
    }

    public function setToindex($toindex)
    {
        $this->toindex = (boolean)$toindex;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getUrl()
    {
        if ($this->getParent() instanceof Node) {
            return $this->getParent()->getUrl(true);
        } else {
            return '';
        }
    }

    /**
     * @return mixed
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param mixed $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }
}


