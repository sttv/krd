/**
 * Всплывающие окна
 */
angular.module('dialog', [])
    .config(['$anchorScrollProvider', function($anchorScrollProvider) {
        $anchorScrollProvider.disableAutoScrolling();
    }])

    /**
     * Сервис диалогов
     * onClose - событие по закрытию диалога
     */
    .service('$dialog', ['$compile', '$timeout', '$rootScope', function($compile, $timeout, $rootScope) {
        return {
            create: function(template, options) {
                options = $.extend({
                    onClose: function() {},
                    'class': '',
					opacity : 0.5
                }, options);
				
				// dangozero: изменения opacity для overlay
                var $dialog = $('<div dialog-window="'+template+'" dialog-overlay-opacity="'+options['opacity']+'" ng-class="\''+options['class']+'\'"></div>');
                $dialog.appendTo('body');

                var newScope = $rootScope.$new(true);
                newScope.onClose = options.onClose;
                $compile($dialog)(newScope);
            }
        }
    }])

    /**
     * Кнопки показа всплывающиего окна
     */
    .directive('dialogShow', ['$dialog', function($dialog) {
        return {
            restrict: 'A',

            link: function($scope, $element, attrs) {
                $element.on('click', function(e) {
                    e.preventDefault();
					
                    $dialog.create(attrs.dialogShow, {
                        'class': attrs.dialogClass,
                        onClose: function() {
                            if (attrs.dialogHash) {
                                window.location.hash = 'none';
                            }
                        }
                    });

                    if (attrs.dialogHash) {
                        window.location.hash = attrs.dialogHash;
                    }
                });

                if (attrs.dialogHash) {
                    var checkHash = window.location.hash.replace('#/', '').replace('#', '');

                    if (checkHash == attrs.dialogHash) {
                        setTimeout(function() {
                            $element.trigger('click');
                        }, 100);
                    }
                }
            }
        }
    }])

    /**
     * Окно диалога
     */
    .directive('dialogWindow', ['$timeout', '$interval', function($timeout, $interval) {
        return {
            restrict: 'A',
            scope: true,
            template: '<div class="dialog">'
                        + '<div class="icon-close close-btn" ng-click="hide()"></div>'
                        + '<div class="w" ng-include="template" onload="show()"></div>'
                    + '</div>',
            replace: true,

            link: function($scope, $element, attrs) {
                $element.hide();
                $scope.state = 'hidden';
                $scope.template = attrs.dialogWindow;
				$scope.opacity = parseFloat(attrs.dialogOverlayOpacity);
				
                $(document).ready($scope.relocate);
                $(window).load($scope.relocate);
                $(window).resize($scope.relocate);
                $timeout($scope.relocate, 100);
                // $interval($scope.relocate, 700);

                $scope.$overlay = $('<div class="dialog-overlay"></div>');
                $scope.$overlay.appendTo('body');
                $scope.$overlay.on('click', function() {
                    $scope.hide();
                });

                $timeout(function() {
                    if ($scope.state == 'hidden') {
                        $scope.$overlay.after('<s class="l"></s>');
                    }
                }, 300);

                $scope.$on('dialog-hide', function() {
                    $scope.hide();
                });
            },

            controller: function($scope, $element) {
                /**
                 * Обновляем позиционирование на экране
                 */
                $scope.relocate = function() {
                    if (document.body.clientHeight < $element.height()) {
                        $element.css({
                            position: 'absolute',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: 0,
                            top: $(window).scrollTop() + 15
                        });
                    } else {
                        $element.css({
                            position: 'fixed',
                            marginLeft: document.body.clientWidth > 1000 ? -$element.width() / 2 : -$element.width() / 2 + 25,
                            marginTop: -$element.height() / 2
                        });
                    }
                };

                $scope.show = function() {
                    $scope.relocate();
                    $scope.state = 'visible';

                    $element.stop(true, true).css({opacity:1}).fadeIn(300);
                    $scope.$overlay.stop(true, true).css({display:'block'}).animate({opacity:$scope.opacity}, 300, function() {
                        $timeout($scope.relocate);
                        $timeout($scope.relocate, 100);
                        $scope.$overlay.next('.l').remove();
                    });
                };

                $scope.hide = function() {
                    $scope.state = 'hidden';
                    $element.stop(true, true).fadeOut(200);
                    $scope.$overlay.stop(true, true).fadeOut(200, function() {
                        $scope.$overlay.remove();
                        $element.remove();
                        !!$scope.onClose && $scope.onClose();
                    });
                };
            }
        }
    }])
;
