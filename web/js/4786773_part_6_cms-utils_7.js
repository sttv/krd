/**
 * Полезные инструменты
 */
angular.module('cms.utils', [])
    /**
     * Фильтр транслитерации
     */
    .filter('translit', [function(){
        var List = {
                'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
                'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
                'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
                'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
                'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
                'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
                'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
                'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
                'Я':'Ya','я':'ya'
            },
            regular = '',
            k;

        for (k in List){
            regular += k;
        }

        regular = new RegExp('[' + regular + ']', 'g');

        k = function(a){
            return a in List ? List[a] : '';
        };

        return function(input){
            if (!!input){
                return input.replace(regular, k);
            }else{
                return '';
            }
        };
    }])

    /**
     * Удобочитаемый размер файла
     */
    .filter('filesize', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '-';
            }

            if (typeof precision === 'undefined') {
                precision = 1;
            }

            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
            var number = Math.floor(Math.log(bytes) / Math.log(1024));

            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })

    /**
     * Обрезание строки по кол-ву символов
     */
    .filter('characters', function () {
        return function (input, chars, breakOnWord) {
            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length >= chars) {
                input = input.substring(0, chars);

                if (!breakOnWord) {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1) {
                        input = input.substr(0, lastspace);
                    }
                }else{
                    while(input.charAt(input.length-1) == ' '){
                        input = input.substr(0, input.length -1);
                    }
                }
                return input + '...';
            }
            return input;
        };
    })

    /**
     * Обрезание строки по кол-ву слов
     */
    .filter('words', function () {
        return function (input, words) {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input) {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words) {
                    input = inputWords.slice(0, words).join(' ') + '...';
                }
            }
            return input;
        };
    })

    /**
     * Директива преобразования в системное имя
     */
    .directive('nodename', ['$filter', function($filter) {
        var getValue = function(value) {
            value = value.toString().toLowerCase();
            value = $filter('translit')(value);
            value = value.replace(/( |\/)/g, '-');
            value = value.replace(/[^a-zA-Z_\-0-9]/g, '');

            return value;
        };

        return {
            restrict: 'A',
            scope: true,
            compile: function($element) {
                if (!$element.is('input')) {
                    return function() {};
                }

                return function($scope, $element, attrs) {
                    var $source = $element.closest('form').find('*[name="'+attrs.nodename+'"]');

                    if ($source.length == 0) {
                        return;
                    }

                    $source.on('keyup click', function() {
                        var value = $(this).val();
                        $element.val(getValue(value));
                    });

                    $element.on('keyup', function() {
                        $source.off('keyup click');
                    });
                }
            }
        }
    }])
;
