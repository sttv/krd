<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Тип меню
 *
 * @ORM\Entity(repositoryClass="Q\CoreBundle\Repository\MenuTypeRepository")
 * @ORM\Table(name="menu_type",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="name", columns={"name"})
 *     }
 * )
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     help={"textarea", {"label"="Описание"}},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class MenuType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * Текстовое название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-zA-Z\-_]+$/", match=true, message="Может состоять только из латиницы")
     */
    private $name;

    /**
     * Описание для помощи
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $help;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function setHelp($help)
    {
        $this->help = $help;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}


