<?php

namespace Krd\SupportBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;

use Q\CoreBundle\Form\DataTransformer\EntityToIntTransformer;


class MessageToIntTransformer extends EntityToIntTransformer
{
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        parent::__construct($om);
        $this->setEntityClass('Krd\SupportBundle\Entity\Message');
        $this->setEntityRepository("KrdSupportBundle:Message");
        $this->setEntityType("message");
    }
}
