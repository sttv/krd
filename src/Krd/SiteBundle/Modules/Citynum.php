<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль - краснодар и цифры
 */
class Citynum extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/citynum.html.twig');
    }

    /**
     * Общее количество активных элементов
     */
    public function getCount()
    {
        return $this->getRepository()->createQuery("SELECT COUNT(e) FROM __CLASS__ e WHERE e.active = 1")->getSingleScalarResult();
    }

    /**
     * Поиск случайного документа
     */
    public function findRandom(array $ids)
    {
        if (empty($ids)) {
            $query = $this->getRepository()->createQuery("SELECT e, RAND() as HIDDEN rand FROM __CLASS__ e WHERE e.active = 1 ORDER BY rand");
        } else {
            $query = $this->getRepository()->createQuery("SELECT e, RAND() as HIDDEN rand FROM __CLASS__ e WHERE e.active = 1 AND e.id NOT IN (:ids) ORDER BY rand");
            $query->setParameter('ids', $ids);
        }

        $query->setMaxResults(1);
        $res = $query->getResult();

        if (isset($res[0])) {
            return $res[0];
        } else {
            return;
        }
    }

    public function renderIndexWidget()
    {
        $item = $this->getRepository()->findOneBy(array('active' => 1), array('id' => 'ASC'));

        return $this->twig->render('KrdSiteBundle:Module:citynum/widget.html.twig', array('item' => $item, 'count' => $this->getCount()));
    }
}
