<?php

namespace Krd\UserBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;
use KrdUserBundle\Repository\BookmarkDirectoryRepository;
use Krd\UserBundle\Entity\Widget;


/**
 * Модуль закладок личного кабинета
 */
class BookmarkModule extends AbstractModule
{
    public function renderAdminContent()
    {
        // Nothing
    }

    /**
     * Получение текущей закладки на основе запроса
     */
    public function getCurrent()
    {
        $id = $this->request->attributes->get('name');
        return $this->getRepository()->getMyBookmark($id);
    }

    /**
     * Репозиторий папок закладок
     * @return BookmarkDirectoryRepository
     */
    public function getDirectoryRepository()
    {
        return $this->getManager()->getRepository('KrdUserBundle:BookmarkDirectory');
    }

    /**
     * Список закладок пользователя
     */
    public function renderContent()
    {
        return $this->twig->render('KrdUserBundle:Module:bookmark/index.html.twig', array('directories' => $this->getDirectoryRepository()->getMyDirectories()));
    }

    /**
     * Виджет в личном кабинете
     */
    public function renderUserWidget(Widget $widget)
    {
        return $this->twig->render('KrdUserBundle:Module:bookmark/widget.html.twig', array('list' => $this->getRepository()->getMyBookmarks(2), 'widget' => $widget));
    }
}
