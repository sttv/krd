<?php

namespace Krd\ConferenceBundle\Modules;

use Symfony\Component\Form\FormFactory;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\SecurityContext;

use Q\CoreBundle\Admin\AbstractModule;
use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Form\DataTransformer\EntityToIntTransformer;
use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;
use Q\UserBundle\Entity\User;
use Krd\ConferenceBundle\Entity\Online as OnlineEntity;
use Krd\ConferenceBundle\Entity\Question;


/**
 * Модуль онлайн конференций
 */
class Online extends AbstractModule
{
    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function setSecurityContext(SecurityContext $sc)
    {
        $this->securityContext = $sc;
    }

    public function renderAdminContent()
    {
        return $this->twig->render('KrdConferenceBundle:Admin:Module/online.html.twig');
    }

    /**
     * Текущая конференция
     * @return Online
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Построение списка
     * @param  integer $root
     * @return array
     */
    public function getItems($root)
    {
        $rootNode = $this->em->getRepository('QCoreBundle:Node')->find($root);

        if (!$rootNode) {
            return array('itemsFuture' => array(), 'itemsPast' => array(), 'rootNode' => $rootNode);
        }

        $itemsFutureQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            ->andWhere('d.dateworkstart <= :now')
            ->andWhere('d.dateworkend >= :now')
            ->addOrderBy('d.dateworkstart', 'DESC')
            ->addOrderBy('d.dateworkend', 'DESC')
            ->setParameter('parent', $root)
            ->setParameter('now', new \DateTime())
            ->getQuery();

        $itemsFuture = $itemsFutureQuery->useResultCache(true, 15)->getResult();


        return array('itemsFuture' => $itemsFuture, 'itemsPast' => $this->getItemsPast($root), 'rootNode' => $rootNode);
    }

    /**
     * Список прошедших конференций
     * @param  integer $root
     * @return arary
     */
    public function getItemsPast($root)
    {
        $itemsPastQuery = $this->getRepository()->createQueryBuilder('d')
            ->andWhere('d.active = 1')
            ->andWhere('d.parent = :parent')
            ->andWhere('d.dateworkend <= :now')
            ->addOrderBy('d.dateworkstart', 'DESC')
            ->addOrderBy('d.dateworkend', 'DESC')
            ->setParameter('parent', $root)
            ->setParameter('now', new \DateTime())
            ->getQuery();

        return Paginator::createFromRequest($this->request, $itemsPastQuery);
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItems($this->node->getNode()->getId());

        return $this->twig->render('KrdConferenceBundle:Module:online/list.html.twig', $result);
    }

    /**
     * Фронтэнд форма отправки вопроса в онлайн конференцию
     */
    public function getQuestionFormBuilder(OnlineEntity $online = null)
    {
        $question = new Question();

        if ($online) {
            $question->setParent($online);
        }

        if (($user = $this->securityContext->getToken()->getUser()) && $user instanceof User) {
            $question->setFioAsk($user->getFullName());
        }

        return $this->formFactory->createNamedBuilder('question', 'conference_question', $question);
    }

    /**
     * Подробная страница
     */
    public function renderDetailContent()
    {
        $result = array('item' => $this->getCurrent());

        if ($result['item']->isOpen()) {
            $formBuilder = $this->getQuestionFormBuilder($result['item']);
            $formBuilder->setAction($this->router->generate('ajax_conference_online_question_post'));
            $result['questionForm'] = $formBuilder->getForm()->createView();
        }

        return $this->twig->render('KrdConferenceBundle:Module:online/detail.html.twig', $result);
    }

    /**
     * Виджет для главной страницы
     */
    public function renderWidget()
    {
        $result = $this->getRepository()->createQuery("SELECT e, RAND() as HIDDEN rand FROM __CLASS__ e WHERE e.active = 1 AND e.dateworkstart <= :date AND e.dateworkend >= :date ORDER BY rand")
                ->setParameter('date', new \DateTime())
                ->setMaxResults(1)
                ->useResultCache(true, 15)
                ->getResult();

        if (isset($result[0])) {
            $conference = $result[0];

            return $this->twig->render('KrdConferenceBundle:Module:online/widget.html.twig', array('conference' => $conference));
        }
    }
}
