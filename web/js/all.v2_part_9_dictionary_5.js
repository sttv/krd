/**
 * Словарь сокращений
 */
angular.module('dictionary', [])
    /**
     * Алфавитный указатель
     */
    .directive('dictionaryAlphabet', [function() {
        return {
            restrict: 'C',

            link: function($scope, $element) {
                $element.find('.item').on('click', function(e) {
                    e.preventDefault();

                    var $letter = $('.letter[data-letter="'+$(this).text()+'"]');

                    $.scrollTo($letter, 200, {offset:{top:-55}});
                });
            }
        }
    }])

    /**
     * Список сокращений
     */
    .directive('dictionaryList', [function() {
        return {
            restrict: 'C',
            scope: true,

            compile: function($element) {
                $element.children('.item').children('a').attr('ng-click', 'toggle($event)')
            },

            controller: function($scope) {
                /**
                 * Переключение видимости элемента
                 */
                $scope.toggle = function($event) {
                    $event.preventDefault();

                    var $item = $($event.srcElement).parent('.item');
                    $item.children('.text-block').slideToggle(200);
                }
            }
        }
    }])
;
