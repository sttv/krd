<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\KrdUserEvents;


/**
 * Фронтэнд авторизация
 */
class AuthorizationController extends Controller implements ActiveSecuredController
{
    /**
     * Страница авторизации
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->get('qcore.routing.current')->getNode()->getParent()->getUrl(true));
        }

        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            $error = $error->getMessage();
        }

        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $csrfToken = $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;

        return array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token' => $csrfToken,
        );
    }

    /**
     * Страница восстановления пароля
     *
     * @Route("/pass-forgot", name="krd_user_password_forgot")
     * @Template()
     */
    public function passForgotAction(Request $request)
    {
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->get('qcore.routing.current')->getNode()->getParent()->getUrl(true));
        }

        $result = array('step' => 0);
        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');

        if ($request->getMethod() == 'POST') {
            if ($email = $request->request->get('email')) {
                $result['email'] = $email;

                if (($user = $userManager->findUserByEmail($email)) && ($user instanceof User)) {
                    $result['step'] = 1;
                    $result['user'] = $user;

                    if ($user->getPasswordRequestedAt() instanceof \DateTime &&
                        time() - $user->getPasswordRequestedAt()->getTimestamp() < 60*5) {
                        return array('error' => 'toofast');
                    }

                    if ($creply = $request->request->get('creply')) {
                        $result['creply'] = $creply;

                        if ($user->getCreply() == $creply) {
                            $result['step'] = 2;

                            $dispatcher->dispatch(KrdUserEvents::RESET_PASSWORD_REQUEST, new UserEvent($user, $request));
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Завершающий этап сброса пароля
     *
     * @Route("/user/authorization/reset/password/{token}", name="krd_user_authorization_reset_password")
     * @Template()
     */
    public function resetPasswordAction(Request $request, $token)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');
        $user = $userManager->findUserByConfirmationToken($token);

        if (!$user || !$user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            throw new NotFoundHttpException();
        }

        if ($request->getMethod() == 'POST') {
            $password = $request->request->get('password');
            $user->setPlainPassword($password);
            $dispatcher->dispatch(KrdUserEvents::RESET_PASSWORD_SUCCESS, new UserEvent($user, $request));
            $userManager->updateUser($user);

            return array('success' => true);
        }
    }

    /**
     * @Route("/user/authorization/index/", name="krd_user_authorization_path")
     */
    public function redirectToIndexAction()
    {
        $url = $this->get('qcore.routing.current')->generate(41);

        if ($url == '#') {
            throw $this->createNotFoundException();
        } else {
            return $this->redirect($url);
        }
    }
}
