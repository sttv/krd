<?php

namespace Krd\GlavaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Фотогалерея
 */
class GalleryController extends Controller
{
    /**
     * Список альбомов
     *
     * @Route("/")
     * @Template()
     */
	// @Secure(roles="ROLE_CMS")
    public function listAction()
    {
    }
	
	/**
     * Список альбомов для постраничного вывода
     * @Route("/ajax/glava/gallery/list.json", name="ajax_glava_gallery_list", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function moreListAction(Request $request)
    {
    	$queryBuilder = $this->get('krd.gallery.modules.gallery')->getRepository()
            ->createQueryBuilder('g')
            ->andWhere('g.active = 1')
            ->andWhere('g.parent = :parent')
            ->orderBy('g.date', 'DESC')
            ->setParameter('parent', $request->get('parent'));
		return Paginator::create($queryBuilder->getQuery(), $request->get('page', 1) + 2, 3, true);
        //return Paginator::createFromRequest($request, $queryBuilder->getQuery(), true, 3);
    }
	
	/**
	 * Слайдер альбома
	 * @Route("/source-{id}.{_format}", requirements={"id"="^\d+$", "_format"="json"})
	 * @Method({"GET"})
	 */
	public function sourceAction($id)
	{
		$items = $this->get('krd.gallery.modules.gallery')->getRepository()
            ->createQueryBuilder('g')
            ->andWhere('g.active = 1')
            ->andWhere('g.id = :id')
            ->orderBy('g.date', 'DESC')
            ->setParameter('id', $id)
			->getQuery()
            ->getResult();
		
		return $items[0];
	}
	
	/**
	 * Слайдер альбома
	 * @Route("/detail-{id}.{_format}", requirements={"id"="^\d+$", "_format"="html"})
	 * @Method({"GET"})
	 */
	public function detailAction(Request $request) {
		$item = $this->getDoctrine()->getManager()->find('KrdGalleryBundle:Gallery', $request->get('id'));
		
		return $this->render('KrdGlavaBundle:Module:gallery/detail.html.twig', array('gallery' => $item));
	}
	
	/**
	 * @Route("/ajax/glava/gallery/popup.html", name="ajax_glava_gallery_popup")
	 * @Method({"GET"})
	 */
	public function popupAction(Request $request) 
	{
		$gallery = $this->getDoctrine()->getManager()->find('KrdGalleryBundle:Gallery', $request->get('id'));
		
		return $this->render('KrdGlavaBundle:Module:gallery/popup.html.twig', array('item' => $gallery, 'currentIndex' => (int)$request->get('current', 0)));
	}
	
	/**
	 * @Route("/ajax/glava/gallery/download/", name="ajax_glava_gallery_download")
	 * @Method({"GET"})
	 */
	public function downloadAction(Request $request) 
	{
		$r = $request->get('r', null);
		if( $r !== null && preg_match('!^/uploads/images/!i', $r) ) {
			$filename = basename($r);
			$r = dirname(self::makeSafe($r));
			$file = $this->get('kernel')->getRootDir() . '/../web'.$r.'/'.$filename;
			$response = new BinaryFileResponse($file);
	
			$response->headers->set('Content-Type', 'text/plain');
			$response->setContentDisposition(
			    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			    $filename
			);
			return $response;
		} else {
			die("wrong params.");
		}
	}
	
	
	public static function makeSafe($path)
	{
		$regex = array('#[^A-Za-z0-9:_\\\/-]#');
		return preg_replace($regex, '', $path);
	}
}