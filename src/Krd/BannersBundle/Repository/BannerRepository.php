<?php

namespace Krd\BannersBundle\Repository;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;


/**
 * Репозиторий баннеров
 */
class BannerRepository extends EntityRepository
{
    /**
     * Активные баннеры указанного типа
     * @param  string $type
     * @return array
     */
    public function findActiveByType($type)
    {
        return $this->createQuery("SELECT b FROM __CLASS__ b WHERE b.active = 1 AND b.type = :type")
            ->setParameter('type', $type)
            ->setMaxResults(20)
            ->useResultCache(true, 15)
            ->getResult();
    }
}
