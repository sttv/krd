<?php

namespace Krd\ApiBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityManager;
use Krd\ApiBundle\Entity\ApnToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


/**
 * Apple APNs
 *
 * @Route("/apn")
 */
class ApnController extends Controller
{
    /**
     * ����������� ������ ������
     *
     * @Route("/register_token")
     * @Method({"POST"})
     */
    public function registerTokenAction(Request $request)
    {
        $token = $request->get('token');

        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ApnToken $apnToken */
        $apnToken = $em->getRepository('KrdApiBundle:ApnToken')->findOneBy(array('token' => $token));

        if (!$apnToken) {
            $apnToken = new ApnToken();
            $apnToken->setToken($token);
            $em->persist($apnToken);
        }

        $apnToken->setUpdated(new DateTime());
        $em->flush($apnToken);
		
		echo $token, " saved ";
        exit;
    }
    /**
     * �������� �������� �������� ���������
     *
     * @Route("/test_emergency_news")
     */
    public function testEmerNewsAction(Request $request)
    {
		error_reporting(-1);
		ini_set('display_errors', 1);
        /** @var Entitymanager $em */
        $em = $this->getDoctrine()->getManager();

        $emer = $em->getRepository('KrdNewsBundle:Emergency')->find(290);
		echo $emer->getId(), " ", $emer->getTitle(), "\n";
        $this->get('notifications.apn.apnmanager')->sendToAll(array(
			'action' => 'emergency',
            'id' => $emer->getId(),
            'title' => $emer->getTitle(),
            'message' => $emer->getMessage(),
            'time' => $emer->getDate()->format('H:i'),
        ));

        die('finish');
    }
}
