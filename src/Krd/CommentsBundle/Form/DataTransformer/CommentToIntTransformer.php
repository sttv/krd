<?php

namespace Krd\CommentsBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;

use Q\CoreBundle\Form\DataTransformer\EntityToIntTransformer;


class CommentToIntTransformer extends EntityToIntTransformer
{
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        parent::__construct($om);
        $this->setEntityClass('Krd\CommentsBundle\Entity\Comment');
        $this->setEntityRepository("KrdCommentsBundle:Comment");
        $this->setEntityType("comment");
    }
}
