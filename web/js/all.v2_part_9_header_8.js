angular.module('header', [])
    /**
     * Контроллер шапки сайта
     */
    .controller('headerPanelCtrl', ['$scope', '$element', function($scope, $element) {
        $scope.showSearch = function() {
            $element.addClass('panel-search-open');
        };

        $scope.hideSearch = function() {
            $element.removeClass('panel-search-open');
        };

        $scope.toggleSearch = function() {
            $element.toggleClass('panel-search-open');
        };

        $(window).on('resize load scroll', function() {
            if ($(this).scrollTop() > 0) {
                $element.addClass('shadow');
            } else {
                $element.removeClass('shadow');
            }
        });
    }])

    /**
     * Выпадающее верхнее меню
     */
    .directive('dropdownTopmenu', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                var $subLevel = $element.children('.sub-level');

                if ($subLevel.length == 0) {
                    return;
                }

                $element.hover(function() {
                    if (!$subLevel.is(':animated')) {
                        $subLevel.stop(true, true).addClass('is-visible').slideDown(150);
                    }
                }, function() {
                    $subLevel.stop(true, true).removeClass('is-visible').slideUp(150);
                });
            }
        };
    }])
