<?php

namespace Krd\MfcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * MFC Ajax load
 */
class AjaxController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/ajax/krd/mfc/{map}/{action}", name="krd_mfc_load", requirements={"action"=".*"})
     */
    public function loadAction(Request $request, $map, $action)
    {
        $action = mb_strtolower($action);
        $em = $this->get('doctrine.orm.oldsite_entity_manager');
        $buf = array();

        switch ($action) {
            // Полный список участков
            case 'load':
                $st = $em->getConnection()->prepare('SELECT * FROM `krd__addresses` WHERE `map_rect` IS NOT NULL AND `map_rect` != "" AND `map` = :map');
                $st->execute(array('map' => $map));

                while ($row = $st->fetch()) {
                    // $row['settlement'] = iconv('windows-1251', 'utf-8', $row['settlement']);
                    // $row['street'] = iconv('windows-1251', 'utf-8', $row['street']);
                    $buf[$row['map_rect']] = $row;
                }

                break;

            // Список свободных участков
            // Для 4 и 5-й карт в список свободных участков включаются участки с подаными заявками, т.к. там немного другое отображение списка
            case 'loadfree':
                if ($map != 4 && $map != 5 && $map != 6) {
                    $st = $em->getConnection()->prepare('SELECT * FROM `krd__addresses` WHERE `map_rect` IS NOT NULL AND `map_rect` != "" AND `map` = :map AND `descr` IN (0) ORDER BY `settlement`, `street`, `number`');
                } else {
                    $st = $em->getConnection()->prepare('SELECT * FROM `krd__addresses` WHERE `map_rect` IS NOT NULL AND `map_rect` != "" AND `map` = :map AND `descr` IN (0, 1) ORDER BY `settlement`, `street`, `number`');
                }

                $st->execute(array('map' => $map));

                while ($row = $st->fetch())
                {
                    // $row['settlement'] = iconv('windows-1251', 'utf-8', $row['settlement']);
                    // $row['street'] = iconv('windows-1251', 'utf-8', $row['street']);
                    $buf[] = $row;
                }

                break;

            default:
                $buf['error'] = 'Action is not defined';
        }

        return new JsonResponse($buf);
    }
}
