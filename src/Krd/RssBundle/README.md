### KrdRssBundle

RSS фиды к новостям.
Логика такова, что фиды создаются отдельными сущностями, которые способны самостоятельно "собрать" нужные новости в список. Для каждого фида просто указывается ссылка на новостной раздел из которого будут браться новости.

В связи с "особенностями" яндекса была добавлена некая модификация. Если к URL фида добавить GET параметр `yandex=1`, то в фид не будут выводиться новости с флагом `rssdisabled = 1`
