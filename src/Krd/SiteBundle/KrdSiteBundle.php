<?php

namespace Krd\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Основной бандл сайта
 */
class KrdSiteBundle extends Bundle
{
}
