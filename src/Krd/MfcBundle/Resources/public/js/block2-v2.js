function getPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
      cursor.x = e.pageX;
      cursor.y = e.pageY;
    }
    else {
      cursor.x = e.clientX +
        (document.documentElement.scrollLeft ||
         document.body.scrollLeft) -
         document.documentElement.clientLeft;
      cursor.y = e.clientY +
        (document.documentElement.scrollTop ||
         document.body.scrollTop) -
         document.documentElement.clientTop;
    }
    return cursor;
}

var MFC = {
	$map: null,
	$map_scroller: null,
	address_loader: '',
	limit: [],
	offset: {x:0, y:0},
	$infobox: {
		block: null,
		info: null
	},
	items: {},
	colors: {
		'state-1': ['#b5b5b5', '#ffe49d'],
		state0: ['#85ea00', '#c2f580'],
		state1: ['#ffcc00', '#ffe26e'],
		state2: ['#ea2d32', '#f59699']
	},
	statuses: {
		'state-1': 'не известно',
		state0: 'земельный участок свободен',
		state1: 'на участок подано заявок:',
		state2: 'участок в стадии оформления'
	},
	prev_selected: null,
	prev_color: '#c0ad00',
	current_selected: null,
	feature_clickHandler: function() {
		var buf = this.getBBox();
		MFC.$infobox.block.css({top:buf.y+buf.height/2+MFC.offset.y, left:buf.x+buf.width/2+MFC.offset.x}).show();
	},
	feature_overHandler: function() {

		var title = this.data('title');
		if (!title) return;

		var buf = this.getBBox();
		MFC.$infobox.block.css({top:buf.y+buf.height/2+MFC.offset.y, left:buf.x+buf.width/2+MFC.offset.x}).show();
		this.attr('fill', this.data('color_hl'));

		MFC.$infobox.info.html(this.data('title'));
		//MFC.$infobox.block.css({top:buf.y, left:buf.x}).show();
	},
	feature_outHandler: function() {
		this.attr('fill', this.data('color_default'));
		MFC.$infobox.block.hide();
	},
	window_resizeHanlder: function() {

		var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],
			x=w.innerWidth||e.clientWidth||g.clientWidth,
			y=w.innerHeight||e.clientHeight||g.clientHeight;

		MFC.$map_scroller.css('height', y - 100 - $('#mfc-block-fullscreen .mfc-block-description').height());
		MFC.$map.css('left', -(1366-MFC.$map_scroller.width())/2);
	},
	init: function() {

		MFC.$infobox.block = $('#mfc-infobox');
		MFC.$infobox.info = $('#mfc-infobox-info');
		MFC.$map_scroller = $('#mfc-map-scroller');
		MFC.offset.x = MFC.$map.attr('infobox:offset-x')*1;
		MFC.offset.y = MFC.$map.attr('infobox:offset-y')*1;

		MFC.limit = MFC.$map.attr('map:limit').split(',');

		$('.mfc-collapse').click(function(){
			$('#mfc-block-fullscreen').removeClass('mfc-block-fullscreen');
			$('.mfc-expand').show();
			$('.mfc-collapse').hide();

			MFC.window_resizeHanlder();
			return false;
		});

		$('.mfc-expand').click(function(){
			$('#mfc-block-fullscreen').addClass('mfc-block-fullscreen');
			$('.mfc-collapse').show();
			$('.mfc-expand').hide();

			MFC.window_resizeHanlder();
			return false;
		});

		$('#mfc-block-fullscreen .btn-fullscreen').on('click', function(e) {
			e.preventDefault();

			$('#mfc-block-fullscreen').toggleClass('mfc-block-fullscreen');
			$(this).toggleClass('fx');

			if ($(this).hasClass('fx') && !!MFC.$map) {
				MFC.$map.css({left: (MFC.$map.parent().width() - MFC.$map.width())/2});
			} else {
				MFC.$map.css({left:0});
			}
		});

		var buf, title;
		MFCMap.init();
		MFCMap.rsr.forEach(function(item){
			if (item.type == 'text')
				return;

			title = null;
			if (MFC.items[item.data('id')]) {
				buf = MFC.items[item.data('id')];
				title = buf.settlement+', '+buf.street+', '+buf.number + '<br/>'+MFC.statuses['state'+buf.descr];
				if (buf.descr == 1) title += ' '+buf.count_pack;
				buf = buf.descr;
				item[0].style.cursor = "pointer";
			}
			else
				buf = '-1';

			item.data('color_default', MFC.colors['state'+buf][0]);
			item.data('color_hl', MFC.colors['state'+buf][1]);
			item.data('title', title);
			item.attr('fill', MFC.colors['state'+buf][0]);

			item.hover(MFC.feature_overHandler, MFC.feature_outHandler);
		});

		$('.mfc-map-mt, .mfc-map-mb, .mfc-map-mr, .mfc-map-ml').click(function(){
			var $c = $(this);
			var dirx = 1, diry = 1;
			var step = 50;
			var mx = 0, my = 0;
			if ($c.hasClass('mfc-map-mr')) {
				dirx = -1;
				mx = step;
			}
			if ($c.hasClass('mfc-map-mb')) {
				diry = -1;
				my = step;
			}
			//alert((parseInt(MFC.$map.css('margin-left')) + dirx * step)+', '+(parseInt(MFC.$map.css('margin-top')) + diry * step));

			var newx = parseInt(MFC.$map.css('left')), newy = parseInt(MFC.$map.css('top'));
			if ($c.hasClass('mfc-map-mr') || $c.hasClass('mfc-map-ml'))
				newx = newx + dirx * step;
			if ($c.hasClass('mfc-map-mt') || $c.hasClass('mfc-map-mb'))
				newy = newy + diry * step;

			//if (newx < -660 || newx > 50 || newy < -850 || newy > -150)
				//return;

			if (newx > MFC.limit[0]*1 && newx < MFC.limit[2]*1)
				MFC.$map.css('left', newx);

			if (newy > MFC.limit[1]*1 && newy < MFC.limit[3]*1)
				MFC.$map.css('top', newy);

		});
	}
}

$(function(){

	MFC.$map = $('#mfc-map');
	MFC.address_loader = MFC.$map.attr('map:address-loader');

	$.ajax({
		url: MFC.address_loader+'load',
		dataType: 'json',
		success: function(data) {
			MFC.items = data;
			MFC.init();
		}
	});

	$lists_free = $('#mfc-list-free');
	$.ajax({
		url: MFC.address_loader+'loadFree',
		dataType: 'json',
		success: function(data) {

			if (!data.length) {
				$('#mfc-list-free-title').text('Свободные участки отсутствуют');
				return;
			}

			var changeTitle = false;
			for (var i = 0; i < data.length; i++){
				if (!changeTitle && data[i].descr == 1) changeTitle = true;
				$li = $('<li>'+data[i].settlement+', '+data[i].street+', '+data[i].number+'</li>');
				if (data[i].descr == 1){
					$li.append('<br />(Принято&nbsp;заявок:&nbsp;' + data[i].count_pack + ')');
				}

				$li.appendTo($lists_free);
			}

			if (changeTitle){
				$('#mfc-list-free-title').text('Свободные участки, участки, на которые поданы заявки');
			}
		}
	});
	//

});
