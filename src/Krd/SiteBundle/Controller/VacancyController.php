<?php

namespace Krd\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Вакансии
 */
class VacancyController extends Controller implements ActiveSecuredController
{
    /**
     * Список
     *
     * @Route("/")
     * @Template("KrdSiteBundle:Content:index.html.twig")
     */
    public function listAction()
    {

    }

    /**
     * Страница подробно
     *
     * @Route("/{name}.html", name="krd_vacancy_detail")
     * @Template()
     */
    public function detailAction()
    {
        if ($item = $this->get('krd.site.modules.vacancy')->getCurrent()) {
            $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => $item->getTitle(), 'url' => $item->getUrl()));
        } else {
            throw $this->createNotFoundException();
        }
    }
}
