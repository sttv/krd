<?php

namespace Q\CoreBundle\Navigation;

/**
 * Пункт меню управления структурой сайта
 */
class Structure extends Item
{
    public function getTitle()
    {
        return 'Структура сайта';
    }

    public function getLink()
    {
        return $this->router->generate('cms_struct_index');
    }

    public function isGranted()
    {
        return $this->securityContext->isGranted('ROLE_CMS');
    }
}
