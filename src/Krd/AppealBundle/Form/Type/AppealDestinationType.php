<?php

namespace Krd\AppealBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Router\CurrentNode;


/**
 * Назначение обращения
 */
class AppealDestinationType extends AbstractType
{
    protected $em;
    protected $cNode;

    public function __construct(EntityManager $em, CurrentNode $cNode)
    {
        $this->em = $em;
        $this->cNode = $cNode;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->cNode->hasNode()) {
            $parentId = $this->cNode->getNode()->getId();
        } else {
            $parentId = null;
        }

        $resolver->setDefaults(array(
            'multiple' => false,
            'required' => true,
            'expanded' => true,
            'class' => 'KrdAppealBundle:Destination',
            'query_builder' => function(EntityRepository $er) use ($parentId) {
                $qb = $er->createQueryBuilder('e')
                    ->andWhere('e.active = 1')
                    ->addOrderBy('e.group', 'ASC')
                    ->addOrderBy('e.sort', 'ASC');

                if (!is_null($parentId)) {
                    $qb->andWhere('e.parent = :parent')
                        ->setParameter('parent', $parentId);
                }

                return $qb;
            },
        ));

        $resolver->addAllowedValues(array(
            'multiple' => array(false),
            'expanded' => array(true),
            'class' => array('KrdAppealBundle:Destination'),
        ));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        foreach($view as $child) {
            $id = (int)$child->vars['value'];
            $child->vars['entity'] = $this->em->getRepository('KrdAppealBundle:Destination')->find($id);
        }
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'appeal_destination';
    }
}
