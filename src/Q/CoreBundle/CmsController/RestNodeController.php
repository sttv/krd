<?php

namespace Q\CoreBundle\CmsController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\NodeController;


/**
 * Контроллер структуры
 *
 * @Route("/rest/q/core/node")
 * @PreAuthorize("hasRole('ROLE_CMS')")
 */
class RestNodeController extends Controller
{
    /**
     * Генерация имени формы
     * @param  string $prefix
     * @return string
     */
    protected function getFormName($prefix = '')
    {
        return $prefix.'qcorebundle_node';
    }

    /**
     * Список нодов
     * Можно передавать $_GET['parent'] для выборки по радителю
     *
     * @Route("/", name="cms_rest_node", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restListAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('g')
            ->from('QCoreBundle:Node', 'g');

        if ($parent = $request->get('parent')) {
            $query->andWhere('g.parent = :parent');
            $query->setParameter('parent', $parent);
        } else {
            $query->andWhere('g.level = 0');
        }

        $query->orderBy('g.left', 'ASC');

        Paginator::disableQueryCache();

        return Paginator::createFromRequest($request, $query->getQuery());
    }

    /**
     * Получение ноды
     *
     * @Route("/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restGetAction($id)
    {
        try {
            return $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QCoreBundle:Node g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Node controller not found');
        }
    }

    /**
     * Получение ноды по URL
     *
     * @Route("/byurl/", name="cms_rest_node_byurl", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function restGetByUrlAction(Request $request)
    {
        $url = $request->query->get('url');

        if (!$url) {
            return array('success' => false, 'error' => 'Url not set');
        }

        if (mb_strpos($url, 'http://') === 0) {
            $url = mb_substr($url, 7);
        }

        if (mb_strpos($url, 'https://') === 0) {
            $url = mb_substr($url, 8);
        }

        if (mb_strpos($url, $request->getHost()) === 0) {
            $url = mb_substr($url, mb_strlen($request->getHost()));
        }

        $cmsStructIndex = $this->get('router')->generate('cms_struct_index');
        if (mb_strpos($url, $cmsStructIndex) === 0) {
            $url = mb_substr($url, mb_strlen($cmsStructIndex));
        }

        $node = $this->getRepository()->getNodeByPath($url);

        if (!$node) {
            return array('success' => false, 'error' => 'Node not found');
        }

        return array(
            'success' => true,
            'node' => $node,
        );
    }

    /**
     * Перемещение ноды
     * @Route("/move/{id}/", name="cms_rest_node_move", defaults={"_format"="json"}, requirements={"id"="\d+"})
     * @Method("POST")
     */
    public function restMoveNode(Request $request, $id)
    {
        $id2 = $request->request->get('id2');
        $movetype = $request->request->get('movetype');

        $em = $this->getDoctrine()->getManager();

        $node = $this->getRepository()->find($id);
        $node2 = $this->getRepository()->find($id2);

        if (!$node || !$node2) {
            return array('success' => false, 'error' => 'Node not found');
        }

        $em->getConnection()->beginTransaction();
        $this->getRepository()->moveNode($node, $node2, $movetype);
        $this->getRepository()->updateUrl($node);
        $em->getConnection()->commit();

        return array('success' => true, 'redirect' => $this->get('router')->generate('cms_struct_index', array('url' => $node->getUrl(false))));
    }

    /**
     * Создание ноды
     *
     * @Route("/", name="cms_rest_node_create", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostAddAction(Request $request)
    {
        $node = new Node();

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, array('menutype'), $this->getFormName('create_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($node, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($parent = $node->getParent()) {
                foreach ($parent->getUsers() as $user) {
                    $node->addUser($user);
                }

                foreach ($parent->getUsergroups() as $group) {
                    $node->addUsergroup($group);
                }
            }

            $this->getDoctrine()->getManager()->beginTransaction();
            $this->getRepository()->update($node);
            $this->getRepository()->persistAsFirstChild($node);
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getManager()->commit();
            return array('success' => true, 'node' => $node);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Редактирование ноды
     *
     * @Route("/{id}", name="cms_rest_node_edit", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditAction(Request $request, $id)
    {
        try {
            $node = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QCoreBundle:Node g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Node controller not found');
        }

        $this->get('qcore.form.helper.request')->parseMultipleSelect($request, array('menutype'), $this->getFormName('edit_'));

        $form = $this->get('form_metadata.mapper')->createFormBuilder($node, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->beginTransaction();
            $this->getRepository()->update($node);
            $this->getDoctrine()->getManager()->commit();
            return array('success' => true, 'node' => $node);
        } else {
            return array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form));
        }
    }

    /**
     * Установка параметров
     *
     * @Route("/set/{id}", requirements={"id"="\d+"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function restPostEditSetAction(Request $request, $id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            return array('success' => false, 'error' => 'Entity not found');
        }

        $entity->setActive($request->get('active', false) === true || $request->get('active', false) == 'true');

        $this->getDoctrine()->getManager()->flush();

        return $entity;
    }

    /**
     * Удаление ноды
     *
     * @Route("/{id}", name="cms_rest_node_delete", defaults={"_format"="json"})
     * @Method("DELETE")
     */
    public function restPostDeleteAction(Request $request, $id)
    {
        try {
            $node = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QCoreBundle:Node g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Node controller not found');
        }

        $this->getDoctrine()->getManager()->remove($node);
        $this->getDoctrine()->getManager()->flush();

        return array('success' => true);
    }

    /**
     * Рендер формы добавления
     * Можно передать $_GET['parent'] для добавления ноды в качестве дочерней
     *
     * @Route("/form/", name="cms_rest_node_form_add", defaults={"_format"="html"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetAddForm(Request $request)
    {
        $node = new Node();
        $node->setTitle('Подраздел');

        $defCrtl = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:NodeController')->find(2);

        if ($defCrtl) {
            $node->setController($defCrtl);
        }

        if (($parent = $request->get('parent')) && ($parent = $this->getRepository()->find($parent))) {
            $node->setParent($parent);
            $node->setActive($parent->isActive());
            // $node->setController($parent->getController());
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($node, null, array('csrf_protection' => false), $this->getFormName('create_'))
            ->setAction($this->generateUrl('cms_rest_node_create'))
            ->getForm();

        return array('form' => $form->createView());
    }

    /**
     * Рендер формы редактирования
     * @Route("/form/{id}", name="cms_rest_node_form_edit", defaults={"_format"="html"}, requirements={"id"="\d+"})
     * @Method("GET")
     * @Template("QCoreBundle::Form/default-form.html.twig")
     */
    public function restGetEditForm($id)
    {
        try{
            $node = $this->getDoctrine()->getManager()->createQuery("SELECT g FROM QCoreBundle:Node g WHERE g.id = :id")->setParameter('id', $id)->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw $this->createNotFoundException('Node controller not found');
        }

        $form = $this->get('form_metadata.mapper')->createFormBuilder($node, null, array('csrf_protection' => false), $this->getFormName('edit_'))
            ->setAction($this->generateUrl('cms_rest_node_edit', array('id' => $id)))
            ->getForm();

        return array('form' => $form->createView(), 'node' => $node);
    }

    /**
     * Сортировка элемента
     *
     * @Route("/sort/{order}", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function actionSort(Request $request, $order)
    {
        $id = $request->request->get('id');
        $parent = $request->get('parent');

        if (!$id || !$parent) {
            throw $this->createNotFoundException();
        }

        $node = $this->getRepository()->findOneBy(array('id' => $id, 'parent' => $parent));

        if (!$node) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        // Поставить элемент перед каким-то конкретным
        if (preg_match('/^before-\d+$/', $order)) {
            $nextNode = $this->getRepository()->findBy(array(
                'id' => str_replace('before-', '', $order),
                'parent' => $node->getParent()->getId(),
            ));

            if (!$nextNode) {
                throw $this->createNotFoundException();
            } else {
                $nextNode = $nextNode[0];
            }

            $this->getRepository()->moveNode($node, $nextNode, 'prev');
            $em->getConnection()->commit();

            return array('success' => true, 'moved' => true);
        }

        // Поставить элемент после какого-то конкретного
        if (preg_match('/^after-\d+$/', $order)) {
            $prevNode = $this->getRepository()->findBy(array(
                'id' => str_replace('after-', '', $order),
                'parent' => $node->getParent()->getId(),
            ));

            if (!$prevNode) {
                throw $this->createNotFoundException();
            } else {
                $prevNode = $prevNode[0];
            }

            $this->getRepository()->moveNode($node, $prevNode, 'next');
            $em->getConnection()->commit();

            return array('success' => true, 'moved' => true);
        }

        switch($order) {
            case 'up':
                $prevNode = $this->getRepository()->getPrevItem($node, $parent);

                if (!$prevNode) {
                    return array('success' => true, 'moved' => false);
                } else {
                    $this->getRepository()->moveNode($node, $prevNode, 'prev');
                    $em->getConnection()->commit();

                    return array('success' => true, 'moved' => true);
                }

            case 'down':
                $nextNode = $this->getRepository()->getNextItem($node, $parent);

                if (!$nextNode) {
                    return array('success' => true, 'moved' => false);
                } else {
                    $this->getRepository()->moveNode($node, $nextNode, 'next');
                    $em->getConnection()->commit();

                    return array('success' => true, 'moved' => true);
                }

            default:
                return array('success' => false, 'moved' => false);
        }
    }

    /**
     * Редактирование доступа к разделу
     *
     * @Route("/ajax/node_security/{id}/", name="cms_rest_node_sucurity_edit", defaults={"_format"="json"})
     * @Method("POST")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function securityEditAction(Request $request, $id)
    {
        $node = $this->getRepository()->find($id);

        if (!$node) {
            throw $this->createNotFoundException('Node controller not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $nodes = array($node->getId());
        $users = array();
        $groups = array();

        $usersWas = array();
        $groupsWas = array();

        foreach ($node->getUsers() as $user) {
            $usersWas[] = $user->getId();
        }

        foreach ($node->getUsergroups() as $group) {
            $groupsWas[] = $group->getId();
        }

        // Добавляем разрешения для текущего раздела
        $data = $request->request->get('node_security');

        if (isset($data['usergroups'])) {
            foreach ($data['usergroups'] as $item) {
                if (is_array($item)) {
                    foreach ($item as $id) {
                        $groups[] = $id;
                    }
                } elseif (is_numeric($item)) {
                    $groups[] = $item;
                }
            }
        }

        if (isset($data['users'])) {
            foreach ($data['users'] as $item) {
                if (is_array($item)) {
                    foreach ($item as $id) {
                        $users[] = $id;
                    }
                } elseif (is_numeric($item)) {
                    $users[] = $item;
                }
            }
        }

        $deletedGroups = array_diff($groupsWas, $groups);
        $newGroups = array_diff($groups, $groupsWas);

        $deletedUsers = array_diff($usersWas, $users);
        $newUsers = array_diff($users, $usersWas);

        $nodeTable = $em->getClassMetadata('QCoreBundle:Node')->getTableName();

        $st = $em->getConnection()->prepare("SELECT `id` FROM `{$nodeTable}` WHERE `left` > :left AND `right` < :right ORDER BY `left`");
        $st->execute(array('left' => $node->getLeft(), 'right' => $node->getRight()));

        while ($subnode = $st->fetch()) {
            $nodes[] = (int)$subnode['id'];
        }

        $userRelTable = $em->getClassMetadata('QCoreBundle:Node')->getAssociationMapping('users');
        $userRelTable = $userRelTable['joinTable']['name'];

        $groupRelTable = $em->getClassMetadata('QCoreBundle:Node')->getAssociationMapping('usergroups');
        $groupRelTable = $groupRelTable['joinTable']['name'];

        $em->clear();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        foreach ($nodes as $nodeId) {
            foreach ($deletedUsers as $userId) {
                $em->getConnection()->delete($userRelTable, array('node_id' => $nodeId, 'user_id' => $userId));
            }

            foreach ($deletedGroups as $groupId) {
                $em->getConnection()->delete($groupRelTable, array('node_id' => $nodeId, 'group_id' => $groupId));
            }

            foreach ($newUsers as $userId) {
                $em->getConnection()->delete($userRelTable, array('node_id' => $nodeId, 'user_id' => $userId));
                $em->getConnection()->insert($userRelTable, array('node_id' => $nodeId, 'user_id' => $userId));
            }

            foreach ($newGroups as $groupId) {
                $em->getConnection()->delete($groupRelTable, array('node_id' => $nodeId, 'group_id' => $groupId));
                $em->getConnection()->insert($groupRelTable, array('node_id' => $nodeId, 'group_id' => $groupId));
            }
        }

        $em->getConnection()->commit();

        return array('success' => true);
    }

    /**
     * Репозиторй
     * @return NodeRepository
     */
    protected function getRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node');
    }
}
