<?php

namespace Krd\UserBundle\Repository;

use JMS\DiExtraBundle\Annotation as JMSDI;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\Entity\Bookmark;


/**
 * Репозиторий пользовательских закладок
 */
class BookmarkRepository extends EntityRepository
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @JMSDI\InjectParams({
     *     "securityContext" = @JMSDI\Inject("security.context")
     * })
     */
    public function setUserBySecurityContext(SecurityContext $securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * Создание новой закладки
     * @param  array $data
     * @return Bookmark|false
     */
    public function createMyBookmark($data)
    {
        if ($this->user instanceof User) {
            if (isset($data['directory']) && !empty($data['directory'])) {
                $directory = $this->_em->getRepository('KrdUserBundle:BookmarkDirectory')->getMyDirectory($data['directory']);

                if (!$directory) {
                    return false;
                }
            }

            $bookmark = new Bookmark();

            if (isset($data['title']) && !empty($data['title'])
                && isset($data['url']) && !empty($data['url'])) {

                $title = strip_tags($data['title']);

                $url = strip_tags($data['url']);

                $bookmark->setTitle($title);
                $bookmark->setUrl($url);
                $bookmark->setParent($directory);
                $directory->addBookmark($bookmark);

                $this->_em->persist($bookmark);
                $this->_em->flush($bookmark);

                return $bookmark;
            }
        } else {
            return false;
        }
    }

    /**
     * Получение закладки пользователя
     * @param  integer $id
     * @return Bookmark|Null
     */
    public function getMyBookmark($id)
    {
        if ($this->user instanceof User) {
            return $this->findOneBy(array('id' => $id, 'createdBy' => $this->user->getId()));
        }
    }

    /**
     * Получение закладок пользователя
     * @param  integer[optional] $count
     * @return ArrayCollection|Null
     */
    public function getMyBookmarks($count = null)
    {
        if ($this->user instanceof User) {
            $qb = $this->createQueryBuilder('e')
                ->andWhere('e.createdBy = :user')
                ->addOrderBy('e.created', 'DESC')
                ->setParameter('user', $this->user->getId());

            if (is_numeric($count) && $count > 0) {
                $qb->setMaxResults($count);
            }

            return $qb->getQuery()->getResult();
        }

        return new ArrayCollection();
    }

    /**
     * Редактирование параметров закладки текущего пользователя
     * @param  integer $id
     * @param  array $data
     * @return Bookmark|false
     */
    public function editMyDocument($id, $data)
    {
        if ($this->user instanceof User) {
            $bookmark = $this->getMyBookmark($id);

            if ($bookmark) {
                if (isset($data['title']) && !empty($data['title'])) {
                    $title = strip_tags($data['title']);
                    $bookmark->setTitle($title);
                }


                $this->_em->flush($bookmark);
                return $bookmark;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Удаление закладки текущего пользователя по ID
     * @param  integer $id
     * @return boolean
     */
    public function removeMyBookmark($id)
    {
        if ($this->user instanceof User) {
            $bookmark = $this->getMyBookmark($id);

            if ($bookmark) {
                $this->_em->remove($bookmark);
                $this->_em->flush($bookmark);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
