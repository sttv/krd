<?php

namespace Krd\CityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\Node;
use Q\FormMetadataBundle\Configuration as Form;


/**
 * Гостиницы
 *
 * @ORM\Entity
 * @ORM\Table(name="city_hotel",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="category", columns={"category"}),
 *          @ORM\Index(name="category_sort", columns={"category", "sort"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="sort", columns={"sort"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     category={"text", {"label"="Категория"}},
 *     address={"text", {"label"="Адрес"}},
 *     website={"text", {"label"="Сайт", "required"=false}},
 *     roomCount={"text", {"label"="Кол-во номеров"}},
 *     bedCount={"text", {"label"="Кол-во койко-мест"}},
 *     phone={"text_collection", {
 *          "label"="Номер телефона",
 *          "required"=false,
 *          "options"={"attr"={"ui-mask"="+7-(999)-999-99-99"}},
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Hotel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Название
     * @ORM\Column(type="string")
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Категория
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $category;

    /**
     * Адрес
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $address;

    /**
     * Ссылка на сайт
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $website;

    /**
     * Количество номеров
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $roomCount;

    /**
     * Количество кофко-мест
     * @ORM\Column(type="string", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     * @Assert\NotBlank
     */
    private $bedCount;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @JMS\Expose
     * @Gedmo\Versioned
     */
    private $phone = array();

    /**
     * Активность
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Сортировка
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Hotel
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Hotel
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Hotel
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Hotel
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    public function hasWebsite()
    {
        $asd = $this->getWebsite();
        return !empty($asd);
    }

    /**
     * Set roomCount
     *
     * @param string $roomCount
     * @return Hotel
     */
    public function setRoomCount($roomCount)
    {
        $this->roomCount = $roomCount;

        return $this;
    }

    /**
     * Get roomCount
     *
     * @return string
     */
    public function getRoomCount()
    {
        return $this->roomCount;
    }

    /**
     * Set bedCount
     *
     * @param string $bedCount
     * @return Hotel
     */
    public function setBedCount($bedCount)
    {
        $this->bedCount = $bedCount;

        return $this;
    }

    /**
     * Get bedCount
     *
     * @return string
     */
    public function getBedCount()
    {
        return $this->bedCount;
    }

    /**
     * Set phone
     *
     * @param array $phone
     * @return Hotel
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return array
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Hotel
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return Hotel
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Hotel
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Hotel
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Q\CoreBundle\Entity\Node $parent
     * @return Hotel
     */
    public function setParent(\Q\CoreBundle\Entity\Node $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Q\CoreBundle\Entity\Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Hotel
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Hotel
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
