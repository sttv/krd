<?php

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

error_reporting(E_ALL & ~(E_WARNING|E_NOTICE));
ini_set('display_errors', 0);

// Define TMP directory
putenv("TMPDIR=".realpath(__DIR__.'/../app/tmp/'));

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
$loader = new ApcClassLoader('krd.', $loader);
$loader->register(true);

require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';

$ru = $_SERVER['REQUEST_URI'];
$ru = explode('?', $ru);
$ru = $ru[0];

if (rtrim($ru, '/') == '/pdf' && $_GET['pass'] != 'krdru') {
    die('Access denied');
}

$kernel = new AppKernel('prod', false);
// $kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$kernel = new AppCache($kernel);
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
