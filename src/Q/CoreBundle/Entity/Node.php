<?php

namespace Q\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\ArrayCollection;

use Q\FormMetadataBundle\Configuration as Form;
use Q\UserBundle\Entity\User;
use Q\UserBundle\Entity\UserGroup;


/**
 * Страница сайта
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Q\CoreBundle\Repository\NodeRepository")
 * @ORM\Table(name="node",
 *      indexes={
 *          @ORM\Index(name="root", columns={"root"}),
 *          @ORM\Index(name="leftkey", columns={"`left`"}),
 *          @ORM\Index(name="rightkey", columns={"`right`"}),
 *          @ORM\Index(name="lvl", columns={"level"}),
 *          @ORM\Index(name="l_r", columns={"`left`", "`right`"}),
 *          @ORM\Index(name="left_root", columns={"`left`", "root"}),
 *          @ORM\Index(name="right_root", columns={"`right`", "root"}),
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *          @ORM\Index(name="commentable", columns={"commentable"}),
 *          @ORM\Index(name="urlhash", columns={"urlhash"}),
 *          @ORM\Index(name="customurlhash", columns={"customurlhash"}),
 *          @ORM\Index(name="urlaliashash", columns={"urlaliashash"}),
 *          @ORM\Index(name="hashindex", columns={"urlhash", "urlaliashash"}),
 *      }
 * )
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Заголовок"}},
 *     titleShort={"text", {"label"="Сокр. заголовок", "required"=false}},
 *     name={"text", {
 *         "label"="Системное имя",
 *         "required"=false,
 *         "attr"={
 *             "nodename"="create_qcorebundle_node[title]"
 *         }
 *     }},
 *     customurl={"text", {"label"="Собственный URL <em>(макс. 255 симв.)</em>", "required"=false}},
 *     redirect={"text", {"label"="Переадресация", "required"=false}},
 *     justPassword={"text", {"label"="Пароль страницы", "required"=false}},
 *     controller={"entity", {
 *         "label"="Тип страницы",
 *         "class"="QCoreBundle:NodeController",
 *         "property"="name",
 *         "multiple"=false,
 *         "required"=true
 *     }},
 *     menutype={"entity", {
 *         "label"="Меню",
 *         "class"="QCoreBundle:MenuType",
 *         "property"="title",
 *         "multiple"=true,
 *         "required"=false
 *     }},
 *     ssl={"checkbox", {
 *         "label"="Только SSL",
 *         "required"=false
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     commentable={"checkbox", {
 *         "label"="Разрешить комментарии",
 *         "required"=false
 *     }},
 *     seoTitle={"text", {
 *         "label"="SEO Title",
 *         "required"=false
 *     }},
 *     seoKeywords={"textarea", {
 *         "label"="SEO Keywords",
 *         "required"=false
 *     }},
 *     seoDescription={"textarea", {
 *         "label"="SEO Description",
 *         "required"=false
 *     }},
 *     seoImage={"text", {
 *         "label"="SEO Image (ссылка)",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Node
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="`left`", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="`right`", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Node", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Node", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    private $children;

    /**
     * Системное имя раздела
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\NodeName
     */
    private $name = '';

    /**
     * Читабельный заголовок раздела
     * @ORM\Column(type="text")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title = '';

    /**
     * Сокращенный заголовок раздела
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $titleShort = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $url = '';

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $urlhash = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\Regex(pattern="/^\/[a-z0-9\-_]+\/$/", message="Должен быть задан относительный URL. Например: /someurl/example/")
     */
    private $customurl = NULL;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $customurlhash = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $urlalias = NULL;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $urlaliashash = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $redirect = '';

    /**
     * Один из контроллеров, определенных в конфиг-файле
     * @ORM\ManyToOne(targetEntity="NodeController")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $controller = '';

    /**
     * @ORM\ManyToMany(targetEntity="MenuType")
     * @ORM\JoinTable(name="node_menu_type_relation",
     *      joinColumns={@ORM\JoinColumn(name="node_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="menu_type_id", referencedColumnName="id")}
     * )
     *
     * @JMS\Expose
     */
    protected $menutype;

    /**
     * Список дочерних пунктов меню
     * Не является ORM
     * Заполняется в сервисе qcore.front.menu
     */
    protected $menuchildren;

    /**
     * Активность пункта в меню
     * Не является ORM
     * Заполняется в сервисе qcore.front.menu
     */
    protected $menuactive = false;

    /**
     * Активность категории для фронтэнда
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

     /**
     * Разрешить комментарии?
     * @ORM\Column(type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @Gedmo\Versioned
     * @Assert\Type("boolean")
     */
    private $commentable = false;

    /**
     * Принудительное использование HTTPS схемы
     *
     * @ORM\Column(type="boolean", name="force_ssl")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $ssl = false;

    /**
     * Пароль для страницы
     * @ORM\Column(type="string", name="just_password", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $justPassword;

    /**
     * SEO Title
     * @ORM\Column(type="text", name="seo_title", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $seoTitle = '';

    /**
     * SEO keywords
     * @ORM\Column(type="text", name="seo_keywords", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $seoKeywords = '';

    /**
     * SEO Description
     * @ORM\Column(type="text", name="seo_description", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $seoDescription = '';

    /**
     * SEO Image
     * @ORM\Column(type="text", name="seo_image", nullable=true)
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     */
    private $seoImage = '';

    /**
     * @ORM\OneToMany(targetEntity="Content", mappedBy="parent")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $content;

    /**
     * Список групп, имеющих доступ к редактированию
     *
     * @ORM\ManyToMany(targetEntity="Q\UserBundle\Entity\UserGroup")
     * @ORM\JoinTable(name="node_user_group_relation",
     *      joinColumns={@ORM\JoinColumn(name="node_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     *
     * @JMS\Expose
     */
    protected $usergroups;

    /**
     * Список пользователей имеющих доступ к разделу
     *
     * @ORM\ManyToMany(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinTable(name="node_user_relation",
     *      joinColumns={@ORM\JoinColumn(name="node_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     *
     * @JMS\Expose
     */
    protected $users;

    /**
     * Является ли узел родительским.
     * Не записывается в БД. Используется исключительно для сериализации
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\SerializedName("isParent")
     * @JMS\Accessor(getter="isParent")
     */
    private $isParent = false;

    /**
     * Является ли узел корнем.
     * Не записывается в БД. Используется исключительно для сериализации
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\SerializedName("isRoot")
     * @JMS\Accessor(getter="isRoot")
     */
    private $isRoot = false;

    /**
     * Имеется ли админ-доступ к этому разделу
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\SerializedName("isGranted")
     * @JMS\Accessor(getter="isGranted")
     */
    private $isGranted = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Произвольные хранимые данные
     * @var array
     */
    private $customData = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->menutype = new \Doctrine\Common\Collections\ArrayCollection();
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usergroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLeft()
    {
        return $this->left;
    }

    public function getRight()
    {
        return $this->right;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function addChild(Node $node)
    {
        $node->setParent($this);
    }

    public function getChildren()
    {
        return $this->children ?: $this->children = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = mb_strtolower($name);
    }

    public function getTitle()
    {
        return $this->title;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitleShort()
    {
        return $this->titleShort;
    }

    function setTitleShort($titleShort)
    {
        $this->titleShort = $titleShort;
    }

    public function getFrontendTitle()
    {
        if (!empty($this->titleShort)) {
            return $this->titleShort;
        } else {
            return $this->title;
        }
    }

    /**
     * Ссылка на нод
     * @param  boolean[optional] $alias Если указать, то будет возвращена ссылка на актуальный алиас
     * @return string
     */
    public function getUrl($alias = false)
    {
        if ($alias && $this->hasRedirect()) {
            return $this->getRedirect();
        } elseif ($alias && $this->hasUrlAlias()) {
            return $this->getUrlAlias();
        } else {
            return $this->url;
        }
    }

    /**
     * Проверка, является ли URL внешним
     * @return boolean
     */
    public function isExternalUrl()
    {
        return (mb_strpos($this->getUrl(true), 'http:') === 0)
            || (mb_strpos($this->getUrl(true), 'https:') === 0)
            || (mb_strpos($this->getUrl(true), '//') === 0)
        ;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setUrlHash($urlhash)
    {
        $this->urlhash = $urlhash;
    }

    public function getCustomUrl()
    {
        return $this->customurl;
    }

    public function setCustomUrl($customurl)
    {
        $this->customurl = $customurl;
    }

    public function setCustomUrlHash($customurlhash)
    {
        $this->customurlhash = $customurlhash;
    }

    public function hasCustomUrl()
    {
        return !empty($this->customurl);
    }

    public function getUrlAlias()
    {
        return $this->urlalias;
    }

    public function setUrlAlias($urlalias)
    {
        $this->urlalias = $urlalias;
    }

    public function setUrlAliasHash($urlaliashash)
    {
        $this->urlaliashash = $urlaliashash;
    }

    public function hasUrlAlias()
    {
        return !empty($this->urlalias);
    }

    public function getRedirect()
    {
        return $this->redirect;
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    public function hasRedirect()
    {
        return !empty($this->redirect);
    }

    function getController()
    {
        return $this->controller;
    }

    function setController(NodeController $controller)
    {
        $this->controller = $controller;
    }

    public function getMenutype()
    {
        return $this->menutype ?: $this->menutype = new ArrayCollection();
    }

    public function addMenutype(MenuType $menutype)
    {
        if (!$this->getMenutype()->contains($menutype)) {
            $this->getMenutype()->add($menutype);
        }
    }

    public function removeMenutype(MenuType $menutype)
    {
        if ($this->getMenutype()->contains($menutype)) {
            $this->getMenutype()->removeElement($menutype);
        }
    }

    public function clearMenutype()
    {
        $this->menutype = new ArrayCollection();
    }

    /**
     * Проверяет принадлежность нода к пункту меню по указанному имени
     * @param  string $menutype
     * @return boolean
     */
    public function inMenu($menutype)
    {
        foreach($this->getMenutype() as $mt) {
            if ($mt->getName() == $menutype) {
                return true;
            }
        }

        return false;
    }

    public function getMenuChildren()
    {
        return $this->menuchildren ?: $this->menuchildren = new ArrayCollection();
    }

    public function addMenuChildren(Node $node)
    {
        if (!$this->getMenuChildren()->contains($node)) {
            $this->getMenuChildren()->add($node);
        }
    }

    public function removeMenuChildren(Node $node)
    {
        if ($this->getMenuChildren()->contains($node)) {
            $this->getMenuChildren()->removeElement($node);
        }
    }

    public function clearMenuChildren()
    {
        $this->menuchildren = new ArrayCollection();
    }

    public function getMenuActive()
    {
        return (boolean)$this->menuactive;
    }

    public function setMenuActive($menuactive)
    {
        $this->menuactive = (boolean)$menuactive;
    }

    public function isActive()
    {
        return (boolean)$this->active;
    }

    public function setActive($value)
    {
        $this->active = (boolean)$value;
    }

    public function isCommentable()
    {
        return (boolean)$this->commentable;
    }

    public function setCommentable($value)
    {
        $this->commentable = (boolean)$value;
    }

    public function isSsl()
    {
        return (boolean)$this->ssl;
    }

    public function setSsl($ssl)
    {
        $this->ssl = (boolean)$ssl;
    }

    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    public function hasSeoTitle()
    {
        return !empty($this->seoTitle);
    }

    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    public function hasSeoKeywords()
    {
        return !empty($this->seoKeywords);
    }

    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    }

    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    public function hasSeoDescription()
    {
        return !empty($this->seoDescription);
    }

    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    }

    public function getSeoImage()
    {
        return $this->seoImage;
    }

    public function hasSeoImage()
    {
        return !empty($this->seoImage);
    }

    public function setSeoImage($seoImage)
    {
        $this->seoImage = $seoImage;
    }

    /**
     * Проверка является ли нода - корнем
     * @return boolean
     */
    public function isRoot()
    {
        return is_null($this->parent);
    }

    /**
     * Проверка, является ли элемент родителем
     * @return boolean
     */
    public function isParent()
    {
        return ($this->right - $this->left) > 1;
    }

    public function addContent(Content $content)
    {
        $content->setParent($this);
    }

    public function getContent($name = null)
    {
        if (is_null($name)) {
            return $this->content ?: $this->content = new ArrayCollection();
        } else {
            $criteria = Criteria::create()
                ->andWhere(Criteria::expr()->eq('name', $name))
                ->orderBy(array('name' => Criteria::ASC));

            $result = $this->getQuestions()->matching($criteria);

            if (isset($result[0])) {
                return $result[0];
            } else {
                return null;
            }
        }
    }

    /**
     * Возвращает первый контент
     * @return Content|null
     */
    public function getFirstContent()
    {
        $result = $this->getContent();

        if (isset($result[0])) {
                return $result[0];
            } else {
                return null;
            }
    }

    /**
     * Возвращает произвольно хранимые данные по ключу
     * Если данных нет, возвращает $default
     * @return mixed
     */
    public function getCustomData($key = null, $default = null)
    {
        if (!is_null($key)) {
            return isset($this->customData[$key]) ? $this->customData[$key] : $default;
        } else {
            return $this->customData;
        }
    }

    /**
     * Устанавливает произвольные данные
     * @param string $key
     * @param mixed $data
     */
    public function setCustomData($key, $data)
    {
        $this->customData[$key] = $data;
    }

    /**
     * Проверяет наличие произвольных данных
     * @param  string  $key
     * @return boolean
     */
    public function hasCustomData($key)
    {
        return isset($this->customData[$key]) && !empty($this->customData[$key]);
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Проверяет доступ на редактирвоание раздела у текущего пользователя
     * @return boolean
     */
    public function isGranted()
    {
        if (!$this->securityContext) {
            return false;
        }

        try {
            // Не используйте isGranted, т.к. падает в 502
            if ($this->securityContext->getToken()->getUser()->hasRole('ROLE_CMS_ADMIN')) {
                return true;
            }

            if ($this->securityContext->getToken()->getUser()->hasRole('ROLE_IDDQD')) {
                return true;
            }

            if ($user = $this->securityContext->getToken()->getUser()) {
                if ($this->users->contains($user)) {
                    return true;
                }

                foreach ($user->getGroups() as $group) {
                    if ($this->usergroups->contains($group)) {
                        return true;
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return false;
    }

    /**
     * Add usergroups
     *
     * @param \Q\UserBundle\Entity\UserGroup $usergroups
     * @return Node
     */
    public function addUsergroup(\Q\UserBundle\Entity\UserGroup $usergroups)
    {
        $this->usergroups[] = $usergroups;

        return $this;
    }

    /**
     * Remove usergroups
     *
     * @param \Q\UserBundle\Entity\UserGroup $usergroups
     */
    public function removeUsergroup(\Q\UserBundle\Entity\UserGroup $usergroups)
    {
        $this->usergroups->removeElement($usergroups);
    }

    public function removeUsergroups()
    {
        foreach ($this->getUsergroups() as $group) {
            $this->removeUsergroup($group);
        }
    }

    /**
     * Get usergroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsergroups()
    {
        return $this->usergroups;
    }

    /**
     * Add users
     *
     * @param \Q\UserBundle\Entity\User $users
     * @return Node
     */
    public function addUser(\Q\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Q\UserBundle\Entity\User $users
     */
    public function removeUser(\Q\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    public function removeUsers()
    {
        foreach ($this->getUsers() as $user) {
            $this->removeUser($user);
        }
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return boolean
     */
    public function isUnderJustPassword()
    {
        return !empty($this->justPassword);
    }

    /**
     * @return mixed
     */
    public function getJustPassword()
    {
        return $this->justPassword;
    }

    /**
     * @param mixed $justPassword
     */
    public function setJustPassword($justPassword)
    {
        $this->justPassword = $justPassword;
    }
}
