<?php

namespace Krd\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;


/**
 * Опрос
 *
 * @ORM\Entity
 * @ORM\Table(name="poll",
 *      indexes={
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Вопрос"}},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Poll
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;


    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * Варианты ответов
     *
     * @ORM\OneToMany(targetEntity="PollAnswer", mappedBy="parent", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $answers;

    /**
     * Список ответов отсортированный по результатам
     *
     * @JMS\Expose
     * @JMS\Type("array")
     * @JMS\SerializedName("answers_by_counter")
     * @JMS\Accessor(getter="getAnswersByCounter")
     */
    private $answersByCounter;

    /**
     * @ORM\Column(type="array")
     *
     * @JMS\Expose
     * @JMS\Type("array")
     *
     * @Gedmo\Versioned
     */
    private $votedip = array();

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Poll
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Poll
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Poll
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Poll
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \Q\CoreBundle\Entity\Node $parent
     * @return Poll
     */
    public function setParent(\Q\CoreBundle\Entity\Node $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Q\CoreBundle\Entity\Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add answers
     *
     * @param \Krd\SiteBundle\Entity\PollAnswer $answers
     * @return Poll
     */
    public function addAnswer(\Krd\SiteBundle\Entity\PollAnswer $answers)
    {
        $this->answers[] = $answers;

        return $this;
    }

    /**
     * Remove answers
     *
     * @param \Krd\SiteBundle\Entity\PollAnswer $answers
     */
    public function removeAnswer(\Krd\SiteBundle\Entity\PollAnswer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Общее количество проголосовавших
     * @return integer
     */
    public function getAnswersCount()
    {
        $i = 0;

        foreach ($this->getAnswers() as $answer) {
            $i += $answer->getCounter();
        }

        return $i;
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActiveAnswers()
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('active', true))
            ->orderBy(array('sort' => Criteria::ASC));

        return $this->getAnswers()->matching($criteria);
    }

    /**
     * Список ответов отсортированный по голосам
     * @return [type] [description]
     */
    public function getAnswersByCounter()
    {
        $criteria = Criteria::create()
            ->orderBy(array('counter' => Criteria::DESC));

        return $this->getAnswers()->matching($criteria);
    }

    /**
     * Set createdBy
     *
     * @param \Q\UserBundle\Entity\User $createdBy
     * @return Poll
     */
    public function setCreatedBy(\Q\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Q\UserBundle\Entity\User $updatedBy
     * @return Poll
     */
    public function setUpdatedBy(\Q\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Q\UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set votedip
     *
     * @param array $votedip
     * @return Poll
     */
    public function setVotedip($votedip)
    {
        $this->votedip = $votedip;

        return $this;
    }

    /**
     * IP already in list
     *
     * @param  string  $votedip
     * @return boolean
     */
    public function hasVotedip($votedip)
    {
        if ($votedip == '127.0.0.1') {
            return false;
        }

        return in_array($votedip, $this->votedip);
    }

    /**
     * Add IP to list
     *
     * @param string $votedip
     * @return Poll
     */
    public function addVotedip($votedip)
    {
        if ($votedip == '127.0.0.1') {
            return $this;
        }

        $this->votedip[] = $votedip;

        $this->votedip = array_unique($this->votedip);

        return $this;
    }

    /**
     * Get votedip
     *
     * @return array
     */
    public function getVotedip()
    {
        return $this->votedip;
    }
}
