<?php

namespace Krd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Event\UserEvent;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;
use Q\UserBundle\Entity\User;
use Krd\UserBundle\KrdUserEvents;
use Krd\UserBundle\Form\Type\UserProfileType;


/**
 * Главная страница личного кабинета
 */
class IndexController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }
    }

    /**
     * @Route("/add_widget/{id}", name="krd_user_add_widget", requirements={"id"="^\d+$"})
     */
    public function addWidgetAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $user->addWidget($id);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->get('qcore.routing.current')->generate(43));
    }

    /**
     * @Route("/remove_widget/{id}", name="krd_user_remove_widget", requirements={"id"="^\d+$"})
     */
    public function removeWidgetAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $user->removeWidget($id);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->get('qcore.routing.current')->generate(43));
    }

    /**
     * @Route("/sort_widgets/", name="krd_user_sort_widgets", defaults={"_format"="json"})
     * @Method({"POST"})
     * @Template()
     */
    public function sortWidgetsAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $user = $this->get('security.context')->getToken()->getUser();

        $ids = $request->get('ids');
        $ids = array_map('intval', $ids);

        if (count($ids) == count($user->getWidgets())) {
            $user->setWidgets($ids);
            $this->getDoctrine()->getManager()->flush();

            return array('success' => true);
        }

        return array('success' => false);
    }

    /**
     * Профиль пользователя
     *
     * @Route("/profile/", name="krd_user_profile")
     * @Template()
     */
    public function profileAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToAuth();
        }

        $this->get('qcore.routing.breadcrumb')->addItemArray(array('title' => 'Профиль', 'url' => '#'));

        return array('form' => $this->getForm());
    }

    /**
     * Проверка авторизации и редирект на форму авторизации
     */
    protected function redirectToAuth()
    {
        $node = $this->getDoctrine()->getManager()->getRepository('QCoreBundle:Node')->createQueryBuilder('e')
            ->andWhere('e.controller = :controller')
            ->setParameter('controller', 41)
            ->addOrderBy('e.left', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
                ->getResult();

        if (isset($node[0])) {
            return $this->redirect($node[0]->getUrl(true));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Построение формы
     */
    protected function getForm()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user instanceof User) {
            return $this->createForm(new UserProfileType(), $user, array(
                'action' => $this->get('router')->generate('krd_user_profile_submit'),
                'method' => 'POST',
            ));
        } else {
            throw new \Exception('User not login');
        }
    }

    /**
     * Сабмит формы профиля
     *
     * @Route("/ajax/krd/user/profile/submit/", name="krd_user_profile_submit")
     * @Method({"POST"})
     */
    public function submitProfileAction(Request $request)
    {
        $form = $this->getForm();
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse(array('success' => false, 'errors' => $this->get('qcore.form.helper.errors')->getErrors($form)));
        }

        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');
        $user = $form->getData();

        if ($user instanceof User) {

            $response = new JsonResponse(array('success' => true, 'message' => 'Профиль успешно обновлен'));

            $userManager->updateUser($user);

            // $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        } else {
            $this->get('logger')->crit('Ошибка редактирования профиля');

            return new JsonResponse(array('success' => false, 'error' => 'Ошибка сервера'));
        }
    }
}
