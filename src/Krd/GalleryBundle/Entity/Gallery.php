<?php

namespace Krd\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

use Q\CoreBundle\Entity\NodeUrlIntf;
use Q\FormMetadataBundle\Configuration as Form;
use Q\CoreBundle\Entity\Node;
use Q\FilesBundle\Entity\Image;


/**
 * Галереи
 *
 * @ORM\Entity(repositoryClass="Krd\GalleryBundle\Repository\GalleryRepository")
 * @ORM\Table(name="gallery",
 *      indexes={
 *          @ORM\Index(name="title", columns={"title"}),
 *          @ORM\Index(name="name", columns={"name"}),
 *          @ORM\Index(name="date", columns={"date"}),
 *          @ORM\Index(name="active", columns={"active"}),
 *      })
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Gedmo\Loggable(logEntryClass="Q\CoreBundle\Entity\ShowLogEntry")
 *
 * @Form\Fields(
 *     parent={"hidden_node"},
 *     title={"text", {"label"="Название"}},
 *     name={"text", {"label"="Системное имя"}},
 *     date={"datetime", {
 *         "label"="Дата/Время",
 *         "attr"={"datetime-picker"=""},
 *         "widget"="single_text",
 *         "format"="yyyy-MM-dd H:mm"
 *     }},
 *     active={"checkbox", {
 *         "label"="Активность",
 *         "required"=false
 *     }},
 *     images={"files", {
 *         "label"="Изображения",
 *         "class"="QFilesBundle:Image",
 *         "widget"="images"
 *     }},
 *     submit={"submit", {"label"="Отправить"}}
 * )
 */
class Gallery implements NodeUrlIntf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Q\CoreBundle\Entity\Node")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * Название
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Assert\NotBlank
     *
     * @Gedmo\Versioned
     */
    private $title;

    /**
     * Дата
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime<'d.m.Y'>")
     *
     * @Gedmo\Versioned
     *
     * @Assert\DateTime
     * @Assert\NotBlank
     */
    private $date;

    /**
     * Системное имя
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     *
     * @Gedmo\Versioned
     *
     * @Q\CoreBundle\Validator\Constraints\SystemName
     */
    private $name;

    /**
     * Активность
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Gedmo\Versioned
     *
     * @Assert\Type("boolean")
     */
    private $active = false;

    /**
     * @ORM\ManyToMany(targetEntity="Q\FilesBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="gallery_image_relation",
     *      joinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     *
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     * @JMS\Expose
     */
    private $images;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Ссылка на страницу
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\SerializedName("url")
     * @JMS\Accessor(getter="getUrl")
     */
    private $url = '#';

    /**
     * @JMS\Expose
     * @JMS\SerializedName("image_main")
     * @JMS\Accessor(getter="getMainImage")
     */
    private $mainImage;

    /**
     * @JMS\Expose
     * @JMS\SerializedName("images_count")
     * @JMS\Accessor(getter="getImagesCount")
     */
    private $imagesCount = 0;

    /**
     * Галерея просматривается в данный момент
     */
    private $current = false;
	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $refid;


    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = (boolean)$active;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function setCurrent($current)
    {
        $this->current = (boolean)$current;
    }

    public function getImages()
    {
        return $this->images ?: $this->images = new ArrayCollection();
    }

    public function updateImagesCount()
    {
        $this->imagesCount = count($this->getImages());
    }

    public function getImagesCount()
    {
        return $this->imagesCount == 0 ? count($this->getImages()) : $this->imagesCount;
    }

    public function hasImages()
    {
        return count($this->getImages()) > 0;
    }
	
	public function setRefId($id) {
		$this->refid = $id;
	}
	
	public function getRefId()
	{
		return $this->refid;
	}

    /**
     * Возвращает изображение отмеченное "Главное"
     */
    public function getMainImage()
    {
        foreach($this->getImages() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        $images = $this->getImages();

        return $images[0];
    }

    public function addImages(Image $image)
    {
        if (!$this->getImages()->contains($image)) {
            $this->getImages()->add($image);
        }
    }

    public function removeImages(Image $image)
    {
        if ($this->getImages()->contains($image)) {
            $this->getImages()->removeElement($image);
        }
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getDetailRoute()
    {
        return 'krd_gallery_gallery_detail';
    }
}


