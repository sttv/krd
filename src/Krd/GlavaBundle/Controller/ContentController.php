<?php

namespace Krd\GlavaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;
use Q\CoreBundle\Controller\ActiveSecuredController;


/**
 * Контентная страница
 */
class ContentController extends Controller implements ActiveSecuredController
{
    /**
     * @Route("/")
     * @Template()
     */
	//@Secure(roles="ROLE_CMS")
    public function indexAction()
    {
    }
	
	/**
	 * @Route("/contact-{id}.{_format}", requirements={"id"="^\d+$", "_format"="html"})
     * @Method({"GET"}) 
	 */
	//@Secure(roles="ROLE_CMS")
	public function detailAction(Request $request) {
		$human = $this->getDoctrine()->getManager()->find('KrdAdministrationBundle:Human', 128);
		
		return $this->render('KrdGlavaBundle:Module:content/detail.html.twig', array('human' => $human));
	}
	
	/**
	 * @Route("/ajax/glava/content/contacts.join", name="krd_glava_content_contacts", defaults={"_format"="json"})
	 * @Method({"GET"}) 
	 */
	public function contactsAction(Request $request)
	{
		$human = $this->getDoctrine()->getManager()->find('KrdAdministrationBundle:Human', 128);
		
		return $this->render('KrdGlavaBundle:Module:schedule/info.html.twig', array('item' => $human));
	}
}
