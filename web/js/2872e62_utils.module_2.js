/**
 * Полезные инструменты
 */
angular.module('utils', [])
	/**
	 * Ползволяет менять знаения свойств объектов.
	 * Например, если у вас есть строка "any.object.prop" и нужно поменять это свойство.
	 *
	 * Пример: scopeApply($scope, attrs.ngModel, value);
	 */
	.factory('scopeApply', [function(){
		// $scope - должно передаваться сюда параметром
		return function($scope, path, value){
			if (path.indexOf('.') == -1){
				$scope[path] = value;
			}else{
				var _model = path.split('.');
				_model = _model.slice(0, _model.length - 1).join('.');
				var _object = $scope.$eval(_model);
				_object[path.split('.').pop()] = value;
			}

			if(!$scope.$$phase) {
				$scope.$digest();
			}
		};
	}])

    /**
     * Фильтр транслитерации
     */
    .filter('translite', [function(){
        var List = {
                'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
                'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
                'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
                'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
                'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
                'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
                'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
                'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
                'Я':'Ya','я':'ya'
            },
            regular = '',
            k;

        for (k in List){
            regular += k;
        }

        regular = new RegExp('[' + regular + ']', 'g');

        k = function(a){
            return a in List ? List[a] : '';
        };

        return function(input){
            if (!!input){
                return input.replace(regular, k);
            }else{
                return '';
            }
        };
    }]);
