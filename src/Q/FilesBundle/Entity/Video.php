<?php

namespace Q\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;

use Q\FormMetadataBundle\Configuration as Form;


/**
 * Загружаемое видео
 *
 * @ORM\Entity(repositoryClass="Q\FilesBundle\Repository\VideoRepository")
 * @ORM\Table(name="video",
 *     indexes={
 *          @ORM\Index(name="sort", columns={"sort"}),
 *          @ORM\Index(name="converted", columns={"converted"}),
 *     }
 * )
 *
 * @FileStore\Uploadable
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @Form\Fields(
 *     title={"text", {"label"="Название"}},
 *     file={"file", {"label"="Файл"}}
 * )
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @JMS\Expose
     */
    private $title = '';

    /**
     * Загруженный видео файл
     * @ORM\Column(type="array")
     * @FileStore\UploadableField(mapping="video")
     *
     * @Assert\File(mimeTypes={"video/mpeg", "video/mp4", "video/ogg", "video/quicktime", "video/webm", "video/x-ms-wmv", "video/x-flv", "video/avi", "video/msvideo", "video/x-msvideo", "video/xmpg2", "application/x-troff-msvideo", "application/octet-stream"})
     *
     * @JMS\Expose
     */
    private $file;

    /**
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     *
     * @JMS\Expose
     **/
    private $image;

    /**
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     */
    private $sort = 0;

    /**
     * @ORM\OneToMany(targetEntity="VideoFormat", mappedBy="parent", cascade={"persist", "remove"})
     * 
     * @JMS\Expose
     */
    private $format;

    /**
     * Видео было сконвертировано?
     * @ORM\Column(type="boolean")
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     *
     * @Assert\Type("boolean")
     */
    private $converted = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="Q\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = (string)$title;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (empty($this->title)) {
            $this->title = $file->getClientOriginalName();
        }
    }

    public function setFileArray(array $file)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function hasFile()
    {
        return !empty($this->file);
    }

    public function getFilePath()
    {
        return isset($this->file['path']) ? $this->file['path'] : '';
    }

    public function getFilename()
    {
        return isset($this->file['originalName']) ? $this->file['originalName'] : '';
    }

    public function setImage(Image $image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function hasImage()
    {
        return !empty($this->image);
    }

    public function setSort($sort)
    {
        $this->sort = (int)$sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getFormat()
    {
        return $this->format ?: $this->format = new ArrayCollection();
    }

    public function addFormat(VideoFormat $videoFormat)
    {
        if (!$this->getFormat()->contains($videoFormat)) {
            $this->getFormat()->add($videoFormat);
            $videoFormat->setParent($this);
        }
    }

    public function removeFormat(VideoFormat $videoFormat)
    {
        if ($this->getFormat()->contains($videoFormat)) {
            $videoFormat->clearParent();
            $this->getFormat()->removeElement($videoFormat);
        }
    }

    public function getConverted()
    {
        return $this->converted;
    }

    public function setConverted($converted)
    {
        $this->converted = (boolean)$converted;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Ссылка на определенный формат видео
     * @param  string $name
     * @return string
     */
    public function getUrl($name)
    {
        foreach($this->getFormat() as $format) {
            if (mb_strtolower($format->getFormat()) == mb_strtolower($name)) {
                return $format->getFilePath();
            }
        }

        return '';
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}
