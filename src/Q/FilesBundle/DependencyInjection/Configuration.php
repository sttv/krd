<?php

namespace Q\FilesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('q_files');

        $rootNode
            ->children()
                ->scalarNode('upload_dir')->end()
                ->scalarNode('upload_path')->end()
            ->end();

        $rootNode
            ->children()
                ->arrayNode('images')
                    ->children()
                        ->scalarNode('upload_dir')->end()
                        ->scalarNode('upload_path')->end()
                    ->end()
                ->end()
            ->end();

        $rootNode
            ->children()
                ->arrayNode('image_cache')
                    ->children()
                        ->scalarNode('secret')->end()
                        ->scalarNode('upload_dir')->end()
                        ->scalarNode('upload_path')->end()
                        ->arrayNode('sizes')
                            ->prototype('array')
                                ->prototype('array')
                                    ->prototype('scalar')
                        ->end()
                    ->end()
                ->end()
            ->end();

        $rootNode
            ->children()
                ->arrayNode('video')
                    ->children()
                        ->scalarNode('upload_dir')->end()
                        ->scalarNode('upload_path')->end()
                        ->scalarNode('cache_dir')->end()
                        ->scalarNode('cache_path')->end()
                        ->scalarNode('ffmpeg_bin')->end()
                        ->scalarNode('ffprobe_bin')->end()
                        ->scalarNode('qtfaststart_bin')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
