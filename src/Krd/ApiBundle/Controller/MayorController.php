<?php

namespace Krd\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Q\CoreBundle\Doctrine\Tools\Pagination\Paginator;


/**
 * Руководители/почетные граждане города
 *
 * @Route("/mayor")
 */
class MayorController extends Controller
{
    /**
     * Список
     *
     * Обязательные параметры:
     *  parent - ID родительской категории
     *
     * Возможные параметры:
     *  page - страница
     *  count - количество на странице
     *
     * @Route("/list/{parent}.{_format}", requirements={"parent"="^\d+$", "_format"="json"})
     * @Method({"GET"})
     */
    public function listAction(Request $request, $parent)
    {
        $queryBuilder = $this->getDoctrine()->getRepository('KrdAdministrationBundle:Mayor')
            ->createQueryBuilder('n')
//            ->leftJoin('n.images', 'i')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->setParameter('parent', $parent)
            ->orderBy('n.sort', 'ASC');

        return Paginator::createFromRequest($request, $queryBuilder->getQuery());
    }

    /**
     * Детально
     *
     * @Route("/item/{id}.{_format}", requirements={"_format"="json"})
     * @Method({"GET"})
     */
    public function itemAction($id)
    {
        $item = $this->getDoctrine()->getRepository('KrdAdministrationBundle:Mayor')->find($id);

        if (!$item || !$item->getActive()) {
            throw $this->createNotFoundException();
        }

        $response = new Response();
        $response->headers->set('Content-type', 'application/json');

        $json = $this->get('jms_serializer')->serialize($item, 'json');
        $json = json_decode($json, true);
        $json['content'] = $item->getContent();
        $json['url_full'] = $item->getParent()->getUrl().$item->getUrl();
        $json['announce'] = mb_substr(strip_tags($json['content']), 0, 150).'...';
        $json = json_encode($json);

        $response->setContent($json);

        return $response;
    }
}
