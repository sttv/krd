<?php

namespace Q\CoreBundle\Repository;

use Doctrine\ORM\Events as DoctrineEvents;

use Q\CoreBundle\Entity\Node;
use Q\CoreBundle\Entity\NodeController;
use Q\CoreBundle\Entity\NodeUrlAlias;
use Q\CoreBundle\Tools\RouterCacheCleaner;


/**
* Репозиторий Node - структуры сайта
*/
class NodeRepository extends \Gedmo\Tree\Entity\Repository\NestedTreeRepository
{
    /**
     * Количество дочерних элементов
     * @return integer
     */
    public function getChildrenCount(Node $node)
    {
        $nodeTable = $this->_em->getClassMetadata('QCoreBundle:Node')->getTableName();

        $st = $this->_em->getConnection()->prepare("SELECT count(*) as 'count' FROM `{$nodeTable}` WHERE `left` > :left AND `right` < :right");
        $st->execute(array('left' => $node->getLeft(), 'right' => $node->getRight()));

        if ($result = $st->fetch()) {
            return isset($result['count']) ? $result['count'] : 0;
        } else {
            return 0;
        }
    }

    /**
    * Обновление Node
    * @param  Node   $node
    */
    public function update(Node $node)
    {
        $this->_em->persist($node);
        $this->_em->flush();

        if ($node->getParent()) {
            $this->updateUrl($node);
        }
    }

    /**
    * Обновление URL нода и URL всех дочерних нодов
    * @param  Node   $node
    */
    public function updateUrl(Node $node)
    {
        $node->setUrl($this->getUrl($node));
        $this->_em->flush();

        $this->updateUrlAlias($node);
        $this->updateUrlHash($node);

        $childQuery = $this->createQueryBuilder('n')
            ->andWhere('n.left > :left')
            ->andWhere('n.right < :right')
            ->orderBy('n.left')
            ->setParameters(array(
            'left' => $node->getLeft(),
            'right' => $node->getRight(),
            ))
            ->setMaxResults(300)
                ->getQuery();

        $i = 0;
        foreach($childQuery->iterate() as $child) {
            $child = $child[0];

            $child->setUrl($this->getUrl($child));
            $this->updateUrlAlias($child);
            $this->updateUrlHash($child);

            if ($i % 100 == 0) {
                $this->_em->flush();
                $this->_em->clear('QCoreBundle:Node');
            }
        }

        $this->_em->flush();
    }

    /**
    * Обновляет хеши URL
    * @param  Node   $node
    */
    public function updateUrlHash(Node $node)
    {
        $node->setUrlHash(md5($node->getUrl()));
        $node->setCustomUrlHash(md5($node->getCustomUrl()));
        $node->setUrlAliasHash(md5($node->getUrlAlias()));
    }

    /**
    * Обновляет алиасы на ноды
    * @param  Node   $node
    */
    public function updateUrlAlias(Node $node)
    {
        $node->setUrlAlias(NULL);

        if ($node->hasCustomUrl()) {
            $node->setUrlAlias($node->getCustomUrl());
        } else {
            if ($parent = $node->getParent()) {
                if ($parent->hasUrlAlias()) {
                    $url = $parent->getUrl(true).$node->getName();
                    $node->setUrlAlias('/'.trim($url, ' /').'/');
                }
            }
        }
    }

    /**
    * Получение ссылки на страницу
    * @param  Node   $node
    * @return string
    */
    public function getUrl(Node $node)
    {
        if ($parent = $node->getParent()) {
            $url = $parent->getUrl(false).$node->getName();
        } else {
            $url = $node->getName();
        }

        if (empty($url)) {
            return '/';
        } else {
            return '/'.trim($url, ' /').'/';
        }
    }

    /**
    * Получение родительской ветки
    * Сортировка сверху вниз
    * @param  Node   $node
    * @param  boolean $includeNode Включать сам нод в результат
    * @return array
    */
    public function getParentBranch(Node $node, $includeNode = false)
    {
        $result = $this->_em->createQuery('SELECT n FROM '.$this->_entityName.' n WHERE n.left < :left AND n.right > :right ORDER BY n.left')
            ->setParameter('left', $node->getLeft())
            ->setParameter('right', $node->getRight())
            ->setMaxResults($node->getLevel() + 1)
            ->getResult();

        if ($includeNode) {
            $result[] = $node;
        }

        return $result;
    }

    /**
    * [isFullyActive description]
    * @param  Node    $node [description]
    * @return boolean       [description]
    */
    public function isFullyActive(Node $node)
    {
        if (!$node->isActive()) {
            return false;
        }

        foreach($this->getParentBranch($node, false) as $parent) {
            if (!$parent->isActive()) {
                return false;
            }
        }

        return true;
    }

    /**
    * Перемещение ноды
    * @param  Node             $node
    * @param  Node             $parentNode
    * @param  string[optional] $moveType
    */
    public function moveNode(Node $node, Node $parentNode, $moveType = 'inner')
    {
        $this->_em->persist($node);
        $this->_em->persist($parentNode);

        if ($node->getId() === $parentNode->getId()) {
            return;
        }

        switch ($moveType) {
            case 'next':
            $this->persistAsNextSiblingOf($node, $parentNode);
            break;

            case 'prev':
            $this->persistAsPrevSiblingOf($node, $parentNode);
            break;

            case 'inner':
            case 'innerlast':
            $this->persistAsLastChildOf($node, $parentNode);
            break;

            case 'innerfirst':
            $this->persistAsFirstChildOf($node, $parentNode);
            break;
        }

        $this->_em->flush();
    }

    /**
    * Получение следующего по сорировке элемента
    * @param  Node   $node
    * @param  integer $parent
    * @return Node|NULL
    */
    public function getNextItem(Node $node, $parent)
    {
        $result = $this->_em
        ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.level = :level AND n.parent = :parent AND n.left > :left ORDER BY n.left')
            ->setMaxResults(2)
            ->setParameters(array(
            'level' => $node->getLevel(),
            'parent' => (int)$parent,
            'left' => $node->getLeft(),
            ))
            ->getResult();

        if (isset($result[0])) {
            return $result[0];
        }
    }

    /**
    * Получение предыдущего по сорировке элемента
    * @param  Node   $node
    * @param  integer $parent
    * @return Node|NULL
    */
    public function getPrevItem(Node $node, $parent)
    {
        $result = $this->_em
        ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.level = :level AND n.parent = :parent AND n.left < :left ORDER BY n.left DESC')
            ->setMaxResults(2)
            ->setParameters(array(
            'level' => $node->getLevel(),
            'parent' => (int)$parent,
            'left' => $node->getLeft(),
            ))
            ->getResult();

        if (isset($result[0])) {
            return $result[0];
        }
    }

    /**
    * Получает общее число нодов
    */
    public function getCount()
    {
        return $this->childCount($this->find(1));
    }

    /**
    * Получение Node по URL
    * @return Node
    */
    public function getNodeByPath($path)
    {
        if ($path == '/') {
            return $this->find(1);
        }

        try {
            $urlTestList = array('/'.trim($path, '/').'/');
            $tmp = strrev($path);
            while (($tmp = ltrim(strstr($tmp, '/'), '/')) && mb_strlen($tmp) > 1) {
                $urlTestList[] = '/'.trim(strrev($tmp), '/').'/';
            }

            $urlTestList = array_map('md5', $urlTestList);

            $urlTestListQuoted = array();

            foreach ($urlTestList as $hash) {
                $urlTestListQuoted[] = '\''.$hash.'\'';
            }

            return $this->_em
            ->createQuery("SELECT n, FIELD(n.urlaliashash,".implode(',', $urlTestListQuoted).") as HIDDEN ord1, FIELD(n.urlhash,".implode(',', $urlTestListQuoted).") as HIDDEN ord2
                          FROM QCoreBundle:Node n
                          WHERE n.urlaliashash IN (:urltestlist) OR n.urlhash IN (:urltestlist)
                          ORDER BY ord1, ord2")
                ->setParameter('urltestlist', $urlTestList)
                ->setMaxResults(1)
                ->useResultCache(true, 5)
                ->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
    // Nothing
        }
    }
}
