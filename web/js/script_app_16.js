(function() {
    angular.extend( angular, {
        toParam: toParam
    });

    /**
    * Преобразует объект, массив или массив объектов в строку,
    * которая соответствует формату передачи данных через url
    * Почти эквивалент [url]http://api.jquery.com/jQuery.param/[/url]
    * Источник [url]http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object/1714899#1714899[/url]
    *
    * @param object
    * @param [prefix]
    * @returns {string}
    */
    function toParam( object, prefix ) {
        var stack = [];
        var value;
        var key;

        for( key in object ) {
            value = object[ key ];
            key = prefix ? prefix + '[' + key + ']' : key;

            if ( value === null ) {
                value = encodeURIComponent( key ) + '=';
            } else if ( typeof( value ) !== 'object' ) {
                value = encodeURIComponent( key ) + '=' + encodeURIComponent( value );
            } else {
                value = toParam( value, key );
            }

            stack.push( value );
        }

        return stack.join( '&' );
    }
}());


angular.module('krd', ['ui.date', 'interactive', 'header', 'widget.reporter', 'horizontal.scroll', 'gallery.slider', 'icons.slider', 'news.slider', 'calendar', 'content', 'administration', 'dialog', 'forms', 'mayor', 'video', 'city-object', 'documents', 'search', 'evacuation', 'appeal', 'citynum', 'dictionary', 'polls', 'subscribe', 'socialgroups', 'indexmap', 'lazy', 'banners'])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.post[ 'Content-Type' ] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.transformRequest = function( data ) {
            return angular.isObject( data ) && String( data ) !== '[object File]' ? angular.toParam( data ) : data;
        };
    }])

    .run([function() {
        var $imgLinks = $('.text-block a[href$=".bmp"]')
            .add('.text-block a[href$=".gif"]')
            .add('.text-block a[href$=".jpg"]')
            .add('.text-block a[href$=".jpeg"]')
            .not('.disable-fancybox')
            .not('.fancybox')
            .not('.lightbox')
            .addClass('.fancybox-auto-init')
        ;


        $('.fancybox-auto-init').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $('.fancybox, .lightbox').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            },
            live: true
        });

        $(window).on('load', function() {
            new MaSha({
                selectable: $('.page-content')[0],
                select_message: 'upmsg-selectable'
            });
        });

        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }])
;
