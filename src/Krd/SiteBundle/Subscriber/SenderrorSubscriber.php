<?php

namespace Krd\SiteBundle\Subscriber;

use Krd\SiteBundle\Entity\Senderror;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;


/**
 * События для Senderror
 */
class SenderrorSubscriber implements EventSubscriber
{
    protected $container;
    protected $mailer;
    protected $logger;
    protected $deferred = array();

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Сбор ново-добавленных обращений
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Senderror) {
            $this->deferred[] = $entity;
        }
    }

    /**
     * Отправка Email уведомлений о новых сообщениях
     * Если не требуется экспорт во вне сразу ставим соответствующий статус
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $list = $this->deferred;
        $this->deferred = array();

        foreach($list as $entity) {
            $message = $this->getMailer()->newMessage('Сообщение об ошибке на сайте %title%');

            try {
                $this->getMailer()->sendTemplate($message, 'KrdSiteBundle:Email:new-senderror.admin.html.twig', array('feedback' => $entity));
            } catch (\Exception $e) {
                $this->logger->crit('Ошибка отправки email уведомления администратору о сообщении об ошибке', array($entity->getId(), $e->getMessage()));
            }

            $email = $entity->getEmail();

            if (!empty($email)) {
                $message = $this->getMailer()->newMessage('Ваше сообщение об ошибке принято');
                $message->setTo($email);

                try {
                    $this->getMailer()->sendTemplate($message, 'KrdSiteBundle:Email:new-senderror.user.html.twig', array('feedback' => $entity));
                } catch (\Exception $e) {
                    $this->logger->crit('Ошибка отправки email уведомления пользователю о сообщении об ошибке', array($entity->getId(), $e->getMessage()));
                }
            }

        }
    }
}

