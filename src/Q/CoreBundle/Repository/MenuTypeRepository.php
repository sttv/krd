<?php

namespace Q\CoreBundle\Repository;

use Doctrine\ORM\NoResultException;

use Q\CoreBundle\Doctrine\ORM\EntityRepository;
use Q\CoreBundle\Entity\MenuType;


class MenuTypeRepository extends EntityRepository
{
    /**
     * Поиск типа меню по имени
     * @param  string $name
     * @return MenyType|NULL
     */
    public function findOneByName($name)
    {
        try {
            return $this->createQueryBuilder('m')
                ->andWhere('m.name = :name')
                ->setParameter('name', $name)
                ->setMaxResults(1)
                ->getQuery()
                    ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
