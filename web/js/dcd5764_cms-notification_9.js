/**
 * Модуль уведомлений работает через $rootScope.$broadcast
 */
angular.module('cms.notification', [])
    .controller('NotificationCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
        /**
         * Список сообщений
         */
        $scope.alerts = [];

        var counter = 0;

        /**
         * Добавление алерта
         */
        $scope.addAlert = function(msg, type, delay) {
            $scope.$apply(function() {
                $scope.alerts.push({id:counter, msg:msg, type:type});
                counter++;
            });

            if (delay !== undefined && delay > 1000) {
                var toClose = counter - 1;

                setTimeout(function() {
                    for(var i = 0; i < $scope.alerts.length; i++) {
                        if ($scope.alerts[i].id == toClose) {
                            $scope.$apply(function() {
                                $scope.closeAlert(i);
                            });
                            break;
                        }
                    }
                }, delay);
            }
        };

        /**
         * Закрытие алерта
         */
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        /**
         * Событие добавления уведомления
         */
        $rootScope.$on('notification', function(event, notification) {
            setTimeout(function() {
                $scope.addAlert(notification.msg, notification.type, notification.delay);
            }, 13);
        });

        /**
         * Краткие способы доабвления уведомлений
         */
        $rootScope.$on('ntf-success', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'success', delay);
            }, 13);
        });

        $rootScope.$on('ntf-info', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'info', delay);
            }, 13);
        });

        $rootScope.$on('ntf-warning', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'warning', delay);
            }, 13);
        });

        $rootScope.$on('ntf-error', function(event, notification, delay) {
            setTimeout(function() {
                $scope.addAlert(notification, 'error', delay);
            }, 13);
        });
    }])
;
