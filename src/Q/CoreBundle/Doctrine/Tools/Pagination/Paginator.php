<?php

namespace Q\CoreBundle\Doctrine\Tools\Pagination;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator as BasePaginator;
use Doctrine\ORM\Query;
use JMS\Serializer\Annotation as JMS;


/**
 * Пагинатор добавляющий новые возможности
 *
 * @JMS\ExclusionPolicy("all")
 */
class Paginator extends BasePaginator implements \ArrayAccess
{
    /**
     * Шаблон ссылки на страницу
     * @var string
     */
    protected $urlTemplate = '#{page}';

    /**
     * Размер разрыва смежных страниц
     * @var integer
     */
    protected $adjacents = 2;

    /**
     * Включен ли кеш результатов запросов?
     * @var boolean
     */
    protected static $queryResultCache = true;

    /**
     * Включаем кеш результатов запросов
     */
    public static function enableQueryCache()
    {
        self::$queryResultCache = true;
    }

    /**
     * Выключаем кеш результатов запросов
     */
    public static function disableQueryCache()
    {
        self::$queryResultCache = false;
    }

    /**
     * Создание пагинатора из запроса
     * @param  Request $request
     * @param  Query $query
     * @param  boolean[optional] $fetchJoinCollection
     * @param  integer[optional] $count
     * @param  string[optional] $urlTemplate
     * @return Paginator
     */
    public static function createFromRequest(Request $request, Query $query, $fetchJoinCollection = true, $count = null, $urlTemplate = null)
    {
        $page = $request->get('page', 1);

        if ($page < 1) {
            $page = 1;
        }

        if (is_null($count)) {
            $count = $request->get('count', 10);
        }

        if ($count > 100) {
            $count = 100;
        }

        if ($count < 0) {
            $count = 1;
        }

        $query->setFirstResult($page*$count - $count)
            ->setMaxResults($count)
            ->useResultCache(self::$queryResultCache, 60);

        $paginator = new self($query, $fetchJoinCollection);

        if (!is_null($urlTemplate)) {
            $paginator->setUrlTemplate($urlTemplate);
        }

        return $paginator;
    }

    /**
     * Создание пагинатора
     * @param  Query   $query
     * @param  integer[optional] $page
     * @param  integer[optional] $count
     * @param  boolean[optional] $fetchJoinCollection
     * @param  string[optional] $urlTemplate
     * @return Paginator
     */
    public static function create(Query $query, $page = 1, $count = 10, $fetchJoinCollection = true, $urlTemplate = null)
    {
        if ($page < 1) {
            $page = 1;
        }

        if ($count > 100) {
            $count = 100;
        }

        $query->setFirstResult($page*$count - $count)
            ->setMaxResults($count)
            ->useResultCache(self::$queryResultCache, 15);

        $paginator = new self($query, $fetchJoinCollection);

        if (!is_null($urlTemplate)) {
            $paginator->setUrlTemplate($urlTemplate);
        }

        return $paginator;
    }

    protected $cachePageCount;

    /**
     * Общее количество страниц
     * @return integer
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function getPageCount()
    {
        if (is_null($this->cachePageCount)) {
            $this->cachePageCount = ceil(count($this) / $this->getQuery()->getMaxResults());
        }

        return $this->cachePageCount;
    }

    protected $cacheOnpageCount;

    /**
     * Количество элементов на странице
     * @return integer
     */
    public function getOnpageCount()
    {
        if (is_null($this->cacheOnpageCount)) {
            $this->cacheOnpageCount = $this->getQuery()->getMaxResults();
        }

        return $this->cacheOnpageCount;
    }

    protected $cacheCurrentPage;

    /**
     * Текущая страница
     * @return integer
     */
    public function getCurrentPage()
    {
        if (is_null($this->cacheCurrentPage)) {
            $this->cacheCurrentPage = ($this->getQuery()->getFirstResult() + $this->getOnpageCount()) / $this->getOnpageCount();
        }

        return $this->cacheCurrentPage;
    }

    /**
     * Объект текущей страницы
     * @return Page
     */
    public function getCurrentPageObj()
    {
        foreach ($this->getPages() as $page) {
            if ($page->isActive()) {
                return $page;
            }
        }

        return new Page(1, $this, false);
    }

    /**
     * Объект следующей страницы
     * @return Page
     */
    public function getNextPageObj()
    {
        $pages = $this->getPages();

        foreach ($pages as $i => $page) {
            if ($page->isActive() && !$page->isLast()) {
                return $pages[$i+1];
            }
        }

        return new Page(1, $this, false);
    }

    /**
     * Объект предыдущей страницы
     * @return Page
     */
    public function getPrevPageObj()
    {
        $pages = $this->getPages();

        foreach ($pages as $i => $page) {
            if ($page->isActive() && !$page->isFirst()) {
                return $pages[$i-1];
            }
        }

        return new Page(1, $this, false);
    }

    /**
     * Разрыв смежных страниц
     * @return integer
     */
    public function getAdjacents()
    {
        return $this->adjacents;
    }

    /**
     * Устанавливает разрыв смежных странц
     * @param integer $adjacents
     */
    public function setAdjacents($adjacents)
    {
        $this->adjacents = $adjacents;
    }

    /**
     * Устанавливает шаблон ссылки
     * @param string $urlTemplate Шаблон ссылки с {page} в виде номер страницы
     */
    public function setUrlTemplate($urlTemplate)
    {
        $urlTemplate = str_replace('%7Bpage%7D', '{page}', $urlTemplate);

        if (mb_strpos($urlTemplate, '{page}') === false) {
            if (mb_strpos($urlTemplate, '?') === false) {
                $urlTemplate .= '?page={page}';
            } else {
                $urlTemplate .= '&page={page}';
            }
        }

        $this->urlTemplate = $urlTemplate;
    }

    /**
     * Шаблон ссылки на страницы
     * @return string
     */
    public function getUrlTemplate()
    {
        return $this->urlTemplate;
    }

    /**
     * Возвращает список страниц
     * @return array
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function getPages()
    {
        $nextPage = $this->getCurrentPage() + 1;
        $prevPage = $this->getCurrentPage() - 1;
        $lastPage = $this->getPageCount();
        $lastPrevPage = $lastPage - 1;

        $pagesList = array();

        if ($lastPage > 1) {
            if ($lastPage < 7 + ($this->adjacents * 2)) {
                for ($i = 1; $i <= $lastPage; $i++) {
                    $pagesList[] = new Page($i, $this);
                }
            } else {
                if ($this->getCurrentPage() < 1 + ($this->adjacents * 3)) {
                    for ($i = 1; $i < 4 + ($this->adjacents * 2); $i++) {
                        $pagesList[] = new Page($i, $this);
                    }

                    $pagesList[] = new Page(0, $this, true);
                    $pagesList[] = new Page($lastPrevPage, $this);
                    $pagesList[] = new Page($lastPage, $this);
                } elseif (($lastPage - ($this->adjacents * 2) > $this->getCurrentPage()) && ($this->getCurrentPage() > ($this->adjacents * 2))) {
                    $pagesList[] = new Page(1, $this);
                    $pagesList[] = new Page(2, $this);
                    $pagesList[] = new Page(0, $this, true);

                    for ($i = $this->getCurrentPage() - $this->adjacents; $i <= $this->getCurrentPage() + $this->adjacents; $i++) {
                        $pagesList[] = new Page($i, $this);
                    }

                    $pagesList[] = new Page(0, $this, true);
                    $pagesList[] = new Page($lastPrevPage, $this);
                    $pagesList[] = new Page($lastPage, $this);
                } else {
                    $pagesList[] = new Page(1, $this);
                    $pagesList[] = new Page(2, $this);
                    $pagesList[] = new Page(0, $this, true);

                    for ($i = $lastPage - (1 + $this->adjacents * 3); $i <= $lastPage; $i++) {
                        $pagesList[] = new Page($i, $this);
                    }
                }
            }
        }

        return $pagesList;
    }

    /**
     * Общее количество найденных элементов
     * @return  integer
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function getItemsCount()
    {
        return count($this);
    }

    /**
     * Список элементов
     * @return array
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"user"})
     */
    public function getItems()
    {
        $items = array();
        foreach($this as $item) {
            $items[] = $item;
        }

        return $items;
    }

    public function offsetSet($offset, $value)
    {
        throw new \Exception('You can not set the page');
    }

    public function offsetExists($offset)
    {
        $items = $this->getItems();
        return isset($items[$offset]);
    }

    public function offsetUnset($offset)
    {
        throw new \Exception('You can not unset the page');
    }

    public function offsetGet($offset)
    {
        $items = $this->getItems();
        return isset($items[$offset]) ? $items[$offset] : null;
    }
}


