<?php

namespace Krd\ConferenceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;

use Q\CoreBundle\Router\CurrentNode;
use Q\CoreBundle\Form\DataTransformer\EntityToIntTransformer;
use Q\CoreBundle\Form\DataTransformer\FilterHtmlTransformer;


/**
 * Вопрос к онлайн конференции
 */
class ConferenceQuestionType extends AbstractType
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIntTransformer($this->em);
        $transformer->setEntityClass('Krd\ConferenceBundle\Entity\Online');
        $transformer->setEntityRepository("KrdConferenceBundle:Online");
        $transformer->setEntityType("online");

        $builder
            ->add(
                $builder->create('parent', 'hidden', array(
                    'data_class' => null,
                ))->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('fioask', 'text', array(
                    'required' => true,
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 2, 'max' => 255)),
                    ),
                ))->addModelTransformer(new FilterHtmlTransformer())
            )
            ->add(
                $builder->create('question', 'textarea', array(
                    'required' => true,
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 10, 'max' => 10000)),
                    ),
                ))->addModelTransformer(new FilterHtmlTransformer())
            )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => true
        ));
    }

    public function getName()
    {
        return 'conference_question';
    }
}
