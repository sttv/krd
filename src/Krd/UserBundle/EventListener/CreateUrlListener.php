<?php

namespace Krd\UserBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Symfony\Component\DependencyInjection\Container;
use Krd\UserBundle\Entity\UrlIntf;


/**
 * Генерация URL для сущностей личного кабинета
 */
class CreateUrlListener
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var CurrentNode
     */
    protected $router;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getRouter()
    {
        return $this->container->get('qcore.routing.current');
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof UrlIntf) {
            $url = $this->getRouter()->generate($entity->getControllerId(), $entity->getDetailRoute(), array('name' => $entity->getName()));

            $entity->setUrl($url);
        }
    }
}
