<?php

namespace Krd\SiteBundle\Modules;

use Q\CoreBundle\Admin\AbstractModule;


/**
 * Модуль вакансий
 */
class Vacancy extends AbstractModule
{
    public function renderAdminContent()
    {
        return $this->twig->render('KrdSiteBundle:Admin:Module/vacancy.html.twig');
    }

    /**
     * Текущая вакансия
     * @return Vacancy
     */
    public function getCurrent()
    {
        return $this->getRepository()->findOneActiveByNameAndParent($this->request->attributes->get('name'), $this->node->getNode()->getId());
    }

    /**
     * Список для текущей страницы
     * @return array
     */
    protected function getItemsList()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $queryBuilder->setParameter('parent', $this->node->getNode()->getId());

        return $queryBuilder->getQuery()->useResultCache(true, 15)->getResult();
    }

    /**
     * Список
     */
    public function renderContent()
    {
        $result = $this->getItemsList();

        return $this->twig->render('KrdSiteBundle:Module:vacancy/list.html.twig', array('list' => $result));
    }

    /**
     * Подробная страница вакансии
     */
    public function renderDetailContent()
    {
        return $this->twig->render('KrdSiteBundle:Module:vacancy/detail.html.twig', array('item' => $this->getCurrent()));
    }

    /**
     * Вакансии на главной департамента
     */
    public function renderDepartamentIndex()
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('n')
            ->andWhere('n.active = 1')
            ->andWhere('n.parent = :parent')
            ->orderBy('n.date', 'DESC');

        $temp = $this->em
            ->createQuery('SELECT n FROM QCoreBundle:Node n WHERE n.parent = :parent AND n.controller = :controller')
            ->setParameter('parent', $this->node->getNode()->getId())
            ->setParameter('controller', 10)
            ->setMaxResults(1)
            ->useResultCache(true, 15)
            ->getResult();

        if (isset($temp[0])) {
            $node = $temp[0];
        } else {
            return '';
        }

        $queryBuilder->setParameter('parent', $node->getId());

        $result = $queryBuilder->getQuery()
            ->setMaxResults(3)
            ->useResultCache(true, 15)
            ->getResult();

        return trim($this->twig->render('KrdSiteBundle:Module:vacancy/departament-block.html.twig', array('list' => $result, 'node' => $node)));
    }
}
