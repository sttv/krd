<?php

namespace Krd\SubscribeBundle\Subscriber;

use Krd\SubscribeBundle\Entity\ESubscriber;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;

use Q\CoreBundle\Swift\Mailer;
use Krd\SubscribeBundle\Entity\Subscriber;


/**
 * События для ESubscriber
 */
class ESubscriberSubscriber implements EventSubscriber
{
    protected $container;
    protected $mailer;
    protected $logger;
    protected $deferred = array();

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * Ленивая загрузка сервиса qcore.mailer
     *
     * @return Mailer
     */
    protected function getMailer()
    {
        if ($this->mailer === null) {
            $this->mailer = $this->container->get('qcore.mailer');
        }

        return $this->mailer;
    }

    /**
     * Сбор ново-добавленных подписчиков
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof ESubscriber) {
            $this->deferred[] = $entity;
        }
    }

    /**
     * Отправка Email подтверждения неактивированным подписчикам
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $list = $this->deferred;
        $this->deferred = array();

        foreach($list as $entity) {
            if ($entity->getActive()) {
                continue;
            }

            $message = $this->getMailer()->newMessage('Подтверждение подписки на экстренные новости сайта %title%');

            $message->setTo($entity->getEmail());

            try {
                $this->getMailer()->sendTemplate($message, 'KrdSubscribeBundle:Email:e-confirmation.user.html.twig', array('subscriber' => $entity));
            } catch (\Exception $e) {
                $this->logger->crit('Ошибка отправки email подтверждения подписки', array($entity->getId(), $e->getMessage()));
            }
        }
    }
}

